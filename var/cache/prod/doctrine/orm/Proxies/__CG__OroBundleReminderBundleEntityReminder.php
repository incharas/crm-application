<?php

namespace Proxies\__CG__\Oro\Bundle\ReminderBundle\Entity;


/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Reminder extends \Oro\Bundle\ReminderBundle\Entity\Reminder implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }

    /**
     * {@inheritDoc}
     * @param string $name
     */
    public function __get(string $name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__get', [$name]);
        return parent::__get($name);
    }

    /**
     * {@inheritDoc}
     * @param string $name
     * @param mixed  $value
     */
    public function __set(string $name, $value)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__set', [$name, $value]);
        return parent::__set($name, $value);
    }

    /**
     * {@inheritDoc}
     * @param  string $name
     * @return boolean
     */
    public function __isset(string $name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__isset', [$name]);

        return parent::__isset($name);
    }

    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'Oro\\Bundle\\ReminderBundle\\Entity\\Reminder' . "\0" . 'id', 'subject', 'startAt', 'expireAt', 'method', 'interval', 'intervalNumber', 'intervalUnit', 'state', 'relatedEntityId', 'relatedEntityClassName', 'recipient', 'sender', 'createdAt', 'updatedAt', 'sentAt', 'failureException', 'extendEntityStorage'];
        }

        return ['__isInitialized__', '' . "\0" . 'Oro\\Bundle\\ReminderBundle\\Entity\\Reminder' . "\0" . 'id', 'subject', 'startAt', 'expireAt', 'method', 'interval', 'intervalNumber', 'intervalUnit', 'state', 'relatedEntityId', 'relatedEntityClassName', 'recipient', 'sender', 'createdAt', 'updatedAt', 'sentAt', 'failureException', 'extendEntityStorage'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Reminder $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);

        parent::__clone();
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load(): void
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized(): bool
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized): void
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null): void
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer(): ?\Closure
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null): void
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner(): ?\Closure
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties(): array
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setSubject($subject)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSubject', [$subject]);

        return parent::setSubject($subject);
    }

    /**
     * {@inheritDoc}
     */
    public function getSubject()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSubject', []);

        return parent::getSubject();
    }

    /**
     * {@inheritDoc}
     */
    public function getStartAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStartAt', []);

        return parent::getStartAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setExpireAt(\DateTime $expireAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setExpireAt', [$expireAt]);

        return parent::setExpireAt($expireAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getExpireAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getExpireAt', []);

        return parent::getExpireAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setMethod($method)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMethod', [$method]);

        return parent::setMethod($method);
    }

    /**
     * {@inheritDoc}
     */
    public function getMethod()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMethod', []);

        return parent::getMethod();
    }

    /**
     * {@inheritDoc}
     */
    public function setInterval(\Oro\Bundle\ReminderBundle\Model\ReminderInterval $interval)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setInterval', [$interval]);

        return parent::setInterval($interval);
    }

    /**
     * {@inheritDoc}
     */
    public function getInterval()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getInterval', []);

        return parent::getInterval();
    }

    /**
     * {@inheritDoc}
     */
    public function setState($state)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setState', [$state]);

        return parent::setState($state);
    }

    /**
     * {@inheritDoc}
     */
    public function getState()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getState', []);

        return parent::getState();
    }

    /**
     * {@inheritDoc}
     */
    public function setRelatedEntityId($relatedEntityId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRelatedEntityId', [$relatedEntityId]);

        return parent::setRelatedEntityId($relatedEntityId);
    }

    /**
     * {@inheritDoc}
     */
    public function getRelatedEntityId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRelatedEntityId', []);

        return parent::getRelatedEntityId();
    }

    /**
     * {@inheritDoc}
     */
    public function setRelatedEntityClassName($relatedEntityClassName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRelatedEntityClassName', [$relatedEntityClassName]);

        return parent::setRelatedEntityClassName($relatedEntityClassName);
    }

    /**
     * {@inheritDoc}
     */
    public function getRelatedEntityClassName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRelatedEntityClassName', []);

        return parent::getRelatedEntityClassName();
    }

    /**
     * {@inheritDoc}
     */
    public function setRecipient(\Oro\Bundle\UserBundle\Entity\User $owner)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRecipient', [$owner]);

        return parent::setRecipient($owner);
    }

    /**
     * {@inheritDoc}
     */
    public function getRecipient()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRecipient', []);

        return parent::getRecipient();
    }

    /**
     * {@inheritDoc}
     */
    public function getSender()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSender', []);

        return parent::getSender();
    }

    /**
     * {@inheritDoc}
     */
    public function setSender(\Oro\Bundle\UserBundle\Entity\User $sender = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSender', [$sender]);

        return parent::setSender($sender);
    }

    /**
     * {@inheritDoc}
     */
    public function setReminderData(\Oro\Bundle\ReminderBundle\Model\ReminderDataInterface $data)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setReminderData', [$data]);

        return parent::setReminderData($data);
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt(\DateTime $createdAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', [$createdAt]);

        return parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdatedAt', [$updatedAt]);

        return parent::setUpdatedAt($updatedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdatedAt', []);

        return parent::getUpdatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setSentAt(\DateTime $sentAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setSentAt', [$sentAt]);

        return parent::setSentAt($sentAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getSentAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getSentAt', []);

        return parent::getSentAt();
    }

    /**
     * {@inheritDoc}
     */
    public function getFailureException()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFailureException', []);

        return parent::getFailureException();
    }

    /**
     * {@inheritDoc}
     */
    public function setFailureException(\Exception $e)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFailureException', [$e]);

        return parent::setFailureException($e);
    }

    /**
     * {@inheritDoc}
     */
    public function prePersist()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'prePersist', []);

        return parent::prePersist();
    }

    /**
     * {@inheritDoc}
     */
    public function preUpdate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'preUpdate', []);

        return parent::preUpdate();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', []);

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function postLoad()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'postLoad', []);

        return parent::postLoad();
    }

    /**
     * {@inheritDoc}
     */
    public function getStorage(): \ArrayObject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStorage', []);

        return parent::getStorage();
    }

    /**
     * {@inheritDoc}
     */
    public function get(string $name): mixed
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'get', [$name]);

        return parent::get($name);
    }

    /**
     * {@inheritDoc}
     */
    public function set(string $name, mixed $value): static
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'set', [$name, $value]);

        return parent::set($name, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function __call(string $name, array $arguments)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__call', [$name, $arguments]);

        return parent::__call($name, $arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function cleanExtendEntityStorage(): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'cleanExtendEntityStorage', []);

        parent::cleanExtendEntityStorage();
    }

}
