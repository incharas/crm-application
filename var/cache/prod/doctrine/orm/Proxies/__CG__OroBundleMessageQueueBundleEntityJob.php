<?php

namespace Proxies\__CG__\Oro\Bundle\MessageQueueBundle\Entity;


/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Job extends \Oro\Bundle\MessageQueueBundle\Entity\Job implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'id', 'ownerId', 'name', 'status', 'interrupted', 'unique', 'rootJob', 'childJobs', 'createdAt', 'startedAt', 'lastActiveAt', 'stoppedAt', 'data', 'jobProgress'];
        }

        return ['__isInitialized__', 'id', 'ownerId', 'name', 'status', 'interrupted', 'unique', 'rootJob', 'childJobs', 'createdAt', 'startedAt', 'lastActiveAt', 'stoppedAt', 'data', 'jobProgress'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Job $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load(): void
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized(): bool
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized): void
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null): void
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer(): ?\Closure
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null): void
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner(): ?\Closure
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties(): array
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setId($id)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setId', [$id]);

        return parent::setId($id);
    }

    /**
     * {@inheritDoc}
     */
    public function getOwnerId()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOwnerId', []);

        return parent::getOwnerId();
    }

    /**
     * {@inheritDoc}
     */
    public function setOwnerId($ownerId)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOwnerId', [$ownerId]);

        return parent::setOwnerId($ownerId);
    }

    /**
     * {@inheritDoc}
     */
    public function getName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getName', []);

        return parent::getName();
    }

    /**
     * {@inheritDoc}
     */
    public function setName($name)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setName', [$name]);

        return parent::setName($name);
    }

    /**
     * {@inheritDoc}
     */
    public function getStatus()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStatus', []);

        return parent::getStatus();
    }

    /**
     * {@inheritDoc}
     */
    public function setStatus($status)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStatus', [$status]);

        return parent::setStatus($status);
    }

    /**
     * {@inheritDoc}
     */
    public function isInterrupted()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isInterrupted', []);

        return parent::isInterrupted();
    }

    /**
     * {@inheritDoc}
     */
    public function setInterrupted($interrupted)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setInterrupted', [$interrupted]);

        return parent::setInterrupted($interrupted);
    }

    /**
     * {@inheritDoc}
     */
    public function isUnique()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isUnique', []);

        return parent::isUnique();
    }

    /**
     * {@inheritDoc}
     */
    public function setUnique($unique)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUnique', [$unique]);

        return parent::setUnique($unique);
    }

    /**
     * {@inheritDoc}
     */
    public function getRootJob()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRootJob', []);

        return parent::getRootJob();
    }

    /**
     * {@inheritDoc}
     */
    public function setRootJob(\Oro\Component\MessageQueue\Job\Job $rootJob)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRootJob', [$rootJob]);

        return parent::setRootJob($rootJob);
    }

    /**
     * {@inheritDoc}
     */
    public function getCreatedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreatedAt', []);

        return parent::getCreatedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreatedAt(\DateTime $createdAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreatedAt', [$createdAt]);

        return parent::setCreatedAt($createdAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getStartedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStartedAt', []);

        return parent::getStartedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setStartedAt(\DateTime $startedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStartedAt', [$startedAt]);

        return parent::setStartedAt($startedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastActiveAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastActiveAt', []);

        return parent::getLastActiveAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastActiveAt(\DateTime $lastActiveAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastActiveAt', [$lastActiveAt]);

        return parent::setLastActiveAt($lastActiveAt);
    }

    /**
     * {@inheritDoc}
     */
    public function getStoppedAt()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStoppedAt', []);

        return parent::getStoppedAt();
    }

    /**
     * {@inheritDoc}
     */
    public function setStoppedAt(\DateTime $stoppedAt)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStoppedAt', [$stoppedAt]);

        return parent::setStoppedAt($stoppedAt);
    }

    /**
     * {@inheritDoc}
     */
    public function isRoot()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isRoot', []);

        return parent::isRoot();
    }

    /**
     * {@inheritDoc}
     */
    public function getChildJobs()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getChildJobs', []);

        return parent::getChildJobs();
    }

    /**
     * {@inheritDoc}
     */
    public function setChildJobs($childJobs)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setChildJobs', [$childJobs]);

        return parent::setChildJobs($childJobs);
    }

    /**
     * {@inheritDoc}
     */
    public function addChildJob(\Oro\Component\MessageQueue\Job\Job $childJob)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addChildJob', [$childJob]);

        return parent::addChildJob($childJob);
    }

    /**
     * {@inheritDoc}
     */
    public function getData()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getData', []);

        return parent::getData();
    }

    /**
     * {@inheritDoc}
     */
    public function setData(array $data)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setData', [$data]);

        return parent::setData($data);
    }

    /**
     * {@inheritDoc}
     */
    public function getProperties()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getProperties', []);

        return parent::getProperties();
    }

    /**
     * {@inheritDoc}
     */
    public function setProperties(array $properties)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setProperties', [$properties]);

        return parent::setProperties($properties);
    }

    /**
     * {@inheritDoc}
     */
    public function getJobProgress()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getJobProgress', []);

        return parent::getJobProgress();
    }

    /**
     * {@inheritDoc}
     */
    public function setJobProgress($jobProgress)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setJobProgress', [$jobProgress]);

        return parent::setJobProgress($jobProgress);
    }

}
