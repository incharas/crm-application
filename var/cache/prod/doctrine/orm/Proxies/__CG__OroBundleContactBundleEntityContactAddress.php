<?php

namespace Proxies\__CG__\Oro\Bundle\ContactBundle\Entity;


/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class ContactAddress extends \Oro\Bundle\ContactBundle\Entity\ContactAddress implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }

    /**
     * {@inheritDoc}
     * @param string $name
     */
    public function __get(string $name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__get', [$name]);
        return parent::__get($name);
    }

    /**
     * {@inheritDoc}
     * @param string $name
     * @param mixed  $value
     */
    public function __set(string $name, $value)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__set', [$name, $value]);
        return parent::__set($name, $value);
    }

    /**
     * {@inheritDoc}
     * @param  string $name
     * @return boolean
     */
    public function __isset(string $name)
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__isset', [$name]);

        return parent::__isset($name);
    }

    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', 'owner', 'types', 'primary', 'id', 'label', 'street', 'street2', 'city', 'postalCode', 'country', 'region', 'organization', 'regionText', 'namePrefix', 'firstName', 'middleName', 'lastName', 'nameSuffix', 'created', 'updated', 'extendEntityStorage'];
        }

        return ['__isInitialized__', 'owner', 'types', 'primary', 'id', 'label', 'street', 'street2', 'city', 'postalCode', 'country', 'region', 'organization', 'regionText', 'namePrefix', 'firstName', 'middleName', 'lastName', 'nameSuffix', 'created', 'updated', 'extendEntityStorage'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (ContactAddress $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * {@inheritDoc}
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);

        parent::__clone();
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load(): void
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized(): bool
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized): void
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null): void
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer(): ?\Closure
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null): void
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner(): ?\Closure
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties(): array
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function setOwner(\Oro\Bundle\ContactBundle\Entity\Contact $owner = NULL)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOwner', [$owner]);

        return parent::setOwner($owner);
    }

    /**
     * {@inheritDoc}
     */
    public function getOwner()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOwner', []);

        return parent::getOwner();
    }

    /**
     * {@inheritDoc}
     */
    public function getCreated()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCreated', []);

        return parent::getCreated();
    }

    /**
     * {@inheritDoc}
     */
    public function getUpdated()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUpdated', []);

        return parent::getUpdated();
    }

    /**
     * {@inheritDoc}
     */
    public function getTypes()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypes', []);

        return parent::getTypes();
    }

    /**
     * {@inheritDoc}
     */
    public function setTypes(\Doctrine\Common\Collections\Collection $types)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setTypes', [$types]);

        return parent::setTypes($types);
    }

    /**
     * {@inheritDoc}
     */
    public function getTypeNames()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypeNames', []);

        return parent::getTypeNames();
    }

    /**
     * {@inheritDoc}
     */
    public function getTypeByName($typeName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypeByName', [$typeName]);

        return parent::getTypeByName($typeName);
    }

    /**
     * {@inheritDoc}
     */
    public function hasTypeWithName($typeName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'hasTypeWithName', [$typeName]);

        return parent::hasTypeWithName($typeName);
    }

    /**
     * {@inheritDoc}
     */
    public function getTypeLabels()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getTypeLabels', []);

        return parent::getTypeLabels();
    }

    /**
     * {@inheritDoc}
     */
    public function addType(\Oro\Bundle\AddressBundle\Entity\AddressType $type)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addType', [$type]);

        return parent::addType($type);
    }

    /**
     * {@inheritDoc}
     */
    public function removeType(\Oro\Bundle\AddressBundle\Entity\AddressType $type)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeType', [$type]);

        return parent::removeType($type);
    }

    /**
     * {@inheritDoc}
     */
    public function setPrimary($primary)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrimary', [$primary]);

        return parent::setPrimary($primary);
    }

    /**
     * {@inheritDoc}
     */
    public function isPrimary()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isPrimary', []);

        return parent::isPrimary();
    }

    /**
     * {@inheritDoc}
     */
    public function isEmpty()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isEmpty', []);

        return parent::isEmpty();
    }

    /**
     * {@inheritDoc}
     */
    public function getId()
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function setLabel($label)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLabel', [$label]);

        return parent::setLabel($label);
    }

    /**
     * {@inheritDoc}
     */
    public function getLabel()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLabel', []);

        return parent::getLabel();
    }

    /**
     * {@inheritDoc}
     */
    public function setStreet($street)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStreet', [$street]);

        return parent::setStreet($street);
    }

    /**
     * {@inheritDoc}
     */
    public function getStreet()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStreet', []);

        return parent::getStreet();
    }

    /**
     * {@inheritDoc}
     */
    public function setStreet2($street2)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStreet2', [$street2]);

        return parent::setStreet2($street2);
    }

    /**
     * {@inheritDoc}
     */
    public function getStreet2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStreet2', []);

        return parent::getStreet2();
    }

    /**
     * {@inheritDoc}
     */
    public function setCity($city)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCity', [$city]);

        return parent::setCity($city);
    }

    /**
     * {@inheritDoc}
     */
    public function getCity()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCity', []);

        return parent::getCity();
    }

    /**
     * {@inheritDoc}
     */
    public function setRegion($region)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRegion', [$region]);

        return parent::setRegion($region);
    }

    /**
     * {@inheritDoc}
     */
    public function getRegion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRegion', []);

        return parent::getRegion();
    }

    /**
     * {@inheritDoc}
     */
    public function setRegionText($regionText)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setRegionText', [$regionText]);

        return parent::setRegionText($regionText);
    }

    /**
     * {@inheritDoc}
     */
    public function getRegionText()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRegionText', []);

        return parent::getRegionText();
    }

    /**
     * {@inheritDoc}
     */
    public function getRegionName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRegionName', []);

        return parent::getRegionName();
    }

    /**
     * {@inheritDoc}
     */
    public function getRegionCode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getRegionCode', []);

        return parent::getRegionCode();
    }

    /**
     * {@inheritDoc}
     */
    public function getUniversalRegion()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getUniversalRegion', []);

        return parent::getUniversalRegion();
    }

    /**
     * {@inheritDoc}
     */
    public function setPostalCode($postalCode)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPostalCode', [$postalCode]);

        return parent::setPostalCode($postalCode);
    }

    /**
     * {@inheritDoc}
     */
    public function getPostalCode()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPostalCode', []);

        return parent::getPostalCode();
    }

    /**
     * {@inheritDoc}
     */
    public function setCountry($country)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCountry', [$country]);

        return parent::setCountry($country);
    }

    /**
     * {@inheritDoc}
     */
    public function getCountry()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountry', []);

        return parent::getCountry();
    }

    /**
     * {@inheritDoc}
     */
    public function getCountryName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountryName', []);

        return parent::getCountryName();
    }

    /**
     * {@inheritDoc}
     */
    public function getCountryIso3()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountryIso3', []);

        return parent::getCountryIso3();
    }

    /**
     * {@inheritDoc}
     */
    public function getCountryIso2()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCountryIso2', []);

        return parent::getCountryIso2();
    }

    /**
     * {@inheritDoc}
     */
    public function setOrganization($organization)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setOrganization', [$organization]);

        return parent::setOrganization($organization);
    }

    /**
     * {@inheritDoc}
     */
    public function getOrganization()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getOrganization', []);

        return parent::getOrganization();
    }

    /**
     * {@inheritDoc}
     */
    public function setNamePrefix($namePrefix)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNamePrefix', [$namePrefix]);

        return parent::setNamePrefix($namePrefix);
    }

    /**
     * {@inheritDoc}
     */
    public function getNamePrefix()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNamePrefix', []);

        return parent::getNamePrefix();
    }

    /**
     * {@inheritDoc}
     */
    public function setFirstName($firstName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setFirstName', [$firstName]);

        return parent::setFirstName($firstName);
    }

    /**
     * {@inheritDoc}
     */
    public function getFirstName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getFirstName', []);

        return parent::getFirstName();
    }

    /**
     * {@inheritDoc}
     */
    public function setMiddleName($middleName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setMiddleName', [$middleName]);

        return parent::setMiddleName($middleName);
    }

    /**
     * {@inheritDoc}
     */
    public function getMiddleName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getMiddleName', []);

        return parent::getMiddleName();
    }

    /**
     * {@inheritDoc}
     */
    public function setLastName($lastName)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setLastName', [$lastName]);

        return parent::setLastName($lastName);
    }

    /**
     * {@inheritDoc}
     */
    public function getLastName()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getLastName', []);

        return parent::getLastName();
    }

    /**
     * {@inheritDoc}
     */
    public function setNameSuffix($nameSuffix)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNameSuffix', [$nameSuffix]);

        return parent::setNameSuffix($nameSuffix);
    }

    /**
     * {@inheritDoc}
     */
    public function getNameSuffix()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNameSuffix', []);

        return parent::getNameSuffix();
    }

    /**
     * {@inheritDoc}
     */
    public function setCreated($created)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCreated', [$created]);

        return parent::setCreated($created);
    }

    /**
     * {@inheritDoc}
     */
    public function setUpdated($updated)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setUpdated', [$updated]);

        return parent::setUpdated($updated);
    }

    /**
     * {@inheritDoc}
     */
    public function beforeSave()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'beforeSave', []);

        return parent::beforeSave();
    }

    /**
     * {@inheritDoc}
     */
    public function beforeUpdate()
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'beforeUpdate', []);

        return parent::beforeUpdate();
    }

    /**
     * {@inheritDoc}
     */
    public function __toString(): string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__toString', []);

        return parent::__toString();
    }

    /**
     * {@inheritDoc}
     */
    public function isEqual($other)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'isEqual', [$other]);

        return parent::isEqual($other);
    }

    /**
     * {@inheritDoc}
     */
    public function getStorage(): \ArrayObject
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStorage', []);

        return parent::getStorage();
    }

    /**
     * {@inheritDoc}
     */
    public function get(string $name): mixed
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'get', [$name]);

        return parent::get($name);
    }

    /**
     * {@inheritDoc}
     */
    public function set(string $name, mixed $value): static
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'set', [$name, $value]);

        return parent::set($name, $value);
    }

    /**
     * {@inheritDoc}
     */
    public function __call(string $name, array $arguments)
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, '__call', [$name, $arguments]);

        return parent::__call($name, $arguments);
    }

    /**
     * {@inheritDoc}
     */
    public function cleanExtendEntityStorage(): void
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'cleanExtendEntityStorage', []);

        parent::cleanExtendEntityStorage();
    }

}
