<?php

namespace Extend\Entity;

/** Start: EV_Auth_Status */
class EV_Auth_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Auth_Status */

/** Start: EV_Dashboard_Type */
class EV_Dashboard_Type extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dashboard_Type */

/** Start: EV_Ce_Attendee_Status */
class EV_Ce_Attendee_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Ce_Attendee_Status */

/** Start: EV_Ce_Attendee_Type */
class EV_Ce_Attendee_Type extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Ce_Attendee_Type */

/** Start: EV_Task_Status */
class EV_Task_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Task_Status */

/** Start: EV_Ma_Type */
class EV_Ma_Type extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Ma_Type */

/** Start: EV_Lead_Source */
class EV_Lead_Source extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Lead_Source */

/** Start: EV_Opportunity_Status */
class EV_Opportunity_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Opportunity_Status */

/** Start: EV_Lead_Status */
class EV_Lead_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Lead_Status */

/** Start: EV_Dm_Cmp_Reply_Action */
class EV_Dm_Cmp_Reply_Action extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Cmp_Reply_Action */

/** Start: EV_Dm_Cmp_Status */
class EV_Dm_Cmp_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Cmp_Status */

/** Start: EV_Dm_Ab_Visibility */
class EV_Dm_Ab_Visibility extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Ab_Visibility */

/** Start: EV_Dm_Import_Status */
class EV_Dm_Import_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Import_Status */

/** Start: EV_Dm_Cnt_Opt_In_Type */
class EV_Dm_Cnt_Opt_In_Type extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Cnt_Opt_In_Type */

/** Start: EV_Dm_Cnt_Email_Type */
class EV_Dm_Cnt_Email_Type extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Cnt_Email_Type */

/** Start: EV_Dm_Cnt_Status */
class EV_Dm_Cnt_Status extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Cnt_Status */

/** Start: EV_Dm_Ab_Cnt_Exp_Type */
class EV_Dm_Ab_Cnt_Exp_Type extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Ab_Cnt_Exp_Type */

/** Start: EV_Dm_Df_Visibility */
class EV_Dm_Df_Visibility extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Df_Visibility */

/** Start: EV_Dm_Df_Type */
class EV_Dm_Df_Type extends \Oro\Bundle\EntityExtendBundle\Entity\AbstractEnumValue implements \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityInterface
{
    use \Oro\Bundle\EntityExtendBundle\Entity\ExtendEntityTrait;
}
/** End: EV_Dm_Df_Type */
