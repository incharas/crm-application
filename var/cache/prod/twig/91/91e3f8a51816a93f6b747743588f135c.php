<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/layouts/default/imports/datagrid_toolbar/layout.html.twig */
class __TwigTemplate_011e52d027d977603f3b6275ab5ada38 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '__datagrid_toolbar__datagrid_toolbar_actions_container_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_actions_container_widget'],
            '__datagrid_toolbar__datagrid_toolbar_mass_actions_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_mass_actions_widget'],
            '__datagrid_toolbar__datagrid_toolbar_extra_actions_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_extra_actions_widget'],
            '__datagrid_toolbar__datagrid_toolbar_tools_container_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_tools_container_widget'],
            '__datagrid_toolbar__datagrid_toolbar_actions_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_actions_widget'],
            '__datagrid_toolbar__datagrid_toolbar_page_size_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_page_size_widget'],
            '__datagrid_toolbar__datagrid_items_counter_widget' => [$this, 'block___datagrid_toolbar__datagrid_items_counter_widget'],
            '__datagrid_toolbar__datagrid_toolbar_pagination_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_pagination_widget'],
            '__datagrid_toolbar__datagrid_toolbar_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_widget'],
            '__datagrid_toolbar__datagrid_toolbar_sorting_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_sorting_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_actions_container_widget', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_mass_actions_widget', $context, $blocks);
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_extra_actions_widget', $context, $blocks);
        // line 22
        echo "
";
        // line 23
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_tools_container_widget', $context, $blocks);
        // line 29
        echo "
";
        // line 30
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_actions_widget', $context, $blocks);
        // line 37
        echo "
";
        // line 38
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_page_size_widget', $context, $blocks);
        // line 45
        echo "
";
        // line 46
        $this->displayBlock('__datagrid_toolbar__datagrid_items_counter_widget', $context, $blocks);
        // line 57
        echo "
";
        // line 58
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_pagination_widget', $context, $blocks);
        // line 65
        echo "
";
        // line 66
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_widget', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_sorting_widget', $context, $blocks);
    }

    // line 1
    public function block___datagrid_toolbar__datagrid_toolbar_actions_container_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["attr"] ?? null), "class", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 3), "")) : ("")) . " grid-toolbar-mass-actions")]);
        // line 5
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 8
    public function block___datagrid_toolbar__datagrid_toolbar_mass_actions_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 10
($context["attr"] ?? null), "class", [], "any", true, true, false, 10)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 10), "")) : ("")) . " mass-actions-panel btn-group icons-holder")]);
        // line 12
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 15
    public function block___datagrid_toolbar__datagrid_toolbar_extra_actions_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 17
($context["attr"] ?? null), "class", [], "any", true, true, false, 17)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 17), "")) : ("")) . " extra-actions-panel"), "data-grid-extra-actions-panel" => ""]);
        // line 20
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 23
    public function block___datagrid_toolbar__datagrid_toolbar_tools_container_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 25
($context["attr"] ?? null), "class", [], "any", true, true, false, 25)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 25), "")) : ("")) . " grid-toolbar-tools")]);
        // line 27
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 30
    public function block___datagrid_toolbar__datagrid_toolbar_actions_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 32
($context["attr"] ?? null), "class", [], "any", true, true, false, 32)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 32), "")) : ("")) . " actions-panel"), "data-grid-actions-panel" => ""]);
        // line 35
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 38
    public function block___datagrid_toolbar__datagrid_toolbar_page_size_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 40
($context["attr"] ?? null), "class", [], "any", true, true, false, 40)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 40), "")) : ("")) . " page-size"), "data-grid-pagesize" => ""]);
        // line 43
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 46
    public function block___datagrid_toolbar__datagrid_items_counter_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 48
($context["attr"] ?? null), "class", [], "any", true, true, false, 48)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 48), "")) : ("")) . " datagrid-tool oro-items-counter"), "data-grid-items-counter" => ""]);
        // line 51
        echo "    ";
        $context["toolbarPosition"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "data-grid-toolbar", [], "array", true, true, false, 51)) ? (_twig_default_filter((($__internal_compile_0 = ($context["attr"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["data-grid-toolbar"] ?? null) : null), "")) : (""));
        // line 52
        echo "
    ";
        // line 53
        if ((($context["toolbarPosition"] ?? null) != "bottom")) {
            // line 54
            echo "        <div";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo ">";
            echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
            echo "</div>
    ";
        }
    }

    // line 58
    public function block___datagrid_toolbar__datagrid_toolbar_pagination_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 60
($context["attr"] ?? null), "class", [], "any", true, true, false, 60)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 60), "")) : ("")) . " pagination pagination-centered"), "data-grid-pagination" => ""]);
        // line 63
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 66
    public function block___datagrid_toolbar__datagrid_toolbar_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 68
($context["attr"] ?? null), "class", [], "any", true, true, false, 68)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 68), "")) : ("")) . " grid-toolbar")]);
        // line 70
        echo "    <script type=\"text/html\" class=\"datagrid_templates\" data-identifier=\"template-datagrid-toolbar\">
        <div";
        // line 71
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
    </script>
";
    }

    // line 75
    public function block___datagrid_toolbar__datagrid_toolbar_sorting_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 76
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 77
($context["attr"] ?? null), "class", [], "any", true, true, false, 77)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 77), "")) : ("")) . " sorting form-horizontal"), "data-grid-sorting" => ""]);
        // line 80
        echo "
    ";
        // line 81
        $context["toolbarPosition"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "data-grid-toolbar", [], "array", true, true, false, 81)) ? (_twig_default_filter((($__internal_compile_1 = ($context["attr"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["data-grid-toolbar"] ?? null) : null), "")) : (""));
        // line 82
        echo "
    ";
        // line 83
        if ((($context["toolbarPosition"] ?? null) != "bottom")) {
            // line 84
            echo "        <div";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo "></div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/layouts/default/imports/datagrid_toolbar/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  279 => 84,  277 => 83,  274 => 82,  272 => 81,  269 => 80,  267 => 77,  265 => 76,  261 => 75,  252 => 71,  249 => 70,  247 => 68,  245 => 67,  241 => 66,  232 => 63,  230 => 60,  228 => 59,  224 => 58,  214 => 54,  212 => 53,  209 => 52,  206 => 51,  204 => 48,  202 => 47,  198 => 46,  189 => 43,  187 => 40,  185 => 39,  181 => 38,  172 => 35,  170 => 32,  168 => 31,  164 => 30,  155 => 27,  153 => 25,  151 => 24,  147 => 23,  138 => 20,  136 => 17,  134 => 16,  130 => 15,  121 => 12,  119 => 10,  117 => 9,  113 => 8,  104 => 5,  102 => 3,  100 => 2,  96 => 1,  92 => 75,  89 => 74,  87 => 66,  84 => 65,  82 => 58,  79 => 57,  77 => 46,  74 => 45,  72 => 38,  69 => 37,  67 => 30,  64 => 29,  62 => 23,  59 => 22,  57 => 15,  54 => 14,  52 => 8,  49 => 7,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/layouts/default/imports/datagrid_toolbar/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/layouts/default/imports/datagrid_toolbar/layout.html.twig");
    }
}
