<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/B2bCustomer/widget/info.html.twig */
class __TwigTemplate_c091eb2e6f437bceefdb9cc8cdeaf8fb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/B2bCustomer/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroSales/B2bCustomer/widget/info.html.twig", 2)->unwrap();
        // line 3
        $macros["address"] = $this->macros["address"] = $this->loadTemplate("@OroAddress/macros.html.twig", "@OroSales/B2bCustomer/widget/info.html.twig", 3)->unwrap();
        // line 4
        $macros["channel"] = $this->macros["channel"] = $this->loadTemplate("@OroChannel/macros.html.twig", "@OroSales/B2bCustomer/widget/info.html.twig", 4)->unwrap();
        // line 5
        $macros["sales"] = $this->macros["sales"] = $this->loadTemplate("@OroSales/macros.html.twig", "@OroSales/B2bCustomer/widget/info.html.twig", 5)->unwrap();
        // line 6
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 10
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 10)], 10, $context, $this->getSourceContext());
        echo "

            ";
        // line 12
        echo twig_call_macro($macros["channel"], "macro_renderChannelProperty", [($context["entity"] ?? null), "oro.sales.b2bcustomer.data_channel.label"], 12, $context, $this->getSourceContext());
        // line 14
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_account_view") && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "account", [], "any", false, false, false, 14))) {
            // line 15
            $context["accountView"] = (((("<a href=\"" . $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "account", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15)])) . "\">") . twig_call_macro($macros["UI"], "macro_renderEntityViewLabel", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "account", [], "any", false, false, false, 15), "name", "oro.account.entity_label"], 15, $context, $this->getSourceContext())) . "</a>");
        } else {
            // line 17
            $context["accountView"] = twig_call_macro($macros["UI"], "macro_renderEntityViewLabel", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "account", [], "any", false, false, false, 17), "name"], 17, $context, $this->getSourceContext());
        }
        // line 19
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.account.label"), ($context["accountView"] ?? null)], 19, $context, $this->getSourceContext());
        echo "
            ";
        // line 20
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.phones.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phones", [], "any", false, false, false, 20), "count", [], "any", false, false, false, 20)) ? (twig_call_macro($macros["sales"], "macro_renderCollectionWithPrimaryElement", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phones", [], "any", false, false, false, 20), false, ($context["entity"] ?? null)], 20, $context, $this->getSourceContext())) : (null))], 20, $context, $this->getSourceContext());
        echo "
            ";
        // line 21
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.emails.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emails", [], "any", false, false, false, 21), "count", [], "any", false, false, false, 21)) ? (twig_call_macro($macros["sales"], "macro_renderCollectionWithPrimaryElement", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emails", [], "any", false, false, false, 21), true, ($context["entity"] ?? null)], 21, $context, $this->getSourceContext())) : (null))], 21, $context, $this->getSourceContext());
        echo "
            ";
        // line 22
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.contact.label"), twig_call_macro($macros["UI"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 24
($context["entity"] ?? null), "contact", [], "any", false, false, false, 24), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "contact", [], "any", false, false, false, 24)) ? ($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "contact", [], "any", false, false, false, 24))) : ("")), "oro_contact_view"], 24, $context, $this->getSourceContext())], 22, $context, $this->getSourceContext());
        // line 25
        echo "

            ";
        // line 27
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "billingAddress", [], "any", false, false, false, 27))) {
            // line 28
            echo "                ";
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.billing_address.label"), twig_call_macro($macros["address"], "macro_renderAddress", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "billingAddress", [], "any", false, false, false, 28), true], 28, $context, $this->getSourceContext())], 28, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 30
        echo "
            ";
        // line 31
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "shippingAddress", [], "any", false, false, false, 31))) {
            // line 32
            echo "                ";
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.shipping_address.label"), twig_call_macro($macros["address"], "macro_renderAddress", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "shippingAddress", [], "any", false, false, false, 32), true], 32, $context, $this->getSourceContext())], 32, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 34
        echo "        </div>
        <div class=\"responsive-block\">
            ";
        // line 36
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 36, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/B2bCustomer/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 36,  106 => 34,  100 => 32,  98 => 31,  95 => 30,  89 => 28,  87 => 27,  83 => 25,  81 => 24,  80 => 22,  76 => 21,  72 => 20,  68 => 19,  65 => 17,  62 => 15,  60 => 14,  58 => 12,  53 => 10,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/B2bCustomer/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/B2bCustomer/widget/info.html.twig");
    }
}
