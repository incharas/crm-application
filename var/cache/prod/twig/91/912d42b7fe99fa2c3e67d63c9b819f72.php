<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/Tag/search.html.twig */
class __TwigTemplate_b0e92aa208b6d19318cbeb983fbe08b5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroTag/Tag/search.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroTag/Tag/search.html.twig", 2)->unwrap();
        // line 4
        $context["gridName"] = "tag-results-grid";

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%tag.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["tag"] ?? null), "name", [], "any", false, false, false, 5)]]);
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <div class=\"container-fluid search-header clearfix\">
        <h2 style=\"width: auto\">";
        // line 9
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.datagrid.search_result", ["%name%" => twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["tag"] ?? null), "name", [], "any", false, false, false, 9))]);
        echo "</h2>
    </div>

    ";
        // line 12
        if (((twig_in_filter("", twig_get_array_keys_filter(($context["groupedResults"] ?? null))) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = ($context["groupedResults"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[""] ?? null) : null), "count", [], "any", false, false, false, 12) > 0)) || (twig_length_filter($this->env, ($context["groupedResults"] ?? null)) > 1))) {
            // line 13
            echo "        ";
            $context["togglerId"] = uniqid("dropdown-");
            // line 14
            echo "        <div class=\"oro-page collapsible-sidebar clearfix\">
            <div class=\"oro-page-sidebar search-entity-types-column dropdown\">
                <a href=\"#\" role=\"button\" id=\"";
            // line 16
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"
                   aria-label=\"";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.result.entity_filter"), "html", null, true);
            echo "\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    <span class=\"fa-filter\" aria-hidden=\"true\"></span>
                    ";
            // line 19
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["selectedResult"] ?? null), "label", [], "any", true, true, false, 19) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["selectedResult"] ?? null), "count", [], "any", true, true, false, 19))) {
                // line 20
                echo "                        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["selectedResult"] ?? null), "label", [], "any", false, false, false, 20)), "html", null, true);
                echo "
                        ";
                // line 21
                $context["selectedResultCount"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["selectedResult"] ?? null), "count", [], "any", false, false, false, 21);
                // line 22
                echo "                    ";
            } else {
                // line 23
                echo "                        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.result.all"), "html", null, true);
                echo "
                        ";
                // line 24
                $context["selectedResultCount"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_1 = ($context["groupedResults"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[""] ?? null) : null), "count", [], "any", false, false, false, 24);
                // line 25
                echo "                    ";
            }
            // line 26
            echo "                    (";
            echo twig_escape_filter($this->env, ($context["selectedResultCount"] ?? null), "html", null, true);
            echo ") <span class=\"fa-sort-desc\" aria-hidden=\"true\"></span>
                </a>
                <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"";
            // line 28
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\">
                ";
            // line 29
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["groupedResults"] ?? null));
            foreach ($context['_seq'] as $context["alias"] => $context["type"]) {
                // line 30
                echo "                    ";
                $context["selected"] = ($context["alias"] == ($context["from"] ?? null));
                // line 31
                echo "                    ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "class", [], "any", true, true, false, 31)) {
                    // line 32
                    echo "                        ";
                    $context["label"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "label", [], "any", false, false, false, 32);
                    // line 33
                    echo "                        ";
                    $context["iconClass"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "icon", [], "any", false, false, false, 33);
                    // line 34
                    echo "                    ";
                } else {
                    // line 35
                    echo "                        ";
                    $context["label"] = "oro.search.result.all";
                    // line 36
                    echo "                        ";
                    $context["iconClass"] = "fa-search";
                    // line 37
                    echo "                    ";
                }
                // line 38
                echo "
                    ";
                // line 39
                if (twig_test_empty(($context["iconClass"] ?? null))) {
                    // line 40
                    echo "                        ";
                    $context["iconClass"] = "fa-file";
                    // line 41
                    echo "                    ";
                }
                // line 42
                echo "
                    <li";
                // line 43
                if (($context["selected"] ?? null)) {
                    echo " class=\"selected\"";
                }
                echo ">
                        <a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tag_search", ["from" => $context["alias"], "id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["tag"] ?? null), "id", [], "any", false, false, false, 44)]), "html", null, true);
                echo "\" ";
                if (($context["selected"] ?? null)) {
                    echo "aria-selected=\"true\"";
                }
                echo " title=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
                echo "\">
                            <span class=\"";
                // line 45
                echo twig_escape_filter($this->env, ($context["iconClass"] ?? null), "html", null, true);
                echo "\" aria-hidden=\"true\"></span>
                            <span class=\"search-label\">";
                // line 46
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
                echo "</span>
                            <span>&nbsp;(";
                // line 47
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "count", [], "any", false, false, false, 47), "html", null, true);
                echo ")</span>
                        </a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['alias'], $context['type'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "                </ul>
            </div>
            <div class=\"oro-page-body search-results-column\">
                <div id=\"tag-search-results-grid\">
                    ";
            // line 55
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ["from" => ($context["from"] ?? null), "tag_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["tag"] ?? null), "id", [], "any", false, false, false, 55)], ["cssClass" => "tag-search-grid"]], 55, $context, $this->getSourceContext());
            echo "
                </div>
            </div>
        </div>
    ";
        } else {
            // line 60
            echo "    <div class=\"no-data\">
        ";
            // line 61
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("No results were found.", [], "messages");
            // line 62
            echo "        <br />
        ";
            // line 63
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Try modifying your search criteria", [], "messages");
            // line 64
            echo "    </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroTag/Tag/search.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 64,  214 => 63,  211 => 62,  209 => 61,  206 => 60,  198 => 55,  192 => 51,  182 => 47,  178 => 46,  174 => 45,  164 => 44,  158 => 43,  155 => 42,  152 => 41,  149 => 40,  147 => 39,  144 => 38,  141 => 37,  138 => 36,  135 => 35,  132 => 34,  129 => 33,  126 => 32,  123 => 31,  120 => 30,  116 => 29,  112 => 28,  106 => 26,  103 => 25,  101 => 24,  96 => 23,  93 => 22,  91 => 21,  86 => 20,  84 => 19,  79 => 17,  75 => 16,  71 => 14,  68 => 13,  66 => 12,  60 => 9,  57 => 8,  53 => 7,  49 => 1,  47 => 5,  44 => 4,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/Tag/search.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/Tag/search.html.twig");
    }
}
