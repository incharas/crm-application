<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/_colors.scss */
class __TwigTemplate_68be56cd73272ef0febaf519ccd9f330 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

\$color-palette: (
    'primary': (
        'main': #2d333b,
        'base': #cc5c28,
        'light': #f2994b,
        'dark': #b84926
    ),
    'secondary': (
        'main': #fcb91d,
        'base': #6e98dc,
        'light': #7ea6a4,
        'lighten': #197b30,
        'dark': #f7941d
    ),
    'additional': (
        'ultra': #fff,
        'base': #f5f5f5,
        'light': #ebebeb,
        'middle': #d1d1d1,
        'dark': #464646,
        'darker': #000
    ),
    'ui': (
        'error': #fff1f1,
        'error-dark': #7a0026,
        'success': #e9f9e0,
        'success-dark': #5c7a58,
        'warning': #fefbe7,
        'warning-dark': #634c17,
        'normal': #4597ca,
        'focus': #348fc6
    )
) !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/_colors.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/_colors.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/_colors.scss");
    }
}
