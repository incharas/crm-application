<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/settings/_variables.scss */
class __TwigTemplate_183b08570104f08ce8f5893c399cbfde extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@use 'sass:color';

\$base-ui-element-offset: 11px 16px !default;
\$base-ui-element-cursor: null !default;

\$base-ui-element-font-size: \$base-font-size !default;
\$base-ui-element-font-family: \$base-font !default;
\$base-ui-element-line-height: 1.5 !default;

\$base-ui-element-border-size: 1px !default;
\$base-ui-element-border: \$base-ui-element-border-size solid get-color('additional', 'light') !default;
\$base-ui-element-border-radius: 5px !default;
\$base-ui-element-bg-color: get-color('additional', 'ultra') !default;
\$base-ui-element-color: get-color('additional', 'darker') !default;
\$base-ui-element-placeholder-color: get-color('additional', 'middle');
\$base-ui-element-height-size-m: 38px !default;

\$base-ui-element-arrow-color: get-color('additional', 'dark') !default;

\$base-ui-element-border-color-focus: transparent !default;
\$base-ui-element-focus-style: 0 0 0 2px get-color('ui', 'focus'),
                              0 0 6px color.adjust(get-color('ui', 'normal'), \$lightness: 1%) !default;
\$base-ui-bth-focus-style: 0 0 0 1px get-color('additional', 'ultra'),
                          0 0 0 3px get-color('ui', 'focus'),
                          0 0 6px color.adjust(get-color('ui', 'normal'), \$lightness: 1%) !default;
\$base-ui-popup-border-radius: \$base-ui-element-border-radius !default;

\$base-ui-popup-background: get-color('additional', 'ultra') !default;
\$base-ui-popup-box-shadow: 1px 2px 8px rgba(0 0 0 / 40%) !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/settings/_variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/settings/_variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/settings/_variables.scss");
    }
}
