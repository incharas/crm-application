<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/templates/multi-checkbox-view.html */
class __TwigTemplate_3192bf448a4e2d16b44ed80dcec45a75 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"multi-checkbox-control\">
    <select data-name=\"multi-checkbox-value-keeper\" multiple>
        <% _.each(options, function (option) { %>
        <option value=\"<%- option.value %>\" title=\"<%- option.text %>\"
        <% if (_.indexOf(values, option.value) !== -1) { %> selected=\"selected\"<% } %>>
        <%- option.text %>
        </option>
        <% }); %>
    </select>
    <% _.each(options, function (option) { %>
    <label class=\"multi-checkbox-control__item\"><input type=\"checkbox\" value=\"<%- option.value %>\" name=\"<%- name %>\"
        <% if (_.indexOf(values, option.value) !== -1) { %> checked=\"checked\"<% } %>>
        <span class=\"multi-checkbox-control_text\"><%- option.text %></span></label>
    <% }); %>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/templates/multi-checkbox-view.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/templates/multi-checkbox-view.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/templates/multi-checkbox-view.html");
    }
}
