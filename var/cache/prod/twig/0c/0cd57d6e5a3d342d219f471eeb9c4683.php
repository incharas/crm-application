<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/B2bCustomer/widget/b2bCustomerLeads.html.twig */
class __TwigTemplate_0add9042ef8f73b8fb81dd22069c77e0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroSales/B2bCustomer/widget/b2bCustomerLeads.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    ";
        // line 4
        $context["scope"] = ("customer-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 4));
        // line 5
        echo "    ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [$this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName("sales-b2bcustomer-leads-grid",         // line 6
($context["scope"] ?? null)), ["customerId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["entity"] ?? null), "id", [], "any", false, false, false, 7)], ["cssClass" => "inner-grid"]], 5, $context, $this->getSourceContext());
        // line 9
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/B2bCustomer/widget/b2bCustomerLeads.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 9,  48 => 7,  47 => 6,  45 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/B2bCustomer/widget/b2bCustomerLeads.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/B2bCustomer/widget/b2bCustomerLeads.html.twig");
    }
}
