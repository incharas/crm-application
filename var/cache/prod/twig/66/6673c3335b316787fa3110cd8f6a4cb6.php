<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/themes/oro/settings.yml */
class __TwigTemplate_d3932191a60de2a1a9268774a22cf692 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "label: Oro Theme
logo: bundles/oroui/themes/oro/images/oro-platform-logo.svg
icon: bundles/oroui/themes/oro/images/favicon.ico
rtl_support: true
styles:
    css:
        inputs:
            - bundles/oroui/themes/oro/css/style.css
        output: css/oro.css
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/themes/oro/settings.yml";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/themes/oro/settings.yml", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/themes/oro/settings.yml");
    }
}
