<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivity/Activity/activities.html.twig */
class __TwigTemplate_ab48afd178a6a0dbf5c60b82f0337cf0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroActivity/Activity/activities.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        ob_start(function () { return ''; });
        // line 4
        echo "    ";
        $this->loadTemplate("@OroUI/tab_panel.html.twig", "@OroActivity/Activity/activities.html.twig", 4)->display($context);
        $context["panelContent"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 6
        echo twig_call_macro($macros["UI"], "macro_scrollSubblock", [null, [0 => ($context["panelContent"] ?? null)]], 6, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroActivity/Activity/activities.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 6,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivity/Activity/activities.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActivityBundle/Resources/views/Activity/activities.html.twig");
    }
}
