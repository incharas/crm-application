<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Datagrid/Property/date.html.twig */
class __TwigTemplate_0758aa59a4069e9f76a1542dbc31ac66 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/Datagrid/Property/date.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["date"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "receivedAt"], "method", false, false, false, 3);
        // line 4
        $context["isNew"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "is_new"], "method", false, false, false, 4);
        // line 5
        echo "
<span class=\"nowrap\">";
        // line 7
        if (($context["isNew"] ?? null)) {
            echo "<strong>";
        }
        // line 8
        echo twig_call_macro($macros["EA"], "macro_date_smart_format", [($context["date"] ?? null)], 8, $context, $this->getSourceContext());
        // line 9
        if (($context["isNew"] ?? null)) {
            echo "</strong>";
        }
        // line 10
        echo "</span>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Datagrid/Property/date.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 10,  55 => 9,  53 => 8,  49 => 7,  46 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Datagrid/Property/date.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Datagrid/Property/date.html.twig");
    }
}
