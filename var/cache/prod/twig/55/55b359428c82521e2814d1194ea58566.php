<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDigitalAsset/DigitalAsset/update.html.twig */
class __TwigTemplate_4619251cec6bcad1f606e96fd11c9bc0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 3)) {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%title%" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocalizationExtension']->getLocalizedValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 4
($context["entity"] ?? null), "titles", [], "any", false, false, false, 4))]]);
        }
        // line 7
        $context["formAction"] = ((array_key_exists("formAction", $context)) ? (_twig_default_filter(($context["formAction"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["entity"] ?? null), "id", [], "any", false, false, false, 8)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_digital_asset_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 8)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_digital_asset_create"))))) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 8)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_digital_asset_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 8)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_digital_asset_create")))));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroDigitalAsset/DigitalAsset/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 12)) {
            // line 13
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 14
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_digital_asset_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.entity_plural_label"), "entityTitle" => _twig_default_filter($this->extensions['Oro\Bundle\LocaleBundle\Twig\LocalizationExtension']->getLocalizedValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["entity"] ?? null), "titles", [], "any", false, false, false, 17)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))];
            // line 19
            echo "
        ";
            // line 20
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 22
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.entity_label")]);
            // line 23
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroDigitalAsset/DigitalAsset/update.html.twig", 23)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 24
            echo "    ";
        }
    }

    // line 27
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDigitalAsset/DigitalAsset/update.html.twig", 28)->unwrap();
        // line 29
        echo "
    ";
        // line 30
        $this->displayParentBlock("navButtons", $context, $blocks);
        echo "

    ";
        // line 32
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_digital_asset_index")], 32, $context, $this->getSourceContext());
        echo "
    ";
        // line 33
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_digital_asset_index"]], 33, $context, $this->getSourceContext());
        // line 36
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_digital_asset_update")) {
            // line 37
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_digital_asset_update", "params" => ["id" => "\$id"]]], 37, $context, $this->getSourceContext()));
            // line 43
            echo "    ";
        }
        // line 44
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 44, $context, $this->getSourceContext());
        echo "
";
    }

    // line 47
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "    ";
        $context["id"] = "digital-asset-page-edit";
        // line 49
        echo "    ";
        $context["data"] = ["formErrors" =>         // line 50
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors'), "dataBlocks" => $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormDataBlocks($this->env, $context,         // line 51
($context["form"] ?? null)), "hiddenData" =>         // line 52
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest')];
        // line 54
        echo "
    ";
        // line 55
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroDigitalAsset/DigitalAsset/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 55,  139 => 54,  137 => 52,  136 => 51,  135 => 50,  133 => 49,  130 => 48,  126 => 47,  119 => 44,  116 => 43,  113 => 37,  110 => 36,  108 => 33,  104 => 32,  99 => 30,  96 => 29,  93 => 28,  89 => 27,  84 => 24,  81 => 23,  78 => 22,  73 => 20,  70 => 19,  68 => 17,  67 => 14,  65 => 13,  62 => 12,  58 => 11,  53 => 1,  51 => 8,  50 => 7,  47 => 4,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDigitalAsset/DigitalAsset/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DigitalAssetBundle/Resources/views/DigitalAsset/update.html.twig");
    }
}
