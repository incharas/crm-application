<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/lib/minicolors/jquery.minicolors.css */
class __TwigTemplate_eb9c3c537199cdef51a8cd4936698d13 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ".minicolors {
\tposition: relative;
}

.minicolors-sprite {
\tbackground-image: url('~bundles/oroui/lib/minicolors/jquery.minicolors.png');
}

.minicolors-no-data-uris .minicolors-sprite {
\tbackground-image: url('~bundles/oroui/lib/minicolors/jquery.minicolors.png');
}

.minicolors-swatch {
\tposition: absolute;
\tvertical-align: middle;
\tbackground-position: -80px 0;
\tborder: solid 1px #ccc;
\tcursor: text;
\tpadding: 0;
\tmargin: 0;
\tdisplay: inline-block;
}

.minicolors-swatch-color {
\tposition: absolute;
\ttop: 0;
\tleft: 0;
\tright: 0;
\tbottom: 0;
}

.minicolors input[type=hidden] + .minicolors-swatch {
\twidth: 28px;
\tposition: static;
\tcursor: pointer;
}

/* Panel */
.minicolors-panel {
\tposition: absolute;
\twidth: 173px;
\theight: 152px;
\tbackground: white;
\tborder: solid 1px #CCC;
\tbox-shadow: 0 0 20px rgba(0, 0, 0, .2);
\tz-index: 99999;
\t-moz-box-sizing: content-box;
\t-webkit-box-sizing: content-box;
\tbox-sizing: content-box;
\tdisplay: none;
}

.minicolors-panel.minicolors-visible {
\tdisplay: block;
}

/* Panel positioning */
.minicolors-position-top .minicolors-panel {
\ttop: -154px;
}

/*rtl:begin:ignore*/
.minicolors-position-right .minicolors-panel {
\tright: 0;
}

.minicolors-position-bottom .minicolors-panel {
\ttop: auto;
}

.minicolors-position-left .minicolors-panel {
\tleft: 0;
}
/*rtl:end:ignore*/
.minicolors-with-opacity .minicolors-panel {
\twidth: 194px;
}

.minicolors .minicolors-grid {
\tposition: absolute;
\ttop: 1px;
\tleft: 1px;
\twidth: 150px;
\theight: 150px;
\tbackground-position: -120px 0;
\tcursor: crosshair;
}

.minicolors .minicolors-grid-inner {
\tposition: absolute;
\ttop: 0;
\tleft: 0;
\twidth: 150px;
\theight: 150px;
}

.minicolors-slider-saturation .minicolors-grid {
\tbackground-position: -420px 0;
}

.minicolors-slider-saturation .minicolors-grid-inner {
\tbackground-position: -270px 0;
\tbackground-image: inherit;
}

.minicolors-slider-brightness .minicolors-grid {
\tbackground-position: -570px 0;
}

.minicolors-slider-brightness .minicolors-grid-inner {
\tbackground-color: black;
}

.minicolors-slider-wheel .minicolors-grid {
\tbackground-position: -720px 0;
}

.minicolors-slider,
.minicolors-opacity-slider {
\tposition: absolute;
\ttop: 1px;
\tleft: 152px;
\twidth: 20px;
\theight: 150px;
\tbackground-color: white;
\tbackground-position: 0 0;
\tcursor: row-resize;
}

.minicolors-slider-saturation .minicolors-slider {
\tbackground-position: -60px 0;
}

.minicolors-slider-brightness .minicolors-slider {
\tbackground-position: -20px 0;
}

.minicolors-slider-wheel .minicolors-slider {
\tbackground-position: -20px 0;
}

.minicolors-opacity-slider {
\tleft: 173px;
\tbackground-position: -40px 0;
\tdisplay: none;
}

.minicolors-with-opacity .minicolors-opacity-slider {
\tdisplay: block;
}

/* Pickers */
.minicolors-grid .minicolors-picker {
\tposition: absolute;
\ttop: 70px;
\tleft: 70px;
\twidth: 12px;
\theight: 12px;
\tborder: solid 1px black;
\tborder-radius: 10px;
\tmargin-top: -6px;
\tmargin-left: -6px;
\tbackground: none;
}

.minicolors-grid .minicolors-picker > div {
\tposition: absolute;
\ttop: 0;
\tleft: 0;
\twidth: 8px;
\theight: 8px;
\tborder-radius: 8px;
\tborder: solid 2px white;
\t-moz-box-sizing: content-box;
\t-webkit-box-sizing: content-box;
\tbox-sizing: content-box;
}

.minicolors-picker {
\tposition: absolute;
\ttop: 0;
\tleft: 0;
\twidth: 18px;
\theight: 2px;
\tbackground: white;
\tborder: solid 1px black;
\tmargin-top: -2px;
\t-moz-box-sizing: content-box;
\t-webkit-box-sizing: content-box;
\tbox-sizing: content-box;
}

/* Inline controls */
.minicolors-inline {
\tdisplay: inline-block;
}

.minicolors-inline .minicolors-input {
\tdisplay: none !important;
}

.minicolors-inline .minicolors-panel {
\tposition: relative;
\ttop: auto;
\tleft: auto;
\tbox-shadow: none;
\tz-index: auto;
\tdisplay: inline-block;
}

/* Default theme */
.minicolors-theme-default .minicolors-swatch {
\ttop: 5px;
\tleft: 5px;
\twidth: 18px;
\theight: 18px;
}
.minicolors-theme-default.minicolors-position-right .minicolors-swatch {
\tleft: auto;
\tright: 5px;
}
.minicolors-theme-default.minicolors {
\twidth: auto;
\tdisplay: inline-block;
}
.minicolors-theme-default .minicolors-input {
\theight: 20px;
\twidth: auto;
\tdisplay: inline-block;
\tpadding-left: 26px;
}
.minicolors-theme-default.minicolors-position-right .minicolors-input {
\tpadding-right: 26px;
\tpadding-left: inherit;
}

/* Bootstrap theme */
.minicolors-theme-bootstrap .minicolors-swatch {
\ttop: 3px;
\tleft: 3px;
\twidth: 28px;
\theight: 28px;
\tborder-radius: 3px;
}
.minicolors-theme-bootstrap .minicolors-swatch-color {
\tborder-radius: inherit;
}
.minicolors-theme-bootstrap.minicolors-position-right .minicolors-swatch {
\tleft: auto;
\tright: 3px;
}
.minicolors-theme-bootstrap .minicolors-input {
\tpadding-left: 44px;
}
.minicolors-theme-bootstrap.minicolors-position-right .minicolors-input {
\tpadding-right: 44px;
\tpadding-left: 12px;
}
.minicolors-theme-bootstrap .minicolors-input.input-lg + .minicolors-swatch {
\ttop: 4px;
\tleft: 4px;
\twidth: 37px;
\theight: 37px;
\tborder-radius: 5px;
}
.minicolors-theme-bootstrap .minicolors-input.input-sm + .minicolors-swatch {
\twidth: 24px;
\theight: 24px;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/lib/minicolors/jquery.minicolors.css";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/lib/minicolors/jquery.minicolors.css", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/lib/minicolors/jquery.minicolors.css");
    }
}
