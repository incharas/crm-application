<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/layout-helper.js */
class __TwigTemplate_c730558503633fb58fe9549973a159e7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const mediator = require('oroui/js/mediator');
    const layout = require('oroui/js/layout');

    const layoutHelper = {

        elementContext: \$('#container'),

        /**
         * @param {string} elementSelector
         * @param {jQuery} elementContext
         */
        setAvailableHeight: function(elementSelector, elementContext) {
            const \$element = \$(elementSelector, elementContext || this.elementContext);

            const calculateHeight = function() {
                const height = \$(window).height() - \$element.offset().top;
                \$element.css({
                    'height': height,
                    'min-height': height
                });
            };

            layout.onPageRendered(calculateHeight);
            \$(window).on('resize', _.debounce(calculateHeight, 50));
            mediator.on('page:afterChange', calculateHeight);
            mediator.on('layout:adjustHeight', calculateHeight);

            calculateHeight();
        }
    };

    return layoutHelper;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/layout-helper.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/layout-helper.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/layout-helper.js");
    }
}
