<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTranslation/macros.html.twig */
class __TwigTemplate_b5e64e5c5a6c4e462964fb2b0af1a649 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "
";
        // line 39
        echo "
";
    }

    // line 10
    public function macro_partialTranslations($__form__ = null, $__fieldsNames__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "fieldsNames" => $__fieldsNames__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 11
            echo "    <div class=\"oro_translations tabbable\">
        <ul class=\"oro_translationsLocales nav nav-tabs\">
        ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
                // line 14
                echo "            ";
                $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 14), "name", [], "any", false, false, false, 14);
                // line 15
                echo "
            <li class=\"nav-item";
                // line 16
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 16), "locale", [], "any", false, false, false, 16) == ($context["locale"] ?? null))) {
                    echo " active";
                }
                echo "\">
                <a href=\"#\" data-toggle=\"tab\" data-target=\".oro_translationsFields-";
                // line 17
                echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
                echo "\" class=\"nav-link\">
                   ";
                // line 18
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ($context["locale"] ?? null)), "html", null, true);
                echo "
                </a>
            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 22
            echo "        </ul>

        <div class=\"oro_translationsFields tab-content\">
        ";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
                // line 26
                echo "            ";
                $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 26), "name", [], "any", false, false, false, 26);
                // line 27
                echo "
            <div class=\"oro_translationsFields-";
                // line 28
                echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
                echo " tab-pane ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 28), "locale", [], "any", false, false, false, 28) == ($context["locale"] ?? null))) {
                    echo "active";
                }
                echo "\">
            ";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["translationsFields"]);
                foreach ($context['_seq'] as $context["_key"] => $context["translationsField"]) {
                    // line 30
                    echo "                ";
                    if (twig_in_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsField"], "vars", [], "any", false, false, false, 30), "name", [], "any", false, false, false, 30), ($context["fieldsNames"] ?? null))) {
                        // line 31
                        echo "                    ";
                        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["translationsField"], 'widget');
                        echo "
                ";
                    }
                    // line 33
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsField'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 34
                echo "            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 40
    public function macro_partialTranslationsGedmo($__form__ = null, $__fieldsNames__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "fieldsNames" => $__fieldsNames__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 41
            echo "    <div class=\"oro_translations tabbable\">
        <ul class=\"oro_translationsLocales nav nav-tabs\">
        ";
            // line 43
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["translationsLocales"]) {
                // line 44
                echo "            ";
                $context["isDefaultLocale"] = ("defaultLocale" == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsLocales"], "vars", [], "any", false, false, false, 44), "name", [], "any", false, false, false, 44));
                // line 45
                echo "
            ";
                // line 46
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["translationsLocales"]);
                foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
                    // line 47
                    echo "                ";
                    $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 47), "name", [], "any", false, false, false, 47);
                    // line 48
                    echo "
                <li class=\"nav-item";
                    // line 49
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 49), "locale", [], "any", false, false, false, 49) == ($context["locale"] ?? null))) {
                        echo " active";
                    }
                    echo "\">
                    <a href=\"#\" data-toggle=\"tab\" data-target=\".oro_translationsFields-";
                    // line 50
                    echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
                    echo "\" class=\"nav-link\">
                        ";
                    // line 51
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ($context["locale"] ?? null)), "html", null, true);
                    echo " ";
                    if (($context["isDefaultLocale"] ?? null)) {
                        echo "[Default]";
                    }
                    // line 52
                    echo "                    </a>
                </li>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 55
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsLocales'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 56
            echo "        </ul>

        <div class=\"oro_translationsFields tab-content\">
        ";
            // line 59
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["translationsLocales"]) {
                // line 60
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["translationsLocales"]);
                foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
                    // line 61
                    echo "                ";
                    $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 61), "name", [], "any", false, false, false, 61);
                    // line 62
                    echo "
                <div class=\"oro_translationsFields-";
                    // line 63
                    echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
                    echo " tab-pane ";
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 63), "locale", [], "any", false, false, false, 63) == ($context["locale"] ?? null))) {
                        echo "active";
                    }
                    echo "\">
                ";
                    // line 64
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["translationsFields"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["translationsField"]) {
                        // line 65
                        echo "                    ";
                        if (twig_in_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsField"], "vars", [], "any", false, false, false, 65), "name", [], "any", false, false, false, 65), ($context["fieldsNames"] ?? null))) {
                            // line 66
                            echo "                        ";
                            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["translationsField"], 'widget');
                            echo "
                    ";
                        }
                        // line 68
                        echo "                ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsField'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 69
                    echo "                </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 71
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsLocales'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 72
            echo "        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroTranslation/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 72,  275 => 71,  268 => 69,  262 => 68,  256 => 66,  253 => 65,  249 => 64,  241 => 63,  238 => 62,  235 => 61,  230 => 60,  226 => 59,  221 => 56,  215 => 55,  207 => 52,  201 => 51,  197 => 50,  191 => 49,  188 => 48,  185 => 47,  181 => 46,  178 => 45,  175 => 44,  171 => 43,  167 => 41,  153 => 40,  142 => 36,  135 => 34,  129 => 33,  123 => 31,  120 => 30,  116 => 29,  108 => 28,  105 => 27,  102 => 26,  98 => 25,  93 => 22,  83 => 18,  79 => 17,  73 => 16,  70 => 15,  67 => 14,  63 => 13,  59 => 11,  45 => 10,  40 => 39,  37 => 9,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTranslation/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/Resources/views/macros.html.twig");
    }
}
