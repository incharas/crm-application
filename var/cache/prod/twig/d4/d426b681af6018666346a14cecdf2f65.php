<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/createResult.html.twig */
class __TwigTemplate_f90f59df2ac99f7b0b01679ebd3f3a18 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/createResult.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%application.name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "N/A")) : ("N/A"))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroOAuth2Server/Client/createResult.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        ob_start(function () { return ''; });
        // line 8
        echo "        ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "frontend", [], "any", false, false, false, 8)) ? ("oro.oauth2server.menu.frontend_oauth_application.label") : ("oro.oauth2server.menu.backoffice_oauth_application.label"))), "html", null, true);
        // line 11
        echo "
    ";
        $context["entityLabel"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 13
        echo "
    ";
        // line 14
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null))) {
            // line 15
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 17)) ? ("oro_oauth2_frontend_view") : ("oro_oauth2_view")), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["entity"] ?? null), "id", [], "any", false, false, false, 18)]), "title" =>             // line 20
($context["entityLabel"] ?? null), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.view"), "iCss" => "fa-eye", "aCss" => "edit-button main-group"]], 15, $context, $this->getSourceContext());
            // line 24
            echo "
    ";
        }
        // line 26
        echo "
    ";
        // line 27
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 28
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 30
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 30)) ? ("oro_oauth2_frontend_update") : ("oro_oauth2_update")), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["entity"] ?? null), "id", [], "any", false, false, false, 31)]), "entity_label" =>             // line 33
($context["entityLabel"] ?? null)]], 28, $context, $this->getSourceContext());
            // line 34
            echo "
    ";
        }
    }

    // line 38
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 40
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 41
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 41)) ? ("oro_oauth2_frontend_index") : ("oro_oauth2_index"))), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 42
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 42)) ? ("oro.oauth2server.menu.frontend_oauth_application.label") : ("oro.oauth2server.menu.backoffice_oauth_application.label"))), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 46
($context["entity"] ?? null), "name", [], "any", true, true, false, 46)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 46), "N/A")) : ("N/A"))];
        // line 48
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 51
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 54
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 55
        echo "        <div class=\"widget-content\">
            <div class=\"row-fluid form-horizontal\">
                <div class=\"alert alert-info \">
                    ";
        // line 58
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.created_warning_message");
        echo "
                </div>
            </div>
            <div class=\"row-fluid form-horizontal\">
                <div class=\"responsive-block\">
                    <div>
                        <strong>";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.identifier.label"), "html", null, true);
        echo ":</strong>
                        ";
        // line 65
        $context["client_id"] = uniqid("client-id-");
        // line 66
        echo "                        <span class=\"text-nowrap\" id=\"";
        echo twig_escape_filter($this->env, ($context["client_id"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "identifier", [], "any", false, false, false, 66), "html", null, true);
        echo "</span>";
        // line 67
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "btn btn-icon", "iCss" => "fa-copy", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.button.copy_to_clipboard.label"), "labelInIcon" => false, "pageComponent" => ["view" => ["view" => "oroui/js/app/views/element-value-copy-to-clipboard-view", "elementSelector" => ("#" .         // line 75
($context["client_id"] ?? null))]]]], 67, $context, $this->getSourceContext());
        // line 79
        echo "</div>
                    <div>
                        <strong>";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.secret.label"), "html", null, true);
        echo ":</strong>
                        ";
        // line 82
        $context["client_secret_id"] = uniqid("client-secret-");
        // line 83
        echo "                        <span class=\"text-nowrap\" id=\"";
        echo twig_escape_filter($this->env, ($context["client_secret_id"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "plainSecret", [], "any", false, false, false, 83), "html", null, true);
        echo "</span>";
        // line 84
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "btn btn-icon", "iCss" => "fa-copy", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.button.copy_to_clipboard.label"), "labelInIcon" => false, "pageComponent" => ["view" => ["view" => "oroui/js/app/views/element-value-copy-to-clipboard-view", "elementSelector" => ("#" .         // line 92
($context["client_secret_id"] ?? null))]]]], 84, $context, $this->getSourceContext());
        // line 96
        echo "</div>
                </div>
            </div>
        </div>
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/createResult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 96,  175 => 92,  174 => 84,  168 => 83,  166 => 82,  162 => 81,  158 => 79,  156 => 75,  155 => 67,  149 => 66,  147 => 65,  143 => 64,  134 => 58,  129 => 55,  125 => 54,  119 => 51,  112 => 48,  110 => 46,  109 => 42,  108 => 41,  107 => 40,  105 => 39,  101 => 38,  95 => 34,  93 => 33,  92 => 31,  91 => 30,  89 => 28,  87 => 27,  84 => 26,  80 => 24,  78 => 20,  77 => 18,  76 => 17,  74 => 15,  72 => 14,  69 => 13,  65 => 11,  62 => 8,  59 => 7,  55 => 6,  50 => 1,  48 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/createResult.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/createResult.html.twig");
    }
}
