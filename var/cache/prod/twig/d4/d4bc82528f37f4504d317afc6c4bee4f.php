<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroConfig/macros.html.twig */
class __TwigTemplate_0bd27c5b88c8a6cc527f1976ed8edab2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "
";
        // line 67
        echo "
";
        // line 128
        echo "
";
        // line 203
        echo "
";
    }

    // line 4
    public function macro_renderTitleAndButtons($__pageTitle__ = null, $__buttons__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "pageTitle" => $__pageTitle__,
            "buttons" => $__buttons__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 5
            echo "    <div class=\"container-fluid page-title\">
        <div class=\"navigation navbar-extra navbar-extra-right\">
            <div class=\"row\">
                <div class=\"pull-left pull-left-extra\">
                    <div class=\"page-title__path\">
                        <div class=\"top-row\">
                            <div class=\"page-title__entity-title-wrapper\">
                                ";
            // line 12
            if (twig_test_iterable(($context["pageTitle"] ?? null))) {
                // line 13
                echo "                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["pageTitle"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
                    // line 14
                    echo "                                        ";
                    if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 14)) {
                        // line 15
                        echo "                                        <div class=\"sub-title\">
                                            ";
                        // line 16
                        echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                        echo "
                                        </div>
                                        <span class=\"separator\">/</span>
                                        ";
                    } else {
                        // line 20
                        echo "                                        <h1 class=\"page-title__entity-title\">";
                        echo $context["title"];
                        echo "</h1>
                                        ";
                    }
                    // line 22
                    echo "                                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "                                ";
            } else {
                // line 24
                echo "                                    <h1 class=\"page-title__entity-title\">
                                        ";
                // line 25
                echo twig_escape_filter($this->env, ($context["pageTitle"] ?? null), "html", null, true);
                echo "
                                    </h1>
                                ";
            }
            // line 28
            echo "                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"pull-right title-buttons-container\">
                    ";
            // line 33
            if (array_key_exists("buttons", $context)) {
                // line 34
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["buttons"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
                    // line 35
                    echo "                            ";
                    echo twig_escape_filter($this->env, $context["button"], "html", null, true);
                    echo "
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 37
                echo "                    ";
            }
            // line 38
            echo "                </div>
            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 54
    public function macro_renderScrollData($__configTree__ = null, $__form__ = null, $__activeTabName__ = false, $__activeSubTabName__ = false, $__routeName__ = "oro_config_configuration_system", $__routeParameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "configTree" => $__configTree__,
            "form" => $__form__,
            "activeTabName" => $__activeTabName__,
            "activeSubTabName" => $__activeSubTabName__,
            "routeName" => $__routeName__,
            "routeParameters" => $__routeParameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 55
            echo "    ";
            $macros["configMacros"] = $this;
            // line 56
            echo "
    ";
            // line 57
            echo twig_call_macro($macros["configMacros"], "macro_renderConfigurationScrollData", [["configTree" =>             // line 58
($context["configTree"] ?? null), "form" =>             // line 59
($context["form"] ?? null), "content" => [], "activeTabName" =>             // line 61
($context["activeTabName"] ?? null), "activeSubTabName" =>             // line 62
($context["activeSubTabName"] ?? null), "routeName" =>             // line 63
($context["routeName"] ?? null), "routeParameters" =>             // line 64
($context["routeParameters"] ?? null)]], 57, $context, $this->getSourceContext());
            // line 65
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 68
    public function macro_renderConfigurationScrollData($__data__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "data" => $__data__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 69
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroConfig/macros.html.twig", 69)->unwrap();
            // line 70
            echo "    ";
            $macros["configMacros"] = $this;
            // line 71
            echo "
    ";
            // line 72
            ob_start(function () { return ''; });
            // line 73
            echo "        <div class=\"layout-content\" data-page-component-view=\"";
            echo twig_escape_filter($this->env, json_encode(["view" => "oroui/js/app/views/highlight-text-view", "highlightSwitcherContainer" => "div.system-configuration-content-header", "highlightStateStorageKey" => "show-all-configuration-items-on-search", "highlightSelectors" => [0 => "div.system-configuration-content-title", 1 => "h5.user-fieldset span", 2 => "div.control-label label", 3 => "i.tooltip-icon", 4 => "div.controls > div.control-subgroup *[data-name=\"field__value\"]", 5 => ".select2-offscreen[data-name]"], "toggleSelectors" => ["div.control-group" => "div.control-group-wrapper", "fieldset.form-horizontal" => "div.system-configuration-content-inner"], "viewGroup" => "configuration"]), "html", null, true);
            // line 90
            echo "\">
            ";
            // line 91
            echo twig_call_macro($macros["configMacros"], "macro_renderTabContent", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "form", [], "any", false, false, false, 91), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "content", [], "any", false, false, false, 91)], 91, $context, $this->getSourceContext());
            echo "
        </div>
    ";
            $context["content"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 94
            echo "    <div class=\"system-configuration-container\">
        ";
            // line 95
            $this->loadTemplate("@OroConfig/macros.html.twig", "@OroConfig/macros.html.twig", 95, "418265485")->display(twig_array_merge($context, ["options" => ["scrollbar" => "[data-role=\"jstree-container\"]"]]));
            // line 126
            echo "    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 134
    public function macro_renderTabContent($__form__ = null, $__content__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "content" => $__content__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 135
            echo "    ";
            $macros["configMacros"] = $this;
            // line 136
            echo "
    ";
            // line 137
            $context["content"] = ((array_key_exists("content", $context)) ? (_twig_default_filter(($context["content"] ?? null), [])) : ([]));
            // line 138
            echo "    ";
            $context["processForm"] = false;
            // line 139
            echo "    ";
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "formErrors", [], "any", true, true, false, 139)) {
                // line 140
                echo "        ";
                $context["content"] = twig_array_merge(($context["content"] ?? null), ["formErrors" =>                 // line 141
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')]);
                // line 143
                echo "    ";
            }
            // line 144
            echo "    ";
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "dataBlocks", [], "any", true, true, false, 144)) {
                // line 145
                echo "        ";
                $context["content"] = twig_array_merge(($context["content"] ?? null), ["dataBlocks" => $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormDataBlocks($this->env, $context,                 // line 146
($context["form"] ?? null))]);
                // line 148
                echo "        ";
                $context["processForm"] = true;
                // line 149
                echo "    ";
            }
            // line 150
            echo "    ";
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "hiddenData", [], "any", true, true, false, 150)) {
                // line 151
                echo "        ";
                $context["content"] = twig_array_merge(($context["content"] ?? null), ["hiddenData" =>                 // line 152
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest')]);
                // line 154
                echo "    ";
            }
            // line 155
            echo "    ";
            if (($context["processForm"] ?? null)) {
                // line 156
                echo "        ";
                $context["content"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->processForm($this->env, ($context["content"] ?? null), ($context["form"] ?? null));
                // line 157
                echo "    ";
            }
            // line 158
            echo "
    <div class=\"placeholder\">
        <div class=\"scrollable-container\">
            <div class=\"system-configuration-content content form-container\" id=\"configuration-options-block\">
                <div class=\"pull-right\">
                    <input type=\"hidden\" name=\"input_action\" value=\"\" data-form-id=\"";
            // line 163
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 163), "id", [], "any", false, false, false, 163), "html", null, true);
            echo "\">
                </div>
                ";
            // line 165
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "formErrors", [], "any", true, true, false, 165) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "formErrors", [], "any", false, false, false, 165)))) {
                // line 166
                echo "                    <div class=\"customer-info-actions container-fluid well-small alert-wrap\" role=\"alert\">
                        <div class=\"alert alert-error alert-dismissible\">
                            <button class=\"close\" type=\"button\" data-dismiss=\"alert\" aria-label=\"";
                // line 168
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Close"), "html", null, true);
                echo "\" data-target=\".alert-wrap\"><span aria-hidden=\"true\">&times;</span></button>
                            ";
                // line 169
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "formErrors", [], "any", false, false, false, 169);
                echo "
                        </div>
                    </div>
                ";
            }
            // line 173
            echo "
                ";
            // line 174
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "dataBlocks", [], "any", false, false, false, 174));
            foreach ($context['_seq'] as $context["_key"] => $context["scrollBlock"]) {
                // line 175
                echo "                <div class=\"system-configuration-content-wrapper\">
                    <div class=\"system-configuration-content-header\">
                        <div class=\"system-configuration-content-title\">
                            ";
                // line 178
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "title", [], "any", false, false, false, 178)), "html", null, true);
                echo "
                        </div>
                    </div>
                    <div class=\"system-configuration-content-inner\">
                        ";
                // line 182
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "description", [], "any", true, true, false, 182) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "description", [], "any", false, false, false, 182) != ""))) {
                    // line 183
                    echo "                            <p>";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "description", [], "any", false, false, false, 183)), "html", null, true);
                    echo "</p>
                        ";
                }
                // line 185
                echo "
                        ";
                // line 186
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "subblocks", [], "any", false, false, false, 186));
                foreach ($context['_seq'] as $context["_key"] => $context["subblock"]) {
                    // line 187
                    echo "                            ";
                    if (twig_test_iterable($context["subblock"])) {
                        // line 188
                        echo "                                ";
                        echo twig_call_macro($macros["configMacros"], "macro_renderFieldset", [$context["subblock"]], 188, $context, $this->getSourceContext());
                        echo "
                            ";
                    } else {
                        // line 190
                        echo "                                ";
                        echo $context["subblock"];
                        echo "
                            ";
                    }
                    // line 192
                    echo "                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subblock'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 193
                echo "                    </div>
                </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['scrollBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 196
            echo "                <div class=\"hide\">
                    ";
            // line 197
            echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content"] ?? null), "hiddenData", [], "any", false, false, false, 197);
            echo "
                </div>
            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 212
    public function macro_renderFieldset($__block__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "block" => $__block__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 213
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroConfig/macros.html.twig", 213)->unwrap();
            // line 214
            echo "    ";
            if ((twig_length_filter($this->env, twig_array_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "data", [], "any", false, false, false, 214), function ($__v__) use ($context, $macros) { $context["v"] = $__v__; return (($context["v"] ?? null) != ""); })) > 0)) {
                // line 215
                echo "    <fieldset class=\"form-horizontal form-horizontal-large\">
        ";
                // line 216
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "title", [], "any", true, true, false, 216)) {
                    // line 217
                    echo "        <h5 class=\"user-fieldset\">
            <span>";
                    // line 218
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "title", [], "any", false, false, false, 218)), "html", null, true);
                    echo "</span>
            ";
                    // line 219
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "tooltip", [], "any", true, true, false, 219) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "tooltip", [], "any", false, false, false, 219) != ""))) {
                        // line 220
                        echo "                <label class=\"control-label header-tooltips\">";
                        echo twig_call_macro($macros["ui"], "macro_tooltip", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "tooltip", [], "any", false, false, false, 220), [], "right"], 220, $context, $this->getSourceContext());
                        echo "</label>
            ";
                    }
                    // line 222
                    echo "        </h5>
        ";
                }
                // line 224
                echo "
        ";
                // line 225
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "description", [], "any", true, true, false, 225) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "description", [], "any", false, false, false, 225) != ""))) {
                    // line 226
                    echo "            <div class=\"container-fluid\">
                ";
                    // line 227
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "descriptionStyle", [], "any", true, true, false, 227) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "descriptionStyle", [], "any", false, false, false, 227) != ""))) {
                        // line 228
                        echo "                    <p class=\"";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "descriptionStyle", [], "any", false, false, false, 228), "html", null, true);
                        echo "\">";
                        echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "description", [], "any", false, false, false, 228)));
                        echo "</p>
                ";
                    } else {
                        // line 230
                        echo "                    <p>";
                        echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "description", [], "any", false, false, false, 230)));
                        echo "</p>
                ";
                    }
                    // line 232
                    echo "            </div>
        ";
                }
                // line 234
                echo "
        <div class=\"control-group-wrapper\">
            ";
                // line 236
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "data", [], "any", false, false, false, 236));
                foreach ($context['_seq'] as $context["_key"] => $context["dataBlock"]) {
                    // line 237
                    echo "                ";
                    echo $context["dataBlock"];
                    echo "
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dataBlock'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 239
                echo "        </div>
    </fieldset>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroConfig/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  536 => 239,  527 => 237,  523 => 236,  519 => 234,  515 => 232,  509 => 230,  501 => 228,  499 => 227,  496 => 226,  494 => 225,  491 => 224,  487 => 222,  481 => 220,  479 => 219,  475 => 218,  472 => 217,  470 => 216,  467 => 215,  464 => 214,  461 => 213,  448 => 212,  433 => 197,  430 => 196,  422 => 193,  416 => 192,  410 => 190,  404 => 188,  401 => 187,  397 => 186,  394 => 185,  388 => 183,  386 => 182,  379 => 178,  374 => 175,  370 => 174,  367 => 173,  360 => 169,  356 => 168,  352 => 166,  350 => 165,  345 => 163,  338 => 158,  335 => 157,  332 => 156,  329 => 155,  326 => 154,  324 => 152,  322 => 151,  319 => 150,  316 => 149,  313 => 148,  311 => 146,  309 => 145,  306 => 144,  303 => 143,  301 => 141,  299 => 140,  296 => 139,  293 => 138,  291 => 137,  288 => 136,  285 => 135,  271 => 134,  261 => 126,  259 => 95,  256 => 94,  250 => 91,  247 => 90,  244 => 73,  242 => 72,  239 => 71,  236 => 70,  233 => 69,  220 => 68,  210 => 65,  208 => 64,  207 => 63,  206 => 62,  205 => 61,  204 => 59,  203 => 58,  202 => 57,  199 => 56,  196 => 55,  178 => 54,  165 => 38,  162 => 37,  153 => 35,  148 => 34,  146 => 33,  139 => 28,  133 => 25,  130 => 24,  127 => 23,  113 => 22,  107 => 20,  100 => 16,  97 => 15,  94 => 14,  76 => 13,  74 => 12,  65 => 5,  51 => 4,  46 => 203,  43 => 128,  40 => 67,  37 => 43,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroConfig/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Resources/views/macros.html.twig");
    }
}


/* @OroConfig/macros.html.twig */
class __TwigTemplate_0bd27c5b88c8a6cc527f1976ed8edab2___418265485 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'header' => [$this, 'block_header'],
            'sidebar' => [$this, 'block_sidebar'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 95
        return "@OroUI/content_sidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUI/content_sidebar.html.twig", "@OroConfig/macros.html.twig", 95);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 98
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 99
        echo "                <div id=\"system-configuration-jstree-inline-actions\"></div>
            ";
    }

    // line 101
    public function block_sidebar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 102
        echo "                ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroConfig/macros.html.twig", 102)->unwrap();
        // line 103
        echo "                ";
        echo twig_call_macro($macros["UI"], "macro_renderJsTree", [["label" => null, "treeOptions" => ["data" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 106
($context["data"] ?? null), "configTree", [], "any", false, false, false, 106), "viewGroup" => "configuration", "nodeId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 108
($context["data"] ?? null), "activeSubTabName", [], "any", false, false, false, 108), "onSelectRoute" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 109
($context["data"] ?? null), "routeName", [], "any", false, false, false, 109), "onSelectRouteParameters" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 110
($context["data"] ?? null), "routeParameters", [], "any", false, false, false, 110), "view" => "oroconfig/js/app/views/configuration-tree-view"], "actionsOptions" => ["inlineActionsCount" => 2, "inlineActionsElement" => "#system-configuration-jstree-inline-actions"]]], 103, $context, $this->getSourceContext());
        // line 117
        echo "
            ";
    }

    // line 120
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 121
        echo "                ";
        // line 122
        echo "                    ";
        echo twig_escape_filter($this->env, ($context["content"] ?? null), "html", null, true);
        echo "
                ";
        // line 124
        echo "            ";
    }

    public function getTemplateName()
    {
        return "@OroConfig/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  641 => 124,  636 => 122,  634 => 121,  630 => 120,  625 => 117,  623 => 110,  622 => 109,  621 => 108,  620 => 106,  618 => 103,  615 => 102,  611 => 101,  606 => 99,  602 => 98,  591 => 95,  536 => 239,  527 => 237,  523 => 236,  519 => 234,  515 => 232,  509 => 230,  501 => 228,  499 => 227,  496 => 226,  494 => 225,  491 => 224,  487 => 222,  481 => 220,  479 => 219,  475 => 218,  472 => 217,  470 => 216,  467 => 215,  464 => 214,  461 => 213,  448 => 212,  433 => 197,  430 => 196,  422 => 193,  416 => 192,  410 => 190,  404 => 188,  401 => 187,  397 => 186,  394 => 185,  388 => 183,  386 => 182,  379 => 178,  374 => 175,  370 => 174,  367 => 173,  360 => 169,  356 => 168,  352 => 166,  350 => 165,  345 => 163,  338 => 158,  335 => 157,  332 => 156,  329 => 155,  326 => 154,  324 => 152,  322 => 151,  319 => 150,  316 => 149,  313 => 148,  311 => 146,  309 => 145,  306 => 144,  303 => 143,  301 => 141,  299 => 140,  296 => 139,  293 => 138,  291 => 137,  288 => 136,  285 => 135,  271 => 134,  261 => 126,  259 => 95,  256 => 94,  250 => 91,  247 => 90,  244 => 73,  242 => 72,  239 => 71,  236 => 70,  233 => 69,  220 => 68,  210 => 65,  208 => 64,  207 => 63,  206 => 62,  205 => 61,  204 => 59,  203 => 58,  202 => 57,  199 => 56,  196 => 55,  178 => 54,  165 => 38,  162 => 37,  153 => 35,  148 => 34,  146 => 33,  139 => 28,  133 => 25,  130 => 24,  127 => 23,  113 => 22,  107 => 20,  100 => 16,  97 => 15,  94 => 14,  76 => 13,  74 => 12,  65 => 5,  51 => 4,  46 => 203,  43 => 128,  40 => 67,  37 => 43,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroConfig/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Resources/views/macros.html.twig");
    }
}
