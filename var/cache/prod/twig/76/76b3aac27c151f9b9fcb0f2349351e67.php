<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/variables/floating-validation-message-config.scss */
class __TwigTemplate_0d20c404130ee7dd1bad6c0dc3c9ea0e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

\$floating-validation-message-top: -10px !default;
\$floating-validation-message-bottom: auto !default;

\$floating-validation-message-after-top: 100% !default;
\$floating-validation-message-after-left: 10px !default;
\$floating-validation-message-after-color: get-color('ui', 'error') transparent !default;

\$floating-validation-message-label-top: -24px !default;
\$floating-validation-message-label-left: 0 !default;
\$floating-validation-message-label-text-align: left !default;
\$floating-validation-message-label-background-color: get-color('ui', 'error') !default;
\$floating-validation-message-label-color: get-color('ui', 'error-dark') !default;
\$floating-validation-message-label-inner-offset: 5px 8px 6px !default;
\$floating-validation-message-label-border-radius: 3px !default;

\$floating-validation-message-icon-display: none !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/variables/floating-validation-message-config.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/variables/floating-validation-message-config.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/variables/floating-validation-message-config.scss");
    }
}
