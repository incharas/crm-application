<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTask/TaskCrud/view.html.twig */
class __TwigTemplate_8f92842eaca2ea75c844217eca2c8bdf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'stats' => [$this, 'block_stats'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 2)->unwrap();
        // line 3
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 3)->unwrap();
        // line 4
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 4)->unwrap();
        // line 5
        $macros["U"] = $this->macros["U"] = $this->loadTemplate("@OroUser/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 5)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.subject%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["entity"] ?? null), "subject", [], "any", true, true, false, 7)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "subject", [], "any", false, false, false, 7), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroTask/TaskCrud/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 10)->unwrap();
        // line 11
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 11)->unwrap();
        // line 12
        echo "
    ";
        // line 13
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_navButtons", $context)) ? (_twig_default_filter(($context["view_navButtons"] ?? null), "view_navButtons")) : ("view_navButtons")), ["entity" => ($context["entity"] ?? null)]);
        // line 14
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 15
            echo "        ";
            // line 16
            echo "        ";
            echo twig_call_macro($macros["AC"], "macro_addContextButton", [($context["entity"] ?? null)], 16, $context, $this->getSourceContext());
            echo "
        ";
            // line 17
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_task_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["entity"] ?? null), "id", [], "any", false, false, false, 18)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.entity_label")]], 17, $context, $this->getSourceContext());
            // line 20
            echo "
    ";
        }
    }

    // line 24
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 25)->unwrap();
        // line 26
        echo "
    ";
        // line 28
        echo "    <li class=\"context-data activity-context-activity-block\">
        ";
        // line 29
        echo twig_call_macro($macros["AC"], "macro_activity_contexts", [($context["entity"] ?? null)], 29, $context, $this->getSourceContext());
        echo "
    </li>
";
    }

    // line 33
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 35
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_task_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 38
($context["entity"] ?? null), "subject", [], "any", false, false, false, 38)];
        // line 40
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 43
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 44)->unwrap();
        // line 45
        echo "
    ";
        // line 46
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <span class=\"page-title__status\">
        ";
        // line 48
        $context["status"] = ["open" => "enabled", "in_progress" => "tentatively", "closed" => "disabled"];
        // line 53
        echo "        ";
        if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 53))) {
            // line 54
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 54), "name", [], "any", false, false, false, 54), (((($__internal_compile_0 = ($context["status"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 54), "id", [], "any", false, false, false, 54)] ?? null) : null)) ? ((($__internal_compile_1 = ($context["status"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 54), "id", [], "any", false, false, false, 54)] ?? null) : null)) : ("disabled"))], 54, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 56
        echo "    </span>
";
    }

    // line 59
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/view.html.twig", 60)->unwrap();
        // line 62
        ob_start(function () { return ''; });
        // line 63
        echo "<div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 65
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.subject.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "subject", [], "any", false, false, false, 65)], 65, $context, $this->getSourceContext());
        echo "
                ";
        // line 66
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 66)], 66, $context, $this->getSourceContext());
        echo "
                ";
        // line 67
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.due_date.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dueDate", [], "any", false, false, false, 67))], 67, $context, $this->getSourceContext());
        echo "
                ";
        // line 68
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.task_priority.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "taskPriority", [], "any", false, false, false, 68)], 68, $context, $this->getSourceContext());
        // line 70
        ob_start(function () { return ''; });
        // line 71
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdBy", [], "any", false, false, false, 71)) {
            // line 72
            echo twig_call_macro($macros["U"], "macro_render_user_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdBy", [], "any", false, false, false, 72)], 72, $context, $this->getSourceContext());
        }
        $context["createdByData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 75
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.created_by.label"), ($context["createdByData"] ?? null)], 75, $context, $this->getSourceContext());
        echo "
            </div>
            <div class=\"responsive-block\">
                ";
        // line 78
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 78, $context, $this->getSourceContext());
        echo "
            </div>
        </div>";
        $context["taskInformation"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 83
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "subblocks" => [0 => ["data" => [0 =>         // line 87
($context["taskInformation"] ?? null)]]]]];
        // line 91
        echo "
    ";
        // line 92
        $context["id"] = "taskView";
        // line 93
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 94
        echo "
    ";
        // line 95
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTask/TaskCrud/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 95,  221 => 94,  218 => 93,  216 => 92,  213 => 91,  211 => 87,  210 => 83,  204 => 78,  198 => 75,  194 => 72,  192 => 71,  190 => 70,  188 => 68,  184 => 67,  180 => 66,  176 => 65,  172 => 63,  170 => 62,  167 => 60,  163 => 59,  158 => 56,  152 => 54,  149 => 53,  147 => 48,  142 => 46,  139 => 45,  136 => 44,  132 => 43,  125 => 40,  123 => 38,  122 => 35,  120 => 34,  116 => 33,  109 => 29,  106 => 28,  103 => 26,  100 => 25,  96 => 24,  90 => 20,  88 => 18,  87 => 17,  82 => 16,  80 => 15,  77 => 14,  75 => 13,  72 => 12,  69 => 11,  66 => 10,  62 => 9,  57 => 1,  55 => 7,  52 => 5,  50 => 4,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTask/TaskCrud/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-task-bundle/Resources/views/TaskCrud/view.html.twig");
    }
}
