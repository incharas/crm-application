<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Dashboard/recentEmailsGrid.html.twig */
class __TwigTemplate_c99f679a4ccffdc50c0847ab86a83e7f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroEmail/Dashboard/recentEmailsGrid.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ["userId" => ($context["loggedUserId"] ?? null)]], 3, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Dashboard/recentEmailsGrid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Dashboard/recentEmailsGrid.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Dashboard/recentEmailsGrid.html.twig");
    }
}
