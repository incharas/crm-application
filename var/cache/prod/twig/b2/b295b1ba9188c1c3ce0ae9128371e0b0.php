<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCampaign/EmailCampaign/Property/schedule.html.twig */
class __TwigTemplate_1f1c0bafd61327585dc419c0dad67b68 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("oro.campaign.emailcampaign.schedule." . ($context["value"] ?? null))), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCampaign/EmailCampaign/Property/schedule.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCampaign/EmailCampaign/Property/schedule.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/CampaignBundle/Resources/views/EmailCampaign/Property/schedule.html.twig");
    }
}
