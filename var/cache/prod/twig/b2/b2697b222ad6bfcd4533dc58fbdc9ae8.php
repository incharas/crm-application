<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/highlight-switcher.html */
class __TwigTemplate_dd861426ab0a11be7b7c2033dbb6fe5d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<a href=\"#\" class=\"highlight-items-switcher\" data-role=\"highlight-switcher\">
    <span class=\"highlight-items-switcher__control\">
        <%- _.__('oro.ui.highlight_switcher.show_all_items') %>
    </span>
    <span class=\"highlight-items-switcher__control\">
        <%- _.__('oro.ui.highlight_switcher.show_highlighted_items_only') %>
    </span>
</a>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/highlight-switcher.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/highlight-switcher.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/highlight-switcher.html");
    }
}
