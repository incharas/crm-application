<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmbeddedForm/layouts/embedded_default/form.html.twig */
class __TwigTemplate_af17e30cb622023b65d1c98e48a1a244 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'root_widget' => [$this, 'block_root_widget'],
            'body_widget' => [$this, 'block_body_widget'],
            'javascript' => [$this, 'block_javascript'],
            '_base_css_widget' => [$this, 'block__base_css_widget'],
            '_form_css_widget' => [$this, 'block__form_css_widget'],
            'embed_form_success_widget' => [$this, 'block_embed_form_success_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('root_widget', $context, $blocks);
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('body_widget', $context, $blocks);
        // line 35
        echo "
";
        // line 36
        $this->displayBlock('_base_css_widget', $context, $blocks);
        // line 44
        echo "
";
        // line 45
        $this->displayBlock('_form_css_widget', $context, $blocks);
        // line 49
        echo "
";
        // line 50
        $this->displayBlock('embed_form_success_widget', $context, $blocks);
    }

    // line 1
    public function block_root_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "<!DOCTYPE ";
        echo twig_escape_filter($this->env, ((array_key_exists("doctype", $context)) ? (_twig_default_filter(($context["doctype"] ?? null), "html")) : ("html")), "html", null, true);
        echo ">
<!--[if IE 7 ]>
<html class=\"no-js ie ie7\" lang=\"en\"> <![endif]-->
<!--[if IE 8 ]>
<html class=\"no-js ie ie8\" lang=\"en\"> <![endif]-->
<!--[if IE 9 ]>
<html class=\"no-js ie ie9\" lang=\"en\"> <![endif]-->
<!--[if (gte IE 10)|!(IE)]><!-->
<html class=\"no-js\" lang=\"en\"> <!--<![endif]-->
";
        // line 11
        $this->displayBlock("container_widget", $context, $blocks);
        echo "
</html>
";
    }

    // line 15
    public function block_body_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "<body";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
    <div id=\"page\">
        ";
        // line 18
        $this->displayBlock("container_widget", $context, $blocks);
        echo "
        ";
        // line 19
        $this->displayBlock('javascript', $context, $blocks);
        // line 32
        echo "    </div>
</body>
";
    }

    // line 19
    public function block_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "            <script>
                var form = document.querySelector('form');
                if (form) {
                    form.addEventListener('submit', function () {
                        var button = this.querySelector('button');
                        if (button) {
                            button.disabled = true;
                        }
                    });
                }
            </script>
        ";
    }

    // line 36
    public function block__base_css_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    ";
        $context["content"] = ('' === $tmp = "    body {
        min-width: 0;
    }
    ") ? '' : new Markup($tmp, $this->env->getCharset());
        // line 42
        echo "    ";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget', ["content" => ($context["content"] ?? null)]);
        echo "
";
    }

    // line 45
    public function block__form_css_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "    ";
        $context["content"] = twig_striptags(($context["content"] ?? null));
        // line 47
        echo "    ";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget', ["content" => ($context["content"] ?? null)]);
        echo "
";
    }

    // line 50
    public function block_embed_form_success_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->extensions['Oro\Bundle\EmbeddedFormBundle\Twig\BackLinkExtension']->backLinkFilter(((array_key_exists("message", $context)) ? (_twig_default_filter(($context["message"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.success_message.default"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.success_message.default"))), ($context["form_id"] ?? null)));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmbeddedForm/layouts/embedded_default/form.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  167 => 51,  163 => 50,  156 => 47,  153 => 46,  149 => 45,  142 => 42,  136 => 37,  132 => 36,  117 => 20,  113 => 19,  107 => 32,  105 => 19,  101 => 18,  95 => 16,  91 => 15,  84 => 11,  71 => 2,  67 => 1,  63 => 50,  60 => 49,  58 => 45,  55 => 44,  53 => 36,  50 => 35,  48 => 15,  45 => 14,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmbeddedForm/layouts/embedded_default/form.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/form.html.twig");
    }
}
