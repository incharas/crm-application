<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/scrolling-overlay.scss */
class __TwigTemplate_4ffeda112633e1f34e5cdf5c13058623 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.scrolling-overlay {
    position: relative;
    height: 100%;
    overflow: hidden;

    &-content {
        position: absolute;
        top: 0;
        bottom: 0;
        right: 0;
        left: 0;
        min-width: 100%;

        overflow-y: auto;

        -webkit-overflow-scrolling: touch;

        // Hide platform native scroll bar
        scrollbar-width: none;

        &::-webkit-scrollbar,
        &::-webkit-scrollbar-corner {
            width: 0;
            height: 0;
        }
    }

    &-btn {
        position: absolute;
        left: 0;
        z-index: 1;

        width: 100%;
        height: 17px;

        background-color: \$primary-500;

        opacity: .85;

        @extend %main-menu-trigger;

        &:hover,
        &:focus {
            background-color: \$primary-700;

            opacity: 1;
        }

        &--light {
            background-color: \$primary-750;

            &::before {
                color: \$primary-200;
            }

            &:hover,
            &:focus {
                background-color: \$primary-800;

                opacity: 1;
            }
        }

        &--up {
            top: 0;

            @include fa-icon(\$menu-icon-up);
        }

        &--down {
            bottom: 0;

            @include fa-icon(\$menu-icon-down);
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/scrolling-overlay.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/scrolling-overlay.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/scrolling-overlay.scss");
    }
}
