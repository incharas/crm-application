<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMigration/schema-template.php.twig */
class __TwigTemplate_4d730b99c47b70fbeca9191fa585b21d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["macros"] = $this->macros["macros"] = $this;
        // line 2
        echo "<?php

namespace ";
        // line 4
        echo twig_escape_filter($this->env, ($context["namespace"] ?? null), "html", null, true);
        echo "\\Migrations\\Schema;

use Doctrine\\DBAL\\Schema\\Schema;
use Oro\\Bundle\\MigrationBundle\\Migration\\Installation;
use Oro\\Bundle\\MigrationBundle\\Migration\\QueryBag;

/**
 * @SuppressWarnings(PHPMD.TooManyMethods)
 * @SuppressWarnings(PHPMD.ExcessiveClassLength)
 */
class ";
        // line 14
        echo twig_escape_filter($this->env, ($context["className"] ?? null), "html", null, true);
        echo " implements Installation
{
    /**
     * {@inheritdoc}
     */
    public function getMigrationVersion()
    {
        return '";
        // line 21
        echo twig_escape_filter($this->env, ($context["version"] ?? null), "html", null, true);
        echo "';
    }

    /**
     * {@inheritdoc}
     */
    public function up(Schema \$schema, QueryBag \$queries)
    {
        /** Tables generation **/
";
        // line 30
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["schema"] ?? null), "tables", [], "any", false, false, false, 30));
        foreach ($context['_seq'] as $context["_key"] => $context["table"]) {
            // line 31
            if ((twig_test_empty(($context["allowedTables"] ?? null)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["allowedTables"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 31), [], "array", true, true, false, 31))) {
                // line 32
                echo "        \$this->";
                echo twig_escape_filter($this->env, (("create" . twig_replace_filter(twig_title_string_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 32), ["_" => " "])), [" " => ""])) . "Table"), "html", null, true);
                echo "(\$schema);
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['table'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
        /** Foreign keys generation **/
";
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["schema"] ?? null), "tables", [], "any", false, false, false, 37));
        foreach ($context['_seq'] as $context["_key"] => $context["table"]) {
            // line 38
            if (((twig_test_empty(($context["allowedTables"] ?? null)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["allowedTables"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 38), [], "array", true, true, false, 38)) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "ForeignKeys", [], "any", false, false, false, 38)))) {
                // line 39
                echo "        \$this->";
                echo twig_escape_filter($this->env, (("add" . twig_replace_filter(twig_title_string_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 39), ["_" => " "])), [" " => ""])) . "ForeignKeys"), "html", null, true);
                echo "(\$schema);
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['table'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "    }
";
        // line 43
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["schema"] ?? null), "tables", [], "any", false, false, false, 43));
        foreach ($context['_seq'] as $context["_key"] => $context["table"]) {
            // line 44
            if ((twig_test_empty(($context["allowedTables"] ?? null)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["allowedTables"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 44), [], "array", true, true, false, 44))) {
                // line 45
                $context["methodName"] = (("create" . twig_replace_filter(twig_title_string_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 45), ["_" => " "])), [" " => ""])) . "Table");
                // line 46
                echo "
    /**
     * Create ";
                // line 48
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 48), "html", null, true);
                echo " table
     *
     * @param Schema \$schema
     */
    protected function ";
                // line 52
                echo twig_escape_filter($this->env, ($context["methodName"] ?? null), "html", null, true);
                echo "(Schema \$schema)
    {
        \$table = \$schema->createTable('";
                // line 54
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 54), "html", null, true);
                echo "');
";
                // line 55
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "columns", [], "any", false, false, false, 55));
                foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                    // line 56
                    $context["columnExtendedOptions"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["extendedOptions"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 56), [], "array", false, true, false, 56), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["column"], "name", [], "any", false, false, false, 56), [], "array", true, true, false, 56)) ? ((($__internal_compile_0 = (($__internal_compile_1 = ($context["extendedOptions"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 56)] ?? null) : null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["column"], "name", [], "any", false, false, false, 56)] ?? null) : null)) : (null));
                    // line 57
                    echo "        \$table->addColumn('";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["column"], "name", [], "any", false, false, false, 57), "html", null, true);
                    echo "', '";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["column"], "type", [], "any", false, false, false, 57), "name", [], "any", false, false, false, 57), "html", null, true);
                    echo "', ";
                    echo twig_call_macro($macros["macros"], "macro_dumpColumnOptions", [$context["column"], ($context["columnExtendedOptions"] ?? null)], 57, $context, $this->getSourceContext());
                    echo ");
";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "indexes", [], "any", false, false, false, 59));
                foreach ($context['_seq'] as $context["_key"] => $context["index"]) {
                    // line 60
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["index"], "isPrimary", [], "any", false, false, false, 60)) {
                        // line 61
                        echo "        \$table->setPrimaryKey(";
                        echo twig_call_macro($macros["macros"], "macro_dumpArray", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["index"], "columns", [], "any", false, false, false, 61)], 61, $context, $this->getSourceContext());
                        echo ");
";
                    } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 62
$context["index"], "isUnique", [], "any", false, false, false, 62)) {
                        // line 63
                        echo "        \$table->addUniqueIndex(";
                        echo twig_call_macro($macros["macros"], "macro_dumpArray", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["index"], "columns", [], "any", false, false, false, 63)], 63, $context, $this->getSourceContext());
                        echo ", '";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["index"], "name", [], "any", false, false, false, 63), "html", null, true);
                        echo "');
";
                    } else {
                        // line 65
                        echo "        \$table->addIndex(";
                        echo twig_call_macro($macros["macros"], "macro_dumpArray", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["index"], "columns", [], "any", false, false, false, 65)], 65, $context, $this->getSourceContext());
                        echo ", '";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["index"], "name", [], "any", false, false, false, 65), "html", null, true);
                        echo "', ";
                        echo twig_call_macro($macros["macros"], "macro_dumpArray", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["index"], "flags", [], "any", false, false, false, 65)], 65, $context, $this->getSourceContext());
                        echo ");
";
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['index'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 68
                echo "    }
";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['table'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["schema"] ?? null), "tables", [], "any", false, false, false, 71));
        foreach ($context['_seq'] as $context["_key"] => $context["table"]) {
            // line 72
            if ((twig_test_empty(($context["allowedTables"] ?? null)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["allowedTables"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 72), [], "array", true, true, false, 72))) {
                // line 73
                $context["methodName"] = (("add" . twig_replace_filter(twig_title_string_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 73), ["_" => " "])), [" " => ""])) . "ForeignKeys");
                // line 74
                if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "ForeignKeys", [], "any", false, false, false, 74))) {
                    // line 75
                    echo "
    /**
     * Add ";
                    // line 77
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 77), "html", null, true);
                    echo " foreign keys.
     *
     * @param Schema \$schema
     */
    protected function ";
                    // line 81
                    echo twig_escape_filter($this->env, ($context["methodName"] ?? null), "html", null, true);
                    echo "(Schema \$schema)
    {
        \$table = \$schema->getTable('";
                    // line 83
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "name", [], "any", false, false, false, 83), "html", null, true);
                    echo "');
";
                    // line 84
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["table"], "ForeignKeys", [], "any", false, false, false, 84));
                    foreach ($context['_seq'] as $context["_key"] => $context["foreignKey"]) {
                        // line 85
                        echo "        \$table->addForeignKeyConstraint(
            \$schema->getTable('";
                        // line 86
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["foreignKey"], "foreignTableName", [], "any", false, false, false, 86), "html", null, true);
                        echo "'),
            ";
                        // line 87
                        echo twig_call_macro($macros["macros"], "macro_dumpArray", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["foreignKey"], "localColumns", [], "any", false, false, false, 87)], 87, $context, $this->getSourceContext());
                        echo ",
            ";
                        // line 88
                        echo twig_call_macro($macros["macros"], "macro_dumpArray", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["foreignKey"], "foreignColumns", [], "any", false, false, false, 88)], 88, $context, $this->getSourceContext());
                        echo ",
            ";
                        // line 89
                        echo twig_call_macro($macros["macros"], "macro_dumpOptionsArray", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["foreignKey"], "options", [], "any", false, false, false, 89)], 89, $context, $this->getSourceContext());
                        echo "
        );
";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['foreignKey'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 92
                    echo "    }
";
                }
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['table'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "}";
        ob_start(function () { return ''; });
        // line 97
        echo "
";
        // line 116
        echo "
";
        // line 137
        echo "
";
        // line 143
        echo "
";
        // line 153
        echo "
";
        // line 163
        echo "
";
        $___internal_parse_68_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 96
        echo twig_spaceless($___internal_parse_68_);
    }

    // line 98
    public function macro_dumpColumnOptions($__column__ = null, $__columnExtendedOptions__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "column" => $__column__,
            "columnExtendedOptions" => $__columnExtendedOptions__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 99
            ob_start(function () { return ''; });
            // line 100
            $macros["macros"] = $this;
            // line 101
            $context["options"] = $this->extensions['Oro\Bundle\MigrationBundle\Twig\SchemaDumperExtension']->getColumnOptions(($context["column"] ?? null));
            // line 102
            $context["items"] = [];
            // line 103
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "default", [], "any", true, true, false, 103)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'default' => " . twig_call_macro($macros["macros"], "macro_dumpString", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "default", [], "any", false, false, false, 103)], 103, $context, $this->getSourceContext()))]);
            }
            // line 104
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "notnull", [], "any", true, true, false, 104)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'notnull' => " . twig_call_macro($macros["macros"], "macro_dumpBoolean", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "notnull", [], "any", false, false, false, 104)], 104, $context, $this->getSourceContext()))]);
            }
            // line 105
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "length", [], "any", true, true, false, 105)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'length' => " . twig_call_macro($macros["macros"], "macro_dumpInteger", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "length", [], "any", false, false, false, 105)], 105, $context, $this->getSourceContext()))]);
            }
            // line 106
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "precision", [], "any", true, true, false, 106)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'precision' => " . twig_call_macro($macros["macros"], "macro_dumpInteger", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "precision", [], "any", false, false, false, 106)], 106, $context, $this->getSourceContext()))]);
            }
            // line 107
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "scale", [], "any", true, true, false, 107)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'scale' => " . twig_call_macro($macros["macros"], "macro_dumpInteger", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "scale", [], "any", false, false, false, 107)], 107, $context, $this->getSourceContext()))]);
            }
            // line 108
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "fixed", [], "any", true, true, false, 108)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'fixed' => " . twig_call_macro($macros["macros"], "macro_dumpBoolean", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "fixed", [], "any", false, false, false, 108)], 108, $context, $this->getSourceContext()))]);
            }
            // line 109
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "unsigned", [], "any", true, true, false, 109)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'unsigned' => " . twig_call_macro($macros["macros"], "macro_dumpBoolean", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "unsigned", [], "any", false, false, false, 109)], 109, $context, $this->getSourceContext()))]);
            }
            // line 110
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "autoincrement", [], "any", true, true, false, 110)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'autoincrement' => " . twig_call_macro($macros["macros"], "macro_dumpBoolean", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "autoincrement", [], "any", false, false, false, 110)], 110, $context, $this->getSourceContext()))]);
            }
            // line 111
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "comment", [], "any", true, true, false, 111)) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'comment' => " . twig_call_macro($macros["macros"], "macro_dumpString", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "comment", [], "any", false, false, false, 111)], 111, $context, $this->getSourceContext()))]);
            }
            // line 112
            if ( !twig_test_empty(($context["columnExtendedOptions"] ?? null))) {
                $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ("'oro_options' => " . twig_call_macro($macros["macros"], "macro_dumpOptionsArray", [($context["columnExtendedOptions"] ?? null)], 112, $context, $this->getSourceContext()))]);
            }
            // line 113
            echo "[";
            echo twig_join_filter(($context["items"] ?? null), ", ");
            echo "]
";
            $___internal_parse_69_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 99
            echo twig_spaceless($___internal_parse_69_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 117
    public function macro_dumpOptionsArray($__arrayValues__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "arrayValues" => $__arrayValues__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 118
            ob_start(function () { return ''; });
            // line 119
            $macros["macros"] = $this;
            // line 120
            $context["items"] = [];
            // line 121
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["arrayValues"] ?? null));
            foreach ($context['_seq'] as $context["valueName"] => $context["value"]) {
                // line 122
                echo "    ";
                if ((null === $context["value"])) {
                    // line 123
                    echo "        ";
                    $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => (("'" . $context["valueName"]) . "' => null")]);
                    // line 124
                    echo "    ";
                } elseif (($context["value"] === true)) {
                    // line 125
                    echo "        ";
                    $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => (("'" . $context["valueName"]) . "' => true")]);
                    // line 126
                    echo "    ";
                } elseif (($context["value"] === false)) {
                    // line 127
                    echo "        ";
                    $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => (("'" . $context["valueName"]) . "' => false")]);
                    // line 128
                    echo "    ";
                } elseif (twig_test_iterable($context["value"])) {
                    // line 129
                    echo "        ";
                    $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => ((("'" . $context["valueName"]) . "' => ") . twig_call_macro($macros["macros"], "macro_dumpOptionsArray", [$context["value"]], 129, $context, $this->getSourceContext()))]);
                    // line 130
                    echo "    ";
                } else {
                    // line 131
                    echo "        ";
                    $context["items"] = twig_array_merge(($context["items"] ?? null), [0 => (((("'" . $context["valueName"]) . "' => '") . $context["value"]) . "'")]);
                    // line 132
                    echo "    ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['valueName'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 134
            echo "[";
            echo twig_join_filter(($context["items"] ?? null), ", ");
            echo "]
";
            $___internal_parse_70_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 118
            echo twig_spaceless($___internal_parse_70_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 138
    public function macro_dumpArray($__arrayValues__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "arrayValues" => $__arrayValues__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 139
            ob_start(function () { return ''; });
            // line 140
            echo "[";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["arrayValues"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 140)) {
                    echo ", ";
                }
                echo "'";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "'";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo "]
";
            $___internal_parse_71_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 139
            echo twig_spaceless($___internal_parse_71_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 144
    public function macro_dumpBoolean($__value__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 145
            ob_start(function () { return ''; });
            // line 146
            echo "    ";
            if (($context["value"] ?? null)) {
                // line 147
                echo "        true
    ";
            } else {
                // line 149
                echo "        false
    ";
            }
            $___internal_parse_72_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 145
            echo twig_spaceless($___internal_parse_72_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 154
    public function macro_dumpString($__value__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 155
            ob_start(function () { return ''; });
            // line 156
            echo "    ";
            if ( !(null === ($context["value"] ?? null))) {
                // line 157
                echo "        '";
                echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
                echo "'
    ";
            } else {
                // line 159
                echo "        null
    ";
            }
            $___internal_parse_73_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 155
            echo twig_spaceless($___internal_parse_73_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 164
    public function macro_dumpInteger($__value__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 165
            ob_start(function () { return ''; });
            // line 166
            echo "    ";
            if ( !(null === ($context["value"] ?? null))) {
                // line 167
                echo "        ";
                echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
                echo "
    ";
            } else {
                // line 169
                echo "        null
    ";
            }
            $___internal_parse_74_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 165
            echo twig_spaceless($___internal_parse_74_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroMigration/schema-template.php.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  618 => 165,  613 => 169,  607 => 167,  604 => 166,  602 => 165,  589 => 164,  580 => 155,  575 => 159,  569 => 157,  566 => 156,  564 => 155,  551 => 154,  542 => 145,  537 => 149,  533 => 147,  530 => 146,  528 => 145,  515 => 144,  506 => 139,  467 => 140,  465 => 139,  452 => 138,  443 => 118,  437 => 134,  430 => 132,  427 => 131,  424 => 130,  421 => 129,  418 => 128,  415 => 127,  412 => 126,  409 => 125,  406 => 124,  403 => 123,  400 => 122,  396 => 121,  394 => 120,  392 => 119,  390 => 118,  377 => 117,  368 => 99,  362 => 113,  358 => 112,  354 => 111,  350 => 110,  346 => 109,  342 => 108,  338 => 107,  334 => 106,  330 => 105,  326 => 104,  322 => 103,  320 => 102,  318 => 101,  316 => 100,  314 => 99,  300 => 98,  296 => 96,  292 => 163,  289 => 153,  286 => 143,  283 => 137,  280 => 116,  277 => 97,  274 => 96,  265 => 92,  256 => 89,  252 => 88,  248 => 87,  244 => 86,  241 => 85,  237 => 84,  233 => 83,  228 => 81,  221 => 77,  217 => 75,  215 => 74,  213 => 73,  211 => 72,  207 => 71,  199 => 68,  185 => 65,  177 => 63,  175 => 62,  170 => 61,  168 => 60,  164 => 59,  151 => 57,  149 => 56,  145 => 55,  141 => 54,  136 => 52,  129 => 48,  125 => 46,  123 => 45,  121 => 44,  117 => 43,  114 => 42,  104 => 39,  102 => 38,  98 => 37,  94 => 35,  84 => 32,  82 => 31,  78 => 30,  66 => 21,  56 => 14,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMigration/schema-template.php.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MigrationBundle/Resources/views/schema-template.php.twig");
    }
}
