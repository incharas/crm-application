<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroForm/Autocomplete/fullName/result.html.twig */
class __TwigTemplate_f5701de84335e68f35d7b0af2dbdb8d0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<%= highlight(_.escape(fullName)) %><% if (id === null) { %>
    <span class=\"select2__result-entry-info\">
        (<%- _.__('oro.form.add_new') %>)
    </span>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroForm/Autocomplete/fullName/result.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroForm/Autocomplete/fullName/result.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/views/Autocomplete/fullName/result.html.twig");
    }
}
