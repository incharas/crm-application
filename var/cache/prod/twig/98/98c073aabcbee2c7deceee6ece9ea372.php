<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Autocomplete/customer/selection.html.twig */
class __TwigTemplate_a48e2e3cbfe892182dc567989bb8cdee extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (!_.isEmpty(obj.icon)) { %>
    <% if (icon.type === 'file-path') { %>
        <img src=\"<%- icon.data.path %>\" class=\"separated-img\">
    <% } else if (icon.type === 'icon') { %>
        <i class=\"<%- icon.data.class %> hide-text separated-img\"></i>
    <% } %>
<% } %>
<%- text %>
<% if (id === null) { %>
    <span class=\"select2__result-entry-info\">
        (<%- _.__('oro.sales.form.new_customer') %>)
    </span>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Autocomplete/customer/selection.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Autocomplete/customer/selection.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Autocomplete/customer/selection.html.twig");
    }
}
