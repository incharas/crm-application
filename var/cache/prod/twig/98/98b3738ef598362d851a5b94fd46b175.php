<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityMerge/Merge/merge.html.twig */
class __TwigTemplate_fffb4a0acb2b5cfa46021ee3bcb8f55a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityMerge/Merge/merge.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["{{ label }}" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 4
($context["entityLabel"] ?? null))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEntityMerge/Merge/merge.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityMerge/Merge/merge.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->addUrlQuery(($context["cancelPath"] ?? null))], 9, $context, $this->getSourceContext());
        echo "
    ";
        // line 10
        echo twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_merge.form.merge_button")]], 10, $context, $this->getSourceContext());
        // line 12
        echo "
";
    }

    // line 15
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    ";
        $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroEntityMerge/Merge/merge.html.twig", 16)->display($context);
    }

    // line 19
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "    ";
        $context["id"] = "entity-merge";
        // line 21
        echo "
    ";
        // line 22
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_merge.form.merge_values"), "subblocks" => [0 => ["title" => null, "useSpan" => false, "data" => [0 =>         // line 29
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ["class" => "select2"]])]]]]];
        // line 33
        echo "
    ";
        // line 34
        $context["data"] = ["formErrors" => ((        // line 35
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 36
($context["dataBlocks"] ?? null)];
        // line 38
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityMerge/Merge/merge.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  104 => 38,  102 => 36,  101 => 35,  100 => 34,  97 => 33,  95 => 29,  94 => 22,  91 => 21,  88 => 20,  84 => 19,  79 => 16,  75 => 15,  70 => 12,  68 => 10,  64 => 9,  61 => 8,  58 => 7,  54 => 6,  49 => 1,  47 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityMerge/Merge/merge.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityMergeBundle/Resources/views/Merge/merge.html.twig");
    }
}
