<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/template-macros-module.js */
class __TwigTemplate_a820379d4a8ad7982b218b358dc1d8a0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import {macros} from 'underscore';

macros('oroui', {
    /**
     * Renders tel link for a phone
     *
     * @param {Object} data
     * @param {Object|string} data.phone
     * @param {string?} data.title optional
     */
    renderPhone: require('tpl-loader!oroui/templates/macros/phone.html'),

    /**
     * Renders link
     *
     * @param {Object} data
     * @param {Object|string} data.href
     * @param {Object|string} data.text
     * @param {Object|string} data.target
     * @param {string?} data.id optional
     * @param {string?} data.class optional
     * @param {string?} data.title optional
     */
    renderLink: require('tpl-loader!oroui/templates/macros/link.html'),

    /**
     * Renders content depended on direction
     *
     * Entered content will be wrapped by html element (SPAN) with applied attributes
     * @example
     * // returns <span dir=\"rtl\">4נינג'ות</dir>
     * renderDirection({content: '4נינג'ות', dir: 'rtl'})
     * @example
     * // returns <span dir=\"ltr\">/text</dir>
     * renderDirection({content: '/text'})
     * @param {Object} data
     * @param {Object|string} data.content
     * @param {Object|string} data.class optional
     * @param {Object|string} data.dir optional
     */
    renderDirection: require('tpl-loader!oroui/templates/macros/direction.html')
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/template-macros-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/template-macros-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/template-macros-module.js");
    }
}
