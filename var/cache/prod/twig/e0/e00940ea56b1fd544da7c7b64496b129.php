<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/form-validate-view.js */
class __TwigTemplate_e1282e01f88244006740b9b3260d4f9c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const BaseView = require('oroui/js/app/views/base/view');
    require('jquery.validate');

    const FormValidateView = BaseView.extend({
        keepElement: true,

        autoRender: true,

        validationOptions: null,

        events: {
            doReset: 'onReset'
        },

        /**
         * @inheritdoc
         */
        constructor: function FormValidateView(options) {
            FormValidateView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            _.extend(this, _.pick(options, 'validationOptions'));
            FormValidateView.__super__.initialize.call(this, options);
        },

        render: function() {
            if (this.\$el.data('validator')) {
                // form already has initialized validator
                return this;
            }

            this._deferredRender();
            this.validator = this.\$el.validate({
                ...(this.validationOptions || {}),
                onMethodsLoaded: () => this._resolveDeferredRender()
            });

            return this;
        },

        onReset: function() {
            if (this.validator) {
                this.validator.resetForm();
            }
        },

        dispose: function() {
            if (this.disposed) {
                return;
            }

            delete this.validationOptions;
            if (this.validator) {
                this.validator.destroy();
                delete this.validator;
            }
            FormValidateView.__super__.dispose.call(this);
        }
    });

    return FormValidateView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/form-validate-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/form-validate-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/form-validate-view.js");
    }
}
