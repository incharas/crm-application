<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Config/widget/info.html.twig */
class __TwigTemplate_9d3fbe680377db888cd820a078799ed1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/widget/info.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 6
        $context["entityIcon"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "icon"], "method", false, false, false, 6);
        // line 7
        echo "            ";
        $context["iconContent"] = null;
        // line 8
        echo "            ";
        if (($context["entityIcon"] ?? null)) {
            // line 9
            echo "                ";
            ob_start(function () { return ''; });
            // line 10
            echo "                    <i class=\"";
            echo twig_escape_filter($this->env, ($context["entityIcon"] ?? null), "html", null, true);
            echo " hide-text\"></i> (";
            echo twig_escape_filter($this->env, ($context["entityIcon"] ?? null), "html", null, true);
            echo ")
                ";
            $context["iconContent"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 12
            echo "            ";
        }
        // line 13
        echo "
            ";
        // line 14
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Name"), ($context["entity_name"] ?? null)], 14, $context, $this->getSourceContext());
        echo "
            ";
        // line 15
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Icon"), ($context["iconContent"] ?? null)], 15, $context, $this->getSourceContext());
        echo "
            ";
        // line 16
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Label"), (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 18
($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 18) == $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 18)))) ? ("") : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 20
($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 20))))], 16, $context, $this->getSourceContext());
        // line 21
        echo "
            ";
        // line 22
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Plural Label"), (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 24
($context["entity_config"] ?? null), "get", [0 => "plural_label"], "method", false, false, false, 24) == $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "plural_label"], "method", false, false, false, 24)))) ? ("") : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 26
($context["entity_config"] ?? null), "get", [0 => "plural_label"], "method", false, false, false, 26))))], 22, $context, $this->getSourceContext());
        // line 27
        echo "
            ";
        // line 28
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Type"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_extend"] ?? null), "get", [0 => "owner"], "method", false, false, false, 28)], 28, $context, $this->getSourceContext());
        echo "
            ";
        // line 29
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Description"), (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 31
($context["entity_config"] ?? null), "get", [0 => "description"], "method", false, false, false, 31) == $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "description"], "method", false, false, false, 31)))) ? ("") : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 33
($context["entity_config"] ?? null), "get", [0 => "description"], "method", false, false, false, 33))))], 29, $context, $this->getSourceContext());
        // line 34
        echo "
            ";
        // line 35
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ownership Type"), ($context["entity_owner_type"] ?? null)], 35, $context, $this->getSourceContext());
        echo "
            ";
        // line 36
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Module"), ($context["module_name"] ?? null)], 36, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Config/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  107 => 36,  103 => 35,  100 => 34,  98 => 33,  97 => 31,  96 => 29,  92 => 28,  89 => 27,  87 => 26,  86 => 24,  85 => 22,  82 => 21,  80 => 20,  79 => 18,  78 => 16,  74 => 15,  70 => 14,  67 => 13,  64 => 12,  56 => 10,  53 => 9,  50 => 8,  47 => 7,  45 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Config/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Config/widget/info.html.twig");
    }
}
