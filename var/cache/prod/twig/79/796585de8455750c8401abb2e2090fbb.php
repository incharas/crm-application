<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/backdrop-manager.js */
class __TwigTemplate_8c9192d9f20d8c172d5488cf683bf519 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const MultiUseResourceManager = require('./multi-use-resource-manager');
    const backdropManager = new MultiUseResourceManager({
        listen: {
            constructResource: function() {
                \$(document.body).addClass('backdrop');
            },
            disposeResource: function() {
                \$(document.body).removeClass('backdrop');
            }
        }
    });

    return backdropManager;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/backdrop-manager.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/backdrop-manager.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/backdrop-manager.js");
    }
}
