<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/menuUpdate/view.html.twig */
class __TwigTemplate_234fb654c710de37fa6fe9d41484f5bb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroNavigation/menuUpdate/view.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $macros["navigationMacro"] = $this->macros["navigationMacro"] = $this->loadTemplate("@OroSync/Include/contentTags.html.twig", "@OroNavigation/menuUpdate/view.html.twig", 3)->unwrap();
        // line 5
        ob_start(function () { return ''; });
        // line 6
        echo "<a href=\"#\" onclick=\"window.location.reload(false);return false;\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.reload_link.label"), "html", null, true);
        // line 8
        echo "</a>";
        $context["reloadLink"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/view.html.twig", 11)->unwrap();
        // line 12
        echo "    ";
        $macros["menuUpdateView"] = $this;
        // line 13
        echo "
    <div class=\"layout-content\"";
        // line 14
        if (array_key_exists("pageComponent", $context)) {
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["pageComponent"] ?? null)], 14, $context, $this->getSourceContext());
        }
        echo ">
        <div class=\"container-fluid page-title\">
            <div class=\"navigation navbar-extra navbar-extra-right\">
                <div class=\"row\">
                    <div class=\"pull-left-extra\">
                        ";
        // line 19
        $this->displayBlock('pageHeader', $context, $blocks);
        // line 22
        echo "                    </div>
                    <div class=\"pull-right\">
                        ";
        // line 24
        echo twig_call_macro($macros["menuUpdateView"], "macro_resetButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((        // line 25
($context["routePrefix"] ?? null) . "ajax_reset"), ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "root", [], "any", false, false, false, 25), "name", [], "any", false, false, false, 25), "context" => ($context["context"] ?? null)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((        // line 26
($context["routePrefix"] ?? null) . "view"), ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "root", [], "any", false, false, false, 26), "name", [], "any", false, false, false, 26), "context" => ($context["context"] ?? null)]), "menuKey" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 27
($context["entity"] ?? null), "root", [], "any", false, false, false, 27), "name", [], "any", false, false, false, 27), "data" => ["success-message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((        // line 29
array_key_exists("resetedMessage", $context)) ? (_twig_default_filter(($context["resetedMessage"] ?? null), "oro.navigation.menuupdate.reset_message")) : ("oro.navigation.menuupdate.reset_message")), ["%menuKey%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "root", [], "any", false, false, false, 29), "name", [], "any", false, false, false, 29), "%reload_link%" => ($context["reloadLink"] ?? null)])]]], 24, $context, $this->getSourceContext());
        // line 31
        echo "

                        ";
        // line 33
        echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 33, $context, $this->getSourceContext());
        echo "

                        ";
        // line 35
        $context["html"] = twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((        // line 36
($context["routePrefix"] ?? null) . "create"), ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "root", [], "any", false, false, false, 36), "name", [], "any", false, false, false, 36), "context" => ($context["context"] ?? null)]), "aCss" => "btn-primary", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.entity_label")]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.entity_label")])]], 35, $context, $this->getSourceContext());
        // line 41
        echo "
                        ";
        // line 43
        echo "                        ";
        $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_ajaxButton", [["aCss" => "btn-primary no-hash menu-divider-create-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.divider")]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.divider")]), "dataMethod" => "POST", "dataRedirect" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 48
($context["app"] ?? null), "request", [], "any", false, false, false, 48), "uri", [], "any", false, false, false, 48), "dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((        // line 49
($context["routePrefix"] ?? null) . "ajax_create"), ["context" => ($context["context"] ?? null), "menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 49), "parentKey" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 49), "isDivider" => true]), "successMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((        // line 50
array_key_exists("dividerCreatedMessage", $context)) ? (_twig_default_filter(($context["dividerCreatedMessage"] ?? null), "oro.navigation.menuupdate.divider_created")) : ("oro.navigation.menuupdate.divider_created")), ["%reload_link%" => ($context["reloadLink"] ?? null)])]], 43, $context, $this->getSourceContext()));
        // line 52
        echo "
                        ";
        // line 53
        $context["parameters"] = ["html" =>         // line 54
($context["html"] ?? null), "groupKey" => "createButtons", "options" => ["moreButtonAttrs" => ["class" => "btn-primary"]]];
        // line 62
        echo "
                        ";
        // line 63
        echo twig_call_macro($macros["UI"], "macro_pinnedDropdownButton", [($context["parameters"] ?? null)], 63, $context, $this->getSourceContext());
        echo "
                    </div>
                </div>
            </div>
        </div>

        <div class=\"scrollable-container sidebar-container clearfix\">
            ";
        // line 70
        $this->displayBlock('content_data', $context, $blocks);
        // line 117
        echo "        </div>
    </div>
";
    }

    // line 19
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "                            ";
        echo twig_include($this->env, $context, "@OroNavigation/menuUpdate/pageHeader.html.twig");
        echo "
                        ";
    }

    // line 70
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "                ";
        $context["treeOptions"] = ["data" =>         // line 72
($context["tree"] ?? null), "menu" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 73
($context["entity"] ?? null), "root", [], "any", false, false, false, 73), "name", [], "any", false, false, false, 73), "nodeId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 74
($context["entity"] ?? null), "root", [], "any", false, false, false, 74), "name", [], "any", false, false, false, 74), "context" =>         // line 75
($context["context"] ?? null), "updateAllowed" => true, "view" => "oronavigation/js/app/views/tree-manage-view", "onRootSelectRoute" => (        // line 78
($context["routePrefix"] ?? null) . "view"), "onSelectRoute" => (        // line 79
($context["routePrefix"] ?? null) . "update"), "onMoveRoute" => (        // line 80
($context["routePrefix"] ?? null) . "ajax_move"), "successMessage" => ((        // line 81
array_key_exists("movedMessage", $context)) ? (_twig_default_filter(($context["movedMessage"] ?? null), "oro.navigation.menuupdate.moved_success_message")) : ("oro.navigation.menuupdate.moved_success_message")), "maxDepth" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 82
($context["entity"] ?? null), "extras", [], "any", false, true, false, 82), "max_nesting_level", [], "any", true, true, false, 82)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "extras", [], "any", false, true, false, 82), "max_nesting_level", [], "any", false, false, false, 82), 3)) : (3))];
        // line 84
        echo "
                ";
        // line 85
        $this->loadTemplate("@OroNavigation/menuUpdate/view.html.twig", "@OroNavigation/menuUpdate/view.html.twig", 85, "1413470395")->display(twig_array_merge($context, ["options" => ["fixSidebarHeight" => false]]));
        // line 116
        echo "            ";
    }

    // line 121
    public function macro_resetButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 122
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/view.html.twig", 122)->unwrap();
            // line 123
            echo "
    ";
            // line 124
            $context["url"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrl", [], "any", true, true, false, 124)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrl", [], "any", false, false, false, 124)) : (""));
            // line 125
            echo "
    ";
            // line 126
            $context["parametersData"] = twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", false, false, false, 126), ["message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.reset_confirm", ["%menuKey%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 127
($context["parameters"] ?? null), "menuKey", [], "any", false, false, false, 127)]), "ok-text" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.reset_ok_text"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.reset_title"), "url" =>             // line 130
($context["url"] ?? null)]);
            // line 132
            echo "
    ";
            // line 133
            $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["data" =>             // line 134
($context["parametersData"] ?? null), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.reset"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.reset"), "aCss" => " btn icons-holder-text no-hash remove-button", "path" => "#"]);
            // line 140
            echo "
    ";
            // line 141
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataRedirect", [], "any", true, true, false, 141)) {
                // line 142
                echo "        ";
                $context["data"] = twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", false, false, false, 142), ["redirect" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataRedirect", [], "any", false, false, false, 142)]);
                // line 143
                echo "        ";
                $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["data" => ($context["data"] ?? null)]);
                // line 144
                echo "    ";
            }
            // line 145
            echo "
    <div class=\"pull-left btn-group icons-holder\">
        ";
            // line 147
            echo twig_call_macro($macros["UI"], "macro_link", [($context["parameters"] ?? null)], 147, $context, $this->getSourceContext());
            echo "
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/menuUpdate/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 147,  226 => 145,  223 => 144,  220 => 143,  217 => 142,  215 => 141,  212 => 140,  210 => 134,  209 => 133,  206 => 132,  204 => 130,  203 => 127,  202 => 126,  199 => 125,  197 => 124,  194 => 123,  191 => 122,  178 => 121,  174 => 116,  172 => 85,  169 => 84,  167 => 82,  166 => 81,  165 => 80,  164 => 79,  163 => 78,  162 => 75,  161 => 74,  160 => 73,  159 => 72,  157 => 71,  153 => 70,  146 => 20,  142 => 19,  136 => 117,  134 => 70,  124 => 63,  121 => 62,  119 => 54,  118 => 53,  115 => 52,  113 => 50,  112 => 49,  111 => 48,  109 => 43,  106 => 41,  104 => 36,  103 => 35,  98 => 33,  94 => 31,  92 => 29,  91 => 27,  90 => 26,  89 => 25,  88 => 24,  84 => 22,  82 => 19,  72 => 14,  69 => 13,  66 => 12,  63 => 11,  59 => 10,  55 => 1,  52 => 8,  50 => 7,  48 => 6,  46 => 5,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/menuUpdate/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/menuUpdate/view.html.twig");
    }
}


/* @OroNavigation/menuUpdate/view.html.twig */
class __TwigTemplate_234fb654c710de37fa6fe9d41484f5bb___1413470395 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'sidebar' => [$this, 'block_sidebar'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 85
        return "@OroUI/content_sidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUI/content_sidebar.html.twig", "@OroNavigation/menuUpdate/view.html.twig", 85);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 88
    public function block_sidebar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "                        ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/view.html.twig", 89)->unwrap();
        // line 90
        echo "
                        ";
        // line 91
        echo twig_call_macro($macros["UI"], "macro_renderJsTree", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menu.menu_list_default.label"), "treeOptions" =>         // line 94
($context["treeOptions"] ?? null)], ["move" => ["view" => "oroui/js/app/views/jstree/move-action-view", "routeName" => (        // line 99
($context["routePrefix"] ?? null) . "move"), "routeParams" => ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 100
($context["entity"] ?? null), "root", [], "any", false, false, false, 100), "name", [], "any", false, false, false, 100), "context" => ($context["context"] ?? null)]]]], 91, $context, $this->getSourceContext());
        // line 103
        echo "
                    ";
    }

    // line 106
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 107
        echo "                        <div class=\"category-data\">
                            <div class=\"tree-empty-content scrollable-container\">
                                <div class=\"no-data\">
                                    ";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.select_existing_or_create_new"), "html", null, true);
        echo "
                                </div>
                            </div>
                        </div>
                    ";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/menuUpdate/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  324 => 110,  319 => 107,  315 => 106,  310 => 103,  308 => 100,  307 => 99,  306 => 94,  305 => 91,  302 => 90,  299 => 89,  295 => 88,  284 => 85,  230 => 147,  226 => 145,  223 => 144,  220 => 143,  217 => 142,  215 => 141,  212 => 140,  210 => 134,  209 => 133,  206 => 132,  204 => 130,  203 => 127,  202 => 126,  199 => 125,  197 => 124,  194 => 123,  191 => 122,  178 => 121,  174 => 116,  172 => 85,  169 => 84,  167 => 82,  166 => 81,  165 => 80,  164 => 79,  163 => 78,  162 => 75,  161 => 74,  160 => 73,  159 => 72,  157 => 71,  153 => 70,  146 => 20,  142 => 19,  136 => 117,  134 => 70,  124 => 63,  121 => 62,  119 => 54,  118 => 53,  115 => 52,  113 => 50,  112 => 49,  111 => 48,  109 => 43,  106 => 41,  104 => 36,  103 => 35,  98 => 33,  94 => 31,  92 => 29,  91 => 27,  90 => 26,  89 => 25,  88 => 24,  84 => 22,  82 => 19,  72 => 14,  69 => 13,  66 => 12,  63 => 11,  59 => 10,  55 => 1,  52 => 8,  50 => 7,  48 => 6,  46 => 5,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/menuUpdate/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/menuUpdate/view.html.twig");
    }
}
