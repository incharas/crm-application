<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Attribute/index.html.twig */
class __TwigTemplate_e47f37d437a10c7445c2833e09a60a3f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Attribute/index.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroEntityConfig/Attribute/index.html.twig", 3)->unwrap();
        // line 5
        $context["gridName"] = "attributes-grid";
        // line 6
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["attributesLabel"] ?? null));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroEntityConfig/Attribute/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Attribute/index.html.twig", 9)->unwrap();
        // line 10
        echo "
    ";
        // line 11
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_attribute_create") && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_attribute_update"))) {
            // line 12
            echo "        ";
            $this->loadTemplate("@OroImportExport/ImportExport/buttons_from_configuration.html.twig", "@OroEntityConfig/Attribute/index.html.twig", 12)->display(twig_array_merge($context, ["alias" => "oro_field_config_model_attribute", "options" => ["entity_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["entity"] ?? null), "id", [], "any", false, false, false, 14)]]));
            // line 16
            echo "    ";
        }
        // line 17
        echo "
    ";
        // line 18
        echo twig_call_macro($macros["entityConfig"], "macro_displayLayoutActions", [($context["layoutActions"] ?? null)], 18, $context, $this->getSourceContext());
        echo "
    ";
        // line 19
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_attribute_create")) {
            // line 20
            echo "        <div class=\"btn-group pull-right\">
            ";
            // line 21
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_create", ["alias" =>             // line 22
($context["entityAlias"] ?? null)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute.entity_label")]], 21, $context, $this->getSourceContext());
            // line 24
            echo "
        </div>
    ";
        }
    }

    // line 29
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $this->displayParentBlock("content", $context, $blocks);
        echo "
    ";
        // line 31
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("attributes_content_after", $context)) ? (_twig_default_filter(($context["attributes_content_after"] ?? null), "attributes_content_after")) : ("attributes_content_after")), ["entityAlias" => ($context["entityAlias"] ?? null)]);
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Attribute/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 31,  101 => 30,  97 => 29,  90 => 24,  88 => 22,  87 => 21,  84 => 20,  82 => 19,  78 => 18,  75 => 17,  72 => 16,  70 => 14,  68 => 12,  66 => 11,  63 => 10,  60 => 9,  56 => 8,  51 => 1,  49 => 6,  47 => 5,  45 => 3,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Attribute/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Attribute/index.html.twig");
    }
}
