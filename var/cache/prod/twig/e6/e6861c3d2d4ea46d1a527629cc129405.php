<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroLocale/js_modules_config.html.twig */
class __TwigTemplate_ba037e77a68e0de9b369e872e696218c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["dateTimeFormats"] = [];
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateFormatExtension']->getDateTimeFormatterList());
        foreach ($context['_seq'] as $context["_key"] => $context["formatterName"]) {
            // line 3
            echo "    ";
            $context["dateTimeFormats"] = twig_array_merge(($context["dateTimeFormats"] ?? null), [            // line 4
$context["formatterName"] => ["day" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateFormatExtension']->getDayFormat(            // line 5
$context["formatterName"]), "date" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateFormatExtension']->getDateFormat(            // line 6
$context["formatterName"]), "time" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateFormatExtension']->getTimeFormat(            // line 7
$context["formatterName"]), "datetime" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateFormatExtension']->getDateTimeFormat(            // line 8
$context["formatterName"])]]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formatterName'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
";
        // line 13
        $context["numberFormatSettings"] = [];
        // line 14
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable([0 => "decimal", 1 => "percent", 2 => "currency"]);
        foreach ($context['_seq'] as $context["_key"] => $context["style"]) {
            // line 15
            echo "    ";
            $context["locale"] = ((($context["style"] == "currency")) ? ((($this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getLocale() . "@currency=") . $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getCurrency())) : ($this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getLocale()));
            // line 16
            echo "    ";
            $context["numberFormatSettings"] = twig_array_merge(($context["numberFormatSettings"] ?? null), [            // line 17
$context["style"] => ["grouping_size" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getAttribute("grouping_size",             // line 18
$context["style"]), "grouping_used" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getAttribute("grouping_used",             // line 19
$context["style"]), "min_fraction_digits" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getAttribute("min_fraction_digits",             // line 20
$context["style"], ($context["locale"] ?? null)), "max_fraction_digits" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getAttribute("max_fraction_digits",             // line 21
$context["style"], ($context["locale"] ?? null)), "negative_prefix" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getTextAttribute("negative_prefix",             // line 23
$context["style"]), "negative_suffix" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getTextAttribute("negative_suffix",             // line 24
$context["style"]), "positive_prefix" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getTextAttribute("positive_prefix",             // line 25
$context["style"]), "positive_suffix" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getTextAttribute("positive_suffix",             // line 26
$context["style"]), "currency_code" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getTextAttribute("currency_code",             // line 27
$context["style"]), "padding_character" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getTextAttribute("padding_character",             // line 28
$context["style"]), "decimal_separator_symbol" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getSymbol("decimal_separator_symbol",             // line 30
$context["style"]), "grouping_separator_symbol" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getSymbol("grouping_separator_symbol",             // line 31
$context["style"]), "monetary_separator_symbol" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getSymbol("monetary_separator_symbol",             // line 32
$context["style"]), "monetary_grouping_separator_symbol" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getSymbol("monetary_grouping_separator_symbol",             // line 33
$context["style"]), "currency_symbol" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getSymbol("currency_symbol",             // line 34
$context["style"]), "zero_digit_symbol" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->getSymbol("zero_digit_symbol",             // line 35
$context["style"])]]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['style'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "
";
        // line 40
        $context["localeConfigurationClass"] = "\\Oro\\Bundle\\LocaleBundle\\DependencyInjection\\Configuration::";
        // line 41
        $context["currencyConfigurationClass"] = "\\Oro\\Bundle\\CurrencyBundle\\DependencyInjection\\Configuration::";
        // line 42
        $context["defaults"] = ["locale" => twig_constant((        // line 43
($context["localeConfigurationClass"] ?? null) . "DEFAULT_LOCALE")), "language" => twig_constant((        // line 44
($context["localeConfigurationClass"] ?? null) . "DEFAULT_LANGUAGE")), "rtl_mode" => false, "country" => twig_constant((        // line 46
($context["localeConfigurationClass"] ?? null) . "DEFAULT_COUNTRY")), "currency" => twig_constant((        // line 47
($context["currencyConfigurationClass"] ?? null) . "DEFAULT_CURRENCY"))];
        // line 49
        echo "
";
        // line 51
        $context["mnemonicWeekDayNames"] = ["1" => "sunday", "2" => "monday", "3" => "tuesday", "4" => "wednesday", "5" => "thursday", "6" => "friday", "7" => "saturday"];
        // line 60
        echo "
";
        // line 61
        $context["settings"] = ["locale" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getLocale(), "language" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getLanguage(), "rtl_mode" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->isRtlMode(), "country" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getCountry(), "currency" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getCurrency(), "currencyViewType" => $this->extensions['Oro\Bundle\CurrencyBundle\Twig\CurrencyExtension']->getViewType(), "currencySymbolPrepend" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->isCurrencySymbolPrepend(), "currency_data" => $this->extensions['Oro\Bundle\CurrencyBundle\Twig\CurrencyExtension']->getSymbolCollection(), "timezone" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone(), "timezone_offset" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZoneOffset(), "format_address_by_address_country" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->isFormatAddressByAddressCountry(), "apiKey" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_google_integration.google_api_key"), "unit" => ["temperature" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_locale.temperature_unit"), "wind_speed" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_locale.wind_speed_unit")], "format" => ["datetime" =>         // line 79
($context["dateTimeFormats"] ?? null), "number" =>         // line 80
($context["numberFormatSettings"] ?? null)], "calendar" => ["dow" => ["wide" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getDayOfWeekNames("wide"), "abbreviated" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getDayOfWeekNames("abbreviated"), "short" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getDayOfWeekNames("short"), "narrow" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getDayOfWeekNames("narrow"), "mnemonic" =>         // line 88
($context["mnemonicWeekDayNames"] ?? null)], "months" => ["wide" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getMonthNames("wide"), "abbreviated" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getMonthNames("abbreviated"), "narrow" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getMonthNames("narrow")], "first_dow" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\CalendarExtension']->getFirstDayOfWeek()]];
        // line 98
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroLocale/js_modules_config.html.twig", 98)->unwrap();
        // line 99
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["orolocale/js/locale-settings" => ["defaults" =>         // line 101
($context["defaults"] ?? null), "settings" =>         // line 102
($context["settings"] ?? null)]]], 99, $context, $this->getSourceContext());
        // line 104
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroLocale/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 104,  121 => 102,  120 => 101,  119 => 99,  117 => 98,  115 => 88,  114 => 80,  113 => 79,  112 => 61,  109 => 60,  107 => 51,  104 => 49,  102 => 47,  101 => 46,  100 => 44,  99 => 43,  98 => 42,  96 => 41,  94 => 40,  91 => 39,  85 => 35,  84 => 34,  83 => 33,  82 => 32,  81 => 31,  80 => 30,  79 => 28,  78 => 27,  77 => 26,  76 => 25,  75 => 24,  74 => 23,  73 => 21,  72 => 20,  71 => 19,  70 => 18,  69 => 17,  67 => 16,  64 => 15,  60 => 14,  58 => 13,  55 => 12,  49 => 8,  48 => 7,  47 => 6,  46 => 5,  45 => 4,  43 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroLocale/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/LocaleBundle/Resources/views/js_modules_config.html.twig");
    }
}
