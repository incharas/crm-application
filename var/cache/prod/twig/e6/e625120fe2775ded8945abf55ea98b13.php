<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNote/Note/widget/notes.html.twig */
class __TwigTemplate_50be226bd04ea0a83b6cd915aae162d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'widget_content' => [$this, 'block_widget_content'],
            'widget_actions' => [$this, 'block_widget_actions'],
            'items_container' => [$this, 'block_items_container'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNote/Note/widget/notes.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["containerExtraClass"] = ((array_key_exists("containerExtraClass", $context)) ? (($context["containerExtraClass"] ?? null)) : (""));
        // line 4
        echo "
<div class=\"widget-content notes ";
        // line 5
        echo twig_escape_filter($this->env, ($context["containerExtraClass"] ?? null), "html", null, true);
        echo "\">
    ";
        // line 6
        $this->displayBlock('widget_content', $context, $blocks);
        // line 70
        echo "</div>
";
    }

    // line 6
    public function block_widget_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "        ";
        $this->displayBlock('widget_actions', $context, $blocks);
        // line 37
        echo "
        ";
        // line 38
        $this->displayBlock('items_container', $context, $blocks);
        // line 69
        echo "    ";
    }

    // line 7
    public function block_widget_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "            <div class=\"widget-actions\">
                ";
        // line 9
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "expand-all-button btn-link", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Expand All"), "dataAttributes" => ["action-name" => "expand_all"]]], 9, $context, $this->getSourceContext());
        // line 13
        echo "
                ";
        // line 14
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "collapse-all-button btn-link", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Collapse All"), "dataAttributes" => ["action-name" => "collapse_all"]]], 14, $context, $this->getSourceContext());
        // line 18
        echo "
                ";
        // line 19
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "btn btn-icon refresh-button icons-holder-text", "iCss" => "fa-refresh", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Refresh"), "dataAttributes" => ["action-name" => "refresh"]]], 19, $context, $this->getSourceContext());
        // line 24
        echo "
                ";
        // line 25
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "btn btn-icon sort-button icons-holder-text", "iCss" => "fa-arrow-up", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Descending Order"), "dataAttributes" => ["action-name" => "toggle_sorting", "title-alt" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ascending Order"), "icon-alt" => "fa-arrow-down"]]], 25, $context, $this->getSourceContext());
        // line 34
        echo "
            </div>
        ";
    }

    // line 38
    public function block_items_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "            ";
        $context["options"] = ["widgetId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 40
($context["app"] ?? null), "request", [], "any", false, false, false, 40), "get", [0 => "_wid"], "method", false, false, false, 40), "notesOptions" => ["template" => "#template-note-list", "itemTemplate" => "#template-note-item", "urls" => ["list" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_note_notes", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 46
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 46)]), "createItem" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_note_create", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 49
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 49)])], "routes" => ["update" => "oro_note_update", "delete" => "oro_api_delete_note"]], "notesData" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_note_notes", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 58
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 58)]))];
        // line 61
        echo "
            <div class=\"container-fluid accordion\"
                data-page-component-module=\"oronote/js/app/components/notes-component\"
                data-page-component-options=\"";
        // line 64
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"></div>

            ";
        // line 66
        $this->loadTemplate("@OroNote/Note/js/list.html.twig", "@OroNote/Note/widget/notes.html.twig", 66)->display(twig_array_merge($context, ["id" => "template-note-list"]));
        // line 67
        echo "            ";
        $this->loadTemplate("@OroNote/Note/js/view.html.twig", "@OroNote/Note/widget/notes.html.twig", 67)->display(twig_array_merge($context, ["id" => "template-note-item"]));
        // line 68
        echo "        ";
    }

    public function getTemplateName()
    {
        return "@OroNote/Note/widget/notes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 68,  130 => 67,  128 => 66,  123 => 64,  118 => 61,  116 => 58,  115 => 49,  114 => 46,  113 => 40,  111 => 39,  107 => 38,  101 => 34,  99 => 25,  96 => 24,  94 => 19,  91 => 18,  89 => 14,  86 => 13,  84 => 9,  81 => 8,  77 => 7,  73 => 69,  71 => 38,  68 => 37,  65 => 7,  61 => 6,  56 => 70,  54 => 6,  50 => 5,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNote/Note/widget/notes.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NoteBundle/Resources/views/Note/widget/notes.html.twig");
    }
}
