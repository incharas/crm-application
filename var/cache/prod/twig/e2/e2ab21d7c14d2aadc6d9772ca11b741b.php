<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCase/Comment/js/view.html.twig */
class __TwigTemplate_e2bcbbe5dcdecebfce376406b857870b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script type=\"text/html\" id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html_attr");
        echo "\">
    <div class=\"accordion-group\">
        <div class=\"accordion-heading collapse<% if (!collapsed) { %> show<% } %>\">
            <div class=\"title-item\">
                <a href=\"#accordion-item<%- id %>\" class=\"no-hash accordion-toggle<% if (collapsed) { %> collapsed<% } %>\"></a>
                <span class=\"visual\">
                    <% if (createdBy) { %>
                        <span class=\"avatar\">
                            <% if (createdBy.avatarPicture && createdBy.avatarPicture.src) { %>
                                <picture>
                                    <% _.each(createdBy.avatarPicture.sources, function(source) { %>
                                        <source srcset=\"<%- source.srcset %>\" type=\"<%- source.type %>\">
                                    <% }); %>
                                    <img src=\"<%- createdBy.avatarPicture.src %>\"/>
                                </picture>
                            <% } else { %>
                                <span class=\"avatar-placeholder\" aria-hidden=\"true\"></span>
                            <% } %>
                        </span>

                        <% if (createdBy.url) { %>
                            <a class=\"user\" href=\"<%- createdBy.url %>\"><%- createdBy.fullName %></a>
                        <% } else { %>
                            <span class=\"user\"><%- createdBy %></span>
                        <% } %>
                    <% } %>
                </span>
            </div>
            <div class=\"message-item\">
                <div class=\"message\"><%- briefMessage %></div>
            </div>
            <div class=\"labels\">
                <% if (!public) { %>
                    <span class=\"label\"><%- _.__(\"oro.case.private\") %></span>
                <% } %>
            </div>
            <div class=\"details\">
                <span class=\"date\"><%- createdAt %></span>
            </div>
            <div class=\"actions\">
                <button class=\"btn btn-icon btn-lighter item-edit-button<% if (!permissions.edit) { %> disabled<% } %>\"
                    <% if (!permissions.edit) { %> disabled=\"disabled\"<% } %> title=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.action.edit"), "html_attr");
        echo "\">
                        <span class=\"action-button-icon fa-pencil-square-o\" aria-label=\"";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.action.edit"), "html");
        echo "\"></span>
                ";
        // line 45
        echo "                </button><button
                     class=\"btn btn-icon btn-lighter item-remove-button<% if (!permissions.delete) { %> disabled<% } %>\"
                     <% if (!permissions.delete) { %> disabled=\"disabled\"<% } %> title=\"";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.action.delete"), "html_attr");
        echo "\">
                        <span class=\"action-button-icon fa-trash-o\" aria-label=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.action.delete"), "html");
        echo "\"></span>
                </button>
            </div>
        </div>
        <div class=\"accordion-body collapse<% if (!collapsed) { %> show<% } %>\" id=\"accordion-item<%- id %>\">
            <div class=\"message\">
                <%- message %>
                <% if (updatedBy) { %>
                <div class=\"details\">
                    <div>
                        <%= _.template(
                        ";
        // line 59
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.message.updated_by"));
        echo ",
                        { interpolate: /\\{\\{(.+?)\\}\\}/g }
                        )({
                        user: updatedBy.url
                        ? '<a class=\"user\" href=\"' + updatedBy.url + '\">' + _.escape(updatedBy.fullName) + '</a>'
                        : (updatedBy ? '<i class=\"user\">' + _.escape(updatedBy) + '</i>' : ''),
                        date: '<span class=\"date\">' + updatedAt + '</span>'
                        }) %>
                    </div>
                </div>
                <% } %>
            </div>
        </div>
    </div>
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroCase/Comment/js/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 59,  98 => 48,  94 => 47,  90 => 45,  86 => 43,  82 => 42,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCase/Comment/js/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/CaseBundle/Resources/views/Comment/js/view.html.twig");
    }
}
