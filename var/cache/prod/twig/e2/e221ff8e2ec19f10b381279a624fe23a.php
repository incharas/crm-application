<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/input-widget/clearable.js */
class __TwigTemplate_6f62927354a6b5534c249c1286d398a0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const template = require('tpl-loader!oroui/templates/clearable.html');
    const AbstractInputWidgetView = require('oroui/js/app/views/input-widget/abstract');

    const ClearableInputWidgetView = AbstractInputWidgetView.extend({
        refreshOnChange: true,

        widgetFunctionName: 'clearable',

        template: template,

        containerClass: 'clearable-input__container',

        \$input: null,

        events: {
            'input input': 'refresh',
            'change input': 'refresh',
            'click .clearable-input__clear': 'onClear'
        },

        /**
         * @inheritdoc
         */
        constructor: function ClearableInputWidgetView(options) {
            ClearableInputWidgetView.__super__.constructor.call(this, options);
        },

        render: function() {
            const \$container = \$(this.template({placeholderIcon: this.\$el.data('placeholder-icon')}));
            this.\$input = this.\$el;
            this.\$el.after(\$container);
            \$container.prepend(this.\$input);

            this.setElement(\$container);

            return this;
        },

        /**
         * @inheritdoc
         */
        widgetFunction: function() {
            this.render();
            this.refresh();
        },

        refresh: function() {
            this.getContainer().toggleClass('clearable-input__container--clear', this.\$input.val().length === 0);
        },

        onClear: function() {
            this.\$input.val('').focus().trigger('input');
        },

        /**
         * @inheritdoc
         */
        findContainer: function() {
            return this.\$el;
        }
    });

    return ClearableInputWidgetView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/input-widget/clearable.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/input-widget/clearable.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/input-widget/clearable.js");
    }
}
