<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Button/transitionButton.html.twig */
class __TwigTemplate_ff39fa40b39771aefe8acb11fa393c0e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["workflowMacros"] = $this->macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/Button/transitionButton.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 4
        $context["transitionMessage"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["button"] ?? null), "transition", [], "any", false, false, false, 4), "message", [], "any", false, false, false, 4), [], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["button"] ?? null), "translationDomain", [], "any", false, false, false, 4));
        // line 5
        if ((($context["transitionMessage"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["button"] ?? null), "transition", [], "any", false, false, false, 5), "message", [], "any", false, false, false, 5))) {
            // line 6
            echo "    ";
            // line 7
            echo "    ";
            $context["transitionMessage"] = "";
        }
        // line 9
        echo "
";
        // line 10
        $context["transitionData"] = ["message" => twig_nl2br(twig_escape_filter($this->env,         // line 11
($context["transitionMessage"] ?? null), "html", null, true)), "transition-condition-messages" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["button"] ?? null), "buttonContext", [], "any", false, false, false, 12), "errors", [], "any", false, false, false, 12), "enabled" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["button"] ?? null), "buttonContext", [], "any", false, false, false, 13), "enabled", [], "any", false, false, false, 13), "transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(        // line 14
($context["executionRoute"] ?? null), ($context["routeParams"] ?? null))];
        // line 16
        echo "
";
        // line 17
        $context["transitionData"] = twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendOptions"] ?? null), "data", [], "any", true, true, false, 17)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendOptions"] ?? null), "data", [], "any", false, false, false, 17), [])) : ([])), ($context["transitionData"] ?? null));
        // line 18
        if (($context["hasForm"] ?? null)) {
            // line 19
            echo "    ";
            if (($context["showDialog"] ?? null)) {
                // line 20
                echo "        ";
                $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["dialog-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["dialogRoute"] ?? null), ($context["routeParams"] ?? null))]);
                // line 21
                echo "    ";
            } else {
                // line 22
                echo "        ";
                $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["dialogRoute"] ?? null), ($context["routeParams"] ?? null))]);
                // line 23
                echo "    ";
            }
        }
        // line 25
        if ( !((array_key_exists("onlyLink", $context)) ? (_twig_default_filter(($context["onlyLink"] ?? null), false)) : (false))) {
            echo "<div class=\"pull-left btn-group icons-holder\">";
        }
        // line 26
        echo "    ";
        echo twig_call_macro($macros["workflowMacros"], "macro_renderTransitionButton", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 27
($context["button"] ?? null), "workflow", [], "any", false, false, false, 27), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 28
($context["button"] ?? null), "transition", [], "any", false, false, false, 28), ((        // line 29
array_key_exists("workflowItem", $context)) ? (_twig_default_filter(($context["workflowItem"] ?? null), null)) : (null)),         // line 30
($context["transitionData"] ?? null), ((        // line 31
array_key_exists("onlyLink", $context)) ? (_twig_default_filter(($context["onlyLink"] ?? null), false)) : (false)), ((        // line 32
array_key_exists("noIcon", $context)) ? (_twig_default_filter(($context["noIcon"] ?? null), false)) : (false)), ((        // line 33
array_key_exists("noIconText", $context)) ? (_twig_default_filter(($context["noIconText"] ?? null), false)) : (false)), ((        // line 34
array_key_exists("aClass", $context)) ? (_twig_default_filter(($context["aClass"] ?? null), "")) : (""))], 26, $context, $this->getSourceContext());
        // line 35
        echo "
";
        // line 36
        if ( !((array_key_exists("onlyLink", $context)) ? (_twig_default_filter(($context["onlyLink"] ?? null), false)) : (false))) {
            echo "</div>";
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Button/transitionButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 36,  99 => 35,  97 => 34,  96 => 33,  95 => 32,  94 => 31,  93 => 30,  92 => 29,  91 => 28,  90 => 27,  88 => 26,  84 => 25,  80 => 23,  77 => 22,  74 => 21,  71 => 20,  68 => 19,  66 => 18,  64 => 17,  61 => 16,  59 => 14,  58 => 13,  57 => 12,  56 => 11,  55 => 10,  52 => 9,  48 => 7,  46 => 6,  44 => 5,  42 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Button/transitionButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Button/transitionButton.html.twig");
    }
}
