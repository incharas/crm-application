<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/form-loading-view.js */
class __TwigTemplate_3bd72be8bec149062d43564970642797 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const FormSectionLoadingView = require('oroform/js/app/views/form-section-loading-view');
    const \$ = require('jquery');
    const _ = require('underscore');
    const mediator = require('oroui/js/mediator');

    const FormLoadingView = BaseView.extend({
        autoRender: true,

        /**
         * @inheritdoc
         */
        constructor: function FormLoadingView(options) {
            FormLoadingView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            // TODO: uncomment when scrol to section will be fixed
            // var index = this.\$(window.location.hash).parents('.responsive-section').index();
            //
            // index = index !== -1 ? index : 0;
            const index = 0;

            this.\$('.responsive-section')
                .not(':nth-child(' + (index + 1) + '),[data-init-section-instantly]')
                .each((index, el) => {
                    this.subview('form-section-loading-' + index, new FormSectionLoadingView({
                        el: \$(el)
                    }));
                });

            FormLoadingView.__super__.initialize.call(this, options);
        },

        render: function() {
            FormLoadingView.__super__.render.call(this);

            this.\$el.removeAttr('data-skip-input-widgets');
            this.\$el.addClass('lazy-loading');

            this.initLayout()
                .then(function() {
                    const \$buttons = this._getNavButtons();
                    \$buttons.addClass('disabled');
                    this._loadSubviews().then(this._afterLoadSubviews.bind(this, \$buttons));
                }.bind(this));

            return this;
        },

        _afterLoadSubviews: function(\$buttons) {
            \$buttons.removeClass('disabled');
            mediator.trigger('page:afterPagePartChange');
            this.\$el.removeClass('lazy-loading');
        },

        _loadSubviews: function() {
            const promises = _.map(this.subviews, function(view) {
                return view.startLoading();
            });

            return \$.when(...promises);
        },

        _getNavButtons: function() {
            return this.\$('.title-buttons-container').find(':button, [role=\"button\"]');
        }
    });

    return FormLoadingView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/form-loading-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/form-loading-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/form-loading-view.js");
    }
}
