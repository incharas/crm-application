<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Twig/picture.html.twig */
class __TwigTemplate_09835fc6c77560553109082479b731cd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'sources' => [$this, 'block_sources'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        ob_start(function () { return ''; });
        // line 2
        echo "    ";
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAttachment/Twig/picture.html.twig", 2)->unwrap();
        // line 3
        echo "
    ";
        // line 4
        $context["picture_attrs"] = ((array_key_exists("picture_attrs", $context)) ? (_twig_default_filter(($context["picture_attrs"] ?? null), [])) : ([]));
        // line 5
        echo "    ";
        $context["file"] = ((array_key_exists("file", $context)) ? (_twig_default_filter(($context["file"] ?? null), null)) : (null));
        // line 6
        echo "    ";
        $context["filter"] = ((array_key_exists("filter", $context)) ? (_twig_default_filter(($context["filter"] ?? null), "original")) : ("original"));
        // line 7
        echo "    ";
        $context["sources"] = _twig_default_filter(((array_key_exists("sources", $context)) ? (_twig_default_filter(($context["sources"] ?? null), $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(($context["file"] ?? null), ($context["filter"] ?? null)))) : ($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(($context["file"] ?? null), ($context["filter"] ?? null)))), []);
        // line 8
        echo "    ";
        $context["img_attrs"] = ((array_key_exists("img_attrs", $context)) ? (_twig_default_filter(($context["img_attrs"] ?? null), [])) : ([]));
        // line 9
        echo "    ";
        $context["img_src"] = ((array_key_exists("img_src", $context)) ? (_twig_default_filter(($context["img_src"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["img_attrs"] ?? null), "src", [], "any", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["img_attrs"] ?? null), "src", [], "any", false, false, false, 9), "")) : ("")))) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["img_attrs"] ?? null), "src", [], "any", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["img_attrs"] ?? null), "src", [], "any", false, false, false, 9), "")) : (""))));
        // line 10
        echo "
    ";
        // line 11
        if ( !twig_test_iterable(($context["sources"] ?? null))) {
            // line 12
            echo "        ";
            $context["img_src"] = ($context["sources"] ?? null);
            // line 13
            echo "        ";
            $context["sources"] = [];
            // line 14
            echo "    ";
        }
        // line 15
        echo "
    ";
        // line 16
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["sources"] ?? null), "src", [], "any", true, true, false, 16) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["sources"] ?? null), "sources", [], "any", true, true, false, 16))) {
            // line 17
            echo "        ";
            $context["img_src"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["sources"] ?? null), "src", [], "any", false, false, false, 17);
            // line 18
            echo "        ";
            $context["sources"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["sources"] ?? null), "sources", [], "any", false, false, false, 18);
            // line 19
            echo "    ";
        }
        // line 20
        echo "
    ";
        // line 21
        if ( !($context["img_src"] ?? null)) {
            // line 22
            echo "        ";
            $context["img_src"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, twig_last($this->env, ($context["sources"] ?? null)), "srcset", [], "any", true, true, false, 22)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, twig_last($this->env, ($context["sources"] ?? null)), "srcset", [], "any", false, false, false, 22), _twig_default_filter(twig_last($this->env, ($context["sources"] ?? null)), ""))) : (_twig_default_filter(twig_last($this->env, ($context["sources"] ?? null)), "")));
            // line 23
            echo "        ";
            $context["sources"] = twig_slice($this->env, ($context["sources"] ?? null), 0,  -1);
            // line 24
            echo "    ";
        }
        // line 25
        echo "
    ";
        // line 26
        if (($context["img_src"] ?? null)) {
            // line 27
            echo "        <picture ";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["picture_attrs"] ?? null)], 27, $context, $this->getSourceContext());
            echo ">
            ";
            // line 28
            $this->displayBlock('sources', $context, $blocks);
            // line 33
            echo "
            ";
            // line 34
            $context["img_attrs"] = twig_array_merge(["src" => ($context["img_src"] ?? null)], ($context["img_attrs"] ?? null));
            // line 35
            echo "            ";
            if (($context["file"] ?? null)) {
                // line 36
                echo "                ";
                $context["img_attrs"] = twig_array_merge(["alt" => $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileTitle(($context["file"] ?? null))], ($context["img_attrs"] ?? null));
                // line 37
                echo "            ";
            }
            // line 38
            echo "            <img ";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["img_attrs"] ?? null)], 38, $context, $this->getSourceContext());
            echo ">
        </picture>
    ";
        }
        $___internal_parse_44_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        echo twig_spaceless($___internal_parse_44_);
    }

    // line 28
    public function block_sources($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["sources"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["source_attrs"]) {
            // line 30
            echo "                    <source ";
            echo twig_call_macro($macros["UI"], "macro_attributes", [$context["source_attrs"]], 30, $context, $this->getSourceContext());
            echo ">
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['source_attrs'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "            ";
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Twig/picture.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 32,  152 => 30,  147 => 29,  143 => 28,  139 => 1,  131 => 38,  128 => 37,  125 => 36,  122 => 35,  120 => 34,  117 => 33,  115 => 28,  110 => 27,  108 => 26,  105 => 25,  102 => 24,  99 => 23,  96 => 22,  94 => 21,  91 => 20,  88 => 19,  85 => 18,  82 => 17,  80 => 16,  77 => 15,  74 => 14,  71 => 13,  68 => 12,  66 => 11,  63 => 10,  60 => 9,  57 => 8,  54 => 7,  51 => 6,  48 => 5,  46 => 4,  43 => 3,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Twig/picture.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Twig/picture.html.twig");
    }
}
