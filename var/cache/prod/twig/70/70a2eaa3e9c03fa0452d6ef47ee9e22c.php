<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/show-more/table-show-more-view.js */
class __TwigTemplate_3d46122999f45dfd9e77f7ac5d650810 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import _ from 'underscore';
import AbstractShowMoreView from 'oroui/js/app/views/show-more/abstract-show-more-view';

const TableShowMoreView = AbstractShowMoreView.extend({
    alwaysVisibleItems: 3,

    itemSelector: 'tbody tr',

    /**
     * @inheritdoc
     */
    constructor: function TableShowMoreView(options) {
        Object.assign(this, _.pick(options, 'alwaysVisibleItems'));

        TableShowMoreView.__super__.constructor.call(this, options);
    },

    render() {
        TableShowMoreView.__super__.render.call(this);

        this.\$(_.rest(this.items, this.alwaysVisibleItems)).addClass('item-to-hide');
    },

    numItemsToHide() {
        return this.items.length > this.alwaysVisibleItems ? this.items.length - this.alwaysVisibleItems : 0;
    }
});

export default TableShowMoreView;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/show-more/table-show-more-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/show-more/table-show-more-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/show-more/table-show-more-view.js");
    }
}
