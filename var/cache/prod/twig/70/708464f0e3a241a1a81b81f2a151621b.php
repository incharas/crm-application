<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/lib/simplecolorpicker/ORO_CHANGELOG.md */
class __TwigTemplate_b1295e950326c7d342799afb6b7701fc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "Changes in BAP 1.5.0
====================

2014-11-12
----------

* `emptyColor` option has been added. It can be used to specify a color for empty option.
* grab CSS class from option element.
* Allow to apply the picker to INPUT element.
* Add `table` option to allow using this control to edit any color in the list.
* Add `replaceColor` method to allow to replace any color when `table` option is specified.
* Add contrast color for Ok/check mark.
* Modify `selectColor` method to allow to remove selection.
* Fix the picker to correct work with disabled element. Add `enable` method to allow to enable/disable this control.
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/lib/simplecolorpicker/ORO_CHANGELOG.md";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/lib/simplecolorpicker/ORO_CHANGELOG.md", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/lib/simplecolorpicker/ORO_CHANGELOG.md");
    }
}
