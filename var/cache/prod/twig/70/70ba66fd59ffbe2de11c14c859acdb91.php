<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/base-button.scss */
class __TwigTemplate_cc353bbb058ffb42d22bdd6586b4b506 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

%base-button {
    display: inline-flex;
    align-items: center;
    justify-content: center;
    font-weight: \$btn-font-weight;
    text-align: center;
    text-transform: \$btn-text-transform;
    white-space: nowrap;
    vertical-align: middle;
    user-select: none;
    border: \$btn-border-width solid transparent;
    flex-shrink: 0;

    @if (\$btn-theme) {
        @include button-custom-variant(\$btn-theme...);
    }

    @include button-size(\$btn-padding-y, \$btn-padding-x, \$btn-font-size, \$btn-line-height, \$btn-border-radius);
    @include transition(\$btn-transition);

    // Share hover and focus styles
    @include hover-focus {
        text-decoration: none;
    }

    &:focus,
    &.focus {
        outline: 0;
    }

    // Disabled comes first so active can properly restyle
    &.disabled,
    &:disabled {
        opacity: \$btn-disabled-opacity;

        @include box-shadow(none);
    }

    // Opinionated: add \"hand\" cursor to non-disabled .btn elements
    &:not(:disabled):not(.disabled) {
        cursor: pointer;
    }

    &:not(:disabled):not(.disabled):active,
    &:not(:disabled):not(.disabled).active {
        background-image: none;

        @include box-shadow(\$btn-active-box-shadow);

        &:focus {
            @include box-shadow(\$btn-focus-box-shadow, \$btn-active-box-shadow);
        }
    }

    [class*='fa-'] {
        font-size: \$btn-fa-icon-font-size;
        line-height: \$btn-fa-icon-line-height;
        vertical-align: \$btn-fa-icon-vertical-align;
    }

    &:not(.btn-icon) [class*='fa-'] {
        margin: \$btn-not-btn-icon-fa-offset;
    }

    &.icon-end:not(.btn-icon) [class^='fa-'] {
        margin: \$btn-not-btn-icon-end-fa-offset;
    }
}

%btn-square {
    line-height: \$btn-square-line-height;
    border-width: \$btn-square-border-width;
    border-radius: \$btn-square-border-radius;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/base-button.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/base-button.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/base-button.scss");
    }
}
