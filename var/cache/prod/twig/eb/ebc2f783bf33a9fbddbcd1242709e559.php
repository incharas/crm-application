<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntity/Choice/entity_field/result.html.twig */
class __TwigTemplate_0aa5c52d960f5968a33c7740f6079b86 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<%= highlight(_.escape(text)) %>
";
    }

    public function getTemplateName()
    {
        return "@OroEntity/Choice/entity_field/result.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntity/Choice/entity_field/result.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Resources/views/Choice/entity_field/result.html.twig");
    }
}
