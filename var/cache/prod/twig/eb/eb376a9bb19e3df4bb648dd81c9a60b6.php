<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/select2-autocomplete-view.js */
class __TwigTemplate_34ce3f20648e4f0a2cc2389ae1fb402b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'jquery',
    'underscore',
    './select2-view'
], function(\$, _, Select2View) {
    'use strict';

    const Select2AutocompleteView = Select2View.extend({
        events: {
            change: function(e) {
                if (this.\$el.data('select2').opts.multiple) {
                    const selectedData = this.\$el.data('selected-data') || [];
                    if (e.added) {
                        selectedData.push(e.added);
                    }
                    if (e.removed) {
                        const index = _.findIndex(selectedData, function(obj) {
                            return obj.id === e.removed.id;
                        });
                        if (index !== -1) {
                            selectedData.splice(index, 1);
                        }
                    }
                } else {
                    this.\$el.data('selected-data', e.added);
                }
            }
        },

        /**
         * @inheritdoc
         */
        constructor: function Select2AutocompleteView(options) {
            Select2AutocompleteView.__super__.constructor.call(this, options);
        }
    });

    return Select2AutocompleteView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/select2-autocomplete-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/select2-autocomplete-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/select2-autocomplete-view.js");
    }
}
