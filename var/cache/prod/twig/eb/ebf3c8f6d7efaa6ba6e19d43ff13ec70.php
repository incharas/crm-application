<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/tinymce/wysiwyg-editor.scss */
class __TwigTemplate_d21c8f022a31af78c2d073726ff47122 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import '~@oroinc/bootstrap/scss/reboot';
@import '~@oroinc/bootstrap/scss/type';

body {
    margin: 1rem;

    background-color: \$primary-inverse;
    // stylelint-disable-next-line property-no-unknown
    scrollbar-3dlight-color: \$primary-900;
    scrollbar-arrow-color: \$primary-400;
    scrollbar-base-color: \$primary-900;
    scrollbar-darkshadow-color: \$primary-750;
    scrollbar-face-color: \$primary-800;
    scrollbar-highlight-color: \$primary-900;
    scrollbar-shadow-color: \$primary-900;
    scrollbar-track-color: \$primary-860;

    &[contenteditable='false'] {
        cursor: not-allowed;
        background-color: \$primary-830;
    }
}

ul,
ol {
    padding-left: 16px;
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/tinymce/wysiwyg-editor.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/tinymce/wysiwyg-editor.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/tinymce/wysiwyg-editor.scss");
    }
}
