<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroComment/Comment/comments.html.twig */
class __TwigTemplate_5f3dddd03c373ad4126c68bf93d5da42 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroComment/Comment/comments.html.twig", 1)->unwrap();
        // line 2
        $context["containerExtraClass"] = ((array_key_exists("containerExtraClass", $context)) ? (($context["containerExtraClass"] ?? null)) : (""));
        // line 3
        echo "<div class=\"widget-content comment-list ";
        echo twig_escape_filter($this->env, ($context["containerExtraClass"] ?? null), "html", null, true);
        echo "\">
    ";
        // line 4
        $context["options"] = ["relatedEntityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["entity"] ?? null), "id", [], "any", false, false, false, 5), "relatedEntityClassName" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 6
($context["entity"] ?? null), true), "listTemplate" => "#template-activity-item-comment", "canCreate" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_comment_create"), "name" => "comments"];
        // line 11
        echo "    <div class=\"comment\"
         data-page-component-module=\"orocomment/js/app/components/comment-component\"
         data-page-component-options=\"";
        // line 13
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\">
    </div>
</div>
";
        // line 16
        $this->loadTemplate("@OroComment/Comment/js/list.html.twig", "@OroComment/Comment/comments.html.twig", 16)->display(twig_array_merge($context, ["id" => "template-activity-item-comment"]));
    }

    public function getTemplateName()
    {
        return "@OroComment/Comment/comments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 16,  54 => 13,  50 => 11,  48 => 6,  47 => 5,  46 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroComment/Comment/comments.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CommentBundle/Resources/views/Comment/comments.html.twig");
    }
}
