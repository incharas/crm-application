<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/widget-picker/widget-picker-filter-view.js */
class __TwigTemplate_0ad215c70fa22d27e9242bae100dbc1a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');

    const WidgetPickerFilterView = BaseView.extend({
        template: require('tpl-loader!oroui/templates/widget-picker/widget-picker-filter-view.html'),

        autoRender: true,

        events: {
            'input [data-role=\"filter-search\"]': 'onSearchChange'
        },

        /**
         * @inheritdoc
         */
        constructor: function WidgetPickerFilterView(options) {
            WidgetPickerFilterView.__super__.constructor.call(this, options);
        },

        /**
         * Handles search input value change and update the model
         *
         * @param {Event} e
         */
        onSearchChange: function(e) {
            this.model.set('search', e.currentTarget.value);
        }
    });

    return WidgetPickerFilterView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/widget-picker/widget-picker-filter-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/widget-picker/widget-picker-filter-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/widget-picker/widget-picker-filter-view.js");
    }
}
