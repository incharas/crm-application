<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/Datagrid/Property/tags.html.twig */
class __TwigTemplate_e3725b3e5ae5b87edf699de291b61c44 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["value"] ?? null)) {
            // line 2
            echo "    ";
            echo json_encode(($context["value"] ?? null));
            echo "
";
        } else {
            // line 4
            echo "    []
";
        }
    }

    public function getTemplateName()
    {
        return "@OroTag/Datagrid/Property/tags.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/Datagrid/Property/tags.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/Datagrid/Property/tags.html.twig");
    }
}
