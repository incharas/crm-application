<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables/content-sidebar.scss */
class __TwigTemplate_8351ba7e6c53403e81e5a61bcb19f997 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

// Main variables
\$content-sidebar-width: 320px !default;
\$content-sidebar-min-height: 90vh !default;

// Component variables
\$content-sidebar-mobile-max-width: \$content-sidebar-width !default;
\$content-sidebar-mobile-min-height: \$content-sidebar-min-height !default;
\$content-sidebar-mobile-margin: 0 0 0 (-\$content-padding) !default;

\$content-sidebar-items-mobile-offset-inner: 0 \$content-padding !default;

\$content-sidebar-jstree-wrapper-mobile-inner-offset: 0 \$content-padding !default;
\$content-sidebar-jstree-wrapper-jstree-mobile-inner-offset: 0
    (-\$content-padding) 0 - (\$content-padding + \$content-padding-medium) !default;

\$content-with-sidebar-controls-background: \$primary-830 !default;
\$content-with-sidebar-controls-color: \$primary-100 !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables/content-sidebar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables/content-sidebar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables/content-sidebar.scss");
    }
}
