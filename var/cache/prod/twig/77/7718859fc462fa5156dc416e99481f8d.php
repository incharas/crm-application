<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContact/Contact/view.html.twig */
class __TwigTemplate_d17cd0f92360d44614dd93eada564455 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'pageTitleIcon' => [$this, 'block_pageTitleIcon'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["macros"] = $this->macros["macros"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContact/Contact/view.html.twig", 2)->unwrap();
        // line 4
        $context["fullname"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["entity"] ?? null)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity.item", ["%id%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 4)]));

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%contact.name%" =>         // line 5
($context["fullname"] ?? null)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroContact/Contact/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContact/Contact/view.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 11
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contact_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 12
($context["entity"] ?? null), "id", [], "any", false, false, false, 12)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.entity_label")]], 11, $context, $this->getSourceContext());
            // line 14
            echo "
    ";
        }
        // line 16
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 17
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_contact", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["entity"] ?? null), "id", [], "any", false, false, false, 18)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contact_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-contact", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["entity"] ?? null), "id", [], "any", false, false, false, 22), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.entity_label")]], 17, $context, $this->getSourceContext());
            // line 24
            echo "
    ";
        }
    }

    // line 28
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 30
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contact_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.entity_plural_label"), "entityTitle" =>         // line 33
($context["fullname"] ?? null)];
        // line 35
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 38
    public function block_pageTitleIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "    <div class=\"page-title__icon\">";
        // line 40
        $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroContact/Contact/view.html.twig", 40)->display(twig_array_merge($context, ["file" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 41
($context["entity"] ?? null), "picture", [], "any", false, false, false, 41), "filter" => "avatar_med", "img_attrs" => ["alt" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(        // line 43
($context["entity"] ?? null))]]));
        // line 45
        echo "</div>
";
    }

    // line 48
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "    ";
        $this->loadTemplate("@OroContact/Contact/headerStats.html.twig", "@OroContact/Contact/view.html.twig", 49)->display(twig_array_merge($context, ["entity" => ($context["entity"] ?? null)]));
    }

    // line 52
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    ";
        ob_start(function () { return ''; });
        // line 54
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contact_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 56
($context["entity"] ?? null), "id", [], "any", false, false, false, 56)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Contact Information")]);
        // line 58
        echo "
    ";
        $context["contactInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 60
        echo "
    ";
        // line 61
        ob_start(function () { return ''; });
        // line 62
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "contentClasses" => [], "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contact_address_book", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 65
($context["entity"] ?? null), "id", [], "any", false, false, false, 65)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Address Book")]);
        // line 67
        echo "
    ";
        $context["addressBookWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 69
        echo "
    ";
        // line 70
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "subblocks" => [0 => ["data" => [0 =>         // line 74
($context["contactInformationWidget"] ?? null)]], 1 => ["data" => [0 =>         // line 75
($context["addressBookWidget"] ?? null)]]]]];
        // line 79
        echo "
    ";
        // line 80
        $context["id"] = "contactView";
        // line 81
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 82
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroContact/Contact/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 82,  172 => 81,  170 => 80,  167 => 79,  165 => 75,  164 => 74,  163 => 70,  160 => 69,  156 => 67,  154 => 65,  152 => 62,  150 => 61,  147 => 60,  143 => 58,  141 => 56,  139 => 54,  136 => 53,  132 => 52,  127 => 49,  123 => 48,  118 => 45,  116 => 43,  115 => 41,  114 => 40,  112 => 39,  108 => 38,  101 => 35,  99 => 33,  98 => 30,  96 => 29,  92 => 28,  86 => 24,  84 => 22,  83 => 18,  81 => 17,  78 => 16,  74 => 14,  72 => 12,  70 => 11,  68 => 10,  65 => 9,  62 => 8,  58 => 7,  53 => 1,  51 => 5,  48 => 4,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContact/Contact/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactBundle/Resources/views/Contact/view.html.twig");
    }
}
