<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Lead/widget/info.html.twig */
class __TwigTemplate_127f4a91d714fbd5f6463f09afc83d71 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Lead/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["email"] = $this->macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroSales/Lead/widget/info.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroSales/Lead/widget/info.html.twig", 3)->unwrap();
        // line 4
        $macros["channel"] = $this->macros["channel"] = $this->loadTemplate("@OroChannel/macros.html.twig", "@OroSales/Lead/widget/info.html.twig", 4)->unwrap();
        // line 5
        $macros["sales"] = $this->macros["sales"] = $this->loadTemplate("@OroSales/macros.html.twig", "@OroSales/Lead/widget/info.html.twig", 5)->unwrap();
        // line 6
        $macros["Tag"] = $this->macros["Tag"] = $this->loadTemplate("@OroTag/macros.html.twig", "@OroSales/Lead/widget/info.html.twig", 6)->unwrap();
        // line 7
        $macros["salesLeadInfo"] = $this->macros["salesLeadInfo"] = $this;
        // line 17
        echo "<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 20
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.first_name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "firstName", [], "any", false, false, false, 20)], 20, $context, $this->getSourceContext());
        echo "
            ";
        // line 21
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.last_name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lastName", [], "any", false, false, false, 21)], 21, $context, $this->getSourceContext());
        echo "

            ";
        // line 23
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dataChannel", [], "array", true, true, false, 23)) {
            // line 24
            echo "                ";
            echo twig_call_macro($macros["channel"], "macro_renderChannelProperty", [($context["entity"] ?? null), "oro.sales.lead.data_channel.label"], 24, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 26
        echo "
            ";
        // line 27
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.contact.label"), twig_call_macro($macros["ui"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 29
($context["entity"] ?? null), "contact", [], "any", false, false, false, 29), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "contact", [], "any", false, false, false, 29)) ? ($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "contact", [], "any", false, false, false, 29))) : ("")), "oro_contact_view"], 29, $context, $this->getSourceContext())], 27, $context, $this->getSourceContext());
        // line 30
        echo "
            ";
        // line 31
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.job_title.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "jobTitle", [], "any", false, false, false, 31)], 31, $context, $this->getSourceContext());
        echo "

            ";
        // line 33
        $context["opportunityResourceEnabled"] = $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\SalesBundle\\Entity\\Opportunity", "entities");
        // line 34
        ob_start(function () { return ''; });
        // line 35
        if ((($context["opportunityResourceEnabled"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "opportunities", [], "any", false, false, false, 35), "count", [], "any", false, false, false, 35))) {
            // line 36
            $context["opportunityViewGranted"] = $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_sales_opportunity_view");
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "opportunities", [], "any", false, false, false, 37));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["opportunity"]) {
                // line 38
                if (($context["opportunityViewGranted"] ?? null)) {
                    // line 39
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["opportunity"], "id", [], "any", false, false, false, 39)]), "html", null, true);
                    echo "\">";
                    echo twig_call_macro($macros["ui"], "macro_renderEntityViewLabel", [$context["opportunity"], "name", "oro.sales.oportunity.entity_label"], 39, $context, $this->getSourceContext());
                    echo "</a>";
                } else {
                    // line 41
                    echo twig_call_macro($macros["ui"], "macro_renderEntityViewLabel", [$context["opportunity"], "name"], 41, $context, $this->getSourceContext());
                }
                // line 43
                if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 43)) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['opportunity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        $context["opportunitiesData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 48
        echo twig_call_macro($macros["sales"], "macro_render_customer_info", [($context["entity"] ?? null)], 48, $context, $this->getSourceContext());
        echo "
            ";
        // line 49
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.company_name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "companyName", [], "any", false, false, false, 49)], 49, $context, $this->getSourceContext());
        echo "
            ";
        // line 50
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.industry.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "industry", [], "any", false, false, false, 50)], 50, $context, $this->getSourceContext());
        echo "
            ";
        // line 51
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.number_of_employees.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "numberOfEmployees", [], "any", false, false, false, 51)], 51, $context, $this->getSourceContext());
        echo "
            ";
        // line 52
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.source.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "source", [], "any", false, false, false, 52)], 52, $context, $this->getSourceContext());
        echo "

            ";
        // line 54
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 54, $context, $this->getSourceContext());
        echo "

            ";
        // line 56
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.entity_plural_label"), twig_call_macro($macros["Tag"], "macro_renderView", [($context["entity"] ?? null)], 56, $context, $this->getSourceContext())], 56, $context, $this->getSourceContext());
        echo "
        </div>";
        // line 59
        ob_start(function () { return ''; });
        // line 60
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "twitter", [], "any", false, false, false, 60) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "linkedIn", [], "any", false, false, false, 60))) {
            // line 61
            echo "<ul class=\"social-list extra-list\">
                    ";
            // line 62
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "twitter", [], "any", false, false, false, 62)) {
                // line 63
                echo "                        <li>
                            <a class=\"no-hash\" href=\"";
                // line 64
                echo twig_call_macro($macros["salesLeadInfo"], "macro_getSocialUrl", ["twitter", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "twitter", [], "any", false, false, false, 64)], 64, $context, $this->getSourceContext());
                echo "\" target=\"_blank\" title=\"Twitter\">
                                <i class=\"fa-twitter\" aria-hidden=\"true\"></i>
                                <span class=\"sr-only\">Twitter</span>
                            </a>
                        </li>
                    ";
            }
            // line 70
            echo "                    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "linkedIn", [], "any", false, false, false, 70)) {
                // line 71
                echo "                        <li>
                            <a class=\"no-hash\" href=\"";
                // line 72
                echo twig_call_macro($macros["salesLeadInfo"], "macro_getSocialUrl", ["linked_in", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "linkedIn", [], "any", false, false, false, 72)], 72, $context, $this->getSourceContext());
                echo "\" target=\"_blank\" title=\"LinkedIn\">
                                <i class=\"fa-linkedin\" aria-hidden=\"true\"></i>
                                <span class=\"sr-only\">LinkedIn</span>
                            </a>
                        </li>
                    ";
            }
            // line 78
            echo "                </ul>";
        }
        $context["socialData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 82
        echo "<div class=\"responsive-block\">
            ";
        // line 83
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.emails.label"), twig_call_macro($macros["sales"], "macro_renderCollectionWithPrimaryElement", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emails", [], "any", false, false, false, 83), true, ($context["entity"] ?? null)], 83, $context, $this->getSourceContext())], 83, $context, $this->getSourceContext());
        echo "
            ";
        // line 84
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.phones.label"), twig_call_macro($macros["sales"], "macro_renderCollectionWithPrimaryElement", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phones", [], "any", false, false, false, 84), false, ($context["entity"] ?? null)], 84, $context, $this->getSourceContext())], 84, $context, $this->getSourceContext());
        // line 85
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "website", [], "any", false, false, false, 85)) {
            // line 86
            $context["website"] = $this->extensions['Oro\Bundle\SecurityBundle\Twig\OroSecurityExtension']->stripDangerousProtocols(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "website", [], "any", false, false, false, 86));
        }
        // line 88
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.website.label"), ((array_key_exists("website", $context)) ? (twig_call_macro($macros["ui"], "macro_renderUrl", [($context["website"] ?? null), ($context["website"] ?? null), "no-hash"], 88, $context, $this->getSourceContext())) : (null))], 88, $context, $this->getSourceContext());
        echo "
            ";
        // line 89
        echo twig_call_macro($macros["ui"], "macro_renderCollapsibleHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.notes.label"), $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "notes", [], "any", false, false, false, 89)), ($context["entity"] ?? null), "notes"], 89, $context, $this->getSourceContext());
        echo "
            ";
        // line 90
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.social.label"), ($context["socialData"] ?? null)], 90, $context, $this->getSourceContext());
        echo "
            ";
        // line 91
        if (($context["opportunityResourceEnabled"] ?? null)) {
            // line 92
            echo "                ";
            echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.entity_label"), ($context["opportunitiesData"] ?? null)], 92, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 94
        echo "        </div>
    </div>
</div>
";
    }

    // line 9
    public function macro_getSocialUrl($__type__ = null, $__value__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "type" => $__type__,
            "value" => $__value__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 10
            if (((twig_slice($this->env, ($context["value"] ?? null), 0, 5) == "http:") || (twig_slice($this->env, ($context["value"] ?? null), 0, 6) == "https:"))) {
                // line 11
                echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            } else {
                // line 13
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\ContactBundle\Twig\ContactExtension']->getSocialUrl(($context["type"] ?? null), ($context["value"] ?? null)), "html", null, true);
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroSales/Lead/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 13,  269 => 11,  267 => 10,  253 => 9,  246 => 94,  240 => 92,  238 => 91,  234 => 90,  230 => 89,  226 => 88,  223 => 86,  221 => 85,  219 => 84,  215 => 83,  212 => 82,  208 => 78,  199 => 72,  196 => 71,  193 => 70,  184 => 64,  181 => 63,  179 => 62,  176 => 61,  174 => 60,  172 => 59,  168 => 56,  163 => 54,  158 => 52,  154 => 51,  150 => 50,  146 => 49,  142 => 48,  124 => 43,  121 => 41,  114 => 39,  112 => 38,  95 => 37,  93 => 36,  91 => 35,  89 => 34,  87 => 33,  82 => 31,  79 => 30,  77 => 29,  76 => 27,  73 => 26,  67 => 24,  65 => 23,  60 => 21,  56 => 20,  51 => 17,  49 => 7,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Lead/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Lead/widget/info.html.twig");
    }
}
