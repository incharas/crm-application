<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/image-preview-modal.scss */
class __TwigTemplate_c627e997074d8406dde37fa1cc42057c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$oro-modal-image-preview-blank-image-bg: linear-gradient(45deg, #e8e8e8 25%, transparent 25%),
    linear-gradient(-45deg, #e8e8e8 25%, transparent 25%),
    linear-gradient(45deg, transparent 75%, #e8e8e8 75%),
    linear-gradient(-45deg, transparent 75%, #e8e8e8 75%) !default;
\$oro-modal-image-preview-blank-image-bg-size: 12px 12px !default;
\$oro-modal-image-preview-blank-image-bg-position: 0 0, 0 6px, 6px -6px, -6px 0 !default;

\$oro-modal-image-preview-transition: transform .35s cubic-bezier(0, 0, .25, 1) 0s,
    opacity .35s cubic-bezier(0, 0, .25, 1) 0s, color .2s linear !default;

\$oro-modal-image-preview-backdrop-bg: #000 !default;
\$oro-modal-image-preview-toolbar-btn-font-suze: 24px !default;

\$oro-modal-image-preview-toolbar-btn-bg: rgba(#000, .45) !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/image-preview-modal.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/image-preview-modal.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/image-preview-modal.scss");
    }
}
