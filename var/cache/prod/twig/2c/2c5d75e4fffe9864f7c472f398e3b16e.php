<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/pin_tabs_list.html.twig */
class __TwigTemplate_1c69635bdb4501661a03b6d7a7114577 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 2
            $context["pinHelpTogglerId"] = uniqid("pin-help-dropdown-");
            // line 3
            $context["showMoreTogglerId"] = uniqid("show-more-dropdown-");
            // line 4
            echo "<div class=\"list-bar-wrapper\" id=\"pinbar\">
    <div class=\"pin-bar-empty dropdown\">
        <a href=\"#\" id=\"";
            // line 6
            echo twig_escape_filter($this->env, ($context["pinHelpTogglerId"] ?? null), "html", null, true);
            echo "\" role=\"button\"
           title=\"";
            // line 7
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menu.how_to_use_pinbar"), "html", null, true);
            echo "\"
           data-toggle=\"dropdown\"
           aria-haspopup=\"true\" aria-expanded=\"false\"
        >";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menu.how_to_use_pinbar"), "html", null, true);
            echo "</a>
        <div class=\"dropdown-menu\" aria-labelledby=\"";
            // line 11
            echo twig_escape_filter($this->env, ($context["pinHelpTogglerId"] ?? null), "html", null, true);
            echo "\">
            <button class=\"btn-link btn-close fa-close\" type=\"button\" data-role=\"close\" aria-hidden=\"true\"></button>
            <div class=\"dropdown-content\">";
            // line 13
            echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.pins.help_message", ["%pinIcon%" => "<span class=\"fa-thumb-tack\"></span>"]);
            echo "</div>
            <button class=\"btn btn-success no-hash\" type=\"button\" title=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ok, got it"), "html", null, true);
            echo "\" ><span class=\"fa-check\"></span>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Ok, got it"), "html", null, true);
            echo "
            </button>
        </div>
    </div>
    <div class=\"list-bar\">
        <ul></ul>
    </div>
    <div class=\"show-more dropdown\"><span role=\"button\" id=\"";
            // line 21
            echo twig_escape_filter($this->env, ($context["showMoreTogglerId"] ?? null), "html", null, true);
            echo "\" class=\"dropdown-toggle dropdown-toggle--no-caret\" data-toggle=\"dropdown\"
                                          aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"";
            // line 22
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.pins.more_tabs.label"), "html", null, true);
            echo "\"
        ><span class=\"fa-ellipsis-v\" aria-hidden=\"true\"></span></span><div class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"";
            // line 23
            echo twig_escape_filter($this->env, ($context["showMoreTogglerId"] ?? null), "html", null, true);
            echo "\"
        ><ul class=\"dropdown-menu-items\" data-role=\"pin-bar-dropdown-items\"></ul></div></div>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/pin_tabs_list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 23,  86 => 22,  82 => 21,  70 => 14,  66 => 13,  61 => 11,  57 => 10,  51 => 7,  47 => 6,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/pin_tabs_list.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/pin_tabs_list.html.twig");
    }
}
