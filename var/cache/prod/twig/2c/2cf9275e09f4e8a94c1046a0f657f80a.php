<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/skeleton/skeleton.scss */
class __TwigTemplate_f9543d4e0427bc734a97cc8e469cc074 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@import './functions';

// Mixin for generate loading skeleton
// @param: List \$particles
// @example
// @include skeleton(
//     skeleton-rect(\$color: #800, \$width: 129px, \$height: 24px, \$x: 0, \$y: 0, \$radius: 3px),
//     skeleton-ellipse(\$color: #800, \$width: 129px, \$height: 24px, \$x: 0, \$y: 0),
//     (
//         skeleton-rect(\$color: #800, \$width: 129px, \$height: 24px, \$x: 0, \$y: 0, \$radius: 3px),
//         skeleton-ellipse(\$color: #800, \$width: 129px, \$height: 24px, \$x: 0, \$y: 0),
//     ),
// );
@mixin skeleton(\$particles...) {
    \$list-normalized: skeleton-normalize(\$particles...);

    background-image: skeleton-image(\$list-normalized...);
    background-size: skeleton-size(\$list-normalized...);
    background-position: skeleton-position(\$list-normalized...);
    background-repeat: no-repeat;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/skeleton/skeleton.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/skeleton/skeleton.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/skeleton/skeleton.scss");
    }
}
