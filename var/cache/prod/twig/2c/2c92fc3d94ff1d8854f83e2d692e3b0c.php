<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Form/fields.html.twig */
class __TwigTemplate_28d743549abdbbff8a97be37dffecb3e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_workflow_transition_row' => [$this, 'block_oro_workflow_transition_row'],
            '_oro_workflow_definition_form_label_widget' => [$this, 'block__oro_workflow_definition_form_label_widget'],
            'oro_workflow_replacement_row' => [$this, 'block_oro_workflow_replacement_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_workflow_transition_row', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('_oro_workflow_definition_form_label_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('oro_workflow_replacement_row', $context, $blocks);
    }

    // line 1
    public function block_oro_workflow_transition_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <fieldset class=\"form-horizontal\">
        ";
        // line 3
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
    </fieldset>

    <div class=\"widget-actions\">
        <button type=\"reset\" class=\"btn\">";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
        <button type=\"submit\" class=\"btn btn-success\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Submit"), "html", null, true);
        echo "</button>
    </div>
";
    }

    // line 12
    public function block__oro_workflow_definition_form_label_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    <span id=\"workflow_translate_link_label\"></span>
";
    }

    // line 17
    public function block_oro_workflow_replacement_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 18), "errors", [], "any", false, false, false, 18)) > 0)) {
            // line 19
            echo "        <div class=\"alert alert-error\" role=\"alert\">
            ";
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
        </div>
    ";
        }
        // line 23
        echo "
    <div class=\"alert alert-warning workflow-deactivation-message\" role=\"alert\">
        <div class=\"message\">
            ";
        // line 26
        if (($context["workflowsToDeactivation"] ?? null)) {
            // line 27
            echo "                <p>
                    ";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.form.workflow_to_deactivation_message"), "html", null, true);
            echo "
                    <ul>
                        ";
            // line 30
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["workflowsToDeactivation"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["workflowToDeactivation"]) {
                // line 31
                echo "                            <li>
                                <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_view", ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflowToDeactivation"], "name", [], "any", false, false, false, 32)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflowToDeactivation"], "label", [], "any", false, false, false, 32), [], "workflows"), "html", null, true);
                echo "</a>
                            </li>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['workflowToDeactivation'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 35
            echo "                    </ul>
                </p>
            ";
        }
        // line 38
        echo "            <p>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.form.replace_message"), "html", null, true);
        echo "</p>
        </div>
    </div>

    ";
        // line 42
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 43
($context["app"] ?? null), "request", [], "any", false, false, false, 43), "uri", [], "any", false, false, false, 43), "attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 45
($context["form"] ?? null), "vars", [], "any", false, false, false, 45), "id", [], "any", false, false, false, 45), "class" => "form-dialog", "data-collect" => "true"]]);
        // line 49
        echo "
        <fieldset class=\"form-horizontal\">
            ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
        </fieldset>

        <div class=\"hidden\">
            ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
        </div>

        <div class=\"widget-actions\">
            <button type=\"reset\" class=\"btn\">";
        // line 59
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
            <button type=\"button\" data-action-name=\"activate\" class=\"btn btn-success\">";
        // line 60
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.datagrid.activate"), "html", null, true);
        echo "</button>
        </div>
    ";
        // line 62
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
    ";
        // line 63
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  188 => 63,  184 => 62,  179 => 60,  175 => 59,  168 => 55,  161 => 51,  157 => 49,  155 => 45,  154 => 43,  153 => 42,  145 => 38,  140 => 35,  129 => 32,  126 => 31,  122 => 30,  117 => 28,  114 => 27,  112 => 26,  107 => 23,  101 => 20,  98 => 19,  95 => 18,  91 => 17,  83 => 13,  79 => 12,  72 => 8,  68 => 7,  61 => 3,  58 => 2,  54 => 1,  50 => 17,  47 => 16,  45 => 12,  42 => 11,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Form/fields.html.twig");
    }
}
