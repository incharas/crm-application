<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Form/fields.html.twig */
class __TwigTemplate_f4a591e69310f80f5b75fa9770f0add1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_sales_opportunity_widget' => [$this, 'block_oro_sales_opportunity_widget'],
            'oro_sales_lead_widget' => [$this, 'block_oro_sales_lead_widget'],
            'oro_sales_opportunity_status_enum_value_widget' => [$this, 'block_oro_sales_opportunity_status_enum_value_widget'],
            'oro_sales_lead_address_widget' => [$this, 'block_oro_sales_lead_address_widget'],
            'oro_sales_lead_address_rows' => [$this, 'block_oro_sales_lead_address_rows'],
            '_oro_sales_lead_form_address_collection_widget' => [$this, 'block__oro_sales_lead_form_address_collection_widget'],
            'oro_sales_customer_widget' => [$this, 'block_oro_sales_customer_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_sales_opportunity_widget', $context, $blocks);
        // line 39
        echo "
";
        // line 40
        $this->displayBlock('oro_sales_lead_widget', $context, $blocks);
        // line 93
        echo "
";
        // line 94
        $this->displayBlock('oro_sales_opportunity_status_enum_value_widget', $context, $blocks);
        // line 119
        echo "
";
        // line 120
        $this->displayBlock('oro_sales_lead_address_widget', $context, $blocks);
        // line 129
        echo "
";
        // line 130
        $this->displayBlock('oro_sales_lead_address_rows', $context, $blocks);
        // line 149
        echo "
";
        // line 150
        $this->displayBlock('_oro_sales_lead_form_address_collection_widget', $context, $blocks);
        // line 157
        echo "
";
        // line 183
        echo "
";
        // line 184
        $this->displayBlock('oro_sales_customer_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_sales_opportunity_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <div class=\"row-fluid\">
        <fieldset class=\"form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 5
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", true, true, false, 5)) {
            // line 6
            echo "                    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 6), 'row');
            echo "
                ";
        }
        // line 8
        echo "                ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 8), 'row');
        echo "
                ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "contact", [], "any", false, false, false, 9), 'row');
        echo "
                ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "customerAssociation", [], "any", false, false, false, 10), 'row');
        echo "
                ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "probability", [], "any", false, false, false, 11), 'row');
        echo "
                ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "budgetAmount", [], "any", false, false, false, 12), 'row');
        echo "
                ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "customerNeed", [], "any", false, false, false, 13), 'row');
        echo "
                ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "proposedSolution", [], "any", false, false, false, 14), 'row');
        echo "
                ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "closeReason", [], "any", false, false, false, 15), 'row');
        echo "
                ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "closeRevenue", [], "any", false, false, false, 16), 'row');
        echo "
                ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "closeDate", [], "any", false, false, false, 17), 'row');
        echo "
                ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "notes", [], "any", false, false, false, 18), 'row');
        echo "

                ";
        // line 20
        $context["additionalData"] = [];
        // line 21
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 21));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 22
            echo "                    ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, true, false, 22), "extra_field", [], "any", true, true, false, 22) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 22), "extra_field", [], "any", false, false, false, 22))) {
                // line 23
                echo "                        ";
                $context["additionalData"] = twig_array_merge(($context["additionalData"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'row')]);
                // line 24
                echo "                    ";
            }
            // line 25
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "                ";
        if ( !twig_test_empty(($context["additionalData"] ?? null))) {
            // line 27
            echo "                    <h5 class=\"user-fieldset\">
                        <span>";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional"), "html", null, true);
            echo "</span>
                    </h5>

                    ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["additionalData"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                // line 32
                echo "                        ";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "                ";
        }
        // line 35
        echo "            </div>
        </fieldset>
    </div>
";
    }

    // line 40
    public function block_oro_sales_lead_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    <div class=\"row-fluid row-fluid-divider\">
        <fieldset class=\"form-horizontal\">
            <div class=\"responsive-block\">
                <h5 class=\"user-fieldset\">
                    <span>";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.details.label"), "html", null, true);
        echo "</span>
                </h5>
                ";
        // line 47
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", true, true, false, 47)) {
            // line 48
            echo "                    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 48), 'row');
            echo "
                ";
        }
        // line 50
        echo "                ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 50), 'row');
        echo "
                ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "namePrefix", [], "any", false, false, false, 51), 'row');
        echo "
                ";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "firstName", [], "any", false, false, false, 52), 'row');
        echo "
                ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "middleName", [], "any", false, false, false, 53), 'row');
        echo "
                ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "lastName", [], "any", false, false, false, 54), 'row');
        echo "
                ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "nameSuffix", [], "any", false, false, false, 55), 'row');
        echo "
                ";
        // line 56
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "contact", [], "any", false, false, false, 56), 'row');
        echo "
                ";
        // line 57
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "jobTitle", [], "any", false, false, false, 57), 'row');
        echo "
                ";
        // line 58
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phones", [], "any", false, false, false, 58), 'row');
        echo "
                ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emails", [], "any", false, false, false, 59), 'row');
        echo "
                ";
        // line 60
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "customerAssociation", [], "any", false, false, false, 60), 'row');
        echo "
                ";
        // line 61
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "companyName", [], "any", false, false, false, 61), 'row');
        echo "
                ";
        // line 62
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "website", [], "any", false, false, false, 62), 'row');
        echo "
                ";
        // line 63
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "numberOfEmployees", [], "any", false, false, false, 63), 'row');
        echo "
                ";
        // line 64
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "industry", [], "any", false, false, false, 64), 'row');
        echo "
                ";
        // line 65
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "source", [], "any", false, false, false, 65), 'row');
        echo "
                ";
        // line 66
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "notes", [], "any", false, false, false, 66), 'row');
        echo "

                ";
        // line 68
        $context["additionalData"] = [];
        // line 69
        echo "                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 69));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 70
            echo "                    ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, true, false, 70), "extra_field", [], "any", true, true, false, 70) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 70), "extra_field", [], "any", false, false, false, 70))) {
                // line 71
                echo "                        ";
                $context["additionalData"] = twig_array_merge(($context["additionalData"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'row')]);
                // line 72
                echo "                    ";
            }
            // line 73
            echo "                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 74
        echo "                ";
        if ( !twig_test_empty(($context["additionalData"] ?? null))) {
            // line 75
            echo "                    <h5 class=\"user-fieldset\">
                        <span>";
            // line 76
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional"), "html", null, true);
            echo "</span>
                    </h5>

                    ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["additionalData"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                // line 80
                echo "                        ";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 82
            echo "                ";
        }
        // line 83
        echo "            </div>
            <div class=\"responsive-cell\">
                <h5 class=\"user-fieldset\">
                    <span>";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.addresses.label"), "html", null, true);
        echo "</span>
                </h5>
                ";
        // line 88
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "addresses", [], "any", false, false, false, 88), 'widget');
        echo "
            </div>
        </fieldset>
    </div>
";
    }

    // line 94
    public function block_oro_sales_opportunity_status_enum_value_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 95
        echo "    <div class=\"float-holder ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 95), "vars", [], "any", false, false, false, 95), "errors", [], "any", false, false, false, 95)) > 0)) {
            echo " validation-error";
        }
        echo "\">
        <div class=\"input-append input-append-sortable collection-element-primary\">
            ";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 97), 'widget', ["disabled" => ($context["disabled"] ?? null)]);
        echo "
            ";
        // line 98
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "probability", [], "any", false, false, false, 98), 'widget', ["disabled" => (        // line 99
($context["disabled"] ?? null) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "probability", [], "any", false, false, false, 99), "vars", [], "any", false, false, false, 99), "disabled", [], "any", false, false, false, 99)), "attr" => ["class" => "add-on-input", "title" => "oro.sales.system_configuration.fields.opportunity_status_probabilities.probability.tooltip"]]);
        // line 104
        echo "
            <span class=\"add-on ui-sortable-handle";
        // line 105
        if (($context["disabled"] ?? null)) {
            echo " disabled";
        }
        echo "\"
                  data-name=\"sortable-handle\"
                  title=\"";
        // line 107
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.enum_options.priority.tooltip"), "html", null, true);
        echo "\">
                <i class=\"fa-arrows-v ";
        // line 108
        if (($context["disabled"] ?? null)) {
            echo " disabled";
        }
        echo "\"></i>
                ";
        // line 109
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "priority", [], "any", false, false, false, 109), 'widget', ["disabled" => ($context["disabled"] ?? null)]);
        echo "
            </span>
            <label class=\"add-on";
        // line 111
        if (($context["disabled"] ?? null)) {
            echo " disabled";
        }
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.enum_options.default.tooltip"), "html", null, true);
        echo "\">
                ";
        // line 112
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "is_default", [], "any", false, false, false, 112), 'widget', ["disabled" => ($context["disabled"] ?? null)]);
        echo "
            </label>
        </div>
        ";
        // line 115
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 115), 'errors');
        echo "
    </div>
    ";
        // line 117
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
    }

    // line 120
    public function block_oro_sales_lead_address_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 121
        echo "    ";
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 121))) {
            // line 122
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 123
            $this->displayBlock("oro_sales_lead_address_rows", $context, $blocks);
            echo "
        </div>
    ";
        } else {
            // line 126
            echo "        ";
            $this->displayBlock("oro_sales_lead_address_rows", $context, $blocks);
            echo "
    ";
        }
    }

    // line 130
    public function block_oro_sales_lead_address_rows($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 131
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "id", [], "any", false, false, false, 131), 'row');
        echo "
    ";
        // line 132
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "primary", [], "any", false, false, false, 132), 'row', ["label" => "oro.sales.leadaddress.primary.label"]);
        echo "
    ";
        // line 133
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 133), 'row', ["label" => "oro.sales.leadaddress.label.label"]);
        echo "
    ";
        // line 134
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "namePrefix", [], "any", false, false, false, 134), 'row', ["label" => "oro.sales.leadaddress.name_prefix.label"]);
        echo "
    ";
        // line 135
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "firstName", [], "any", false, false, false, 135), 'row', ["label" => "oro.sales.leadaddress.first_name.label"]);
        echo "
    ";
        // line 136
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "middleName", [], "any", false, false, false, 136), 'row', ["label" => "oro.sales.leadaddress.middle_name.label"]);
        echo "
    ";
        // line 137
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "lastName", [], "any", false, false, false, 137), 'row', ["label" => "oro.sales.leadaddress.last_name.label"]);
        echo "
    ";
        // line 138
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "nameSuffix", [], "any", false, false, false, 138), 'row', ["label" => "oro.sales.leadaddress.name_suffix.label"]);
        echo "
    ";
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", false, false, false, 139), 'row', ["label" => "oro.sales.leadaddress.organization.label"]);
        echo "
    ";
        // line 140
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "country", [], "any", false, false, false, 140), 'row', ["label" => "oro.sales.leadaddress.country.label"]);
        echo "
    ";
        // line 141
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "street", [], "any", false, false, false, 141), 'row', ["label" => "oro.sales.leadaddress.street.label"]);
        echo "
    ";
        // line 142
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "street2", [], "any", false, false, false, 142), 'row', ["label" => "oro.sales.leadaddress.street2.label"]);
        echo "
    ";
        // line 143
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "city", [], "any", false, false, false, 143), 'row', ["label" => "oro.sales.leadaddress.city.label"]);
        echo "
    ";
        // line 144
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "region_text", [], "any", false, false, false, 144), 'row', ["label" => "oro.sales.leadaddress.region_text.label"]);
        echo "
    ";
        // line 145
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "region", [], "any", false, false, false, 145), 'row', ["label" => "oro.sales.leadaddress.region.label"]);
        echo "
    ";
        // line 146
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "postalCode", [], "any", false, false, false, 146), 'row', ["label" => "oro.sales.leadaddress.postal_code.label"]);
        echo "
    ";
        // line 147
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
    }

    // line 150
    public function block__oro_sales_lead_form_address_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 151
        echo "    ";
        $macros["fields"] = $this;
        // line 152
        echo "
    ";
        // line 153
        $this->displayBlock("oro_address_collection_widget", $context, $blocks);
        echo "
    ";
        // line 154
        $context["id"] = (($context["id"] ?? null) . "_collection");
        // line 155
        echo "    ";
        echo twig_call_macro($macros["fields"], "macro_oro_adddress_collection_prefill_names", [$context], 155, $context, $this->getSourceContext());
        echo "
";
    }

    // line 184
    public function block_oro_sales_customer_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 185
        echo "    ";
        if (( !($context["hasGridData"] ?? null) &&  !($context["createCustomersData"] ?? null))) {
            // line 186
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
    ";
        } else {
            // line 188
            echo "        <div id=\"";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "-el\"
             ";
            // line 189
            if (twig_length_filter($this->env, ($context["createCustomersData"] ?? null))) {
                echo "class=\"entity-create-or-select-container entity-create-multi-enabled\"";
            }
            // line 190
            echo "             data-page-component-module=\"orosales/js/app/components/customer-component\"
             data-page-component-options=\"";
            // line 191
            echo twig_escape_filter($this->env, json_encode(["inputSelector" => ("#" .             // line 192
($context["id"] ?? null)), "customerSelector" => ".dropdown-menu li"]), "html", null, true);
            // line 194
            echo "\">
            <div class=\"input-append\">
                ";
            // line 196
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
                ";
            // line 197
            if (($context["hasGridData"] ?? null)) {
                // line 198
                echo "                    ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Form/fields.html.twig", 198)->unwrap();
                // line 199
                echo "                    ";
                echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_customer_grid_dialog", ["entityClass" =>                 // line 202
($context["parentClass"] ?? null)]), "aCss" => "btn btn-icon btn-square-default entity-select-btn", "iCss" => "fa-bars", "ariaLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.entity_select", ["%name%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(                // line 206
($context["label"] ?? null), [], ($context["translation_domain"] ?? null))]), "dataAttributes" => ["purpose" => "open-dialog-widget"], "widget" => ["type" => "dialog", "multiple" => true, "options" => ["alias" => "customer-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(                // line 214
($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "allowMaximize" => true, "allowMinimize" => true, "modal" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded", "height" => 600]]]]], 199, $context, $this->getSourceContext());
                // line 226
                echo "
                ";
            }
            // line 228
            echo "                ";
            if (twig_length_filter($this->env, ($context["createCustomersData"] ?? null))) {
                // line 229
                echo "                    ";
                $context["togglerId"] = uniqid("dropdown-");
                // line 230
                echo "                    <div class=\"dropdown btn-group entity-create-dropdown\">
                        <button id=\"";
                // line 231
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\" type=\"button\" data-toggle=\"dropdown\"
                                class=\"dropdown-toggle btn btn-icon btn-square-default entity-create-btn\"
                                aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"";
                // line 233
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.dropdown_option_aria_label", [], ($context["translation_domain"] ?? null)), "html", null, true);
                echo "\">
                            <span class=\"fa-plus\" aria-hidden=\"true\"></span>
                        </button>
                        <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"";
                // line 236
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\">
                            ";
                // line 237
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["createCustomersData"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["customer"]) {
                    // line 238
                    echo "                                <li>
                                    <button type=\"button\" data-customer=\"";
                    // line 239
                    echo twig_escape_filter($this->env, json_encode($context["customer"]), "html", null, true);
                    echo "\">
                                        <span class=\"";
                    // line 240
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["customer"], "icon", [], "any", false, false, false, 240), "html", null, true);
                    echo "\" aria-hidden=\"true\"></span>
                                        ";
                    // line 241
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["customer"], "label", [], "any", false, false, false, 241)), "html", null, true);
                    echo "
                                    </button>
                                </li>
                            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 245
                echo "                        </ul>
                    </div>
                ";
            }
            // line 248
            echo "            </div>
        </div>
    ";
        }
    }

    // line 158
    public function macro_oro_adddress_collection_prefill_names($__context__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "context" => $__context__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 159
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "form", [], "any", false, true, false, 159), "parent", [], "any", true, true, false, 159)) {
                // line 160
                echo "        ";
                $context["parentId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "form", [], "any", false, false, false, 160), "parent", [], "any", false, false, false, 160), "vars", [], "any", false, false, false, 160), "id", [], "any", false, false, false, 160);
                // line 161
                echo "        ";
                $context["parentName"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "form", [], "any", false, false, false, 161), "parent", [], "any", false, false, false, 161), "vars", [], "any", false, false, false, 161), "full_name", [], "any", false, false, false, 161);
                // line 162
                echo "        <script>
            loadModules(['jquery'],
            function(\$){
                \$(function() {
                    var container = \$('#";
                // line 166
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "id", [], "any", false, false, false, 166), "html", null, true);
                echo "');
                    var parentContainer = \$('#";
                // line 167
                echo twig_escape_filter($this->env, ($context["parentId"] ?? null), "html", null, true);
                echo "');
                    var nameFields = ['firstName', 'lastName', 'namePrefix', 'middleName', 'nameSuffix'];
                    container.on('content:changed', function() {
                        nameFields.forEach(function (field, index) {
                            container.find('[name\$=\"[' + field +']\"]').each(function (idx, el) {
                                if (!\$(el).val()) {
                                    \$(el).val(parentContainer.find('[name\$=\"";
                // line 173
                echo twig_escape_filter($this->env, ($context["parentName"] ?? null), "html", null, true);
                echo "[' + field +']\"]').val());
                                }
                            });
                        })
                    })
                });
            });
        </script>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroSales/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  709 => 173,  700 => 167,  696 => 166,  690 => 162,  687 => 161,  684 => 160,  681 => 159,  668 => 158,  661 => 248,  656 => 245,  646 => 241,  642 => 240,  638 => 239,  635 => 238,  631 => 237,  627 => 236,  621 => 233,  616 => 231,  613 => 230,  610 => 229,  607 => 228,  603 => 226,  601 => 214,  600 => 206,  599 => 202,  597 => 199,  594 => 198,  592 => 197,  588 => 196,  584 => 194,  582 => 192,  581 => 191,  578 => 190,  574 => 189,  569 => 188,  563 => 186,  560 => 185,  556 => 184,  549 => 155,  547 => 154,  543 => 153,  540 => 152,  537 => 151,  533 => 150,  527 => 147,  523 => 146,  519 => 145,  515 => 144,  511 => 143,  507 => 142,  503 => 141,  499 => 140,  495 => 139,  491 => 138,  487 => 137,  483 => 136,  479 => 135,  475 => 134,  471 => 133,  467 => 132,  462 => 131,  458 => 130,  450 => 126,  444 => 123,  439 => 122,  436 => 121,  432 => 120,  426 => 117,  421 => 115,  415 => 112,  407 => 111,  402 => 109,  396 => 108,  392 => 107,  385 => 105,  382 => 104,  380 => 99,  379 => 98,  375 => 97,  367 => 95,  363 => 94,  354 => 88,  349 => 86,  344 => 83,  341 => 82,  332 => 80,  328 => 79,  322 => 76,  319 => 75,  316 => 74,  310 => 73,  307 => 72,  304 => 71,  301 => 70,  296 => 69,  294 => 68,  289 => 66,  285 => 65,  281 => 64,  277 => 63,  273 => 62,  269 => 61,  265 => 60,  261 => 59,  257 => 58,  253 => 57,  249 => 56,  245 => 55,  241 => 54,  237 => 53,  233 => 52,  229 => 51,  224 => 50,  218 => 48,  216 => 47,  211 => 45,  205 => 41,  201 => 40,  194 => 35,  191 => 34,  182 => 32,  178 => 31,  172 => 28,  169 => 27,  166 => 26,  160 => 25,  157 => 24,  154 => 23,  151 => 22,  146 => 21,  144 => 20,  139 => 18,  135 => 17,  131 => 16,  127 => 15,  123 => 14,  119 => 13,  115 => 12,  111 => 11,  107 => 10,  103 => 9,  98 => 8,  92 => 6,  90 => 5,  85 => 2,  81 => 1,  77 => 184,  74 => 183,  71 => 157,  69 => 150,  66 => 149,  64 => 130,  61 => 129,  59 => 120,  56 => 119,  54 => 94,  51 => 93,  49 => 40,  46 => 39,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Form/fields.html.twig");
    }
}
