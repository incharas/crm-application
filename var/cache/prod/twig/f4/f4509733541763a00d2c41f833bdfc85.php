<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/app-loading-bar-component.js */
class __TwigTemplate_6a40849064fcdb7d95cb3c491ce0378f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const mediator = require('oroui/js/mediator');
    const BaseComponent = require('oroui/js/app/components/base/component');
    const LoadingBarView = require('oroui/js/app/views/loading-bar-view');

    const AppLoadingBarComponent = BaseComponent.extend({
        listen: {
            'page:beforeChange mediator': 'showLoading',
            'page:afterChange mediator': 'hideLoading'
        },

        /**
         * @inheritdoc
         */
        constructor: function AppLoadingBarComponent(options) {
            AppLoadingBarComponent.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this.initView(options);

            mediator.setHandler('showLoadingBar', this.showLoading, this);
            mediator.setHandler('hideLoadingBar', this.hideLoading, this);

            if (options.showOnStartup) {
                this.showLoading();
            }

            AppLoadingBarComponent.__super__.initialize.call(this, options);
        },

        /**
         * Initializes loading bar view
         *
         * @param {Object} options
         */
        initView: function(options) {
            const viewOptions = _.defaults({}, options.viewOptions, {
                ajaxLoading: true
            });
            this.view = new LoadingBarView(viewOptions);
        },

        /**
         * Shows loading bar
         */
        showLoading: function() {
            this.view.showLoader();
        },

        /**
         * Hides loading bar
         */
        hideLoading: function() {
            this.view.hideLoader();
        }
    });

    return AppLoadingBarComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/app-loading-bar-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/app-loading-bar-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/app-loading-bar-component.js");
    }
}
