<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/templates/select2-tree-autocomplete-result.html */
class __TwigTemplate_d42ef89fdf23eb1f6934edf96d42cd1f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (typeof treePath === 'undefined') { %>
    <%= getLabel(obj, highlight) %>
<% } else if (treePath.length > 3) { %>
   <%= getLabel(treePath[0], highlight) %>
   <span class=\"select2-tree-result-item-divider\">/</span>
   <span class=\"select2-tree-result-item-more-path\" title=\"<%  for (var i = 1; i < treePath.length - 1; i++) { %><%= getLabel(treePath[i], highlight) %><% if (i !== treePath.length - 2) { %> / <% } %><% } %>\">...</span>
   <span class=\"select2-tree-result-item-divider\">/</span>
   <%= getLabel(treePath[treePath.length - 1], highlight) %>
<% } else { %>
   <%  for (var i = 0; i < treePath.length; i++) { %>
       <%= getLabel(treePath[i], highlight) %>
       <% if (i !== treePath.length - 1) { %>
           <span class=\"select2-tree-result-item-divider\">/</span>
       <% } %>
   <% } %>
<% } %>
<% if (id === null) { %>
    <span class=\"select2__result-entry-info\"><%- _.__(newKey) %></span>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/templates/select2-tree-autocomplete-result.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/templates/select2-tree-autocomplete-result.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/templates/select2-tree-autocomplete-result.html");
    }
}
