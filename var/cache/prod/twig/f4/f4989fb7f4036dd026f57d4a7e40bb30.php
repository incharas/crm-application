<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCron/Schedule/Datagrid/definition.html.twig */
class __TwigTemplate_c97b5a79c64bb7484325ada9a3ac5cf3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<b>";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "definition"], "method", false, false, false, 1), "html", null, true);
        echo "</b>
";
    }

    public function getTemplateName()
    {
        return "@OroCron/Schedule/Datagrid/definition.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCron/Schedule/Datagrid/definition.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CronBundle/Resources/views/Schedule/Datagrid/definition.html.twig");
    }
}
