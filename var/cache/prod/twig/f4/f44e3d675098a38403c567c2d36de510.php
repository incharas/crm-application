<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/variables/datepicker-config.scss */
class __TwigTemplate_4d7731c723939fbd803bc21c17cba6fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

\$datepicker-box-position: relative !default;

\$datepicker-box-icon-position: absolute !default;
\$datepicker-box-icon-left: 12px !default;
\$datepicker-box-icon-top: 9px !default;
\$datepicker-box-icon-z-index: z('base') !default;

\$datepicker-box-icon-font-size: \$base-font-size--m !default;
\$datepicker-box-icon-line-height: 1 !default;
\$datepicker-box-icon-color: get-color('additional', 'middle') !default;

\$datepicker-box-icon-pointer-events: none !default;
\$datepicker-input-display: block !default;
\$datepicker-input-max-width: 140px !default;
\$datepicker-input-padding-left: 39px !default;
\$datepicker-input-position: relative !default;

\$datepicker-input-form-mode-icon-top: 35px !default;
\$datepicker-box-form-mode-datepicker-input-max-width: none !default;
\$datepicker-box-form-mode-datepicker-input-width: 100% !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/variables/datepicker-config.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/variables/datepicker-config.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/variables/datepicker-config.scss");
    }
}
