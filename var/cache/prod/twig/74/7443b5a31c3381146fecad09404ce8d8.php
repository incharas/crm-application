<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/macros.html.twig */
class __TwigTemplate_7d6246de1c52e751567d70c3c925150b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "
";
    }

    // line 1
    public function macro_renderOrganizer($__user__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "user" => $__user__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/macros.html.twig", 2)->unwrap();
            // line 3
            echo "
    <div class=\"calendar-event-organizer\">
        ";
            // line 5
            if (( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "organizerUser", [], "any", false, false, false, 5)) &&  !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "organizerDisplayName", [], "any", false, false, false, 5)))) {
                // line 6
                echo "            ";
                $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroCalendar/macros.html.twig", 6)->display(twig_array_merge($context, ["sources" => (($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 7
($context["user"] ?? null), "organizerUser", [], "any", false, false, false, 7), "avatar", [], "any", false, false, false, 7), "avatar_xsmall")) ? ($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "organizerUser", [], "any", false, false, false, 7), "avatar", [], "any", false, false, false, 7), "avatar_xsmall")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroui/img/avatar-xsmall.png")))]));
                // line 9
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_link", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 10
($context["user"] ?? null), "organizerUser", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)]), "label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 11
($context["user"] ?? null), "organizerDisplayName", [], "any", false, false, false, 11)]], 9, $context, $this->getSourceContext());
                // line 12
                echo "
        ";
            } elseif (( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["user"] ?? null), "organizerEmail", [], "any", false, false, false, 13)) &&  !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "organizerDisplayName", [], "any", false, false, false, 13)))) {
                // line 14
                echo "            <img src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroui/img/avatar-xsmall.png"), "html", null, true);
                echo "\" />
            ";
                // line 15
                echo twig_escape_filter($this->env, (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "organizerDisplayName", [], "any", false, false, false, 15) . " (") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "organizerEmail", [], "any", false, false, false, 15)) . ")"), "html", null, true);
                echo "
        ";
            } else {
                // line 17
                echo "            ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.na.label"), "html", null, true);
                echo "
        ";
            }
            // line 19
            echo "    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 22
    public function macro_briefDateCell($__theDate__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "theDate" => $__theDate__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 23
            echo "    <span class=\"day-of-week\">";
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["theDate"] ?? null), "D", $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()), "html", null, true);
            echo "</span>
    <span class=\"day-of-month\">";
            // line 24
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, ($context["theDate"] ?? null), "j", $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()), "html", null, true);
            echo "</span>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroCalendar/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 24,  117 => 23,  104 => 22,  94 => 19,  88 => 17,  83 => 15,  78 => 14,  76 => 13,  73 => 12,  71 => 11,  70 => 10,  68 => 9,  66 => 7,  64 => 6,  62 => 5,  58 => 3,  55 => 2,  42 => 1,  37 => 21,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/macros.html.twig");
    }
}
