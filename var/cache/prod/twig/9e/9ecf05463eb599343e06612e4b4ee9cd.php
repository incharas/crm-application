<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/scrollspy.scss */
class __TwigTemplate_045843219edd16f6884445b979095e81 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.scrollspy {
    .accordion-group {
        margin-bottom: \$scrollspy-mobile-accordion-group-offset;

        &:last-child {
            margin-bottom: 0;
        }
    }

    .accordion-toggle {
        display: flex;
        align-items: center;
        padding: \$scrollspy-mobile-toggle-offset;

        border-radius: \$scrollspy-mobile-border-radius;

        @include fa-icon(\$fa-var-angle-down, before, true) {
            width: \$scrollspy-mobile-toggle-icon-width;
            margin: \$scrollspy-mobile-toggle-icon-offset;

            font-size: \$scrollspy-mobile-toggle-icon-font-size;
            font-weight: font-weight('light');

            color: \$scrollspy-mobile-toggle-icon-color;
            text-align: \$scrollspy-mobile-toggle-icon-text-align;

            vertical-align: middle;
        }

        &.collapsed {
            @include fa-icon(var(--fa-var-angle-right), before, false, true);
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/scrollspy.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/scrollspy.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/scrollspy.scss");
    }
}
