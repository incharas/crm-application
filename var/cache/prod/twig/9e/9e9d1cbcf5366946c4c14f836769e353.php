<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Form/fields.html.twig */
class __TwigTemplate_5cdd4c19bf0257fc9f331e04e351bd2c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_email_configuration_oro_email___smtp_settings_password_row' => [$this, 'block__email_configuration_oro_email___smtp_settings_password_row'],
            '_email_configuration_oro_email___attachment_sync_max_size_value_widget' => [$this, 'block__email_configuration_oro_email___attachment_sync_max_size_value_widget'],
            '_oro_email_autoresponserule_template_new_entity_translations_widget' => [$this, 'block__oro_email_autoresponserule_template_new_entity_translations_widget'],
            'oro_email_template_list_row' => [$this, 'block_oro_email_template_list_row'],
            'oro_email_link_to_scope_row' => [$this, 'block_oro_email_link_to_scope_row'],
            'oro_email_attachments_row' => [$this, 'block_oro_email_attachments_row'],
            'oro_email_emailtemplate_localizations_widget' => [$this, 'block_oro_email_emailtemplate_localizations_widget'],
            'oro_email_email_folder_tree_row' => [$this, 'block_oro_email_email_folder_tree_row'],
            'oro_email_email_folder_tree_widget' => [$this, 'block_oro_email_email_folder_tree_widget'],
            'oro_email_mailbox_grid_row' => [$this, 'block_oro_email_mailbox_grid_row'],
            'oro_email_mailbox_grid_label' => [$this, 'block_oro_email_mailbox_grid_label'],
            'oro_email_mailbox_grid_widget' => [$this, 'block_oro_email_mailbox_grid_widget'],
            'oro_email_mailbox_widget' => [$this, 'block_oro_email_mailbox_widget'],
            'oro_email_emailtemplate_localization_widget' => [$this, 'block_oro_email_emailtemplate_localization_widget'],
            '_oro_email_emailtemplate_translations_template_fallback_checkbox_row' => [$this, 'block__oro_email_emailtemplate_translations_template_fallback_checkbox_row'],
            '_oro_email_autoresponserule_template_new_entity_oro_email_emailtemplate_template_fallback_checkbox_row' => [$this, 'block__oro_email_autoresponserule_template_new_entity_oro_email_emailtemplate_template_fallback_checkbox_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_email_configuration_oro_email___smtp_settings_password_row', $context, $blocks);
        // line 40
        echo "
";
        // line 41
        $this->displayBlock('_email_configuration_oro_email___attachment_sync_max_size_value_widget', $context, $blocks);
        // line 54
        echo "
";
        // line 55
        $this->displayBlock('_oro_email_autoresponserule_template_new_entity_translations_widget', $context, $blocks);
        // line 62
        echo "
";
        // line 63
        $this->displayBlock('oro_email_template_list_row', $context, $blocks);
        // line 95
        echo "
";
        // line 96
        $this->displayBlock('oro_email_link_to_scope_row', $context, $blocks);
        // line 111
        echo "
";
        // line 112
        $this->displayBlock('oro_email_attachments_row', $context, $blocks);
        // line 153
        echo "
";
        // line 154
        $this->displayBlock('oro_email_emailtemplate_localizations_widget', $context, $blocks);
        // line 185
        echo "
";
        // line 186
        $this->displayBlock('oro_email_email_folder_tree_row', $context, $blocks);
        // line 191
        echo "
";
        // line 192
        $this->displayBlock('oro_email_email_folder_tree_widget', $context, $blocks);
        // line 219
        echo "
";
        // line 220
        $this->displayBlock('oro_email_mailbox_grid_row', $context, $blocks);
        // line 224
        echo "
";
        // line 225
        $this->displayBlock('oro_email_mailbox_grid_label', $context, $blocks);
        // line 242
        echo "
";
        // line 243
        $this->displayBlock('oro_email_mailbox_grid_widget', $context, $blocks);
        // line 255
        echo "
";
        // line 256
        $this->displayBlock('oro_email_mailbox_widget', $context, $blocks);
        // line 311
        echo "
";
        // line 312
        $this->displayBlock('oro_email_emailtemplate_localization_widget', $context, $blocks);
        // line 325
        echo "
";
        // line 326
        $this->displayBlock('_oro_email_emailtemplate_translations_template_fallback_checkbox_row', $context, $blocks);
        // line 335
        echo "
";
        // line 336
        $this->displayBlock('_oro_email_autoresponserule_template_new_entity_oro_email_emailtemplate_template_fallback_checkbox_row', $context, $blocks);
        // line 345
        echo "
";
        // line 376
        echo "
";
        // line 406
        echo "
";
    }

    // line 1
    public function block__email_configuration_oro_email___smtp_settings_password_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        ob_start(function () { return ''; });
        // line 3
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "

        ";
        // line 5
        if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 5), "parent", [], "any", false, false, false, 5))) {
            // line 6
            echo "            ";
            $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 6), "parent", [], "any", false, false, false, 6), "vars", [], "any", false, false, false, 6), "value", [], "any", false, false, false, 6);
            // line 7
            echo "        ";
        } else {
            // line 8
            echo "            ";
            $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 8), "vars", [], "any", false, false, false, 8), "value", [], "any", false, false, false, 8);
            // line 9
            echo "        ";
        }
        // line 10
        echo "
        ";
        // line 11
        $context["options"] = twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 11), "options", [], "any", true, true, false, 11)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 11), "options", [], "any", false, false, false, 11), [])) : ([])), ["elementNamePrototype" =>         // line 12
($context["full_name"] ?? null), "id" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "value", [], "any", false, false, false, 13) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 13), "value", [], "any", false, true, false, 13), "id", [], "any", true, true, false, 13))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "value", [], "any", false, false, false, 13), "id", [], "any", false, false, false, 13)) : (null)), "forEntity" => "user", "organization" => ((((        // line 15
($context["data"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", true, true, false, 15)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 15))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15)) : (null)), "parentElementSelector" => "fieldset", "showLoading" => true]);
        // line 19
        echo "        <div class=\"control-group\">
            <div class=\"controls\">
                <span class=\"form-text\">
                    <button class=\"btn btn-primary check-connection-messages\"
                            data-role=\"check-smtp-connection\"
                            data-page-component-module=\"";
        // line 24
        echo "oroemail/js/app/components/check-smtp-connection-component";
        echo "\"
                            data-page-component-options=\"";
        // line 25
        echo twig_escape_filter($this->env, json_encode(twig_array_merge(($context["options"] ?? null), ["view" => "new"])), "html", null, true);
        echo "\"
                    >";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.system_configuration.smtp_settings.check_connection.label"), "html", null, true);
        echo "</button>
                </span>
                <span class=\"form-text\">
                    <button class=\"btn btn-primary check-connection-messages\"
                            data-role=\"check-saved-smtp-connection\"
                            data-page-component-module=\"";
        // line 31
        echo "oroemail/js/app/components/check-smtp-connection-component";
        echo "\"
                            data-page-component-options=\"";
        // line 32
        echo twig_escape_filter($this->env, json_encode(twig_array_merge(($context["options"] ?? null), ["view" => "saved"])), "html", null, true);
        echo "\"
                    >";
        // line 33
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.system_configuration.smtp_settings.check_saved_connection.label"), "html", null, true);
        echo "</button>
                </span>
                <div class=\"check-connection-messages check-smtp-connection-messages\"></div>
            </div>
        </div>
    ";
        $___internal_parse_46_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 2
        echo twig_spaceless($___internal_parse_46_);
    }

    // line 41
    public function block__email_configuration_oro_email___attachment_sync_max_size_value_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    ";
        // line 43
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_config_system")) {
            // line 44
            echo "    <span class=\"form-text\">
        <a class=\"btn btn-block btn-danger\"
           href=\"";
            // line 46
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_purge_emails_attachments");
            echo "\"
           data-request-method=\"POST\"
           data-page-component-module=\"";
            // line 48
            echo "oroui/js/app/components/hidden-redirect-component";
            echo "\"
           data-page-component-options=\"";
            // line 49
            echo twig_escape_filter($this->env, json_encode(["showLoading" => true]), "html", null, true);
            echo "\"
        >";
            // line 50
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.system_configuration.attachment_configuration.remove_larger_attachments.label"), "html", null, true);
            echo "</a>
    </span>
    ";
        }
    }

    // line 55
    public function block__oro_email_autoresponserule_template_new_entity_translations_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "    ";
        $macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Form/fields.html.twig", 56)->unwrap();
        // line 57
        echo "    ";
        $context["entityNameForm"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 57), "entityName", [], "any", false, false, false, 57);
        // line 58
        echo "
    ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    ";
        // line 60
        echo twig_call_macro($macros["email"], "macro_renderAvailableVariablesWidget", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entityNameForm"] ?? null), "vars", [], "any", false, false, false, 60), "value", [], "any", false, false, false, 60), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entityNameForm"] ?? null), "vars", [], "any", false, false, false, 60), "id", [], "any", false, false, false, 60)], 60, $context, $this->getSourceContext());
        echo "
";
    }

    // line 63
    public function block_oro_email_template_list_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 64
        echo "    <script type=\"text/template\" id=\"emailtemplate-chooser-template\">
        <% _.each(entities, function(entity, i) { %>
        <option value=\"<%- entity.get('id') %>\"><%- entity.get('name') %></option>
        <% }); %>
    </script>

    ";
        // line 70
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "

    ";
        // line 72
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Form/fields.html.twig", 72)->unwrap();
        // line 73
        echo "
    ";
        // line 74
        if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 74), "parent", [], "any", false, false, false, 74))) {
            // line 75
            echo "        ";
            $context["parentId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 75), "parent", [], "any", false, false, false, 75), "vars", [], "any", false, false, false, 75), "id", [], "any", false, false, false, 75);
            // line 76
            echo "    ";
        } else {
            // line 77
            echo "        ";
            $context["parentId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 77), "vars", [], "any", false, false, false, 77), "id", [], "any", false, false, false, 77);
            // line 78
            echo "    ";
        }
        // line 79
        echo "    ";
        $context["options"] = ["targetSelector" => ("#" .         // line 80
($context["id"] ?? null)), "_sourceElement" => (((("#" .         // line 81
($context["parentId"] ?? null)) . " [name\$=\"[") . ($context["depends_on_parent_field"] ?? null)) . "]\"]"), "collectionOptions" => ["route" =>         // line 83
($context["data_route"] ?? null), "routeId" =>         // line 84
($context["data_route_parameter"] ?? null), "includeNonEntity" => (((        // line 85
array_key_exists("includeNonEntity", $context) && ($context["includeNonEntity"] ?? null))) ? (true) : (false)), "includeSystemTemplates" => (((        // line 86
array_key_exists("includeSystemTemplates", $context) &&  !($context["includeSystemTemplates"] ?? null))) ? (false) : (true))]];
        // line 89
        echo "
    <div ";
        // line 90
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroemail/js/app/views/email-template-view", "options" =>         // line 92
($context["options"] ?? null)]], 90, $context, $this->getSourceContext());
        // line 93
        echo "></div>
";
    }

    // line 96
    public function block_oro_email_link_to_scope_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 97
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Form/fields.html.twig", 97)->unwrap();
        // line 98
        echo "
    ";
        // line 99
        $context["options"] = ["enableAttachmentSelector" => "[data-ftid=oro_entity_config_type_attachment_enabled]", "_sourceElement" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 101
($context["form"] ?? null), "vars", [], "any", false, false, false, 101), "id", [], "any", false, false, false, 101))];
        // line 103
        echo "
    ";
        // line 104
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "

    <div ";
        // line 106
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroemail/js/app/views/email-attachment-context-view", "options" =>         // line 108
($context["options"] ?? null)]], 106, $context, $this->getSourceContext());
        // line 109
        echo "></div>
";
    }

    // line 112
    public function block_oro_email_attachments_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 113
        echo "    ";
        $context["entityAttachmentsArray"] = [];
        // line 114
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "entityAttachments", [], "any", false, false, false, 114));
        foreach ($context['_seq'] as $context["_key"] => $context["attachment"]) {
            // line 115
            echo "        ";
            $context["entityAttachmentArray"] = ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 116
$context["attachment"], "id", [], "any", false, false, false, 116), "type" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 117
$context["attachment"], "type", [], "any", false, false, false, 117), "fileName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 118
$context["attachment"], "fileName", [], "any", false, false, false, 118), "icon" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 119
$context["attachment"], "icon", [], "any", false, false, false, 119), "errors" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 120
$context["attachment"], "errors", [], "any", false, false, false, 120)];
            // line 122
            echo "        ";
            $context["entityAttachmentsArray"] = twig_array_merge(($context["entityAttachmentsArray"] ?? null), [0 => ($context["entityAttachmentArray"] ?? null)]);
            // line 123
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attachment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "
    ";
        // line 125
        $context["attachmentsAvailableArray"] = [];
        // line 126
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "attachmentsAvailable", [], "any", false, false, false, 126));
        foreach ($context['_seq'] as $context["_key"] => $context["attachment"]) {
            // line 127
            echo "        ";
            $context["attachmentAvailableArray"] = ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 128
$context["attachment"], "id", [], "any", false, false, false, 128), "type" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 129
$context["attachment"], "type", [], "any", false, false, false, 129), "fileName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 130
$context["attachment"], "fileName", [], "any", false, false, false, 130), "fileSize" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 131
$context["attachment"], "fileSize", [], "any", false, false, false, 131), "modified" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 132
$context["attachment"], "modified", [], "any", false, false, false, 132), "icon" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 133
$context["attachment"], "icon", [], "any", false, false, false, 133), "preview" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 134
$context["attachment"], "preview", [], "any", false, false, false, 134)];
            // line 136
            echo "        ";
            $context["attachmentsAvailableArray"] = twig_array_merge(($context["attachmentsAvailableArray"] ?? null), [0 => ($context["attachmentAvailableArray"] ?? null)]);
            // line 137
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attachment'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 138
        echo "
    ";
        // line 139
        $context["options"] = twig_array_merge(($context["options"] ?? null), ["containerId" =>         // line 140
($context["id"] ?? null), "inputName" =>         // line 141
($context["full_name"] ?? null), "entityAttachments" =>         // line 142
($context["entityAttachmentsArray"] ?? null), "attachmentsAvailable" =>         // line 143
($context["attachmentsAvailableArray"] ?? null), "fileIcons" => $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileIconsConfig()]);
        // line 146
        echo "
    <div data-page-component-module=\"oroemail/js/app/components/email-attachment-component\"
         data-page-component-options=\"";
        // line 148
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
         class=\"attachment-container\">
        ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
    </div>
";
    }

    // line 154
    public function block_oro_email_emailtemplate_localizations_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 155
        echo "    ";
        $macros["emailFormFealds"] = $this;
        // line 156
        echo "
    ";
        // line 157
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Form/fields.html.twig", 157)->unwrap();
        // line 158
        echo "    <div class=\"emailtemplate-translatation oro-tabs tabbable\" ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroemail/js/app/views/email-translation-view"]], 158, $context, $this->getSourceContext());
        // line 160
        echo ">
        ";
        // line 161
        $context["tabId"] = uniqid("email-translation-");
        // line 162
        echo "        ";
        $context["tabContentId"] = ($context["tabId"] ?? null);
        // line 163
        echo "        <div class=\"oro-tabs__head\"  ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/tabs-component"]], 163, $context, $this->getSourceContext());
        // line 165
        echo ">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                ";
        // line 167
        ob_start(function () { return ''; });
        // line 168
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["templateLocalization"]) {
            // line 169
            echo "                        ";
            echo twig_call_macro($macros["emailFormFealds"], "macro_renderTabNavItem", [$context["templateLocalization"], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 169), ((($context["tabId"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 169))], 169, $context, $this->getSourceContext());
            echo "
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['templateLocalization'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 171
        echo "                ";
        $___internal_parse_47_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 167
        echo twig_spaceless($___internal_parse_47_);
        // line 172
        echo "            </ul>
        </div>
        <div class=\"oro-tabs__content\">
            <div class=\"tab-content\">
                ";
        // line 176
        ob_start(function () { return ''; });
        // line 177
        echo "                    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["templateLocalization"]) {
            // line 178
            echo "                        ";
            echo twig_call_macro($macros["emailFormFealds"], "macro_renderTab", [$context["templateLocalization"], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 178), ((($context["tabContentId"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 178))], 178, $context, $this->getSourceContext());
            echo "
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['templateLocalization'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 180
        echo "                ";
        $___internal_parse_48_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 176
        echo twig_spaceless($___internal_parse_48_);
        // line 181
        echo "            </div>
        </div>
    </div>
";
    }

    // line 186
    public function block_oro_email_email_folder_tree_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 187
        echo "    ";
        if (( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 187), "value", [], "any", false, false, false, 187)) && (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 187), "value", [], "any", false, false, false, 187)) > 0))) {
            // line 188
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
            echo "
    ";
        }
    }

    // line 192
    public function block_oro_email_email_folder_tree_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 193
        echo "    ";
        $macros["emailFormFealds"] = $this;
        // line 194
        echo "
    ";
        // line 195
        $context["options"] = ["dataInputSelector" => (("input[name=\"" .         // line 196
($context["full_name"] ?? null)) . "\"]"), "checkAllSelector" => ".check-all", "relatedCheckboxesSelector" => ".folder-list :checkbox"];
        // line 200
        echo "    <div class=\"folder-tree-widget\"
         data-page-component-module=\"oroemail/js/app/components/folder-tree-component\"
         data-page-component-options=\"";
        // line 202
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
         data-layout=\"separate\"
    >
        <label class=\"folder-label\">
            <input class=\"check-all\" type=\"checkbox\">
            ";
        // line 207
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.imap.folder.checkAll"), "html", null, true);
        echo "
        </label>
        <div class=\"folder-list\">
            ";
        // line 210
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 210), "value", [], "any", false, false, false, 210));
        foreach ($context['_seq'] as $context["key"] => $context["folder"]) {
            // line 211
            echo "                ";
            if (((null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["folder"], "parentFolder", [], "any", false, false, false, 211)) && (null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["folder"], "outdatedAt", [], "any", false, false, false, 211)))) {
                // line 212
                echo "                    ";
                echo twig_call_macro($macros["emailFormFealds"], "macro_renderFolder", [$context["key"], $context["folder"], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 212), "full_name", [], "any", false, false, false, 212)], 212, $context, $this->getSourceContext());
                echo "
                ";
            }
            // line 214
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['folder'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 215
        echo "        </div>
        <input name=\"";
        // line 216
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\" type=\"hidden\">
    </div>
";
    }

    // line 220
    public function block_oro_email_mailbox_grid_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 221
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
        echo "
    ";
        // line 222
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    // line 225
    public function block_oro_email_mailbox_grid_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 226
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Form/fields.html.twig", 226)->unwrap();
        // line 227
        echo "    ";
        $context["redirectData"] = ["route" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 228
($context["app"] ?? null), "request", [], "any", false, false, false, 228), "attributes", [], "any", false, false, false, 228), "get", [0 => "_route"], "method", false, false, false, 228), "parameters" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 229
($context["app"] ?? null), "request", [], "any", false, false, false, 229), "attributes", [], "any", false, false, false, 229), "get", [0 => "_route_params"], "method", false, false, false, 229)];
        // line 231
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_organization_update")) {
            // line 232
            echo "        <div class=\"row user-fieldset-block-actions\">
            <div class=\"btn-group\">
                ";
            // line 234
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_mailbox_create", ["redirectData" =>             // line 235
($context["redirectData"] ?? null)]), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.system_configuration.mailbox_configuration.add_mailbox.label")]], 234, $context, $this->getSourceContext());
            // line 237
            echo "
            </div>
        </div>
    ";
        }
    }

    // line 243
    public function block_oro_email_mailbox_grid_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 244
        echo "    ";
        $macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroEmail/Form/fields.html.twig", 244)->unwrap();
        // line 245
        echo "    ";
        $context["redirectData"] = ["route" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 246
($context["app"] ?? null), "request", [], "any", false, false, false, 246), "attributes", [], "any", false, false, false, 246), "get", [0 => "_route"], "method", false, false, false, 246), "parameters" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 247
($context["app"] ?? null), "request", [], "any", false, false, false, 247), "attributes", [], "any", false, false, false, 247), "get", [0 => "_route_params"], "method", false, false, false, 247)];
        // line 249
        echo "    <div class=\"row user-fieldset-block-body\">
        ";
        // line 250
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["base-mailboxes-grid", ["redirectData" =>         // line 251
($context["redirectData"] ?? null), "organization_ids" => [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $this->extensions['Oro\Bundle\SecurityBundle\Twig\OroSecurityExtension']->getCurrentOrganization(), "getId", [], "method", false, false, false, 251)]]], 250, $context, $this->getSourceContext());
        // line 252
        echo "
    </div>
";
    }

    // line 256
    public function block_oro_email_mailbox_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 257
        echo "    ";
        ob_start(function () { return ''; });
        // line 258
        echo "        ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 258), "origin", [], "any", true, true, false, 258)) {
            // line 259
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 259), "origin", [], "any", false, false, false, 259), 'widget');
            echo "
            ";
            // line 260
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 260), "origin", [], "any", false, false, false, 260), 'errors');
            echo "
        ";
        } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 261
($context["form"] ?? null), "children", [], "any", false, true, false, 261), "imapAccountType", [], "any", true, true, false, 261)) {
            // line 262
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 262), "imapAccountType", [], "any", false, false, false, 262), 'widget');
            echo "
            ";
            // line 263
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 263), "imapAccountType", [], "any", false, false, false, 263), 'errors');
            echo "
        ";
        }
        // line 265
        echo "    ";
        $context["imapAccountType"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 266
        echo "
    ";
        // line 267
        $context["process"] = ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 267), "processType", [], "any", false, false, false, 267), 'row') .         // line 268
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 268), "processSettings", [], "any", false, false, false, 268), 'widget'));
        // line 269
        echo "    ";
        $context["access"] = ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 269), "authorizedUsers", [], "any", false, false, false, 269), 'row') .         // line 270
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 270), "authorizedRoles", [], "any", false, false, false, 270), 'row'));
        // line 272
        echo "    ";
        $context["options"] = ["el" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 273
($context["form"] ?? null), "vars", [], "any", false, false, false, 273), "id", [], "any", false, false, false, 273))];
        // line 275
        echo "    <div data-page-component-module=\"oroemail/js/app/views/mailbox-update-view\"
         data-page-component-options=\"";
        // line 276
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\">
        <fieldset class=\"form-horizontal form-horizontal-large\">
            <h5 class=\"user-fieldset\">
                <span>";
        // line 279
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.mailbox.general.label"), "html", null, true);
        echo "</span>
            </h5>
            <div class=\"control-group-wrapper\">
                ";
        // line 282
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
            </div>
        </fieldset>
        <fieldset class=\"form-horizontal form-horizontal-large\">
            <h5 class=\"user-fieldset\">
                <span>";
        // line 287
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.mailbox.origin.label"), "html", null, true);
        echo "</span>
            </h5>
            <div class=\"control-group-wrapper\">
                ";
        // line 290
        echo ($context["imapAccountType"] ?? null);
        echo "
            </div>
        </fieldset>
        <fieldset class=\"form-horizontal form-horizontal-large\">
            <h5 class=\"user-fieldset\">
                <span>";
        // line 295
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.mailbox.process.label"), "html", null, true);
        echo "</span>
            </h5>
            <div class=\"control-group-wrapper\">
                ";
        // line 298
        echo ($context["process"] ?? null);
        echo "
            </div>
        </fieldset>
        <fieldset class=\"form-horizontal form-horizontal-large\">
            <h5 class=\"user-fieldset\">
                <span>";
        // line 303
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.mailbox.access.label"), "html", null, true);
        echo "</span>
            </h5>
            <div class=\"control-group-wrapper\">
                ";
        // line 306
        echo ($context["access"] ?? null);
        echo "
            </div>
        </fieldset>
    </div>
";
    }

    // line 312
    public function block_oro_email_emailtemplate_localization_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 313
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        ";
        // line 314
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subject", [], "any", false, false, false, 314), 'row');
        echo "
        ";
        // line 315
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subjectFallback", [], "any", true, true, false, 315)) {
            // line 316
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subjectFallback", [], "any", false, false, false, 316), 'row');
            echo "
        ";
        }
        // line 318
        echo "
        ";
        // line 319
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "content", [], "any", false, false, false, 319), 'row');
        echo "
        ";
        // line 320
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "contentFallback", [], "any", true, true, false, 320)) {
            // line 321
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "contentFallback", [], "any", false, false, false, 321), 'row');
            echo "
        ";
        }
        // line 323
        echo "    </div>
";
    }

    // line 326
    public function block__oro_email_emailtemplate_translations_template_fallback_checkbox_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 327
        echo "    <div class=\"control-group control-group-checkbox\">
        <div class=\"control-label wrap\">&nbsp;</div>
        <div class=\"controls\">
            ";
        // line 330
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
            ";
        // line 331
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
        echo "
        </div>
    </div>
";
    }

    // line 336
    public function block__oro_email_autoresponserule_template_new_entity_oro_email_emailtemplate_template_fallback_checkbox_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 337
        echo "    <div class=\"control-group control-group-checkbox\">
        <div class=\"control-label wrap\">&nbsp;</div>
        <div class=\"controls\">
            ";
        // line 340
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
            ";
        // line 341
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
        echo "
        </div>
    </div>
";
    }

    // line 353
    public function macro_renderTabNavItem($__form__ = null, $__isActive__ = false, $__uniqid__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "isActive" => $__isActive__,
            "uniqid" => $__uniqid__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 354
            echo "    <li class=\"nav-item\">
        <a href=\"#\"
           class=\"nav-link ";
            // line 356
            if (($context["isActive"] ?? null)) {
                echo "active";
            }
            echo "\"
           data-role=\"change-localization\"
           data-target=\".emailtemplate-translatation-fields-";
            // line 358
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 358), "name", [], "any", false, false, false, 358), "html", null, true);
            echo "\"
           data-toggle=\"tab\"
           data-related=\"";
            // line 360
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 360), "name", [], "any", false, false, false, 360), "html", null, true);
            echo "\"
           role=\"tab\"
           aria-selected=\"";
            // line 362
            echo ((($context["isActive"] ?? null)) ? ("true") : ("false"));
            echo "\"
                ";
            // line 363
            if (($context["uniqid"] ?? null)) {
                // line 364
                echo "                    id=\"";
                echo twig_escape_filter($this->env, (($context["uniqid"] ?? null) . "-tab"), "html", null, true);
                echo "\"
                    aria-controls=\"";
                // line 365
                echo twig_escape_filter($this->env, ($context["uniqid"] ?? null), "html", null, true);
                echo "\"
                ";
            }
            // line 367
            echo "        >";
            // line 368
            if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 368), "localization_title", [], "any", false, false, false, 368))) {
                // line 369
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 369), "localization_title", [], "any", false, false, false, 369), "html", null, true);
            } else {
                // line 371
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplatetranslation.form.default_localization"), "html", null, true);
            }
            // line 373
            echo "</a>
    </li>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 384
    public function macro_renderTab($__form__ = null, $__isActive__ = false, $__uniqid__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "isActive" => $__isActive__,
            "uniqid" => $__uniqid__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 385
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Form/fields.html.twig", 385)->unwrap();
            // line 386
            echo "
    <div class=\"emailtemplate-translatation-fields-";
            // line 387
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 387), "name", [], "any", false, false, false, 387), "html", null, true);
            echo " tab-pane";
            if (($context["isActive"] ?? null)) {
                echo " active";
            }
            echo "\"
         role=\"tabpanel\"
            ";
            // line 389
            if (($context["uniqid"] ?? null)) {
                // line 390
                echo "                id=\"";
                echo twig_escape_filter($this->env, ($context["uniqid"] ?? null), "html", null, true);
                echo "\"
                aria-labelledby=\"";
                // line 391
                echo twig_escape_filter($this->env, (($context["uniqid"] ?? null) . "-tab"), "html", null, true);
                echo "\"
            ";
            }
            // line 393
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroemail/js/app/views/email-template-localization-view", "options" => ["localization" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 397
($context["form"] ?? null), "vars", [], "any", false, false, false, 397), "localization_id", [], "any", false, false, false, 397), "parentId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 398
($context["form"] ?? null), "vars", [], "any", false, false, false, 398), "localization_parent_id", [], "any", false, false, false, 398)]]]], 393, $context, $this->getSourceContext());
            // line 401
            echo "
    >
        ";
            // line 403
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 407
    public function macro_renderFolder($__key__ = null, $__folder__ = null, $__namePrefix__ = null, $__maxDepth__ = 10, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "key" => $__key__,
            "folder" => $__folder__,
            "namePrefix" => $__namePrefix__,
            "maxDepth" => $__maxDepth__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 408
            echo "    ";
            $macros["emailFormFealds"] = $this;
            // line 409
            echo "    ";
            if (($context["maxDepth"] ?? null)) {
                // line 410
                echo "        <div>
            <label class=\"folder-label\">
                <input type=\"checkbox\" data-name=\"syncEnabled\"";
                // line 412
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["folder"] ?? null), "syncEnabled", [], "any", false, false, false, 412)) {
                    echo "checked=\"checked\"";
                }
                echo ">
                ";
                // line 413
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["folder"] ?? null), "name", [], "any", false, false, false, 413), "html", null, true);
                echo "
            </label>
            <input type=\"hidden\" data-name=\"fullName\" value=\"";
                // line 415
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["folder"] ?? null), "fullName", [], "any", false, false, false, 415), "html", null, true);
                echo "\">
            <input type=\"hidden\" data-name=\"name\" value=\"";
                // line 416
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["folder"] ?? null), "name", [], "any", false, false, false, 416), "html", null, true);
                echo "\">
            <input type=\"hidden\" data-name=\"type\" value=\"";
                // line 417
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["folder"] ?? null), "type", [], "any", false, false, false, 417), "html", null, true);
                echo "\">
            ";
                // line 418
                if (((($context["maxDepth"] ?? null) > 1) && (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["folder"] ?? null), "subFolders", [], "any", false, false, false, 418)) > 0))) {
                    // line 419
                    echo "                <div class=\"folder-sub-folders\">
                    ";
                    // line 420
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["folder"] ?? null), "subFolders", [], "any", false, false, false, 420));
                    foreach ($context['_seq'] as $context["subKey"] => $context["subFolder"]) {
                        // line 421
                        echo "                        ";
                        echo twig_call_macro($macros["emailFormFealds"], "macro_renderFolder", [$context["subKey"], $context["subFolder"], (((($context["namePrefix"] ?? null) . "[") . ($context["key"] ?? null)) . "][subFolders]"), (($context["maxDepth"] ?? null) - 1)], 421, $context, $this->getSourceContext());
                        echo "
                    ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['subKey'], $context['subFolder'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 423
                    echo "                </div>
            ";
                }
                // line 425
                echo "        </div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1120 => 425,  1116 => 423,  1107 => 421,  1103 => 420,  1100 => 419,  1098 => 418,  1094 => 417,  1090 => 416,  1086 => 415,  1081 => 413,  1075 => 412,  1071 => 410,  1068 => 409,  1065 => 408,  1049 => 407,  1037 => 403,  1033 => 401,  1031 => 398,  1030 => 397,  1028 => 393,  1023 => 391,  1018 => 390,  1016 => 389,  1007 => 387,  1004 => 386,  1001 => 385,  986 => 384,  975 => 373,  972 => 371,  969 => 369,  967 => 368,  965 => 367,  960 => 365,  955 => 364,  953 => 363,  949 => 362,  944 => 360,  939 => 358,  932 => 356,  928 => 354,  913 => 353,  905 => 341,  901 => 340,  896 => 337,  892 => 336,  884 => 331,  880 => 330,  875 => 327,  871 => 326,  866 => 323,  861 => 321,  859 => 320,  855 => 319,  852 => 318,  847 => 316,  845 => 315,  841 => 314,  836 => 313,  832 => 312,  823 => 306,  817 => 303,  809 => 298,  803 => 295,  795 => 290,  789 => 287,  781 => 282,  775 => 279,  769 => 276,  766 => 275,  764 => 273,  762 => 272,  760 => 270,  758 => 269,  756 => 268,  755 => 267,  752 => 266,  749 => 265,  744 => 263,  739 => 262,  737 => 261,  733 => 260,  728 => 259,  725 => 258,  722 => 257,  718 => 256,  712 => 252,  710 => 251,  709 => 250,  706 => 249,  704 => 247,  703 => 246,  701 => 245,  698 => 244,  694 => 243,  686 => 237,  684 => 235,  683 => 234,  679 => 232,  676 => 231,  674 => 229,  673 => 228,  671 => 227,  668 => 226,  664 => 225,  658 => 222,  653 => 221,  649 => 220,  642 => 216,  639 => 215,  633 => 214,  627 => 212,  624 => 211,  620 => 210,  614 => 207,  606 => 202,  602 => 200,  600 => 196,  599 => 195,  596 => 194,  593 => 193,  589 => 192,  581 => 188,  578 => 187,  574 => 186,  567 => 181,  565 => 176,  562 => 180,  545 => 178,  527 => 177,  525 => 176,  519 => 172,  517 => 167,  514 => 171,  497 => 169,  479 => 168,  477 => 167,  473 => 165,  470 => 163,  467 => 162,  465 => 161,  462 => 160,  459 => 158,  457 => 157,  454 => 156,  451 => 155,  447 => 154,  440 => 150,  435 => 148,  431 => 146,  429 => 143,  428 => 142,  427 => 141,  426 => 140,  425 => 139,  422 => 138,  416 => 137,  413 => 136,  411 => 134,  410 => 133,  409 => 132,  408 => 131,  407 => 130,  406 => 129,  405 => 128,  403 => 127,  398 => 126,  396 => 125,  393 => 124,  387 => 123,  384 => 122,  382 => 120,  381 => 119,  380 => 118,  379 => 117,  378 => 116,  376 => 115,  371 => 114,  368 => 113,  364 => 112,  359 => 109,  357 => 108,  356 => 106,  351 => 104,  348 => 103,  346 => 101,  345 => 99,  342 => 98,  339 => 97,  335 => 96,  330 => 93,  328 => 92,  327 => 90,  324 => 89,  322 => 86,  321 => 85,  320 => 84,  319 => 83,  318 => 81,  317 => 80,  315 => 79,  312 => 78,  309 => 77,  306 => 76,  303 => 75,  301 => 74,  298 => 73,  296 => 72,  291 => 70,  283 => 64,  279 => 63,  273 => 60,  269 => 59,  266 => 58,  263 => 57,  260 => 56,  256 => 55,  248 => 50,  244 => 49,  240 => 48,  235 => 46,  231 => 44,  229 => 43,  224 => 42,  220 => 41,  216 => 2,  207 => 33,  203 => 32,  199 => 31,  191 => 26,  187 => 25,  183 => 24,  176 => 19,  174 => 15,  173 => 13,  172 => 12,  171 => 11,  168 => 10,  165 => 9,  162 => 8,  159 => 7,  156 => 6,  154 => 5,  148 => 3,  145 => 2,  141 => 1,  136 => 406,  133 => 376,  130 => 345,  128 => 336,  125 => 335,  123 => 326,  120 => 325,  118 => 312,  115 => 311,  113 => 256,  110 => 255,  108 => 243,  105 => 242,  103 => 225,  100 => 224,  98 => 220,  95 => 219,  93 => 192,  90 => 191,  88 => 186,  85 => 185,  83 => 154,  80 => 153,  78 => 112,  75 => 111,  73 => 96,  70 => 95,  68 => 63,  65 => 62,  63 => 55,  60 => 54,  58 => 41,  55 => 40,  53 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Form/fields.html.twig");
    }
}
