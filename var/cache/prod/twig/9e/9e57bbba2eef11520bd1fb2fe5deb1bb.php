<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/js_modules_config.html.twig */
class __TwigTemplate_d2b5932fc6e0b1273ac8562271b21961 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["userName"] = null;
        // line 2
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 2)) {
            // line 3
            echo "    ";
            $context["userName"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 3), "username", [], "any", false, false, false, 3);
        }
        // line 5
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroUI/js_modules_config.html.twig", 5)->unwrap();
        // line 6
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["oroui/js/app" => ["baseUrl" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["app"] ?? null), "request", [], "any", false, false, false, 8), "getSchemeAndHttpHost", [], "method", false, false, false, 8), "headerId" => $this->extensions['Oro\Bundle\NavigationBundle\Twig\HashNavExtension']->getHashNavigationHeaderConst(), "userName" =>         // line 10
($context["userName"] ?? null), "root" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["app"] ?? null), "request", [], "any", false, false, false, 11), "getBaseURL", [], "method", false, false, false, 11) . "/"), "startRouteName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["app"] ?? null), "request", [], "any", false, false, false, 12), "attributes", [], "any", false, false, false, 12), "get", [0 => "_master_request_route"], "method", false, false, false, 12), "debug" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["app"] ?? null), "debug", [], "any", false, false, false, 13)) ? (true) : (false)), "skipRouting" => "[data-nohash=true], .no-hash", "controllerPath" => "controllers/", "controllerSuffix" => "-controller", "trailing" => null], "oroui/js/extend/scriptjs" => ["bundlesPath" => (($__internal_compile_0 = twig_split_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/"), "?", 2)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null)]]], 6, $context, $this->getSourceContext());
        // line 22
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroUI/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 22,  52 => 13,  51 => 12,  50 => 11,  49 => 10,  48 => 8,  47 => 6,  45 => 5,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/js_modules_config.html.twig");
    }
}
