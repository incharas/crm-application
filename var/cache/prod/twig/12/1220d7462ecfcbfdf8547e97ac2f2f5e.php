<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContactUs/ContactRequest/update.html.twig */
class __TwigTemplate_c508c89c9e96feeb0b40d0c662c924df extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 3), "value", [], "any", false, false, false, 3), "id", [], "any", false, false, false, 3)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_request_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 3), "value", [], "any", false, false, false, 3), "id", [], "any", false, false, false, 3)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_request_create")));
        // line 4
        $context["fullname"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "value", [], "any", false, false, false, 4)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"));

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%request.clientName%" =>         // line 5
($context["fullname"] ?? null)]]);
        // line 7
        $context["formAttr"] = ["data-validation" => ["Oro\\Bundle\\ContactUsBundle\\Validator\\ContactRequestCallbackValidator" => ["target" => "preferredContactMethod", "deps" => ["oro.contactus.contactrequest.method.both" => [0 => "emailAddress", 1 => "phone"], "oro.contactus.contactrequest.method.phone" => "phone", "oro.contactus.contactrequest.method.email" => "emailAddress"]]]];
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroContactUs/ContactRequest/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 20
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContactUs/ContactRequest/update.html.twig", 21)->unwrap();
        // line 22
        echo "
    ";
        // line 23
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 23), "value", [], "any", false, false, false, 23), "id", [], "any", false, false, false, 23) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contactus_request_delete"))) {
            // line 24
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_request_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 25
($context["form"] ?? null), "vars", [], "any", false, false, false, 25), "value", [], "any", false, false, false, 25), "id", [], "any", false, false, false, 25)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_request_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-contact-request-form", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 29
($context["form"] ?? null), "vars", [], "any", false, false, false, 29), "value", [], "any", false, false, false, 29), "id", [], "any", false, false, false, 29), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.entity_label")]], 24, $context, $this->getSourceContext());
            // line 31
            echo "
        ";
            // line 32
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 32, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 34
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_request_index"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel")]], 34, $context, $this->getSourceContext());
        echo "
    ";
        // line 35
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_contactus_request_view", "params" => ["id" => "\$id"]]], 35, $context, $this->getSourceContext());
        // line 39
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contactus_request_create")) {
            // line 40
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_contactus_request_create"]], 40, $context, $this->getSourceContext()));
            // line 43
            echo "    ";
        }
        // line 44
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contactus_request_edit")) {
            // line 45
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_contactus_request_update", "params" => ["id" => "\$id"]]], 45, $context, $this->getSourceContext()));
            // line 49
            echo "    ";
        }
        // line 50
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 50, $context, $this->getSourceContext());
        echo "
";
    }

    // line 53
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 54), "value", [], "any", false, false, false, 54), "id", [], "any", false, false, false, 54)) {
            // line 55
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 56
($context["form"] ?? null), "vars", [], "any", false, false, false, 56), "value", [], "any", false, false, false, 56), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_request_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.entity_plural_label"), "entityTitle" =>             // line 59
($context["fullname"] ?? null)];
            // line 61
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 63
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.entity_label")]);
            // line 64
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroContactUs/ContactRequest/update.html.twig", 64)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 65
            echo "    ";
        }
    }

    // line 68
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 69
        echo "    ";
        $context["id"] = "contact-request-form";
        // line 70
        echo "
    ";
        // line 71
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.block.general"), "class" => "active", "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.block.request_information"), "data" => [0 =>         // line 78
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "firstName", [], "any", false, false, false, 78), 'row'), 1 =>         // line 79
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "lastName", [], "any", false, false, false, 79), 'row'), 2 =>         // line 80
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organizationName", [], "any", false, false, false, 80), 'row'), 3 =>         // line 81
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "preferredContactMethod", [], "any", false, false, false, 81), 'row'), 4 =>         // line 82
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phone", [], "any", false, false, false, 82), 'row'), 5 =>         // line 83
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emailAddress", [], "any", false, false, false, 83), 'row'), 6 =>         // line 84
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "contactReason", [], "any", false, false, false, 84), 'row'), 7 =>         // line 85
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "comment", [], "any", false, false, false, 85), 'row')]]]]];
        // line 90
        echo "
    ";
        // line 91
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.block.additional")));
        // line 92
        echo "
    ";
        // line 93
        $context["data"] = ["formErrors" => ((        // line 94
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 95
($context["dataBlocks"] ?? null)];
        // line 97
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroContactUs/ContactRequest/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  176 => 97,  174 => 95,  173 => 94,  172 => 93,  169 => 92,  167 => 91,  164 => 90,  162 => 85,  161 => 84,  160 => 83,  159 => 82,  158 => 81,  157 => 80,  156 => 79,  155 => 78,  154 => 71,  151 => 70,  148 => 69,  144 => 68,  139 => 65,  136 => 64,  133 => 63,  127 => 61,  125 => 59,  124 => 56,  122 => 55,  119 => 54,  115 => 53,  108 => 50,  105 => 49,  102 => 45,  99 => 44,  96 => 43,  93 => 40,  90 => 39,  88 => 35,  83 => 34,  78 => 32,  75 => 31,  73 => 29,  72 => 25,  70 => 24,  68 => 23,  65 => 22,  62 => 21,  58 => 20,  53 => 1,  51 => 7,  49 => 5,  46 => 4,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContactUs/ContactRequest/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactUsBundle/Resources/views/ContactRequest/update.html.twig");
    }
}
