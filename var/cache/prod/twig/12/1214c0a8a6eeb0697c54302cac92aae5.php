<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCampaign/EmailCampaign/widget/stats.html.twig */
class __TwigTemplate_3d9de993c3e9d26d09a5beca1f2bfa02 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCampaign/EmailCampaign/widget/stats.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 6
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.stats.open.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stats"] ?? null), "open", [], "any", false, false, false, 6))], 6, $context, $this->getSourceContext());
        echo "
            ";
        // line 7
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.stats.click.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stats"] ?? null), "click", [], "any", false, false, false, 7))], 7, $context, $this->getSourceContext());
        echo "
            ";
        // line 8
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.stats.bounce.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stats"] ?? null), "bounce", [], "any", false, false, false, 8))], 8, $context, $this->getSourceContext());
        echo "
            ";
        // line 9
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.stats.abuse.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stats"] ?? null), "abuse", [], "any", false, false, false, 9))], 9, $context, $this->getSourceContext());
        echo "
            ";
        // line 10
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.stats.unsubscribe.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stats"] ?? null), "unsubscribe", [], "any", false, false, false, 10))], 10, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroCampaign/EmailCampaign/widget/stats.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 10,  57 => 9,  53 => 8,  49 => 7,  45 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCampaign/EmailCampaign/widget/stats.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/CampaignBundle/Resources/views/EmailCampaign/widget/stats.html.twig");
    }
}
