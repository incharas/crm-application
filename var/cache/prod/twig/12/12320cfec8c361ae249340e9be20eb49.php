<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/Dotmailer/marketingListSyncStatus.html.twig */
class __TwigTemplate_4c82f393dde716033283ea3c3081c6c8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/Dotmailer/marketingListSyncStatus.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\" xmlns=\"http://www.w3.org/1999/html\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 6
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["address_book"] ?? null), "syncStatus", [], "any", false, false, false, 6)) {
            // line 7
            echo "                ";
            $context["status"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("oro.dotmailer.syncstatus." . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["address_book"] ?? null), "syncStatus", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)));
            // line 8
            echo "            ";
        } else {
            // line 9
            echo "                ";
            $context["status"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty");
            // line 10
            echo "            ";
        }
        // line 11
        echo "
            ";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.syncstatus.list_is_connected.label", ["%address_book%" => (("<strong>" . twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["address_book"] ?? null), "name", [], "any", false, false, false, 13))) . "</strong>"), "%synced%" => (("<strong>" . _twig_default_filter($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["address_book"] ?? null), "lastExportedAt", [], "any", false, false, false, 15)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) . "</strong>"), "%status%" => (("<strong>" .         // line 18
($context["status"] ?? null)) . "</strong>")]);
        // line 21
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/Dotmailer/marketingListSyncStatus.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 21,  65 => 18,  64 => 15,  63 => 13,  62 => 12,  59 => 11,  56 => 10,  53 => 9,  50 => 8,  47 => 7,  45 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/Dotmailer/marketingListSyncStatus.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/Dotmailer/marketingListSyncStatus.html.twig");
    }
}
