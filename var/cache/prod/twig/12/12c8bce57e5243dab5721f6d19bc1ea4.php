<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivityList/ActivityList/js/workflowTemplate.html.twig */
class __TwigTemplate_1654247c62d5a20b92d0648d1ca8641b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (workflowsData && workflowsData.length > 0) { %>
    <% _.each(workflowsData, function(workflow) { %>
        <% if (workflow.transitionsData.length > 0) { %>
            <li role=\"separator\" class=\"dropdown-divider\">&nbsp;</li>
            <% _.each(workflow.transitionsData, function(transition) { %>
                <li class=\"activity-action\">
                    <a href=\"<%- transition.transitionUrl %>\" class=\"dropdown-item transition-item\"
                       id=\"transition-<%- workflow.name %>-<%- transition.name %>\"
                       data-transition-url=\"<%- transition.transitionUrl %>\"
                       data-transition-label=\"<%- transition.label %>\"
                       data-message=\"<%- transition.message %>\"
                       data-dialog-url=\"<%- transition.dialogUrl %>\"
                        <% if (transition.frontendOptions && transition.frontendOptions.dialog) { %>
                            data-dialog-options=\"<%- JSON.stringify(transition.frontendOptions.dialog) %>\"
                        <% } %>
                    >
                        <i class=\"<%- transition.frontendOptions && transition.frontendOptions.icon
                                ? transition.frontendOptions.icon
                                : 'icon-empty'
                            %> hide-text\"><%- transition.label %></i>
                        <%- transition.label %>
                    </a>
                </li>
            <% }) %>
        <% } %>
    <% }) %>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroActivityList/ActivityList/js/workflowTemplate.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivityList/ActivityList/js/workflowTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActivityListBundle/Resources/views/ActivityList/js/workflowTemplate.html.twig");
    }
}
