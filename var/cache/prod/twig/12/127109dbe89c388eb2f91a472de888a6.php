<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/mobile/form-layout.scss */
class __TwigTemplate_d49d8170b47da622479ab70f58b09375 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.form-layout {
    &__row {
        flex-wrap: wrap;
        width: 100%;
    }

    &__part {
        width: 100%;

        &.is-label {
            padding-bottom: \$form-horizontal-control-label-inner-offset-bottom;
            padding-right: \$content-padding;

            line-height: \$form-horizontal-control-label-inner-line-height;
        }

        &.is-group {
            width: 100%;

            .fields-row-error {
                padding-bottom: \$content-padding-medium;

                &:last-child {
                    padding-bottom: 0;
                }
            }
        }
    }

    .is-group__col-start,
    .is-group__col-end {
        width: 100%;
    }

    /* stylelint-disable selector-type-no-unknown */
    // Update static widths in form fields
    #{map_get(\$oro-form-selectors, 'select2-append'),
    map_get(\$oro-form-selectors, 'select2-prepend')} {
        width: calc(100% - #{\$btn-icon-mobile-width + \$add-on-append-offset-left});
    }

    #{map_get(\$oro-form-selectors, 'select2-add-entity-enabled')} {
        width:
            calc(
                100% - #{\$btn-icon-mobile-width * 2} -
                #{\$add-on-append-outer-offset-left + \$add-on-append-offset-left}
            );
    }
    /* stylelint-enable selector-type-no-unknown */
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/mobile/form-layout.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/mobile/form-layout.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/mobile/form-layout.scss");
    }
}
