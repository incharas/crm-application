<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/image-preview-modal.html */
class __TwigTemplate_42d057e1a7df092b7da3287b8b9bb8ff extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"modal-dialog hide-controls\" role=\"document\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <div class=\"counter\">
                <span class=\"counter-current\"><%- counterCurrent %></span>
                /
                <span class=\"counter-all\"><%- counterAll %></span>
            </div>
            <div class=\"right-toolbar\">
                <button type=\"button\" class=\"btn btn-icon print\" aria-label=\"<%- _.__('Print') %>\">
                    <span aria-hidden=\"true\" class=\"fa-print\"></span>
                </button>
                <a class=\"btn btn-icon download\" href=\"#\" download aria-label=\"<%- _.__('Download') %>\">
                    <span aria-hidden=\"true\" class=\"fa-download\"></span>
                </a>
                <button type=\"button\" class=\"btn btn-icon close\" data-dismiss=\"modal\" aria-label=\"<%- _.__('Close') %>\">
                    <span aria-hidden=\"true\" class=\"fa-close\"></span>
                </button>
            </div>
        </div>
        <div class=\"modal-body\">
            <div class=\"wrap-modal-slider\">
                <div class=\"images-list\">
                    <% _.each(images, function(image) { %>
                        <div class=\"lazy-loading\">
                            <picture>
                                <% _.each(image.sources, function(source) { %>
                                    <source srcset=\"<%- source.srcset %>\" type=\"<%- source.type %>\">
                                <% }) %>
                                <img src=\"<%- image.src %>\" class=\"images-list__item\" loading=\"lazy\">
                            </picture>
                        </div>
                    <% }) %>
                </div>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/image-preview-modal.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/image-preview-modal.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/image-preview-modal.html");
    }
}
