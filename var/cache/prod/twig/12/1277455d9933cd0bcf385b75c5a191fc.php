<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/view.html.twig */
class __TwigTemplate_5ce77b82b30ed6a702654bf12666a353 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'stats' => [$this, 'block_stats'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 5
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/view.html.twig", 6)->unwrap();
        // line 7
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/view.html.twig", 7)->unwrap();
        // line 8
        $context["name"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 8)), "N/A");

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%subject%" => $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["entity"] ?? null), "subject", [], "any", false, false, false, 9)), "%username%" => ($context["name"] ?? null)]]);
        // line 5
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroEmail/Email/view.html.twig", 5);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 13
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_user_emails"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.entity_plural_label"), "entityTitle" => $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 16
($context["entity"] ?? null), "subject", [], "any", false, false, false, 16))];
        // line 18
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 21
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        $context["breadcrumbs"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.menu.user_emails")]];
        // line 25
        echo "    ";
        $this->loadTemplate("@OroNavigation/Menu/breadcrumbs.html.twig", "@OroEmail/Email/view.html.twig", 25)->display($context);
    }

    // line 28
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 29), "created", [], "any", false, false, false, 29)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 29), "created", [], "any", false, false, false, 29)), "html", null, true))) : (print ("N/A")));
        echo "</li>
";
    }

    // line 32
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 34
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/view.html.twig", 35)->unwrap();
        // line 36
        echo "
    ";
        // line 37
        $context["id"] = "email-profile";
        // line 38
        echo "    ";
        $context["attributes"] = [0 => twig_call_macro($macros["UI"], "macro_attibuteRow", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.sent_at.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 39
($context["entity"] ?? null), "sentAt", [], "any", false, false, false, 39))], 39, $context, $this->getSourceContext()), 1 => twig_call_macro($macros["UI"], "macro_attibuteRow", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.from_name.label"), twig_call_macro($macros["EA"], "macro_email_address", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 40
($context["entity"] ?? null), "fromEmailAddress", [], "any", false, false, false, 40), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "fromName", [], "any", false, false, false, 40)], 40, $context, $this->getSourceContext())], 40, $context, $this->getSourceContext()), 2 => twig_call_macro($macros["UI"], "macro_attibuteRow", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("To"), twig_call_macro($macros["EA"], "macro_recipient_email_addresses", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 41
($context["entity"] ?? null), "recipients", [0 => "to"], "method", false, false, false, 41)], 41, $context, $this->getSourceContext())], 41, $context, $this->getSourceContext())];
        // line 43
        echo "
    ";
        // line 44
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recipients", [0 => "cc"], "method", false, false, false, 44)) > 0)) {
            // line 45
            echo "        ";
            $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), [0 => twig_call_macro($macros["UI"], "macro_attibuteRow", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cc"), twig_call_macro($macros["EA"], "macro_recipient_email_addresses", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 46
($context["entity"] ?? null), "recipients", [0 => "cc"], "method", false, false, false, 46)], 46, $context, $this->getSourceContext())], 46, $context, $this->getSourceContext())]);
            // line 48
            echo "    ";
        }
        // line 49
        echo "    ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recipients", [0 => "bcc"], "method", false, false, false, 49)) > 0)) {
            // line 50
            echo "        ";
            $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), [0 => twig_call_macro($macros["UI"], "macro_attibuteRow", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Bcc"), twig_call_macro($macros["EA"], "macro_recipient_email_addresses", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 51
($context["entity"] ?? null), "recipients", [0 => "bcc"], "method", false, false, false, 51)], 51, $context, $this->getSourceContext())], 51, $context, $this->getSourceContext())]);
            // line 53
            echo "    ";
        }
        // line 54
        echo "    ";
        $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), [0 => twig_call_macro($macros["UI"], "macro_attibuteRow", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.subject.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 55
($context["entity"] ?? null), "subject", [], "any", false, false, false, 55)], 55, $context, $this->getSourceContext())]);
        // line 57
        echo "
    ";
        // line 58
        if (($context["noBodyFound"] ?? null)) {
            // line 59
            echo "        ";
            ob_start(function () { return ''; });
            echo "<strong>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.unable_to_load_body"), "html", null, true);
            echo "</strong>";
            $context["notFoundMessage"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 60
            echo "        ";
            $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), [0 => twig_call_macro($macros["UI"], "macro_attibuteRow", ["", ($context["notFoundMessage"] ?? null)], 60, $context, $this->getSourceContext())]);
            // line 61
            echo "    ";
        } else {
            // line 62
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emailBody", [], "any", false, false, false, 62), "hasAttachments", [], "any", false, false, false, 62) && array_key_exists("attachments", $context))) {
                // line 63
                echo "            ";
                $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), [0 => twig_call_macro($macros["UI"], "macro_attibuteRow", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Attachments"), twig_call_macro($macros["EA"], "macro_attachments", [                // line 66
($context["attachments"] ?? null), ($context["targetEntityData"] ?? null)], 66, $context, $this->getSourceContext())], 64, $context, $this->getSourceContext())]);
                // line 68
                echo "        ";
            }
            // line 69
            echo "        ";
            if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emailBody", [], "any", false, false, false, 69), "bodyContent", [], "any", false, false, false, 69))) {
                // line 70
                echo "            ";
                $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), [0 => twig_call_macro($macros["UI"], "macro_attibuteRow", ["", $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.empty_body")], 70, $context, $this->getSourceContext())]);
                // line 71
                echo "        ";
            } else {
                // line 72
                echo "            ";
                $context["attributes"] = twig_array_merge(($context["attributes"] ?? null), [0 => twig_call_macro($macros["EA"], "macro_body", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emailBody", [], "any", false, false, false, 72)], 72, $context, $this->getSourceContext())]);
                // line 73
                echo "        ";
            }
            // line 74
            echo "    ";
        }
        // line 75
        echo "
    ";
        // line 76
        $context["data"] = ["dataBlocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => null, "useSpan" => false, "data" =>         // line 85
($context["attributes"] ?? null)]]]]];
        // line 91
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 91,  201 => 85,  200 => 76,  197 => 75,  194 => 74,  191 => 73,  188 => 72,  185 => 71,  182 => 70,  179 => 69,  176 => 68,  174 => 66,  172 => 63,  169 => 62,  166 => 61,  163 => 60,  156 => 59,  154 => 58,  151 => 57,  149 => 55,  147 => 54,  144 => 53,  142 => 51,  140 => 50,  137 => 49,  134 => 48,  132 => 46,  130 => 45,  128 => 44,  125 => 43,  123 => 41,  122 => 40,  121 => 39,  119 => 38,  117 => 37,  114 => 36,  111 => 35,  107 => 34,  101 => 32,  92 => 29,  88 => 28,  83 => 25,  80 => 22,  76 => 21,  69 => 18,  67 => 16,  66 => 13,  64 => 12,  60 => 11,  55 => 5,  53 => 9,  50 => 8,  48 => 7,  46 => 6,  39 => 5,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/view.html.twig");
    }
}
