<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/open-range.js */
class __TwigTemplate_2e80ac73d812ee758f8d4806390bac2e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const __ = require('orotranslation/js/translator');
    const numberFormatter = require('orolocale/js/formatter/number');

    const defaultParam = {
        minMessage: 'This value should be more than ";
        // line 8
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo ".',
        maxMessage: 'This value should be less than ";
        // line 9
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo ".',
        invalidMessage: 'This value should be a valid number.'
    };

    /**
     * @export oroform/js/validator/open-range
     */
    return [
        'OpenRange',
        function(value, element, param) {
            value = numberFormatter.unformat(value);
            return this.optional(element) ||
                !(isNaN(value) ||
                    (param.min !== null && value <= Number(param.min)) ||
                    (param.max !== null && value >= Number(param.max)));
        },
        function(param, element) {
            let message;
            const placeholders = {};
            const value = this.elementValue(element);
            const normalizedValue = numberFormatter.unformat(value);
            param = Object.assign({}, defaultParam, param);
            if (isNaN(normalizedValue)) {
                message = param.invalidMessage;
            } else if (param.min !== null && normalizedValue <= Number(param.min)) {
                message = param.minMessage;
                placeholders.limit = param.min;
            } else if (param.max !== null && normalizedValue >= Number(param.max)) {
                message = param.maxMessage;
                placeholders.limit = param.max;
            }
            placeholders.value = value;
            return __(message, placeholders);
        }
    ];
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/open-range.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 9,  46 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/open-range.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/open-range.js");
    }
}
