<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImportExport/ImportExport/buttons_from_configuration.html.twig */
class __TwigTemplate_25a8b52a1f35f8f58757dae72b6f0a9d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"pull-left\">
    ";
        // line 2
        echo twig_include($this->env, $context, "@OroImportExport/ImportExport/export-buttons.html.twig");
        echo "
    ";
        // line 3
        echo twig_include($this->env, $context, "@OroImportExport/ImportExport/import-button.html.twig");
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroImportExport/ImportExport/buttons_from_configuration.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImportExport/ImportExport/buttons_from_configuration.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Resources/views/ImportExport/buttons_from_configuration.html.twig");
    }
}
