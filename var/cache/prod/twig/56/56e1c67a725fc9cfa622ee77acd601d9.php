<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/EmailTemplate/update.html.twig */
class __TwigTemplate_a07dc292cf902ed50efac0a119b9a9f7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["pageComponent"] = ["module" => "oroui/js/app/components/view-component", "options" => ["view" => "oroemail/js/app/views/email-template-editor-view"], "layout" => "separate"];
        // line 7
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => "@OroEmail/Form/fields.html.twig"], true);
        // line 11
        $macros["_emailMacros"] = $this->macros["_emailMacros"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/EmailTemplate/update.html.twig", 11)->unwrap();
        // line 12
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "value", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12)) {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "value", [], "any", false, false, false, 13), "name", [], "any", false, false, false, 13)]]);
        }
        // line 16
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 16), "value", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_emailtemplate_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 17
($context["form"] ?? null), "vars", [], "any", false, false, false, 17), "value", [], "any", false, false, false, 17), "id", [], "any", false, false, false, 17)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_emailtemplate_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEmail/EmailTemplate/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 21
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/EmailTemplate/update.html.twig", 22)->unwrap();
        // line 23
        echo "
    ";
        // line 24
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_emailtemplate_preview")) {
            // line 25
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_emailtemplate_preview", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "value", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.action.preview"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.action.preview"), "aCss" => "btn-success dialog-form-renderer no-hash", "iCss" => "fa-share-square-o"]], 25, $context, $this->getSourceContext());
            // line 31
            echo "
    ";
        }
        // line 33
        echo "
    ";
        // line 34
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 34), "value", [], "any", false, false, false, 34), "id", [], "any", false, false, false, 34) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_emailtemplate_clone"))) {
            // line 35
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_emailtemplate_clone", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 36
($context["form"] ?? null), "vars", [], "any", false, false, false, 36), "value", [], "any", false, false, false, 36), "id", [], "any", false, false, false, 36)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.action.clone"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.action.clone"), "aCss" => "btn-success", "iCss" => "fa-share-square-o"]], 35, $context, $this->getSourceContext());
            // line 41
            echo "
        ";
            // line 42
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 42, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 44
        echo "
    ";
        // line 45
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 45), "value", [], "any", false, false, false, 45), "id", [], "any", false, false, false, 45) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 45), "value", [], "any", false, false, false, 45)))) {
            // line 46
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_emailtemplate", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 47
($context["form"] ?? null), "vars", [], "any", false, false, false, 47), "value", [], "any", false, false, false, 47), "id", [], "any", false, false, false, 47)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_emailtemplate_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-emailtemplate", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 51
($context["form"] ?? null), "vars", [], "any", false, false, false, 51), "value", [], "any", false, false, false, 51), "id", [], "any", false, false, false, 51), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.entity_label")]], 46, $context, $this->getSourceContext());
            // line 53
            echo "
    ";
        }
        // line 55
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_emailtemplate_index")], 55, $context, $this->getSourceContext());
        echo "
    ";
        // line 56
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 56), "value", [], "any", false, false, false, 56), "isEditable", [], "any", false, false, false, 56)) {
            // line 57
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 57, $context, $this->getSourceContext());
            echo "
        ";
            // line 58
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_email_emailtemplate_index"]], 58, $context, $this->getSourceContext());
            // line 59
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_emailtemplate_create")) {
                // line 60
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_email_emailtemplate_create"]], 60, $context, $this->getSourceContext()));
                // line 63
                echo "        ";
            }
            // line 64
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 64), "value", [], "any", false, false, false, 64), "id", [], "any", false, false, false, 64) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_emailtemplate_update"))) {
                // line 65
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_email_emailtemplate_update", "params" => ["id" => "\$id"]]], 65, $context, $this->getSourceContext()));
                // line 69
                echo "        ";
            }
            // line 70
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 70, $context, $this->getSourceContext());
            echo "
    ";
        }
    }

    // line 74
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 75
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 75), "value", [], "any", false, false, false, 75), "id", [], "any", false, false, false, 75)) {
            // line 76
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 77
($context["form"] ?? null), "vars", [], "any", false, false, false, 77), "value", [], "any", false, false, false, 77), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_emailtemplate_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.entity_plural_label"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.edit_entity", ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 80
($context["form"] ?? null), "vars", [], "any", false, false, false, 80), "value", [], "any", false, false, false, 80), "name", [], "any", false, false, false, 80)])];
            // line 82
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 84
            echo "        ";
            $context["title"] = ((($context["isClone"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.clone_entity")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.entity_label")])));
            // line 87
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroEmail/EmailTemplate/update.html.twig", 87)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 88
            echo "    ";
        }
    }

    // line 91
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        echo "    ";
        $context["id"] = "emailtemplate-edit";
        // line 93
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.sections.general"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 100
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 100), 'row'), 1 =>         // line 101
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 101), 'row', ["attr" => ["class" => "choice-template-type"]]), 2 =>         // line 102
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entityName", [], "any", false, false, false, 102), 'row')]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.sections.template_data"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 112
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "translations", [], "any", false, false, false, 112), 'widget')]], 1 => ["title" => "", "data" => [0 => twig_call_macro($macros["_emailMacros"], "macro_renderAvailableVariablesWidget", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 118
($context["form"] ?? null), "vars", [], "any", false, false, false, 118), "value", [], "any", false, false, false, 118), "entityName", [], "any", false, false, false, 118), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entityName", [], "any", false, false, false, 118), "vars", [], "any", false, false, false, 118), "id", [], "any", false, false, false, 118)], 118, $context, $this->getSourceContext())]]]]];
        // line 123
        echo "
    ";
        // line 124
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 125
        echo "
    ";
        // line 126
        $context["data"] = ["formErrors" => ((        // line 127
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 128
($context["dataBlocks"] ?? null)];
        // line 131
        echo "
    ";
        // line 132
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/EmailTemplate/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  214 => 132,  211 => 131,  209 => 128,  208 => 127,  207 => 126,  204 => 125,  202 => 124,  199 => 123,  197 => 118,  196 => 112,  195 => 102,  194 => 101,  193 => 100,  191 => 93,  188 => 92,  184 => 91,  179 => 88,  176 => 87,  173 => 84,  167 => 82,  165 => 80,  164 => 77,  162 => 76,  159 => 75,  155 => 74,  147 => 70,  144 => 69,  141 => 65,  138 => 64,  135 => 63,  132 => 60,  129 => 59,  127 => 58,  122 => 57,  120 => 56,  115 => 55,  111 => 53,  109 => 51,  108 => 47,  106 => 46,  104 => 45,  101 => 44,  96 => 42,  93 => 41,  91 => 36,  89 => 35,  87 => 34,  84 => 33,  80 => 31,  78 => 26,  76 => 25,  74 => 24,  71 => 23,  68 => 22,  64 => 21,  59 => 1,  57 => 17,  56 => 16,  53 => 13,  50 => 12,  48 => 11,  46 => 7,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/EmailTemplate/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/EmailTemplate/update.html.twig");
    }
}
