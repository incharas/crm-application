<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/select2/single-choice.html */
class __TwigTemplate_94271458ba0cef59649bb32618a8d9af extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% var resultsId = _.uniqueId('select2-results-'); %>
<% var chosenId = _.uniqueId('select2-chosen-'); %>

<div class=\"select2-container select2-container-single\">
    <a class=\"select2-choice\"
       href=\"#\"
       onclick=\"return false\"
       tabindex=\"-1\"
       role=\"combobox\"
       aria-owns=\"<%- resultsId %>\"
       aria-labelledby=\"<%- chosenId %>\"
       aria-haspopup=\"listbox\"
       aria-expanded=\"false\"
    >
        <span id=\"<%- chosenId %>\"
              class=\"select2-chosen\"
              role=\"textbox\"
              aria-readonly=\"true\"
        >&nbsp</span>
        <abbr class=\"select2-search-choice-close\"
              aria-label=\"<%- _.__('oro.ui.select2.clear.aria_label') %>\"
        ></abbr>
        <span class=\"select2-arrow\" aria-hidden=\"true\"><b></b></span>
    </a>
    <input class=\"select2-focusser select2-offscreen\"
           type=\"text\"
           role=\"combobox\"
           aria-haspopup=\"true\"
           aria-expanded=\"false\"
           aria-owns=\"<%- resultsId %>\"
           aria-autocomplete=\"list\"
    >
    <div class=\"select2-drop select2-display-none\">
        <div class=\"select2-search\">
            <input type=\"text\"
                   autocomplete=\"off\"
                   class=\"select2-input\"
                   role=\"searchbox\"
                   aria-autocomplete=\"list\"
                   aria-controls=\"<%- resultsId %>\"
                   aria-label=\"<%- _.__('oro.ui.select2.search.aria_label') %>\"
            >
        </div>
        <ul class=\"select2-results\"
            id=\"<%- resultsId %>\"
            role=\"listbox\"
            tabindex=\"-1\"
            aria-hidden=\"true\"
            aria-expanded=\"false\"
        ></ul>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/select2/single-choice.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/select2/single-choice.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/select2/single-choice.html");
    }
}
