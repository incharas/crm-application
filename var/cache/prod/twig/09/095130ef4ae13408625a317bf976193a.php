<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/app-loading-mask-component.js */
class __TwigTemplate_51e828c627f46f233d839ff314b3ac50 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const mediator = require('oroui/js/mediator');
    const BaseComponent = require('oroui/js/app/components/base/component');
    const LoadingMaskView = require('oroui/js/app/views/loading-mask-view');

    const AppLoadingMaskComponent = BaseComponent.extend({
        listen: {
            'page:beforeChange mediator': 'showLoading',
            'page:afterChange mediator': 'hideLoading'
        },

        /**
         * @inheritdoc
         */
        constructor: function AppLoadingMaskComponent(options) {
            AppLoadingMaskComponent.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this.initView(options);

            mediator.setHandler('showLoading', this.showLoading, this);
            mediator.setHandler('hideLoading', this.hideLoading, this);

            if (options.showOnStartup) {
                this.showLoading();
            }

            AppLoadingMaskComponent.__super__.initialize.call(this, options);
        },

        /**
         * Initializes loading mask view
         *
         * @param {Object} options
         */
        initView: function(options) {
            const viewOptions = _.defaults({}, options.viewOptions, {
                container: 'body',
                hideDelay: 25
            });
            this.view = new LoadingMaskView(viewOptions);
        },

        /**
         * Shows loading mask
         */
        showLoading: function() {
            this.view.show();
        },

        /**
         * Hides loading mask
         */
        hideLoading: function() {
            this.view.hide();
        }
    });

    return AppLoadingMaskComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/app-loading-mask-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/app-loading-mask-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/app-loading-mask-component.js");
    }
}
