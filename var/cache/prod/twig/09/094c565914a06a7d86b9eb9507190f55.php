<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Opportunity/update.html.twig */
class __TwigTemplate_22edee7b64f35469cf04c9e9b9570acb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head_script' => [$this, 'block_head_script'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["name"] = "N/A";
        // line 3
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 3)) {
            // line 4
            $context["name"] = (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "name")) ? (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 5
($context["entity"] ?? null), "name", [], "any", true, true, false, 5)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 5), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("view %fieldName% not granted", ["%fieldName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.name.label")])));
        }

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%opportunity.name%" =>         // line 9
($context["name"] ?? null)]]);
        // line 11
        $context["pageComponent"] = ["module" => "oroui/js/app/components/view-component", "options" => ["view" => "orosales/js/app/views/update-page-view"], "layout" => "separate"];
        // line 17
        if (array_key_exists("saveFormAction", $context)) {
            // line 18
            $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["saveFormAction"] ?? null), "route", [], "any", false, false, false, 18), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["saveFormAction"] ?? null), "parameters", [], "any", false, false, false, 18));
        } else {
            // line 20
            $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20), "id", [], "any", false, false, false, 20)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "value", [], "any", false, false, false, 21), "id", [], "any", false, false, false, 21)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_create")));
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroSales/Opportunity/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 26
    public function block_head_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    ";
        $this->displayParentBlock("head_script", $context, $blocks);
        echo "
    ";
        // line 28
        $this->displayBlock('stylesheets', $context, $blocks);
    }

    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'stylesheet');
        echo "
    ";
    }

    // line 33
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Opportunity/update.html.twig", 34)->unwrap();
        // line 35
        echo "
    ";
        // line 36
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 36), "value", [], "any", false, false, false, 36), "id", [], "any", false, false, false, 36) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 36), "value", [], "any", false, false, false, 36)))) {
            // line 37
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_opportunity", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 38
($context["form"] ?? null), "vars", [], "any", false, false, false, 38), "value", [], "any", false, false, false, 38), "id", [], "any", false, false, false, 38)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-oppotunity", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 42
($context["form"] ?? null), "vars", [], "any", false, false, false, 42), "value", [], "any", false, false, false, 42), "id", [], "any", false, false, false, 42), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.entity_label")]], 37, $context, $this->getSourceContext());
            // line 44
            echo "
        ";
            // line 45
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 45, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 47
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_index")], 47, $context, $this->getSourceContext());
        echo "
    ";
        // line 48
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_sales_opportunity_view", "params" => ["id" => "\$id"]]], 48, $context, $this->getSourceContext());
        // line 52
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_sales_opportunity_create")) {
            // line 53
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_sales_opportunity_create"]], 53, $context, $this->getSourceContext()));
            // line 56
            echo "    ";
        }
        // line 57
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 57), "value", [], "any", false, false, false, 57), "id", [], "any", false, false, false, 57) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_sales_opportunity_update"))) {
            // line 58
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_sales_opportunity_update", "params" => ["id" => "\$id"]]], 58, $context, $this->getSourceContext()));
            // line 62
            echo "    ";
        }
        // line 63
        echo "    ";
        if ((array_key_exists("returnAction", $context) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["returnAction"] ?? null), "aclRole", [], "any", false, false, false, 63)))) {
            // line 64
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndReturnButton", [["route" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 65
($context["returnAction"] ?? null), "route", [], "any", false, false, false, 65), "params" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 66
($context["returnAction"] ?? null), "parameters", [], "any", false, false, false, 66)]], 64, $context, $this->getSourceContext()));
            // line 68
            echo "    ";
        }
        // line 69
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 69, $context, $this->getSourceContext());
        echo "
";
    }

    // line 72
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Opportunity/update.html.twig", 73)->unwrap();
        // line 74
        echo "
    ";
        // line 75
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 75), "value", [], "any", false, false, false, 75), "id", [], "any", false, false, false, 75)) {
            // line 76
            echo "        ";
            $context["name"] = "N/A";
            // line 77
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 77)) {
                // line 78
                echo "            ";
                $context["name"] = (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "name")) ? (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 79
($context["entity"] ?? null), "name", [], "any", true, true, false, 79)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 79), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))) : (twig_call_macro($macros["UI"], "macro_renderDisabledLabel", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("view %fieldName% not granted", ["%fieldName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.name.label")])], 80, $context, $this->getSourceContext())));
                // line 82
                echo "        ";
            }
            // line 83
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 84
($context["form"] ?? null), "vars", [], "any", false, false, false, 84), "value", [], "any", false, false, false, 84), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.entity_plural_label"), "entityTitle" =>             // line 87
($context["name"] ?? null)];
            // line 89
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 91
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.entity_label")]);
            // line 92
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroSales/Opportunity/update.html.twig", 92)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 93
            echo "    ";
        }
    }

    // line 96
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 97
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "createdAt")) {
            // line 98
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
            echo ": ";
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 98)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 98)), "html", null, true))) : (print ("N/A")));
            echo "</li>
    ";
        }
        // line 100
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "updatedAt")) {
            // line 101
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
            echo ": ";
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 101)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 101)), "html", null, true))) : (print ("N/A")));
            echo "</li>
    ";
        }
    }

    // line 105
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 106
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Opportunity/update.html.twig", 106)->unwrap();
        // line 107
        echo "
    ";
        // line 108
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    ";
        // line 109
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 109)) {
            // line 110
            echo "        <span class=\"page-title__status\">
            ";
            // line 111
            echo twig_call_macro($macros["UI"], "macro_badge", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 111), "name", [], "any", false, false, false, 111), (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 111), "id", [], "any", false, false, false, 111) != "lost")) ? ("enabled") : ("disabled"))], 111, $context, $this->getSourceContext());
            echo "
        </span>
    ";
        }
    }

    // line 116
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 117
        echo "    ";
        $context["id"] = "opportunity-profile";
        // line 118
        echo "
    ";
        // line 119
        $context["formFields"] = [];
        // line 120
        echo "    ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null), "owner") && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", true, true, false, 120))) {
            // line 121
            echo "        ";
            $context["formFields"] = twig_array_merge(($context["formFields"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 121), 'row', ["attr" => ["class" => "control-group-justified"]])]);
            // line 122
            echo "    ";
        }
        // line 123
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null), "name")) {
            // line 124
            echo "        ";
            $context["formFields"] = twig_array_merge(($context["formFields"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 124), 'row', ["attr" => ["autofocus" => true]])]);
            // line 125
            echo "    ";
        }
        // line 126
        echo "    ";
        $context["formFieldsNames"] = [0 => "customerAssociation", 1 => "contact", 2 => "status", 3 => "probability", 4 => "budgetAmount", 5 => "closeDate", 6 => "closeRevenue", 7 => "closeReason"];
        // line 136
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["formFieldsNames"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["formFieldName"]) {
            // line 137
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null), $context["formFieldName"])) {
                // line 138
                echo "            ";
                $context["formFields"] = twig_array_merge(($context["formFields"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), $context["formFieldName"], [], "any", false, false, false, 138), 'row')]);
                // line 139
                echo "        ";
            }
            // line 140
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['formFieldName'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 141
        echo "
    ";
        // line 142
        $context["subBlockData"] = [];
        // line 143
        echo "    ";
        $context["subBlockFieldsNames"] = [0 => "customerNeed", 1 => "proposedSolution", 2 => "notes"];
        // line 144
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["subBlockFieldsNames"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["subBlockFieldName"]) {
            // line 145
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null), $context["subBlockFieldName"])) {
                // line 146
                echo "            ";
                $context["subBlockData"] = twig_array_merge(($context["subBlockData"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), $context["subBlockFieldName"], [], "any", false, false, false, 146), 'row')]);
                // line 147
                echo "        ";
            }
            // line 148
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subBlockFieldName'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 149
        echo "
    ";
        // line 150
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Opportunity Information"), "data" =>         // line 155
($context["formFields"] ?? null)], 1 => ["data" =>         // line 158
($context["subBlockData"] ?? null)]]]];
        // line 162
        echo "
    ";
        // line 163
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 164
        echo "
    ";
        // line 165
        $context["data"] = ["formErrors" => ((        // line 166
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 167
($context["dataBlocks"] ?? null)];
        // line 169
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Opportunity/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  368 => 169,  366 => 167,  365 => 166,  364 => 165,  361 => 164,  359 => 163,  356 => 162,  354 => 158,  353 => 155,  352 => 150,  349 => 149,  343 => 148,  340 => 147,  337 => 146,  334 => 145,  329 => 144,  326 => 143,  324 => 142,  321 => 141,  315 => 140,  312 => 139,  309 => 138,  306 => 137,  301 => 136,  298 => 126,  295 => 125,  292 => 124,  289 => 123,  286 => 122,  283 => 121,  280 => 120,  278 => 119,  275 => 118,  272 => 117,  268 => 116,  260 => 111,  257 => 110,  255 => 109,  251 => 108,  248 => 107,  245 => 106,  241 => 105,  231 => 101,  228 => 100,  220 => 98,  217 => 97,  213 => 96,  208 => 93,  205 => 92,  202 => 91,  196 => 89,  194 => 87,  193 => 84,  191 => 83,  188 => 82,  186 => 79,  184 => 78,  181 => 77,  178 => 76,  176 => 75,  173 => 74,  170 => 73,  166 => 72,  159 => 69,  156 => 68,  154 => 66,  153 => 65,  151 => 64,  148 => 63,  145 => 62,  142 => 58,  139 => 57,  136 => 56,  133 => 53,  130 => 52,  128 => 48,  123 => 47,  118 => 45,  115 => 44,  113 => 42,  112 => 38,  110 => 37,  108 => 36,  105 => 35,  102 => 34,  98 => 33,  91 => 29,  84 => 28,  79 => 27,  75 => 26,  70 => 1,  67 => 21,  66 => 20,  63 => 18,  61 => 17,  59 => 11,  57 => 9,  53 => 5,  52 => 4,  50 => 3,  48 => 2,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Opportunity/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Opportunity/update.html.twig");
    }
}
