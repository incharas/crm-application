<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/widget_loader.html.twig */
class __TwigTemplate_0c07d7dca066efa3cba2b860df48b099 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/widget_loader.html.twig", 1)->unwrap();
        // line 2
        $context["widgetComponentOptions"] = ["type" =>         // line 3
($context["widgetType"] ?? null), "options" =>         // line 4
($context["options"] ?? null)];
        // line 7
        $context["content"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "elementFirst", [], "any", false, false, false, 7)) ? ($this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "url", [], "any", false, false, false, 7))) : (""));
        // line 8
        if ((twig_length_filter($this->env, ($context["content"] ?? null)) ||  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "elementFirst", [], "any", false, false, false, 8))) {
            // line 9
            $context["separateLayout"] = ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "separateLayout", [], "any", true, true, false, 9) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "separateLayout", [], "any", false, false, false, 9));
            // line 10
            echo "    ";
            ob_start(function () { return ''; });
            // line 11
            echo "        ";
            if (($context["separateLayout"] ?? null)) {
                echo " data-layout=\"separate\"";
            }
            // line 12
            echo "    ";
            $context["separateLayoutContent"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 13
            echo "    <div id=\"";
            echo twig_escape_filter($this->env, ($context["elementId"] ?? null), "html", null, true);
            echo "\" ";
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [($context["widgetComponentOptions"] ?? null)], 13, $context, $this->getSourceContext());
            echo twig_escape_filter($this->env, ($context["separateLayoutContent"] ?? null), "html", null, true);
            echo ">
        ";
            // line 14
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "elementFirst", [], "any", false, false, false, 14)) {
                // line 15
                echo "            ";
                echo ($context["content"] ?? null);
                echo "
        ";
            }
            // line 17
            echo "    </div>";
        }
    }

    public function getTemplateName()
    {
        return "@OroUI/widget_loader.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 17,  70 => 15,  68 => 14,  60 => 13,  57 => 12,  52 => 11,  49 => 10,  47 => 9,  45 => 8,  43 => 7,  41 => 4,  40 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/widget_loader.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/widget_loader.html.twig");
    }
}
