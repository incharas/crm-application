<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/page-visibility-tracker.js */
class __TwigTemplate_1003e6b4fe4a3a7b70d16a40c51eee61 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    /**
     * Designed to provide timeout that takes in account hidden state of page and guaranteed waits
     * given time in visible page mode
     */
    const _ = require('underscore');
    const timeoutIds = {};
    let pageShownTime = window.document.visibilityState === 'visible' ? Date.now() : -1;

    /**
     * Recursively creates timeouts till the whole timeout is spent in visible mode of page and stores
     * current timeoutId to ability stop timeout chain by calling `pageVisibilityTracker.clearTimeout`
     *
     * @param {string} uid
     * @param {function} callback
     * @param {number} delay
     */
    function timeout(uid, callback, delay) {
        const startTime = Date.now();

        timeoutIds[uid] = setTimeout(function() {
            if (window.document.visibilityState === 'hidden' || pageShownTime > startTime) {
                timeout(uid, callback, delay);
            } else {
                callback();
            }
        }, delay);
    }

    window.document.addEventListener('visibilitychange', function() {
        if (window.document.visibilityState === 'visible') {
            pageShownTime = Date.now();
        }
    });

    const pageVisibilityTracker = {
        /**
         * @param {function} callback
         * @param {number} delay
         * @return {string}
         */
        setTimeout: function(callback, delay) {
            const uid = _.uniqueId();

            timeout(uid, callback, delay);

            return uid;
        },

        /**
         * @param {string} uid
         */
        clearTimeout: function(uid) {
            if (uid in timeoutIds) {
                clearTimeout(timeoutIds[uid]);
            }
        }
    };

    return pageVisibilityTracker;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/page-visibility-tracker.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/page-visibility-tracker.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/page-visibility-tracker.js");
    }
}
