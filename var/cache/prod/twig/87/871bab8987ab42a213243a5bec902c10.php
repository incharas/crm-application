<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/button-group.scss */
class __TwigTemplate_1ca3b5169d574cb7060f7d6107abe1f7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

/* stylelint-disable no-descending-specificity */

// Make the div behave like a button
.btn-group,
.btn-group-vertical {
    position: relative;
    display: inline-flex;
    // match .btn alignment given font-size hack above
    vertical-align: middle;

    > .btn {
        position: relative;
        flex: 0 1 auto;

        // Bring the hover, focused, and \"active\" buttons to the front to overlay
        // the borders properly
        @include hover {
            z-index: 1;
        }

        &:focus,
        &:active,
        &.active {
            z-index: 1;
        }
    }

    // Prevent double borders when buttons are next to each other
    .btn + .btn,
    .btn + .btn-group,
    .btn-group + .btn,
    .btn-group + .btn-group {
        margin-left: -\$btn-group-divider-width;
    }

    @each \$color, \$value in \$btn-group-square-divider-keys {
        .btn + .btn-square-#{\$color},
        .btn-group + .btn-square-#{\$color} {
            margin-left: -\$btn-group-square-divider-width;
        }
    }

    > .btn-sm:first-child {
        padding-right: \$btn-in-group-padding-x-sm;
    }

    .btn-sm + .btn-sm {
        padding-left: \$btn-in-group-padding-x-sm;
    }
}

// Optional: Group multiple button groups together for a toolbar
.btn-toolbar {
    display: flex;
    flex-wrap: wrap;
    justify-content: flex-start;

    .input-group {
        width: auto;
    }
}

.btn-group {
    > .btn:first-child {
        margin-left: 0;
    }

    // Reset rounded corners
    > .btn:not(:last-child):not(.dropdown-toggle),
    > .btn-group:not(:last-child) > .btn {
        @include border-end-radius(0);

        border-right-width: \$btn-group-divider-width;

        &,
        &:hover,
        &:focus,
        &:active {
            border-right-color: \$btn-group-divider-color;
        }
    }

    > .btn:not(:first-child),
    > .btn-group:not(:first-child) > .btn {
        @include border-start-radius(0);

        border-left-width: \$btn-group-divider-width;

        &,
        &:hover,
        &:focus,
        &:active {
            border-left-color: \$btn-group-divider-color;
        }
    }

    @each \$color, \$value in \$btn-group-divider-keys {
        > .btn-#{\$color}:not(:last-child):not(.dropdown-toggle),
        > .btn-group:not(:last-child) > .btn-#{\$color} {
            &,
            &:hover,
            &:focus,
            &:active {
                border-right-color: \$value;
            }
        }

        > .btn-#{\$color}:not(:first-child),
        > .btn-group:not(:first-child) > .btn-#{\$color} {
            &,
            &:hover,
            &:focus,
            &:active {
                border-left-color: \$value;
            }
        }
    }

    @each \$color, \$value in \$btn-group-square-divider-keys {
        > .btn-square-#{\$color}:not(:last-child):not(.dropdown-toggle),
        > .btn-group:not(:last-child) > .btn-square-#{\$color} {
            border-right-width: \$btn-group-square-divider-width;

            &,
            &:hover,
            &:active {
                border-right-color: \$value;
            }

            &:focus {
                border-right-color: \$secondary-100;
            }
        }

        > .btn-square-#{\$color}:not(:first-child),
        > .btn-group:not(:first-child) > .btn-square-#{\$color} {
            border-left-width: \$btn-group-square-divider-width;

            &,
            &:hover,
            &:active {
                border-left-color: \$value;
            }

            &:focus {
                border-left-color: \$secondary-100;
            }
        }
    }
}

// Sizing

/*
   Split button dropdowns
*/

.dropdown-toggle-split {
    padding-right: \$btn-padding-x * .75;
    padding-left: \$btn-padding-x * .75;

    &::after,
    .dropup &::after,
    .dropright &::after {
        margin-left: 0;
    }

    .dropleft &::before {
        margin-right: 0;
    }
}

.btn-sm + .dropdown-toggle-split {
    padding-right: \$btn-padding-x-sm * .75;
    padding-left: \$btn-padding-x-sm * .75;
}

.btn-lg + .dropdown-toggle-split {
    padding-right: \$btn-padding-x-lg * .75;
    padding-left: \$btn-padding-x-lg * .75;
}

// The clickable button for toggling the menu
// Set the same inset shadow as the :active state
.btn-group.show .dropdown-toggle {
    @include box-shadow(\$btn-active-box-shadow);

    // Show no shadow for `.btn-link` since it has no other button styles.
    &.btn-link {
        @include box-shadow(none);
    }
}

/*
   Vertical button groups
*/

.btn-group-vertical {
    flex-direction: column;
    align-items: flex-start;
    justify-content: center;

    .btn,
    .btn-group {
        width: 100%;
    }

    > .btn + .btn,
    > .btn + .btn-group,
    > .btn-group + .btn,
    > .btn-group + .btn-group {
        margin-top: -\$btn-group-divider-width;
        margin-left: 0;
    }

    // Reset rounded corners
    > .btn:not(:last-child):not(.dropdown-toggle),
    > .btn-group:not(:last-child) > .btn {
        @include border-bottom-radius(0);
    }

    > .btn:not(:first-child),
    > .btn-group:not(:first-child) > .btn {
        @include border-top-radius(0);
    }
}

// Checkbox and radio options
// In order to support the browser's form validation feedback, powered by the
// `required` attribute, we have to \"hide\" the inputs via `clip`. We cannot use
// `display: none;` or `visibility: hidden;` as that also hides the popover.
// Simply visually hiding the inputs via `opacity` would leave them clickable in
// certain cases which is prevented by using `clip` and `pointer-events`.
// This way, we ensure a DOM element is visible to position the popover from.
// See https://github.com/twbs/bootstrap/pull/12794 and
// https://github.com/twbs/bootstrap/pull/14559 for more information.

.btn-group-toggle {
    > .btn,
    > .btn-group > .btn {
        // Override default `<label>` value
        margin-bottom: 0;

        input[type='radio'],
        input[type='checkbox'] {
            position: absolute;
            clip: rect(0, 0, 0, 0);
            pointer-events: none;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/button-group.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/button-group.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/button-group.scss");
    }
}
