<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/_dots_tabs.html.twig */
class __TwigTemplate_23064f0b76ac71061944e1c9b2cede35 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'item' => [$this, 'block_item'],
            'item_renderer' => [$this, 'block_item_renderer'],
            'linkElement' => [$this, 'block_linkElement'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/_htabs.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/_htabs.html.twig", "@OroNavigation/Menu/_dots_tabs.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $this->displayBlock("item_renderer", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_item_renderer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $context["showNonAuthorized"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 8), "show_non_authorized", [], "any", true, true, false, 8) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 8), "show_non_authorized", [], "any", false, false, false, 8));
        // line 9
        echo "    ";
        $context["displayable"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 9), "isAllowed", [], "any", false, false, false, 9) || ($context["showNonAuthorized"] ?? null));
        // line 10
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayed", [], "any", false, false, false, 10) && ($context["displayable"] ?? null))) {
            // line 11
            echo "        ";
            // line 12
            echo "        <li>
            ";
            // line 13
            $this->displayBlock("linkElement", $context, $blocks);
            echo "
        </li>
    ";
        }
    }

    // line 18
    public function block_linkElement($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/_dots_tabs.html.twig", 19)->unwrap();
        // line 20
        echo "    ";
        $context["class"] = [0 => ""];
        // line 21
        echo "    ";
        $context["linkAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "linkAttributes", [], "any", false, false, false, 21);
        // line 22
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 22), "active_if_first_is_empty", [], "any", true, true, false, 22) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 22), "active_if_first_is_empty", [], "any", false, false, false, 22))) {
            // line 23
            echo "        ";
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["aria-selected" => "true"]);
            // line 26
            echo "        ";
            $context["class"] = twig_array_merge(($context["class"] ?? null), [0 => "active"]);
            // line 27
            echo "    ";
        } else {
            // line 28
            echo "        ";
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["aria-selected" => "false"]);
            // line 31
            echo "    ";
        }
        // line 32
        echo "    ";
        $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["id" => (twig_trim_filter(twig_lower_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 33
($context["item"] ?? null), "name", [], "any", false, false, false, 33), [" " => "_", "#" => "_"]))) . "-tab"), "href" => (("#" . twig_trim_filter(twig_lower_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["item"] ?? null), "name", [], "any", false, false, false, 34), [" " => "_", "#" => "_"])))) . "-content"), "class" => twig_join_filter(        // line 35
($context["class"] ?? null), " "), "role" => "tab", "data-toggle" => "tab", "aria-controls" => (twig_trim_filter(twig_lower_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 38
($context["item"] ?? null), "name", [], "any", false, false, false, 38), [" " => "_", "#" => "_"]))) . "-content")]);
        // line 40
        echo "    <a ";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 40, $context, $this->getSourceContext());
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/_dots_tabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 40,  121 => 38,  120 => 35,  119 => 34,  118 => 33,  116 => 32,  113 => 31,  110 => 28,  107 => 27,  104 => 26,  101 => 23,  98 => 22,  95 => 21,  92 => 20,  89 => 19,  85 => 18,  77 => 13,  74 => 12,  72 => 11,  69 => 10,  66 => 9,  63 => 8,  59 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/_dots_tabs.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/_dots_tabs.html.twig");
    }
}
