<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContactUs/ContactReason/update.html.twig */
class __TwigTemplate_8d19c6f56c33da83cf65a1a5af1ce9e9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 3), "value", [], "any", false, false, false, 3), "id", [], "any", false, false, false, 3)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_reason_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 3), "value", [], "any", false, false, false, 3), "id", [], "any", false, false, false, 3)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_reason_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroContactUs/ContactReason/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContactUs/ContactReason/update.html.twig", 6)->unwrap();
        // line 7
        echo "
    ";
        // line 8
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 8), "value", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contactus_reason_delete"))) {
            // line 9
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_reason_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "value", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_reason_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-contact-reason-form", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["form"] ?? null), "vars", [], "any", false, false, false, 14), "value", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactreason.entity_label")]], 9, $context, $this->getSourceContext());
            // line 16
            echo "
        ";
            // line 17
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 17, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 19
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_reason_index"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel")]], 19, $context, $this->getSourceContext());
        echo "
    ";
        // line 20
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_contactus_reason_index"]], 20, $context, $this->getSourceContext());
        // line 23
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contactus_reason_create")) {
            // line 24
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_contactus_reason_create"]], 24, $context, $this->getSourceContext()));
            // line 27
            echo "    ";
        }
        // line 28
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contactus_reason_update")) {
            // line 29
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_contactus_reason_update", "params" => ["id" => "\$id"]]], 29, $context, $this->getSourceContext()));
            // line 33
            echo "    ";
        }
        // line 34
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 34, $context, $this->getSourceContext());
        echo "
";
    }

    // line 37
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 38), "value", [], "any", false, false, false, 38), "id", [], "any", false, false, false, 38)) {
            // line 39
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 40
($context["form"] ?? null), "vars", [], "any", false, false, false, 40), "value", [], "any", false, false, false, 40), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contactus_reason_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactreason.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 43
($context["entity"] ?? null), "defaultTitle", [], "any", false, false, false, 43)];
            // line 45
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 47
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactreason.entity_label")]);
            // line 48
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroContactUs/ContactReason/update.html.twig", 48)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 49
            echo "    ";
        }
    }

    // line 52
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    ";
        $context["id"] = "contact-reason-form";
        // line 54
        echo "
    ";
        // line 55
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.block.general"), "class" => "nav-item active", "subblocks" => [0 => ["data" => [0 =>         // line 61
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "titles", [], "any", false, false, false, 61), 'row')]]]]];
        // line 66
        echo "
    ";
        // line 67
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 68
        echo "
    ";
        // line 69
        $context["data"] = ["formErrors" => ((        // line 70
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 71
($context["dataBlocks"] ?? null)];
        // line 73
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroContactUs/ContactReason/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 73,  160 => 71,  159 => 70,  158 => 69,  155 => 68,  153 => 67,  150 => 66,  148 => 61,  147 => 55,  144 => 54,  141 => 53,  137 => 52,  132 => 49,  129 => 48,  126 => 47,  120 => 45,  118 => 43,  117 => 40,  115 => 39,  112 => 38,  108 => 37,  101 => 34,  98 => 33,  95 => 29,  92 => 28,  89 => 27,  86 => 24,  83 => 23,  81 => 20,  76 => 19,  71 => 17,  68 => 16,  66 => 14,  65 => 10,  63 => 9,  61 => 8,  58 => 7,  55 => 6,  51 => 5,  46 => 1,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContactUs/ContactReason/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactUsBundle/Resources/views/ContactReason/update.html.twig");
    }
}
