<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/img/oro_icon.svg */
class __TwigTemplate_42f55da92608d6c3de8ba659329dcaaf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<svg id=\"Layer_1\" data-name=\"Layer 1\" xmlns=\"http://www.w3.org/2000/svg\" viewBox=\"0 0 94.56 94.5\"><defs><style>.cls-1{fill:#fbba1d;}.cls-2{fill:#df9026;}.cls-3{fill:#ac732a;}.cls-4{fill:#ffc841;}</style></defs><title>oro_icon</title><path class=\"cls-1\" d=\"M270.65,433.66l38.56-37.81a10.5,10.5,0,0,1,14.85.15h0l-17.5-17.85a10.5,10.5,0,0,0-14.85-.15h0l-38.56,37.81a10.5,10.5,0,0,0-.15,14.85h0l17.5,17.85a10.5,10.5,0,0,1,.15-14.85Z\" transform=\"translate(-250 -375)\"/><path class=\"cls-2\" d=\"M285.35,448.65l38.56-37.81a10.5,10.5,0,0,0,.15-14.85h0l17.5,17.85a10.5,10.5,0,0,1-.15,14.85h0L302.85,466.5a10.5,10.5,0,0,1-14.85-.15h0L270.5,448.5a10.5,10.5,0,0,0,14.85.15Z\" transform=\"translate(-250 -375)\"/><path class=\"cls-3\" d=\"M341.56,413.85,324.06,396a10.5,10.5,0,0,1-.15,14.85h0l-1.07,1L333,422.2a10.5,10.5,0,0,1-.15,14.85h0l8.57-8.4A10.5,10.5,0,0,0,341.56,413.85Z\" transform=\"translate(-250 -375)\"/><path class=\"cls-4\" d=\"M298,386.55l10.15,10.35,1.07-1a10.5,10.5,0,0,1,14.85.15h0l-17.5-17.85A10.5,10.5,0,0,0,291.7,378h0l-1.07,1Z\" transform=\"translate(-250 -375)\"/></svg>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/img/oro_icon.svg";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/img/oro_icon.svg", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/img/oro_icon.svg");
    }
}
