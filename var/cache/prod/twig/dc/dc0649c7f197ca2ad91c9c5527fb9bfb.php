<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/error-page.scss */
class __TwigTemplate_5bb9821d8cfc3ad4a6a9caa5deea3132 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.error-page-wrapper {
    position: absolute;
    top: 0;
    left: 0;
    bottom: 0;
    right: 0;

    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: center;
    min-height: 400px;
    height: inherit;
    padding: 20px;

    background-color: \$error-page-wrapper-background;
}

.error-page-main {
    display: flex;
    flex-direction: column;
    flex-grow: 1;
    justify-content: center;
    align-items: center;
}

.error-page-footer {
    font-size: 12px;
    text-align: center;

    color: \$error-page-footer-text-color;
}

.error-page-content {
    max-width: 320px;

    font-size: 14px;
    text-align: center;
    line-height: 1.3;

    color: \$error-page-content-text-color;
}

.error-page-img {
    height: 40px;

    margin-bottom: 32px;
}

.error-page-title {
    margin: 0 0 8px;

    font-size: 22px;
    font-weight: font-weight('light');
    line-height: 1.3;

    color: \$error-page-title-text-color;
}

.error-page-description {
    margin-bottom: 32px;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/error-page.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/error-page.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/error-page.scss");
    }
}
