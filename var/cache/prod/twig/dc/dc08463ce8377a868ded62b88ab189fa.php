<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAsset/Asset.html.twig */
class __TwigTemplate_3f903349e0a4237dbe0673847ba4114b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "
";
        // line 12
        echo "
";
    }

    // line 1
    public function macro_css($__src__ = null, $__extra_attributes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "src" => $__src__,
            "extra_attributes" => $__extra_attributes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            if ($this->extensions['Oro\Bundle\AssetBundle\Twig\WebpackExtension']->isRunning()) {
                // line 3
                echo "        <script src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($this->extensions['Oro\Bundle\AssetBundle\Twig\WebpackExtension']->getServerUrl(($context["src"] ?? null))), "html", null, true);
                echo "\"></script>
    ";
            } else {
                // line 5
                echo "        <link rel=\"stylesheet\" ";
                echo ($context["extra_attributes"] ?? null);
                echo " href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(($context["src"] ?? null)), "html", null, true);
                echo "\">
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 9
    public function macro_js($__src__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "src" => $__src__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 10
            echo "    <script src=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(($context["src"] ?? null)), "html", null, true);
            echo "\"></script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 13
    public function macro_js_modules_config($__config__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "config" => $__config__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 14
            echo "    <script data-role=\"config\" type=\"application/json\">
        ";
            // line 15
            echo json_encode(($context["config"] ?? null));
            echo "
    </script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroAsset/Asset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 15,  121 => 14,  108 => 13,  96 => 10,  83 => 9,  68 => 5,  62 => 3,  59 => 2,  45 => 1,  40 => 12,  37 => 8,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAsset/Asset.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AssetBundle/Resources/views/Asset.html.twig");
    }
}
