<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/viewport-manager.js */
class __TwigTemplate_9795ad95f8f932f9fef913893ff978d5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import mediator from 'oroui/js/mediator';
import error from 'oroui/js/error';

const viewportManager = {
    /**
     * @property {Object}
     */
    mediaTypes: null,

    /**
     * @inheritdoc
     */
    initialize() {
        this.mediaTypes = this._prepareMediaTypes(this.getBreakpoints());

        this._subscribeToAll();
    },

    /**
     * Check viewport ability
     * @param mediaTypes
     * @returns {boolean}
     */
    isApplicable(mediaTypes) {
        return [mediaTypes || 'all']
            .flat(Infinity)
            .some(item => {
                const type = this.getMediaType(item);

                if (type === void 0) {
                    return false;
                }

                return type.matches;
            });
    },

    /**
     * @param {HTMLElement} context
     * @returns {any}
     */
    getBreakpoints(context) {
        if (!context) {
            context = document.documentElement;
        }

        const cssProperty = window.getComputedStyle(context).getPropertyValue('--breakpoints').trim() || '{}';

        return {all: 'all', ...JSON.parse(cssProperty)};
    },

    _prepareMediaTypes(breakpoints) {
        return Object.entries(breakpoints)
            .reduce((mediaTypes, [key, value]) => Object.assign(mediaTypes, {
                [key]: Object.assign(window.matchMedia(value), {mediaType: key})
            }), {});
    },

    _subscribe(mediaType, handler) {
        // There is no reason to subscribe on 'all' event because it never changes
        if (mediaType === 'all') {
            return;
        }

        const mql = this.getMediaType(mediaType);

        if (mql) {
            mql.addEventListener('change', handler);
        } else {
            error.showErrorInConsole(`The media type \"\${mediaType}\" is not defined`);
        }
    },

    getMediaType(mediaType) {
        return this.mediaTypes[mediaType];
    },

    _subscribeToAll() {
        Object.keys(this.mediaTypes).forEach(mediaType => this._subscribe(mediaType, this._onChangeHandler));
    },

    _onChangeHandler(event) {
        mediator.trigger(`viewport:change`, event);
        mediator.trigger(`viewport:\${event.target.mediaType}`, event);
    }
};

export default viewportManager;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/viewport-manager.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/viewport-manager.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/viewport-manager.js");
    }
}
