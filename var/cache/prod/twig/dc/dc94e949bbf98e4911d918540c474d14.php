<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/jstree/move-action-view.js */
class __TwigTemplate_ee24b09498a288231bd42f97d9c0894f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const AbstractActionView = require('oroui/js/app/views/jstree/abstract-action-view');
    const _ = require('underscore');
    const \$ = require('jquery');
    const __ = require('orotranslation/js/translator');
    const DialogWidget = require('oro/dialog-widget');
    const routing = require('routing');
    const messenger = require('oroui/js/messenger');

    const MoveActionView = AbstractActionView.extend({
        options: _.extend({}, AbstractActionView.prototype.options, {
            icon: 'random',
            label: __('oro.ui.jstree.actions.move'),
            routeName: null,
            routeParams: {}
        }),

        /**
         * @inheritdoc
         */
        constructor: function MoveActionView(options) {
            MoveActionView.__super__.constructor.call(this, options);
        },

        onClick: function() {
            const \$tree = this.options.\$tree;
            const selectedIds = \$tree.jstree('get_checked');

            if (!selectedIds.length) {
                messenger.notificationFlashMessage('warning', __('oro.ui.jstree.no_node_selected_error'));
                return;
            }

            let url = false;
            if (this.options.routeName) {
                const routeParams = this.options.routeParams;
                routeParams.selected = selectedIds;
                url = routing.generate(this.options.routeName, routeParams);
            }

            this.dialogWidget = new DialogWidget({
                title: __('oro.ui.jstree.actions.move'),
                url: url,
                stateEnabled: false,
                incrementalPosition: true,
                dialogOptions: {
                    modal: true,
                    allowMaximize: true,
                    width: 650,
                    minHeight: 100,
                    close: this.onDialogClose.bind(this)
                }
            });

            \$tree.data('treeView').moveTriggered = true;

            this.dialogWidget.once('formSave', changed => {
                \$.when(_.each(changed, data => {
                    const defer = \$.Deferred();
                    \$tree.jstree('move_node', data.id, data.parent, data.position);
                    \$tree.jstree('uncheck_node', '#' + data.id);

                    return defer.resolve();
                })).done(() => {
                    \$tree.data('treeView').moveTriggered = false;
                });
            });

            this.dialogWidget.render();
        },

        onDialogClose: function() {
            this.options.\$tree.data('treeView').moveTriggered = false;
        }
    });

    return MoveActionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/jstree/move-action-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/jstree/move-action-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/jstree/move-action-view.js");
    }
}
