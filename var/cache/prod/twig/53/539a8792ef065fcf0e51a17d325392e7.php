<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/images/icons/arrow-down--gray.svg */
class __TwigTemplate_32ba3577c0c99df2202a240f3dad82d0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<svg width=\"10\" height=\"6\" viewBox=\"0 0 10 6\" xmlns=\"http://www.w3.org/2000/svg\"><path d=\"M0 .027l4.99 5.954L9.98.027z\" fill=\"#707070\" fill-rule=\"evenodd\"/></svg>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/images/icons/arrow-down--gray.svg";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/images/icons/arrow-down--gray.svg", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/images/icons/arrow-down--gray.svg");
    }
}
