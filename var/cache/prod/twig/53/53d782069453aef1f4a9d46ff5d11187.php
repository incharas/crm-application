<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/catalog-switcher.scss */
class __TwigTemplate_06765ac191ad1ca03ad11cf16f955ac8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

.catalog-switcher {
    &__tooltip-trigger {
        position: absolute;
        inset: 0;
    }

    .dropdown-toggle {
        position: relative;
    }

    .dropdown-menu[x-placement] {
        .dropdown-item {
            white-space: \$catalog-switcher-dropdown-item-white-space;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/catalog-switcher.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/catalog-switcher.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/catalog-switcher.scss");
    }
}
