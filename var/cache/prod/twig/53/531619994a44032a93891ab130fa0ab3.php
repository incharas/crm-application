<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroQueryDesigner/macros.html.twig */
class __TwigTemplate_71353ae85daa447d4f760bdb3c950c16 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "
";
        // line 76
        echo "
";
        // line 95
        echo "
";
        // line 131
        echo "
";
        // line 154
        echo "
";
        // line 224
        echo "
";
        // line 260
        echo "
";
        // line 279
        echo "
";
    }

    // line 1
    public function macro_query_designer_condition_builder($__params__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $context["params"] = twig_array_merge(["column_chain_template_selector" => "#column-chain-template", "field_choice_filter_preset" => "querydesigner"],             // line 5
($context["params"] ?? null));
            // line 6
            echo "    ";
            $context["fieldConditionModule"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "fieldConditionModule", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "fieldConditionModule", [], "any", false, false, false, 6), "oroquerydesigner/js/app/views/field-condition-view")) : ("oroquerydesigner/js/app/views/field-condition-view"));
            // line 7
            echo "    ";
            $context["fieldConditionOptions"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(["filters" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 8
($context["params"] ?? null), "metadata", [], "any", false, true, false, 8), "filters", [], "any", true, true, false, 8)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "metadata", [], "any", false, true, false, 8), "filters", [], "any", false, false, false, 8), [])) : ([])), "hierarchy" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["params"] ?? null), "metadata", [], "any", false, true, false, 9), "hierarchy", [], "any", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "metadata", [], "any", false, true, false, 9), "hierarchy", [], "any", false, false, false, 9), [])) : ([])), "fieldChoice" => ["filterPreset" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["params"] ?? null), "field_choice_filter_preset", [], "any", false, false, false, 11), "select2" => ["placeholder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.choose_entity_field"), "formatSelectionTemplateSelector" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["params"] ?? null), "column_chain_template_selector", [], "any", true, true, false, 14)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "column_chain_template_selector", [], "any", false, false, false, 14), null)) : (null))]]], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["params"] ?? null), "fieldConditionOptions", [], "any", true, true, false, 17)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "fieldConditionOptions", [], "any", false, false, false, 17), [])) : ([])));
            // line 18
            echo "    ";
            $context["conditionBuilderOptions"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(["fieldsRelatedCriteria" => [0 => "condition-item"]], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["params"] ?? null), "conditionBuilderOptions", [], "any", true, true, false, 20)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "conditionBuilderOptions", [], "any", false, false, false, 20), [])) : ([])));
            // line 21
            echo "    <div class=\"condition-builder\"
         data-page-component-module=\"oroquerydesigner/js/app/components/condition-builder-component\"
         data-page-component-options=\"";
            // line 23
            echo twig_escape_filter($this->env, json_encode(($context["conditionBuilderOptions"] ?? null)), "html", null, true);
            echo "\"
         data-page-component-name=\"";
            // line 24
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "componentName", [], "any", true, true, false, 24)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "componentName", [], "any", false, false, false, 24), "condition-builder")) : ("condition-builder")), "html", null, true);
            echo "\">
        <div class=\"row-fluid\">
            <div class=\"criteria-list-container filter-criteria\">
                <ul class=\"criteria-list\">
                    <li class=\"option\" data-criteria=\"condition-item\"
                        data-module=\"";
            // line 29
            echo twig_escape_filter($this->env, ($context["fieldConditionModule"] ?? null), "html", null, true);
            echo "\"
                        data-options=\"";
            // line 30
            echo twig_escape_filter($this->env, json_encode(($context["fieldConditionOptions"] ?? null)), "html", null, true);
            echo "\">
                        ";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.criteria.field_condition"), "html", null, true);
            echo "
                    </li>
                    <li class=\"option\" data-criteria=\"conditions-group\">
                        ";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.criteria.conditions_group"), "html", null, true);
            echo "
                    </li>
                </ul>
            </div>
            <div class=\"condition-container\" data-ignore-form-state-change>
                <div class=\"drag-n-drop-hint\"><div>";
            // line 39
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.drag_n_drop_hint"), "html", null, true);
            echo "</div></div>
            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 45
    public function macro_query_designer_column_list($__attr__ = null, $__showItems__ = [0 => "column", 1 => "label", 2 => "function", 3 => "sorting", 4 => "action"], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attr" => $__attr__,
            "showItems" => $__showItems__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 46
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroQueryDesigner/macros.html.twig", 46)->unwrap();
            // line 47
            echo "    ";
            $macros["queryDesignerMacros"] = $this;
            // line 48
            echo "    ";
            $context["attr"] = twig_array_merge(((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 48)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 48), "")) : ("")) . " grid-container query-designer-grid-container query-designer-columns-grid-container"))]);
            // line 49
            echo "    <div";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr"] ?? null)], 49, $context, $this->getSourceContext());
            echo ">
        <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\" style=\"display: table;\">
            <thead>
            <tr>
                ";
            // line 53
            if (twig_in_filter("column", ($context["showItems"] ?? null))) {
                // line 54
                echo "                <th class=\"name-column\"><span>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.column"), "html", null, true);
                echo "</span></th>
                ";
            }
            // line 56
            echo "                ";
            if (twig_in_filter("label", ($context["showItems"] ?? null))) {
                // line 57
                echo "                <th class=\"label-column\"><span>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.label"), "html", null, true);
                echo "</span></th>
                ";
            }
            // line 59
            echo "                ";
            if (twig_in_filter("function", ($context["showItems"] ?? null))) {
                // line 60
                echo "                <th class=\"function-column\"><span>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.function"), "html", null, true);
                echo "</span></th>
                ";
            }
            // line 62
            echo "                ";
            if (twig_in_filter("sorting", ($context["showItems"] ?? null))) {
                // line 63
                echo "                <th class=\"sorting-column\"><span>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.sorting"), "html", null, true);
                echo "</span></th>
                ";
            }
            // line 65
            echo "                ";
            if (twig_in_filter("action", ($context["showItems"] ?? null))) {
                // line 66
                echo "                <th class=\"action-column\"><span>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.actions"), "html", null, true);
                echo "</span></th>
                ";
            }
            // line 68
            echo "            </tr>
            </thead>
            <tbody class=\"item-container\">
            </tbody>
        </table>
    </div>
    ";
            // line 74
            echo twig_call_macro($macros["queryDesignerMacros"], "macro_query_designer_column_template", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "rowId", [], "any", false, false, false, 74), ($context["showItems"] ?? null)], 74, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 77
    public function macro_query_designer_grouping_list($__attr__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attr" => $__attr__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 78
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroQueryDesigner/macros.html.twig", 78)->unwrap();
            // line 79
            echo "    ";
            $context["attr"] = twig_array_merge(((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 79)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 79), "")) : ("")) . " grid-container query-designer-grid-container query-designer-columns-grid-container"))]);
            // line 80
            echo "    <div";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr"] ?? null)], 80, $context, $this->getSourceContext());
            echo ">
        <div class=\"grid-container query-designer-grid-container query-designer-columns-grid-container\">
            <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\" style=\"display: table;\">
                <thead>
                <tr>
                    <th class=\"name-column\"><span>";
            // line 85
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.column"), "html", null, true);
            echo "</span></th>
                    <th class=\"action-column\"><span>";
            // line 86
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.actions"), "html", null, true);
            echo "</span></th>
                </tr>
                </thead>
                <tbody class=\"grouping-item-container\">
                </tbody>
            </table>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 96
    public function macro_query_designer_column_template($__id__ = null, $__showItems__ = [0 => "column", 1 => "label", 2 => "function", 3 => "sorting", 4 => "action"], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "id" => $__id__,
            "showItems" => $__showItems__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 97
            echo "    <script type=\"text/html\" id=\"";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "\">
        <tr data-cid=\"<%- cid %>\">
            ";
            // line 99
            if (twig_in_filter("column", ($context["showItems"] ?? null))) {
                // line 100
                echo "            <td class=\"name-cell<% if (obj.deleted) { %> deleted-field<% } %>\"><%= name %></td>
            ";
            }
            // line 102
            echo "            ";
            if (twig_in_filter("label", ($context["showItems"] ?? null))) {
                // line 103
                echo "            <td class=\"label-cell\"><%- label %></td>
            ";
            }
            // line 105
            echo "            ";
            if (twig_in_filter("function", ($context["showItems"] ?? null))) {
                // line 106
                echo "            <td class=\"function-cell\"><%= func %></td>
            ";
            }
            // line 108
            echo "            ";
            if (twig_in_filter("sorting", ($context["showItems"] ?? null))) {
                // line 109
                echo "            <td class=\"sorting-cell\"><%= sorting %></td>
            ";
            }
            // line 111
            echo "            ";
            if (twig_in_filter("action", ($context["showItems"] ?? null))) {
                // line 112
                echo "            <td class=\"action-cell\">
                ";
                // line 113
                ob_start(function () { return ''; });
                // line 114
                echo "                <a href=\"#\" class=\"btn btn-icon btn-lighter action no-hash edit-button\"
                        title=\"";
                // line 115
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.update_column"), "html", null, true);
                echo "\"
                        data-collection-action=\"edit\">
                    <span class=\"fa-pencil-square-o\" aria-hidden=\"true\"></span></a>
                <a href=\"#\" class=\"btn btn-icon btn-lighter action no-hash delete-button\"
                        title=\"";
                // line 119
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.delete_column"), "html", null, true);
                echo "\"
                        data-collection-action=\"delete\"
                        data-message=\"";
                // line 121
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.delete_column_confirmation"), "html", null, true);
                echo "\">
                    <span class=\"fa-trash-o\" aria-hidden=\"true\"></span></a>
                <span class=\"btn btn-icon btn-lighter\" title=\"";
                // line 123
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.move_column"), "html", null, true);
                echo "\">
                    <span class=\"fa-arrows-v handle\" aria-hidden=\"true\"></span></span>
                ";
                $___internal_parse_75_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 113
                echo twig_spaceless($___internal_parse_75_);
                // line 126
                echo "            </td>
            ";
            }
            // line 128
            echo "        </tr>
    </script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 132
    public function macro_query_designer_grouping_item_template($__id__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "id" => $__id__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 133
            echo "    <script type=\"text/html\" id=\"";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "\">
        <tr data-cid=\"<%- cid %>\">
            <td class=\"name-cell<% if (obj.deleted) { %> deleted-field<% } %>\"><%= name %></td>
            <td class=\"action-cell\">
                ";
            // line 137
            ob_start(function () { return ''; });
            // line 138
            echo "                <a href=\"#\" class=\"btn btn-icon btn-lighter action no-hash edit-button\"
                        title=\"";
            // line 139
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.update_column"), "html", null, true);
            echo "\"
                        data-collection-action=\"edit\">
                    <span class=\"fa-pencil-square-o\" aria-hidden=\"true\"></span></a>
                <a href=\"#\" class=\"btn btn-icon btn-lighter action no-hash delete-button\"
                        title=\"";
            // line 143
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.delete_column"), "html", null, true);
            echo "\"
                        data-collection-action=\"delete\"
                        data-message=\"";
            // line 145
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.delete_column_confirmation"), "html", null, true);
            echo "\">
                    <span class=\"fa-trash-o\" aria-hidden=\"true\"></span></a>
                <span class=\"btn btn-icon btn-lighter\" title=\"";
            // line 147
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.action.move_column"), "html", null, true);
            echo "\">
                    <span class=\"fa-arrows-v handle\" aria-hidden=\"true\"></span></span>
                ";
            $___internal_parse_76_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 137
            echo twig_spaceless($___internal_parse_76_);
            // line 150
            echo "            </td>
        </tr>
    </script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 155
    public function macro_query_designer_column_form($__form__ = null, $__attr__ = null, $__params__ = null, $__showItems__ = [0 => "column", 1 => "label", 2 => "function", 3 => "sorting", 4 => "action"], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "attr" => $__attr__,
            "params" => $__params__,
            "showItems" => $__showItems__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 156
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroQueryDesigner/macros.html.twig", 156)->unwrap();
            // line 157
            echo "    ";
            $context["attr"] = twig_array_merge(((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 158
($context["attr"] ?? null), "class", [], "any", true, true, false, 158)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 158), "")) : ("")) . " query-designer-form"))]);
            // line 160
            echo "    <div ";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr"] ?? null)], 160, $context, $this->getSourceContext());
            echo ">
        <div class=\"query-designer-row\">
            ";
            // line 163
            echo "            ";
            if (twig_in_filter("column", ($context["showItems"] ?? null))) {
                // line 164
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 164), 'row', ["label" => "oro.query_designer.form.column", "attr" => ["data-purpose" => "column-selector", "data-validation" => json_encode(["NotBlank" => null])]]);
                // line 170
                echo "
            ";
            }
            // line 172
            echo "            ";
            // line 173
            echo "            ";
            if (twig_in_filter("label", ($context["showItems"] ?? null))) {
                // line 174
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 174), 'row', ["label" => "oro.query_designer.form.label", "attr" => ["class" => "label-text", "data-purpose" => "label", "data-validation" => json_encode(["NotBlank" => null])]]);
                // line 181
                echo "
            ";
            }
            // line 183
            echo "            ";
            if (twig_in_filter("function", ($context["showItems"] ?? null))) {
                // line 184
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "func", [], "any", false, false, false, 184), 'row', ["label" => "oro.query_designer.form.function", "attr" => ["class" => "function-selector", "data-purpose" => "function-selector"]]);
                // line 190
                echo "
            ";
            }
            // line 192
            echo "            ";
            if (twig_in_filter("sorting", ($context["showItems"] ?? null))) {
                // line 193
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "sorting", [], "any", false, false, false, 193), 'row', ["label" => "oro.query_designer.form.sorting", "attr" => ["class" => "sorting-selector", "data-purpose" => "sorting-selector"]]);
                // line 199
                echo "
            ";
            }
            // line 201
            echo "            ";
            if (twig_in_filter("action", ($context["showItems"] ?? null))) {
                // line 202
                echo "            <div class=\"submit-cancel-buttons\">
                ";
                // line 203
                ob_start(function () { return ''; });
                // line 204
                echo "                ";
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["aCss" => "no-hash cancel-button column-form-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.cancel")]], 204, $context, $this->getSourceContext());
                // line 207
                echo "
                ";
                // line 208
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["visible" => false, "aCss" => "no-hash btn-success save-button column-form-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.save")]], 208, $context, $this->getSourceContext());
                // line 212
                echo "
                ";
                // line 213
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["visible" => false, "aCss" => "no-hash btn-primary add-button column-form-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.add")]], 213, $context, $this->getSourceContext());
                // line 217
                echo "
                ";
                $___internal_parse_77_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 203
                echo twig_spaceless($___internal_parse_77_);
                // line 219
                echo "            </div>
            ";
            }
            // line 221
            echo "        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 225
    public function macro_query_designer_grouping_form($__form__ = null, $__attr__ = null, $__params__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "attr" => $__attr__,
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 226
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroQueryDesigner/macros.html.twig", 226)->unwrap();
            // line 227
            echo "    ";
            $context["attr"] = twig_array_merge(((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 227)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 227), "")) : ("")) . " query-designer-grouping-form query-designer-form"))]);
            // line 228
            echo "    <div";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr"] ?? null)], 228, $context, $this->getSourceContext());
            echo ">
        <div class=\"query-designer-row\">
            ";
            // line 231
            echo "            ";
            // line 232
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "columnNames", [], "any", false, false, false, 232), 'row', ["label" => "oro.query_designer.form.grouping_columns", "attr" => ["data-purpose" => "column-selector", "data-validation" => json_encode(["NotBlank" => null])]]);
            // line 238
            echo "
            <div class=\"submit-cancel-buttons\">
                ";
            // line 240
            ob_start(function () { return ''; });
            // line 241
            echo "                ";
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["aCss" => "no-hash cancel-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.cancel")]], 241, $context, $this->getSourceContext());
            // line 244
            echo "
                ";
            // line 245
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["visible" => false, "aCss" => "no-hash btn-success save-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.save")]], 245, $context, $this->getSourceContext());
            // line 249
            echo "
                ";
            // line 250
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["visible" => false, "aCss" => "no-hash btn-primary add-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.add")]], 250, $context, $this->getSourceContext());
            // line 254
            echo "
                ";
            $___internal_parse_78_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 240
            echo twig_spaceless($___internal_parse_78_);
            // line 256
            echo "            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 261
    public function macro_query_designer_date_grouping_form($__form__ = null, $__params__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 262
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroQueryDesigner/macros.html.twig", 262)->unwrap();
            // line 263
            echo "    ";
            $context["attr"] = twig_array_merge(((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 263)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 263), "")) : ("")) . " query-designer-grouping-form clearfix"))]);
            // line 264
            echo "    <div";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr"] ?? null)], 264, $context, $this->getSourceContext());
            echo " ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["view" => "oroquerydesigner/js/app/views/date-grouping-view"]]], 264, $context, $this->getSourceContext());
            // line 269
            echo ">
        ";
            // line 270
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "useDateGroupFilter", [], "any", false, false, false, 270), 'row');
            echo "
        ";
            // line 271
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "fieldName", [], "any", false, false, false, 271), 'row', ["attr" => ["data-purpose" => "date-grouping-selector"]]);
            // line 275
            echo "
        ";
            // line 276
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "useSkipEmptyPeriodsFilter", [], "any", false, false, false, 276), 'row');
            echo "
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 280
    public function macro_query_designer_column_chain_template($__id__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "id" => $__id__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 281
            echo "    ";
            ob_start(function () { return ''; });
            // line 282
            echo "    <script type=\"text/html\" id=\"";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "\">
        <span class=\"entity-field-path\">
        <% _.each(obj, function (item, index, list) { %>
            <% if (index === 0) { %>
                <span><%- item.entity.label %></span>
            <% }  else { %>
                <% if (index !== list.length - 1) { %>
                    <span><%- item.field.label %></span>
                <% } else { %>
                    <b><%- item.field.label %></b>
                <% } %>
            <% } %>
        <% }) %>
        </span>
    </script>
    ";
            $___internal_parse_79_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 281
            echo twig_spaceless($___internal_parse_79_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroQueryDesigner/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  719 => 281,  699 => 282,  696 => 281,  683 => 280,  671 => 276,  668 => 275,  666 => 271,  662 => 270,  659 => 269,  654 => 264,  651 => 263,  648 => 262,  634 => 261,  622 => 256,  620 => 240,  616 => 254,  614 => 250,  611 => 249,  609 => 245,  606 => 244,  603 => 241,  601 => 240,  597 => 238,  594 => 232,  592 => 231,  586 => 228,  583 => 227,  580 => 226,  565 => 225,  554 => 221,  550 => 219,  548 => 203,  544 => 217,  542 => 213,  539 => 212,  537 => 208,  534 => 207,  531 => 204,  529 => 203,  526 => 202,  523 => 201,  519 => 199,  516 => 193,  513 => 192,  509 => 190,  506 => 184,  503 => 183,  499 => 181,  496 => 174,  493 => 173,  491 => 172,  487 => 170,  484 => 164,  481 => 163,  475 => 160,  473 => 158,  471 => 157,  468 => 156,  452 => 155,  440 => 150,  438 => 137,  432 => 147,  427 => 145,  422 => 143,  415 => 139,  412 => 138,  410 => 137,  402 => 133,  389 => 132,  378 => 128,  374 => 126,  372 => 113,  366 => 123,  361 => 121,  356 => 119,  349 => 115,  346 => 114,  344 => 113,  341 => 112,  338 => 111,  334 => 109,  331 => 108,  327 => 106,  324 => 105,  320 => 103,  317 => 102,  313 => 100,  311 => 99,  305 => 97,  291 => 96,  273 => 86,  269 => 85,  260 => 80,  257 => 79,  254 => 78,  241 => 77,  230 => 74,  222 => 68,  216 => 66,  213 => 65,  207 => 63,  204 => 62,  198 => 60,  195 => 59,  189 => 57,  186 => 56,  180 => 54,  178 => 53,  170 => 49,  167 => 48,  164 => 47,  161 => 46,  147 => 45,  133 => 39,  125 => 34,  119 => 31,  115 => 30,  111 => 29,  103 => 24,  99 => 23,  95 => 21,  93 => 20,  91 => 18,  89 => 17,  88 => 14,  87 => 11,  86 => 9,  85 => 8,  83 => 7,  80 => 6,  78 => 5,  76 => 2,  63 => 1,  58 => 279,  55 => 260,  52 => 224,  49 => 154,  46 => 131,  43 => 95,  40 => 76,  37 => 44,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroQueryDesigner/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/QueryDesignerBundle/Resources/views/macros.html.twig");
    }
}
