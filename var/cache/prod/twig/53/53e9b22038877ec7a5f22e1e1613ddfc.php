<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/invitations.html.twig */
class __TwigTemplate_9f4d96e8b35e5488ae87fc9e4c2bbed0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "
";
        // line 47
        echo "
";
        // line 57
        echo "
";
        // line 86
        echo "
";
    }

    // line 2
    public function macro_calendar_event_invitation($__parentEvent__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parentEvent" => $__parentEvent__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 3
            echo "    ";
            $macros["invitations"] = $this;
            // line 4
            echo "    <div class=\"row-fluid\">
        <div class=\"responsive-block\">
            <ul class=\"user-status-list list-group\">
                ";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parentEvent"] ?? null), "attendees", [], "any", false, false, false, 7));
            foreach ($context['_seq'] as $context["_key"] => $context["attendee"]) {
                // line 8
                echo "                    ";
                echo twig_call_macro($macros["invitations"], "macro_build_invitation_link", [$context["attendee"]], 8, $context, $this->getSourceContext());
                echo "
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendee'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "            </ul>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 16
    public function macro_build_invitation_link($__attendee__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attendee" => $__attendee__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 17
            echo "    ";
            $macros["invitations"] = $this;
            // line 18
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/invitations.html.twig", 18)->unwrap();
            // line 19
            echo "        ";
            $context["invitationStatus"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "statusCode", [], "any", false, false, false, 19);
            // line 20
            echo "        ";
            $context["invitationClass"] = twig_call_macro($macros["invitations"], "macro_get_invitatition_status_class", [($context["invitationStatus"] ?? null)], 20, $context, $this->getSourceContext());
            // line 21
            echo "        <li class=\"list-group-item\">
            <i ";
            // line 22
            if (($context["invitationClass"] ?? null)) {
                echo "class=\"";
                echo twig_escape_filter($this->env, ($context["invitationClass"] ?? null), "html", null, true);
                echo "\" title=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "status", [], "any", false, false, false, 22), "name", [], "any", false, false, false, 22), "html", null, true);
                echo "\"";
            }
            echo "></i>
            <span class=\"list-group-item-text\">
                ";
            // line 24
            $context["avatar"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "user", [], "any", false, false, false, 24)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "user", [], "any", false, false, false, 24), "avatar", [], "any", false, false, false, 24)) : (null));
            // line 25
            echo "                ";
            $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroCalendar/invitations.html.twig", 25)->display(twig_array_merge($context, ["sources" => (($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(            // line 26
($context["avatar"] ?? null), "avatar_xsmall")) ? ($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(($context["avatar"] ?? null), "avatar_xsmall")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroui/img/avatar-xsmall.png")))]));
            // line 28
            echo "                ";
            if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_user_view") && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "user", [], "any", false, false, false, 28))) {
                // line 29
                echo "                    ";
                echo twig_call_macro($macros["UI"], "macro_link", [["label" => _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 30
($context["attendee"] ?? null), "user", [], "any", false, false, false, 30)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")), "path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 31
($context["attendee"] ?? null), "user", [], "any", false, false, false, 31), "id", [], "any", false, false, false, 31)])]], 29, $context, $this->getSourceContext());
                // line 32
                echo "
                ";
            } else {
                // line 34
                echo "                    ";
                $context["attendeeName"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "displayName", [], "any", false, false, false, 34);
                // line 35
                echo "                    ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "email", [], "any", false, false, false, 35)) {
                    // line 36
                    echo "                        ";
                    $context["attendeeName"] = ((($context["attendeeName"] ?? null)) ? ((((($context["attendeeName"] ?? null) . " (") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "email", [], "any", false, false, false, 36)) . ")")) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "email", [], "any", false, false, false, 36)));
                    // line 37
                    echo "                    ";
                }
                // line 38
                echo "                    ";
                echo twig_escape_filter($this->env, ($context["attendeeName"] ?? null), "html", null, true);
                echo "
                ";
            }
            // line 40
            echo "                ";
            $context["typeId"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "type", [], "any", false, false, false, 40)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attendee"] ?? null), "type", [], "any", false, false, false, 40), "id", [], "any", false, false, false, 40)) : (null));
            // line 41
            if (($context["typeId"] ?? null)) {
                // line 42
                echo "                - ";
                echo twig_call_macro($macros["invitations"], "macro_get_attendee_type", [($context["typeId"] ?? null)], 42, $context, $this->getSourceContext());
                echo "
                ";
            }
            // line 44
            echo "</span>
        </li>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 48
    public function macro_get_invitatition_badge_class($__invitationStatus__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "invitationStatus" => $__invitationStatus__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 49
            if ((($context["invitationStatus"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_TENTATIVE"))) {
                // line 50
                echo "tentatively";
            } elseif ((            // line 51
($context["invitationStatus"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_ACCEPTED"))) {
                // line 52
                echo "enabled";
            } elseif ((            // line 53
($context["invitationStatus"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED"))) {
                // line 54
                echo "disabled";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 58
    public function macro_get_invitatition_status_class($__invitationStatus__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "invitationStatus" => $__invitationStatus__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 59
            if ((($context["invitationStatus"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_NONE"))) {
                // line 60
                echo "fa-reply";
            } elseif ((            // line 61
($context["invitationStatus"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_TENTATIVE"))) {
                // line 62
                echo "fa-question";
            } elseif ((            // line 63
($context["invitationStatus"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_ACCEPTED"))) {
                // line 64
                echo "fa-check";
            } elseif ((            // line 65
($context["invitationStatus"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED"))) {
                // line 66
                echo "fa-close";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 70
    public function macro_calendar_event_invitation_status($__statusCode__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "statusCode" => $__statusCode__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 71
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((("oro.calendar.calendarevent.statuses." . ($context["statusCode"] ?? null)) . ".label")), "html", null, true);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 74
    public function macro_calendar_event_invitation_going_status($__statusCode__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "statusCode" => $__statusCode__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 75
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((("oro.calendar.calendarevent.action.going_status." . ($context["statusCode"] ?? null)) . ".label")), "html", null, true);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 78
    public function macro_notify_attendees_component(...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 79
            echo "    ";
            if ($this->extensions['Oro\Bundle\CalendarBundle\Twig\AttendeesExtension']->isAttendeesInvitationEnabled()) {
                // line 80
                echo "    <div style=\"display: none\"
         data-page-component-module=\"oroui/js/app/components/view-component\"
         data-page-component-options=\"";
                // line 82
                echo twig_escape_filter($this->env, json_encode(["view" => "orocalendar/js/app/views/attendee-notifier-view"]), "html", null, true);
                echo "\">
    </div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 87
    public function macro_get_attendee_type($__typeId__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "typeId" => $__typeId__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 88
            if ((($context["typeId"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::TYPE_ORGANIZER"))) {
                // line 89
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.attendee.type.organizer.label"), "html", null, true);
            } elseif ((            // line 90
($context["typeId"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::TYPE_OPTIONAL"))) {
                // line 91
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.attendee.type.optional.label"), "html", null, true);
            } elseif ((            // line 92
($context["typeId"] ?? null) == twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::TYPE_REQUIRED"))) {
                // line 93
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.attendee.type.required.label"), "html", null, true);
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroCalendar/invitations.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 93,  361 => 92,  359 => 91,  357 => 90,  355 => 89,  353 => 88,  340 => 87,  327 => 82,  323 => 80,  320 => 79,  308 => 78,  299 => 75,  286 => 74,  277 => 71,  264 => 70,  254 => 66,  252 => 65,  250 => 64,  248 => 63,  246 => 62,  244 => 61,  242 => 60,  240 => 59,  227 => 58,  217 => 54,  215 => 53,  213 => 52,  211 => 51,  209 => 50,  207 => 49,  194 => 48,  183 => 44,  177 => 42,  175 => 41,  172 => 40,  166 => 38,  163 => 37,  160 => 36,  157 => 35,  154 => 34,  150 => 32,  148 => 31,  147 => 30,  145 => 29,  142 => 28,  140 => 26,  138 => 25,  136 => 24,  125 => 22,  122 => 21,  119 => 20,  116 => 19,  113 => 18,  110 => 17,  97 => 16,  85 => 10,  76 => 8,  72 => 7,  67 => 4,  64 => 3,  51 => 2,  46 => 86,  43 => 57,  40 => 47,  37 => 14,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/invitations.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/invitations.html.twig");
    }
}
