<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/inline-actions.scss */
class __TwigTemplate_3a33969e148666be4e42b83a2674df9b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

/* stylelint-disable no-descending-specificity */

@use 'sass:list';

.inline-actions-element {
    display: inline-flex;
    align-items: flex-start;
    margin: (-\$content-padding-small * .5 - 2px) (-\$content-padding-small * .5) (-\$content-padding-small * .5 - 3px);
    padding: (\$content-padding-small * .5) (\$content-padding-small * .5);

    transition: \$hover-transition;

    .inline-actions-element_actions {
        visibility: hidden;
    }

    &:hover {
        background: \$inline-action-background;

        .inline-actions-element_actions {
            visibility: visible;
        }
    }

    &.inline-actions-element_no-actions:hover {
        background: transparent;
    }

    .inline-actions-element_wrapper {
        word-break: break-all;
        display: inline-block;
        padding: (\$content-padding-small * .5) 0 (\$content-padding-small * .5) (\$content-padding-small * .5);
        margin: (-\$content-padding-small * .5) 0 (-\$content-padding-small * .5) (-\$content-padding-small * .5);
    }

    &.truncate {
        display: inline-flex;

        .inline-actions-element_wrapper {
            padding: 0;
            margin: 0;

            @include text-line-truncate(200px, true);
        }
    }
}

.inline-actions-element_actions {
    display: inline-flex;
    align-items: center;
    vertical-align: top;
    min-height: 20px;
    line-height: 1.15;

    .btn {
        font: 0/0 a, sans-serif;
        width: 20px;
        height: 20px;
        padding: 0;
        text-align: center;

        &,
        &:focus,
        &:active,
        &:hover {
            color: transparent;
            text-shadow: none;
            border-color: transparent;
            background: transparent none;
            box-shadow: none;
        }

        [class^='fa-'],
        [class*=' fa-'] {
            width: 14px;
            height: 20px;
            margin: 0 1px;
            line-height: 1;

            &::before {
                font: list.slash(\$base-font-size, 26px) \$fa-font-family;
                color: \$inline-action-color;
                font-size: 15px;
                line-height: 22px;
                height: 22px;
            }

            &.hide-text {
                @include hide-text();
            }
        }
    }

    .attribute-item & {
        .inline-actions-btn {
            line-height: 1;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/inline-actions.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/inline-actions.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/inline-actions.scss");
    }
}
