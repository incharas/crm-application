<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/vue-app-component.js */
class __TwigTemplate_36ffe8823f69fe038beee8f871548c86 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import {createApp} from 'vue';
import BaseComponent from 'oroui/js/app/components/base/component';
import loadModules from 'oroui/js/app/services/load-modules';

/**
 * Creates a Vue app with passed 'vue_app' module and mount in _sourceElement
 */
const VueAppComponent = BaseComponent.extend({
    app: null,

    appContainerElement: null,

    constructor: function VueAppComponent(...args) {
        VueAppComponent.__super__.constructor.apply(this, args);
    },

    initialize(options) {
        this._initVueApp(options);
    },

    async _initVueApp(options = {}) {
        const {
            _sourceElement,
            _subPromises,
            name,
            vueApp,
            ...props
        } = options || {};

        this.appContainerElement = _sourceElement[0];

        this._deferredInit();

        const App = await this._loadModule(vueApp);

        if (this.disposed) {
            this._resolveDeferredInit();
            return;
        }

        this.createApp(App, props);

        this._resolveDeferredInit();
    },

    beforeAppMount() {},

    createApp(App, props) {
        this.app = createApp(App, props);

        this.beforeAppMount();
        this.appMount(this.appContainerElement);
    },

    async _loadModule(vueAppModule) {
        if (!vueAppModule) {
            throw new Error('Missing app module name');
        }

        return await loadModules(vueAppModule);
    },

    appMount(mountElement) {
        if (!mountElement) {
            return console.warn('Missing DOM element to mount app instance');
        }

        if (!this.app) {
            return;
        }

        this.app.mount(mountElement);
    },

    dispose() {
        if (this.disposed) {
            return;
        }

        this.app.unmount();
        delete this.app;

        VueAppComponent.__super__.dispose.call(this);
    }
});

export default VueAppComponent;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/vue-app-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/vue-app-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/vue-app-component.js");
    }
}
