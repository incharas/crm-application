<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMessageQueue/Job/childJobs.html.twig */
class __TwigTemplate_9b5b75b933acc516708d72ce08892b54 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'stats' => [$this, 'block_stats'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroMessageQueue/Job/childJobs.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "%id%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 4)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroMessageQueue/Job/childJobs.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMessageQueue/Job/childJobs.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "

    <span class=\"page-title__status\">
        ";
        // line 12
        $this->loadTemplate("@OroMessageQueue/Job/Datagrid/status.html.twig", "@OroMessageQueue/Job/childJobs.html.twig", 12)->display(twig_array_merge($context, ["record" => ["rootEntity" => ($context["entity"] ?? null)]]));
        // line 13
        echo "
        ";
        // line 14
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "unique", [], "any", false, false, false, 14)) {
            // line 15
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.message_queue_job.header.unique"), "info"], 15, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 17
        echo "
        ";
        // line 18
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "interrupted", [], "any", false, false, false, 18)) {
            // line 19
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.message_queue_job.header.interrupted"), "info"], 19, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 21
        echo "    </span>
";
    }

    // line 24
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.message_queue_job.header.startedAt"), "html", null, true);
        echo ": ";
        (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startedAt", [], "any", true, true, false, 25) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startedAt", [], "any", false, false, false, 25))) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startedAt", [], "any", false, false, false, 25)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.message_queue_job.header.stoppedAt"), "html", null, true);
        echo ": ";
        (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "stoppedAt", [], "any", true, true, false, 26) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "stoppedAt", [], "any", false, false, false, 26))) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "stoppedAt", [], "any", false, false, false, 26)), "html", null, true))) : (print ("N/A")));
        echo "</li>
";
    }

    // line 29
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 31
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_message_queue_root_jobs"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.message_queue_job.header.root_jobs"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.message_queue_job.header.name", ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["entity"] ?? null), "name", [], "any", false, false, false, 34)])];
        // line 36
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 39
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "    ";
        $macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroMessageQueue/Job/childJobs.html.twig", 40)->unwrap();
        // line 41
        echo "
    ";
        // line 42
        $context["gridName"] = "oro_message_queue_child_jobs";
        // line 43
        echo "    ";
        $context["params"] = ["root_job_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 43)];
        // line 44
        echo "    ";
        $context["renderParams"] = ["enableFullScreenLayout" => true, "enableViews" => true, "showViewsInNavbar" => true];
        // line 49
        echo "
    ";
        // line 50
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ($context["params"] ?? null), ($context["renderParams"] ?? null)], 50, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroMessageQueue/Job/childJobs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 50,  153 => 49,  150 => 44,  147 => 43,  145 => 42,  142 => 41,  139 => 40,  135 => 39,  128 => 36,  126 => 34,  125 => 31,  123 => 30,  119 => 29,  111 => 26,  104 => 25,  100 => 24,  95 => 21,  89 => 19,  87 => 18,  84 => 17,  78 => 15,  76 => 14,  73 => 13,  71 => 12,  65 => 9,  62 => 8,  59 => 7,  55 => 6,  50 => 1,  48 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMessageQueue/Job/childJobs.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MessageQueueBundle/Resources/views/Job/childJobs.html.twig");
    }
}
