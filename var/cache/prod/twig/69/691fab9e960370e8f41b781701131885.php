<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAction/Widget/viewNavButtons.html.twig */
class __TwigTemplate_91acc1a0bd862ec86bced101f1949a12 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["params"] = twig_array_merge($this->extensions['Oro\Bundle\ActionBundle\Twig\OperationExtension']->getActionParameters($context), ["group" => "view_navButtons"]);
        // line 2
        echo "
";
        // line 3
        $this->loadTemplate("@OroAction/Widget/_widget.html.twig", "@OroAction/Widget/viewNavButtons.html.twig", 3)->display($context);
    }

    public function getTemplateName()
    {
        return "@OroAction/Widget/viewNavButtons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAction/Widget/viewNavButtons.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActionBundle/Resources/views/Widget/viewNavButtons.html.twig");
    }
}
