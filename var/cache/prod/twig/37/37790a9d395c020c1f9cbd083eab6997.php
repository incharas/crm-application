<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/widget-picker/widget-picker-modal-template.html */
class __TwigTemplate_0f868d6e8cda95ff691abc56c61906ba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget-picker__filter\" data-role=\"widget-picker-filter\"></div>
<div class=\"widget-picker__container\" data-role=\"widget-picker-container\">
    <div class=\"widget-picker__results\" data-role=\"widget-picker-results\"></div>
    <div class=\"no-data\" data-role=\"widget-picker-no-results-found\">
        <%- _.__('oro.ui.widget_picker.filter.search_no_found') %>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/widget-picker/widget-picker-modal-template.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/widget-picker/widget-picker-modal-template.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/widget-picker/widget-picker-modal-template.html");
    }
}
