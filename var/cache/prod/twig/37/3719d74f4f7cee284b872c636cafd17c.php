<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/searchResult.html.twig */
class __TwigTemplate_564fb5f53211d63e9dd27acad7ee526f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "@OroSearch/Search/searchResultItem.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/searchResult.html.twig", 8)->unwrap();
        // line 9
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/searchResult.html.twig", 9)->unwrap();
        // line 11
        $context["iconType"] = "envelope";
        // line 13
        $context["recordUrl"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["indexer_item"] ?? null), "recordUrl", [], "any", false, false, false, 13);
        // line 14
        $context["title"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["indexer_item"] ?? null), "selectedData", [], "any", false, false, false, 14), "name", [], "any", false, false, false, 14);
        // line 16
        $context["entityType"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.entity_label");
        // line 18
        $context["entityInfo"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.sent_at.label"), "value" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 19
($context["entity"] ?? null), "email", [], "any", false, false, false, 19), "sentAt", [], "any", false, false, false, 19))], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.from_name.label"), "value" => twig_call_macro($macros["EA"], "macro_email_address", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 20
($context["entity"] ?? null), "email", [], "any", false, false, false, 20), "fromEmailAddress", [], "any", false, false, false, 20), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "email", [], "any", false, false, false, 20), "fromName", [], "any", false, false, false, 20)], 20, $context, $this->getSourceContext())], 2 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.to.label"), "value" => twig_call_macro($macros["EA"], "macro_recipient_email_addresses", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 21
($context["entity"] ?? null), "email", [], "any", false, false, false, 21), "recipients", [0 => "to"], "method", false, false, false, 21)], 21, $context, $this->getSourceContext())], 3 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailuser.owner.label"), "value" => twig_call_macro($macros["UI"], "macro_entityOwnerLink", [        // line 22
($context["entity"] ?? null), false], 22, $context, $this->getSourceContext())]];
        // line 6
        $this->parent = $this->loadTemplate("@OroSearch/Search/searchResultItem.html.twig", "@OroEmail/Email/searchResult.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/searchResult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 6,  57 => 22,  56 => 21,  55 => 20,  54 => 19,  53 => 18,  51 => 16,  49 => 14,  47 => 13,  45 => 11,  43 => 9,  41 => 8,  34 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/searchResult.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/searchResult.html.twig");
    }
}
