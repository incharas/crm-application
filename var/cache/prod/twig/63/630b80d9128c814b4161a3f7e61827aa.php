<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Calendar/Menu/removeCalendar.html.twig */
class __TwigTemplate_6f353866339b47ac104fb564e70aed9a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["oro_menu"] = $this->macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroCalendar/Calendar/Menu/removeCalendar.html.twig", 1)->unwrap();
        // line 2
        echo "
<% if (removable) { %>";
        // line 4
        ob_start(function () { return ''; });
        // line 5
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "icon"], "method", false, false, false, 5)) {
            // line 6
            echo "<span class=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "icon"], "method", false, false, false, 6), "html", null, true);
            echo "\" aria-hidden=\"true\"></span>";
        }
        // line 8
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "allow_safe_labels", [], "any", false, false, false, 8) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "safe_label", 1 => false], "method", false, false, false, 8))) {
            // line 9
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "label", [], "any", false, false, false, 9)));
        } else {
            // line 11
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "label", [], "any", false, false, false, 11), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "translateParams", 1 => []], "method", false, false, false, 11), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "translateDomain", 1 => "messages"], "method", false, false, false, 11)), "html", null, true);
        }
        $context["Label"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 14
        echo "<li ";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["itemAttributes"] ?? null)], 14, $context, $this->getSourceContext());
        echo ">";
        // line 15
        $context["linkAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "linkAttributes", [], "any", false, false, false, 15);
        // line 16
        echo "<a href=\"#\" role=\"button\" ";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 16, $context, $this->getSourceContext());
        echo ">";
        echo twig_escape_filter($this->env, ($context["Label"] ?? null), "html", null, true);
        echo "</a>
    </li>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Calendar/Menu/removeCalendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 16,  64 => 15,  60 => 14,  56 => 11,  53 => 9,  51 => 8,  46 => 6,  44 => 5,  42 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Calendar/Menu/removeCalendar.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Calendar/Menu/removeCalendar.html.twig");
    }
}
