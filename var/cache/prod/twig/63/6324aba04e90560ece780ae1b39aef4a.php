<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/shortcuts.html.twig */
class __TwigTemplate_1b69508cd8e525733561634f60d76534 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'list' => [$this, 'block_list'],
            'item' => [$this, 'block_item'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/shortcuts.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_list($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/Menu/shortcuts.html.twig", 4)->unwrap();
        // line 5
        echo "    ";
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 5) &&  !(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 5) === 0)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 5))) {
            // line 6
            echo "            ";
            $context["togglerId"] = uniqid("dropdown-");
            // line 7
            echo "            <div class=\"dropdown header-dropdown-shortcut\" data-layout=\"separate\"
                ";
            // line 8
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oronavigation/js/app/views/shortcuts-view"]], 8, $context, $this->getSourceContext());
            echo ">
                <button class=\"dropdown-toggle dropdown-toggle--no-caret\" type=\"button\" data-toggle=\"dropdown\" id=\"";
            // line 9
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\"
                    aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.title"), "html", null, true);
            echo "\"
                    title=\"";
            // line 11
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.title"), "html", null, true);
            echo "\"
                    data-prevent-close-on-menu-click=\"true\">
                    <span class=\"fa-share-square\" aria-hidden=\"true\"></span>
                </button>
                <ul class=\"dropdown-menu\" aria-labelledby=\"";
            // line 15
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\">
                    <li class=\"nav-header nav-header-title\">";
            // line 16
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.title"), "html", null, true);
            echo "
                    ";
            // line 17
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                // line 18
                echo "                        <button class=\"btn-link btn-close fa-close hide-text\" data-role=\"close\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Close"), "html", null, true);
                echo "</button>
                    ";
            }
            // line 20
            echo "                    </li>
                    <li class=\"nav-content\">
                        <div class=\"shortcut-container\">
                            <input type=\"text\"
                                   class=\"header-dropdown-shortcut__search\"
                                   placeholder=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.placeholder"), "html", null, true);
            echo "\"
                                   aria-label=\"";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.aria_label"), "html", null, true);
            echo "\"
                                   data-source-url=";
            // line 27
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "source", [], "any", false, false, false, 27));
            echo "
                                   data-role=\"shortcut-search\"
                                   data-clearable
                                   data-placeholder-icon=\"fa-search\"
                                   data-entity-class=\"";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 31), true), "html", null, true);
            echo "\"
                                   data-entity-id=\"";
            // line 32
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 32), "id", [], "any", false, false, false, 32), "html", null, true);
            echo "\"
                            >
                            <div class=\"clearfix\">
                                <div class=\"extra-small\">";
            // line 35
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.example"), "html", null, true);
            echo "
                                    <a href=\"";
            // line 36
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "details", [], "any", false, false, false, 36), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.see_all"), "html", null, true);
            echo "</a>
                                </div>
                            </div>
                        </div>
                    </li>
                    <li class=\"nav-header\">";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.shortcuts.most_used"), "html", null, true);
            echo "</li>
                    ";
            // line 42
            $this->displayBlock("children", $context, $blocks);
            echo "
                </ul>
            </div>
    ";
        }
    }

    // line 48
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 49), "is_custom_action", [], "any", true, true, false, 49)) {
            // line 50
            $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => "dropdown-item"]);
            // line 51
            echo "        ";
            $this->displayBlock("item_renderer", $context, $blocks);
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/shortcuts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 51,  164 => 50,  161 => 49,  157 => 48,  148 => 42,  144 => 41,  134 => 36,  130 => 35,  124 => 32,  120 => 31,  113 => 27,  109 => 26,  105 => 25,  98 => 20,  92 => 18,  90 => 17,  86 => 16,  82 => 15,  75 => 11,  71 => 10,  67 => 9,  63 => 8,  60 => 7,  57 => 6,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/shortcuts.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/shortcuts.html.twig");
    }
}
