<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/layouts/default/dialog/dialog.html.twig */
class __TwigTemplate_edae5e5596f7691e04e29b0a4127f987 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_root_widget' => [$this, 'block__root_widget'],
            '_widget_content_widget' => [$this, 'block__widget_content_widget'],
            '_widget_actions_widget' => [$this, 'block__widget_actions_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_root_widget', $context, $blocks);
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('_widget_content_widget', $context, $blocks);
        // line 17
        echo "
";
        // line 18
        $this->displayBlock('_widget_actions_widget', $context, $blocks);
    }

    // line 1
    public function block__root_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $this->displayBlock("container_widget", $context, $blocks);
        echo "
";
    }

    // line 5
    public function block__widget_content_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $context["pageComponentOptions"] = ["_wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["app"] ?? null), "request", [], "any", false, false, false, 7), "get", [0 => "_wid"], "method", false, false, false, 7)];
        // line 9
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 10
($context["attr"] ?? null), "class", [], "any", true, true, false, 10)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 10), "")) : ("")) . "widget-content ") . ((($context["class_prefix"] ?? null)) ? (($context["class_prefix"] ?? null)) : (""))), "data-page-component-options" => json_encode(twig_array_merge(        // line 11
($context["pageComponentOptions"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "data-page-component-options", [], "array", true, true, false, 11)) ? (_twig_default_filter((($__internal_compile_0 = ($context["attr"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["data-page-component-options"] ?? null) : null), [])) : ([]))))]);
        // line 13
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 14
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 18
    public function block__widget_actions_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 20
($context["attr"] ?? null), "class", [], "any", true, true, false, 20)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 20), "")) : ("")) . "widget-actions")]);
        // line 22
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 23
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/layouts/default/dialog/dialog.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  103 => 23,  98 => 22,  96 => 20,  94 => 19,  90 => 18,  83 => 14,  78 => 13,  76 => 11,  75 => 10,  73 => 9,  71 => 7,  69 => 6,  65 => 5,  58 => 2,  54 => 1,  50 => 18,  47 => 17,  45 => 5,  42 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/layouts/default/dialog/dialog.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/dialog/dialog.html.twig");
    }
}
