<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/dots_menu.html.twig */
class __TwigTemplate_ae9a266928e06c6fdb2a47e9f1c0aa23 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'root' => [$this, 'block_root'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/dots_tabs.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/dots_tabs.html.twig", "@OroNavigation/Menu/dots_menu.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        if (((($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 4)) &&  !(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 4) === 0)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 4))) {
            // line 5
            echo "    ";
            $context["togglerId"] = uniqid("dropdown-");
            // line 6
            echo "    <li class=\"dot-menu dropdown\">
        <a href=\"#\" role=\"button\" id=\"";
            // line 7
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\" data-toggle=\"dropdown\" class=\"dropdown-toggle dropdown-toggle--no-caret\"
           data-prevent-close-on-menu-click=\"true\"
           title=\"";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menu.recent_pages.label"), "html", null, true);
            echo "\"
           aria-label=\"";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menu.recent_pages.label"), "html", null, true);
            echo "\" aria-haspopup=\"true\" aria-expanded=\"false\">
            <span class=\"fa-bars\" aria-hidden=\"true\"></span>
        </a>
        <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"";
            // line 13
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\"  tabindex=\"0\">
            <div class=\"tabbable tabs-left\">
                ";
            // line 15
            $this->displayBlock("navbar_tabs", $context, $blocks);
            echo "
                ";
            // line 16
            $this->displayBlock("navbar_tabs_content", $context, $blocks);
            echo "
            </div>
        </div>
    </li>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/dots_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  83 => 16,  79 => 15,  74 => 13,  68 => 10,  64 => 9,  59 => 7,  56 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/dots_menu.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/dots_menu.html.twig");
    }
}
