<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/macros.html.twig */
class __TwigTemplate_1bb04bd43d96d4b6af7a5cd5826db839 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 108
        echo "
";
    }

    // line 1
    public function macro_renderTransitionButton($__workflow__ = null, $__transition__ = null, $__workflowItem__ = null, $__transitionData__ = null, $__onlyLink__ = null, $__noIcon__ = null, $__noIconText__ = null, $__aClass__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "workflow" => $__workflow__,
            "transition" => $__transition__,
            "workflowItem" => $__workflowItem__,
            "transitionData" => $__transitionData__,
            "onlyLink" => $__onlyLink__,
            "noIcon" => $__noIcon__,
            "noIconText" => $__noIconText__,
            "aClass" => $__aClass__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/macros.html.twig", 2)->unwrap();
            // line 3
            echo "
    ";
            // line 4
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "displayType", [], "any", false, false, false, 4) == "dialog") && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "hasForm", [], "method", false, false, false, 4))) {
                // line 5
                echo "        ";
                if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transitionData"] ?? null), "dialog-url", [], "array", true, true, false, 5) ||  !(($__internal_compile_0 = ($context["transitionData"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["dialog-url"] ?? null) : null))) {
                    // line 6
                    echo "            ";
                    $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["dialog-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_widget_transition_form", ["workflowItemId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 10
($context["workflowItem"] ?? null), "id", [], "any", false, false, false, 10), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 11
($context["transition"] ?? null), "name", [], "any", false, false, false, 11)])]);
                    // line 15
                    echo "        ";
                }
                // line 16
                echo "        ";
                if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transitionData"] ?? null), "jsDialogWidget", [], "array", true, true, false, 16) ||  !(($__internal_compile_1 = ($context["transitionData"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["jsDialogWidget"] ?? null) : null))) {
                    // line 17
                    echo "            ";
                    $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["jsDialogWidget" => twig_constant("Oro\\Bundle\\WorkflowBundle\\Button\\AbstractTransitionButton::TRANSITION_JS_DIALOG_WIDGET")]);
                    // line 20
                    echo "        ";
                }
                // line 21
                echo "    ";
            }
            // line 22
            echo "
    ";
            // line 23
            if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transitionData"] ?? null), "transition-url", [], "array", true, true, false, 23) ||  !(($__internal_compile_2 = ($context["transitionData"] ?? null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2["transition-url"] ?? null) : null))) {
                // line 24
                echo "        ";
                $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_transit", ["workflowItemId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 28
($context["workflowItem"] ?? null), "id", [], "any", false, false, false, 28), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 29
($context["transition"] ?? null), "name", [], "any", false, false, false, 29)])]);
                // line 33
                echo "    ";
            }
            // line 34
            echo "
    ";
            // line 35
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transitionData"] ?? null), "transition-condition-messages", [], "array", true, true, false, 35)) {
                // line 36
                echo "        ";
                $context["conditionMessages"] = [];
                // line 37
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable((($__internal_compile_3 = ($context["transitionData"] ?? null)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3["transition-condition-messages"] ?? null) : null));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 38
                    echo "            ";
                    $context["conditionMessages"] = twig_array_merge(($context["conditionMessages"] ?? null), [0 => (("<li>" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 38), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "parameters", [], "any", false, false, false, 38))) . "</li>")]);
                    // line 39
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 40
                echo "        ";
                if (twig_length_filter($this->env, ($context["conditionMessages"] ?? null))) {
                    // line 41
                    echo "            ";
                    $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-condition-messages" => (("<ol>" . twig_join_filter(                    // line 42
($context["conditionMessages"] ?? null))) . "</ol>")]);
                    // line 44
                    echo "        ";
                } else {
                    // line 45
                    echo "            ";
                    $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-condition-messages" => ""]);
                    // line 48
                    echo "        ";
                }
                // line 49
                echo "    ";
            }
            // line 50
            echo "
    ";
            // line 51
            $context["data"] = twig_array_merge(($context["transitionData"] ?? null), ["page-component-module" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 52
($context["transitionData"] ?? null), "page-component-module", [], "array", true, true, false, 52)) ? (_twig_default_filter((($__internal_compile_4 = ($context["transitionData"] ?? null)) && is_array($__internal_compile_4) || $__internal_compile_4 instanceof ArrayAccess ? ($__internal_compile_4["page-component-module"] ?? null) : null), "oroworkflow/js/app/components/button-component")) : ("oroworkflow/js/app/components/button-component")), "page-component-options" => json_encode(twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 53
($context["transitionData"] ?? null), "page-component-options", [], "array", true, true, false, 53)) ? (_twig_default_filter((($__internal_compile_5 = ($context["transitionData"] ?? null)) && is_array($__internal_compile_5) || $__internal_compile_5 instanceof ArrayAccess ? ($__internal_compile_5["page-component-options"] ?? null) : null), [])) : ([])), ["displayType" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "displayType", [], "any", false, false, false, 53)]))]);
            // line 55
            echo "
    ";
            // line 56
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", true, true, false, 56) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 56), "dialog", [], "any", true, true, false, 56))) {
                // line 57
                echo "        ";
                $context["data"] = twig_array_merge(($context["data"] ?? null), ["dialog-options" => json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 58
($context["transition"] ?? null), "frontendOptions", [], "any", false, false, false, 58), "dialog", [], "any", false, false, false, 58))]);
                // line 60
                echo "    ";
            }
            // line 61
            echo "
    ";
            // line 62
            $context["label"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "buttonLabel", [], "any", false, false, false, 62), [], "workflows");
            // line 63
            echo "    ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "buttonTitle", [], "any", false, false, false, 63), [], "workflows");
            // line 64
            echo "
    ";
            // line 65
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 65), "message", [], "any", true, true, false, 65) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 65), "message", [], "any", false, true, false, 65), "content", [], "any", true, true, false, 65))) {
                // line 66
                echo "        ";
                $context["frontendMessage"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, false, false, 66), "message", [], "any", false, false, false, 66);
                // line 67
                echo "
        ";
                // line 68
                $context["transitionMessage"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "content", [], "any", false, false, false, 68), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "message_parameters", [], "any", false, false, false, 68), "workflows");
                // line 69
                echo "        ";
                if ((($context["transitionMessage"] ?? null) && (($context["transitionMessage"] ?? null) != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "content", [], "any", false, false, false, 69)))) {
                    // line 70
                    echo "            ";
                    // line 71
                    echo "            ";
                    $context["message"] = twig_array_merge(($context["frontendMessage"] ?? null), ["content" => twig_nl2br(twig_escape_filter($this->env,                     // line 72
($context["transitionMessage"] ?? null), "html", null, true)), "title" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 73
($context["frontendMessage"] ?? null), "title", [], "any", true, true, false, 73)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "title", [], "any", false, false, false, 73), ($context["title"] ?? null))) : (($context["title"] ?? null)))]);
                    // line 75
                    echo "        ";
                }
                // line 76
                echo "    ";
            }
            // line 77
            echo "    
    ";
            // line 78
            $context["data"] = twig_array_merge(($context["data"] ?? null), ["message" => json_encode(((            // line 79
array_key_exists("message", $context)) ? (_twig_default_filter(($context["message"] ?? null), [])) : ([]))), "transition-label" =>             // line 80
($context["label"] ?? null)]);
            // line 82
            echo "    
    ";
            // line 83
            ob_start(function () { return ''; });
            // line 84
            echo "        ";
            echo twig_escape_filter($this->env, ($context["aClass"] ?? null), "html", null, true);
            echo "
        icons-holder-text
        ";
            // line 86
            if ( !((array_key_exists("onlyLink", $context)) ? (_twig_default_filter(($context["onlyLink"] ?? null), false)) : (false))) {
                echo "btn btn-sm workflow-transition-buttons";
            }
            // line 87
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "displayType", [], "any", false, false, false, 87) == "dialog")) {
                echo "no-hash";
            }
            // line 88
            echo "        ";
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transitionData"] ?? null), "enabled", [], "any", false, false, false, 88)) {
                echo "disabled";
            }
            // line 89
            echo "        ";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 89), "class", [], "any", true, true, false, 89)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 89), "class", [], "any", false, false, false, 89), "")) : ("")), "html", null, true);
            echo "
    ";
            $context["class"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 91
            echo "
    ";
            // line 92
            $context["iCss"] = ((((array_key_exists("noIcon", $context)) ? (_twig_default_filter(($context["noIcon"] ?? null), false)) : (false))) ? (false) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 92), "icon", [], "any", true, true, false, 92)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 92), "icon", [], "any", false, false, false, 92), "")) : (""))));
            // line 93
            echo "
    ";
            // line 95
            echo "    ";
            echo twig_call_macro($macros["ui"], "macro_link", [["path" => "#", "id" => twig_sprintf("transition-%s-%s-%s", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 97
($context["workflow"] ?? null), "name", [], "any", false, false, false, 97), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "name", [], "any", false, false, false, 97), twig_random($this->env)), "iCss" =>             // line 98
($context["iCss"] ?? null), "title" =>             // line 99
($context["title"] ?? null), "role" => "button", "class" =>             // line 101
($context["class"] ?? null), "data" =>             // line 102
($context["data"] ?? null), "label" =>             // line 103
($context["label"] ?? null), "role" => "button", "noIconText" => ((            // line 105
array_key_exists("noIconText", $context)) ? (_twig_default_filter(($context["noIconText"] ?? null), true)) : (true))]], 95, $context, $this->getSourceContext());
            // line 106
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 109
    public function macro_renderGoToTranslationsIconByLink($__link__ = null, $__large__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "link" => $__link__,
            "large" => $__large__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 110
            echo "    <span class=\"workflow-translatable-field\">
        <a target=\"_blank\" href=\"";
            // line 111
            echo ($context["link"] ?? null);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.action.workflow.go_to_translations"), "html", null, true);
            echo "\">
            <span class=\"fa fa-language";
            // line 112
            if (($context["large"] ?? null)) {
                echo " large-icon";
            }
            echo "\" aria-hidden=\"true\"></span>
        </a>
    </span>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  292 => 112,  286 => 111,  283 => 110,  269 => 109,  259 => 106,  257 => 105,  256 => 103,  255 => 102,  254 => 101,  253 => 99,  252 => 98,  251 => 97,  249 => 95,  246 => 93,  244 => 92,  241 => 91,  235 => 89,  230 => 88,  225 => 87,  221 => 86,  215 => 84,  213 => 83,  210 => 82,  208 => 80,  207 => 79,  206 => 78,  203 => 77,  200 => 76,  197 => 75,  195 => 73,  194 => 72,  192 => 71,  190 => 70,  187 => 69,  185 => 68,  182 => 67,  179 => 66,  177 => 65,  174 => 64,  171 => 63,  169 => 62,  166 => 61,  163 => 60,  161 => 58,  159 => 57,  157 => 56,  154 => 55,  152 => 53,  151 => 52,  150 => 51,  147 => 50,  144 => 49,  141 => 48,  138 => 45,  135 => 44,  133 => 42,  131 => 41,  128 => 40,  122 => 39,  119 => 38,  114 => 37,  111 => 36,  109 => 35,  106 => 34,  103 => 33,  101 => 29,  100 => 28,  98 => 24,  96 => 23,  93 => 22,  90 => 21,  87 => 20,  84 => 17,  81 => 16,  78 => 15,  76 => 11,  75 => 10,  73 => 6,  70 => 5,  68 => 4,  65 => 3,  62 => 2,  42 => 1,  37 => 108,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/macros.html.twig");
    }
}
