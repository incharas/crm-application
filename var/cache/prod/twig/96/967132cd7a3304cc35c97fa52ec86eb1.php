<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/mobile/side-menu.js */
class __TwigTemplate_626cde5866594ee3ffc5fec0d9a4e9b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(['../side-menu', '../mediator', 'oroui/js/tools/scroll-helper'], function(\$, mediator, scrollHelper) {
    'use strict';

    \$.widget('oroui.mobileSideMenu', \$.oroui.sideMenu, {
        /**
         * Creates side menu
         *
         * @private
         */
        _create: function() {
            this._super();

            this.listener.listenTo(mediator, 'page:request', this._hide.bind(this));

            // handler for hiding menu on outside click
            this._onOutsideClick = function(e) {
                if (!\$.contains(this.element.get(0), e.target)) {
                    this._hide();
                }
            }.bind(this);
        },

        /**
         * Adds accordion's styles for HTML of menu
         *
         * @private
         */
        _init: function() {
            this._convertToAccordion();
        },

        /**
         * Performs show menu action
         *
         * @private
         */
        _show: function() {
            this.\$toggle.addClass('open');
            \$(document).trigger('clearMenus'); // hides all opened dropdown menus
            \$('#main-menu').show();
            \$(document).on('click shown.bs.dropdown', this._onOutsideClick);
            scrollHelper.disableBodyTouchScroll();
        },

        /**
         * Performs hide menu action
         *
         * @private
         */
        _hide: function() {
            this.\$toggle.removeClass('open');
            \$('#main-menu').hide();
            \$(document).off('click shown.bs.dropdown', this._onOutsideClick);
            scrollHelper.enableBodyTouchScroll();
        },

        /**
         * Handles open/close side menu
         *
         * @private
         */
        _toggle: function(e) {
            if (!this.\$toggle.hasClass('open')) {
                this._show();
            } else {
                this._hide();
            }
        }
    });
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/mobile/side-menu.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/mobile/side-menu.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/mobile/side-menu.js");
    }
}
