<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/skeleton/_rect.scss */
class __TwigTemplate_8fef9c0be5b10bc85d30bf3d104c849e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@use 'sass:map';
@use 'sass:math';

// @private: background-image generator
// Take a particle map and generate background-image value
// @param: Map \$particle
// @return: List of background-image value
@function skeleton-image-rect(\$particle) {
    \$color: map.get(\$particle, 'color');
    \$width: map.get(\$particle, 'width');
    \$height: map.get(\$particle, 'height');
    \$radius: math.min(map.get(\$particle, 'radius'), math.floor(\$width * .5), math.floor(\$height * .5));
    \$result: ();

    @if not \$radius or strip-units(\$radius) == 0 {
        \$result: (
            linear-gradient(\$color, \$color),
        );
    } @else {
        \$result: (
            radial-gradient(closest-side, \$color 100%, transparent 100%),
            radial-gradient(closest-side, \$color 100%, transparent 100%),
            radial-gradient(closest-side, \$color 100%, transparent 100%),
            radial-gradient(closest-side, \$color 100%, transparent 100%),
            linear-gradient(\$color, \$color),
            linear-gradient(\$color, \$color),
        );
    }

    @return \$result;
}

// @private: background-size generator
// Take a particle map and generate background-size value
// @param: Map \$particle
// @return: List of background-size value
@function skeleton-size-rect(\$particle) {
    \$width: map.get(\$particle, 'width');
    \$height: map.get(\$particle, 'height');
    \$radius: math.min(map.get(\$particle, 'radius'), math.floor(\$width * .5), math.floor(\$height * .5));
    \$result: ();

    @if not \$radius or strip-units(\$radius) == 0 {
        \$result: (
            \$width \$height,
        );
    } @else {
        \$result: (
            #{\$radius * 2} #{\$radius * 2},
            #{\$radius * 2} #{\$radius * 2},
            #{\$radius * 2} #{\$radius * 2},
            #{\$radius * 2} #{\$radius * 2},
            \$width #{\$height - \$radius * 2},
            #{\$width - \$radius * 2} \$height
        );
    }

    @return \$result;
}

// @private: background-position generator
// Take a particle map and generate background-position value
// @param: Map \$particle
// @return: List of background-position value
@function skeleton-position-rect(\$particle) {
    \$width: map.get(\$particle, 'width');
    \$height: map.get(\$particle, 'height');
    \$x: map.get(\$particle, 'x');
    \$y: map.get(\$particle, 'y');
    \$radius: math.min(map.get(\$particle, 'radius'), math.floor(\$width * .5), math.floor(\$height * .5));
    \$result: ();

    @if not \$radius or strip-units(\$radius) == 0 {
        \$result: (
            \$x \$y,
        );
    } @else {
        \$result: (
            \$x \$y,
            #{\$x + \$width - \$radius * 2} \$y,
            \$x #{\$y + \$height - \$radius * 2},
            #{\$x + \$width - \$radius * 2} #{\$y + \$height - \$radius * 2},
            \$x #{\$y + \$radius},
            #{\$x + \$radius} \$y
        );
    }

    @return \$result;
}

// @public: function for get \$particle object for draw rect
// @param: \$color
// @param: \$width
// @param: \$height
// @param: \$x: 0
// @param: \$y: 0
// @param: \$radius: null
// @return: Formated map of parameters
@function skeleton-rect(
    \$color,
    \$width,
    \$height,
    \$x: 0,
    \$y: 0,
    \$radius: null
) {
    @return (
        'type': 'rect',
        'color': \$color,
        'width': \$width,
        'height': \$height,
        'x': \$x,
        'y': \$y,
        'radius': \$radius
    );
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/skeleton/_rect.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/skeleton/_rect.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/skeleton/_rect.scss");
    }
}
