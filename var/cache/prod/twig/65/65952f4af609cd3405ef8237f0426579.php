<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/_sizes.scss */
class __TwigTemplate_68eacb575f3c2c9d480a24d3e8f4b485 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

\$site-width: 1400px !default;

// Breakpoints

// Desktop Media Breakpoint
\$breakpoint-desktop: 1100px !default;
\$breakpoint-tablet: \$breakpoint-desktop - 1px !default;
\$breakpoint-tablet-small: 992px !default;
// iPad mini 4 (768*1024), iPad Air 2 (768*1024), iPad Pro (1024*1366)
\$breakpoint-mobile-big: 767px !default;
\$breakpoint-mobile-landscape: 640px !default;
// iPhone 4s (320*480), iPhone 5s (320*568), iPhone 6s (375*667), iPhone 6s Plus (414*763)
\$breakpoint-mobile: 414px !default;

// Offsets
\$offset-y: 16px !default;
\$offset-y-m: 8px !default;
\$offset-y-s: 4px !default;
\$offset-x: 16px !default;
\$offset-x-m: 8px !default;
\$offset-x-s: 4px !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/_sizes.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/_sizes.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/_sizes.scss");
    }
}
