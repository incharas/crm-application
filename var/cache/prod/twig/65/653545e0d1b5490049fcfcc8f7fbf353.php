<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/three-buttons-modal.html */
class __TwigTemplate_65a839dd2dee2b6ee71f450e4e7ed0a9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"modal-dialog\" role=\"document\">
    <div class=\"modal-content\">
        <div class=\"modal-header\">
            <% if (title) { %>
                <h3 id=\"<%- modalId %>\" class=\"modal-title\"><%- title %></h3>
            <% } %>
            <button type=\"button\" class=\"close <%- closeButtonClass %>\"
                data-dismiss=\"modal\"
                aria-label=\"<%- _.__('Close') %>\">
                <% if (closeText) { %>
                    <%- closeText %>
                <% } else { %>
                    <span aria-hidden=\"true\">&times;</span>
                <% } %>
            </button>
        </div>
        <div class=\"modal-body\"><%= content %></div>
        <div class=\"modal-footer\">
            <% if (allowCancel) { %>
                <a href=\"#\" role=\"button\" class=\"cancel <%- cancelButtonClass %>\" data-dismiss=\"modal\"><%- cancelText %></a>
            <% } %>
                <a href=\"#\" role=\"button\" class=\"<%- secondaryButtonClass %>\" data-button-id=\"secondary\"><%- secondaryText %></a>
            <% if (allowOk) { %>
                <a href=\"#\" role=\"button\" class=\"ok <%- okButtonClass %>\"><%- okText %></a>
            <% } %>
        </div>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/three-buttons-modal.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/three-buttons-modal.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/three-buttons-modal.html");
    }
}
