<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/dropdown.scss */
class __TwigTemplate_ccc98b4dab1082f48c43d42d1ab662d7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$dropdown-menu-background: #fff !default;
\$dropdown-menu-min-width: 140px !default;
\$dropdown-menu-offset: 2px 0 0 !default;
\$dropdown-menu-inner-offset: 5px 0 !default;
\$dropdown-menu-border: none !default;
\$dropdown-menu-border-radius: 2px !default;
\$dropdown-menu-box-shadow: 1px 1px 9px 3px rgba(0 0 0 / 8%) !default;
\$dropdown-menu-divider-color: \$primary-860 !default;

\$dropdown-item-background: none !default;
\$dropdown-item-width: 100% !default;
\$dropdown-item-border: none !default;
\$dropdown-item-display: block !default;
\$dropdown-item-color: \$primary-200 !default;
\$dropdown-item-text-align: left !default;
\$dropdown-item-inner-offset: 4px 16px !default;
\$dropdown-item-outline: none !default;

\$dropdown-item-hover-background: \$primary-900 !default;
\$dropdown-item-hover-color: \$primary-200 !default;

\$dropdown-item-active-background: \$primary-900 !default;
\$dropdown-item-active-color: \$primary-200 !default;

\$dropdown-toggle-box-shadow: none !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/dropdown.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/dropdown.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/dropdown.scss");
    }
}
