<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataAudit/change_history_link.html.twig */
class __TwigTemplate_71ca2e3cbd94d8c40de174632162b953 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDataAudit/change_history_link.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if (twig_test_empty(($context["entity_class"] ?? null))) {
            // line 4
            echo "    ";
            $context["entity_class"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null), true);
        }
        // line 6
        echo "<li>
    <a href=\"#\" data-url=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["audit_path"] ?? null), ["entity" => ($context["entity_class"] ?? null), "id" => ($context["id"] ?? null)]), "html", null, true);
        echo "\"
        title=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dataaudit.change_history.title", ["%item%" => ($context["title"] ?? null)]), "html", null, true);
        echo "\"
        ";
        // line 9
        echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "createOnEvent" => "click", "options" => ["dialogOptions" => ["allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dataaudit.change_history.title", ["%item%" =>         // line 20
($context["title"] ?? null)])]]]], 9, $context, $this->getSourceContext());
        // line 23
        echo ">
        ";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dataaudit.change_history"), "html", null, true);
        echo "
    </a>
</li>
";
    }

    public function getTemplateName()
    {
        return "@OroDataAudit/change_history_link.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 24,  62 => 23,  60 => 20,  59 => 9,  55 => 8,  51 => 7,  48 => 6,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataAudit/change_history_link.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataAuditBundle/Resources/views/change_history_link.html.twig");
    }
}
