<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/layout-module.js */
class __TwigTemplate_d65abb0b8fc09bc43b92db14a0d33ddc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import mediator from 'oroui/js/mediator';
import layout from 'oroui/js/layout';

/**
 * Init layout's handlers and listeners
 */
mediator.setHandler('layout:init', layout.init, layout);
mediator.setHandler('layout:dispose', layout.dispose, layout);
mediator.setHandler('layout:getPreferredLayout', layout.getPreferredLayout, layout);
mediator.setHandler('layout:getAvailableHeight', layout.getAvailableHeight, layout);
mediator.setHandler('layout:enablePageScroll', layout.enablePageScroll, layout);
mediator.setHandler('layout:disablePageScroll', layout.disablePageScroll, layout);
mediator.setHandler('layout:hasHorizontalScroll', layout.disablePageScroll, layout);
mediator.setHandler('layout:scrollbarWidth', layout.scrollbarWidth, layout);
mediator.setHandler('layout:adjustLabelsWidth', layout.adjustLabelsWidth, layout);
mediator.on('page:beforeChange', layout.pageRendering, layout);
mediator.on('page:afterChange', layout.pageRendered, layout);

if (document) {
    document.body.style.setProperty('--system-scroll-width', `\${layout.scrollbarWidth()}px`);
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/layout-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/layout-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/layout-module.js");
    }
}
