<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/layout.scss */
class __TwigTemplate_fe5e4f00c595c12ff738e84a3bc89f54 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

/* stylelint-disable no-descending-specificity, selector-max-compound-selectors, scss/selector-no-redundant-nesting-selector */

body:not(.login-page) {
    min-width: \$width-body-page;
}

&,
.app-page,
.app-page__central-panel {
    height: 100%;
    // prevents scroll bar appearance
    overflow: hidden;
}

& {
    --responsive-section-container-minimal-width-threshold: #{\$responsive-section-container-minimal-width-threshold};

    overflow-x: auto;
}

.responsive-section-container-minimal {
    min-width: calc(var(--responsive-section-container-minimal-width-threshold) * 1px);
    width: fit-content;
}

.responsive-section {
    .responsive-cell {
        &:nth-last-child(1n),
        &:nth-last-child(2n) {
            margin-bottom: 0;
        }
    }

    &.responsive-big {
        .responsive-cell {
            &:not(:only-child) {
                .responsive-block {
                    padding-left: 0;
                    padding-right: 0;
                }
            }
        }
    }

    &.responsive-small {
        .responsive-block {
            padding-left: 0;
            padding-right: 0;
        }
    }
}

.responsive-block {
    min-width: 340px;

    &:first-child {
        padding-right: \$content-padding-small;
    }

    &:last-child {
        padding-left: \$content-padding-small;
    }

    &:only-child {
        padding: 0;
    }

    &.border-right {
        border-right: 2px solid \$primary-860;
    }
}

.app-page {
    box-sizing: border-box;
    display: flex;
    min-height: 0;

    &__content-side {
        flex-shrink: 0;
    }

    &__content {
        flex-grow: 1;
        overflow: inherit;
        height: 100%;
        display: flex;
        flex-direction: column;
    }

    &__main {
        display: flex;
        width: 100%;
        height: 100%;
        align-items: stretch;
        justify-content: stretch;
        flex-grow: 1;
        flex-shrink: 1;
        min-height: 0;
    }

    &__left-panel,
    &__right-panel {
        position: relative;
        overflow: visible;
    }

    &__central-panel {
        display: flex;
        flex-direction: column;
        height: 100%;
        overflow: hidden;
        flex-grow: 1;
    }
}

.layout-content {
    > .scrollable-container {
        padding-left: \$content-padding;
        padding-right: \$content-padding;
    }

    // Update offset on pages with sidebar
    > .sidebar-container .category-data {
        padding-left: \$content-padding;
        padding-right: \$content-padding;
        margin-right: -\$content-padding;
    }

    //  Update offset on view pages
    .layout-content .scrollspy-main-container > .scrollable-container {
        margin-left: -\$content-padding;
        margin-right: -\$content-padding;
        padding-left: \$content-padding;
        padding-right: \$content-padding;
    }
}

#container,
.scrollspy-main-container,
.scrollable-container > form,
.layout-content,
.layout-content > *:only-child,
.layout-content > .responsive-form-inner,
.content-with-sidebar--content .category-data {
    display: flex;
    flex-direction: column;
    align-items: stretch;
    justify-content: stretch;
    flex-grow: 1;
    flex-shrink: 1;
    height: 100%;
    min-height: 0;
}

.scrollable-container {
    overflow: auto;
    flex-grow: 1;
    flex-shrink: 1;
    height: 100%;
    min-height: 0;
}

.container-fluid {
    > .responsive-section {
        &:not(.responsive-small) {
            .responsive-cell {
                &:first-child {
                    .user-fieldset,
                    .widget-title {
                        margin-left: \$content-padding-medium;
                    }
                }

                &:nth-child(2) {
                    .inner-grid {
                        // stylelint-disable-next-line max-nesting-depth
                        .grid-views {
                            margin-left: 0;
                        }
                    }
                }
            }
        }

        &.responsive-medium {
            &:not(.responsive-section-no-blocks) {
                .responsive-cell {
                    .user-fieldset,
                    .widget-title {
                        margin-left: \$content-padding-medium;
                    }
                }
            }
        }

        &.responsive-small {
            .responsive-cell {
                .user-fieldset,
                .widget-title {
                    margin-left: \$content-padding-medium;
                }
            }
        }
    }
}

//  Update offset on create / update pages
form > .layout-content {
    padding-left: \$content-padding;
    padding-right: \$content-padding;

    .scrollspy-main-container > .scrollable-container {
        margin-left: -\$content-padding;
        margin-right: -\$content-padding;
        padding-left: \$content-padding;
        padding-right: \$content-padding;
    }
}

.page-title + [data-bound-component*='datagrid'] {
    padding-left: \$content-padding;
    padding-right: \$content-padding;
}

.oro-page {
    &.collapsible-sidebar {
        .oro-page-sidebar {
            > .dropdown-menu {
                // stylelint-disable-next-line declaration-no-important
                display: block !important;
                z-index: inherit;
                width: 100%;
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/layout.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/layout.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/layout.scss");
    }
}
