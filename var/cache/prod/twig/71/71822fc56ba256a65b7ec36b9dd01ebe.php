<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/_typography.scss */
class __TwigTemplate_1b787cf57cbd0f66a226bed0072ae6c0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

// NOTICE!
// add !default to each variable;
// \$theme-fonts: map_merge(\$theme-fonts, \$theme-default-fonts); => \$theme-fonts;
\$charset: 'UTF-8' !default;

// List with theme fonts
\$theme-fonts: (
    'main': (
        'family': 'Lato',
        'variants': (
            (
                'path': '#{\$global-url}/orofrontend/default/fonts/lato/lato-regular-webfont',
                'weight': 400,
                'style': normal
            ),
            (
                'path': '#{\$global-url}/orofrontend/default/fonts/lato/lato-bold-webfont',
                'weight': 700,
                'style': normal
            )
        ),
        'formats': ('woff2', 'woff')
    ),
    'font-awesome': (
            'family': 'FontAwesome',
            'variants': (
                    (
                            'path': '~@oroinc/font-awesome/fonts/fontawesome-webfont',
                            'weight': normal,
                            'style': normal
                    )
            ),
            'formats': ('woff2', 'woff')
    ),
    'secondary': (
        'family': 'Roboto',
        'variants': (
            (
                'path': '#{\$global-url}/orofrontend/default/fonts/roboto/roboto-regular-webfont',
                'weight': 700,
                'style': normal
            )
        ),
        'formats': ('woff2', 'woff')
    ),
    'icon': (
        'family': 'custom-icon-font',
        'variants': (
            (
                'path': '#{\$global-url}/orofrontend/default/fonts/custom-font-icon/custom-font-icon',
                'weight': normal,
                'style': normal
            )
        ),
        'formats': ('woff')
    ),
    'only-bullets': (
        'family': 'only-bullets',
        'variants': (
            (
                // This font based on 'Lato regular' font.
                'path': '#{\$global-url}/orofrontend/default/fonts/only-bullets/only-bullets-regular',
                'weight': normal,
                'style': normal
            )
        ),
        'formats': ('woff2', 'woff')
    )
) !default;

// Fonts weights
\$font-weights: (
    // Thin (Hairline)
        'thin': 100,
    // Extra Light (Ultra Light)
        'extra': 200,
    // Light
        'light': 300,
    // Normal
        'normal': 400,
    // Medium
        'medium': 500,
    // Semi Bold (Demi Bold)
        'semi-bold': 600,
    // Bold
        'bold': 700,
    // Extra Bold (Ultra Bold)
        'extra-bold': 800,
    // Black (Heavy)
        'black': 900
) !default;

// Fonts families
\$base-font: get-font-name('main'),'helvetica', arial, sans-serif !default;
\$base-font-minor: get-font-name('secondary'),'helvetica', arial, sans-serif !default;
\$base-font-icon: get-font-name('icon') !default;
\$base-font-masked: get-font-name('only-bullets'), 'helvetica', arial, sans-serif !default;

// include in assets
\$icon-font: 'FontAwesome';

// Fonts sizes
\$root-font-size: 14px !default;
\$base-font-size: 14px !default;
\$base-font-size--large: 16px !default;
\$base-font-size--xs: 11px !default;
\$base-font-size--s: 13px !default;
\$base-font-size--m: 20px !default;
\$base-font-size--l: 23px !default;
\$base-font-size--xl: 26px !default;
\$base-line-height: 1.35 !default;
\$base-line-height--s: 1.25 !default;
\$base-font-weight: font-weight('normal') !default;

// Font smoothing
\$global-font-smoothing: true !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/_typography.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/_typography.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/_typography.scss");
    }
}
