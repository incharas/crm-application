<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Default/navbar/blocks.html.twig */
class __TwigTemplate_214160b5e02b4bb437af064b8c15d11b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'application_menu' => [$this, 'block_application_menu'],
            'user_menu' => [$this, 'block_user_menu'],
            'navbar' => [$this, 'block_navbar'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('application_menu', $context, $blocks);
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('user_menu', $context, $blocks);
        // line 8
        echo "
";
        // line 9
        $this->displayBlock('navbar', $context, $blocks);
    }

    // line 1
    public function block_application_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("application_menu", $context)) ? (_twig_default_filter(($context["application_menu"] ?? null), "application_menu")) : ("application_menu")), array());
    }

    // line 5
    public function block_user_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("user_menu", $context)) ? (_twig_default_filter(($context["user_menu"] ?? null), "user_menu")) : ("user_menu")), array());
    }

    // line 9
    public function block_navbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("navbar", $context)) ? (_twig_default_filter(($context["navbar"] ?? null), "navbar")) : ("navbar")), array());
    }

    public function getTemplateName()
    {
        return "@OroUI/Default/navbar/blocks.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  76 => 10,  72 => 9,  67 => 6,  63 => 5,  58 => 2,  54 => 1,  50 => 9,  47 => 8,  45 => 5,  42 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Default/navbar/blocks.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Default/navbar/blocks.html.twig");
    }
}
