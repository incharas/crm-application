<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/breadcrumb.html */
class __TwigTemplate_6a361067572c99426b4235f175527e5d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul class=\"breadcrumb\">
    <% for (var i = 0; i < breadcrumbs.length; i++) { %>
        <li class=\"breadcrumb-item<%= (i + 1 === breadcrumbs.length) ? ' active': '' %>\"><%- breadcrumbs[i] %></li>
    <% } %>
</ul>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/breadcrumb.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/breadcrumb.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/breadcrumb.html");
    }
}
