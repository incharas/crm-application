<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChannel/Channel/view.html.twig */
class __TwigTemplate_6e414ce672eb5e64e300183a3e2c5997 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtons' => [$this, 'block_navButtons'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["channelMacro"] = $this->macros["channelMacro"] = $this->loadTemplate("@OroChannel/macros.html.twig", "@OroChannel/Channel/view.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%channel.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", false, false, false, 4)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroChannel/Channel/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 8
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["entity"] ?? null), "name", [], "any", true, true, false, 11)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 11), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))];
        // line 13
        echo "
    ";
        // line 14
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 17
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/Channel/view.html.twig", 18)->unwrap();
        // line 19
        echo "
    ";
        // line 20
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $this->extensions['Oro\Bundle\ChannelBundle\Twig\ChannelExtension']->getChannelTypeMetadata(), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "channelType", [], "any", false, false, false, 20), [], "array", true, true, false, 20))) {
            // line 21
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["entity"] ?? null), "id", [], "any", false, false, false, 22)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_label")]], 21, $context, $this->getSourceContext());
            // line 24
            echo "
    ";
        }
        // line 26
        echo "
    ";
        // line 27
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 28
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_channel", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 29
($context["entity"] ?? null), "id", [], "any", false, false, false, 29)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-channel", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 33
($context["entity"] ?? null), "id", [], "any", false, false, false, 33), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_label")]], 28, $context, $this->getSourceContext());
            // line 35
            echo "
    ";
        }
    }

    // line 39
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "    ";
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
";
    }

    // line 43
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "    ";
        $context["id"] = "channel-view";
        // line 45
        echo "    ";
        ob_start(function () { return ''; });
        // line 46
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_widget_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 48
($context["entity"] ?? null), "id", [], "any", false, false, false, 48)])]);
        // line 49
        echo "
    ";
        $context["channelInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 51
        echo "
    ";
        // line 52
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 56
($context["channelInformationWidget"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Entities"), "subblocks" => [0 => ["title" => "", "data" => [0 => twig_call_macro($macros["channelMacro"], "macro_inializeEntitiesViewComponent", [        // line 62
($context["entity"] ?? null)], 62, $context, $this->getSourceContext())]]]]];
        // line 65
        echo "
    ";
        // line 66
        $context["data"] = ["dataBlocks" =>         // line 67
($context["dataBlocks"] ?? null)];
        // line 69
        echo "
    ";
        // line 70
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroChannel/Channel/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 70,  151 => 69,  149 => 67,  148 => 66,  145 => 65,  143 => 62,  142 => 56,  141 => 52,  138 => 51,  134 => 49,  132 => 48,  130 => 46,  127 => 45,  124 => 44,  120 => 43,  113 => 40,  109 => 39,  103 => 35,  101 => 33,  100 => 29,  98 => 28,  96 => 27,  93 => 26,  89 => 24,  87 => 22,  85 => 21,  83 => 20,  80 => 19,  77 => 18,  73 => 17,  67 => 14,  64 => 13,  62 => 11,  61 => 8,  59 => 7,  55 => 6,  50 => 1,  48 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChannel/Channel/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ChannelBundle/Resources/views/Channel/view.html.twig");
    }
}
