<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/_mixins.scss */
class __TwigTemplate_84f07698aef0275cb826dbd7d734b26c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@import './mixins/after';
@import './mixins/ellipsis';
@import './mixins/clearfix';
@import './mixins/list-normalize';
@import './mixins/breakpoint';
@import './mixins/font-face';
@import './mixins/font-smoothing';
@import './mixins/only-desktop';
@import './mixins/only-mobile';
@import './mixins/fullscreen-mode';
@import './mixins/disable-search-styles';
@import './mixins/aspect-ratio';
@import './mixins/element-state';
@import './mixins/safe-area-offset';
@import './mixins/link';
@import './mixins/fa-icon';
@import './mixins/caret';
@import './mixins/border';
@import './mixins/nav-tabs';
@import './mixins/list-separator';
@import './mixins/base-transition';
@import './mixins/block-substrate';
@import './mixins/keyframe';
@import './mixins/flexible-arrow';
@import './mixins/line-clamp';
@import './skeleton/skeleton';
@import './mixins/table-base';
@import './mixins/mosaic-grid';
@import './mixins/grid-cell-align';
@import './mixins/badge';
@import './mixins/slick-arrow';
@import './mixins/slick-dots';
@import './mixins/direct-link';
@import './mixins/loading-blur';
@import './mixins/loading-blur-overlay';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/_mixins.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/_mixins.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/_mixins.scss");
    }
}
