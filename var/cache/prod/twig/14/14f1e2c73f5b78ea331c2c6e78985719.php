<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContactUs/ContactRequest/widget/info.html.twig */
class __TwigTemplate_b068c14ad3d814dc76e59bfea9f8f29f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContactUs/ContactRequest/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["email"] = $this->macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroContactUs/ContactRequest/widget/info.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroContactUs/ContactRequest/widget/info.html.twig", 3)->unwrap();
        // line 4
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 8
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.first_name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "firstName", [], "any", false, false, false, 8)], 8, $context, $this->getSourceContext());
        echo "
            ";
        // line 9
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.last_name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lastName", [], "any", false, false, false, 9)], 9, $context, $this->getSourceContext());
        echo "
            ";
        // line 10
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.organization_name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "organizationName", [], "any", false, false, false, 10)], 10, $context, $this->getSourceContext());
        echo "
            ";
        // line 11
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.preferred_contact_method.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "preferredContactMethod", [], "any", false, false, false, 11))], 11, $context, $this->getSourceContext());
        echo "

            ";
        // line 13
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 13, $context, $this->getSourceContext());
        echo "
        </div>

        <div class=\"responsive-block\">
            ";
        // line 17
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.email_address.label"), twig_call_macro($macros["email"], "macro_renderEmailWithActions", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emailAddress", [], "any", false, false, false, 17), ($context["entity"] ?? null)], 17, $context, $this->getSourceContext())], 17, $context, $this->getSourceContext());
        echo "
            ";
        // line 18
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.phone.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phone", [], "any", false, false, false, 18)) ? (twig_call_macro($macros["ui"], "macro_renderPhoneWithActions", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phone", [], "any", false, false, false, 18), ($context["entity"] ?? null)], 18, $context, $this->getSourceContext())) : (null))], 18, $context, $this->getSourceContext());
        echo "
            ";
        // line 19
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "contactReason", [], "any", false, false, false, 19) || $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\ContactUsBundle\\Entity\\ContactReason", "entities"))) {
            // line 20
            echo "                ";
            echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.contact_reason.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "contactReason", [], "any", false, false, false, 20))], 20, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 22
        echo "            ";
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.comment.label"), twig_nl2br(twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "comment", [], "any", false, false, false, 22), "html", null, true))], 22, $context, $this->getSourceContext());
        echo "
            ";
        // line 23
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.feedback.label"), twig_nl2br(twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "feedback", [], "any", false, false, false, 23), "html", null, true))], 23, $context, $this->getSourceContext());
        echo "

            ";
        // line 25
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "opportunity", [], "any", false, false, false, 25) && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\SalesBundle\\Entity\\Opportunity", "entities"))) {
            // line 26
            echo "                ";
            echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.opportunity.label"), twig_call_macro($macros["ui"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 28
($context["entity"] ?? null), "opportunity", [], "any", false, false, false, 28), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "opportunity", [], "any", false, false, false, 28), "name", [], "any", false, false, false, 28), "oro_sales_opportunity_view"], 28, $context, $this->getSourceContext())], 26, $context, $this->getSourceContext());
            // line 29
            echo "
            ";
        }
        // line 31
        echo "
            ";
        // line 32
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lead", [], "any", false, false, false, 32) && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\SalesBundle\\Entity\\Lead", "entities"))) {
            // line 33
            echo "                ";
            echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contactus.contactrequest.lead.label"), twig_call_macro($macros["ui"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 35
($context["entity"] ?? null), "lead", [], "any", false, false, false, 35), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lead", [], "any", false, false, false, 35), "name", [], "any", false, false, false, 35), "oro_sales_lead_view"], 35, $context, $this->getSourceContext())], 33, $context, $this->getSourceContext());
            // line 36
            echo "
            ";
        }
        // line 38
        echo "        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroContactUs/ContactRequest/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 38,  118 => 36,  116 => 35,  114 => 33,  112 => 32,  109 => 31,  105 => 29,  103 => 28,  101 => 26,  99 => 25,  94 => 23,  89 => 22,  83 => 20,  81 => 19,  77 => 18,  73 => 17,  66 => 13,  61 => 11,  57 => 10,  53 => 9,  49 => 8,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContactUs/ContactRequest/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactUsBundle/Resources/views/ContactRequest/widget/info.html.twig");
    }
}
