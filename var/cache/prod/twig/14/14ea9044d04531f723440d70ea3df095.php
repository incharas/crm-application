<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/update.html.twig */
class __TwigTemplate_b4809be85f80dfb3de2c3f75bc388f29 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/update.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%application.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "data", [], "any", false, false, false, 4), "name", [], "any", false, false, false, 4)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroOAuth2Server/Client/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "data", [], "any", false, false, false, 7), "frontend", [], "any", false, false, false, 7)) ? ("oro_oauth2_frontend_index") : ("oro_oauth2_index")))], 7, $context, $this->getSourceContext());
        echo "

    ";
        // line 9
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 10
($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "data", [], "any", false, false, false, 10), "frontend", [], "any", false, false, false, 10)) ? ("oro_oauth2_frontend_view") : ("oro_oauth2_view")), "params" => ["id" => "\$id"]]], 9, $context, $this->getSourceContext());
        // line 13
        echo "
    ";
        // line 14
        $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["form"] ?? null), "vars", [], "any", false, false, false, 15), "data", [], "any", false, false, false, 15), "frontend", [], "any", false, false, false, 15)) ? ("oro_oauth2_frontend_update") : ("oro_oauth2_update")), "params" => ["id" => "\$id"]]], 14, $context, $this->getSourceContext()));
        // line 18
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 18, $context, $this->getSourceContext());
        echo "
";
    }

    // line 21
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.update_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 23
($context["form"] ?? null), "vars", [], "any", false, false, false, 23), "data", [], "any", false, false, false, 23), "frontend", [], "any", false, false, false, 23)) ? ("oro.oauth2server.client.entity_frontend_label") : ("oro.oauth2server.client.entity_label")))]);
        // line 28
        echo "    ";
        $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroOAuth2Server/Client/update.html.twig", 28)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
    }

    // line 31
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "    ";
        $context["id"] = "oauth-application";
        // line 33
        echo "
    ";
        // line 34
        ob_start(function () { return ''; });
        // line 35
        echo "        ";
        if (twig_in_filter("hidden", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 35), "vars", [], "any", false, false, false, 35), "block_prefixes", [], "any", false, false, false, 35))) {
            // line 36
            echo "            <div data-validation-ignore=\"true\">";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 36), 'row');
            echo "</div>
        ";
        } else {
            // line 38
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 38), 'row', ["group_attr" => ["class" => "client-grants"]]);
            echo "
        ";
        }
        // line 40
        echo "    ";
        $context["grantsRow"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 41
        echo "
    ";
        // line 42
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" => [0 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 48
($context["form"] ?? null), "organization", [], "any", true, true, false, 48)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", false, false, false, 48), 'row')) : ("")), 1 =>         // line 49
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "identifier", [], "any", false, false, false, 49), 'row'), 2 =>         // line 50
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 50), 'row'), 3 =>         // line 51
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "active", [], "any", false, false, false, 51), 'row'), 4 =>         // line 52
($context["grantsRow"] ?? null), 5 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 53
($context["form"] ?? null), "redirectUris", [], "any", true, true, false, 53)) ? (        $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "redirectUris", [], "any", false, false, false, 53), 'form_row_collection')) : ("")), 6 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 54
($context["form"] ?? null), "owner", [], "any", true, true, false, 54)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 54), 'row')) : (""))]]]]];
        // line 59
        echo "
    ";
        // line 60
        $context["data"] = ["formErrors" => ((        // line 61
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 62
($context["dataBlocks"] ?? null)];
        // line 64
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 64,  140 => 62,  139 => 61,  138 => 60,  135 => 59,  133 => 54,  132 => 53,  131 => 52,  130 => 51,  129 => 50,  128 => 49,  127 => 48,  126 => 42,  123 => 41,  120 => 40,  114 => 38,  108 => 36,  105 => 35,  103 => 34,  100 => 33,  97 => 32,  93 => 31,  88 => 28,  86 => 23,  84 => 22,  80 => 21,  73 => 18,  71 => 15,  70 => 14,  67 => 13,  65 => 10,  64 => 9,  58 => 7,  54 => 6,  49 => 1,  47 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/update.html.twig");
    }
}
