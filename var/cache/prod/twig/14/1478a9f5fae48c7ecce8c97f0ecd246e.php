<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/checkbox-view.js */
class __TwigTemplate_a84c20c61c8bc6da962f4dba4c71e8b7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');

    const CheckboxView = BaseView.extend({
        options: {
            selectors: {
                checkbox: null,
                hiddenInput: null
            }
        },

        events: {
            'change input[type=\"checkbox\"]': 'onChange'
        },

        /**
         * @inheritdoc
         */
        constructor: function CheckboxView(...args) {
            CheckboxView.__super__.constructor.apply(this, args);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            CheckboxView.__super__.initialize.call(this, options);

            this.options = Object.assign({}, this.options, options);
        },

        onChange() {
            const {checkbox, hiddenInput} = this.options.selectors;

            this.\$(hiddenInput).prop('disabled', this.\$(checkbox).is(':checked'));
        }
    });

    return CheckboxView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/checkbox-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/checkbox-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/checkbox-view.js");
    }
}
