<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/module-registry.js */
class __TwigTemplate_627d8cc52da74102ac3ca7aa9cf37070 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "const loadModules = require('oroui/js/app/services/load-modules');
const preloadedModules = {};

module.exports = {
    preload: function(moduleName) {
        return loadModules(moduleName).then(function(module) {
            if (!preloadedModules.hasOwnProperty(moduleName)) {
                preloadedModules[moduleName] = module;
            }
        });
    },

    get: function(moduleName) {
        if (preloadedModules.hasOwnProperty(moduleName)) {
            return preloadedModules[moduleName];
        }

        throw new Error('Module name \"' + moduleName + '\" has not been loaded yet');
    }
};
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/module-registry.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/module-registry.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/module-registry.js");
    }
}
