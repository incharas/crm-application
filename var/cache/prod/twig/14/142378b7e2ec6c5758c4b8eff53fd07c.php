<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Thread/emailItem.html.twig */
class __TwigTemplate_cd88aa78d2f11583e50f7233cb35419b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["Actions"] = $this->macros["Actions"] = $this->loadTemplate("@OroEmail/actions.html.twig", "@OroEmail/Email/Thread/emailItem.html.twig", 1)->unwrap();
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/Thread/emailItem.html.twig", 2)->unwrap();
        // line 3
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/Thread/emailItem.html.twig", 3)->unwrap();
        // line 4
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroEmail/Email/Thread/emailItem.html.twig", 4)->unwrap();
        // line 5
        echo "
";
        // line 11
        echo "
";
        // line 12
        $context["emailCollapsed"] = ((array_key_exists("emailCollapsed", $context)) ? (($context["emailCollapsed"] ?? null)) : (false));
        // line 13
        echo "<div class=\"email-info";
        echo ((($context["emailCollapsed"] ?? null)) ? ("") : (" in"));
        echo "\" data-layout=\"separate\">
    <div class=\"email-short\">
        <div class=\"email-view-toggle\">
            <div class=\"email-sent-date\">
                <div class=\"comment-date\">
                     <span class=\"comment-count\" style=\"display:none\" title=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.comment.quantity_label"), "html", null, true);
        echo "\">
                        <span class=\"count\" aria-hidden=\"true\"></span>
                    </span>
                    ";
        // line 21
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 21) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 21), "hasAttachments", [], "any", false, false, false, 21))) {
            // line 22
            echo "                        <span class=\"email-has-attachment\" aria-hidden=\"true\"></span>
                    ";
        }
        // line 24
        echo "                    ";
        echo twig_call_macro($macros["EA"], "macro_date_smart_format", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "sentAt", [], "any", false, false, false, 24)], 24, $context, $this->getSourceContext());
        echo "
                </div>


                <div class=\"email-participants\">
                    <span class=\"email-author\">";
        // line 29
        echo twig_call_macro($macros["EA"], "macro_email_participant_name_or_me", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromEmailAddress", [], "any", false, false, false, 29), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromName", [], "any", false, false, false, 29), true], 29, $context, $this->getSourceContext());
        echo "</span>
                    <span class=\"email-recipients\">";
        // line 30
        echo twig_escape_filter($this->env, twig_lower_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("To")), "html", null, true);
        echo " ";
        echo twig_call_macro($macros["EA"], "macro_email_participants_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "recipients", [], "any", false, false, false, 30), true, false], 30, $context, $this->getSourceContext());
        echo "</span>
                </div>
            </div>
            <div class=\"email-body\">
                ";
        // line 34
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 34)) {
            // line 35
            echo "                    ";
            echo twig_call_macro($macros["EA"], "macro_email_short_body", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 35), 200], 35, $context, $this->getSourceContext());
            echo "
                ";
        } else {
            // line 37
            echo "                    ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.body_is_unavailable"), "html", null, true);
            echo "
                ";
        }
        // line 39
        echo "            </div>
        </div>
    </div>
    <div class=\"email-full\">
        <header class=\"email-header\">
            ";
        // line 45
        echo "            ";
        if (((array_key_exists("renderContexts", $context) && ($context["renderContexts"] ?? null)) && (twig_length_filter($this->env, ($context["thread"] ?? null)) > 0))) {
            // line 46
            echo "                <div class=\"email-contexts-targets\">
                    <div class=\"activity-context-activity-list\">
                        ";
            // line 48
            $context["checkTarget"] = ((($context["target"] ?? null)) ? (true) : (false));
            // line 49
            echo "                        ";
            echo twig_call_macro($macros["AC"], "macro_activity_contexts", [twig_first($this->env, ($context["thread"] ?? null)), ($context["target"] ?? null), ($context["checkTarget"] ?? null)], 49, $context, $this->getSourceContext());
            echo "
                    </div>
                </div>
            ";
        }
        // line 53
        echo "
            <div class=\"email-actions\">
                <div class=\"email-sent-date\">
                    <span class=\"comment-count\" style=\"display:none\" title=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.comment.quantity_label"), "html", null, true);
        echo "\">
                        <span class=\"count\" aria-hidden=\"true\"></span>
                    </span>
                    ";
        // line 59
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 59) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 59), "hasAttachments", [], "any", false, false, false, 59))) {
            // line 60
            echo "                        <span class=\"email-has-attachment\" aria-hidden=\"true\"></span>
                    ";
        }
        // line 62
        echo "                    ";
        echo twig_call_macro($macros["EA"], "macro_date_smart_format", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "sentAt", [], "any", false, false, false, 62)], 62, $context, $this->getSourceContext());
        echo "
                </div>

                ";
        // line 65
        $context["actionParameters"] = ["routeParameters" => ((        // line 66
array_key_exists("routeParameters", $context)) ? (($context["routeParameters"] ?? null)) : ([])), "aCss" => " btn-sm"];
        // line 69
        echo "
                ";
        // line 70
        ob_start(function () { return ''; });
        // line 71
        echo "                    ";
        if (( !array_key_exists("defaultReplyButton", $context) || (($context["defaultReplyButton"] ?? null) == 1))) {
            // line 72
            echo "                        ";
            echo twig_call_macro($macros["Actions"], "macro_replyButton", [($context["email"] ?? null), ($context["actionParameters"] ?? null)], 72, $context, $this->getSourceContext());
            echo "
                        ";
            // line 73
            echo twig_call_macro($macros["Actions"], "macro_replyAllButton", [($context["email"] ?? null), ($context["actionParameters"] ?? null)], 73, $context, $this->getSourceContext());
            echo "
                    ";
        } else {
            // line 75
            echo "                        ";
            echo twig_call_macro($macros["Actions"], "macro_replyAllButton", [($context["email"] ?? null), ($context["actionParameters"] ?? null)], 75, $context, $this->getSourceContext());
            echo "
                        ";
            // line 76
            echo twig_call_macro($macros["Actions"], "macro_replyButton", [($context["email"] ?? null), ($context["actionParameters"] ?? null)], 76, $context, $this->getSourceContext());
            echo "
                    ";
        }
        // line 78
        echo "                    ";
        echo twig_call_macro($macros["Actions"], "macro_forwardButton", [($context["email"] ?? null), ($context["actionParameters"] ?? null)], 78, $context, $this->getSourceContext());
        echo "
                ";
        $context["buttonsHtml"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 80
        echo "
                ";
        // line 81
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 82
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_pinnedDropdownButton", [["html" =>             // line 83
($context["buttonsHtml"] ?? null), "options" => ["moreButtonAttrs" => ["class" => " btn-sm"]]]], 82, $context, $this->getSourceContext());
            // line 89
            echo "
                ";
        }
        // line 91
        echo "
                ";
        // line 92
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 93
            echo "                    <div class=\"email-actions-items\">
                        ";
            // line 94
            echo twig_escape_filter($this->env, ($context["buttonsHtml"] ?? null), "html", null, true);
            echo "
                    </div>
                ";
        }
        // line 97
        echo "            </div>
        </header>
        <div class=\"email-content\">
            <div class=\"email-body responsive-cell\">
                <div class=\"email-view-toggle\">
                    <div class=\"email-participants\">
                        <div class=\"email-author\">";
        // line 103
        echo twig_call_macro($macros["EA"], "macro_email_participant_name_or_me", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromEmailAddress", [], "any", false, false, false, 103), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromName", [], "any", false, false, false, 103), true], 103, $context, $this->getSourceContext());
        echo "</div>
                        <span class=\"email-recipients\">";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("To"), "html", null, true);
        echo ": ";
        echo twig_call_macro($macros["EA"], "macro_email_participants_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "recipients", [], "any", false, false, false, 104), true], 104, $context, $this->getSourceContext());
        echo "</span>
                        <div class=\"email-detailed-info-table dropdown\">
                            ";
        // line 106
        $context["togglerId"] = uniqid("dropdown-");
        // line 107
        echo "                            <span id=\"";
        echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
        echo "\" role=\"button\" class=\"btn btn-light dropdown-toggle dropdown-toggle--no-caret\" data-toggle=\"dropdown\"
                                  aria-haspopup=\"true\" aria-expanded=\"false\"
                                  aria-label=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.show_details.label"), "html", null, true);
        echo "\"
                                  title=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.show_details.tooltip"), "html", null, true);
        echo "\">
                                <span class=\"email-detailed-info-table__icon\" aria-hidden=\"true\"></span>
                            </span>
                            <div class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"";
        // line 113
        echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
        echo "\">
                                ";
        // line 114
        echo twig_call_macro($macros["EA"], "macro_email_detailed_info_table", [($context["email"] ?? null)], 114, $context, $this->getSourceContext());
        echo "
                            </div>
                        </div>
                    </div>
                </div>

                ";
        // line 120
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 120)) {
            // line 121
            echo "                    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 121), "bodyIsText", [], "any", false, false, false, 121)) {
                // line 122
                echo "                        ";
                echo twig_nl2br(twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 122), "bodyContent", [], "any", false, false, false, 122), "html", null, true));
                echo "
                    ";
            } else {
                // line 124
                echo "                        ";
                $context["emailBodyViewOptions"] = ["name" => "email-body", "view" => "oroemail/js/app/views/email-body-view", "bodyContent" => $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 127
($context["email"] ?? null), "emailBody", [], "any", false, false, false, 127), "bodyContent", [], "any", false, false, false, 127)), "styles" => [0 => $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("build/admin/tinymce/wysiwyg-editor.css"), 1 => $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroemail/css/email-body.css")]];
                // line 133
                echo "                        <iframe data-page-component-module=\"oroui/js/app/components/view-component\"
                                data-page-component-options=\"";
                // line 134
                echo twig_escape_filter($this->env, json_encode(($context["emailBodyViewOptions"] ?? null)), "html", null, true);
                echo "\"></iframe>
                    ";
            }
            // line 136
            echo "                ";
        } else {
            // line 137
            echo "                    ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.body_is_unavailable"), "html", null, true);
            echo "
                ";
        }
        // line 139
        echo "                ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 139)) {
            // line 140
            echo "                    ";
            $context["aCount"] = twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 140), "attachments", [], "any", false, false, false, 140));
            // line 141
            echo "                    ";
            $context["previewLimit"] = $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_email.attachment_preview_limit");
            // line 142
            echo "                    ";
            if (($context["aCount"] ?? null)) {
                // line 143
                echo "                        <div class=\"email-attachments-list-cont\">
                            <h6>";
                // line 144
                echo twig_escape_filter($this->env, ($context["aCount"] ?? null), "html", null, true);
                echo " ";
                echo twig_escape_filter($this->env, (((($context["aCount"] ?? null) > 1)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.attachment.entity_plural_label")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.attachment.entity_label"))), "html", null, true);
                echo "</h6>
                            <a class=\"no-hash\" href=\"";
                // line 145
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_body_attachments", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 145), "id", [], "any", false, false, false, 145)]), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.attachment.download_all"), "html", null, true);
                echo "</a>
                            <ul class=\"email-attachments-list thumbnails ";
                // line 146
                echo (((($context["aCount"] ?? null) > ($context["previewLimit"] ?? null))) ? ("name-only") : (""));
                echo "\">
                                ";
                // line 147
                echo twig_call_macro($macros["EA"], "macro_attachments", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 147), "attachments", [], "any", false, false, false, 147), ($context["target"] ?? null), ($context["hasGrantReattach"] ?? null)], 147, $context, $this->getSourceContext());
                echo "
                            </ul>
                        </div>
                    ";
            }
            // line 151
            echo "                ";
        }
        // line 152
        echo "            </div>";
        // line 153
        ob_start(function () { return ''; });
        // line 154
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_comments", $context)) ? (_twig_default_filter(($context["view_content_data_comments"] ?? null), "view_content_data_comments")) : ("view_content_data_comments")), ["entity" => ($context["email"] ?? null)]);
        $context["commentsData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 156
        if ( !twig_test_empty(($context["commentsData"] ?? null))) {
            // line 157
            echo "                <div class=\"responsive-cell\">
                    ";
            // line 158
            echo twig_escape_filter($this->env, ($context["commentsData"] ?? null), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 161
        echo "        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Thread/emailItem.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  363 => 161,  357 => 158,  354 => 157,  352 => 156,  349 => 154,  347 => 153,  345 => 152,  342 => 151,  335 => 147,  331 => 146,  325 => 145,  319 => 144,  316 => 143,  313 => 142,  310 => 141,  307 => 140,  304 => 139,  298 => 137,  295 => 136,  290 => 134,  287 => 133,  285 => 127,  283 => 124,  277 => 122,  274 => 121,  272 => 120,  263 => 114,  259 => 113,  253 => 110,  249 => 109,  243 => 107,  241 => 106,  234 => 104,  230 => 103,  222 => 97,  216 => 94,  213 => 93,  211 => 92,  208 => 91,  204 => 89,  202 => 83,  200 => 82,  198 => 81,  195 => 80,  189 => 78,  184 => 76,  179 => 75,  174 => 73,  169 => 72,  166 => 71,  164 => 70,  161 => 69,  159 => 66,  158 => 65,  151 => 62,  147 => 60,  145 => 59,  139 => 56,  134 => 53,  126 => 49,  124 => 48,  120 => 46,  117 => 45,  110 => 39,  104 => 37,  98 => 35,  96 => 34,  87 => 30,  83 => 29,  74 => 24,  70 => 22,  68 => 21,  62 => 18,  53 => 13,  51 => 12,  48 => 11,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Thread/emailItem.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Thread/emailItem.html.twig");
    }
}
