<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/components/entity-tree-select-form-type-view.js */
class __TwigTemplate_4d88cb7c18dbd1df050af1a3761690b2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const BaseTreeView = require('oroui/js/app/views/jstree/base-tree-view');

    /**
     * Additional option:
     *  - fieldSelector - selector for field ID field
     */
    const EntityTreeSelectFormTypeView = BaseTreeView.extend({
        /**
         * @property {Object}
         */
        \$fieldSelector: null,

        /**
         * @inheritdoc
         */
        constructor: function EntityTreeSelectFormTypeView(options) {
            EntityTreeSelectFormTypeView.__super__.constructor.call(this, options);
        },

        /**
         * @param {Object} options
         */
        initialize: function(options) {
            EntityTreeSelectFormTypeView.__super__.initialize.call(this, options);
            if (!this.\$tree) {
                return;
            }

            const fieldSelector = options.fieldSelector;
            if (!fieldSelector) {
                return;
            }
            this.\$fieldSelector = \$(fieldSelector);

            this.\$tree.on('select_node.jstree', this.onSelect.bind(this));
            this.\$tree.on('deselect_node.jstree', this.onDeselect.bind(this));

            this.\$fieldSelector.on('disable', this.onDisable.bind(this));
            this.\$fieldSelector.on('enable', this.onEnable.bind(this));
        },

        /**
         * Set category ID to field value
         *
         * @param {Object} node
         * @param {Object} selected
         */
        onSelect: function(node, selected) {
            this.\$fieldSelector.val(selected.node.id);
        },

        /**
         * Clear field value
         */
        onDeselect: function() {
            this.\$fieldSelector.val('');
        },

        onDisable: function() {
            this.toggleDisable(true);
        },

        onEnable: function() {
            this.toggleDisable(false);
        }
    });

    return EntityTreeSelectFormTypeView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/components/entity-tree-select-form-type-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/components/entity-tree-select-form-type-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/components/entity-tree-select-form-type-view.js");
    }
}
