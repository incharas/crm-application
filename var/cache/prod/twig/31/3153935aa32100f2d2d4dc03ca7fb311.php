<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroQueryDesigner/Form/fields.html.twig */
class __TwigTemplate_8ed357535282bb439d11d4608b7dad26 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_field_choice_row' => [$this, 'block_oro_field_choice_row'],
            'oro_date_field_choice_row' => [$this, 'block_oro_date_field_choice_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_field_choice_row', $context, $blocks);
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('oro_date_field_choice_row', $context, $blocks);
    }

    // line 1
    public function block_oro_field_choice_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        echo "
";
    }

    // line 5
    public function block_oro_date_field_choice_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroQueryDesigner/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  63 => 6,  59 => 5,  52 => 2,  48 => 1,  44 => 5,  41 => 4,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroQueryDesigner/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/QueryDesignerBundle/Resources/views/Form/fields.html.twig");
    }
}
