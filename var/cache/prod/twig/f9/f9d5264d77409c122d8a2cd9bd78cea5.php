<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/startHangoutButton.html.twig */
class __TwigTemplate_dca85d8150b68d8205e960a99d677985 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["HangoutsCall"] = $this->macros["HangoutsCall"] = $this->loadTemplate("@OroHangoutsCall/macros.html.twig", "@OroHangoutsCall/startHangoutButton.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        echo twig_call_macro($macros["HangoutsCall"], "macro_renderStartButton", [["hangoutOptions" => ["invites" => ((        // line 5
array_key_exists("invites", $context)) ? (($context["invites"] ?? null)) : ([]))]]], 3, $context, $this->getSourceContext());
        // line 7
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/startHangoutButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  43 => 5,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/startHangoutButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/startHangoutButton.html.twig");
    }
}
