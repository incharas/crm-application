<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/tinymce/tinymce.scss */
class __TwigTemplate_24195ff71481993259b90ea6ba727506 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.tox-tinymce {
    max-width: 100%;
    /* stylelint-disable declaration-no-important */
    border-radius: \$input-field-border-radius !important;
    border: \$input-border-width solid \$input-border-color !important;
    /* stylelint-enable declaration-no-important */
}

.narrow-text-field {
    .tox-tinymce:not(.tox-fullscreen) {
        // stylelint-disable-next-line declaration-no-important
        width: \$field-width !important;
        min-height: 300px;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/tinymce/tinymce.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/tinymce/tinymce.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/tinymce/tinymce.scss");
    }
}
