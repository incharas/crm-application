<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/badge.scss */
class __TwigTemplate_afa9df5d92cba0f3902f200a6be90fe9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import '~@oroinc/bootstrap/scss/badge';

.badge {
    [class^='icon-status-'],
    [class*=' icon-status-'] {
        font-size: \$badge-icon-height;
        margin-right: \$badge-icon-offset;
    }
}

.badge-pill {
    font-size: \$badge-font-size;
    line-height: \$badge-line-height;
    vertical-align: \$badge-vertical-align;
}

@each \$color, \$values in \$badge-theme-keys {
    .badge-#{\$color} {
        @include badge-custom-variant(\$values...);
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/badge.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/badge.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/badge.scss");
    }
}
