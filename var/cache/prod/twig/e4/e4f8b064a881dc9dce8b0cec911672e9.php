<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/js/toolbar.html.twig */
class __TwigTemplate_7a1e2cc6ac621f4ffc894585f04ef126 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script type=\"text/html\" class=\"datagrid_templates\" data-identifier=\"template-datagrid-toolbar\">
    <div class=\"grid-toolbar\">
     ";
        // line 3
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 4
            echo "        <div class=\"grid-toolbar__part grid-toolbar__part--start\">
            <div class=\"pagination\" data-grid-pagination></div>
            <div data-grid-items-counter></div>
        </div>
        <div class=\"grid-toolbar__part grid-toolbar__part--center\">
            <div class=\"sorting\" data-grid-sorting></div>
            <div class=\"grid-toolbar-mass-actions\">
                <div class=\"extra-actions-panel\" data-grid-extra-actions-panel></div>
            </div>
        </div>
        <div class=\"grid-toolbar__part grid-toolbar__part--end\">
            <div class=\"grid-toolbar-tools\">
                <div class=\"page-size\" data-grid-pagesize></div>
                <div class=\"actions-panel\" data-grid-actions-panel></div>
            </div>
        </div>
    ";
        } else {
            // line 21
            echo "        <div class=\"grid-toolbar__part grid-toolbar__part--start\">
            <div class=\"page-size\" data-grid-pagesize></div>
            <div class=\"actions-panel\" data-grid-actions-panel></div>
        </div>
        <div class=\"grid-toolbar__part grid-toolbar__part--end\">
            <div class=\"pagination-container\">
                <div class=\"pagination\" data-grid-pagination></div>
            </div>
        </div>
    ";
        }
        // line 31
        echo "    </div>
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/js/toolbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 31,  62 => 21,  43 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/js/toolbar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/js/toolbar.html.twig");
    }
}
