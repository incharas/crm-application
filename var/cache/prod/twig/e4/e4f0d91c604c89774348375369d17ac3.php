<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/main-menu/main.scss */
class __TwigTemplate_0587ad649afbfea9c0141f5f4f916000 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

%main-menu-trigger {
    font-size: \$menu-toggler-font-size;
    line-height: 1;

    text-align: center;

    border: 0;
    color: \$menu-toggler-color;

    cursor: pointer;

    &:hover,
    &:focus {
        color: \$menu-item-first-level-text-color-active;
        outline: none;
    }

    &::before {
        transition: \$menu-animation;
    }
}

// common decoration
@import 'main-menu';

// menu top decoration
@import 'main-menu-top';

// sided menu decoration
@import 'main-menu-sided';
@import 'side-menu-overlay';
@import 'main-menu-toggler';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/main-menu/main.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/main-menu/main.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/main-menu/main.scss");
    }
}
