<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/js/app/modules/validator-constraints-module.js */
class __TwigTemplate_c6fc7106080760e7003e8011940841ab extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import \$ from 'jquery.validate';

// general validation methods
\$.validator.loadMethod([
    'oroform/js/validator/count',
    'oroform/js/validator/date',
    'oroform/js/validator/datetime',
    'oroform/js/validator/email',
    'oroform/js/validator/length',
    'oroform/js/validator/notblank',
    'oroform/js/validator/notnull',
    'oroform/js/validator/number',
    'oroform/js/validator/numeric-range',
    'oroform/js/validator/range',
    'oroform/js/validator/open-range',
    'oroform/js/validator/regex',
    'oroform/js/validator/repeated',
    'oroform/js/validator/not-blank-group',
    'oroform/js/validator/time',
    // 'oroform/js/validator/url', /* turned off, due to it is too heavy and not in use on the front */
    'oroform/js/validator/type',
    'oroform/js/validator/callback'
]);
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/js/app/modules/validator-constraints-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/js/app/modules/validator-constraints-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/js/app/modules/validator-constraints-module.js");
    }
}
