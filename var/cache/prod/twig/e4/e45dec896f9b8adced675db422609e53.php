<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/macros.html.twig */
class __TwigTemplate_fdcb7094f740eec56869909ae9994e5f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_renderClientLink($__config__ = null, $__urlParameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "config" => $__config__,
            "urlParameters" => $__urlParameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/macros.html.twig", 2)->unwrap();
            // line 3
            echo "    ";
            echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 5
($context["config"] ?? null), "dataUrl", [], "any", false, false, false, 5), ($context["urlParameters"] ?? null)), "aCss" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 6
($context["config"] ?? null), "aCss", [], "any", false, false, false, 6), "iCss" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 7
($context["config"] ?? null), "iCss", [], "any", false, false, false, 7), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 8
($context["config"] ?? null), "label", [], "any", false, false, false, 8)), "widget" => ["type" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["config"] ?? null), "widget", [], "any", false, false, false, 10), "type", [], "any", false, false, false, 10), "multiple" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["config"] ?? null), "widget", [], "any", false, false, false, 11), "multiple", [], "any", false, false, false, 11), "refresh-widget-alias" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 12
($context["config"] ?? null), "widget", [], "any", false, false, false, 12), "refreshWidgetAlias", [], "any", false, false, false, 12), "reload-grid-name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["config"] ?? null), "widget", [], "any", false, false, false, 13), "reloadGridName", [], "any", false, false, false, 13), "options" => ["alias" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["config"] ?? null), "widget", [], "any", false, false, false, 15), "options", [], "any", false, false, false, 15), "alias", [], "any", false, false, false, 15), "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["config"] ?? null), "widget", [], "any", false, false, false, 17), "options", [], "any", false, false, false, 17), "dialogOptions", [], "any", false, false, false, 17), "title", [], "any", false, false, false, 17)), "allowMaximize" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["config"] ?? null), "widget", [], "any", false, false, false, 18), "options", [], "any", false, false, false, 18), "dialogOptions", [], "any", false, false, false, 18), "allowMaximize", [], "any", false, false, false, 18), "allowMinimize" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 19
($context["config"] ?? null), "widget", [], "any", false, false, false, 19), "options", [], "any", false, false, false, 19), "dialogOptions", [], "any", false, false, false, 19), "allowMinimize", [], "any", false, false, false, 19), "dblclick" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["config"] ?? null), "widget", [], "any", false, false, false, 20), "options", [], "any", false, false, false, 20), "dialogOptions", [], "any", false, false, false, 20), "dblclick", [], "any", false, false, false, 20), "maximizedHeightDecreaseBy" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["config"] ?? null), "widget", [], "any", false, false, false, 21), "options", [], "any", false, false, false, 21), "dialogOptions", [], "any", false, false, false, 21), "maximizedHeightDecreaseBy", [], "any", false, false, false, 21), "width" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["config"] ?? null), "widget", [], "any", false, false, false, 22), "options", [], "any", false, false, false, 22), "dialogOptions", [], "any", false, false, false, 22), "width", [], "any", false, false, false, 22)]]]]], 4, $context, $this->getSourceContext());
            // line 27
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 27,  72 => 22,  71 => 21,  70 => 20,  69 => 19,  68 => 18,  67 => 17,  66 => 15,  65 => 13,  64 => 12,  63 => 11,  62 => 10,  61 => 8,  60 => 7,  59 => 6,  58 => 5,  56 => 3,  53 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/macros.html.twig");
    }
}
