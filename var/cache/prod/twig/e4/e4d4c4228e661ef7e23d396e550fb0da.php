<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/lib/minicolors/ORO_CHANGELOG.md */
class __TwigTemplate_a6b434cfc2a6ff22732482409c9e9540 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "Changes in BAP 1.5.0
====================

2014-11-12
----------

* Remove embedded image from `.minicolors-sprite` CSS style to decrease size of CSS file.
* Allow to apply the picker to any HTML element, not only INPUT. For example, this feature is used to integrate `jquery.minicolors` with `jquery.simplecolorpicker`.
* Fix `letterCase` in `change` event
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/lib/minicolors/ORO_CHANGELOG.md";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/lib/minicolors/ORO_CHANGELOG.md", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/lib/minicolors/ORO_CHANGELOG.md");
    }
}
