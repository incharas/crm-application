<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMarketingActivity/MarketingActivity/js/marketingActivitySectionItem.html.twig */
class __TwigTemplate_8f4f322c1653df27864e954001e0e34a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'activityContent' => [$this, 'block_activityContent'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script type=\"text/html\" id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html_attr");
        echo "\">
    <% var labelId = _.uniqueId('label-') %>
    <% var regionId = _.uniqueId('region-') %>
    <div class=\"accordion-group\">
        <div class=\"accordion-heading clearfix\">
            <a href=\"#<%- regionId %>\" data-toggle=\"collapse\" aria-expanded=\"<%- collapsed ? 'false' : 'true' %>\"
               aria-controls=\"<%- regionId %>\"
               class=\"accordion-icon accordion-toggle<% if (collapsed) { %> collapsed<% } %>\"
               title=\"<%- _.__(collapsed ? 'Expand' : 'Collapse')%>\"
            ></a>
            <div class=\"icon\">
                <span class=\"fa-volume-up\" aria-hidden=\"true\"></span>
            </div>
            <div class=\"campaign-name\" id=\"<%- labelId %>\">
                <strong><%- campaignName %></strong>
            </div>
            <div class=\"actions\">
                ";
        // line 18
        ob_start(function () { return ''; });
        // line 19
        echo "                    <a href=\"<%- routing.generate('oro_campaign_view', {'id': id}) %>\"
                       title=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.view_campaign.label"), "html", null, true);
        echo "\"><i
                                class=\"fa-eye hide-text\">";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.view_campaign.label"), "html", null, true);
        echo "</i>
                        ";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.view_campaign.label"), "html", null, true);
        echo "
                    </a>
                ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 25
        echo "                ";
        $context["actions"] = [0 => ($context["action"] ?? null)];
        // line 26
        echo "
                <% var togglerId = _.uniqueId('dropdown-') %>
                <div class=\"vertical-actions activity-actions\">
                    <a href=\"#\" role=\"button\" id=\"<%- togglerId %>\" data-placement=\"left-start\" data-toggle=\"dropdown\" class=\"btn btn-icon btn-lighter dropdown-toggle activity-item\"
                       aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.actions.label"), "html", null, true);
        echo "\">...</a>
                    <ul class=\"dropdown-menu activity-item\" role=\"menu\" aria-labelledby=\"<%- togglerId %>\">
                        ";
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(((array_key_exists("actions", $context)) ? (_twig_default_filter(($context["actions"] ?? null), [])) : ([])));
        foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
            // line 33
            echo "                            <li class=\"activity-action\">";
            echo $context["action"];
            echo "</li>
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "                    </ul>
                </div>
            </div>
            <div class=\"extra-info\">
                <div class=\"marketing-activity-type\">
                    ";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.last_event_type.label"), "html", null, true);
        echo ": <%- eventType %>
                </div>
                <div class=\"marketing-activity-date\">
                    ";
        // line 43
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.last_activity_date.label"), "html", null, true);
        echo ": <%- dateFormatter.formatDateTime(eventDate) %>
                </div>
            </div>
        </div>
        <div id=\"<%- regionId %>\" role=\"region\" aria-labelledby=\"<%- labelId %>\" class=\"accordion-body collapse<% if (!collapsed) { %> show<% } %>\"
             data-collapsed-title=\"";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Expand"), "html", null, true);
        echo "\" data-expanded-title=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Collapse"), "html", null, true);
        echo "\">
            <div class=\"message\">
                ";
        // line 50
        $this->displayBlock('activityContent', $context, $blocks);
        // line 56
        echo "            </div>
        </div>
    </div>
</script>
";
    }

    // line 50
    public function block_activityContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "                    <div class=\"activity-item-content\">
                        ";
        // line 53
        echo "                        <div class=\"info responsive-cell\"></div>
                    </div>
                ";
    }

    public function getTemplateName()
    {
        return "@OroMarketingActivity/MarketingActivity/js/marketingActivitySectionItem.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 53,  147 => 51,  143 => 50,  135 => 56,  133 => 50,  126 => 48,  118 => 43,  112 => 40,  105 => 35,  96 => 33,  92 => 32,  87 => 30,  81 => 26,  78 => 25,  72 => 22,  68 => 21,  64 => 20,  61 => 19,  59 => 18,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMarketingActivity/MarketingActivity/js/marketingActivitySectionItem.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/MarketingActivityBundle/Resources/views/MarketingActivity/js/marketingActivitySectionItem.html.twig");
    }
}
