<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/widget-form-component.js */
class __TwigTemplate_1e1b531481437889bd6fabd132d30121 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(
    ['oroui/js/widget-manager', 'oroui/js/messenger', 'oroui/js/mediator', 'orotranslation/js/translator'],
    function(widgetManager, messenger, mediator, __) {
        'use strict';

        return function(options) {
            widgetManager.getWidgetInstance(
                options._wid,
                function(widget) {
                    if (options.data) {
                        if (!options.message) {
                            options.message = __('oro.ui.widget_form_component.save_flash_success');
                        }

                        messenger.notificationFlashMessage('success', options.message);
                        mediator.trigger('widget_success:' + widget.getAlias(), options);
                        mediator.trigger('widget_success:' + widget.getWid(), options);

                        widget.trigger('formSave', options.data);
                    } else {
                        if (options.formError) {
                            widget.trigger('formSaveError');
                        }
                    }

                    if (options.reloadLayout) {
                        mediator.trigger('layout:adjustHeight');
                    }

                    if (!options.preventRemove && !widget.disposed) {
                        widget.remove();
                    }
                }
            );
        };
    }
);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/widget-form-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/widget-form-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/widget-form-component.js");
    }
}
