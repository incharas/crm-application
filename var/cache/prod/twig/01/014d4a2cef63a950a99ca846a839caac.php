<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/macros.html.twig */
class __TwigTemplate_f5a5149b5dc97911b448723e678fad51 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "
";
    }

    // line 1
    public function macro_renderCollectionWithPrimaryElement($__collection__ = null, $__isEmail__ = null, $__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "collection" => $__collection__,
            "isEmail" => $__isEmail__,
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/macros.html.twig", 2)->unwrap();
            // line 3
            echo "    ";
            $macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroSales/macros.html.twig", 3)->unwrap();
            // line 4
            echo "
    ";
            // line 5
            $context["primaryElement"] = null;
            // line 6
            echo "    ";
            $context["elements"] = [];
            // line 7
            echo "
    ";
            // line 8
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["collection"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 9
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["element"], "primary", [], "any", false, false, false, 9)) {
                    // line 10
                    echo "            ";
                    $context["primaryElement"] = $context["element"];
                    // line 11
                    echo "        ";
                } else {
                    // line 12
                    echo "            ";
                    $context["elements"] = twig_array_merge(($context["elements"] ?? null), [0 => $context["element"]]);
                    // line 13
                    echo "        ";
                }
                // line 14
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "    ";
            if (($context["primaryElement"] ?? null)) {
                // line 16
                echo "        ";
                $context["elements"] = twig_array_merge([0 => ($context["primaryElement"] ?? null)], ($context["elements"] ?? null));
                // line 17
                echo "    ";
            }
            // line 18
            echo "
    <ul class=\"extra-list\">";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["elements"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 21
                echo "            <li class=\"contact-collection-element";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["element"], "primary", [], "any", false, false, false, 21)) {
                    echo " primary";
                }
                echo "\">
                ";
                // line 22
                if (($context["isEmail"] ?? null)) {
                    // line 23
                    echo "                    ";
                    echo twig_call_macro($macros["email"], "macro_renderEmailWithActions", [$context["element"], ($context["entity"] ?? null)], 23, $context, $this->getSourceContext());
                    echo "
                ";
                } else {
                    // line 25
                    echo "                    ";
                    echo twig_call_macro($macros["ui"], "macro_renderPhoneWithActions", [$context["element"], ($context["entity"] ?? null)], 25, $context, $this->getSourceContext());
                    echo "
                ";
                }
                // line 27
                echo "            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 29
            echo "</ul>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 32
    public function macro_render_customer_info($__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 33
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/macros.html.twig", 33)->unwrap();
            // line 34
            echo "    ";
            $macros["sales"] = $this;
            // line 35
            echo "
    ";
            // line 36
            $context["customer"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "customerAssociation", [], "any", false, false, false, 36) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "customerAssociation", [], "any", false, false, false, 36), "customerTarget", [], "any", false, false, false, 36))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 37
($context["entity"] ?? null), "customerAssociation", [], "any", false, false, false, 37), "customerTarget", [], "any", false, false, false, 37)) : (null));
            // line 39
            echo "    ";
            $context["account"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "customerAssociation", [], "any", false, false, false, 39) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "customerAssociation", [], "any", false, false, false, 39), "account", [], "any", false, false, false, 39))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 40
($context["entity"] ?? null), "customerAssociation", [], "any", false, false, false, 40), "account", [], "any", false, false, false, 40)) : (null));
            // line 43
            $context["accountView"] = twig_call_macro($macros["sales"], "macro_entity_view", [($context["account"] ?? null)], 43, $context, $this->getSourceContext());
            // line 45
            echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.customer.label"), ($context["accountView"] ?? null), ($context["entity"] ?? null), "customer"], 45, $context, $this->getSourceContext());
            echo "

    ";
            // line 47
            if ( !twig_test_empty(($context["customer"] ?? null))) {
                // line 48
                echo "        <div class=\"base-currency-wrapper\">
            ";
                // line 49
                echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["customer"] ?? null)), "label")), twig_call_macro($macros["sales"], "macro_entity_view", [($context["customer"] ?? null)], 49, $context, $this->getSourceContext()), ($context["entity"] ?? null), "customer"], 49, $context, $this->getSourceContext());
                echo "
        </div>
    ";
            }
            // line 52
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 55
    public function macro_entity_view($__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 56
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/macros.html.twig", 56)->unwrap();
            // line 58
            $context["entityName"] = ((($context["entity"] ?? null)) ? ($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["entity"] ?? null))) : (""));
            // line 59
            if ((($context["entity"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null)))) {
                // line 60
                echo twig_call_macro($macros["ui"], "macro_renderUrl", [$this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getEntityViewLink(($context["entity"] ?? null)), ($context["entityName"] ?? null)], 60, $context, $this->getSourceContext());
            } else {
                // line 62
                echo twig_escape_filter($this->env, ($context["entityName"] ?? null), "html", null, true);
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroSales/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  232 => 62,  229 => 60,  227 => 59,  225 => 58,  223 => 56,  210 => 55,  200 => 52,  194 => 49,  191 => 48,  189 => 47,  184 => 45,  182 => 43,  180 => 40,  178 => 39,  176 => 37,  175 => 36,  172 => 35,  169 => 34,  166 => 33,  153 => 32,  143 => 29,  136 => 27,  130 => 25,  124 => 23,  122 => 22,  115 => 21,  111 => 20,  108 => 18,  105 => 17,  102 => 16,  99 => 15,  93 => 14,  90 => 13,  87 => 12,  84 => 11,  81 => 10,  78 => 9,  74 => 8,  71 => 7,  68 => 6,  66 => 5,  63 => 4,  60 => 3,  57 => 2,  42 => 1,  37 => 31,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/macros.html.twig");
    }
}
