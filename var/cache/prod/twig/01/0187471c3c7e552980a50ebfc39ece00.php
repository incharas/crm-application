<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/page/organization-switch-view.js */
class __TwigTemplate_e38b5b5041c4768046bda0837c09ed36 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'oroui/js/app/views/base/page-region-view'
], function(PageRegionView) {
    'use strict';

    const OrganizationSwitchView = PageRegionView.extend({
        template: function(data) {
            return data.organization_switch;
        },

        /**
         * @inheritdoc
         */
        constructor: function OrganizationSwitchView(options) {
            OrganizationSwitchView.__super__.constructor.call(this, options);
        },

        pageItems: ['organization_switch']
    });

    return OrganizationSwitchView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/page/organization-switch-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/page/organization-switch-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/page/organization-switch-view.js");
    }
}
