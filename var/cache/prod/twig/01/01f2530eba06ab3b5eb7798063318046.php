<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAction/Operation/form.html.twig */
class __TwigTemplate_40e439b3c8726bffd9c3747bd3a567fc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'widget_content' => [$this, 'block_widget_content'],
            'widget_content_class' => [$this, 'block_widget_content_class'],
            'widget_content_inner' => [$this, 'block_widget_content_inner'],
            'messages' => [$this, 'block_messages'],
            'errors' => [$this, 'block_errors'],
            'errors_inner' => [$this, 'block_errors_inner'],
            'form_errors' => [$this, 'block_form_errors'],
            'form_errors_inner' => [$this, 'block_form_errors_inner'],
            'form' => [$this, 'block_form'],
            'form_inner' => [$this, 'block_form_inner'],
            'form_widget' => [$this, 'block_form_widget'],
            'form_actions' => [$this, 'block_form_actions'],
            'form_actions_inner' => [$this, 'block_form_actions_inner'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('widget_content', $context, $blocks);
    }

    public function block_widget_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        if (array_key_exists("response", $context)) {
            // line 3
            echo "        ";
            $context["widgetResponse"] = ["widget" => ["trigger" => [0 => ["eventBroker" => "widget", "name" => "formSave", "args" => [0 =>             // line 8
($context["response"] ?? null)]]]]];
            // line 12
            echo "
        ";
            // line 13
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
    ";
        } else {
            // line 15
            echo "        <div class=\"";
            $this->displayBlock('widget_content_class', $context, $blocks);
            echo "\">
            ";
            // line 16
            $this->displayBlock('widget_content_inner', $context, $blocks);
            // line 89
            echo "        </div>
    ";
        }
    }

    // line 15
    public function block_widget_content_class($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "widget-content";
    }

    // line 16
    public function block_widget_content_inner($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "                ";
        $this->displayBlock('messages', $context, $blocks);
        // line 30
        echo "                ";
        $this->displayBlock('errors', $context, $blocks);
        // line 43
        echo "                ";
        if (array_key_exists("form", $context)) {
            // line 44
            echo "                    ";
            $this->displayBlock('form_errors', $context, $blocks);
            // line 53
            echo "
                    ";
            // line 54
            $this->displayBlock('form', $context, $blocks);
            // line 86
            echo "                ";
        }
        // line 87
        echo "
            ";
    }

    // line 17
    public function block_messages($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "                    ";
        if ((array_key_exists("messages", $context) && twig_length_filter($this->env, ($context["messages"] ?? null)))) {
            // line 19
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["messages"] ?? null));
            foreach ($context['_seq'] as $context["type"] => $context["items"]) {
                // line 20
                echo "                        <div class=\"alert alert-";
                echo twig_escape_filter($this->env, $context["type"], "html", null, true);
                echo "\" role=\"alert\">
                            <ul>
                                ";
                // line 22
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["items"]);
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 23
                    echo "                                    <li>";
                    echo twig_escape_filter($this->env, $context["item"], "html", null, true);
                    echo "</li>
                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 25
                echo "                            </ul>
                        </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['items'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "                    ";
        }
        // line 29
        echo "                ";
    }

    // line 30
    public function block_errors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "                    ";
        if ((array_key_exists("errors", $context) && twig_length_filter($this->env, ($context["errors"] ?? null)))) {
            // line 32
            echo "                        <div class=\"alert alert-error\" role=\"alert\">
                            <ul>
                            ";
            // line 34
            $this->displayBlock('errors_inner', $context, $blocks);
            // line 39
            echo "                            </ul>
                        </div>
                    ";
        }
        // line 42
        echo "                ";
    }

    // line 34
    public function block_errors_inner($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "                                ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
            // line 36
            echo "                                    <li>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 36), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "parameters", [], "any", true, true, false, 36)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "parameters", [], "any", false, false, false, 36), [])) : ([]))), "html", null, true);
            echo "</li>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "                            ";
    }

    // line 44
    public function block_form_errors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "                        ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 45), "errors", [], "any", false, false, false, 45)) > 0)) {
            // line 46
            echo "                            <div class=\"alert alert-error\" role=\"alert\">
                                ";
            // line 47
            $this->displayBlock('form_errors_inner', $context, $blocks);
            // line 50
            echo "                            </div>
                        ";
        }
        // line 52
        echo "                    ";
    }

    // line 47
    public function block_form_errors_inner($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "                                    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
                                ";
    }

    // line 54
    public function block_form($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 55
        echo "                        ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["method" => "POST", "action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 58
($context["app"] ?? null), "request", [], "any", false, false, false, 58), "attributes", [], "any", false, false, false, 58), "get", [0 => "_route"], "method", false, false, false, 58), twig_array_filter($this->env, twig_array_merge(twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 59
($context["app"] ?? null), "request", [], "any", false, false, false, 59), "query", [], "any", false, false, false, 59), "all", [], "any", false, false, false, 59), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 59), "request", [], "any", false, false, false, 59), "all", [], "any", false, false, false, 59)), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 59), "attributes", [], "any", false, false, false, 59), "get", [0 => "_route_params"], "method", false, false, false, 59)), function ($__v__, $__k__) use ($context, $macros) { $context["v"] = $__v__; $context["k"] = $__k__; return (($context["k"] ?? null) != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 59), "name", [], "any", false, false, false, 59)); })), "attr" => ["class" => "form-dialog", "data-nohash" => "true", "data-disable-autofocus" => "true", "id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 65
($context["form"] ?? null), "vars", [], "any", false, false, false, 65), "id", [], "any", false, false, false, 65)]]);
        // line 67
        echo "
                            ";
        // line 68
        $this->displayBlock('form_inner', $context, $blocks);
        // line 83
        echo "                        ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
                        ";
        // line 84
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        echo "
                    ";
    }

    // line 68
    public function block_form_inner($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 69
        echo "                                ";
        $this->displayBlock('form_widget', $context, $blocks);
        // line 72
        echo "
                                ";
        // line 73
        $this->displayBlock('form_actions', $context, $blocks);
        // line 82
        echo "                            ";
    }

    // line 69
    public function block_form_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 70
        echo "                                    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
                                ";
    }

    // line 73
    public function block_form_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 74
        echo "                                    ";
        $context["options"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["operation"] ?? null), "definition", [], "any", false, true, false, 74), "frontendOptions", [], "any", false, true, false, 74), "options", [], "any", true, true, false, 74)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["operation"] ?? null), "definition", [], "any", false, true, false, 74), "frontendOptions", [], "any", false, true, false, 74), "options", [], "any", false, false, false, 74), [])) : ([]));
        // line 75
        echo "                                    <div class=\"widget-actions form-actions\">
                                        ";
        // line 76
        $this->displayBlock('form_actions_inner', $context, $blocks);
        // line 80
        echo "                                    </div>
                                ";
    }

    // line 76
    public function block_form_actions_inner($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 77
        echo "                                            <button class=\"btn\" type=\"reset\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "cancelText", [], "any", true, true, false, 77)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "cancelText", [], "any", false, false, false, 77), "Cancel")) : ("Cancel"))), "html", null, true);
        echo "</button>
                                            <button class=\"btn btn-success\" type=\"submit\">";
        // line 78
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "okText", [], "any", true, true, false, 78)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "okText", [], "any", false, false, false, 78), "Submit")) : ("Submit"))), "html", null, true);
        echo "</button>
                                        ";
    }

    public function getTemplateName()
    {
        return "@OroAction/Operation/form.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  321 => 78,  316 => 77,  312 => 76,  307 => 80,  305 => 76,  302 => 75,  299 => 74,  295 => 73,  288 => 70,  284 => 69,  280 => 82,  278 => 73,  275 => 72,  272 => 69,  268 => 68,  262 => 84,  257 => 83,  255 => 68,  252 => 67,  250 => 65,  249 => 59,  248 => 58,  246 => 55,  242 => 54,  235 => 48,  231 => 47,  227 => 52,  223 => 50,  221 => 47,  218 => 46,  215 => 45,  211 => 44,  207 => 38,  198 => 36,  193 => 35,  189 => 34,  185 => 42,  180 => 39,  178 => 34,  174 => 32,  171 => 31,  167 => 30,  163 => 29,  160 => 28,  152 => 25,  143 => 23,  139 => 22,  133 => 20,  128 => 19,  125 => 18,  121 => 17,  116 => 87,  113 => 86,  111 => 54,  108 => 53,  105 => 44,  102 => 43,  99 => 30,  96 => 17,  92 => 16,  85 => 15,  79 => 89,  77 => 16,  72 => 15,  67 => 13,  64 => 12,  62 => 8,  60 => 3,  57 => 2,  50 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAction/Operation/form.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActionBundle/Resources/views/Operation/form.html.twig");
    }
}
