<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/component-shortcuts-manager.js */
class __TwigTemplate_73f693bef68dc5e26d9e13d9aab6f5b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require, exports, module) {
    'use strict';

    const _ = require('underscore');
    const \$ = require('jquery');

    let config = require('module-config').default(module.id);
    config = _.extend({
        reservedKeys: ['options']
    }, config);

    const ComponentShortcutsManager = {
        reservedKeys: config.reservedKeys,

        shortcuts: {},

        /**
         * @param {String} key
         * @param {Object} shortcut
         */
        add: function(key, shortcut) {
            if (this.reservedKeys.indexOf(key) !== -1) {
                throw new Error('Component shortcut `' + key + '` is reserved!');
            }

            if (this.shortcuts[key]) {
                throw new Error('Component shortcut `' + key + '` already exists!');
            }

            const capitalizeKey = _.map(key.split('-'), function(item) {
                return _.capitalize(item);
            }).join('');

            shortcut.dataKey = 'pageComponent' + capitalizeKey;
            shortcut.dataAttr = 'data-page-component-' + key;

            this.shortcuts[key] = shortcut;
        },

        /**
         * @param {string} key
         */
        remove: function(key) {
            delete this.shortcuts[key];
        },

        /**
         * @returns {object}
         */
        getAll: function() {
            return this.shortcuts;
        },

        /**
         * Prepare component data by element attributes and shortcut config
         *
         * @param {Object} shortcut
         * @param {Object} elemData
         * @return {Object}
         */
        getComponentData: function(shortcut, elemData) {
            let dataOptions = elemData[shortcut.dataKey];
            const module = shortcut.moduleName || dataOptions;

            if (!_.isObject(dataOptions)) {
                dataOptions = {};
                if (shortcut.scalarOption) {
                    dataOptions[shortcut.scalarOption] = elemData[shortcut.dataKey];
                }
            }

            const options = \$.extend(
                true,
                {},
                shortcut.options,
                dataOptions,
                elemData.pageComponentOptions
            );

            return {
                pageComponentModule: module,
                pageComponentOptions: options
            };
        }
    };

    return ComponentShortcutsManager;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/component-shortcuts-manager.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/component-shortcuts-manager.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/component-shortcuts-manager.js");
    }
}
