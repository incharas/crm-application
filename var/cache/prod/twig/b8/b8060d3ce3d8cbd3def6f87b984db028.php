<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/component-shortcuts-module.js */
class __TwigTemplate_3c4c347c7126003217e814b61c422a63 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import ComponentShortcutsManager from 'oroui/js/component-shortcuts-manager';

ComponentShortcutsManager.add('module', {});

ComponentShortcutsManager.add('view', {
    moduleName: 'oroui/js/app/components/view-component',
    scalarOption: 'view'
});

ComponentShortcutsManager.add('viewport', {
    moduleName: 'oroui/js/app/components/viewport-component'
});

ComponentShortcutsManager.add('jquery', {
    moduleName: 'oroui/js/app/components/jquery-widget-component',
    scalarOption: 'widgetModule'
});

ComponentShortcutsManager.add('responsive-tabs', {
    moduleName: 'oroui/js/app/components/tabs-component',
    scalarOption: 'useDropdown'
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/component-shortcuts-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/component-shortcuts-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/component-shortcuts-module.js");
    }
}
