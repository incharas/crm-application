<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTranslation/js_modules_config.html.twig */
class __TwigTemplate_d03ea49e315fc4fa61f041564147ac0a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "debug", [], "any", false, false, false, 1) && $this->extensions['Oro\Bundle\TranslationBundle\Twig\TranslationExtension']->isDebugJsTranslations())) {
            // line 2
            echo "    ";
            $context["translations"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_translation_jstranslation");
        } else {
            // line 4
            echo "    ";
            $context["translations"] = $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($this->extensions['Oro\Bundle\UIBundle\Twig\FormatExtension']->generateUrlWithoutFrontController("oro_translation_jstranslation"), "translations");
        }
        // line 6
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroTranslation/js_modules_config.html.twig", 6)->unwrap();
        // line 7
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["orotranslation/js/translator" => ["debugTranslator" => $this->extensions['Oro\Bundle\TranslationBundle\Twig\TranslationExtension']->isDebugTranslator()], "orotranslation/js/translation-loader" => ["translationsResources" =>         // line 12
($context["translations"] ?? null)]]], 7, $context, $this->getSourceContext());
        // line 14
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTranslation/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 14,  50 => 12,  49 => 7,  47 => 6,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTranslation/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/Resources/views/js_modules_config.html.twig");
    }
}
