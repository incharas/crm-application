<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/length.js */
class __TwigTemplate_0f08b21ae408f695cd750ac29bcfbb0a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const numberValidator = require('oroform/js/validator/number');

    const defaultParam = {
        exactMessage: 'This value should have exactly ";
        // line 7
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " character.|' +
            'This value should have exactly ";
        // line 8
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " characters.',
        maxMessage: 'This value is too long. It should have ";
        // line 9
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " character or less.|' +
            'This value is too long. It should have ";
        // line 10
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " characters or less.',
        minMessage: 'This value is too short. It should have ";
        // line 11
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " character or more.|' +
            'This value is too short. It should have ";
        // line 12
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " characters or more.'
    };

    /**
     * @export oroform/js/validator/length
     */
    return [
        'Length',
        function(value, element, param) {
            return this.optional(element) || numberValidator[1].call(this, value.length, element, param);
        },
        function(param, element) {
            const value = this.elementValue(element);
            const placeholders = {};
            param = Object.assign({}, defaultParam, param);
            placeholders.value = value;
            return numberValidator[2].call(this, param, element, value.length, placeholders);
        }
    ];
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/length.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 12,  61 => 11,  57 => 10,  53 => 9,  49 => 8,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/length.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/length.js");
    }
}
