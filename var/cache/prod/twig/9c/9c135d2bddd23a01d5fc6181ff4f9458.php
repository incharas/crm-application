<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSidebar/sidebar.html.twig */
class __TwigTemplate_23574284526351972ba689b907269b78 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSidebar/sidebar.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if ( !array_key_exists("sidebarsGetURL", $context)) {
            // line 4
            echo "    ";
            $context["sidebarsGetURL"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_sidebars", ["position" => ("SIDEBAR_" . twig_upper_filter($this->env, ($context["placement"] ?? null)))]);
        }
        // line 6
        echo "
";
        // line 7
        if ( !array_key_exists("sidebarPostURL", $context)) {
            // line 8
            echo "    ";
            $context["sidebarPostURL"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_post_sidebars");
        }
        // line 10
        echo "
";
        // line 11
        if ( !array_key_exists("widgetsGetURL", $context)) {
            // line 12
            echo "    ";
            $context["widgetsGetURL"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_sidebarwidgets", ["placement" => ($context["placement"] ?? null)]);
        }
        // line 14
        echo "
";
        // line 15
        if ( !array_key_exists("widgetPostURL", $context)) {
            // line 16
            echo "    ";
            $context["widgetPostURL"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_post_sidebarwidgets");
        }
        // line 18
        echo "
";
        // line 19
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue((("oro_sidebar.sidebar_" . ($context["placement"] ?? null)) . "_active")) == true))) {
            // line 20
            echo "    ";
            $context["pageComponentOptions"] = ["sidebarData" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(            // line 21
($context["sidebarsGetURL"] ?? null)), "widgetsData" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(            // line 22
($context["widgetsGetURL"] ?? null)), "availableWidgets" => $this->extensions['Oro\Bundle\SidebarBundle\Twig\SidebarExtension']->getWidgetDefinitions(            // line 23
($context["placement"] ?? null)), "urlRoot" =>             // line 24
($context["sidebarPostURL"] ?? null), "url" =>             // line 25
($context["widgetPostURL"] ?? null)];
            // line 27
            echo "    ";
            if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageComponentOptions"] ?? null), "sidebarData", [], "any", false, false, false, 27))) {
                // line 28
                echo "        ";
                $context["pageComponentOptions"] = twig_array_merge(($context["pageComponentOptions"] ?? null), ["sidebarData" => json_encode(["position" => ("SIDEBAR_" . twig_upper_filter($this->env,                 // line 29
($context["placement"] ?? null)))])]);
                // line 31
                echo "    ";
            }
            // line 32
            echo "
<div id=\"sidebar-";
            // line 33
            echo twig_escape_filter($this->env, ($context["placement"] ?? null), "html", null, true);
            echo "\" class=\"sidebar sidebar-";
            echo twig_escape_filter($this->env, ($context["placement"] ?? null), "html", null, true);
            echo "\"
    ";
            // line 34
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orosidebar/js/app/components/sidebar-component", "options" =>             // line 36
($context["pageComponentOptions"] ?? null)]], 34, $context, $this->getSourceContext());
            // line 37
            echo "></div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroSidebar/sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 37,  108 => 36,  107 => 34,  101 => 33,  98 => 32,  95 => 31,  93 => 29,  91 => 28,  88 => 27,  86 => 25,  85 => 24,  84 => 23,  83 => 22,  82 => 21,  80 => 20,  78 => 19,  75 => 18,  71 => 16,  69 => 15,  66 => 14,  62 => 12,  60 => 11,  57 => 10,  53 => 8,  51 => 7,  48 => 6,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSidebar/sidebar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SidebarBundle/Resources/views/sidebar.html.twig");
    }
}
