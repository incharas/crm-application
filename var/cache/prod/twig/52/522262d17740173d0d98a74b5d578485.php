<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/Datagrid/Property/title.html.twig */
class __TwigTemplate_669c669c3a92bfeece60653710b2c348 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["value"] ?? null)) {
            // line 2
            echo "    ";
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/Datagrid/Property/title.html.twig", 2)->unwrap();
            // line 3
            echo "
    ";
            // line 4
            $context["eventId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "id"], "method", false, false, false, 4);
            // line 5
            echo "    ";
            echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_widget_info", ["id" =>             // line 6
($context["eventId"] ?? null)]), "aCss" => "no-hash", "label" =>             // line 8
($context["value"] ?? null), "widget" => ["type" => "dialog", "multiple" => false, "options" => ["alias" => ("calendar_event_info_widget_" .             // line 13
($context["eventId"] ?? null)), "dialogOptions" => ["title" =>             // line 15
($context["value"] ?? null), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 600]]]]], 5, $context, $this->getSourceContext());
            // line 24
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/Datagrid/Property/title.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 24,  52 => 15,  51 => 13,  50 => 8,  49 => 6,  47 => 5,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/Datagrid/Property/title.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/Datagrid/Property/title.html.twig");
    }
}
