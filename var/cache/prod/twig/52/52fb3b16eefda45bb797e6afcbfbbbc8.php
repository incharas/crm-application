<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/widget/emails.html.twig */
class __TwigTemplate_78b8981ff5ee430bb7ecbf1535ecbd4d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroEmail/Email/widget/emails.html.twig", 1)->unwrap();
        // line 2
        $macros["email"] = $this->macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/widget/emails.html.twig", 2)->unwrap();
        // line 3
        echo "
<div class=\"widget-content\">
    ";
        // line 5
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["email-grid", ($context["datagridParameters"] ?? null)], 5, $context, $this->getSourceContext());
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/widget/emails.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 5,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/widget/emails.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/widget/emails.html.twig");
    }
}
