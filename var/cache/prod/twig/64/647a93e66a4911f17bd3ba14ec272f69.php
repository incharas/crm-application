<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/jstree/subtree-action-view.js */
class __TwigTemplate_2dbfa85530008ba6c2ab656626ed4cc3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const AbstractActionView = require('oroui/js/app/views/jstree/abstract-action-view');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');

    const SubTreeActionView = AbstractActionView.extend({
        options: _.extend({}, AbstractActionView.prototype.options, {
            itemsLabel: __('oro.ui.jstree.actions.subitems.itemsLabel'),
            doNotSelectIcon: 'long-arrow-up',
            doNotSelectLabel: 'oro.ui.jstree.actions.subitems.do_not_select',
            selectIcon: 'long-arrow-down',
            selectLabel: 'oro.ui.jstree.actions.subitems.select'
        }),

        selectSubTree: false,

        /**
         * @inheritdoc
         */
        constructor: function SubTreeActionView(options) {
            SubTreeActionView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            SubTreeActionView.__super__.initialize.call(this, options);
            this.options.selectLabel = __(this.options.selectLabel, {
                itemsLabel: this.options.itemsLabel
            });
            this.options.doNotSelectLabel = __(this.options.doNotSelectLabel, {
                itemsLabel: this.options.itemsLabel
            });
        },

        render: function() {
            if (this.selectSubTree) {
                this.options.icon = this.options.doNotSelectIcon;
                this.options.label = this.options.doNotSelectLabel;
            } else {
                this.options.icon = this.options.selectIcon;
                this.options.label = this.options.selectLabel;
            }
            return SubTreeActionView.__super__.render.call(this);
        },

        onClick: function() {
            this.selectSubTree = !this.selectSubTree;
            this.render();
            this.options.\$tree.trigger('select-subtree-item:change', {selectSubTree: this.selectSubTree});
        }
    });

    return SubTreeActionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/jstree/subtree-action-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/jstree/subtree-action-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/jstree/subtree-action-view.js");
    }
}
