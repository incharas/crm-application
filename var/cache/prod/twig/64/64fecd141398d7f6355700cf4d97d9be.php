<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMarketingActivity/MarketingActivity/widget/marketingActivitiesSection.html.twig */
class __TwigTemplate_0320f384c91d66c6e63935844556eb18 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'widget_content' => [$this, 'block_widget_content'],
            'widget_actions' => [$this, 'block_widget_actions'],
            'items_container' => [$this, 'block_items_container'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMarketingActivity/MarketingActivity/widget/marketingActivitiesSection.html.twig", 1)->unwrap();
        // line 2
        $context["containerExtraClass"] = ((array_key_exists("containerExtraClass", $context)) ? (($context["containerExtraClass"] ?? null)) : (""));
        // line 3
        echo "<div class=\"widget-content activity-list marketing-activities ";
        echo twig_escape_filter($this->env, ($context["containerExtraClass"] ?? null), "html", null, true);
        echo "\">
    ";
        // line 4
        $context["pager"] = ["current" => 1, "pagesize" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_activity_list.per_page"), "total" => 1, "count" => 1, "sortingField" => "eventDate"];
        // line 11
        echo "    ";
        $context["configuration"] = [        // line 12
($context["configurationKey"] ?? null) => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.entity_label"), "template" => "@OroMarketingActivity/MarketingActivity/js/marketingActivitySectionItem.html.twig", "has_comments" => false, "routes" => ["itemView" => "oro_marketing_activity_widget_marketing_activities_info"]]];
        // line 21
        echo "    ";
        $this->displayBlock('widget_content', $context, $blocks);
        // line 103
        echo "</div>
";
    }

    // line 21
    public function block_widget_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "
        ";
        // line 23
        $this->displayBlock('widget_actions', $context, $blocks);
        // line 59
        echo "        ";
        $this->displayBlock('items_container', $context, $blocks);
        // line 101
        echo "
    ";
    }

    // line 23
    public function block_widget_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "        <div class=\"grid-toolbar\">
            <div class=\"filter-box\">
                <div class=\"filter-container\"></div>
            </div>
            <div class=\"pagination\">
                <button class=\"btn pagination-previous\"
                        data-section=\"top\"
                        data-action-name=\"goto_previous\"
                        title=\"";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.newer"), "html", null, true);
        echo "\"
                        ";
        // line 33
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "current", [], "any", false, false, false, 33) == 1)) {
            echo "disabled";
        }
        // line 34
        echo "                        type=\"button\"
                >
                    <span class=\"fa-chevron-left\" aria-hidden=\"true\"></span>
                    ";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.newer"), "html", null, true);
        echo "
                </button>
                <button class=\"btn icon-end pagination-next\"
                        data-section=\"top\"
                        data-action-name=\"goto_next\"
                        title=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.older"), "html", null, true);
        echo "\"
                        type=\"button\"
                >
                    ";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.older"), "html", null, true);
        echo "
                    <span class=\"fa-chevron-right\" aria-hidden=\"true\"></span>
                </button>
            </div>
            <div class=\"actions-panel\">
                ";
        // line 50
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "action btn btn-icon", "iCss" => "fa-refresh", "label" => (" " . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Refresh")), "dataAttributes" => ["action-name" => "refresh", "section" => "top"]]], 50, $context, $this->getSourceContext());
        // line 55
        echo "
            </div>
        </div>
        ";
    }

    // line 59
    public function block_items_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "            ";
        $context["options"] = ["widgetId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 61
($context["app"] ?? null), "request", [], "any", false, false, false, 61), "get", [0 => "_wid"], "method", false, false, false, 61), "activityListData" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_activity_widget_marketing_activities_list", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 65
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 66
($context["entity"] ?? null), "id", [], "any", false, false, false, 66)])), "activityListOptions" => ["configuration" =>         // line 70
($context["configuration"] ?? null), "template" => "#template-marketing-activities", "itemTemplate" => "#template-marketing-activities-item", "listWidgetSelector" => ".marketing-activities-container .marketing-activities-list-widget", "activityListSelector" => ".marketing-activities", "reloadOnAdd" => false, "reloadOnUpdate" => false, "triggerRefreshEvent" => false, "urls" => ["route" => "oro_marketing_activity_widget_marketing_activities_list", "parameters" => ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 81
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 82
($context["entity"] ?? null), "id", [], "any", false, false, false, 82)]], "loadingContainerSelector" => ".marketing-activities", "pager" =>         // line 86
($context["pager"] ?? null), "campaignFilterValues" =>         // line 87
($context["campaignFilterValues"] ?? null), "routes" => []]];
        // line 91
        echo "
            <div class=\"container-fluid accordion\"
                data-page-component-module=\"oromarketingactivity/js/app/components/marketing-activities-section-component\"
                data-page-component-options=\"";
        // line 94
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"></div>
            ";
        // line 95
        $this->loadTemplate("@OroMarketingActivity/MarketingActivity/js/list.html.twig", "@OroMarketingActivity/MarketingActivity/widget/marketingActivitiesSection.html.twig", 95)->display(twig_array_merge($context, ["id" => "template-marketing-activities"]));
        // line 96
        echo "            ";
        $this->loadTemplate("@OroMarketingActivity/MarketingActivity/js/marketingActivitySectionItem.html.twig", "@OroMarketingActivity/MarketingActivity/widget/marketingActivitiesSection.html.twig", 96)->display(twig_array_merge($context, ["id" => "template-marketing-activities-item"]));
        // line 97
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["configuration"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["activityClass"] => $context["activityOptions"]) {
            // line 98
            echo "                ";
            $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["activityOptions"], "template", [], "any", false, false, false, 98), "@OroMarketingActivity/MarketingActivity/widget/marketingActivitiesSection.html.twig", 98)->display(twig_array_merge($context, ["id" => ("template-activity-item-" . $context["activityClass"])]));
            // line 99
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['activityClass'], $context['activityOptions'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 100
        echo "        ";
    }

    public function getTemplateName()
    {
        return "@OroMarketingActivity/MarketingActivity/widget/marketingActivitiesSection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 100,  188 => 99,  185 => 98,  167 => 97,  164 => 96,  162 => 95,  158 => 94,  153 => 91,  151 => 87,  150 => 86,  149 => 82,  148 => 81,  147 => 70,  146 => 66,  145 => 65,  144 => 61,  142 => 60,  138 => 59,  131 => 55,  129 => 50,  121 => 45,  115 => 42,  107 => 37,  102 => 34,  98 => 33,  94 => 32,  84 => 24,  80 => 23,  75 => 101,  72 => 59,  70 => 23,  67 => 22,  63 => 21,  58 => 103,  55 => 21,  53 => 12,  51 => 11,  49 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMarketingActivity/MarketingActivity/widget/marketingActivitiesSection.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/MarketingActivityBundle/Resources/views/MarketingActivity/widget/marketingActivitiesSection.html.twig");
    }
}
