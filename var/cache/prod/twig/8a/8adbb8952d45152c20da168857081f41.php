<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/invitationControl.html.twig */
class __TwigTemplate_19819ec9a8308a2b84147254a90e1db0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/invitationControl.html.twig", 1)->unwrap();
        // line 2
        $macros["invitations"] = $this->macros["invitations"] = $this->loadTemplate("@OroCalendar/invitations.html.twig", "@OroCalendar/CalendarEvent/invitationControl.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        $context["statuses"] = [0 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_ACCEPTED"), 1 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_TENTATIVE"), 2 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED")];
        // line 9
        $context["properties"] = [];
        // line 10
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
            // line 11
            echo "    ";
            if (($context["status"] != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "invitationStatus", [], "any", false, false, false, 11))) {
                // line 12
                echo "        ";
                $context["properties"] = twig_array_merge(($context["properties"] ?? null), [0 => twig_call_macro($macros["UI"], "macro_link", [["label" => twig_call_macro($macros["invitations"], "macro_calendar_event_invitation_going_status", [                // line 14
$context["status"]], 14, $context, $this->getSourceContext()), "title" => twig_call_macro($macros["invitations"], "macro_calendar_event_invitation_going_status", [                // line 15
$context["status"]], 15, $context, $this->getSourceContext()), "path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(("oro_calendar_event_" .                 // line 16
$context["status"]), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 16)]), "data" => ["page-component-module" => "oroui/js/app/components/view-component", "page-component-options" => json_encode(["view" => "orocalendar/js/app/views/change-status-view", "triggerEventName" =>                 // line 21
($context["triggerEventName"] ?? null)])]]], 13, $context, $this->getSourceContext())]);
                // line 26
                echo "    ";
            } else {
                // line 27
                echo "        ";
                $context["properties"] = twig_array_merge(($context["properties"] ?? null), [0 => twig_call_macro($macros["invitations"], "macro_calendar_event_invitation_going_status", [                // line 28
$context["status"]], 28, $context, $this->getSourceContext())]);
                // line 30
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "
";
        // line 33
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.action.going_status.label"), twig_join_filter(        // line 35
($context["properties"] ?? null), "&nbsp;")], 33, $context, $this->getSourceContext());
        // line 36
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/invitationControl.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 36,  80 => 35,  79 => 33,  76 => 32,  69 => 30,  67 => 28,  65 => 27,  62 => 26,  60 => 21,  59 => 16,  58 => 15,  57 => 14,  55 => 12,  52 => 11,  48 => 10,  46 => 9,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/invitationControl.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/invitationControl.html.twig");
    }
}
