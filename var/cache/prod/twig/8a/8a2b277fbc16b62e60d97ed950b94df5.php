<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmbeddedForm/EmbeddedForm/index.html.twig */
class __TwigTemplate_031d2471596dbdbaffcba5372bd7546e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmbeddedForm/EmbeddedForm/index.html.twig", 2)->unwrap();
        // line 3
        $context["gridName"] = "embedded-forms-grid";
        // line 4
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.entity_plural_label");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroEmbeddedForm/EmbeddedForm/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmbeddedForm/EmbeddedForm/index.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_embedded_form_create")) {
            // line 10
            echo "        <div class=\"btn-group\">
            ";
            // line 11
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_embedded_form_create"), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.entity_label")]], 11, $context, $this->getSourceContext());
            // line 14
            echo "
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroEmbeddedForm/EmbeddedForm/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 14,  68 => 11,  65 => 10,  63 => 9,  60 => 8,  57 => 7,  53 => 6,  48 => 1,  46 => 4,  44 => 3,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmbeddedForm/EmbeddedForm/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/EmbeddedForm/index.html.twig");
    }
}
