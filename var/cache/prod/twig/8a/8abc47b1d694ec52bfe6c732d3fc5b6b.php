<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/mobile/entities.scss */
class __TwigTemplate_9298ae63907f5f39e3c796a3cc57bca1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:math';

.entities.list-group {
    .list-group-item {
        width: calc(#{\$entities-list-mobile-group-item-width} - #{\$entities-list-group-item-offset-start});

        &__header {
            padding-right: \$entities-list-group-item-header-mobile-inner-offset-end;
        }
    }
}

@media screen and (min-width: 654px) {
    .entities.list-group {
        .list-group-item {
            width: calc(#{(\$entities-list-mobile-group-item-width * .5)} - #{\$entities-list-group-item-offset-start});
        }
    }
}

@media screen and (min-width: 950px) {
    .entities.list-group {
        .list-group-item {
            width:
                calc(
                    #{math.div(\$entities-list-mobile-group-item-width, 3)} -
                    #{\$entities-list-group-item-offset-start}
                );
        }
    }
}

@media screen and (min-width: 1252px) {
    .entities.list-group {
        .list-group-item {
            width: calc(#{(\$entities-list-mobile-group-item-width * .25)} - #{\$entities-list-group-item-offset-start});
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/mobile/entities.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/mobile/entities.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/mobile/entities.scss");
    }
}
