<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSecurity/Form/fields.html.twig */
class __TwigTemplate_2ee88edee5ef40e46f00bf087bdc438b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_acl_access_level_selector_widget' => [$this, 'block_oro_acl_access_level_selector_widget'],
            'oro_acl_privilege_identity_widget' => [$this, 'block_oro_acl_privilege_identity_widget'],
            'oro_acl_label_widget' => [$this, 'block_oro_acl_label_widget'],
            'oro_acl_object_name_widget' => [$this, 'block_oro_acl_object_name_widget'],
            'oro_acl_collection_widget' => [$this, 'block_oro_acl_collection_widget'],
            'oro_acl_privilege_widget' => [$this, 'block_oro_acl_privilege_widget'],
            'oro_acl_permission_widget' => [$this, 'block_oro_acl_permission_widget'],
            'oro_acl_permission_collection_widget' => [$this, 'block_oro_acl_permission_collection_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_acl_access_level_selector_widget', $context, $blocks);
        // line 19
        echo "
";
        // line 20
        $this->displayBlock('oro_acl_privilege_identity_widget', $context, $blocks);
        // line 28
        echo "
";
        // line 29
        $this->displayBlock('oro_acl_label_widget', $context, $blocks);
        // line 40
        echo "
";
        // line 41
        $this->displayBlock('oro_acl_object_name_widget', $context, $blocks);
        // line 45
        echo "
";
        // line 46
        $this->displayBlock('oro_acl_collection_widget', $context, $blocks);
        // line 69
        echo "
";
        // line 70
        $this->displayBlock('oro_acl_privilege_widget', $context, $blocks);
        // line 92
        echo "
";
        // line 93
        $this->displayBlock('oro_acl_permission_widget', $context, $blocks);
        // line 97
        echo "
";
        // line 98
        $this->displayBlock('oro_acl_permission_collection_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_acl_access_level_selector_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["additionalClass"] = "";
        // line 3
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 3), "parent", [], "any", false, true, false, 3), "vars", [], "any", false, true, false, 3), "privileges_config", [], "any", false, true, false, 3), "view_type", [], "any", true, true, false, 3) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 3), "parent", [], "any", false, false, false, 3), "vars", [], "any", false, false, false, 3), "privileges_config", [], "any", false, false, false, 3), "view_type", [], "any", false, false, false, 3) != "grid"))) {
            // line 4
            echo "        ";
            $context["additionalClass"] = "span2";
            // line 5
            echo "    ";
        }
        // line 6
        echo "    ";
        $context["label"] = ((( !array_key_exists("level_label", $context) || twig_test_empty(($context["level_label"] ?? null)))) ? ((        // line 7
($context["translation_prefix"] ?? null) . "NONE")) : ((        // line 8
($context["translation_prefix"] ?? null) . ($context["level_label"] ?? null))));
        // line 10
        echo "    <div class=\"access_level_value ";
        echo twig_escape_filter($this->env, ($context["additionalClass"] ?? null), "html", null, true);
        echo "\"
         data-identity=\"";
        // line 11
        echo twig_escape_filter($this->env, ($context["identity"] ?? null), "html", null, true);
        echo "\"
         data-selector-id=\"";
        // line 12
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\"
         data-selector-name=\"";
        // line 13
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\"
         data-value=\"";
        // line 14
        echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
        echo "\"
    >
        <input type=\"hidden\" data-value-text=\"";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
        echo "\">
    </div>
";
    }

    // line 20
    public function block_oro_acl_privilege_identity_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        $context["class"] = "";
        // line 22
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 22), "vars", [], "any", false, true, false, 22), "privileges_config", [], "any", false, true, false, 22), "view_type", [], "any", true, true, false, 22) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 22), "vars", [], "any", false, false, false, 22), "privileges_config", [], "any", false, false, false, 22), "view_type", [], "any", false, false, false, 22) != "grid"))) {
            // line 23
            echo "        ";
            $context["class"] = "span8";
            // line 24
            echo "    ";
        }
        // line 25
        echo "
    ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ["class" => ($context["class"] ?? null)]]);
        echo "
";
    }

    // line 29
    public function block_oro_acl_label_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSecurity/Form/fields.html.twig", 30)->unwrap();
        // line 31
        echo "    <strong>
        ";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["value"] ?? null)), "html", null, true);
        echo "
    </strong>
    ";
        // line 34
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 34), "parent", [], "any", false, false, false, 34), "vars", [], "any", false, false, false, 34), "value", [], "any", false, false, false, 34), "description", [], "any", false, false, false, 34)) {
            // line 35
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_tooltip", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 35), "parent", [], "any", false, false, false, 35), "vars", [], "any", false, false, false, 35), "value", [], "any", false, false, false, 35), "description", [], "any", false, false, false, 35))], 35, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 37
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter(($context["type"] ?? null), "hidden")) : ("hidden"));
        // line 38
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 41
    public function block_oro_acl_object_name_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "oid", [], "any", false, false, false, 42), 'widget');
        echo "
    ";
        // line 43
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 43), 'widget');
        echo "
";
    }

    // line 46
    public function block_oro_acl_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "    <div data-page-component-module=\"";
        echo twig_escape_filter($this->env, ($context["page_component_module"] ?? null));
        echo "\" data-page-component-options=\"";
        echo twig_escape_filter($this->env, json_encode(($context["page_component_options"] ?? null)));
        echo "\">
        ";
        // line 48
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["privileges_config"] ?? null), "view_type", [], "any", false, false, false, 48) == "grid")) {
            // line 49
            echo "            <table class=\"table acl-table\">
                <tbody>
                <tr>
                    <th class=\"span6\">&nbsp;</th>
                    <th>";
            // line 53
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.security.permissions"), "html", null, true);
            echo "</th>
                </tr>
                ";
            // line 55
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 56
                echo "                    ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
                echo "
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "                </tbody>
            </table>
        ";
        } else {
            // line 61
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 62
                echo "                <div class=\"security-row row-fluid\">
                    ";
                // line 63
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
                echo "
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 66
            echo "        ";
        }
        // line 67
        echo "    </div>
";
    }

    // line 70
    public function block_oro_acl_privilege_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "    ";
        ob_start(function () { return ''; });
        // line 72
        echo "        <tr ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 72), "value", [], "any", false, false, false, 72), "identity", [], "any", false, false, false, 72), "name", [], "any", false, false, false, 72) == "(default)")) {
            echo "class=\"default-field\"";
        }
        echo ">
            <td>
                ";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "identity", [], "any", false, false, false, 74), 'widget');
        echo "
            </td>
            ";
        // line 76
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["privileges_config"] ?? null), "view_type", [], "any", false, false, false, 76) == "grid")) {
            // line 77
            echo "                <td>
                    <div class=\"table-responsive\">
                        <table class=\"table table-condensed\">
                            <tbody>
                                ";
            // line 81
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "permissions", [], "any", false, false, false, 81), 'widget');
            echo "
                            </tbody>
                        </table>
                    </div>
                </td>
            ";
        } else {
            // line 87
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "permissions", [], "any", false, false, false, 87), 'widget');
            echo "
            ";
        }
        // line 89
        echo "        </tr>
    ";
        $___internal_parse_39_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 71
        echo twig_spaceless($___internal_parse_39_);
    }

    // line 93
    public function block_oro_acl_permission_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 94
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "accessLevel", [], "any", false, false, false, 94), 'widget');
        echo "
    ";
        // line 95
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 95), 'widget');
        echo "
";
    }

    // line 98
    public function block_oro_acl_permission_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 99
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((($__internal_compile_0 = ($context["privileges_config"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["permissions"] ?? null) : null));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 100
            echo "        <tr ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 100), "vars", [], "any", false, false, false, 100), "value", [], "any", false, false, false, 100), "identity", [], "any", false, false, false, 100), "name", [], "any", false, false, false, 100) == "(default)")) {
                echo "class=\"default-field\"";
            }
            echo ">
            ";
            // line 101
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["permission"]) {
                // line 102
                echo "                ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["permission"], "vars", [], "any", false, false, false, 102), "value", [], "any", false, false, false, 102), "name", [], "any", false, false, false, 102) == $context["field"])) {
                    // line 103
                    echo "                    <td class=\"span8\">
                        ";
                    // line 104
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["privileges_config"] ?? null), "view_type", [], "any", false, false, false, 104) == "grid")) {
                        // line 105
                        echo "                            ";
                        $context["aclPermission"] = $this->extensions['Oro\Bundle\SecurityBundle\Twig\OroSecurityExtension']->getPermission(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["permission"], "vars", [], "any", false, false, false, 105), "value", [], "any", false, false, false, 105));
                        // line 106
                        echo "
                            ";
                        // line 107
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["aclPermission"] ?? null), "label", [], "any", true, true, false, 107)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["aclPermission"] ?? null), "label", [], "any", false, false, false, 107), $context["field"])) : ($context["field"]))), "html", null, true);
                        echo "
                            ";
                        // line 108
                        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["aclPermission"] ?? null), "description", [], "any", false, false, false, 108)) {
                            echo " (";
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["aclPermission"] ?? null), "description", [], "any", false, false, false, 108)), "html", null, true);
                            echo ")";
                        }
                        // line 109
                        echo "                        ";
                    }
                    // line 110
                    echo "                    </td>
                    <td>
                        ";
                    // line 112
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["permission"], 'widget');
                    echo "
                    </td>
                ";
                }
                // line 115
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['permission'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 116
            echo "        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "@OroSecurity/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  407 => 116,  401 => 115,  395 => 112,  391 => 110,  388 => 109,  382 => 108,  378 => 107,  375 => 106,  372 => 105,  370 => 104,  367 => 103,  364 => 102,  360 => 101,  353 => 100,  348 => 99,  344 => 98,  338 => 95,  333 => 94,  329 => 93,  325 => 71,  321 => 89,  315 => 87,  306 => 81,  300 => 77,  298 => 76,  293 => 74,  285 => 72,  282 => 71,  278 => 70,  273 => 67,  270 => 66,  261 => 63,  258 => 62,  253 => 61,  248 => 58,  239 => 56,  235 => 55,  230 => 53,  224 => 49,  222 => 48,  215 => 47,  211 => 46,  205 => 43,  200 => 42,  196 => 41,  189 => 38,  186 => 37,  180 => 35,  178 => 34,  173 => 32,  170 => 31,  167 => 30,  163 => 29,  157 => 26,  154 => 25,  151 => 24,  148 => 23,  145 => 22,  142 => 21,  138 => 20,  127 => 16,  122 => 14,  118 => 13,  114 => 12,  110 => 11,  105 => 10,  103 => 8,  102 => 7,  100 => 6,  97 => 5,  94 => 4,  91 => 3,  88 => 2,  84 => 1,  80 => 98,  77 => 97,  75 => 93,  72 => 92,  70 => 70,  67 => 69,  65 => 46,  62 => 45,  60 => 41,  57 => 40,  55 => 29,  52 => 28,  50 => 20,  47 => 19,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSecurity/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SecurityBundle/Resources/views/Form/fields.html.twig");
    }
}
