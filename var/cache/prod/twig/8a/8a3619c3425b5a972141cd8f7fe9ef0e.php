<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAddress/Include/fields.html.twig */
class __TwigTemplate_57731c68c04e8cfa8dc5c9720f5d94ef extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_region_widget' => [$this, 'block_oro_region_widget'],
            'oro_email_widget' => [$this, 'block_oro_email_widget'],
            'oro_phone_widget' => [$this, 'block_oro_phone_widget'],
            'oro_address_widget' => [$this, 'block_oro_address_widget'],
            'oro_address_rows' => [$this, 'block_oro_address_rows'],
            'oro_typed_address_widget' => [$this, 'block_oro_typed_address_widget'],
            'oro_typed_address_rows' => [$this, 'block_oro_typed_address_rows'],
            'oro_address_collection_widget' => [$this, 'block_oro_address_collection_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_region_widget', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('oro_email_widget', $context, $blocks);
        // line 61
        echo "
";
        // line 62
        $this->displayBlock('oro_phone_widget', $context, $blocks);
        // line 81
        echo "
";
        // line 82
        $this->displayBlock('oro_address_widget', $context, $blocks);
        // line 91
        echo "
";
        // line 92
        $this->displayBlock('oro_address_rows', $context, $blocks);
        // line 110
        echo "
";
        // line 111
        $this->displayBlock('oro_typed_address_widget', $context, $blocks);
        // line 114
        echo "
";
        // line 115
        $this->displayBlock('oro_typed_address_rows', $context, $blocks);
        // line 120
        echo "
";
        // line 121
        $this->displayBlock('oro_address_collection_widget', $context, $blocks);
        // line 129
        echo "
";
    }

    // line 1
    public function block_oro_region_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        // line 3
        echo "    ";
        if ( !        $this->hasBlock("oro_region_updater_js", $context, $blocks)) {
            // line 4
            echo "        ";
            $this->loadTemplate("@OroAddress/Include/javascript.html.twig", "@OroAddress/Include/fields.html.twig", 4)->displayBlock("oro_region_updater_js", $context);
            echo "
    ";
        }
        // line 6
        echo "
    ";
        // line 7
        if (( !array_key_exists("country_field", $context) || twig_test_empty(($context["country_field"] ?? null)))) {
            // line 8
            echo "        ";
            $context["country_field"] = twig_replace_filter(($context["name"] ?? null), ["region" => "country"]);
            // line 9
            echo "    ";
        }
        // line 10
        echo "    ";
        $context["country_field"] = (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 10)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[($context["country_field"] ?? null)] ?? null) : null);
        // line 11
        echo "
    ";
        // line 12
        if (( !array_key_exists("region_text_field", $context) || twig_test_empty(($context["region_text_field"] ?? null)))) {
            // line 13
            echo "        ";
            $context["region_text_field"] = (($context["name"] ?? null) . "_text");
            // line 14
            echo "    ";
        }
        // line 15
        echo "    ";
        $context["region_text_field"] = (($__internal_compile_1 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 15)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[($context["region_text_field"] ?? null)] ?? null) : null);
        // line 16
        echo "
    ";
        // line 17
        $context["attr"] = [];
        // line 18
        echo "
    ";
        // line 19
        if (($context["required"] ?? null)) {
            // line 20
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["attr" => ["data-validation" => json_encode(["NotBlank" => null])]]);
            // line 21
            echo "    ";
        }
        // line 22
        echo "
    ";
        // line 23
        $context["showSelect"] = ((( !twig_test_empty(($context["choices"] ?? null)) && twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["region_text_field"] ?? null), "vars", [], "any", false, false, false, 23), "value", [], "any", false, false, false, 23)))) ? (" show-select") : (""));
        // line 24
        echo "    <div class=\"region-widget";
        echo twig_escape_filter($this->env, ($context["showSelect"] ?? null), "html", null, true);
        echo "\">
        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ($context["attr"] ?? null));
        echo "
        ";
        // line 26
        $context["regionView"] = ((array_key_exists("regionView", $context)) ? (_twig_default_filter(($context["regionView"] ?? null), "oroaddress/js/region/view")) : ("oroaddress/js/region/view"));
        // line 27
        echo "        ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAddress/Include/fields.html.twig", 27)->unwrap();
        // line 28
        echo "        <div ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" =>         // line 29
($context["regionView"] ?? null), "options" => ["_sourceElement" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 31
($context["country_field"] ?? null), "vars", [], "any", false, false, false, 31), "id", [], "any", false, false, false, 31)), "target" => ("#" .         // line 32
($context["id"] ?? null)), "simpleEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 33
($context["region_text_field"] ?? null), "vars", [], "any", false, false, false, 33), "id", [], "any", false, false, false, 33)), "collectionRoute" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["form"] ?? null), "parent", [], "any", false, true, false, 34), "vars", [], "any", false, true, false, 34), "region_route", [], "any", true, true, false, 34)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 34), "vars", [], "any", false, true, false, 34), "region_route", [], "any", false, false, false, 34), "oro_api_country_get_regions")) : ("oro_api_country_get_regions")), "showSelect" =>         // line 35
($context["showSelect"] ?? null), "regionRequired" =>         // line 36
($context["required"] ?? null)]]], 28, $context, $this->getSourceContext());
        // line 38
        echo "></div>
    </div>
";
    }

    // line 42
    public function block_oro_email_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    <div class=\"float-holder ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 43), "vars", [], "any", false, false, false, 43), "errors", [], "any", false, false, false, 43)) > 0)) {
            echo " validation-error";
        }
        echo "\">
        <div class=\"input-append collection-element-primary\">
            ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 45), 'widget');
        echo "<label class=\"add-on\" title=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Primary"), "html", null, true);
        echo "\">
                ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "primary", [], "any", false, false, false, 46), 'widget');
        echo "
            </label>
        </div>
        ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 49), 'errors');
        echo "
        ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 50));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 51
            echo "            ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, true, false, 51), "extra_field", [], "any", true, true, false, 51) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 51), "extra_field", [], "any", false, false, false, 51))) {
                // line 52
                echo "                <div class=\"collection-element-other clearfix\">
                    ";
                // line 53
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', ["attr" => ["title" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 53), "label", [], "any", false, false, false, 53)]]);
                echo "
                </div>
                ";
                // line 55
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'errors');
                echo "
            ";
            }
            // line 57
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 58
        echo "    </div>
    ";
        // line 59
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
    }

    // line 62
    public function block_oro_phone_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "    <div class=\"float-holder ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phone", [], "any", false, false, false, 63), "vars", [], "any", false, false, false, 63), "errors", [], "any", false, false, false, 63)) > 0)) {
            echo " validation-error";
        }
        echo "\">
        <div class=\"input-append collection-element-primary\">
            ";
        // line 65
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phone", [], "any", false, false, false, 65), 'widget');
        echo "<label class=\"add-on\" title=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Primary"), "html", null, true);
        echo "\">
                ";
        // line 66
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "primary", [], "any", false, false, false, 66), 'widget');
        echo "
            </label>
        </div>
        ";
        // line 69
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phone", [], "any", false, false, false, 69), 'errors');
        echo "
        ";
        // line 70
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 70));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 71
            echo "            ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, true, false, 71), "extra_field", [], "any", true, true, false, 71) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 71), "extra_field", [], "any", false, false, false, 71))) {
                // line 72
                echo "                <div class=\"collection-element-other clearfix\">
                    ";
                // line 73
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', ["attr" => ["title" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 73), "label", [], "any", false, false, false, 73)]]);
                echo "
                </div>
                ";
                // line 75
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'errors');
                echo "
            ";
            }
            // line 77
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 78
        echo "    </div>
    ";
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
    }

    // line 82
    public function block_oro_address_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 83
        echo "    ";
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 83))) {
            // line 84
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 85
            $this->displayBlock("oro_address_rows", $context, $blocks);
            echo "
        </div>
    ";
        } else {
            // line 88
            echo "        ";
            $this->displayBlock("oro_address_rows", $context, $blocks);
            echo "
    ";
        }
    }

    // line 92
    public function block_oro_address_rows($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 93
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "id", [], "any", false, false, false, 93), 'row');
        echo "
    ";
        // line 94
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 94), 'row');
        echo "
    ";
        // line 95
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "namePrefix", [], "any", false, false, false, 95), 'row');
        echo "
    ";
        // line 96
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "firstName", [], "any", false, false, false, 96), 'row');
        echo "
    ";
        // line 97
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "middleName", [], "any", false, false, false, 97), 'row');
        echo "
    ";
        // line 98
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "lastName", [], "any", false, false, false, 98), 'row');
        echo "
    ";
        // line 99
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "nameSuffix", [], "any", false, false, false, 99), 'row');
        echo "
    ";
        // line 100
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", false, false, false, 100), 'row');
        echo "
    ";
        // line 101
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "country", [], "any", false, false, false, 101), 'row');
        echo "
    ";
        // line 102
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "street", [], "any", false, false, false, 102), 'row');
        echo "
    ";
        // line 103
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "street2", [], "any", false, false, false, 103), 'row');
        echo "
    ";
        // line 104
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "city", [], "any", false, false, false, 104), 'row');
        echo "
    ";
        // line 105
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "region_text", [], "any", false, false, false, 105), 'row');
        echo "
    ";
        // line 106
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "region", [], "any", false, false, false, 106), 'row');
        echo "
    ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "postalCode", [], "any", false, false, false, 107), 'row');
        echo "
    ";
        // line 108
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
    }

    // line 111
    public function block_oro_typed_address_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 112
        echo "    ";
        $this->displayBlock("oro_typed_address_rows", $context, $blocks);
        echo "
";
    }

    // line 115
    public function block_oro_typed_address_rows($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 116
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "types", [], "any", false, false, false, 116), 'row');
        echo "
    ";
        // line 117
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "primary", [], "any", false, false, false, 117), 'row');
        echo "
    ";
        // line 118
        $this->displayBlock("oro_address_rows", $context, $blocks);
        echo "
";
    }

    // line 121
    public function block_oro_address_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 122
        echo "    ";
        $macros["addressIncludeFields"] = $this;
        // line 123
        echo "
    ";
        // line 124
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 124)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 124) . " ")) : ("")) . "collection-fields-list-bg oro-address-collection")]);
        // line 125
        echo "    ";
        $this->displayBlock("oro_collection_widget", $context, $blocks);
        echo "
    ";
        // line 126
        $context["id"] = (($context["id"] ?? null) . "_collection");
        // line 127
        echo "    ";
        echo twig_call_macro($macros["addressIncludeFields"], "macro_oro_collection_validate_types_js", [$context], 127, $context, $this->getSourceContext());
        echo "
";
    }

    // line 130
    public function macro_oro_collection_validate_types_js($__context__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "context" => $__context__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 131
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "form", [], "any", false, true, false, 131), "vars", [], "any", false, true, false, 131), "prototype", [], "any", false, true, false, 131), "types", [], "any", true, true, false, 131)) {
                // line 132
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAddress/Include/fields.html.twig", 132)->unwrap();
                // line 133
                echo "
        <div ";
                // line 134
                echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroaddress/js/app/views/address-collection-view", "options" => ["_sourceElement" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 137
($context["context"] ?? null), "id", [], "any", false, false, false, 137))]]], 134, $context, $this->getSourceContext());
                // line 139
                echo "></div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroAddress/Include/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  485 => 139,  483 => 137,  482 => 134,  479 => 133,  476 => 132,  473 => 131,  460 => 130,  453 => 127,  451 => 126,  446 => 125,  444 => 124,  441 => 123,  438 => 122,  434 => 121,  428 => 118,  424 => 117,  419 => 116,  415 => 115,  408 => 112,  404 => 111,  398 => 108,  394 => 107,  390 => 106,  386 => 105,  382 => 104,  378 => 103,  374 => 102,  370 => 101,  366 => 100,  362 => 99,  358 => 98,  354 => 97,  350 => 96,  346 => 95,  342 => 94,  337 => 93,  333 => 92,  325 => 88,  319 => 85,  314 => 84,  311 => 83,  307 => 82,  301 => 79,  298 => 78,  292 => 77,  287 => 75,  282 => 73,  279 => 72,  276 => 71,  272 => 70,  268 => 69,  262 => 66,  256 => 65,  248 => 63,  244 => 62,  238 => 59,  235 => 58,  229 => 57,  224 => 55,  219 => 53,  216 => 52,  213 => 51,  209 => 50,  205 => 49,  199 => 46,  193 => 45,  185 => 43,  181 => 42,  175 => 38,  173 => 36,  172 => 35,  171 => 34,  170 => 33,  169 => 32,  168 => 31,  167 => 29,  165 => 28,  162 => 27,  160 => 26,  156 => 25,  151 => 24,  149 => 23,  146 => 22,  143 => 21,  140 => 20,  138 => 19,  135 => 18,  133 => 17,  130 => 16,  127 => 15,  124 => 14,  121 => 13,  119 => 12,  116 => 11,  113 => 10,  110 => 9,  107 => 8,  105 => 7,  102 => 6,  96 => 4,  93 => 3,  91 => 2,  87 => 1,  82 => 129,  80 => 121,  77 => 120,  75 => 115,  72 => 114,  70 => 111,  67 => 110,  65 => 92,  62 => 91,  60 => 82,  57 => 81,  55 => 62,  52 => 61,  50 => 42,  47 => 41,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAddress/Include/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AddressBundle/Resources/views/Include/fields.html.twig");
    }
}
