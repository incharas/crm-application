<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroConfig/configPage.html.twig */
class __TwigTemplate_780d2fb05a4e375177fd9609a2d89390 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroConfig/configPage.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        if (($context["form"] ?? null)) {
            // line 13
            $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroConfig/Form/fields.html.twig", 1 => "@OroForm/Form/fields.html.twig", 2 => "@OroLocale/Form/fields.html.twig"], true);
        }
        // line 15
        $macros["syncMacro"] = $this->macros["syncMacro"] = $this->loadTemplate("@OroSync/Include/contentTags.html.twig", "@OroConfig/configPage.html.twig", 15)->unwrap();
        // line 16
        $macros["configUI"] = $this->macros["configUI"] = $this->loadTemplate("@OroConfig/macros.html.twig", "@OroConfig/configPage.html.twig", 16)->unwrap();
        // line 17
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroConfig/configPage.html.twig", 17)->unwrap();
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 19
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroConfig/configPage.html.twig", 20)->unwrap();
        // line 21
        echo "
    ";
        // line 22
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 23
            echo "        ";
            $context["saveButton"] = twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.config.actions.save_settings")]], 23, $context, $this->getSourceContext());
            // line 26
            echo "        ";
            $context["restoreButton"] = twig_call_macro($macros["UI"], "macro_buttonType", [["type" => "reset", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.config.actions.restore_saved_values")]], 26, $context, $this->getSourceContext());
            // line 27
            echo "        ";
            $context["pageReload"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 27), "block_config", [], "any", false, false, false, 27), ($context["activeSubGroup"] ?? null), [], "any", false, false, false, 27), "page_reload", [], "any", false, false, false, 27);
            // line 28
            echo "        ";
            $context["options"] = ["view" => "oroconfig/js/form/config-form", "pageReload" =>             // line 30
($context["pageReload"] ?? null), "isFormValid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["form"] ?? null), "vars", [], "any", false, false, false, 31), "valid", [], "any", false, false, false, 31)];
            // line 33
            echo "
        ";
            // line 34
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" =>             // line 35
($context["formAction"] ?? null), "attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 37
($context["form"] ?? null), "vars", [], "any", false, false, false, 37), "id", [], "any", false, false, false, 37), "data-scope-class" => ((            // line 38
array_key_exists("scopeEntityClass", $context)) ? (($context["scopeEntityClass"] ?? null)) : (null)), "data-scope-id" => ((            // line 39
array_key_exists("scopeEntityId", $context)) ? (($context["scopeEntityId"] ?? null)) : (null)), "data-collect" => "true", "data-page-component-view" => json_encode(            // line 41
($context["options"] ?? null))]]);
            // line 43
            echo "
            ";
            // line 44
            echo twig_call_macro($macros["configUI"], "macro_renderTitleAndButtons", [($context["pageTitle"] ?? null), [0 => ($context["restoreButton"] ?? null), 1 => ($context["saveButton"] ?? null)]], 44, $context, $this->getSourceContext());
            echo "
            ";
            // line 45
            echo twig_call_macro($macros["configUI"], "macro_renderScrollData", [($context["data"] ?? null), ($context["form"] ?? null), ($context["activeGroup"] ?? null), ($context["activeSubGroup"] ?? null), ($context["routeName"] ?? null), ($context["routeParameters"] ?? null)], 45, $context, $this->getSourceContext());
            echo "
        ";
            // line 46
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end', ["render_rest" => false]);
            echo "
        ";
            // line 47
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
        ";
            // line 48
            echo twig_call_macro($macros["syncMacro"], "macro_syncContentTags", [["name" => "system_configuration", "params" => [0 => ($context["activeGroup"] ?? null), 1 => ($context["activeSubGroup"] ?? null)]]], 48, $context, $this->getSourceContext());
            echo "
    ";
        } else {
            // line 50
            echo "        ";
            // line 51
            echo "        <div class=\"no-data\">
            ";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.config.system_configuration.desktop_only"), "html", null, true);
            echo "
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroConfig/configPage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  122 => 52,  119 => 51,  117 => 50,  112 => 48,  108 => 47,  104 => 46,  100 => 45,  96 => 44,  93 => 43,  91 => 41,  90 => 39,  89 => 38,  88 => 37,  87 => 35,  86 => 34,  83 => 33,  81 => 31,  80 => 30,  78 => 28,  75 => 27,  72 => 26,  69 => 23,  67 => 22,  64 => 21,  61 => 20,  57 => 19,  53 => 1,  51 => 17,  49 => 16,  47 => 15,  44 => 13,  42 => 12,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroConfig/configPage.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Resources/views/configPage.html.twig");
    }
}
