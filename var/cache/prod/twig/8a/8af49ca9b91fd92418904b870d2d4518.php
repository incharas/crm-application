<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/variables/textarea-config.scss */
class __TwigTemplate_3f1397f3e7dd9d6501f08714e1ed58db extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@use 'sass:color';

\$use-base-style-for-textarea: true !default;

// Default
\$textarea-padding: 8px 9px !default;
\$textarea-padding--m: 8px 9px 7px !default;
\$textarea-padding--s: 5px 9px 4px !default;
\$textarea-min-height: 36px !default;
\$textarea-font-size: \$base-ui-element-font-size !default;
\$textarea-font-family: \$base-ui-element-font-family !default;
\$textarea-line-height: \$base-ui-element-line-height !default;
\$textarea-border: \$base-ui-element-border !default;
\$textarea-border-radius: \$base-ui-element-border-radius !default;
\$textarea-background-color: \$base-ui-element-bg-color !default;
\$textarea-color: \$base-ui-element-color !default;
\$textarea-placeholder-color: get-color('additional', 'middle') !default;
// Hover
\$textarea-border-color-hover-state: get-color('additional', 'dark') !default;
\$textarea-box-shadow-hover-state: inset 0 1px 1px color.scale(get-color('additional', 'ultra'), \$alpha: -75%),
                                        0 0 8px color.scale(get-color('ui', 'focus'), \$alpha: -60%);

// Focus
\$textarea-border-color-focus-state: \$base-ui-element-border-color-focus !default;
\$textarea-box-shadow-focus-state: \$base-ui-element-focus-style !default;

// Error
\$textarea-border-color-error-state: get-color('ui', 'error-dark') !default;
\$textarea-box-shadow-error-state: 0 1px 7px 0 color.scale(get-color('ui', 'error-dark'), \$alpha: -60%) !default;

// Disabled

\$textarea-border-color-disabled-background: get-color('additional', 'base') !default;
\$textarea-border-color-disabled-hover-border-color: get-color('additional', 'light') !default;
\$textarea-background-color-disabled-state: get-color('additional', 'base') !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/variables/textarea-config.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/variables/textarea-config.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/variables/textarea-config.scss");
    }
}
