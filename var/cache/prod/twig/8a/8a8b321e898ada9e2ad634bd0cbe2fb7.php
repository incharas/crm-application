<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTask/TaskCrud/update.html.twig */
class __TwigTemplate_da5c892d3ec8d0300bf579afb2f2c5a8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/update.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.subject%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "subject", [], "any", false, false, false, 4), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.entity_label")]]);
        // line 5
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroTask/TaskCrud/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/update.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_task_view", "params" => ["id" => "\$id"]]], 10, $context, $this->getSourceContext());
        // line 14
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_task_create")) {
            // line 15
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_task_create"]], 15, $context, $this->getSourceContext()));
            // line 18
            echo "    ";
        }
        // line 19
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_task_update")) {
            // line 20
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_task_update", "params" => ["id" => "\$id"]]], 20, $context, $this->getSourceContext()));
            // line 24
            echo "    ";
        }
        // line 25
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 25, $context, $this->getSourceContext());
        echo "
    ";
        // line 26
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_task_index")], 26, $context, $this->getSourceContext());
        echo "
";
    }

    // line 29
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 31
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 32
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_task_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 35
($context["entity"] ?? null), "subject", [], "any", false, false, false, 35)];
            // line 37
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 39
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.entity_label")]);
            // line 40
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroTask/TaskCrud/update.html.twig", 40)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 41
            echo "    ";
        }
    }

    // line 44
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/update.html.twig", 45)->unwrap();
        // line 46
        echo "
    ";
        // line 47
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <span class=\"page-title__status\">
        ";
        // line 49
        $context["status"] = ["open" => "enabled", "in_progress" => "tentatively", "closed" => "disabled"];
        // line 54
        echo "        ";
        if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 54))) {
            // line 55
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 55), "name", [], "any", false, false, false, 55), (((($__internal_compile_0 = ($context["status"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 55), "id", [], "any", false, false, false, 55)] ?? null) : null)) ? ((($__internal_compile_1 = ($context["status"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 55), "id", [], "any", false, false, false, 55)] ?? null) : null)) : ("disabled"))], 55, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 57
        echo "    </span>
";
    }

    // line 60
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 61
        echo "    ";
        $context["id"] = "task-form";
        // line 62
        echo "
    ";
        // line 63
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 69
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subject", [], "any", false, false, false, 69), 'row'), 1 =>         // line 70
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 70), 'row'), 2 =>         // line 71
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "dueDate", [], "any", false, false, false, 71), 'row'), 3 =>         // line 72
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "status", [], "any", false, false, false, 72), 'row'), 4 =>         // line 73
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "taskPriority", [], "any", false, false, false, 73), 'row'), 5 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 74
($context["form"] ?? null), "owner", [], "any", true, true, false, 74)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 74), 'row')) : ("")), 6 =>         // line 75
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "reminders", [], "any", false, false, false, 75), 'row')]]]]];
        // line 80
        echo "
    ";
        // line 81
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 82
        echo "
    ";
        // line 83
        $context["data"] = ["formErrors" => ((        // line 84
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 85
($context["dataBlocks"] ?? null)];
        // line 87
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTask/TaskCrud/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 87,  187 => 85,  186 => 84,  185 => 83,  182 => 82,  180 => 81,  177 => 80,  175 => 75,  174 => 74,  173 => 73,  172 => 72,  171 => 71,  170 => 70,  169 => 69,  168 => 63,  165 => 62,  162 => 61,  158 => 60,  153 => 57,  147 => 55,  144 => 54,  142 => 49,  137 => 47,  134 => 46,  131 => 45,  127 => 44,  122 => 41,  119 => 40,  116 => 39,  110 => 37,  108 => 35,  107 => 32,  105 => 31,  102 => 30,  98 => 29,  92 => 26,  87 => 25,  84 => 24,  81 => 20,  78 => 19,  75 => 18,  72 => 15,  69 => 14,  67 => 10,  64 => 9,  61 => 8,  57 => 7,  52 => 1,  50 => 5,  48 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTask/TaskCrud/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-task-bundle/Resources/views/TaskCrud/update.html.twig");
    }
}
