<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMessageQueue/macros.html.twig */
class __TwigTemplate_9886bfb88911cd89117f0d4e5e88fd23 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_getJobStatusClass($__job__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "job" => $__job__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            ob_start(function () { return ''; });
            // line 3
            echo "    ";
            $context["status"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["job"] ?? null), "status", [], "any", false, false, false, 3);
            // line 4
            echo "
    ";
            // line 5
            if ((($context["status"] ?? null) === constant(get_class(($context["job"] ?? null))."::"."STATUS_RUNNING"))) {
                // line 6
                echo "        ";
                $context["class"] = "info";
                // line 7
                echo "    ";
            } elseif ((($context["status"] ?? null) === constant(get_class(($context["job"] ?? null))."::"."STATUS_SUCCESS"))) {
                // line 8
                echo "        ";
                $context["class"] = "success";
                // line 9
                echo "    ";
            } elseif ((($context["status"] ?? null) === constant(get_class(($context["job"] ?? null))."::"."STATUS_FAILED"))) {
                // line 10
                echo "        ";
                $context["class"] = "warning";
                // line 11
                echo "    ";
            } elseif ((($context["status"] ?? null) === constant(get_class(($context["job"] ?? null))."::"."STATUS_FAILED_REDELIVERED"))) {
                // line 12
                echo "        ";
                $context["class"] = "warning";
                // line 13
                echo "    ";
            } elseif (twig_in_filter(($context["status"] ?? null), [0 => twig_constant("STATUS_CANCELLED", ($context["job"] ?? null)), 1 => twig_constant("STATUS_STALE", ($context["job"] ?? null))])) {
                // line 14
                echo "        ";
                $context["class"] = "inverse";
                // line 15
                echo "    ";
            } else {
                // line 16
                echo "        ";
                $context["class"] = "";
                // line 17
                echo "    ";
            }
            // line 18
            echo "
    ";
            // line 19
            echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
            echo "
";
            $___internal_parse_45_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 2
            echo twig_spaceless($___internal_parse_45_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroMessageQueue/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 2,  101 => 19,  98 => 18,  95 => 17,  92 => 16,  89 => 15,  86 => 14,  83 => 13,  80 => 12,  77 => 11,  74 => 10,  71 => 9,  68 => 8,  65 => 7,  62 => 6,  60 => 5,  57 => 4,  54 => 3,  52 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMessageQueue/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MessageQueueBundle/Resources/views/macros.html.twig");
    }
}
