<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroApi/Collector/api.html.twig */
class __TwigTemplate_4b5a716183e0232609b352ed8a16defa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'menu' => [$this, 'block_menu'],
            'panel' => [$this, 'block_panel'],
            'stats' => [$this, 'block_stats'],
            'actions' => [$this, 'block_actions'],
            'applicableCheckers' => [$this, 'block_applicableCheckers'],
            'processors' => [$this, 'block_processors'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@OroApi/Collector/api.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_toolbar($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 6
    public function block_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <span class=\"label ";
        echo ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "empty", [], "any", false, false, false, 7)) ? ("disabled") : (""));
        echo "\">
        <strong>API</strong>
    </span>
";
    }

    // line 12
    public function block_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "empty", [], "any", false, false, false, 13)) {
            // line 14
            echo "        <h2>API</h2>
        <div class=\"empty\">
            <p>No API actions were executed.</p>
        </div>
    ";
        } else {
            // line 19
            echo "        ";
            $this->displayBlock("stats", $context, $blocks);
            echo "
        ";
            // line 20
            $this->displayBlock("actions", $context, $blocks);
            echo "
        ";
            // line 21
            $this->displayBlock("applicableCheckers", $context, $blocks);
            echo "
        ";
            // line 22
            $this->displayBlock("processors", $context, $blocks);
            echo "

        <script>//<![CDATA[

            function sortTable(header, column, targetId) {
                \"use strict\";

                var direction = parseInt(header.getAttribute('data-sort-direction')) || 1,
                    items = [],
                    target = document.getElementById(targetId),
                    rows = target.children,
                    headers = header.parentElement.children,
                    i;

                for (i = 0; i < rows.length; ++i) {
                    items.push(rows[i]);
                }

                for (i = 0; i < headers.length; ++i) {
                    headers[i].removeAttribute('data-sort-direction');
                    if (headers[i].children.length > 0) {
                        headers[i].children[0].innerHTML = '';
                    }
                }

                header.setAttribute('data-sort-direction', (-1*direction).toString());
                header.children[0].innerHTML = direction > 0 ? '&#9650;' : '&#9660;';

                items.sort(function(a, b) {
                    return direction*(parseFloat(a.children[column].innerHTML) - parseFloat(b.children[column].innerHTML));
                });

                for (i = 0; i < items.length; ++i) {
                    Sfjs.removeClass(items[i], i % 2 ? 'even' : 'odd');
                    Sfjs.addClass(items[i], i % 2 ? 'odd' : 'even');
                    target.appendChild(items[i]);
                }
            }

        //]]></script>
    ";
        }
    }

    // line 65
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "    <h2>API Metrics</h2>

    <div class=\"metrics\">
        <div class=\"metric\">
            <span class=\"value\">";
        // line 70
        echo twig_escape_filter($this->env, twig_sprintf("%0.2f", (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "totalTime", [], "any", false, false, false, 70) * 1000)), "html", null, true);
        echo " ms</span>
            <span class=\"label\">Total time</span>
        </div>
        <div class=\"metric\">
            <span class=\"value\">";
        // line 74
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "actionCount", [], "any", false, false, false, 74), "html", null, true);
        echo "</span>
            <span class=\"label\">Executed actions</span>
        </div>
        <div class=\"metric\">
            <span class=\"value\">";
        // line 78
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "processorCount", [], "any", false, false, false, 78), "html", null, true);
        echo "</span>
            <span class=\"label\">Executed processors</span>
        </div>
    </div>
";
    }

    // line 84
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 85
        echo "    <h2>Actions</h2>

    <table class=\"alt\" id=\"actionsPlaceholder\">
        <thead>
        <tr>
            <th onclick=\"javascript:sortTable(this, 0, 'actions')\" style=\"cursor: pointer;\">Time<span></span></th>
            <th onclick=\"javascript:sortTable(this, 1, 'actions')\" style=\"cursor: pointer;\">Used<span></span></th>
            <th>Name</th>
        </tr>
        </thead>
        <tbody id=\"actions\">
        ";
        // line 96
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "actions", [], "any", false, false, false, 96));
        foreach ($context['_seq'] as $context["i"] => $context["action"]) {
            // line 97
            echo "            <tr id=\"actionNo-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" class=\"";
            echo twig_escape_filter($this->env, twig_cycle([0 => "odd", 1 => "even"], $context["i"]), "html", null, true);
            echo "\">
                <td>";
            // line 98
            echo twig_escape_filter($this->env, twig_sprintf("%0.2f", (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "time", [], "any", false, false, false, 98) * 1000)), "html", null, true);
            echo "&nbsp;ms";
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "time", [], "any", false, false, false, 98) * 1000) > 5)) {
                echo " (";
                echo twig_escape_filter($this->env, twig_sprintf("%0.1f", ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "time", [], "any", false, false, false, 98) * 100) / Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "totalTime", [], "any", false, false, false, 98))), "html", null, true);
                echo "%)";
            }
            echo "</td>
                <td>";
            // line 99
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "count", [], "any", false, false, false, 99), "html", null, true);
            echo " times</td>
                <td>";
            // line 100
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "name", [], "any", false, false, false, 100), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['action'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "        </tbody>
    </table>
";
    }

    // line 107
    public function block_applicableCheckers($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 108
        echo "    <h2>Applicable Checkers</h2>

    <table class=\"alt\" id=\"applicableCheckersPlaceholder\">
        <thead>
        <tr>
            <th onclick=\"javascript:sortTable(this, 0, 'applicableCheckers')\" style=\"cursor: pointer;\">Time<span></span></th>
            <th onclick=\"javascript:sortTable(this, 1, 'applicableCheckers')\" style=\"cursor: pointer;\">Used<span></span></th>
            <th>Class</th>
        </tr>
        </thead>
        <tbody id=\"applicableCheckers\">
        ";
        // line 119
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "applicableCheckers", [], "any", false, false, false, 119));
        foreach ($context['_seq'] as $context["i"] => $context["applicableChecker"]) {
            // line 120
            echo "            <tr id=\"applicableCheckerNo-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" class=\"";
            echo twig_escape_filter($this->env, twig_cycle([0 => "odd", 1 => "even"], $context["i"]), "html", null, true);
            echo "\">
                <td>";
            // line 121
            echo twig_escape_filter($this->env, twig_sprintf("%0.2f", (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["applicableChecker"], "time", [], "any", false, false, false, 121) * 1000)), "html", null, true);
            echo "&nbsp;ms";
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["applicableChecker"], "time", [], "any", false, false, false, 121) * 1000) > 1)) {
                echo " (";
                echo twig_escape_filter($this->env, twig_sprintf("%0.1f", ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["applicableChecker"], "time", [], "any", false, false, false, 121) * 100) / Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "totalTime", [], "any", false, false, false, 121))), "html", null, true);
                echo "%)";
            }
            echo "</td>
                <td>";
            // line 122
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["applicableChecker"], "count", [], "any", false, false, false, 122), "html", null, true);
            echo " times</td>
                <td>";
            // line 123
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["applicableChecker"], "class", [], "any", false, false, false, 123), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['applicableChecker'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 126
        echo "        </tbody>
    </table>
";
    }

    // line 130
    public function block_processors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 131
        echo "    <h2>Processors</h2>

    <table class=\"alt\" id=\"processorsPlaceholder\">
        <thead>
        <tr>
            <th onclick=\"javascript:sortTable(this, 0, 'processors')\" style=\"cursor: pointer;\">Time<span></span></th>
            <th onclick=\"javascript:sortTable(this, 1, 'processors')\" style=\"cursor: pointer;\">Used<span></span></th>
            <th>Id</th>
        </tr>
        </thead>
        <tbody id=\"processors\">
        ";
        // line 142
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "processors", [], "any", false, false, false, 142));
        foreach ($context['_seq'] as $context["i"] => $context["processor"]) {
            // line 143
            echo "            <tr id=\"processorNo-";
            echo twig_escape_filter($this->env, $context["i"], "html", null, true);
            echo "\" class=\"";
            echo twig_escape_filter($this->env, twig_cycle([0 => "odd", 1 => "even"], $context["i"]), "html", null, true);
            echo "\">
                <td>";
            // line 144
            echo twig_escape_filter($this->env, twig_sprintf("%0.2f", (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["processor"], "time", [], "any", false, false, false, 144) * 1000)), "html", null, true);
            echo "&nbsp;ms";
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["processor"], "time", [], "any", false, false, false, 144) * 1000) > 1)) {
                echo " (";
                echo twig_escape_filter($this->env, twig_sprintf("%0.1f", ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["processor"], "time", [], "any", false, false, false, 144) * 100) / Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "totalTime", [], "any", false, false, false, 144))), "html", null, true);
                echo "%)";
            }
            echo "</td>
                <td>";
            // line 145
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["processor"], "count", [], "any", false, false, false, 145), "html", null, true);
            echo " times</td>
                <td>";
            // line 146
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["processor"], "id", [], "any", false, false, false, 146), "html", null, true);
            echo "</td>
            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['i'], $context['processor'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 149
        echo "        </tbody>
    </table>
";
    }

    public function getTemplateName()
    {
        return "@OroApi/Collector/api.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  342 => 149,  333 => 146,  329 => 145,  319 => 144,  312 => 143,  308 => 142,  295 => 131,  291 => 130,  285 => 126,  276 => 123,  272 => 122,  262 => 121,  255 => 120,  251 => 119,  238 => 108,  234 => 107,  228 => 103,  219 => 100,  215 => 99,  205 => 98,  198 => 97,  194 => 96,  181 => 85,  177 => 84,  168 => 78,  161 => 74,  154 => 70,  148 => 66,  144 => 65,  98 => 22,  94 => 21,  90 => 20,  85 => 19,  78 => 14,  75 => 13,  71 => 12,  62 => 7,  58 => 6,  52 => 3,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroApi/Collector/api.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ApiBundle/Resources/views/Collector/api.html.twig");
    }
}
