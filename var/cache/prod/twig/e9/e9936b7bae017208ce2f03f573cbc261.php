<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/fuzzy-search.js */
class __TwigTemplate_16a6fb2d6755aab3cb181e45f9674946 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require, exports, module) {
    'use strict';

    const Fuse = require('Fuse');
    const _ = require('underscore');

    const config = _.extend({
        checkScore: 0.49,
        engineOptions: {
            includeScore: true
        }
    }, require('module-config').default(module.id) || {});

    const FuzzySearch = {
        engineOptions: config.engineOptions,

        checkScore: config.checkScore,

        _cache: {},

        clearCache: function() {
            this._cache = {};
        },

        isMatched: function(str, query) {
            return this.getMatches(str, query).length > 0;
        },

        getMatches: function(str, query) {
            let cache = this._cache[str];
            if (!cache) {
                cache = this._cache[str] = {
                    searchEngine: this._newSearchEngine(str),
                    queries: {}
                };
            }

            if (cache.queries[query] === undefined) {
                const matches = cache.searchEngine.search(query);
                cache.queries[query] = this._filterMatches(matches);
            }

            return cache.queries[query];
        },

        _filterMatches: function(matches) {
            return this._filterMatchesByScore(matches);
        },

        _filterMatchesByScore: function(matches) {
            if (this.checkScore) {
                matches = matches.filter(function(match) {
                    return match.score <= this.checkScore;
                }.bind(this));
            }
            return matches;
        },

        _newSearchEngine: function(str) {
            return new Fuse([str], this.engineOptions);
        }
    };

    return FuzzySearch;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/fuzzy-search.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/fuzzy-search.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/fuzzy-search.js");
    }
}
