<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/WorkflowDefinition/view.html.twig */
class __TwigTemplate_b294ef0a28e6e5adfaf4cf1e03996425 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'stats' => [$this, 'block_stats'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["workflowMacros"] = $this->macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/WorkflowDefinition/view.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%workflow_definition.label%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "label", [], "any", false, false, false, 4)]]);
        // line 6
        $context["isActive"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isActive", [], "any", false, false, false, 6);
        // line 8
        $context["pageComponent"] = ["module" => "oroworkflow/js/app/components/workflow-viewer-component", "options" => ["entity" => ["configuration" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["entity"] ?? null), "configuration", [], "any", false, false, false, 12), "name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["entity"] ?? null), "name", [], "any", false, false, false, 13), "label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 14
($context["entity"] ?? null), "label", [], "any", false, false, false, 14), "entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["entity"] ?? null), "relatedEntity", [], "any", false, false, false, 15), "entity_attribute" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 16
($context["entity"] ?? null), "entityAttributeName", [], "any", true, true, false, 16)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "entityAttributeName", [], "any", false, false, false, 16), "entity")) : ("entity")), "startStep" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 17
($context["entity"] ?? null), "startStep", [], "any", false, true, false, 17), "name", [], "any", true, true, false, 17)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startStep", [], "any", false, true, false, 17), "name", [], "any", false, false, false, 17), null)) : (null)), "stepsDisplayOrdered" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 18
($context["entity"] ?? null), "stepsDisplayOrdered", [], "any", false, false, false, 18)], "chartOptions" => ["Endpoint" => [0 => "Blank", 1 => []]], "connectionOptions" => ["detachable" => false]]];
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroWorkflow/WorkflowDefinition/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 30
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/view.html.twig", 31)->unwrap();
        // line 32
        echo "
    ";
        // line 33
        $this->displayParentBlock("navButtons", $context, $blocks);
        echo "

    ";
        // line 35
        if (($context["isActive"] ?? null)) {
            // line 36
            echo "        ";
            $context["idButton"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 36) . "-workflow-deactivate-btn");
            // line 37
            echo "
        <span
            data-page-component-module=\"oroui/js/app/components/view-component\"
            data-page-component-options=\"";
            // line 40
            echo twig_escape_filter($this->env, json_encode(["view" => "oroworkflow/js/app/views/workflow-deactivate-btn-view", "selectors" => ["button" => ("#" .             // line 43
($context["idButton"] ?? null))]]), "html", null, true);
            // line 45
            echo "\">

            ";
            // line 47
            echo twig_call_macro($macros["UI"], "macro_button", [["aCss" => "no-hash btn-danger", "iCss" => "fa-close", "id" =>             // line 50
($context["idButton"] ?? null), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.datagrid.deactivate"), "path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_deactivate", ["workflowDefinition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 52
($context["entity"] ?? null), "name", [], "any", false, false, false, 52)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.datagrid.deactivate"), "data" => ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 55
($context["entity"] ?? null), "name", [], "any", false, false, false, 55), "label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 56
($context["entity"] ?? null), "label", [], "any", false, false, false, 56)]]], 47, $context, $this->getSourceContext());
            // line 58
            echo "
        </span>
    ";
        } else {
            // line 61
            echo "        ";
            $context["idButton"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 61) . "-workflow-activate-btn");
            // line 62
            echo "
        <span
            data-page-component-module=\"oroui/js/app/components/view-component\"
            data-page-component-options=\"";
            // line 65
            echo twig_escape_filter($this->env, json_encode(["view" => "oroworkflow/js/app/views/workflow-activate-btn-view", "selectors" => ["button" => ("#" .             // line 68
($context["idButton"] ?? null))]]), "html", null, true);
            // line 70
            echo "\">

            ";
            // line 72
            echo twig_call_macro($macros["UI"], "macro_button", [["aCss" => "no-hash btn-success", "iCss" => "fa-check", "id" =>             // line 75
($context["idButton"] ?? null), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.datagrid.activate"), "path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_activate", ["workflowDefinition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 77
($context["entity"] ?? null), "name", [], "any", false, false, false, 77)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.datagrid.activate"), "data" => ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 80
($context["entity"] ?? null), "name", [], "any", false, false, false, 80), "label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 81
($context["entity"] ?? null), "label", [], "any", false, false, false, 81)]]], 72, $context, $this->getSourceContext());
            // line 83
            echo "
        </span>
    ";
        }
        // line 86
        echo "
    ";
        // line 87
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "system", [], "any", false, false, false, 87)) {
            // line 88
            echo "        ";
            if ((($context["edit_allowed"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)))) {
                // line 89
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_update", ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 90
($context["entity"] ?? null), "name", [], "any", false, false, false, 90)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.entity_label")]], 89, $context, $this->getSourceContext());
                // line 92
                echo "
        ";
            }
            // line 94
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
                // line 95
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_definition_delete", ["workflowDefinition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 96
($context["entity"] ?? null), "name", [], "any", false, false, false, 96)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-workflow-definition", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 100
($context["entity"] ?? null), "name", [], "any", false, false, false, 100), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.entity_label")]], 95, $context, $this->getSourceContext());
                // line 102
                echo "
        ";
            }
            // line 104
            echo "    ";
        }
    }

    // line 107
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 108
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 108)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 108)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 109)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 109)), "html", null, true))) : (print ("N/A")));
        echo "</li>
";
    }

    // line 112
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 113
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 114
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 117
($context["entity"] ?? null), "label", [], "any", false, false, false, 117)];
        // line 119
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 122
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 123
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/view.html.twig", 123)->unwrap();
        // line 124
        echo "
    ";
        // line 125
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "

    <span class=\"page-title__status\">
        ";
        // line 128
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "system", [], "any", false, false, false, 128)) {
            // line 129
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.status.system.label"), "tentatively"], 129, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 131
        echo "        ";
        if ( !($context["edit_allowed"] ?? null)) {
            // line 132
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.status.readonly.label"), "tentatively"], 132, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 134
        echo "        ";
        if (($context["isActive"] ?? null)) {
            // line 135
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Active"), "enabled"], 135, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 137
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Inactive"), "disabled"], 137, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 139
        echo "    </span>
";
    }

    // line 201
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 202
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/view.html.twig", 202)->unwrap();
        // line 203
        echo "    ";
        $macros["workflowDefinitionView"] = $this;
        // line 204
        echo "
    ";
        // line 205
        ob_start(function () { return ''; });
        // line 206
        echo "    <div class=\"widget-content\">
        <div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 209
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.label.label"), (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "label", [], "any", false, false, false, 209)) . twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["translateLinks"] ?? null), "label", [], "any", false, false, false, 209)], 209, $context, $this->getSourceContext()))], 209, $context, $this->getSourceContext());
        echo "

                ";
        // line 211
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.related_entity.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 213
($context["entity"] ?? null), "relatedEntity", [], "any", false, false, false, 213), "label"))], 211, $context, $this->getSourceContext());
        // line 214
        echo "
                ";
        // line 215
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.view.workflow.default_step"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 217
($context["entity"] ?? null), "startStep", [], "any", false, false, false, 217)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startStep", [], "any", false, false, false, 217), "label", [], "any", false, false, false, 217), [], "workflows")) : (""))], 215, $context, $this->getSourceContext());
        // line 218
        echo "
                ";
        // line 219
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.steps_display_ordered.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 221
($context["entity"] ?? null), "stepsDisplayOrdered", [], "any", false, false, false, 221)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No")))], 219, $context, $this->getSourceContext());
        // line 222
        echo "
                ";
        // line 223
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.priority.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 225
($context["entity"] ?? null), "priority", [], "any", false, false, false, 225)], 223, $context, $this->getSourceContext());
        // line 226
        echo "

                ";
        // line 228
        ob_start(function () { return ''; });
        // line 229
        echo "                    ";
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "applications", [], "any", false, false, false, 229))) {
            // line 230
            echo "                        ";
            echo twig_call_macro($macros["UI"], "macro_renderList", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "applications", [], "any", false, false, false, 230)], 230, $context, $this->getSourceContext());
            echo "
                    ";
        } else {
            // line 232
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "
                    ";
        }
        // line 234
        echo "                ";
        $context["applications"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 235
        echo "
                ";
        // line 236
        ob_start(function () { return ''; });
        // line 237
        echo "                    ";
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveActiveGroups", [], "any", false, false, false, 237))) {
            // line 238
            echo "                        ";
            echo twig_call_macro($macros["UI"], "macro_renderList", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveActiveGroups", [], "any", false, false, false, 238)], 238, $context, $this->getSourceContext());
            echo "
                    ";
        } else {
            // line 240
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "
                    ";
        }
        // line 242
        echo "                ";
        $context["activeGroups"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 243
        echo "
                ";
        // line 244
        ob_start(function () { return ''; });
        // line 245
        echo "                    ";
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveRecordGroups", [], "any", false, false, false, 245))) {
            // line 246
            echo "                        ";
            echo twig_call_macro($macros["UI"], "macro_renderList", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveRecordGroups", [], "any", false, false, false, 246)], 246, $context, $this->getSourceContext());
            echo "
                    ";
        } else {
            // line 248
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "
                    ";
        }
        // line 250
        echo "                ";
        $context["recordGroups"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 251
        echo "
                ";
        // line 252
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.applications.label"), ($context["applications"] ?? null)], 252, $context, $this->getSourceContext());
        echo "
                ";
        // line 253
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.exclusive_active_groups.label"), ($context["activeGroups"] ?? null)], 253, $context, $this->getSourceContext());
        echo "
                ";
        // line 254
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.exclusive_record_groups.label"), ($context["recordGroups"] ?? null)], 254, $context, $this->getSourceContext());
        echo "
            </div>
        </div>
    </div>
    ";
        $context["workflowDefinitionInfo"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 259
        echo "
    ";
        // line 260
        ob_start(function () { return ''; });
        // line 261
        echo "        <div class=\"row-fluid clearfix\">
            <div class=\"workflow-table-container\">
                <div class=\"workflow-definition-steps-list-container clearfix\">
                    <div class=\"grid-container steps-list\">
                        <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\" style=\"margin-bottom: 10px\">
                            <thead>
                                <tr>
                                    <th class=\"label-column\"><span>";
        // line 268
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Step"), "html", null, true);
        echo "</span></th>
                                    <th><span>";
        // line 269
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Transitions"), "html", null, true);
        echo "</span></th>
                                    <th>
                                        <span title=\"";
        // line 271
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.step.order.tooltip"), "html", null, true);
        echo "\">
                                            ";
        // line 272
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Position"), "html", null, true);
        echo "
                                        </span>
                                    </th>
                                </tr>
                            </thead>
                            <tbody class=\"item-container\">";
        // line 278
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "configuration", [], "any", false, false, false, 278), "steps", [], "any", false, false, false, 278));
        $context['_iterated'] = false;
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["stepName"] => $context["stepData"]) {
            // line 283
            echo "                                ";
            $context["stepData"] = twig_array_merge($context["stepData"], ["name" => $context["stepName"]]);
            // line 284
            echo "                                ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 284) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isSystem", [], "any", false, false, false, 284))) {
                // line 285
                echo "                                    ";
                echo twig_call_macro($macros["workflowDefinitionView"], "macro_view_start_step_row", [($context["entity"] ?? null)], 285, $context, $this->getSourceContext());
                echo "
                                ";
            }
            // line 287
            echo "
                                ";
            // line 288
            echo twig_call_macro($macros["workflowDefinitionView"], "macro_view_step_row", [$context["stepData"], ($context["entity"] ?? null), ($context["translateLinks"] ?? null)], 288, $context, $this->getSourceContext());
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 290
            echo twig_call_macro($macros["workflowDefinitionView"], "macro_view_start_step_row", [($context["entity"] ?? null)], 290, $context, $this->getSourceContext());
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['stepName'], $context['stepData'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 292
        echo "</tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    ";
        $context["workflowStepsAndTransitions"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 299
        echo "
    ";
        // line 300
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.title.general_information"), "subblocks" => [0 => ["data" => [0 =>         // line 304
($context["workflowDefinitionInfo"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.title.steps_and_transitions"), "subblocks" => [0 => ["data" => [0 =>         // line 311
($context["workflowStepsAndTransitions"] ?? null)]]]]];
        // line 316
        echo "
    ";
        // line 317
        if ( !$this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 318
            echo "        ";
            ob_start(function () { return ''; });
            // line 319
            echo "            <div class=\"workflow-step-viewer\" ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroworkflow/js/app/views/flowchart/flowchart-container-view", "name" => "flowchart-container"]], 319, $context, $this->getSourceContext());
            // line 322
            echo "></div>
        ";
            $context["workflowDiagram"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 324
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.title.workflow_diagrams"), "subblocks" => [0 => ["data" => [0 =>             // line 329
($context["workflowDiagram"] ?? null)]]]]]);
            // line 334
            echo "    ";
        }
        // line 335
        echo "
    ";
        // line 336
        if ((array_key_exists("variables", $context) && twig_length_filter($this->env, ($context["variables"] ?? null)))) {
            // line 337
            echo "        ";
            ob_start(function () { return ''; });
            // line 338
            echo "            <div class=\"widget-content\">
                <div class=\"row-fluid form-horizontal\">
                    <div class=\"responsive-block\">
                        ";
            // line 341
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["variables"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["variable"]) {
                // line 342
                echo "                            <div class=\"control-group\">
                                <label class=\"control-label\">
                                    ";
                // line 344
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "options", [], "any", false, true, false, 344), "form_options", [], "any", true, true, false, 344) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "options", [], "any", false, true, false, 344), "form_options", [], "any", false, true, false, 344), "tooltip", [], "any", true, true, false, 344))) {
                    // line 345
                    echo "                                        ";
                    echo twig_call_macro($macros["UI"], "macro_tooltip", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "options", [], "any", false, false, false, 345), "form_options", [], "any", false, false, false, 345), "tooltip", [], "any", false, false, false, 345)], 345, $context, $this->getSourceContext());
                    echo "
                                    ";
                }
                // line 347
                echo "                                    ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "label", [], "any", false, false, false, 347), "html", null, true);
                echo "
                                </label>
                                <div class=\"controls\">
                                    <div class=\"control-label\">
                                        ";
                // line 351
                echo twig_escape_filter($this->env, _twig_default_filter($this->extensions['Oro\Bundle\WorkflowBundle\Twig\WorkflowExtension']->formatWorkflowVariableValue($context["variable"]), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")), "html", null, true);
                echo "
                                        ";
                // line 352
                echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", [(($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["translateLinks"] ?? null), "variable_definitions", [], "any", false, false, false, 352), "variables", [], "any", false, false, false, 352)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "name", [], "any", false, false, false, 352)] ?? null) : null)], 352, $context, $this->getSourceContext());
                echo "
                                    </div>
                                </div>
                            </div>
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['variable'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 357
            echo "                    </div>
                </div>
            </div>
        ";
            $context["variablesInfo"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 361
            echo "
        ";
            // line 362
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.configuration.label"), "subblocks" => [0 => ["data" => [0 =>             // line 365
($context["variablesInfo"] ?? null)]]]]]);
            // line 368
            echo "
    ";
        }
        // line 370
        echo "
    ";
        // line 371
        $context["id"] = "workflowDefinitionView";
        // line 372
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 373
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    // line 142
    public function macro_view_step_row($__stepData__ = null, $__entity__ = null, $__translateLinks__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "stepData" => $__stepData__,
            "entity" => $__entity__,
            "translateLinks" => $__translateLinks__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 143
            echo "    ";
            $macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/WorkflowDefinition/view.html.twig", 143)->unwrap();
            // line 144
            echo "    <tr>
        <td class=\"step-name workflow-translatable-label\">
            ";
            // line 146
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stepData"] ?? null), "label", [], "any", false, false, false, 146), "html", null, true);
            echo "
            ";
            // line 147
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stepData"] ?? null), "is_final", [], "any", false, false, false, 147)) {
                // line 148
                echo "                <strong title=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.step.is_final.tooltip"), "html", null, true);
                echo "\">
                    (";
                // line 149
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Final"), "html", null, true);
                echo ")
                </strong>
            ";
            }
            // line 152
            echo "            ";
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stepData"] ?? null), "order", [], "any", false, false, false, 152) >= 0) && ($context["translateLinks"] ?? null))) {
                // line 153
                echo "                ";
                echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_1 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["translateLinks"] ?? null), "steps", [], "any", false, false, false, 153)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stepData"] ?? null), "name", [], "any", false, false, false, 153)] ?? null) : null), "label", [], "any", false, false, false, 153)], 153, $context, $this->getSourceContext());
                echo "
            ";
            }
            // line 155
            echo "        </td>
        <td class=\"step-transitions\">
            ";
            // line 157
            if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stepData"] ?? null), "allowed_transitions", [], "any", false, false, false, 157))) {
                // line 158
                echo "                <ul class=\"transitions-list-short\">
                    ";
                // line 159
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stepData"] ?? null), "allowed_transitions", [], "any", false, false, false, 159));
                foreach ($context['_seq'] as $context["_key"] => $context["transitionName"]) {
                    // line 160
                    echo "                        ";
                    $context["currentTransition"] = (($__internal_compile_2 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "configuration", [], "any", false, false, false, 160), "transitions", [], "any", false, false, false, 160)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2[$context["transitionName"]] ?? null) : null);
                    // line 161
                    echo "                        ";
                    $context["toStep"] = (($__internal_compile_3 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "configuration", [], "any", false, false, false, 161), "steps", [], "any", false, false, false, 161)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["currentTransition"] ?? null), "step_to", [], "any", false, false, false, 161)] ?? null) : null);
                    // line 162
                    echo "                        <li>
                            <span>";
                    // line 163
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["currentTransition"] ?? null), "label", [], "any", false, false, false, 163), [], "workflows"), "html", null, true);
                    echo "</span>
                            ";
                    // line 164
                    if (($context["translateLinks"] ?? null)) {
                        // line 165
                        echo "                                ";
                        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_4 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["translateLinks"] ?? null), "transitions", [], "any", false, false, false, 165)) && is_array($__internal_compile_4) || $__internal_compile_4 instanceof ArrayAccess ? ($__internal_compile_4[$context["transitionName"]] ?? null) : null), "label", [], "any", false, false, false, 165)], 165, $context, $this->getSourceContext());
                        echo "
                            ";
                    }
                    // line 167
                    echo "                            <i class=\"fa-long-arrow-right\"></i>
                            <span title=\"";
                    // line 168
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.transition.step_to.tooltip"), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["toStep"] ?? null), "label", [], "any", false, false, false, 168), [], "workflows"), "html", null, true);
                    echo "</span>
                        </li>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['transitionName'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 171
                echo "                </ul>
            ";
            }
            // line 173
            echo "        </td>
        <td>
            <span title=\"";
            // line 175
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.step.order.tooltip"), "html", null, true);
            echo "\">
                ";
            // line 176
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["stepData"] ?? null), "order", [], "any", false, false, false, 176), "html", null, true);
            echo "
            </span>
        </td>
    </tr>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 182
    public function macro_view_start_step_row($__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 183
            echo "    ";
            $macros["workflowDefinitionView"] = $this;
            // line 184
            echo "
    ";
            // line 185
            $context["startTransitionNames"] = [];
            // line 186
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "configuration", [], "any", false, false, false, 186), "transitions", [], "any", false, false, false, 186));
            foreach ($context['_seq'] as $context["transitionName"] => $context["transitionConfig"]) {
                // line 187
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["transitionConfig"], "is_start", [], "any", false, false, false, 187)) {
                    // line 188
                    echo "            ";
                    $context["startTransitionNames"] = twig_array_merge(($context["startTransitionNames"] ?? null), [0 => $context["transitionName"]]);
                    // line 189
                    echo "        ";
                }
                // line 190
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['transitionName'], $context['transitionConfig'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 191
            echo twig_call_macro($macros["workflowDefinitionView"], "macro_view_step_row", [["label" => (("(" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Start")) . ")"), "order" =>  -1, "is_final" => false, "allowed_transitions" =>             // line 196
($context["startTransitionNames"] ?? null)],             // line 197
($context["entity"] ?? null)], 192, $context, $this->getSourceContext());

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/WorkflowDefinition/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  765 => 197,  764 => 196,  763 => 191,  757 => 190,  754 => 189,  751 => 188,  748 => 187,  743 => 186,  741 => 185,  738 => 184,  735 => 183,  722 => 182,  708 => 176,  704 => 175,  700 => 173,  696 => 171,  685 => 168,  682 => 167,  676 => 165,  674 => 164,  670 => 163,  667 => 162,  664 => 161,  661 => 160,  657 => 159,  654 => 158,  652 => 157,  648 => 155,  642 => 153,  639 => 152,  633 => 149,  628 => 148,  626 => 147,  622 => 146,  618 => 144,  615 => 143,  600 => 142,  593 => 373,  590 => 372,  588 => 371,  585 => 370,  581 => 368,  579 => 365,  578 => 362,  575 => 361,  569 => 357,  558 => 352,  554 => 351,  546 => 347,  540 => 345,  538 => 344,  534 => 342,  530 => 341,  525 => 338,  522 => 337,  520 => 336,  517 => 335,  514 => 334,  512 => 329,  510 => 324,  506 => 322,  503 => 319,  500 => 318,  498 => 317,  495 => 316,  493 => 311,  492 => 304,  491 => 300,  488 => 299,  479 => 292,  473 => 290,  460 => 288,  457 => 287,  451 => 285,  448 => 284,  445 => 283,  427 => 278,  419 => 272,  415 => 271,  410 => 269,  406 => 268,  397 => 261,  395 => 260,  392 => 259,  384 => 254,  380 => 253,  376 => 252,  373 => 251,  370 => 250,  364 => 248,  358 => 246,  355 => 245,  353 => 244,  350 => 243,  347 => 242,  341 => 240,  335 => 238,  332 => 237,  330 => 236,  327 => 235,  324 => 234,  318 => 232,  312 => 230,  309 => 229,  307 => 228,  303 => 226,  301 => 225,  300 => 223,  297 => 222,  295 => 221,  294 => 219,  291 => 218,  289 => 217,  288 => 215,  285 => 214,  283 => 213,  282 => 211,  277 => 209,  272 => 206,  270 => 205,  267 => 204,  264 => 203,  261 => 202,  257 => 201,  252 => 139,  246 => 137,  240 => 135,  237 => 134,  231 => 132,  228 => 131,  222 => 129,  220 => 128,  214 => 125,  211 => 124,  208 => 123,  204 => 122,  197 => 119,  195 => 117,  194 => 114,  192 => 113,  188 => 112,  180 => 109,  173 => 108,  169 => 107,  164 => 104,  160 => 102,  158 => 100,  157 => 96,  155 => 95,  152 => 94,  148 => 92,  146 => 90,  144 => 89,  141 => 88,  139 => 87,  136 => 86,  131 => 83,  129 => 81,  128 => 80,  127 => 77,  126 => 75,  125 => 72,  121 => 70,  119 => 68,  118 => 65,  113 => 62,  110 => 61,  105 => 58,  103 => 56,  102 => 55,  101 => 52,  100 => 50,  99 => 47,  95 => 45,  93 => 43,  92 => 40,  87 => 37,  84 => 36,  82 => 35,  77 => 33,  74 => 32,  71 => 31,  67 => 30,  62 => 1,  60 => 18,  59 => 17,  58 => 16,  57 => 15,  56 => 14,  55 => 13,  54 => 12,  53 => 8,  51 => 6,  49 => 4,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/WorkflowDefinition/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/WorkflowDefinition/view.html.twig");
    }
}
