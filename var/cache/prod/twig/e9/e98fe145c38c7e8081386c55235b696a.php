<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/Grid/widget/widget.html.twig */
class __TwigTemplate_1145f81ba3b38d8b318a8cb6d41774c1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'widget_content' => [$this, 'block_widget_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroDataGrid/Grid/widget/widget.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    ";
        // line 4
        $this->displayBlock('widget_content', $context, $blocks);
        // line 14
        echo "</div>
";
    }

    // line 4
    public function block_widget_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    <div class=\"flash-messages\">
        <div class=\"flash-messages-frame\">
            <div class=\"flash-messages-holder\"></div>
        </div>
    </div>
    <div class=\"filters-view-container\"></div>

    ";
        // line 12
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ($context["params"] ?? null), twig_array_merge(["enableViews" => false, "enableToggleFilters" => false], ($context["renderParams"] ?? null))], 12, $context, $this->getSourceContext());
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/Grid/widget/widget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 12,  55 => 5,  51 => 4,  46 => 14,  44 => 4,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/Grid/widget/widget.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/Grid/widget/widget.html.twig");
    }
}
