<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/actions.html.twig */
class __TwigTemplate_cbdefba9ab89a43472e8a89ca167fc90 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 11
    public function macro_logCallLink($__phone__ = null, $__entity__ = null, $__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "phone" => $__phone__,
            "entity" => $__entity__,
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 12
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCall/actions.html.twig", 12)->unwrap();
            // line 13
            echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_call_create", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(            // line 16
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["entity"] ?? null), "id", [], "any", false, false, false, 17), "phone" => ((            // line 18
array_key_exists("phone", $context)) ? (($context["phone"] ?? null)) : (null))]), "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 20)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 20) . " no-hash")) : ("no-hash")), "label" =>             // line 21
($context["phone"] ?? null), "widget" => ["type" => "dialog", "multiple" => true, "reload-grid-name" => "activity-call-grid", "options" => ["alias" => "call-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.log_call"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 13, $context, $this->getSourceContext());

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroCall/actions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 21,  60 => 20,  59 => 18,  58 => 17,  57 => 16,  56 => 13,  54 => 12,  39 => 11,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/actions.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/actions.html.twig");
    }
}
