<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/Call/widget/additionalProperties.html.twig */
class __TwigTemplate_1475f606ff81ba978e59de8e680e6db6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroHangoutsCall/Call/widget/additionalProperties.html.twig", 1)->unwrap();
        // line 2
        ob_start(function () { return ''; });
        // line 3
        echo "    ";
        $context["hangoutOptions"] = ["widget_size" => 70];
        // line 6
        echo "    ";
        $this->loadTemplate("@OroHangoutsCall/Call/updateActions.html.twig", "@OroHangoutsCall/Call/widget/additionalProperties.html.twig", 6)->display($context);
        $context["hangoutButton"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 8
        echo twig_call_macro($macros["UI"], "macro_renderControlGroup", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.hangoutscall.label"), ($context["hangoutButton"] ?? null)], 8, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/Call/widget/additionalProperties.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 8,  44 => 6,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/Call/widget/additionalProperties.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/Call/widget/additionalProperties.html.twig");
    }
}
