<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Calendar/Menu/toggleCalendar.html.twig */
class __TwigTemplate_672f5ef993f715773a80d39ca1e3001e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["oro_menu"] = $this->macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroCalendar/Calendar/Menu/toggleCalendar.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        ob_start(function () { return ''; });
        // line 4
        echo "<li";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["itemAttributes"] ?? null)], 4, $context, $this->getSourceContext());
        echo ">";
        // line 5
        $context["linkAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "linkAttributes", [], "any", false, false, false, 5);
        // line 6
        echo "    <a href=\"#\" role=\"button\" ";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 6, $context, $this->getSourceContext());
        echo ">
    <% if (visible) { %>
        <span class=\"fa-eye-slash\" aria-hidden=\"true\"></span>";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.context.hide"), "html", null, true);
        echo "
    <% } else { %>
        <span class=\"fa-eye\" aria-hidden=\"true\"></span>";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.context.show"), "html", null, true);
        echo "
    <% } %>
    </a>
</li>
";
        $___internal_parse_87_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 3
        echo twig_spaceless($___internal_parse_87_);
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Calendar/Menu/toggleCalendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 3,  61 => 10,  56 => 8,  50 => 6,  48 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Calendar/Menu/toggleCalendar.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Calendar/Menu/toggleCalendar.html.twig");
    }
}
