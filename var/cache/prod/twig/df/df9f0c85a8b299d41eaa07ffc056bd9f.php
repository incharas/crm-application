<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroZendesk/Case/caseTicketInfo.html.twig */
class __TwigTemplate_4280deb213c6302cdbf5ff7ba7f0ad55 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_case_view")) {
            // line 2
            echo "    ";
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroZendesk/Case/caseTicketInfo.html.twig", 2)->unwrap();
            // line 3
            echo "    ";
            $macros["email"] = $this->macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroZendesk/Case/caseTicketInfo.html.twig", 3)->unwrap();
            // line 4
            echo "    ";
            $macros["caseTicketInfo"] = $this->macros["caseTicketInfo"] = $this;
            // line 5
            echo "
    ";
            // line 31
            echo "
    ";
            // line 32
            $context["ticket"] = $this->extensions['Oro\Bundle\ZendeskBundle\Twig\ZendeskExtension']->getTicketByCase(($context["entity"] ?? null));
            // line 33
            echo "    ";
            if (($context["ticket"] ?? null)) {
                // line 34
                echo "        <div class=\"responsive-cell\">
            <div class=\"box-type1\">
                <div class=\"title\">
                    <span class=\"widget-title\">";
                // line 37
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket_info_title"), "html", null, true);
                echo "</span>
                </div>
                <div class=\"row-fluid form-horizontal\">
                    <div class=\"responsive-block\">

                        ";
                // line 42
                ob_start(function () { return ''; });
                // line 43
                echo "                            ";
                $context["url"] = $this->extensions['Oro\Bundle\ZendeskBundle\Twig\ZendeskExtension']->getTicketUrl(($context["ticket"] ?? null));
                // line 44
                echo "                            ";
                if (($context["url"] ?? null)) {
                    // line 45
                    echo "                                <a href=\"";
                    echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
                    echo "\" target=\"_blank\" class=\"no-hash\">";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "originId", [], "any", false, false, false, 45), "html", null, true);
                    echo "</a>
                            ";
                } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 46
($context["ticket"] ?? null), "originId", [], "any", false, false, false, 46)) {
                    // line 47
                    echo "                                ";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "originId", [], "any", false, false, false, 47), "html", null, true);
                    echo "
                            ";
                } else {
                    // line 49
                    echo "                                ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"), "html", null, true);
                    echo "
                            ";
                }
                // line 51
                echo "                        ";
                $context["link"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 52
                echo "                        ";
                echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.origin_id.label"), ($context["link"] ?? null)], 52, $context, $this->getSourceContext());
                echo "

                        ";
                // line 54
                ob_start(function () { return ''; });
                // line 55
                echo "                            ";
                echo twig_call_macro($macros["email"], "macro_email_address_simple", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "recipient", [], "any", false, false, false, 55)], 55, $context, $this->getSourceContext());
                echo "
                        ";
                $context["recipient"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 57
                echo "                        ";
                echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.recipient.label"), twig_trim_filter(($context["recipient"] ?? null))], 57, $context, $this->getSourceContext());
                echo "
                        ";
                // line 58
                echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.status.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "status", [], "any", false, true, false, 58), "label", [], "any", true, true, false, 58)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "status", [], "any", false, true, false, 58), "label", [], "any", false, false, false, 58), false)) : (false))], 58, $context, $this->getSourceContext());
                echo "
                        ";
                // line 59
                echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.type.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "type", [], "any", false, true, false, 59), "label", [], "any", true, true, false, 59)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "type", [], "any", false, true, false, 59), "label", [], "any", false, false, false, 59), false)) : (false))], 59, $context, $this->getSourceContext());
                echo "
                        ";
                // line 60
                echo twig_call_macro($macros["caseTicketInfo"], "macro_render_zendesk_user", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.submitter.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "submitter", [], "any", false, false, false, 60)], 60, $context, $this->getSourceContext());
                echo "
                        ";
                // line 61
                echo twig_call_macro($macros["caseTicketInfo"], "macro_render_zendesk_user", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.assignee.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "assignee", [], "any", false, false, false, 61)], 61, $context, $this->getSourceContext());
                echo "
                        ";
                // line 62
                echo twig_call_macro($macros["caseTicketInfo"], "macro_render_zendesk_user", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.requester.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "requester", [], "any", false, false, false, 62)], 62, $context, $this->getSourceContext());
                echo "
                        ";
                // line 63
                echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.priority.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "priority", [], "any", false, true, false, 63), "label", [], "any", true, true, false, 63)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "priority", [], "any", false, true, false, 63), "label", [], "any", false, false, false, 63), false)) : (false))], 63, $context, $this->getSourceContext());
                echo "
                        ";
                // line 64
                ob_start(function () { return ''; });
                // line 65
                echo "                            ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "problem", [], "any", false, false, false, 65) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "problem", [], "any", false, false, false, 65), "relatedCase", [], "any", false, false, false, 65))) {
                    // line 66
                    echo "                                <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "problem", [], "any", false, false, false, 66), "relatedCase", [], "any", false, false, false, 66), "id", [], "any", false, false, false, 66)]), "html", null, true);
                    echo "\">
                                    ";
                    // line 67
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "problem", [], "any", false, false, false, 67), "subject", [], "any", false, false, false, 67), "html", null, true);
                    echo "
                                </a>
                            ";
                }
                // line 70
                echo "                        ";
                $context["problem"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 71
                echo "                        ";
                echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.problem.label"), twig_trim_filter($this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(($context["problem"] ?? null)))], 71, $context, $this->getSourceContext());
                echo "

                        ";
                // line 73
                ob_start(function () { return ''; });
                // line 74
                echo "                            ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "collaborators", [], "any", false, false, false, 74)) {
                    // line 75
                    echo "                                ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ticket"] ?? null), "collaborators", [], "any", false, false, false, 75));
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["collaborator"]) {
                        // line 76
                        echo "                                    ";
                        echo twig_call_macro($macros["caseTicketInfo"], "macro_render_zendesk_user", [false, $context["collaborator"]], 76, $context, $this->getSourceContext());
                        echo "
                                    ";
                        // line 77
                        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "length", [], "any", false, false, false, 77) > 1) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 77))) {
                            // line 78
                            echo "                                        ,&nbsp;
                                    ";
                        }
                        // line 80
                        echo "                                ";
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['collaborator'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 81
                    echo "                            ";
                }
                // line 82
                echo "                        ";
                $context["collaborators"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 83
                echo "                        ";
                echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.ticket.collaborators.label"), twig_trim_filter(($context["collaborators"] ?? null))], 83, $context, $this->getSourceContext());
                echo "
                    </div>
                </div>
            </div>
        </div>
    ";
            }
        }
    }

    // line 6
    public function macro_render_zendesk_user($__label__ = null, $__user__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "label" => $__label__,
            "user" => $__user__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 7
            echo "        ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroZendesk/Case/caseTicketInfo.html.twig", 7)->unwrap();
            // line 8
            echo "        ";
            $macros["caseTicketInfo"] = $this;
            // line 9
            ob_start(function () { return ''; });
            // line 10
            if (($context["user"] ?? null)) {
                // line 11
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "relatedUser", [], "any", false, false, false, 11) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "relatedUser", [], "any", false, false, false, 11)))) {
                    // line 12
                    echo "                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "relatedUser", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12)]), "html", null, true);
                    echo "\">
                        ";
                    // line 13
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 13));
                    echo "
                    </a>
                ";
                } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 15
($context["user"] ?? null), "relatedContact", [], "any", false, false, false, 15) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "relatedContact", [], "any", false, false, false, 15)))) {
                    // line 16
                    echo "                    <a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contact_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "relatedContact", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16)]), "html", null, true);
                    echo "\">
                        ";
                    // line 17
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 17));
                    echo "
                    </a>
                ";
                } else {
                    // line 20
                    echo "                    ";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "name", [], "any", false, false, false, 20));
                    echo "
                ";
                }
            }
            $context["userHtml"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 25
            if (($context["label"] ?? null)) {
                // line 26
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [($context["label"] ?? null), ($context["userHtml"] ?? null)], 26, $context, $this->getSourceContext());
                echo "
        ";
            } else {
                // line 28
                echo "            ";
                echo twig_escape_filter($this->env, ($context["userHtml"] ?? null), "html", null, true);
                echo "
        ";
            }
            // line 30
            echo "    ";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroZendesk/Case/caseTicketInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  310 => 30,  304 => 28,  298 => 26,  296 => 25,  288 => 20,  282 => 17,  277 => 16,  275 => 15,  270 => 13,  265 => 12,  263 => 11,  261 => 10,  259 => 9,  256 => 8,  253 => 7,  239 => 6,  226 => 83,  223 => 82,  220 => 81,  206 => 80,  202 => 78,  200 => 77,  195 => 76,  177 => 75,  174 => 74,  172 => 73,  166 => 71,  163 => 70,  157 => 67,  152 => 66,  149 => 65,  147 => 64,  143 => 63,  139 => 62,  135 => 61,  131 => 60,  127 => 59,  123 => 58,  118 => 57,  112 => 55,  110 => 54,  104 => 52,  101 => 51,  95 => 49,  89 => 47,  87 => 46,  80 => 45,  77 => 44,  74 => 43,  72 => 42,  64 => 37,  59 => 34,  56 => 33,  54 => 32,  51 => 31,  48 => 5,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroZendesk/Case/caseTicketInfo.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-zendesk/Resources/views/Case/caseTicketInfo.html.twig");
    }
}
