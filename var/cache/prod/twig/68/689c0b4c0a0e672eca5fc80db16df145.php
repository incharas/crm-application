<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/AttributeFamily/view.html.twig */
class __TwigTemplate_c21cee7a212b603fb1d2de0b094a76d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroEntityConfig/AttributeFamily/view.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "defaultLabel", [], "any", false, false, false, 4), "string", [], "any", false, false, false, 4)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroEntityConfig/AttributeFamily/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/AttributeFamily/view.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_attribute_family_update")) {
            // line 10
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_family_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["entity"] ?? null), "id", [], "any", false, false, false, 11)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute_family.entity_label")]], 10, $context, $this->getSourceContext());
            // line 13
            echo "
    ";
        }
        // line 15
        echo "    ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_attribute_family_delete") && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("delete", ($context["entity"] ?? null)))) {
            // line 16
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_family_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["entity"] ?? null), "id", [], "any", false, false, false, 17)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_family_index", ["alias" =>             // line 18
($context["entityAlias"] ?? null)]), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["entity"] ?? null), "id", [], "any", false, false, false, 20), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute_family.entity_label")]], 16, $context, $this->getSourceContext());
            // line 22
            echo "
    ";
        }
        // line 24
        echo "    ";
        $this->displayParentBlock("navButtons", $context, $blocks);
        echo "
";
    }

    // line 27
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 29
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_family_index", ["alias" =>         // line 30
($context["entityAlias"] ?? null)]), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute_family.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 32
($context["entity"] ?? null), "defaultLabel", [], "method", false, false, false, 32)];
        // line 34
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 37
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "
    ";
        // line 39
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.sections.attribute_family.attributes"), "class" => "active", "subblocks" => [0 => ["data" => [0 => twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["attribute-groups-grid", ["family_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 46
($context["entity"] ?? null), "id", [], "any", false, false, false, 46)]], 46, $context, $this->getSourceContext())]]]]];
        // line 52
        echo "
    ";
        // line 53
        $context["id"] = "attributefamily-view";
        // line 54
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 55
        echo "
    ";
        // line 56
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/AttributeFamily/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 56,  129 => 55,  126 => 54,  124 => 53,  121 => 52,  119 => 46,  118 => 39,  115 => 38,  111 => 37,  104 => 34,  102 => 32,  101 => 30,  100 => 29,  98 => 28,  94 => 27,  87 => 24,  83 => 22,  81 => 20,  80 => 18,  79 => 17,  77 => 16,  74 => 15,  70 => 13,  68 => 11,  66 => 10,  64 => 9,  61 => 8,  58 => 7,  54 => 6,  49 => 1,  47 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/AttributeFamily/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/AttributeFamily/view.html.twig");
    }
}
