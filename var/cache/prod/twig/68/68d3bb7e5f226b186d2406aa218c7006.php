<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChart/Chart/bar.html.twig */
class __TwigTemplate_d07ef1a4a95a507fc15ee50b037a1caa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["chart"] = $this->macros["chart"] = $this->loadTemplate("@OroChart/macros.html.twig", "@OroChart/Chart/bar.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 22
        echo "
";
        // line 23
        if ((twig_length_filter($this->env, ($context["data"] ?? null)) > 0)) {
            // line 24
            echo "    ";
            echo twig_call_macro($macros["chart"], "macro_renderChart", [($context["data"] ?? null), ($context["options"] ?? null), ($context["config"] ?? null)], 24, $context, $this->getSourceContext());
            echo "
";
        } else {
            // line 26
            echo "    <div class=\"no-data\">
        ";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.no_data_found"), "html", null, true);
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroChart/Chart/bar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 27,  53 => 26,  47 => 24,  45 => 23,  42 => 22,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChart/Chart/bar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ChartBundle/Resources/views/Chart/bar.html.twig");
    }
}
