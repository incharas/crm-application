<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroIntegration/Autocomplete/integration/selection.html.twig */
class __TwigTemplate_b63b751705c05cbf7d47f24e6e4f720d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<%
    var data = \$(element).data(),
        background;
    if (data.icon) {
        background = 'style=\"background: url(' + data.icon + ') no-repeat;\"';
    } else {
        background = 'style=\"display: none;\"';
    }
%>
<span class=\"aware-icon-block aware-icon-block-text aware-icon-block-selected\" <%= background %> ></span>
<span class=\"aware-icon-block-text\">
    <%= highlight(_.escape(text)) %>
    (<% if (data.status) { %>";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.integration.integration.enabled.active.label"), "html", null, true);
        echo "<% } else { %>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.integration.integration.enabled.inactive.label"), "html", null, true);
        echo "<% } %>)
</span>
";
    }

    public function getTemplateName()
    {
        return "@OroIntegration/Autocomplete/integration/selection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 13,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroIntegration/Autocomplete/integration/selection.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Resources/views/Autocomplete/integration/selection.html.twig");
    }
}
