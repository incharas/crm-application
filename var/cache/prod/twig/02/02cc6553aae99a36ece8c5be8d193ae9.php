<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/copy_button.html.twig */
class __TwigTemplate_c49d56b19ff315db360e66fac192ecf0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'action_controll' => [$this, 'block_action_controll'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/copy_button.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["id"] = uniqid("value-copy-to-clipboard-");
        // line 4
        $context["options"] = ["id" =>         // line 5
($context["id"] ?? null), "aCss" => "no-hash", "iCss" => "fa-copy", "class" => "btn icons-holder-text", "label" => twig_escape_filter($this->env,         // line 9
($context["data"] ?? null)), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.button.copy_to_clipboard.label"), "labelInIcon" => 0, "pageComponent" => ["view" => ["view" => "oroui/js/app/views/element-value-copy-to-clipboard-view", "elementSelector" => ("#" .         // line 15
($context["id"] ?? null))]]];
        // line 19
        echo "
";
        // line 20
        $this->displayBlock('action_controll', $context, $blocks);
    }

    public function block_action_controll($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_clientLink", [($context["options"] ?? null)], 21, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroUI/copy_button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 21,  53 => 20,  50 => 19,  48 => 15,  47 => 9,  46 => 5,  45 => 4,  43 => 3,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/copy_button.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/copy_button.html.twig");
    }
}
