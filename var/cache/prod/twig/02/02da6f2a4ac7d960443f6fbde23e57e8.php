<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Twig/file.html.twig */
class __TwigTemplate_eb974613d0835a7a26b516a2854635f7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<span class=\"filename\">
    <i class=\"fa ";
        // line 2
        echo twig_escape_filter($this->env, ($context["iconClass"] ?? null), "html", null, true);
        echo "\"></i>
    ";
        // line 3
        if (($context["url"] ?? null)) {
            // line 4
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
            echo "\" data-filename=\"";
            echo twig_escape_filter($this->env, ($context["fileName"] ?? null), "html", null, true);
            echo "\"
           ";
            // line 5
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["additional"] ?? null), "galleryId", [], "array", true, true, false, 5)) {
                echo "data-gallery=\"";
                echo twig_escape_filter($this->env, (($__internal_compile_0 = ($context["additional"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["galleryId"] ?? null) : null), "html", null, true);
                echo "\"";
            }
            // line 6
            echo "           data-sources=\"";
            echo twig_escape_filter($this->env, json_encode(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pictureSources"] ?? null), "sources", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pictureSources"] ?? null), "sources", [], "any", false, false, false, 6), [])) : ([]))), "html", null, true);
            echo "\"
           class=\"no-hash\" title=\"";
            // line 7
            echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
            echo "\">
    ";
        }
        // line 9
        echo "        ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\FormatExtension']->formatFilename(($context["fileName"] ?? null)), "html", null, true);
        echo "
    ";
        // line 10
        if (($context["url"] ?? null)) {
            // line 11
            echo "        </a>
    ";
        }
        // line 13
        echo "</span>
";
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Twig/file.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 13,  76 => 11,  74 => 10,  69 => 9,  64 => 7,  59 => 6,  53 => 5,  46 => 4,  44 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Twig/file.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Twig/file.html.twig");
    }
}
