<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Dashboard/myCalendar.html.twig */
class __TwigTemplate_9e86b373dc0e011426e561f9e4397539 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'actions' => [$this, 'block_actions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/widget.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["calendarMacros"] = $this->macros["calendarMacros"] = $this->loadTemplate("@OroCalendar/macros.html.twig", "@OroCalendar/Dashboard/myCalendar.html.twig", 2)->unwrap();
        // line 3
        $macros["calendarTemplates"] = $this->macros["calendarTemplates"] = $this->loadTemplate("@OroCalendar/templates.html.twig", "@OroCalendar/Dashboard/myCalendar.html.twig", 3)->unwrap();
        // line 1
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/widget.html.twig", "@OroCalendar/Dashboard/myCalendar.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $context["dashboardCalendarOptions"] = ["widgetId" =>         // line 7
($context["widgetId"] ?? null), "calendar" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["entity"] ?? null), "id", [], "any", false, false, false, 8), "aspectRatio" => 2, "calendarOptions" =>         // line 10
($context["calendar"] ?? null), "eventsItemsJson" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_calendarevents", ["calendar" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["entity"] ?? null), "id", [], "any", false, false, false, 11), "start" => twig_date_format_filter($this->env, ($context["startDate"] ?? null), "c"), "end" => twig_date_format_filter($this->env, ($context["endDate"] ?? null), "c"), "subordinate" => true])), "connectionsItemsJson" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_calendar_connections", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["entity"] ?? null), "id", [], "any", false, false, false, 12)])), "eventsOptions" => ["defaultDate" => twig_date_format_filter($this->env, "now", "c", $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()), "containerSelector" => ".calendar-events", "itemViewTemplateSelector" => ("#template-view-calendar-event-" .         // line 16
($context["widgetId"] ?? null)), "itemFormTemplateSelector" => ("#template-calendar-event-" .         // line 17
($context["widgetId"] ?? null)), "defaultView" => "agendaDay", "firstHour" =>         // line 19
($context["firstHour"] ?? null), "aspectRatio" => 2, "recoverView" => false, "allDayHtml" => twig_call_macro($macros["calendarMacros"], "macro_briefDateCell", ["now"], 22, $context, $this->getSourceContext()), "enableAttendeesInvitations" => $this->extensions['Oro\Bundle\CalendarBundle\Twig\AttendeesExtension']->isAttendeesInvitationEnabled()], "connectionsOptions" => [], "colorManagerOptions" => ["colors" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_calendar.calendar_colors")], "invitationStatuses" => [0 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_ACCEPTED"), 1 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_TENTATIVE"), 2 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED")]];
        // line 36
        echo "    <div class=\"calendar-dashboard-widget\" data-page-component-module=\"orocalendar/js/app/components/dashboard-calendar-component\"
         data-page-component-options=\"";
        // line 37
        echo twig_escape_filter($this->env, json_encode(($context["dashboardCalendarOptions"] ?? null)), "html", null, true);
        echo "\">
        <div class=\"calendar-events\"></div>
    </div>
    ";
        // line 40
        echo twig_call_macro($macros["calendarTemplates"], "macro_calendar_event_view_template", [("template-view-calendar-event-" . ($context["widgetId"] ?? null))], 40, $context, $this->getSourceContext());
        echo "
    ";
        // line 41
        echo twig_call_macro($macros["calendarTemplates"], "macro_calendar_event_form_template", [("template-calendar-event-" . ($context["widgetId"] ?? null)), ($context["event_form"] ?? null)], 41, $context, $this->getSourceContext());
        echo "
";
    }

    // line 44
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        $context["actions"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.calendar.new_event"), "data" => ["action-name" => "new-event"]], 1 => ["type" => "link", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.calendar.view_calendar"), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_view_default")]];
        // line 56
        echo "
    ";
        // line 57
        $this->displayParentBlock("actions", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Dashboard/myCalendar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 57,  93 => 56,  90 => 45,  86 => 44,  80 => 41,  76 => 40,  70 => 37,  67 => 36,  65 => 19,  64 => 17,  63 => 16,  62 => 12,  61 => 11,  60 => 10,  59 => 8,  58 => 7,  56 => 6,  52 => 5,  47 => 1,  45 => 3,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Dashboard/myCalendar.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Dashboard/myCalendar.html.twig");
    }
}
