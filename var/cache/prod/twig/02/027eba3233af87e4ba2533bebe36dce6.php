<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/jstree-action.html */
class __TwigTemplate_9d0df7b7676d6530b7080c237e34bc77 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<li>
    <a href=\"#\" class=\"action dropdown-item\" data-role=\"jstree-action\" title=\"<%- label %>\">
        <i class=\"fa-<%- icon %> jstree-actions__icon jstree-actions__icon--<%- icon %>\"></i>
        <%- label %>
    </a>
</li>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/jstree-action.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/jstree-action.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/jstree-action.html");
    }
}
