<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/form/form-description.scss */
class __TwigTemplate_8bf487453e026cd79fd2546b3211ab3e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.form-description {
    margin-bottom: \$form-description-offset-bottom;

    &--signin-help {
        margin-bottom: \$form-description-signin-help-offset-bottom;
    }

    &__logo {
        margin-bottom: \$form-description-logo-offset-bottom;
    }

    &__logo-img {
        display: \$form-description-logo-img-display;
        margin: \$form-description-logo-img-offset;
        max-height: \$form-description-logo-img-max-height;
    }

    &__main {
        font-size: \$form-description-main-font-size;
        font-weight: \$form-description-main-font-weight;
        line-height: \$form-description-main-line-height;
        text-align: \$form-description-main-text-align;
        color: \$form-description-main-color;
        margin-bottom: \$form-description-main-offset-bottom;
    }

    &__text {
        font-size: \$form-description-text-font-size;
        text-align: \$form-description-text-align;
        color: \$form-description-color;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/form/form-description.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/form/form-description.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/form/form-description.scss");
    }
}
