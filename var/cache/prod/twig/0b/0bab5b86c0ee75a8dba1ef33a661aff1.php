<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/CalendarEvent/widget/additionalProperties.html.twig */
class __TwigTemplate_d43475939a5d8fe409d5b5ec4b8b1e7a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (attendees && attendees.length && connection && connection.userId === ";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1), "id", [], "any", false, false, false, 1), "html", null, true);
        echo " && obj.use_hangout) { %>
    ";
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroHangoutsCall/CalendarEvent/widget/additionalProperties.html.twig", 2)->unwrap();
        // line 3
        echo "    <% var hangoutCallOptions = {
        calendarEvent: obj,
        ownerUserId: ";
        // line 5
        echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 5), "id", [], "any", false, false, false, 5));
        echo ",
        declinedStatus: ";
        // line 6
        echo json_encode(twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED"));
        echo ",
        hangoutOptions: {
            widget_size: 70
        }
    }; %>
    ";
        // line 11
        $context["hangoutButton"] = ('' === $tmp = "        <div data-action-name=\"hangout-call\"
             data-page-component-module=\"orohangoutscall/js/app/components/calendar-event-start-hangout-component\"
             data-page-component-options=\"<%- JSON.stringify(hangoutCallOptions) %>\" class=\"action\"></div>
        </div>
    ") ? '' : new Markup($tmp, $this->env->getCharset());
        // line 17
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_renderControlGroup", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.hangoutscall.label"), ($context["hangoutButton"] ?? null)], 17, $context, $this->getSourceContext());
        echo "
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/CalendarEvent/widget/additionalProperties.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 17,  60 => 11,  52 => 6,  48 => 5,  44 => 3,  42 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/CalendarEvent/widget/additionalProperties.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/CalendarEvent/widget/additionalProperties.html.twig");
    }
}
