<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Form/fields.html.twig */
class __TwigTemplate_51669b8b1bc5481b6c0b1ec0994c604d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_file_widget' => [$this, 'block_oro_file_widget'],
            'oro_image_widget' => [$this, 'block_oro_image_widget'],
            'oro_attachment_multi_file_widget' => [$this, 'block_oro_attachment_multi_file_widget'],
            'oro_attachment_multi_file_row' => [$this, 'block_oro_attachment_multi_file_row'],
            'oro_attachment_multi_image_widget' => [$this, 'block_oro_attachment_multi_image_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_file_widget', $context, $blocks);
        // line 47
        echo "
";
        // line 48
        $this->displayBlock('oro_image_widget', $context, $blocks);
        // line 104
        echo "
";
        // line 134
        echo "
";
        // line 135
        $this->displayBlock('oro_attachment_multi_file_widget', $context, $blocks);
        // line 187
        echo "
";
        // line 188
        $this->displayBlock('oro_attachment_multi_file_row', $context, $blocks);
        // line 192
        echo "
";
        // line 193
        $this->displayBlock('oro_attachment_multi_image_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_file_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        ob_start(function () { return ''; });
        // line 3
        echo "        <div class=\"control-group control-group--column file-widget\">
            <div class=\"controls\">
                ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 5), 'widget');
        echo "
                ";
        // line 6
        if ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 6), 'errors')) {
            // line 7
            echo "                    ";
            $context["file_has_errors"] = true;
            // line 8
            echo "                    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 8), 'errors');
            echo "
                ";
        }
        // line 10
        echo "                ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 10), 'widget');
        echo "
            </div>
        </div>
        ";
        // line 13
        if ((( !twig_test_empty(($context["value"] ?? null)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "id", [], "any", false, false, false, 13)) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "emptyFile", [], "any", false, false, false, 13))) {
            // line 14
            echo "            ";
            $context["filename"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "originalFilename", [], "any", true, true, false, 14)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "originalFilename", [], "any", false, false, false, 14), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "filename", [], "any", false, false, false, 14))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "filename", [], "any", false, false, false, 14)));
            // line 15
            echo "            <div class=\"control-group file-widget-preview\">
                <div class=\"controls\">
                    ";
            // line 17
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAttachment/Form/fields.html.twig", 17)->unwrap();
            // line 18
            echo "
                    <div class=\"attachment-item\" ";
            // line 19
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroattachment/js/app/views/attachment-view", "options" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "attachmentViewOptions", [], "any", false, false, false, 21)]], 19, $context, $this->getSourceContext());
            // line 22
            echo ">
                        ";
            // line 23
            if ( !array_key_exists("file_has_errors", $context)) {
                // line 24
                echo "                            <i class=\"attachment-item__icon fa ";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getAttachmentIcon(($context["value"] ?? null)), "html", null, true);
                echo "\" aria-hidden=\"true\"></i>
                            <a href=\"";
                // line 25
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileUrl(($context["value"] ?? null), "download", true), "html", null, true);
                echo "\" class=\"no-hash attachment-item__filename\" title=\"";
                echo twig_escape_filter($this->env, ($context["filename"] ?? null), "html", null, true);
                echo "\">
                                ";
                // line 26
                echo twig_escape_filter($this->env, ($context["filename"] ?? null), "html", null, true);
                echo "
                            </a>
                            <span class=\"attachment-item__file-size\">(";
                // line 28
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileSize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "fileSize", [], "any", false, false, false, 28)), "html", null, true);
                echo ")</span>

                            ";
                // line 30
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 30), "allowDelete", [], "any", false, false, false, 30)) {
                    // line 31
                    echo "                                <button data-role=\"remove\"
                                        class=\"btn btn-action btn-link delete\"
                                        type=\"button\"
                                        data-related=\"";
                    // line 34
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 34), "vars", [], "any", false, false, false, 34), "name", [], "any", false, false, false, 34), "html", null, true);
                    echo "\"
                                        ";
                    // line 35
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 35), "vars", [], "any", false, false, false, 35), "disabled", [], "any", false, false, false, 35)) {
                        echo "disabled=\"disabled\"";
                    }
                    // line 36
                    echo "                                >
                                    <span class=\"fa-close\" aria-hidden=\"true\"></span>
                                </button>
                            ";
                }
                // line 40
                echo "                        ";
            }
            // line 41
            echo "                    </div>
                </div>
            </div>
        ";
        }
        // line 45
        echo "    ";
        $___internal_parse_41_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 2
        echo twig_spaceless($___internal_parse_41_);
    }

    // line 48
    public function block_oro_image_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "    ";
        ob_start(function () { return ''; });
        // line 50
        echo "        <div class=\"control-group control-group--column image-widget\">
            ";
        // line 51
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 51), "name", [], "any", false, false, false, 51) == "attachment")) {
            // line 52
            echo "                <div class=\"controls\">
                    <div class=\"attachment-file\">
                        ";
            // line 54
            $context["jsonParams"] = (((("{\"initializeOptions\":{\"fileDefaultHtml\":\"" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Attach file:")) . "\" ,\"fileButtonHtml\":\"") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Upload")) . "\"}}");
            // line 55
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 55), 'widget', ["attr" => ["data-input-widget-options" => ($context["jsonParams"] ?? null)]]);
            echo "
                    </div>
                </div>
            ";
        } else {
            // line 59
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 59), 'widget');
            echo "
                ";
            // line 60
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 60), 'widget');
            echo "
            ";
        }
        // line 62
        echo "            ";
        if ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 62), 'errors')) {
            // line 63
            echo "                ";
            $context["file_has_errors"] = true;
            // line 64
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 64), 'errors');
            echo "
            ";
        }
        // line 66
        echo "        </div>
        ";
        // line 67
        if (((( !twig_test_empty(($context["value"] ?? null)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "id", [], "any", false, false, false, 67)) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "emptyFile", [], "any", false, false, false, 67)) &&  !array_key_exists("file_has_errors", $context))) {
            // line 68
            echo "            ";
            $context["filename"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "originalFilename", [], "any", true, true, false, 68)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "originalFilename", [], "any", false, false, false, 68), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "filename", [], "any", false, false, false, 68))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "filename", [], "any", false, false, false, 68)));
            // line 69
            echo "            <div class=\"control-group image-widget-preview\">
                <div class=\"controls\">
                    ";
            // line 71
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAttachment/Form/fields.html.twig", 71)->unwrap();
            // line 72
            echo "
                    <div class=\"attachment-item\" ";
            // line 73
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroattachment/js/app/views/attachment-view", "options" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 75
($context["form"] ?? null), "vars", [], "any", false, false, false, 75), "attachmentViewOptions", [], "any", false, false, false, 75)]], 73, $context, $this->getSourceContext());
            // line 76
            echo ">
                        ";
            // line 77
            if ( !array_key_exists("file_has_errors", $context)) {
                // line 78
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileUrl(($context["value"] ?? null), "download", true), "html", null, true);
                echo "\" class=\"no-hash attachment-item__filename\" title=\"";
                echo twig_escape_filter($this->env, ($context["filename"] ?? null), "html", null, true);
                echo "\">
                                ";
                // line 79
                $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroAttachment/Form/fields.html.twig", 79)->display(twig_array_merge($context, ["sources" => $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getResizedPictureSources(                // line 80
($context["value"] ?? null)), "img_attrs" => ["alt" =>                 // line 81
($context["filename"] ?? null)]]));
                // line 83
                echo "                                ";
                echo twig_escape_filter($this->env, ($context["filename"] ?? null), "html", null, true);
                echo "
                            </a>
                            <span class=\"attachment-item__file-size\">(";
                // line 85
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileSize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "fileSize", [], "any", false, false, false, 85)), "html", null, true);
                echo ")</span>

                            ";
                // line 87
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 87), "allowDelete", [], "any", false, false, false, 87)) {
                    // line 88
                    echo "                                <button data-role=\"remove\"
                                        class=\"btn btn-action btn-link delete\"
                                        type=\"button\"
                                        data-related=\"";
                    // line 91
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 91), "vars", [], "any", false, false, false, 91), "name", [], "any", false, false, false, 91), "html", null, true);
                    echo "\"
                                        ";
                    // line 92
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 92), "vars", [], "any", false, false, false, 92), "disabled", [], "any", false, false, false, 92)) {
                        echo "disabled=\"disabled\"";
                    }
                    // line 93
                    echo "                                >
                                    <span class=\"fa-close\" aria-hidden=\"true\"></span>
                                </button>
                            ";
                }
                // line 97
                echo "                        ";
            }
            // line 98
            echo "                    </div>
                </div>
            </div>
        ";
        }
        // line 102
        echo "    ";
        $___internal_parse_42_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 49
        echo twig_spaceless($___internal_parse_42_);
    }

    // line 135
    public function block_oro_attachment_multi_file_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 136
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAttachment/Form/fields.html.twig", 136)->unwrap();
        // line 137
        echo "    ";
        $macros["fields"] = $this;
        // line 138
        echo "    ";
        ob_start(function () { return ''; });
        // line 139
        echo "        ";
        if (array_key_exists("prototype", $context)) {
            // line 140
            echo "            ";
            $context["prototype_html"] = twig_call_macro($macros["fields"], "macro_oro_attachment_multi_file_item_prototype", [($context["form"] ?? null)], 140, $context, $this->getSourceContext());
            // line 141
            echo "        ";
        }
        // line 142
        echo "        ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 142)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 142) . " ")) : ("")) . " file-collection")]);
        // line 143
        echo "        ";
        $context["prototype_name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 143), "prototype_name", [], "any", false, false, false, 143);
        // line 144
        echo "
        <div class=\"row-oro attachment-fileitems\" ";
        // line 145
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroattachment/js/app/components/multi-file-control-component", "options" => ["maxNumber" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 148
($context["form"] ?? null), "vars", [], "any", false, false, false, 148), "maxNumber", [], "any", false, false, false, 148)]]], 145, $context, $this->getSourceContext());
        // line 150
        echo ">
            <div ";
        // line 151
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
                <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\">
                    <thead>
                        <tr>
                            <th class=\"sort-order\"><span>";
        // line 155
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.attachment.fileitem.sort_order.label"), "html", null, true);
        echo "</span></th>
                            <th class=\"file\"><span>";
        // line 156
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((array_key_exists("fileLabel", $context)) ? (_twig_default_filter(($context["fileLabel"] ?? null), "oro.attachment.fileitem.file.label")) : ("oro.attachment.fileitem.file.label"))), "html", null, true);
        echo "</span></th>
                            ";
        // line 157
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 157), "allow_delete", [], "any", false, false, false, 157)) {
            // line 158
            echo "                                <th class=\"remove\"></th>
                            ";
        }
        // line 160
        echo "                        </tr>
                    </thead>
                    <tbody data-last-index=\"";
        // line 162
        echo twig_escape_filter($this->env, twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 162)), "html", null, true);
        echo "\"
                           data-row-count-add=\"";
        // line 163
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 163), "row_count_add", [], "any", false, false, false, 163), "html", null, true);
        echo "\"
                           data-prototype-name=\"";
        // line 164
        echo twig_escape_filter($this->env, ($context["prototype_name"] ?? null), "html", null, true);
        echo "\"
                           ";
        // line 165
        if (array_key_exists("prototype_html", $context)) {
            echo " data-prototype=\"";
            echo twig_escape_filter($this->env, ($context["prototype_html"] ?? null));
            echo "\"";
        }
        // line 166
        echo "                    >
                        ";
        // line 167
        if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 167))) {
            // line 168
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 168));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 169
                echo "                                ";
                echo twig_call_macro($macros["fields"], "macro_oro_attachment_multi_file_item_prototype", [$context["child"]], 169, $context, $this->getSourceContext());
                echo "
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 171
            echo "                        ";
        } elseif ((($context["show_form_when_empty"] ?? null) && array_key_exists("prototype_html", $context))) {
            // line 172
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 172), "row_count_initial", [], "any", false, false, false, 172) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 173
                echo "                                ";
                echo twig_replace_filter(($context["prototype_html"] ?? null), [($context["prototype_name"] ?? null) => $context["i"]]);
                echo "
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 175
            echo "                        ";
        }
        // line 176
        echo "                    </tbody>
                </table>
                ";
        // line 178
        if (($context["allow_add"] ?? null)) {
            // line 179
            echo "                    <a class=\"btn add-list-item\" data-container=\".file-collection tbody\" href=\"javascript: void(0);\">
                        ";
            // line 180
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((array_key_exists("addLabel", $context)) ? (_twig_default_filter(($context["addLabel"] ?? null), "oro.attachment.fileitem.file.add.label")) : ("oro.attachment.fileitem.file.add.label"))), "html", null, true);
            echo "
                    </a>
                ";
        }
        // line 183
        echo "            </div>
        </div>
    ";
        $___internal_parse_43_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 138
        echo twig_spaceless($___internal_parse_43_);
    }

    // line 188
    public function block_oro_attachment_multi_file_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 189
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    ";
        // line 190
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
";
    }

    // line 193
    public function block_oro_attachment_multi_image_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 194
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["addLabel" => "oro.attachment.fileitem.image.add.label", "fileLabel" => "oro.attachment.fileitem.image.label"]);
        echo "
";
    }

    // line 105
    public function macro_oro_attachment_multi_file_item_prototype($__widget__ = null, $__attributes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "attributes" => $__attributes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 106
            echo "    ";
            if (twig_in_filter("collection", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 106), "block_prefixes", [], "any", false, false, false, 106))) {
                // line 107
                echo "        ";
                $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 107), "prototype", [], "any", false, false, false, 107);
                // line 108
                echo "        ";
                $context["name"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 108), "full_name", [], "any", false, false, false, 108) . "[") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 108), "prototype", [], "any", false, false, false, 108), "vars", [], "any", false, false, false, 108), "name", [], "any", false, false, false, 108)) . "]");
                // line 109
                echo "        ";
                $context["disabled"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 109), "disabled", [], "any", false, false, false, 109);
                // line 110
                echo "        ";
                $context["allow_delete"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 110), "allow_delete", [], "any", false, false, false, 110);
                // line 111
                echo "    ";
            } else {
                // line 112
                echo "        ";
                $context["form"] = ($context["widget"] ?? null);
                // line 113
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 113), "full_name", [], "any", false, false, false, 113);
                // line 114
                echo "        ";
                $context["disabled"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, false, false, 114), "vars", [], "any", false, false, false, 114), "disabled", [], "any", false, false, false, 114);
                // line 115
                echo "        ";
                $context["allow_delete"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, false, false, 115), "vars", [], "any", false, false, false, 115), "allow_delete", [], "any", false, false, false, 115);
                // line 116
                echo "    ";
            }
            // line 117
            echo "
    <tr data-content=\"";
            // line 118
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\" data-validation-optional-group ";
            echo twig_escape_filter($this->env, ($context["attributes"] ?? null), "html", null, true);
            echo ">
        <td class=\"sort-order\">
            ";
            // line 120
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "sortOrder", [], "any", false, false, false, 120), 'widget');
            echo "
        </td>
        <td class=\"file\">
            ";
            // line 123
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 123), 'widget');
            echo "
        </td>
        ";
            // line 125
            if (($context["allow_delete"] ?? null)) {
                // line 126
                echo "            <td>
                <button type=\"button\" class=\"removeRow btn btn-icon btn-square-lighter\" aria-label=\"";
                // line 127
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Remove row"), "html", null, true);
                echo "\">
                    <span class=\"fa-trash-o\" aria-hidden=\"true\"></span>
                </button>
            </td>
        ";
            }
            // line 132
            echo "    </tr>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  564 => 132,  556 => 127,  553 => 126,  551 => 125,  546 => 123,  540 => 120,  533 => 118,  530 => 117,  527 => 116,  524 => 115,  521 => 114,  518 => 113,  515 => 112,  512 => 111,  509 => 110,  506 => 109,  503 => 108,  500 => 107,  497 => 106,  483 => 105,  476 => 194,  472 => 193,  466 => 190,  461 => 189,  457 => 188,  453 => 138,  448 => 183,  442 => 180,  439 => 179,  437 => 178,  433 => 176,  430 => 175,  421 => 173,  416 => 172,  413 => 171,  404 => 169,  399 => 168,  397 => 167,  394 => 166,  388 => 165,  384 => 164,  380 => 163,  376 => 162,  372 => 160,  368 => 158,  366 => 157,  362 => 156,  358 => 155,  351 => 151,  348 => 150,  346 => 148,  345 => 145,  342 => 144,  339 => 143,  336 => 142,  333 => 141,  330 => 140,  327 => 139,  324 => 138,  321 => 137,  318 => 136,  314 => 135,  310 => 49,  307 => 102,  301 => 98,  298 => 97,  292 => 93,  288 => 92,  284 => 91,  279 => 88,  277 => 87,  272 => 85,  266 => 83,  264 => 81,  263 => 80,  262 => 79,  255 => 78,  253 => 77,  250 => 76,  248 => 75,  247 => 73,  244 => 72,  242 => 71,  238 => 69,  235 => 68,  233 => 67,  230 => 66,  224 => 64,  221 => 63,  218 => 62,  213 => 60,  208 => 59,  200 => 55,  198 => 54,  194 => 52,  192 => 51,  189 => 50,  186 => 49,  182 => 48,  178 => 2,  175 => 45,  169 => 41,  166 => 40,  160 => 36,  156 => 35,  152 => 34,  147 => 31,  145 => 30,  140 => 28,  135 => 26,  129 => 25,  124 => 24,  122 => 23,  119 => 22,  117 => 21,  116 => 19,  113 => 18,  111 => 17,  107 => 15,  104 => 14,  102 => 13,  95 => 10,  89 => 8,  86 => 7,  84 => 6,  80 => 5,  76 => 3,  73 => 2,  69 => 1,  65 => 193,  62 => 192,  60 => 188,  57 => 187,  55 => 135,  52 => 134,  49 => 104,  47 => 48,  44 => 47,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Form/fields.html.twig");
    }
}
