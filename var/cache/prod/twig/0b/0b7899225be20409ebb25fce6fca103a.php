<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/widget/widget-manager.js */
class __TwigTemplate_4bb8b7b032e52bca4b29f4294338ec40 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'jquery',
    'underscore',
    'oroui/js/mediator'
], function(\$, _, mediator) {
    'use strict';

    /**
     * @export oroui/js/widget-manager
     * @name   oro.widgetManager
     */
    const widgetManager = {
        widgets: {},
        aliases: {},

        /**
         * Reset manager to initial state.
         */
        resetWidgets: function() {
            _.each(this.widgets, function(widget) {
                // if widget is not actual any more -- remove it
                if (!widget.isActual()) {
                    widget.remove();
                }
            });
        },

        /**
         * Add widget instance to registry.
         *
         * @param {oroui.widget.AbstractWidget} widget
         */
        addWidgetInstance: function(widget) {
            this.widgets[widget.getWid()] = widget;
            mediator.trigger('widget_registration:wid:' + widget.getWid(), widget);
            if (widget.getAlias()) {
                this.aliases[widget.getAlias()] = widget.getWid();
                mediator.trigger('widget_registration:' + widget.getAlias(), widget);
            }
        },

        /**
         * Get widget instance by widget identifier and pass it to callback when became available.
         *
         * @param {string} wid unique widget identifier
         * @param {Function} callback widget instance handler
         */
        getWidgetInstance: function(wid, callback) {
            if (this.widgets.hasOwnProperty(wid)) {
                callback(this.widgets[wid]);
            } else {
                mediator.once('widget_registration:wid:' + wid, callback);
            }
        },

        /**
         * Get widget instance by alias and pass it to callback when became available.
         *
         * @param {string} alias widget alias
         * @param {Function} callback widget instance handler
         */
        getWidgetInstanceByAlias: function(alias, callback) {
            if (this.aliases.hasOwnProperty(alias)) {
                this.getWidgetInstance(this.aliases[alias], callback);
            } else {
                mediator.once('widget_registration:' + alias, callback);
            }
        },

        /**
         * Remove widget instance from registry.
         *
         * @param {string} wid unique widget identifier
         */
        removeWidget: function(wid) {
            const widget = this.widgets[wid];
            if (widget) {
                delete this.aliases[widget.getAlias()];
            }
            delete this.widgets[wid];
        }
    };

    return widgetManager;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/widget/widget-manager.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/widget/widget-manager.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/widget/widget-manager.js");
    }
}
