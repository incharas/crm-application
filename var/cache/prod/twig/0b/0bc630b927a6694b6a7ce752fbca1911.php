<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/actions/view.html.twig */
class __TwigTemplate_e18042f74df10a08f9cc173edacb42e2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'before_content_addition' => [$this, 'block_before_content_addition'],
            'ownerLink' => [$this, 'block_ownerLink'],
            'content' => [$this, 'block_content'],
            'workflowStepListContainer' => [$this, 'block_workflowStepListContainer'],
            'navButtonContainer' => [$this, 'block_navButtonContainer'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageActions' => [$this, 'block_pageActions'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'pageTitleIcon' => [$this, 'block_pageTitleIcon'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'after_breadcrumbs' => [$this, 'block_after_breadcrumbs'],
            'breadcrumbMessage' => [$this, 'block_breadcrumbMessage'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
            'sync_content_tags' => [$this, 'block_sync_content_tags'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroUI/actions/view.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["syncMacro"] = $this->macros["syncMacro"] = $this->loadTemplate("@OroSync/Include/contentTags.html.twig", "@OroUI/actions/view.html.twig", 2)->unwrap();
        // line 3
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/actions/view.html.twig", 3)->unwrap();
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_before_content_addition($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_before_content_addition", $context)) ? (_twig_default_filter(($context["view_before_content_addition"] ?? null), "view_before_content_addition")) : ("view_before_content_addition")), ["entity" => ($context["entity"] ?? null)]);
    }

    // line 9
    public function block_ownerLink($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/actions/view.html.twig", 10)->unwrap();
        // line 11
        echo "
    ";
        // line 12
        if ( !(null === ($context["entity"] ?? null))) {
            // line 13
            echo "        ";
            ob_start(function () { return ''; });
            // line 14
            echo twig_call_macro($macros["UI"], "macro_entityOwnerLink", [($context["entity"] ?? null)], 14, $context, $this->getSourceContext());
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("additional_owner_info", $context)) ? (_twig_default_filter(($context["additional_owner_info"] ?? null), "additional_owner_info")) : ("additional_owner_info")), ["entity" => ($context["entity"] ?? null)]);
            $context["ownerLink"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 16
            echo "        ";
            if ( !twig_test_empty(twig_trim_filter(($context["ownerLink"] ?? null)))) {
                // line 17
                echo "            <li>";
                echo twig_escape_filter($this->env, ($context["ownerLink"] ?? null), "html", null, true);
                echo "</li>
        ";
            }
            // line 19
            echo "    ";
        }
    }

    // line 22
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/actions/view.html.twig", 23)->unwrap();
        // line 24
        echo "
";
        // line 25
        $context["content_attr"] = twig_array_merge(((array_key_exists("content_attr", $context)) ? (_twig_default_filter(($context["content_attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 26
($context["content_attr"] ?? null), "class", [], "any", true, true, false, 26)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content_attr"] ?? null), "class", [], "any", false, false, false, 26)) : ("layout-content")))]);
        // line 28
        echo "
<div";
        // line 29
        echo twig_call_macro($macros["UI"], "macro_attributes", [($context["content_attr"] ?? null)], 29, $context, $this->getSourceContext());
        echo "
        ";
        // line 30
        if (array_key_exists("pageComponent", $context)) {
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["pageComponent"] ?? null)], 30, $context, $this->getSourceContext());
        }
        // line 31
        echo "        >
    <div class=\"container-fluid page-title\">
        ";
        // line 33
        $this->displayBlock('workflowStepListContainer', $context, $blocks);
        // line 36
        echo "
        ";
        // line 37
        ob_start(function () { return ''; });
        // line 38
        echo "            ";
        $this->displayBlock('navButtonContainer', $context, $blocks);
        // line 43
        echo "        ";
        $context["titleButtonsBlock"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 44
        echo "
        ";
        // line 45
        ob_start(function () { return ''; });
        // line 46
        echo "            ";
        $this->displayBlock('pageActions', $context, $blocks);
        // line 67
        echo "        ";
        $context["pageActionsBlock"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 68
        echo "
        <div class=\"navigation navbar-extra navbar-extra-right\">
            ";
        // line 70
        $this->displayBlock('pageHeader', $context, $blocks);
        // line 149
        echo "        </div>
        ";
        // line 150
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("page_title_after", $context)) ? (_twig_default_filter(($context["page_title_after"] ?? null), "page_title_after")) : ("page_title_after")), ["entity" => ($context["entity"] ?? null)]);
        // line 151
        echo "    </div>

    ";
        // line 153
        $context["content_data_attr"] = twig_array_merge(((array_key_exists("content_data_attr", $context)) ? (_twig_default_filter(($context["content_data_attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 154
($context["content_data_attr"] ?? null), "class", [], "any", true, true, false, 154)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["content_data_attr"] ?? null), "class", [], "any", false, false, false, 154)) : ("layout-content")) . " scrollable-container"))]);
        // line 156
        echo "    <div";
        echo twig_call_macro($macros["UI"], "macro_attributes", [($context["content_data_attr"] ?? null)], 156, $context, $this->getSourceContext());
        echo ">
        ";
        // line 157
        $this->displayBlock('content_data', $context, $blocks);
        // line 251
        echo "    </div>

    ";
        // line 253
        $this->displayBlock('sync_content_tags', $context, $blocks);
        // line 258
        echo "</div>
";
    }

    // line 33
    public function block_workflowStepListContainer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "            ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("page_title_before", $context)) ? (_twig_default_filter(($context["page_title_before"] ?? null), "page_title_before")) : ("page_title_before")), ["entity" => ($context["entity"] ?? null)]);
        // line 35
        echo "        ";
    }

    // line 38
    public function block_navButtonContainer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "                ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_navButtons_before", $context)) ? (_twig_default_filter(($context["view_navButtons_before"] ?? null), "view_navButtons_before")) : ("view_navButtons_before")), ["entity" => ($context["entity"] ?? null)]);
        // line 40
        echo "                ";
        $this->displayBlock('navButtons', $context, $blocks);
        // line 41
        echo "                ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_navButtons_after", $context)) ? (_twig_default_filter(($context["view_navButtons_after"] ?? null), "view_navButtons_after")) : ("view_navButtons_after")), ["entity" => ($context["entity"] ?? null)]);
        // line 42
        echo "            ";
    }

    // line 40
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_navButtons", $context)) ? (_twig_default_filter(($context["view_navButtons"] ?? null), "view_navButtons")) : ("view_navButtons")), ["entity" => ($context["entity"] ?? null)]);
    }

    // line 46
    public function block_pageActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "                ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_pageActions_before", $context)) ? (_twig_default_filter(($context["view_pageActions_before"] ?? null), "view_pageActions_before")) : ("view_pageActions_before")), ["entity" => ($context["entity"] ?? null)]);
        // line 48
        echo "
                ";
        // line 49
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 50
            echo "                    ";
            $this->displayBlock("ownerLink", $context, $blocks);
            echo "
                ";
        }
        // line 52
        echo "
                ";
        // line 53
        $context["audit_entity_id"] = ((array_key_exists("audit_entity_id", $context)) ? (($context["audit_entity_id"] ?? null)) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", true, true, false, 53)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 53))) : (""))));
        // line 54
        echo "                ";
        if (($context["audit_entity_id"] ?? null)) {
            // line 55
            echo "                    ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("change_history_block", $context)) ? (_twig_default_filter(($context["change_history_block"] ?? null), "change_history_block")) : ("change_history_block")), ["entity" =>             // line 56
($context["entity"] ?? null), "entity_class" => ((            // line 57
array_key_exists("audit_entity_class", $context)) ? (_twig_default_filter(($context["audit_entity_class"] ?? null), null)) : (null)), "id" =>             // line 58
($context["audit_entity_id"] ?? null), "title" => ((            // line 59
array_key_exists("audit_title", $context)) ? (_twig_default_filter(($context["audit_title"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "__toString", [], "any", true, true, false, 59)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "__toString", [], "any", false, false, false, 59)) : (null)))) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "__toString", [], "any", true, true, false, 59)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "__toString", [], "any", false, false, false, 59)) : (null)))), "audit_path" => ((            // line 60
array_key_exists("audit_path", $context)) ? (_twig_default_filter(($context["audit_path"] ?? null), "oro_dataaudit_history")) : ("oro_dataaudit_history")), "audit_show_change_history" => ((            // line 61
array_key_exists("audit_show_change_history", $context)) ? (_twig_default_filter(($context["audit_show_change_history"] ?? null), false)) : (false))]);
            // line 63
            echo "                ";
        }
        // line 64
        echo "
                ";
        // line 65
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_pageActions_after", $context)) ? (_twig_default_filter(($context["view_pageActions_after"] ?? null), "view_pageActions_after")) : ("view_pageActions_after")), ["entity" => ($context["entity"] ?? null)]);
        // line 66
        echo "            ";
    }

    // line 70
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "                <div class=\"row\">
                    <div class=\"pull-left-extra\">
                        ";
        // line 73
        $this->displayBlock('pageTitleIcon', $context, $blocks);
        // line 74
        echo "
                        <div class=\"page-title__path\">
                            <div class=\"top-row\">
                                ";
        // line 77
        $this->displayBlock('breadcrumbs', $context, $blocks);
        // line 112
        echo "                            </div>
                        </div>
                    </div>
                    ";
        // line 115
        ob_start(function () { return ''; });
        // line 116
        echo "                        <div class=\"pull-right title-buttons-container\">
                            ";
        // line 117
        echo twig_escape_filter($this->env, ($context["titleButtonsBlock"] ?? null), "html", null, true);
        echo "
                        </div>
                    ";
        $___internal_parse_22_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 115
        echo twig_spaceless($___internal_parse_22_);
        // line 120
        echo "                </div>
                <div class=\"row inline-info\">
                    <div class=\"pull-left-extra\">
                        <ul class=\"inline\">
                            ";
        // line 124
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("page_header_stats_before", $context)) ? (_twig_default_filter(($context["page_header_stats_before"] ?? null), "page_header_stats_before")) : ("page_header_stats_before")), ["entity" => ($context["entity"] ?? null)]);
        // line 125
        echo "                            ";
        $this->displayBlock('stats', $context, $blocks);
        // line 133
        echo "                            ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("page_header_stats_after", $context)) ? (_twig_default_filter(($context["page_header_stats_after"] ?? null), "page_header_stats_after")) : ("page_header_stats_after")), ["entity" => ($context["entity"] ?? null)]);
        // line 134
        echo "
                            ";
        // line 135
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 136
            echo "                                ";
            $this->displayBlock("ownerLink", $context, $blocks);
            echo "
                            ";
        }
        // line 138
        echo "                        </ul>
                    </div>
                    <div class=\"pull-right page-title__entity-info-state\">
                        <div class=\"inline-decorate-container\">
                            <ul class=\"inline-decorate\">
                                ";
        // line 143
        echo twig_escape_filter($this->env, ($context["pageActionsBlock"] ?? null), "html", null, true);
        echo "
                            </ul>
                        </div>
                    </div>
                </div>
            ";
    }

    // line 73
    public function block_pageTitleIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 77
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 78
        echo "                                    ";
        if (array_key_exists("breadcrumbs", $context)) {
            // line 79
            echo "                                        <div class=\"page-title__entity-title-wrapper\">
                                            ";
            // line 80
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", true, true, false, 80)) {
                // line 81
                echo "                                                <div class=\"sub-title\">";
                // line 82
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexPath", [], "any", true, true, false, 82)) {
                    // line 83
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->addUrlQuery(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexPath", [], "any", false, false, false, 83)), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", false, false, false, 83), "html", null, true);
                    echo "</a>";
                } else {
                    // line 85
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", false, false, false, 85), "html", null, true);
                }
                // line 87
                echo "</div>
                                                <span class=\"separator\">/</span>
                                            ";
            }
            // line 90
            echo "                                            ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "additional", [], "any", true, true, false, 90)) {
                // line 91
                echo "                                                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "additional", [], "any", false, false, false, 91));
                foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                    // line 92
                    echo "                                                    <div class=\"sub-title\">";
                    // line 93
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexPath", [], "any", true, true, false, 93)) {
                        // line 94
                        echo "<a href=\"";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexPath", [], "any", false, false, false, 94), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexLabel", [], "any", false, false, false, 94), "html", null, true);
                        echo "</a>";
                    } else {
                        // line 96
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexLabel", [], "any", false, false, false, 96), "html", null, true);
                    }
                    // line 98
                    echo "</div>
                                                    <span class=\"separator\">/</span>
                                                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 101
                echo "                                            ";
            }
            // line 102
            echo "                                            ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "rawEntityTitle", [], "any", true, true, false, 102) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "rawEntityTitle", [], "any", false, false, false, 102))) {
                // line 103
                echo "                                                <h1 class=\"page-title__entity-title\">";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entityTitle", [], "any", false, false, false, 103);
                echo "</h1>
                                            ";
            } else {
                // line 105
                echo "                                                <h1 class=\"page-title__entity-title\">";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entityTitle", [], "any", false, false, false, 105), "html", null, true);
                echo "</h1>
                                            ";
            }
            // line 107
            echo "                                        </div>
                                        ";
            // line 108
            $this->displayBlock('after_breadcrumbs', $context, $blocks);
            // line 109
            echo "                                    ";
        }
        // line 110
        echo "                                    ";
        $this->displayBlock('breadcrumbMessage', $context, $blocks);
        // line 111
        echo "                                ";
    }

    // line 108
    public function block_after_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 110
    public function block_breadcrumbMessage($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 125
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 126
        echo "                                ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "createdAt")) {
            // line 127
            echo "                                    <li>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
            echo ": ";
            (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, true, false, 127), "createdAt", [], "any", true, true, false, 127) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 127), "createdAt", [], "any", false, false, false, 127))) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 127), "createdAt", [], "any", false, false, false, 127)), "html", null, true))) : (print ("N/A")));
            echo "</li>
                                ";
        }
        // line 129
        echo "                                ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "updatedAt")) {
            // line 130
            echo "                                    <li>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
            echo ": ";
            (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, true, false, 130), "updatedAt", [], "any", true, true, false, 130) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 130), "updatedAt", [], "any", false, false, false, 130))) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 130), "updatedAt", [], "any", false, false, false, 130)), "html", null, true))) : (print ("N/A")));
            echo "</li>
                                ";
        }
        // line 132
        echo "                            ";
    }

    // line 157
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 158
        echo "            ";
        if ((array_key_exists("data", $context) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "dataBlocks", [], "any", true, true, false, 158))) {
            // line 159
            echo "                ";
            $context["data"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->processView($this->env, ($context["data"] ?? null), ($context["entity"] ?? null));
            // line 160
            echo "                ";
            $context["dataBlocks"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "dataBlocks", [], "any", false, false, false, 160);
            // line 162
            ob_start(function () { return ''; });
            // line 163
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_activities", $context)) ? (_twig_default_filter(($context["view_content_data_activities"] ?? null), "view_content_data_activities")) : ("view_content_data_activities")), ["entity" => ($context["entity"] ?? null)]);
            $context["activitiesData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 166
            if ( !twig_test_empty(($context["activitiesData"] ?? null))) {
                // line 167
                echo "                    ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.sections.activities"), "priority" => 1000, "subblocks" => [0 => ["spanClass" => "empty activities-container", "data" => [0 =>                 // line 173
($context["activitiesData"] ?? null)]]]]]);
                // line 177
                echo "                ";
            }
            // line 179
            ob_start(function () { return ''; });
            // line 180
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_marketing_activities", $context)) ? (_twig_default_filter(($context["view_content_data_marketing_activities"] ?? null), "view_content_data_marketing_activities")) : ("view_content_data_marketing_activities")), ["entity" => ($context["entity"] ?? null)]);
            $context["marketingActivitiesData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 183
            if ( !twig_test_empty(($context["marketingActivitiesData"] ?? null))) {
                // line 184
                echo "                    ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.sections.marketingactivities"), "priority" => 1050, "subblocks" => [0 => ["spanClass" => "empty marketing-activities-container", "data" => [0 =>                 // line 190
($context["marketingActivitiesData"] ?? null)]]]]]);
                // line 194
                echo "                ";
            }
            // line 196
            ob_start(function () { return ''; });
            // line 197
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_communications", $context)) ? (_twig_default_filter(($context["view_content_data_communications"] ?? null), "view_content_data_communications")) : ("view_content_data_communications")), ["entity" => ($context["entity"] ?? null)]);
            $context["communicationsData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 200
            if ( !twig_test_empty(($context["communicationsData"] ?? null))) {
                // line 201
                echo "                    ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Communications"), "priority" => 1100, "subblocks" => [0 => ["spanClass" => "empty", "data" => [0 =>                 // line 207
($context["communicationsData"] ?? null)]]]]]);
                // line 211
                echo "                ";
            }
            // line 213
            ob_start(function () { return ''; });
            // line 214
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_additional_information", $context)) ? (_twig_default_filter(($context["view_content_data_additional_information"] ?? null), "view_content_data_additional_information")) : ("view_content_data_additional_information")), ["entity" => ($context["entity"] ?? null)]);
            $context["additionalInformationData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 217
            if ( !twig_test_empty(($context["additionalInformationData"] ?? null))) {
                // line 218
                echo "                    ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional Information"), "priority" => 1200, "subblocks" => [0 => ["spanClass" => "empty", "data" => [0 =>                 // line 224
($context["additionalInformationData"] ?? null)]]]]]);
                // line 228
                echo "                ";
            }
            // line 230
            ob_start(function () { return ''; });
            // line 231
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_comments", $context)) ? (_twig_default_filter(($context["view_content_data_comments"] ?? null), "view_content_data_comments")) : ("view_content_data_comments")), ["entity" => ($context["entity"] ?? null)]);
            $context["commentsData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 234
            if ( !twig_test_empty(($context["commentsData"] ?? null))) {
                // line 235
                echo "                    ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.comment.entity_plural_label"), "priority" => 1300, "subblocks" => [0 => ["spanClass" => "responsive-cell activity-list-widget", "data" => [0 =>                 // line 241
($context["commentsData"] ?? null)]]]]]);
                // line 245
                echo "                ";
            }
            // line 246
            echo "
                ";
            // line 247
            $context["data"] = twig_array_merge(($context["data"] ?? null), ["dataBlocks" => ($context["dataBlocks"] ?? null)]);
            // line 248
            echo "            ";
        }
        // line 249
        echo "            ";
        echo twig_call_macro($macros["UI"], "macro_scrollData", [($context["id"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null)], 249, $context, $this->getSourceContext());
        echo "
        ";
    }

    // line 253
    public function block_sync_content_tags($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 254
        echo "        ";
        // line 255
        echo "        ";
        echo twig_call_macro($macros["syncMacro"], "macro_syncContentTags", [($context["entity"] ?? null)], 255, $context, $this->getSourceContext());
        echo "
        ";
        // line 256
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_entity_sync_content_tags", $context)) ? (_twig_default_filter(($context["view_entity_sync_content_tags"] ?? null), "view_entity_sync_content_tags")) : ("view_entity_sync_content_tags")), ["entity" => ($context["entity"] ?? null)]);
        // line 257
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@OroUI/actions/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  605 => 257,  603 => 256,  598 => 255,  596 => 254,  592 => 253,  585 => 249,  582 => 248,  580 => 247,  577 => 246,  574 => 245,  572 => 241,  570 => 235,  568 => 234,  565 => 231,  563 => 230,  560 => 228,  558 => 224,  556 => 218,  554 => 217,  551 => 214,  549 => 213,  546 => 211,  544 => 207,  542 => 201,  540 => 200,  537 => 197,  535 => 196,  532 => 194,  530 => 190,  528 => 184,  526 => 183,  523 => 180,  521 => 179,  518 => 177,  516 => 173,  514 => 167,  512 => 166,  509 => 163,  507 => 162,  504 => 160,  501 => 159,  498 => 158,  494 => 157,  490 => 132,  482 => 130,  479 => 129,  471 => 127,  468 => 126,  464 => 125,  458 => 110,  452 => 108,  448 => 111,  445 => 110,  442 => 109,  440 => 108,  437 => 107,  431 => 105,  425 => 103,  422 => 102,  419 => 101,  411 => 98,  408 => 96,  401 => 94,  399 => 93,  397 => 92,  392 => 91,  389 => 90,  384 => 87,  381 => 85,  374 => 83,  372 => 82,  370 => 81,  368 => 80,  365 => 79,  362 => 78,  358 => 77,  352 => 73,  342 => 143,  335 => 138,  329 => 136,  327 => 135,  324 => 134,  321 => 133,  318 => 125,  316 => 124,  310 => 120,  308 => 115,  302 => 117,  299 => 116,  297 => 115,  292 => 112,  290 => 77,  285 => 74,  283 => 73,  279 => 71,  275 => 70,  271 => 66,  269 => 65,  266 => 64,  263 => 63,  261 => 61,  260 => 60,  259 => 59,  258 => 58,  257 => 57,  256 => 56,  254 => 55,  251 => 54,  249 => 53,  246 => 52,  240 => 50,  238 => 49,  235 => 48,  232 => 47,  228 => 46,  221 => 40,  217 => 42,  214 => 41,  211 => 40,  208 => 39,  204 => 38,  200 => 35,  197 => 34,  193 => 33,  188 => 258,  186 => 253,  182 => 251,  180 => 157,  175 => 156,  173 => 154,  172 => 153,  168 => 151,  166 => 150,  163 => 149,  161 => 70,  157 => 68,  154 => 67,  151 => 46,  149 => 45,  146 => 44,  143 => 43,  140 => 38,  138 => 37,  135 => 36,  133 => 33,  129 => 31,  125 => 30,  121 => 29,  118 => 28,  116 => 26,  115 => 25,  112 => 24,  110 => 23,  106 => 22,  101 => 19,  95 => 17,  92 => 16,  88 => 14,  85 => 13,  83 => 12,  80 => 11,  77 => 10,  73 => 9,  68 => 6,  64 => 5,  60 => 1,  58 => 3,  56 => 2,  49 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/actions/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/actions/view.html.twig");
    }
}
