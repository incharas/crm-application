<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/main.scss */
class __TwigTemplate_2bda0afbabb11c442aeb266926f93f3f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

// add all lib
@import 'layout';
@import 'uniform';
@import 'oro';
@import 'utilites';
@import 'oro/main';

// desktop resolution
@import 'desktop/main';

// mobile resolution
@import 'mobile/main';

// printing rules
@import 'print/main';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/main.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/main.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/main.scss");
    }
}
