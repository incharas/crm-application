<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntity/Form/fields.html.twig */
class __TwigTemplate_f3d436b37aca261cc2dbcbdf1341f9f8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_oro_entity_config_type_view_is_displayable_widget' => [$this, 'block__oro_entity_config_type_view_is_displayable_widget'],
            'oro_entity_fallback_value_widget' => [$this, 'block_oro_entity_fallback_value_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_oro_entity_config_type_view_is_displayable_widget', $context, $blocks);
        // line 13
        echo "
";
        // line 14
        $this->displayBlock('oro_entity_fallback_value_widget', $context, $blocks);
    }

    // line 1
    public function block__oro_entity_config_type_view_is_displayable_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntity/Form/fields.html.twig", 2)->unwrap();
        // line 3
        echo "
    <div ";
        // line 4
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroentity/js/app/views/displayable-priority-view", "options" => ["prioritySelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["form"] ?? null), "parent", [], "any", false, false, false, 7), "children", [], "any", false, false, false, 7), "priority", [], "any", false, false, false, 7), "vars", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7))]]], 4, $context, $this->getSourceContext());
        // line 9
        echo ">
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ["data-field" => "is_displayable"]]);
        echo "
    </div>
";
    }

    // line 14
    public function block_oro_entity_fallback_value_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    <div class=\"entity-fallback-container\"
           data-page-component-module=\"oroui/js/app/components/view-component\"
           data-page-component-options=\"";
        // line 17
        echo twig_escape_filter($this->env, json_encode(["view" => "oroentity/js/app/views/entity-field-fallback"]), "html", null, true);
        echo "\"
    >
        <div class=\"entity-fallback-container__value\">
            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "scalarValue", [], "any", false, false, false, 20), 'widget', ["attr" => ["class" => "entity-field-value"]]);
        echo "
            ";
        // line 21
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "scalarValue", [], "any", false, false, false, 21), 'errors');
        echo "
        </div>
        <div class=\"entity-fallback-container__line\">
            <div class=\"entity-fallback-container__use\">
                ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "useFallback", [], "any", false, false, false, 25), 'widget', ["attr" => ["class" => "use-fallback-checkbox"]]);
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "useFallback", [], "any", false, false, false, 25), "vars", [], "any", false, false, false, 25), "label", [], "any", false, false, false, 25)), "html", null, true);
        echo "
            </div>
            <div class=\"entity-fallback-container__fallback\">
                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "fallback", [], "any", false, false, false, 28), 'widget', ["attr" => ["class" => "fallback fallback-select"]]);
        echo "
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroEntity/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  103 => 28,  96 => 25,  89 => 21,  85 => 20,  79 => 17,  75 => 15,  71 => 14,  64 => 10,  61 => 9,  59 => 7,  58 => 4,  55 => 3,  52 => 2,  48 => 1,  44 => 14,  41 => 13,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntity/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Resources/views/Form/fields.html.twig");
    }
}
