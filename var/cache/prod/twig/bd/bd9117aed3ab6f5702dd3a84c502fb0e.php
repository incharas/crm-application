<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/progressbar.scss */
class __TwigTemplate_412d6a5753cad9e953c5eb594f550ae4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

#progressbar {
    position: absolute;
    top: 50%;
    left: 50%;

    transform: translate(-50%, -50%);
}

.progress-bar-container {
    position: relative;
    width: \$progress-container-size;
    margin-right: auto;
    margin-left: auto;

    text-align: center;
}

.progress-bar {
    &.success {
        background-color: \$success-lighten;
    }

    &.inverse {
        background-color: \$extra;
    }

    &.info {
        background-color: \$info;
    }

    &.warning {
        background-color: \$warning;
    }
}

.progress {
    &.infinite {
        position: relative;

        height: \$progressbar-infinite-height;
        width: 100%;
        margin-top: \$progressbar-infinite-offset;

        background-color: \$progressbar-infinite-background-color;

        &::before {
            position: absolute;
            bottom: 0;
            left: 0;

            display: block;
            height: inherit;
            width: \$progressbar-infinite-line-point-size;

            background-color: \$progressbar-infinite-line-background-color;

            animation: line-point 1.5s steps(40, end) infinite;

            content: '';
        }

        .progress-bar {
            display: none;
        }
    }

    &-title {
        font-size: \$progress-font-size;
        font-weight: font-weight('light');
    }

    &-label {
        display: block;
        margin-top: -\$progress-height;

        text-align: center;
        line-height: \$progress-height;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/progressbar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/progressbar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/progressbar.scss");
    }
}
