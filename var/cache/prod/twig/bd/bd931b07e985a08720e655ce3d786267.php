<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Calendar/Menu/contextMenu.html.twig */
class __TwigTemplate_bad7f40a3b3da75170002b685467b1db extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'item' => [$this, 'block_item'],
            'linkElement' => [$this, 'block_linkElement'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroCalendar/Calendar/Menu/contextMenu.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 4), "module", [], "any", true, true, false, 4)) {
            // line 5
            echo "        ";
            $context["itemAttributes"] = twig_array_merge(($context["itemAttributes"] ?? null), ["data-module" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 5), "module", [], "any", false, false, false, 5)]);
            // line 6
            echo "    ";
        }
        // line 7
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 7), "template", [], "any", true, true, false, 7)) {
            // line 8
            echo "        ";
            echo twig_include($this->env, $context, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 8), "template", [], "any", false, false, false, 8), array(), true, true);
            echo "
    ";
        } else {
            // line 10
            echo "        ";
            $this->displayBlock("item_renderer", $context, $blocks);
            echo "
    ";
        }
    }

    // line 14
    public function block_linkElement($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroCalendar/Calendar/Menu/contextMenu.html.twig", 15)->unwrap();
        // line 16
        echo "    <a href=\"#\" role=\"button\" ";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 16, $context, $this->getSourceContext());
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Calendar/Menu/contextMenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 16,  81 => 15,  77 => 14,  69 => 10,  63 => 8,  60 => 7,  57 => 6,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Calendar/Menu/contextMenu.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Calendar/Menu/contextMenu.html.twig");
    }
}
