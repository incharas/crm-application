<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/Call/action/inviteHangoutButton.html.twig */
class __TwigTemplate_1028ee8ca801a3cc7900da15ed373d36 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'action_controll' => [$this, 'block_action_controll'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroCall/Call/activityLink.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroCall/Call/activityLink.html.twig", "@OroHangoutsCall/Call/action/inviteHangoutButton.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_action_controll($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["cssClass"] = "btn icons-holder-text";
        // line 5
        echo "
    ";
        // line 6
        if (array_key_exists("phone", $context)) {
            // line 7
            echo "        ";
            $context["hangoutOptions"] = ["invites" => [0 => ["name" => (( !twig_test_empty(            // line 9
($context["entity"] ?? null))) ? (twig_escape_filter($this->env, ($context["entity"] ?? null), "html")) : ("")), "id" => (( !twig_test_empty(            // line 10
($context["phone"] ?? null))) ? (twig_escape_filter($this->env, ($context["phone"] ?? null), "html")) : ("")), "invite_type" => "PHONE"]]];
            // line 14
            echo "    ";
        } elseif (array_key_exists("email", $context)) {
            // line 15
            echo "        ";
            $context["hangoutOptions"] = ["invites" => [0 => ["name" => (( !twig_test_empty(            // line 17
($context["entity"] ?? null))) ? (twig_escape_filter($this->env, ($context["entity"] ?? null), "html")) : ("")), "id" => (( !twig_test_empty(            // line 18
($context["email"] ?? null))) ? (twig_escape_filter($this->env, ($context["email"] ?? null), "html")) : ("")), "invite_type" => "EMAIL"]]];
            // line 22
            echo "    ";
        }
        // line 23
        echo "
    ";
        // line 24
        if ((array_key_exists("entity", $context) && $this->extensions['Oro\Bundle\CallBundle\Twig\OroCallExtension']->isCallLogApplicable(($context["entity"] ?? null)))) {
            // line 25
            echo "        ";
            $context["dataUrl"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_call_create", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(            // line 26
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 27
($context["entity"] ?? null), "id", [], "any", false, false, false, 27), "phone" => ((            // line 28
array_key_exists("phone", $context)) ? (twig_escape_filter($this->env, ($context["phone"] ?? null), "html")) : (((array_key_exists("email", $context)) ? (twig_escape_filter($this->env, ($context["email"] ?? null), "html")) : (null))))]);
            // line 30
            echo "
        ";
            // line 32
            echo "        ";
            $context["extraComponentOptions"] = ["onAppStartOptions" => ["widgetComponentOptions" => twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 34
($context["options"] ?? null), "widget", [], "any", false, false, false, 34), ["options" => twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 35
($context["options"] ?? null), "widget", [], "any", false, false, false, 35), "options", [], "any", false, false, false, 35), ["url" =>             // line 36
($context["dataUrl"] ?? null)])]), "targetComponentName" => "log-hangout-call-component"]];
            // line 42
            echo "    ";
        }
        // line 43
        echo "
    ";
        // line 44
        $this->loadTemplate("@OroHangoutsCall/inviteHangoutLink.html.twig", "@OroHangoutsCall/Call/action/inviteHangoutButton.html.twig", 44)->display($context);
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/Call/action/inviteHangoutButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 44,  97 => 43,  94 => 42,  92 => 36,  91 => 35,  90 => 34,  88 => 32,  85 => 30,  83 => 28,  82 => 27,  81 => 26,  79 => 25,  77 => 24,  74 => 23,  71 => 22,  69 => 18,  68 => 17,  66 => 15,  63 => 14,  61 => 10,  60 => 9,  58 => 7,  56 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/Call/action/inviteHangoutButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/Call/action/inviteHangoutButton.html.twig");
    }
}
