<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/numeric-range.js */
class __TwigTemplate_88ddabc490511a3deea1e32a0c4c46ba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const range = require('oroform/js/validator/range');

    // clone of range constraint
    const numericRange = range.slice();

    numericRange[0] = 'Oro\\\\Bundle\\\\ValidationBundle\\\\Validator\\\\Constraints\\\\NumericRange';

    return numericRange;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/numeric-range.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/numeric-range.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/numeric-range.js");
    }
}
