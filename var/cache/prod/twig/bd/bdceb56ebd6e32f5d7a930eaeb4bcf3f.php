<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/jquery.uniform.js */
class __TwigTemplate_1b7c3110f5cc3d802e8dc689c89e4cc8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    /**
     * Add or remove extra class in Uniform container if selected value is empty
     * @param {jQuery.Element} \$el
     * @param {jQuery.Element} \$elParent
     */
    function markIfEmpty(\$el, \$elParent) {
        if (!\$el.length || !\$elParent.length) {
            return;
        }

        \$elParent.toggleClass('uniform-empty-value', \$el[0].value.trim() === '');
    }

    /**
     * Add or remove extra class in Uniform container if an element is in \"readonly\" state
     * @param {jQuery.Element} \$el
     * @param {jQuery.Element} \$elParent
     */
    function markAsReadonly(\$el, \$elParent) {
        if (!\$el.length || !\$elParent.length) {
            return;
        }

        \$elParent.toggleClass('readonly', \$el.is('[readonly]'));
    }

    const \$ = require('jquery');
    const _ = require('underscore');

    require('jquery.uniform');

    const classList = ['selectClass', 'selectMultiClass'];
    const originalUniform = \$.fn.uniform;

    \$.fn.uniform = _.wrap(\$.fn.uniform, function(original, options) {
        if (\$(this).is('select')) {
            const config = _.extend({}, \$.uniform.defaults, options);
            const uniformParentSelectors = _.map(
                _.values(_.pick(config, classList)),
                function(selector) {
                    return '.' + selector;
                }).join(', ');

            original.call(this, config);

            return this.each(function() {
                const \$el = \$(this);
                const uniformContainer = \$el.parent(uniformParentSelectors);

                markIfEmpty(\$el, uniformContainer);
                markAsReadonly(\$el, uniformContainer);

                \$el.on('change' + config.eventNamespace, _.partial(markIfEmpty, \$el, uniformContainer));
            });
        }

        return original.call(this, options);
    });

    \$.fn.uniform.restore = originalUniform.restore;
    \$.fn.uniform.update = originalUniform.update;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/jquery.uniform.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/jquery.uniform.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/jquery.uniform.js");
    }
}
