<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReminder/Form/fields.html.twig */
class __TwigTemplate_f79158cb5bd30cd5a0389fd749bd2fc1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_reminder_collection_widget' => [$this, 'block_oro_reminder_collection_widget'],
            'oro_reminder_widget' => [$this, 'block_oro_reminder_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_reminder_collection_widget', $context, $blocks);
        // line 7
        echo "
";
        // line 8
        $this->displayBlock('oro_reminder_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_reminder_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["class"] = "reminders-collection";
        // line 3
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 3)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 3) . ($context["class"] ?? null))) : (($context["class"] ?? null)))]);
        // line 4
        echo "    ";
        $this->displayBlock("oro_collection_widget", $context, $blocks);
        echo "
    ";
        // line 5
        $context["id"] = (($context["id"] ?? null) . "_collection");
    }

    // line 8
    public function block_oro_reminder_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        <div class=\"float-holder\">
            <div class=\"fields-row\">
                <div class=\"method inline-field";
        // line 12
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "method", [], "any", false, false, false, 12), "vars", [], "any", false, false, false, 12), "errors", [], "any", false, false, false, 12)) > 0)) {
            echo " validation-error";
        }
        echo "\">
                    ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "method", [], "any", false, false, false, 13), 'widget');
        echo "
                </div>
                <div class=\"number inline-field";
        // line 15
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "interval", [], "any", false, false, false, 15), "number", [], "any", false, false, false, 15), "vars", [], "any", false, false, false, 15), "errors", [], "any", false, false, false, 15)) > 0)) {
            echo " validation-error";
        }
        echo "\">
                    ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "interval", [], "any", false, false, false, 16), "number", [], "any", false, false, false, 16), 'widget');
        echo "
                </div>
                <div class=\"unit inline-field";
        // line 18
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "interval", [], "any", false, false, false, 18), "unit", [], "any", false, false, false, 18), "vars", [], "any", false, false, false, 18), "errors", [], "any", false, false, false, 18)) > 0)) {
            echo " validation-error";
        }
        echo "\">
                    ";
        // line 19
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "interval", [], "any", false, false, false, 19), "unit", [], "any", false, false, false, 19), 'widget');
        echo "
                </div>
            </div>
        ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "method", [], "any", false, false, false, 22), 'errors');
        echo "
        ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "interval", [], "any", false, false, false, 23), "number", [], "any", false, false, false, 23), 'errors');
        echo "
        ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "interval", [], "any", false, false, false, 24), "unit", [], "any", false, false, false, 24), 'errors');
        echo "
        ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
            <input type=\"hidden\" name=\"";
        // line 26
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "full_name", [], "any", false, false, false, 26), "html", null, true);
        echo "\" disabled data-validate-element>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroReminder/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  128 => 26,  124 => 25,  120 => 24,  116 => 23,  112 => 22,  106 => 19,  100 => 18,  95 => 16,  89 => 15,  84 => 13,  78 => 12,  71 => 9,  67 => 8,  63 => 5,  58 => 4,  55 => 3,  52 => 2,  48 => 1,  44 => 8,  41 => 7,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReminder/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ReminderBundle/Resources/views/Form/fields.html.twig");
    }
}
