<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/Tag/Datagrid/Property/background_color.html.twig */
class __TwigTemplate_a1629a6d66429a51a1d75563e8f9022a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["backgroundColor"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "backgroundColor"], "method", false, false, false, 1);
        // line 2
        echo "
<span class=\"tags-container\">
        <a href=\"\"
           class=\"tags-container__tag-entry\"
           style=\"width: 45px; ";
        // line 6
        if (($context["backgroundColor"] ?? null)) {
            echo "background-color:";
            echo twig_escape_filter($this->env, ($context["backgroundColor"] ?? null), "html", null, true);
        }
        echo "\">&nbsp;</a>
</span>
";
    }

    public function getTemplateName()
    {
        return "@OroTag/Tag/Datagrid/Property/background_color.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/Tag/Datagrid/Property/background_color.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/Tag/Datagrid/Property/background_color.html.twig");
    }
}
