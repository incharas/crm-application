<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/Dotmailer/emailCampaignStats.html.twig */
class __TwigTemplate_923dabb3b2f5b575f0e5a102aee9e683 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ( !twig_test_empty(($context["campaignStats"] ?? null))) {
            // line 2
            echo "    ";
            $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroDotmailer/macros.html.twig", "@OroDotmailer/Dotmailer/emailCampaignStats.html.twig", 2)->unwrap();
            // line 3
            echo "
    <div class=\"row-fluid\">
        <h5 class=\"user-fieldset\">
            <span>";
            // line 6
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.emailcampaign.statistics.label"), "html", null, true);
            echo "</span>
        </h5>
    </div>

    <div class=\"row-fluid row-fluid-divider form-horizontal\">
        <div class=\"responsive-cell\">
            <div class=\"responsive-block\">
                <strong>";
            // line 13
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.emailcampaign.positive.label"), "html", null, true);
            echo "</strong>

                ";
            // line 15
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.emailsSent.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["campaignStats"] ?? null), "numTotalSent", [], "any", false, false, false, 17)), "oro.dotmailer.emailcampaign.emailsSent.tooltip"], 15, $context, $this->getSourceContext());
            // line 19
            echo "

                ";
            // line 21
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.totalDelivered.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 23
($context["campaignStats"] ?? null), "numTotalDelivered", [], "any", false, false, false, 23)), "oro.dotmailer.emailcampaign.totalDelivered.tooltip"], 21, $context, $this->getSourceContext());
            // line 25
            echo "
                ";
            // line 26
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.percentageDelivered.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 28
($context["campaignStats"] ?? null), "percentageDelivered", [], "any", false, false, false, 28)), "oro.dotmailer.emailcampaign.percentageDelivered.tooltip"], 26, $context, $this->getSourceContext());
            // line 30
            echo "

                <h5>";
            // line 32
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.emailcampaign.engagement.label"), "html", null, true);
            echo "</h5>
                ";
            // line 33
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.opens.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 35
($context["campaignStats"] ?? null), "numTotalOpens", [], "any", false, false, false, 35)), "oro.dotmailer.emailcampaign.opens.tooltip"], 33, $context, $this->getSourceContext());
            // line 37
            echo "
                ";
            // line 38
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.unique_opens.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 40
($context["campaignStats"] ?? null), "numTotalUniqueOpens", [], "any", false, false, false, 40)), "oro.dotmailer.emailcampaign.unique_opens.tooltip"], 38, $context, $this->getSourceContext());
            // line 42
            echo "
                ";
            // line 43
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.percentageUniqueOpens.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 45
($context["campaignStats"] ?? null), "percentageUniqueOpens", [], "any", false, false, false, 45)), "oro.dotmailer.emailcampaign.percentageUniqueOpens.tooltip"], 43, $context, $this->getSourceContext());
            // line 47
            echo "
                ";
            // line 48
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.estimatedForwards.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 50
($context["campaignStats"] ?? null), "numTotalEstimatedForwards", [], "any", false, false, false, 50)), "oro.dotmailer.emailcampaign.estimatedForwards.tooltip"], 48, $context, $this->getSourceContext());
            // line 52
            echo "

                <h5>";
            // line 54
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.emailcampaign.interaction.label"), "html", null, true);
            echo "</h5>
                ";
            // line 55
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.user_clicks.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 57
($context["campaignStats"] ?? null), "numRecipientsClicked", [], "any", false, false, false, 57)), "oro.dotmailer.emailcampaign.user_clicks.tooltip"], 55, $context, $this->getSourceContext());
            // line 59
            echo "
                ";
            // line 60
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.user_clicks_percentage.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 62
($context["campaignStats"] ?? null), "percentageUsersClicked", [], "any", false, false, false, 62)), "oro.dotmailer.emailcampaign.user_clicks_percentage.tooltip"], 60, $context, $this->getSourceContext());
            // line 64
            echo "
                ";
            // line 65
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.clicks.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 67
($context["campaignStats"] ?? null), "numTotalClicks", [], "any", false, false, false, 67)), "oro.dotmailer.emailcampaign.clicks.tooltip"], 65, $context, $this->getSourceContext());
            // line 69
            echo "
                ";
            // line 70
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.click_to_open.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 72
($context["campaignStats"] ?? null), "percentageClicksToOpens", [], "any", false, false, false, 72)), "oro.dotmailer.emailcampaign.click_to_open.tooltip"], 70, $context, $this->getSourceContext());
            // line 74
            echo "
                ";
            // line 75
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.page_views.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 77
($context["campaignStats"] ?? null), "numTotalPageViews", [], "any", false, false, false, 77)), "oro.dotmailer.emailcampaign.page_views.tooltip"], 75, $context, $this->getSourceContext());
            // line 79
            echo "
                ";
            // line 80
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.total_replies.label", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 82
($context["campaignStats"] ?? null), "numTotalReplies", [], "any", false, false, false, 82), "oro.dotmailer.emailcampaign.total_replies.tooltip"], 80, $context, $this->getSourceContext());
            // line 84
            echo "
                ";
            // line 85
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.replies_percentage.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 87
($context["campaignStats"] ?? null), "percentageReplies", [], "any", false, false, false, 87)), "oro.dotmailer.emailcampaign.replies_percentage.tooltip"], 85, $context, $this->getSourceContext());
            // line 89
            echo "

                ";
            // line 91
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.forwards.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 93
($context["campaignStats"] ?? null), "numForwards", [], "any", false, false, false, 93)), "oro.dotmailer.emailcampaign.forwards.tooltip"], 91, $context, $this->getSourceContext());
            // line 95
            echo "
            </div>
        </div>

        <div class=\"responsive-cell\">
            <div class=\"responsive-block\">
                <strong>";
            // line 101
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.emailcampaign.negative.label"), "html", null, true);
            echo "</strong>

                ";
            // line 103
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.hardBounces.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 105
($context["campaignStats"] ?? null), "numTotalHardBounces", [], "any", false, false, false, 105)), "oro.dotmailer.emailcampaign.hardBounces.tooltip"], 103, $context, $this->getSourceContext());
            // line 107
            echo "
                ";
            // line 108
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.hardBouncesPercentage.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 110
($context["campaignStats"] ?? null), "percentageHardBounces", [], "any", false, false, false, 110)), "oro.dotmailer.emailcampaign.hardBouncesPercentage.tooltip"], 108, $context, $this->getSourceContext());
            // line 112
            echo "

                ";
            // line 114
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.softBounces.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 116
($context["campaignStats"] ?? null), "numTotalSoftBounces", [], "any", false, false, false, 116)), "oro.dotmailer.emailcampaign.softBounces.tooltip"], 114, $context, $this->getSourceContext());
            // line 118
            echo "
                ";
            // line 119
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.softBouncesPercentage.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 121
($context["campaignStats"] ?? null), "percentageSoftBounces", [], "any", false, false, false, 121)), "oro.dotmailer.emailcampaign.softBouncesPercentage.tooltip"], 119, $context, $this->getSourceContext());
            // line 123
            echo "

                ";
            // line 125
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.unsubscribes.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 127
($context["campaignStats"] ?? null), "numTotalUnsubscribes", [], "any", false, false, false, 127)), "oro.dotmailer.emailcampaign.unsubscribes.tooltip"], 125, $context, $this->getSourceContext());
            // line 129
            echo "
                ";
            // line 130
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.unsubscribesPercentage.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 132
($context["campaignStats"] ?? null), "percentageUnsubscribes", [], "any", false, false, false, 132)), "oro.dotmailer.emailcampaign.unsubscribesPercentage.tooltip"], 130, $context, $this->getSourceContext());
            // line 134
            echo "

                ";
            // line 136
            echo twig_call_macro($macros["ui"], "macro_renderAttributeWithTooltip", ["oro.dotmailer.emailcampaign.isp_complaints.label", $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 138
($context["campaignStats"] ?? null), "numTotalIspComplaints", [], "any", false, false, false, 138)), "oro.dotmailer.emailcampaign.isp_complaints.tooltip"], 136, $context, $this->getSourceContext());
            // line 140
            echo "
            </div>
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/Dotmailer/emailCampaignStats.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 140,  214 => 138,  213 => 136,  209 => 134,  207 => 132,  206 => 130,  203 => 129,  201 => 127,  200 => 125,  196 => 123,  194 => 121,  193 => 119,  190 => 118,  188 => 116,  187 => 114,  183 => 112,  181 => 110,  180 => 108,  177 => 107,  175 => 105,  174 => 103,  169 => 101,  161 => 95,  159 => 93,  158 => 91,  154 => 89,  152 => 87,  151 => 85,  148 => 84,  146 => 82,  145 => 80,  142 => 79,  140 => 77,  139 => 75,  136 => 74,  134 => 72,  133 => 70,  130 => 69,  128 => 67,  127 => 65,  124 => 64,  122 => 62,  121 => 60,  118 => 59,  116 => 57,  115 => 55,  111 => 54,  107 => 52,  105 => 50,  104 => 48,  101 => 47,  99 => 45,  98 => 43,  95 => 42,  93 => 40,  92 => 38,  89 => 37,  87 => 35,  86 => 33,  82 => 32,  78 => 30,  76 => 28,  75 => 26,  72 => 25,  70 => 23,  69 => 21,  65 => 19,  63 => 17,  62 => 15,  57 => 13,  47 => 6,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/Dotmailer/emailCampaignStats.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/Dotmailer/emailCampaignStats.html.twig");
    }
}
