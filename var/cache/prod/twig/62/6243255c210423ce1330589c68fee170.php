<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/Taxonomy/widget/info.html.twig */
class __TwigTemplate_593163acd9502715859a6576aeb98ee7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTag/Taxonomy/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroTag/Taxonomy/widget/info.html.twig", 2)->unwrap();
        // line 3
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        ";
        // line 6
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["taxonomy"] ?? null), "name")) {
            // line 7
            echo "            <div class=\"responsive-block\">
                ";
            // line 8
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.taxonomy.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["taxonomy"] ?? null), "name", [], "any", false, false, false, 8)], 8, $context, $this->getSourceContext());
            echo "
            </div>
            <div class=\"responsive-block\">
                ";
            // line 11
            echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["taxonomy"] ?? null)], 11, $context, $this->getSourceContext());
            echo "
            </div>
        ";
        }
        // line 14
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroTag/Taxonomy/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 14,  57 => 11,  51 => 8,  48 => 7,  46 => 6,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/Taxonomy/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/Taxonomy/widget/info.html.twig");
    }
}
