<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validate-topmost-label-mixin.js */
class __TwigTemplate_7fd169ac6950ebf77e4934b3608ca133 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const mediator = require('oroui/js/mediator');
    const validationMessageHandlers = require('oroform/js/validation-message-handlers');

    /**
     * Mixin designed to make jquery validator error message visible over container element (e.g. dialog header)
     * @this validator
     */
    const validateTopmostLabelMixin = {
        init: function() {
            const parentDialog = \$(this.currentForm).closest('.ui-dialog');

            if (parentDialog.length === 1) {
                this.parentDialog = parentDialog;
                const dialogEvents = _.map(['dialogresize', 'dialogdrag', 'dialogreposition'], function(item) {
                    return item + '.validate';
                });
                parentDialog.on(dialogEvents.join(' '), validateTopmostLabelMixin.updateMessageHandlerViews.bind(this));
            }

            mediator.on('layout:reposition', validateTopmostLabelMixin.updateMessageHandlerViews, this);

            this.validationMessageHandlerViews = {};
        },

        showLabel: function(element, message, label) {
            if (_.has(this.validationMessageHandlerViews, element.name)) {
                this.validationMessageHandlerViews[element.name].dispose();
                delete this.validationMessageHandlerViews[element.name];
            }

            const Handler = _.find(validationMessageHandlers, function(item) {
                return item.test(element);
            });

            if (Handler) {
                this.validationMessageHandlerViews[element.name] = new Handler({
                    el: element,
                    label: label.length ? label : this.errorsFor(element)
                });
            }
        },

        validationSuccessHandler: function(element) {
            validateTopmostLabelMixin.removeHandlerByElement.call(this, element);
        },

        validationResetHandler: function(element) {
            validateTopmostLabelMixin.removeHandlerByElement.call(this, element);
        },

        removeHandlerByElement: function(element) {
            const name = element.getAttribute('name');

            if (this.validationMessageHandlerViews && name in this.validationMessageHandlerViews) {
                this.validationMessageHandlerViews[name].dispose();
                delete this.validationMessageHandlerViews[name];
            }
        },

        updateMessageHandlerViews: function() {
            _.invoke(this.validationMessageHandlerViews, 'update');
        },

        destroy: function() {
            if (this.parentDialog) {
                this.parentDialog.off('validate');
            }

            _.each(this.validationMessageHandlerViews, function(view) {
                view.dispose();
            });

            mediator.off('layout:reposition', validateTopmostLabelMixin.updateMessageHandlerViews, this);

            delete this.validationMessageHandlerViews;
        }
    };

    return validateTopmostLabelMixin;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validate-topmost-label-mixin.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validate-topmost-label-mixin.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validate-topmost-label-mixin.js");
    }
}
