<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/mobile/variables/forms.scss */
class __TwigTemplate_c90e349f7ed2da8b9d1931783a029087 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$section-content-mobile-offset-top: 0 !default;
\$section-content-mobile-inner-offset: \$content-padding-medium 0 !default;

\$row-fluid-mobile-inner-offset: 0 !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/mobile/variables/forms.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/mobile/variables/forms.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/mobile/variables/forms.scss");
    }
}
