<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSecurity/Organization/current_name.html.twig */
class __TwigTemplate_c2c1323bb5994203415f07169ca8b8e0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["current_organization"] = $this->extensions['Oro\Bundle\SecurityBundle\Twig\OroSecurityExtension']->getCurrentOrganization();
        // line 2
        if (($context["current_organization"] ?? null)) {
            // line 3
            echo "    ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["current_organization"] ?? null), "name", [], "any", false, false, false, 3), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroSecurity/Organization/current_name.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSecurity/Organization/current_name.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SecurityBundle/Resources/views/Organization/current_name.html.twig");
    }
}
