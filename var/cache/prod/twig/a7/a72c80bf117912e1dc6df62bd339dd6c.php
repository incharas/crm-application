<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNotification/MassNotification/view.html.twig */
class __TwigTemplate_209531a773064f5d1880e04ebefbbcf0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "email", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "email", [], "any", false, false, false, 3), "N/A")) : ("N/A"))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroNotification/MassNotification/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 7
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_notification_massnotification_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.massnotification.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 10
($context["entity"] ?? null), "email", [], "any", true, true, false, 10)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "email", [], "any", false, false, false, 10), "N/A")) : ("N/A"))];
        // line 12
        echo "    ";
        $this->displayBlock('stats', $context, $blocks);
        // line 14
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 12
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
    }

    // line 17
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        ob_start(function () { return ''; });
        // line 19
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_notification_massnotification_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 21
($context["entity"] ?? null), "id", [], "any", false, false, false, 21)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.massnotification.block.info")]);
        // line 23
        echo "
    ";
        $context["massNotificationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 25
        echo "
    ";
        // line 26
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 31
($context["massNotificationWidget"] ?? null)]]]]];
        // line 35
        echo "
    ";
        // line 36
        $context["id"] = "massNotificationView";
        // line 37
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 38
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroNotification/MassNotification/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 38,  105 => 37,  103 => 36,  100 => 35,  98 => 31,  97 => 26,  94 => 25,  90 => 23,  88 => 21,  86 => 19,  83 => 18,  79 => 17,  75 => 13,  71 => 12,  64 => 14,  61 => 12,  59 => 10,  58 => 7,  56 => 6,  52 => 5,  47 => 1,  45 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNotification/MassNotification/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NotificationBundle/Resources/views/MassNotification/view.html.twig");
    }
}
