<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/form-layout.scss */
class __TwigTemplate_5936017b0a9839a2d6979689c95c5a4a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.form-layout {
    margin-bottom: -\$content-padding-medium;
    min-width: \$field-width;

    &__row {
        display: inline-flex;
        align-items: flex-start;
        justify-content: space-between;
        max-width: 100%;
        padding-bottom: \$content-padding-medium;
    }

    &__part {
        display: inline-flex;
        flex-wrap: wrap;
        max-width: 100%;
        // Override Flexbox 'auto' property with introduced as the initial value of min-width
        min-width: 0;

        .fields-row-error {
            width: 100%;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/form-layout.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/form-layout.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/form-layout.scss");
    }
}
