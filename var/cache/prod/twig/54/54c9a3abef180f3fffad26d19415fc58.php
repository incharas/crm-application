<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/tabs-view.js */
class __TwigTemplate_058de75c0608c7ec11bb34888cc3cc60 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const BaseView = require('oroui/js/app/views/base/view');

    const DatePickerTabsView = BaseView.extend({
        autoRender: true,

        events: {
            'click .nav-tabs a': 'onTabSwitch'
        },

        /**
         * @inheritdoc
         */
        constructor: function DatePickerTabsView(options) {
            DatePickerTabsView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            _.extend(this, _.pick(options, ['data', 'template']));
            DatePickerTabsView.__super__.initialize.call(this, options);
        },

        /**
         * @inheritdoc
         */
        render: function() {
            const data = this.getTemplateData();
            const template = this.getTemplateFunction();
            const html = template(data);
            this.\$el.html(html);
        },

        /**
         * @inheritdoc
         * @returns {*}
         */
        getTemplateData: function() {
            return this.data;
        },

        /**
         * Handles tab switch event
         *
         * @param {jQuery.Event} e
         */
        onTabSwitch: function(e) {
            e.preventDefault();
            e.stopPropagation();
            this.\$(e.currentTarget).tab('show');
        },

        /**
         * Opens the tab by its name
         *
         * @param {string} tabName
         */
        show: function(tabName) {
            this.\$('[href^=\"#' + tabName + '-\"]').tab('show');
        },

        updateTabsVisibility: function() {
            let visibleTabShown = false;
            _.each(this.data.tabs, function(tab) {
                const visible = !_.isFunction(tab.isVisible) || tab.isVisible();
                this.setTabVisibility(tab.name, visible);

                if (visible && !visibleTabShown) {
                    this.show(tab.name);
                    visibleTabShown = true;
                }
            }, this);
        },

        /**
         * @param {String} tabName
         * @param {Boolean} visible
         */
        setTabVisibility: function(tabName, visible) {
            const method = visible ? 'show' : 'hide';
            this.\$('li:has(a.' + tabName + ')')[method]();
        }
    });

    return DatePickerTabsView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/tabs-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/tabs-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/tabs-view.js");
    }
}
