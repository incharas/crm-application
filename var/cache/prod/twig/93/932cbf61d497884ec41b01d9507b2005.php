<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/_breakpoints.scss */
class __TwigTemplate_c1971042738407ffad6b26a840ffaf40 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

// Native breakpoints;
// @Notice! These values contains in \$oro-breakpoints array will be synchronized with `viewportManager`

\$oro-breakpoints: (
    'desktop':
        '(min-width: #{\$breakpoint-desktop})',
    'tablet':
        '(max-width: #{\$breakpoint-tablet})',
    'strict-tablet':
        '(max-width: #{\$breakpoint-tablet}) and (min-width: #{\$breakpoint-tablet-small + 1})',
    'tablet-small': '(max-width: ' + \$breakpoint-tablet-small + ')',
    'strict-tablet-small':
        '(max-width: #{\$breakpoint-tablet-small}) and (min-width: #{\$breakpoint-mobile-landscape + 1})',
    'mobile-big':
        '(max-width: #{\$breakpoint-mobile-big})',
    'strict-mobile-big':
        '(max-width: #{\$breakpoint-mobile-big}) and (min-width: #{\$breakpoint-mobile-landscape + 1})',
    'mobile-landscape':
        'screen and (max-width: #{\$breakpoint-mobile-landscape})',
    'strict-mobile-landscape':
        '(max-width: #{\$breakpoint-mobile-landscape}) and (min-width: #{\$breakpoint-mobile + 1})',
    'mobile':
        '(max-width: #{\$breakpoint-mobile})',
    'print': 'print',
    // Custom breakpoints
    'product-datagrid-toolbar-narrow':
        '(max-width: 1280px)',
    'popup-gallery-tablet':
        '(max-height: 730px), (max-width: #{\$breakpoint-tablet-small})',
    'mega-menu-wide':
        '(min-width: #{\$breakpoint-mobile-big + 1})'
);

// Variable for extend native breakpoints is empty by default;
\$custom-breakpoints: () !default;

\$breakpoints: merge-breakpoints(\$oro-breakpoints, \$custom-breakpoints) !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/_breakpoints.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/_breakpoints.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/_breakpoints.scss");
    }
}
