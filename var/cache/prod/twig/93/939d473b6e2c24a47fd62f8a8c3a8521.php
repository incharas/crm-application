<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/zoom-controls.html */
class __TwigTemplate_d23ba3f443ed8b9dbb5ac88e6bf46240 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% var togglerId = _.uniqueId('dropdown-') %>
<button class=\"btn btn-icon btn-zoom-in\" title=\"<%- _.__('oro.ui.zoom.in') %>\"><span class=\"fa-search-plus\" aria-hidden=\"true\"></span></button>
<div class=\"btn-group\">
    <button id=\"<%- togglerId %>\" data-toggle=\"dropdown\" class=\"btn dropdown-toggle \"
            aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"<%- _.__('oro.ui.zoom.steps') %>\">
        <%- Math.round(zoom * 100) %>%
    </button>
    <ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\" aria-labelledby=\"<%- togglerId %>\">
        <li><a href=\"#\" role=\"button\" class=\"btn-set-zoom\" data-size=\"50\">50%</a></li>
        <li><a href=\"#\" role=\"button\" class=\"btn-set-zoom\" data-size=\"75\">75%</a></li>
        <li><a href=\"#\" role=\"button\" class=\"btn-set-zoom\" data-size=\"100\">100%</a></li>
        <li><a href=\"#\" role=\"button\" class=\"btn-set-zoom\" data-size=\"150\">150%</a></li>
        <li><a href=\"#\" role=\"button\" class=\"btn-set-zoom\" data-size=\"200\">200%</a></li>
    </ul>
</div>
<button class=\"btn btn-icon btn-zoom-out\" title=\"<%- _.__('oro.ui.zoom.out') %>\"><span class=\"fa-search-minus\" aria-hidden=\"true\"></span></button>
<button class=\"btn btn-icon btn-auto-zoom\" title=\"<%- _.__('oro.ui.zoom.auto') %>\"><span class=\"fa-expand\" aria-hidden=\"true\"></span></button>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/zoom-controls.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/zoom-controls.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/zoom-controls.html");
    }
}
