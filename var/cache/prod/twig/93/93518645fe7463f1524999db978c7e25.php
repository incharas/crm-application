<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivityContact/ActivityContact/widget/metrics.html.twig */
class __TwigTemplate_ab54ad56a1a4dd0462270d430031d10c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["entityClassName"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null), true);
        // line 2
        echo "
";
        // line 3
        $macros["metrics"] = $this->macros["metrics"] = $this;
        // line 4
        echo "
<div id=\"activity-count-";
        // line 5
        echo twig_escape_filter($this->env, ((($context["entityClassName"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5)), "html", null, true);
        echo "\" class=\"activity-contact-info-title\">
    ";
        // line 6
        echo twig_call_macro($macros["metrics"], "macro_last_contacted_time", [($context["data"] ?? null), ($context["entity"] ?? null)], 6, $context, $this->getSourceContext());
        echo "
</div>
<script>
    loadModules(['jquery', 'routing', 'oroui/js/mediator'], function (\$, routing, mediator) {
        \$(function () {
            var reloadStatistics = function () {
                \$.ajax({
                    url: routing.generate('oro_activity_contact_metrics', {
                        entityClass: '";
        // line 14
        echo twig_escape_filter($this->env, ($context["entityClassName"] ?? null), "html", null, true);
        echo "',
                        entityId: '";
        // line 15
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 15), "html", null, true);
        echo "'
                    }),
                    type: 'GET',
                    success: function (response) {
                        \$('#activity-count-' + '";
        // line 19
        echo twig_escape_filter($this->env, ((($context["entityClassName"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 19)), "html", null, true);
        echo "').html(
                            \$(response).filter('#activity-count-' + '";
        // line 20
        echo twig_escape_filter($this->env, ((($context["entityClassName"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 20)), "html", null, true);
        echo "').html()
                        );
                    }
                })
            };

            mediator.on(
                'widget:doRefresh:activity-list-widget ' +
                'widget_success:activity_list:refresh ' +
                'widget_success:activity_list:item:delete ' +
                'widget_success:activity_list:item:update',
                reloadStatistics
            );
            mediator.once('page:beforeChange', function () {
                mediator.off(
                    'widget:doRefresh:activity-list-widget ' +
                    'widget_success:activity_list:refresh ' +
                    'widget_success:activity_list:item:delete ' +
                    'widget_success:activity_list:item:update',
                    reloadStatistics
                );
            });
        });
    });
</script>

";
    }

    // line 46
    public function macro_last_contacted_time($__data__ = null, $__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "data" => $__data__,
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 47
            echo "    ";
            $context["activity_scope_class"] = "Oro\\Bundle\\ActivityContactBundle\\EntityConfig\\ActivityScope";
            // line 48
            echo "
    ";
            // line 49
            $context["last_contact_date_key"] = twig_constant((($context["activity_scope_class"] ?? null) . "::LAST_CONTACT_DATE"));
            // line 50
            echo "    ";
            $context["last_contact_date_in_key"] = twig_constant((($context["activity_scope_class"] ?? null) . "::LAST_CONTACT_DATE_IN"));
            // line 51
            echo "    ";
            $context["last_contact_date_out_key"] = twig_constant((($context["activity_scope_class"] ?? null) . "::LAST_CONTACT_DATE_OUT"));
            // line 52
            echo "    ";
            $context["contact_count_key"] = twig_constant((($context["activity_scope_class"] ?? null) . "::CONTACT_COUNT"));
            // line 53
            echo "
    ";
            // line 54
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), ($context["contact_count_key"] ?? null), [], "array", true, true, false, 54) && ((($__internal_compile_0 = ($context["data"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[($context["contact_count_key"] ?? null)] ?? null) : null) > 0))) {
                // line 55
                echo "        ";
                $context["last_datetime"] = "";
                // line 56
                echo "        ";
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["last_contact_date_key"] ?? null))) {
                    // line 57
                    echo "            ";
                    $context["last_datetime"] = _twig_default_filter($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime((($__internal_compile_1 = ($context["data"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[($context["last_contact_date_key"] ?? null)] ?? null) : null)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"));
                    // line 58
                    echo "        ";
                }
                // line 59
                echo "        ";
                $context["direction"] = (("<i class=\"fa-sign-" . ((((($__internal_compile_2 =                 // line 60
($context["data"] ?? null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2[($context["last_contact_date_in_key"] ?? null)] ?? null) : null) >= (($__internal_compile_3 = ($context["data"] ?? null)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3[($context["last_contact_date_out_key"] ?? null)] ?? null) : null))) ? ("in") : ("out"))) . "\"></i>");
                // line 63
                echo "    <ul class=\"inline\">
        ";
                // line 64
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["contact_count_key"] ?? null))) {
                    // line 65
                    echo "            <li>
            ";
                    // line 66
                    echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity_contact.info_title_times", ["{{ total_contacted }}" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 67
($context["data"] ?? null), ($context["contact_count_key"] ?? null), [], "array", true, true, false, 67)) ? (_twig_default_filter((($__internal_compile_4 = ($context["data"] ?? null)) && is_array($__internal_compile_4) || $__internal_compile_4 instanceof ArrayAccess ? ($__internal_compile_4[($context["contact_count_key"] ?? null)] ?? null) : null), 0)) : (0))]);
                    // line 68
                    echo "
            </li>
        ";
                }
                // line 71
                echo "        <li style=\"padding-right:0\">
        ";
                // line 72
                echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity_contact.info_title_last", ["{{ direction }}" =>                 // line 73
($context["direction"] ?? null), "{{ last_datetime }}" =>                 // line 74
($context["last_datetime"] ?? null)]);
                // line 75
                echo "
        </li>
    </ul>
    ";
            } else {
                // line 79
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity_contact.info_title_no_data"), "html", null, true);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroActivityContact/ActivityContact/widget/metrics.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  189 => 79,  183 => 75,  181 => 74,  180 => 73,  179 => 72,  176 => 71,  171 => 68,  169 => 67,  168 => 66,  165 => 65,  163 => 64,  160 => 63,  158 => 60,  156 => 59,  153 => 58,  150 => 57,  147 => 56,  144 => 55,  142 => 54,  139 => 53,  136 => 52,  133 => 51,  130 => 50,  128 => 49,  125 => 48,  122 => 47,  108 => 46,  77 => 20,  73 => 19,  66 => 15,  62 => 14,  51 => 6,  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivityContact/ActivityContact/widget/metrics.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ActivityContactBundle/Resources/views/ActivityContact/widget/metrics.html.twig");
    }
}
