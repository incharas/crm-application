<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Audit/data.html.twig */
class __TwigTemplate_cf985f92df1b38575f159f2442f5d1dc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<ul>
";
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["value"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["val"]) {
            // line 3
            echo "    ";
            $context["items"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config_manager"] ?? null), "getProvider", [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["val"], "scope", [], "any", false, false, false, 3)], "method", false, false, false, 3), "getPropertyConfig", [], "method", false, false, false, 3), "getItems", [0 => ((($context["is_entity"] ?? null)) ? ("entity") : ("type"))], "method", false, false, false, 3);
            // line 4
            echo "    ";
            $context["translatable"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config_manager"] ?? null), "getProvider", [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["val"], "scope", [], "any", false, false, false, 4)], "method", false, false, false, 4), "getPropertyConfig", [], "method", false, false, false, 4), "getTranslatableValues", [0 => ((($context["is_entity"] ?? null)) ? ("entity") : ("field"))], "method", false, false, false, 4);
            // line 5
            echo "
    ";
            // line 6
            if (((($context["is_entity"] ?? null) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["val"], "fieldName", [], "method", false, false, false, 6) == null)) || ((($context["is_entity"] ?? null) == false) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["val"], "fieldName", [], "method", false, false, false, 6) == ($context["field_name"] ?? null))))) {
                // line 7
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["val"], "diff", [], "any", false, false, false, 7));
                foreach ($context['_seq'] as $context["key"] => $context["data"]) {
                    // line 8
                    echo "            ";
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["items"] ?? null), $context["key"], [], "array", false, true, false, 8), "form", [], "array", false, true, false, 8), "options", [], "array", false, true, false, 8), "label", [], "array", true, true, false, 8)) {
                        // line 9
                        echo "                ";
                        $context["label"] = (($__internal_compile_0 = (($__internal_compile_1 = (($__internal_compile_2 = (($__internal_compile_3 = ($context["items"] ?? null)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3[$context["key"]] ?? null) : null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2["form"] ?? null) : null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["options"] ?? null) : null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["label"] ?? null) : null);
                        // line 10
                        echo "            ";
                    } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["items"] ?? null), $context["key"], [], "array", false, true, false, 10), "options", [], "array", false, true, false, 10), "label", [], "array", true, true, false, 10)) {
                        // line 11
                        echo "                ";
                        $context["label"] = (($__internal_compile_4 = (($__internal_compile_5 = (($__internal_compile_6 = ($context["items"] ?? null)) && is_array($__internal_compile_6) || $__internal_compile_6 instanceof ArrayAccess ? ($__internal_compile_6[$context["key"]] ?? null) : null)) && is_array($__internal_compile_5) || $__internal_compile_5 instanceof ArrayAccess ? ($__internal_compile_5["options"] ?? null) : null)) && is_array($__internal_compile_4) || $__internal_compile_4 instanceof ArrayAccess ? ($__internal_compile_4["label"] ?? null) : null);
                        // line 12
                        echo "            ";
                    } else {
                        // line 13
                        echo "                ";
                        $context["label"] = twig_replace_filter(twig_capitalize_string_filter($this->env, $context["key"]), ["_" => " "]);
                        // line 14
                        echo "            ";
                    }
                    // line 15
                    echo "
            ";
                    // line 16
                    if (!twig_in_filter((".entity_" . $context["key"]), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((($__internal_compile_7 = $context["data"]) && is_array($__internal_compile_7) || $__internal_compile_7 instanceof ArrayAccess ? ($__internal_compile_7[1] ?? null) : null)))) {
                        // line 17
                        echo "            <li>
                ";
                        // line 18
                        $context["new"] = ((twig_in_filter($context["key"], ($context["translatable"] ?? null))) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((($__internal_compile_8 = $context["data"]) && is_array($__internal_compile_8) || $__internal_compile_8 instanceof ArrayAccess ? ($__internal_compile_8[1] ?? null) : null))) : ((($__internal_compile_9 = $context["data"]) && is_array($__internal_compile_9) || $__internal_compile_9 instanceof ArrayAccess ? ($__internal_compile_9[1] ?? null) : null)));
                        // line 19
                        echo "                <b>";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
                        echo "</b>: ";
                        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->renderValue(($context["new"] ?? null)), "html", null, true);
                        echo "
            </li>
            ";
                    }
                    // line 22
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['data'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 23
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['val'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Audit/data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 25,  106 => 23,  100 => 22,  91 => 19,  89 => 18,  86 => 17,  84 => 16,  81 => 15,  78 => 14,  75 => 13,  72 => 12,  69 => 11,  66 => 10,  63 => 9,  60 => 8,  55 => 7,  53 => 6,  50 => 5,  47 => 4,  44 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Audit/data.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Audit/data.html.twig");
    }
}
