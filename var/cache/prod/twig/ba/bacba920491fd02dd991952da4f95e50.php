<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Config/update.html.twig */
class __TwigTemplate_ee1d18c1793ea66b5cf6cdddaf997f51 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/update.html.twig", 2)->unwrap();
        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), "@OroForm/Form/fields.html.twig", true);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 4), "N/A")) : ("N/A")))]]);
        // line 6
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 6)]);
        // line 8
        $context["audit_entity_class"] = twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "classname", [], "any", false, false, false, 8), ["\\" => "_"]);
        // line 9
        $context["audit_title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 9), "N/A")) : ("N/A")));
        // line 10
        $context["audit_path"] = "oro_entityconfig_audit";
        // line 11
        $context["audit_entity_id"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 11);
        // line 12
        $context["audit_show_change_history"] = true;
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEntityConfig/Config/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/update.html.twig", 15)->unwrap();
        // line 16
        echo "
    ";
        // line 17
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_entityconfig_manage")) {
            // line 18
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 18)])], 18, $context, $this->getSourceContext());
            echo "
        ";
            // line 19
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_entityconfig_view", "params" => ["id" => "\$id"]]], 19, $context, $this->getSourceContext());
            // line 23
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_entityextend_entity_create")) {
                // line 24
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_entityextend_entity_create"]], 24, $context, $this->getSourceContext()));
                // line 27
                echo "        ";
            }
            // line 28
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_entityconfig_update", "params" => ["id" => "\$id"]]], 28, $context, $this->getSourceContext()));
            // line 32
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 32, $context, $this->getSourceContext());
            echo "
    ";
        }
    }

    // line 36
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    ";
        $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.entity.plural_label"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 41
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 41)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 41), "N/A")) : ("N/A")))];
        // line 43
        echo "
    ";
        // line 44
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 47
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/update.html.twig", 48)->unwrap();
        // line 49
        echo "
    <li>";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "created", [], "any", false, false, false, 50)), "html", null, true);
        echo "</li>
    <li>";
        // line 51
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updated", [], "any", false, false, false, 51)), "html", null, true);
        echo "</li>
    ";
        // line 52
        if ((array_key_exists("link", $context) && ($context["link"] ?? null))) {
            // line 53
            echo "        <li>
            ";
            // line 54
            echo twig_call_macro($macros["UI"], "macro_link", [["path" =>             // line 55
($context["link"] ?? null), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.info.records_count.label", ["%count%" => ((            // line 56
array_key_exists("entity_count", $context)) ? (_twig_default_filter(($context["entity_count"] ?? null), 0)) : (0))])]], 54, $context, $this->getSourceContext());
            // line 57
            echo "
        </li>
    ";
        } else {
            // line 60
            echo "        <li>
            <span>";
            // line 61
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.info.records_count.label", ["%count%" => ((array_key_exists("entity_count", $context)) ? (_twig_default_filter(($context["entity_count"] ?? null), 0)) : (0))]), "html", null, true);
            echo "</span>
        </li>
    ";
        }
    }

    // line 66
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "    ";
        $context["id"] = "configentity-update";
        // line 68
        echo "    ";
        $context["dataBlocks"] = $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormDataBlocks($this->env, $context, ($context["form"] ?? null));
        // line 69
        echo "    ";
        $context["data"] = ["formErrors" => ((        // line 70
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 71
($context["dataBlocks"] ?? null)];
        // line 73
        echo "
    ";
        // line 74
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Config/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 74,  187 => 73,  185 => 71,  184 => 70,  182 => 69,  179 => 68,  176 => 67,  172 => 66,  164 => 61,  161 => 60,  156 => 57,  154 => 56,  153 => 55,  152 => 54,  149 => 53,  147 => 52,  141 => 51,  135 => 50,  132 => 49,  129 => 48,  125 => 47,  119 => 44,  116 => 43,  114 => 41,  112 => 37,  108 => 36,  100 => 32,  97 => 28,  94 => 27,  91 => 24,  88 => 23,  86 => 19,  81 => 18,  79 => 17,  76 => 16,  73 => 15,  69 => 14,  64 => 1,  62 => 12,  60 => 11,  58 => 10,  56 => 9,  54 => 8,  52 => 6,  50 => 4,  47 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Config/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Config/update.html.twig");
    }
}
