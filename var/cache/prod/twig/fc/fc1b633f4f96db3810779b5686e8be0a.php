<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/fields-groups-collection-view.js */
class __TwigTemplate_affb2910e03cf622a40cb693465c1c40 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const \$ = require('jquery');

    const FieldsGroupsCollectionView = BaseView.extend({
        PRIMARY_FILED_SELECTOR: '[name\$=\"[primary]\"]',

        events: {
            'click [name\$=\"[primary]\"]': 'onPrimaryClick',
            'change >*': 'onChangeInFiledGroup'
        },

        /**
         * @inheritdoc
         */
        constructor: function FieldsGroupsCollectionView(options) {
            FieldsGroupsCollectionView.__super__.constructor.call(this, options);
        },

        /**
         * Allows only 1 primary checkbox|radiobutton to be checked.
         * This logic convert checkbox logic to logic used in radiobutton
         *
         * @param {jQuery.Event} e
         */
        onPrimaryClick: function(e) {
            this.\$(this.PRIMARY_FILED_SELECTOR).each(function() {
                this.checked = false;
                \$(this).trigger('change');
            });
            e.target.checked = true;
            \$(e.target).trigger('change');
        },

        /**
         * Handles changes in a group of fields and marks this group as primary
         * if there's no other primary group in collection
         *
         * @param {jQuery.Event} e
         */
        onChangeInFiledGroup: function(e) {
            const \$fieldsGroup = this.\$(e.currentTarget);
            if (!this.\$(e.target).is(this.PRIMARY_FILED_SELECTOR) &&
                !this.\$(this.PRIMARY_FILED_SELECTOR + ':checked').length) {
                \$fieldsGroup.find(this.PRIMARY_FILED_SELECTOR).prop('checked', true);
            }
        }
    });

    return FieldsGroupsCollectionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/fields-groups-collection-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/fields-groups-collection-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/fields-groups-collection-view.js");
    }
}
