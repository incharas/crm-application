<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImap/Form/fields.html.twig */
class __TwigTemplate_f2093fb342ee606c9167af222e1a2e92 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_imap_configuration_check_widget' => [$this, 'block_oro_imap_configuration_check_widget'],
            'oro_imap_choice_account_type_widget' => [$this, 'block_oro_imap_choice_account_type_widget'],
            'oro_imap_configuration_gmail_widget' => [$this, 'block_oro_imap_configuration_gmail_widget'],
            'oro_config_google_imap_sync_checkbox_widget' => [$this, 'block_oro_config_google_imap_sync_checkbox_widget'],
            'oro_imap_configuration_microsoft_widget' => [$this, 'block_oro_imap_configuration_microsoft_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_imap_configuration_check_widget', $context, $blocks);
        // line 35
        echo "
";
        // line 36
        $this->displayBlock('oro_imap_choice_account_type_widget', $context, $blocks);
        // line 69
        echo "
";
        // line 70
        $this->displayBlock('oro_imap_configuration_gmail_widget', $context, $blocks);
        // line 143
        echo "
";
        // line 144
        $this->displayBlock('oro_config_google_imap_sync_checkbox_widget', $context, $blocks);
        // line 168
        echo "


";
        // line 171
        $this->displayBlock('oro_imap_configuration_microsoft_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_imap_configuration_check_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 2), "parent", [], "any", false, false, false, 2), "parent", [], "any", false, false, false, 2))) {
            // line 3
            echo "        ";
            $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 3), "parent", [], "any", false, false, false, 3), "parent", [], "any", false, false, false, 3), "vars", [], "any", false, false, false, 3), "value", [], "any", false, false, false, 3);
            // line 4
            echo "    ";
        } else {
            // line 5
            echo "        ";
            $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 5), "parent", [], "any", false, false, false, 5), "vars", [], "any", false, false, false, 5), "value", [], "any", false, false, false, 5);
            // line 6
            echo "    ";
        }
        // line 7
        echo "    ";
        if ((($context["data"] ?? null) && twig_in_filter("oro_email_mailbox", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 7), "parent", [], "any", false, false, false, 7), "vars", [], "any", false, false, false, 7), "full_name", [], "any", false, false, false, 7)))) {
            // line 8
            echo "        ";
            $context["forEntity"] = "mailbox";
            // line 9
            echo "    ";
        } else {
            // line 10
            echo "        ";
            $context["forEntity"] = "user";
            // line 11
            echo "    ";
        }
        // line 12
        echo "    ";
        $context["options"] = twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 12), "options", [], "any", true, true, false, 12)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 12), "options", [], "any", false, false, false, 12), [])) : ([])), ["elementNamePrototype" =>         // line 13
($context["full_name"] ?? null), "id" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 14
($context["form"] ?? null), "parent", [], "any", false, false, false, 14), "vars", [], "any", false, false, false, 14), "value", [], "any", false, false, false, 14) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 14), "vars", [], "any", false, true, false, 14), "value", [], "any", false, true, false, 14), "id", [], "any", true, true, false, 14))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 14), "vars", [], "any", false, false, false, 14), "value", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14)) : (null)), "forEntity" =>         // line 15
($context["forEntity"] ?? null), "organization" => ((((        // line 16
($context["data"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", true, true, false, 16)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 16))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16)) : (null)), "parentElementSelector" => "fieldset"]);
        // line 19
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["data-role" => "check-connection-btn", "data-page-component-module" => "oroimap/js/app/components/check-connection-component", "data-page-component-options" => json_encode(        // line 22
($context["options"] ?? null))]);
        // line 24
        echo "    <div class=\"control-group\">
        <div class=\"controls\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ($context["attr"] ?? null)]);
        echo "
            <div class=\"check-connection-messages\"></div>
        </div>
    </div>
    <div class=\"container-config-group\"
         data-page-component-module=\"oroimap/js/app/components/check-config-settings\"
         data-page-component-options=\"\">
    </div>
";
    }

    // line 36
    public function block_oro_imap_choice_account_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    ";
        $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 37), "vars", [], "any", false, false, false, 37), "value", [], "any", false, false, false, 37);
        // line 38
        echo "
    ";
        // line 39
        $context["options"] = twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 39), "options", [], "any", true, true, false, 39)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 39), "options", [], "any", false, false, false, 39), [])) : ([])), ["route" => "oro_imap_change_account_type", "formSelector" => ("#" . ((        // line 41
array_key_exists("formSelector", $context)) ? (_twig_default_filter(($context["formSelector"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 41), "id", [], "any", false, false, false, 41))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 41), "id", [], "any", false, false, false, 41)))), "formParentName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 42
($context["form"] ?? null), "parent", [], "any", false, false, false, 42), "vars", [], "any", false, false, false, 42), "name", [], "any", false, false, false, 42), "organization" => ((((        // line 43
($context["data"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", true, true, false, 43)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 43))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 43), "id", [], "any", false, false, false, 43)) : (null)), "id" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 44
($context["form"] ?? null), "vars", [], "any", false, false, false, 44), "value", [], "any", false, false, false, 44) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 44), "value", [], "any", false, true, false, 44), "userEmailOrigin", [], "any", false, true, false, 44), "id", [], "any", true, true, false, 44))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 44), "value", [], "any", false, false, false, 44), "userEmailOrigin", [], "any", false, false, false, 44), "id", [], "any", false, false, false, 44)) : (null))]);
        // line 46
        echo "
    <div class=\"container-change-account-type\"
        data-page-component-module=\"oroimap/js/app/components/account-type-component\"
        data-page-component-options=\"";
        // line 49
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
        data-layout=\"separate\"
    >
        <div ";
        // line 52
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 53
        if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 53))) {
            // line 54
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        }
        // line 57
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "accountType", [], "any", true, true, false, 57)) {
            // line 58
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "accountType", [], "any", false, false, false, 58), 'row');
            echo "
            ";
        }
        // line 60
        echo "
            ";
        // line 61
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "userEmailOrigin", [], "any", true, true, false, 61)) {
            // line 62
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "userEmailOrigin", [], "any", false, false, false, 62), 'widget');
            echo "
            ";
        }
        // line 65
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        // line 66
        echo "</div>
    </div>
";
    }

    // line 70
    public function block_oro_imap_configuration_gmail_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "    ";
        $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 71), "parent", [], "any", false, false, false, 71), "vars", [], "any", false, false, false, 71), "value", [], "any", false, false, false, 71);
        // line 72
        echo "
    ";
        // line 73
        $context["options"] = twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 73), "options", [], "any", true, true, false, 73)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 73), "options", [], "any", false, false, false, 73), [])) : ([])), ["route" => "oro_imap_change_account_type", "routeAccessToken" => "oro_imap_gmail_access_token", "routeGetFolders" => "oro_imap_gmail_connection_check", "formSelector" => ("#" . ((        // line 77
array_key_exists("formSelector", $context)) ? (_twig_default_filter(($context["formSelector"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 77), "id", [], "any", false, false, false, 77))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 77), "id", [], "any", false, false, false, 77)))), "vendorErrorMessage" => ".google-alert", "formParentName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 79
($context["form"] ?? null), "parent", [], "any", false, false, false, 79), "parent", [], "any", false, false, false, 79), "vars", [], "any", false, false, false, 79), "name", [], "any", false, false, false, 79), "organization" => ((((        // line 80
($context["data"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", true, true, false, 80)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 80))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 80), "id", [], "any", false, false, false, 80)) : (null)), "user" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 81
($context["form"] ?? null), "parent", [], "any", false, false, false, 81), "userEmailOrigin", [], "any", false, false, false, 81), "user", [], "any", false, false, false, 81), "vars", [], "any", false, false, false, 81), "value", [], "any", false, false, false, 81), "accessToken" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 82
($context["form"] ?? null), "parent", [], "any", false, false, false, 82), "userEmailOrigin", [], "any", false, false, false, 82), "accessToken", [], "any", false, false, false, 82), "vars", [], "any", false, false, false, 82), "value", [], "any", false, false, false, 82), "accessTokenExpiresAt" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 83
($context["form"] ?? null), "parent", [], "any", false, false, false, 83), "userEmailOrigin", [], "any", false, false, false, 83), "accessTokenExpiresAt", [], "any", false, false, false, 83), "vars", [], "any", false, false, false, 83), "value", [], "any", false, false, false, 83), "id" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 84
($context["form"] ?? null), "vars", [], "any", false, false, false, 84), "value", [], "any", false, false, false, 84) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 84), "value", [], "any", false, true, false, 84), "id", [], "any", true, true, false, 84))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 84), "value", [], "any", false, false, false, 84), "id", [], "any", false, false, false, 84)) : (null))]);
        // line 86
        echo "
    <div class=\"container-imap-gmail-container\"
         data-page-component-module=\"oroimap/js/app/components/imap-gmail-component\"
         data-page-component-options=\"";
        // line 89
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
         ";
        // line 90
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 90), "is_partial", [], "any", false, false, false, 90)) {
            // line 91
            echo "            data-layout=\"separate\"
         ";
        }
        // line 93
        echo "    >
        <div ";
        // line 94
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 95
        if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 95))) {
            // line 96
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        }
        // line 99
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 99), "userEmailOrigin", [], "any", false, false, false, 99), "user", [], "any", false, false, false, 99), "vars", [], "any", false, false, false, 99), "value", [], "any", false, false, false, 99))) {
            // line 100
            echo "                <div class=\"control-group\">
                    <div class=\"control-label wrap\">
                        ";
            // line 102
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.configuration.reset_email.label"), "html", null, true);
            echo "
                    </div>
                    <div class=\"controls oro-item-collection\">
                        <div class=\"controls-line-group\">
                            <strong>";
            // line 106
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 106), "userEmailOrigin", [], "any", false, false, false, 106), "user", [], "any", false, false, false, 106), "vars", [], "any", false, false, false, 106), "value", [], "any", false, false, false, 106), "html", null, true);
            echo "</strong>
                            <button data-role=\"remove\" class=\"btn btn-icon btn-square-lighter delete\" type=\"button\"
                                    title=\"";
            // line 108
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.configuration.disconnect.label"), "html", null, true);
            echo "\"
                            >
                                <span class=\"fa-close\" aria-hidden=\"true\"></span>
                            </button>
                        </div>
                    </div>
                </div>
            ";
        }
        // line 116
        echo "
            ";
        // line 117
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "check", [], "any", true, true, false, 117)) {
            // line 118
            echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
            // line 120
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "check", [], "any", false, false, false, 120), 'widget');
            echo "
                    </div>
                </div>
            ";
        }
        // line 124
        echo "
            <div class=\"control-group\">
                <div class=\"controls\">
                    <div class=\"google-alert google-connection-status alert alert-error\" role=\"alert\" style=\"display: none\"></div>
                </div>
            </div>

            ";
        // line 131
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "checkFolder", [], "any", true, true, false, 131)) {
            // line 132
            echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
            // line 134
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "checkFolder", [], "any", false, false, false, 134), 'widget');
            echo "
                    </div>
                </div>
            ";
        }
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        // line 140
        echo "</div>
    </div>
";
    }

    // line 144
    public function block_oro_config_google_imap_sync_checkbox_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 145
        echo "    ";
        $context["options"] = ["errorMessage" => ".default-alert", "successMessage" => ".alert-success", "googleErrorMessage" => ".google-alert", "googleWarningMessage" => ".alert-warning"];
        // line 151
        echo "    <div data-page-component-module=\"oroimap/js/app/components/google-sync-checkbox-component\"
        data-page-component-options=\"";
        // line 152
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\">
        ";
        // line 153
        $this->displayBlock("checkbox_widget", $context, $blocks);
        echo "
        <div class=\"alert google-connection-status alert-warning\" role=\"alert\" style=\"display: none\">
            ";
        // line 155
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.system_configuration.fields.enable_google_imap.warning.label"), "html", null, true);
        echo "
        </div>
        <div class=\"google-alert google-connection-status alert alert-error\" role=\"alert\" style=\"display: none\">

        </div>
        <div class=\"default-alert google-connection-status alert alert-error\" role=\"alert\" style=\"display: none\">
            ";
        // line 161
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.system_configuration.fields.enable_google_imap.error.label"), "html", null, true);
        echo "
        </div>
        <div class=\"alert google-connection-status alert-success\" role=\"alert\" style=\"display: none\">
            ";
        // line 164
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.system_configuration.fields.enable_google_imap.success.label"), "html", null, true);
        echo "
        </div>
    </div>
";
    }

    // line 171
    public function block_oro_imap_configuration_microsoft_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 172
        echo "    ";
        $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 172), "parent", [], "any", false, false, false, 172), "vars", [], "any", false, false, false, 172), "value", [], "any", false, false, false, 172);
        // line 173
        echo "
    ";
        // line 174
        $context["options"] = twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 174), "options", [], "any", true, true, false, 174)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 174), "options", [], "any", false, false, false, 174), [])) : ([])), ["route" => "oro_imap_change_account_type", "routeAccessToken" => "oro_imap_microsoft_access_token", "routeGetFolders" => "oro_imap_microsoft_connection_check", "formSelector" => ("#" . ((        // line 178
array_key_exists("formSelector", $context)) ? (_twig_default_filter(($context["formSelector"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 178), "id", [], "any", false, false, false, 178))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 178), "id", [], "any", false, false, false, 178)))), "vendorErrorMessage" => ".vendor-alert", "formParentName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 180
($context["form"] ?? null), "parent", [], "any", false, false, false, 180), "parent", [], "any", false, false, false, 180), "vars", [], "any", false, false, false, 180), "name", [], "any", false, false, false, 180), "organization" => ((((        // line 181
($context["data"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", true, true, false, 181)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 181))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "organization", [], "any", false, false, false, 181), "id", [], "any", false, false, false, 181)) : (null)), "user" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 182
($context["form"] ?? null), "parent", [], "any", false, false, false, 182), "userEmailOrigin", [], "any", false, false, false, 182), "user", [], "any", false, false, false, 182), "vars", [], "any", false, false, false, 182), "value", [], "any", false, false, false, 182), "accessToken" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 183
($context["form"] ?? null), "parent", [], "any", false, false, false, 183), "userEmailOrigin", [], "any", false, false, false, 183), "accessToken", [], "any", false, false, false, 183), "vars", [], "any", false, false, false, 183), "value", [], "any", false, false, false, 183), "accessTokenExpiresAt" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 184
($context["form"] ?? null), "parent", [], "any", false, false, false, 184), "userEmailOrigin", [], "any", false, false, false, 184), "accessTokenExpiresAt", [], "any", false, false, false, 184), "vars", [], "any", false, false, false, 184), "value", [], "any", false, false, false, 184), "id" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 185
($context["form"] ?? null), "vars", [], "any", false, false, false, 185), "value", [], "any", false, false, false, 185) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 185), "value", [], "any", false, true, false, 185), "id", [], "any", true, true, false, 185))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 185), "value", [], "any", false, false, false, 185), "id", [], "any", false, false, false, 185)) : (null))]);
        // line 187
        echo "
    <div class=\"container-imap-microsoft-container\"
         data-page-component-module=\"oroimap/js/app/components/imap-microsoft-component\"
         data-page-component-options=\"";
        // line 190
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
            ";
        // line 191
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 191), "is_partial", [], "any", false, false, false, 191)) {
            // line 192
            echo "                data-layout=\"separate\"
            ";
        }
        // line 194
        echo "    >
        <div ";
        // line 195
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">";
        // line 196
        if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 196))) {
            // line 197
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        }
        // line 200
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 200), "userEmailOrigin", [], "any", false, false, false, 200), "user", [], "any", false, false, false, 200), "vars", [], "any", false, false, false, 200), "value", [], "any", false, false, false, 200))) {
            // line 201
            echo "                <div class=\"control-group\">
                    <div class=\"control-label wrap\">
                        ";
            // line 203
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.configuration.reset_email.label"), "html", null, true);
            echo "
                    </div>
                    <div class=\"controls oro-item-collection\">
                        <div class=\"controls-line-group\">
                            <strong>";
            // line 207
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 207), "userEmailOrigin", [], "any", false, false, false, 207), "user", [], "any", false, false, false, 207), "vars", [], "any", false, false, false, 207), "value", [], "any", false, false, false, 207), "html", null, true);
            echo "</strong>
                            <button data-role=\"remove\" class=\"btn btn-icon btn-square-lighter delete\" type=\"button\"
                                    title=\"";
            // line 209
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.configuration.disconnect.label"), "html", null, true);
            echo "\"
                            >
                                <span class=\"fa-close\" aria-hidden=\"true\"></span>
                            </button>
                        </div>
                    </div>
                </div>
            ";
        }
        // line 217
        echo "
            ";
        // line 218
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "check", [], "any", true, true, false, 218)) {
            // line 219
            echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
            // line 221
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "check", [], "any", false, false, false, 221), 'widget');
            echo "
                        ";
            // line 222
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 222), "value", [], "any", false, false, false, 222), "isSyncEnabled", [], "method", false, false, false, 222) == false)) {
                // line 223
                echo "                            <div class=\"controls messages\">
                                <div class=\"alert alert-error\" role=\"alert\">
                                    ";
                // line 225
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.imap.configuration.reconnect_description"), "html", null, true);
                echo "
                                </div>
                            </div>
                        ";
            }
            // line 229
            echo "                    </div>
                </div>
            ";
        }
        // line 232
        echo "
            <div class=\"control-group\">
                <div class=\"controls\">
                    <div class=\"vendor-alert google-connection-status alert alert-error\" role=\"alert\" style=\"display: none\"></div>
                </div>
            </div>

            ";
        // line 239
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "checkFolder", [], "any", true, true, false, 239)) {
            // line 240
            echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
            // line 242
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "checkFolder", [], "any", false, false, false, 242), 'widget');
            echo "
                    </div>
                </div>
            ";
        }
        // line 247
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        // line 248
        echo "</div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroImap/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  483 => 248,  481 => 247,  474 => 242,  470 => 240,  468 => 239,  459 => 232,  454 => 229,  447 => 225,  443 => 223,  441 => 222,  437 => 221,  433 => 219,  431 => 218,  428 => 217,  417 => 209,  412 => 207,  405 => 203,  401 => 201,  399 => 200,  396 => 197,  394 => 196,  391 => 195,  388 => 194,  384 => 192,  382 => 191,  378 => 190,  373 => 187,  371 => 185,  370 => 184,  369 => 183,  368 => 182,  367 => 181,  366 => 180,  365 => 178,  364 => 174,  361 => 173,  358 => 172,  354 => 171,  346 => 164,  340 => 161,  331 => 155,  326 => 153,  322 => 152,  319 => 151,  316 => 145,  312 => 144,  306 => 140,  304 => 139,  297 => 134,  293 => 132,  291 => 131,  282 => 124,  275 => 120,  271 => 118,  269 => 117,  266 => 116,  255 => 108,  250 => 106,  243 => 102,  239 => 100,  237 => 99,  234 => 96,  232 => 95,  229 => 94,  226 => 93,  222 => 91,  220 => 90,  216 => 89,  211 => 86,  209 => 84,  208 => 83,  207 => 82,  206 => 81,  205 => 80,  204 => 79,  203 => 77,  202 => 73,  199 => 72,  196 => 71,  192 => 70,  186 => 66,  184 => 65,  178 => 62,  176 => 61,  173 => 60,  167 => 58,  165 => 57,  162 => 54,  160 => 53,  157 => 52,  151 => 49,  146 => 46,  144 => 44,  143 => 43,  142 => 42,  141 => 41,  140 => 39,  137 => 38,  134 => 37,  130 => 36,  117 => 26,  113 => 24,  111 => 22,  109 => 19,  107 => 16,  106 => 15,  105 => 14,  104 => 13,  102 => 12,  99 => 11,  96 => 10,  93 => 9,  90 => 8,  87 => 7,  84 => 6,  81 => 5,  78 => 4,  75 => 3,  72 => 2,  68 => 1,  64 => 171,  59 => 168,  57 => 144,  54 => 143,  52 => 70,  49 => 69,  47 => 36,  44 => 35,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImap/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImapBundle/Resources/views/Form/fields.html.twig");
    }
}
