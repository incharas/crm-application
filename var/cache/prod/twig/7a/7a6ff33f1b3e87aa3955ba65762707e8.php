<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/scrollspy.scss */
class __TwigTemplate_364201eaca6693a8f87db4ce970e61ef extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.scrollspy {
    position: \$scrollspy-position;

    overflow: \$scrollspy-overflow;

    &-nav {
        margin: \$scrollspy-nav-offset;

        background: \$scrollspy-nav-background;
        border-radius: \$scrollspy-nav-border-radius;

        position: \$scrollspy-nav-position;
        z-index: \$scrollspy-nav-z-index;

        @include clearfix();

        &::after {
            content: '';
            position: absolute;
            pointer-events: none;
            bottom: -\$scrollspy-nav-gradient-height;
            left: 0;
            right: 0;
            height: \$scrollspy-nav-gradient-height;
            background: \$scrollspy-nav-gradient;
        }

        &-target {
            height: \$scrollspy-nav-target-height;
        }

        a,
        .nav-link {
            display: inline-block;
            padding: \$scrollspy-nav-navbar-inner-nav-link-inner-offset;

            background-color: \$scrollspy-nav-navbar-inner-nav-link-background-color;
            border-radius: \$scrollspy-nav-navbar-inner-nav-link-border-radius;
            color: \$scrollspy-nav-navbar-inner-nav-link-color;

            &:hover {
                background: \$scrollspy-nav-navbar-inner-nav-link-background-color;
            }

            &.active {
                font-weight: \$scrollspy-nav-navbar-inner-nav-link-active-font-weight;

                background-color: \$scrollspy-nav-navbar-inner-nav-link-active-background-color;
            }
        }
    }

    &-title {
        padding: \$scrollspy-title-inner-offset;
        margin: \$scrollspy-title-offset;

        font-size: \$scrollspy-title-font-size;
        font-weight: \$scrollspy-title-font-weight;

        background: \$scrollspy-title-background;
        border-radius: \$scrollspy-title-border-radius;
        color: \$scrollspy-title-color;

        @at-root .scrollable-container .responsive-section:first-child .scrollspy-title {
            display: none;
        }
    }
}

[data-scroll-focus] {
    outline: \$scrollspy-data-scroll-focus-outline;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/scrollspy.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/scrollspy.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/scrollspy.scss");
    }
}
