<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityExtend/Datagrid/Property/multiEnum.html.twig */
class __TwigTemplate_ccc4b30782550f0a0f5052299307a8bd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ( !twig_test_empty(($context["value"] ?? null))) {
            // line 2
            echo "<ul class=\"list-unstyled options\">
    ";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->extensions['Oro\Bundle\EntityExtendBundle\Twig\EnumExtension']->sortEnum(($context["value"] ?? null), ($context["entity_class"] ?? null)));
            foreach ($context['_seq'] as $context["_key"] => $context["id"]) {
                // line 4
                echo "    <li>";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityExtendBundle\Twig\EnumExtension']->transEnum($context["id"], ($context["entity_class"] ?? null)), "html", null, true);
                echo "</li>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['id'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 6
            echo "</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityExtend/Datagrid/Property/multiEnum.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 6,  46 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityExtend/Datagrid/Property/multiEnum.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityExtendBundle/Resources/views/Datagrid/Property/multiEnum.html.twig");
    }
}
