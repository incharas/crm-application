<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/zoom-controls-view.js */
class __TwigTemplate_66a45f541cc5da1a1ad74a3bd9b31d69 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const \$ = require('jquery');

    const ZoomControlsView = BaseView.extend({
        autoRender: true,
        template: require('tpl-loader!../../../templates/zoom-controls.html'),

        events: {
            'click .btn-zoom-in': 'onZoomInClick',
            'click .btn-zoom-out': 'onZoomOutClick',
            'click .btn-auto-zoom': 'onAutoZoomClick',
            'click .btn-set-zoom': 'onSetZoomClick'
        },

        listen: {
            'change model': 'render'
        },

        /**
         * @inheritdoc
         */
        constructor: function ZoomControlsView(options) {
            ZoomControlsView.__super__.constructor.call(this, options);
        },

        onZoomInClick: function(e) {
            e.preventDefault();
            this.model.zoomIn();
        },

        onZoomOutClick: function(e) {
            e.preventDefault();
            this.model.zoomOut();
        },

        onAutoZoomClick: function(e) {
            e.preventDefault();
            this.model.autoZoom();
        },

        onSetZoomClick: function(e) {
            e.preventDefault();
            this.model.setZoom(parseFloat(\$(e.currentTarget).attr('data-size')) / 100);
        }
    });

    return ZoomControlsView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/zoom-controls-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/zoom-controls-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/zoom-controls-view.js");
    }
}
