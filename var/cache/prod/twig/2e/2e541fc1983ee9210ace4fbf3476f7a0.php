<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/disable-search-styles.scss */
class __TwigTemplate_bc4ec8ad59cf047be234ee31636e2747 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

// Disable platform styles for input search
@mixin disable-search-styles(\$input) {
    #{\$input}::-webkit-search-decoration,
    #{\$input}::-webkit-search-cancel-button,
    #{\$input}::-webkit-search-results-button,
    #{\$input}::-webkit-search-results-decoration {
        display: none;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/disable-search-styles.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/disable-search-styles.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/disable-search-styles.scss");
    }
}
