<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/forms.scss */
class __TwigTemplate_a835fefe7944f5e2d3855ff834b278cb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$form-horizontal-control-label-width: 182px !default;
\$form-horizontal-control-label-offset: 6px !default;
\$form-horizontal-control-label-line-height: 20px !default;
\$form-horizontal-control-html-content-offset: 5px !default;
\$form-horizontal-widget-control-label-width: auto !default;

\$form-horizontal-control-margin-start: 24px !default;
\$form-container-control-row-space: \$content-padding-medium !default;

\$form-container-column-max-width: 480px !default;
\$form-container-column-offset-right: 20px !default;

\$form-container-control-group-max-width: 800px !default;

\$modal-form-horizontal-control-label-width: 140px !default;
\$modal-form-horizontal-controls-input-width: 100% !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/forms.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/forms.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/forms.scss");
    }
}
