<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Forward/parentBody.html.twig */
class __TwigTemplate_9670f78fa62f62817c20fb4adc10939b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/Forward/parentBody.html.twig", 1)->unwrap();
        // line 2
        echo "
<body> ";
        // line 4
        echo "<br><br>
<div class=\"quote\">
    <div>
        <p>---------- ";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forwarded_message"), "html", null, true);
        echo " ----------</p>
        <p>";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("From"), "html", null, true);
        echo ":
            ";
        // line 9
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromEmailAddress", [], "any", false, false, false, 9), "owner", [], "any", false, false, false, 9))) {
            // line 10
            echo "                ";
            echo twig_call_macro($macros["EA"], "macro_email_address_link", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromEmailAddress", [], "any", false, false, false, 10)], 10, $context, $this->getSourceContext());
            echo "
            ";
        } else {
            // line 12
            echo "                ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromName", [], "any", false, false, false, 12), "html", null, true);
            echo "
            ";
        }
        // line 14
        echo "        </p>
        <p>";
        // line 15
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.sent_at.label"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "sentAt", [], "any", false, false, false, 15)), "html", null, true);
        echo "</p>
        <p>";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Subject"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "subject", [], "any", false, false, false, 16), "html", null, true);
        echo "</p>
        <p>";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("To"), "html", null, true);
        echo ": <bdo dir=\"ltr\">";
        echo twig_call_macro($macros["EA"], "macro_recipient_email_addresses", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "toCc", [], "any", false, false, false, 17), false, false], 17, $context, $this->getSourceContext());
        echo "</bdo></p>
    </div>
    <br>
    <div class=\"email-prev-body\">
        ";
        // line 21
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 21)) {
            // line 22
            echo "            ";
            echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 22), "bodyContent", [], "any", false, false, false, 22);
            echo "
        ";
        }
        // line 24
        echo "    </div>
</div>
</body>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Forward/parentBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 24,  95 => 22,  93 => 21,  84 => 17,  78 => 16,  72 => 15,  69 => 14,  63 => 12,  57 => 10,  55 => 9,  51 => 8,  47 => 7,  42 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Forward/parentBody.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Forward/parentBody.html.twig");
    }
}
