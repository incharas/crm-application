<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroGoogleIntegration/oauthLogin.html.twig */
class __TwigTemplate_476223e4c5af6bb446d54f8ee2e64893 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->extensions['HWI\Bundle\OAuthBundle\Twig\Extension\OAuthExtension']->getAuthorizationUrl("google"), "html", null, true);
        echo "\" class=\"btn btn-primary btn-large btn-uppercase btn-block btn-brand-google\">
    ";
        // line 2
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.google_integration.sso.login_button.label"), "html", null, true);
        echo "
</a>
";
    }

    public function getTemplateName()
    {
        return "@OroGoogleIntegration/oauthLogin.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroGoogleIntegration/oauthLogin.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/GoogleIntegrationBundle/Resources/views/oauthLogin.html.twig");
    }
}
