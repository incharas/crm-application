<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSecurity/Organization/selector.html.twig */
class __TwigTemplate_56728305718150cfb69edc5e275ced9f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSecurity/Organization/selector.html.twig", 1)->unwrap();
        // line 2
        $macros["organizationSelector"] = $this->macros["organizationSelector"] = $this;
        // line 3
        echo "
";
        // line 4
        $context["curr_organization"] = $this->extensions['Oro\Bundle\SecurityBundle\Twig\OroSecurityExtension']->getCurrentOrganization();
        // line 5
        $context["organizations"] = $this->extensions['Oro\Bundle\SecurityBundle\Twig\OroSecurityExtension']->getOrganizations();
        // line 6
        echo "
";
        // line 7
        ob_start(function () { return ''; });
        // line 8
        ob_start(function () { return ''; });
        // line 9
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("organization_name", $context)) ? (_twig_default_filter(($context["organization_name"] ?? null), "organization_name")) : ("organization_name")), array());
        $___internal_parse_40_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 8
        echo twig_spaceless($___internal_parse_40_);
        $context["organization_name"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 12
        echo "
";
        // line 13
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 14
            echo "    ";
            if ((twig_length_filter($this->env, ($context["organizations"] ?? null)) > 1)) {
                // line 15
                echo "        <div class=\"nav logo-wrapper dropdown\"
            ";
                // line 16
                echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orosecurity/js/app/components/switch-organization-component", "options" => ["currentOrganizationId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 19
($context["curr_organization"] ?? null), "getId", [], "method", false, false, false, 19)]]], 16, $context, $this->getSourceContext());
                // line 21
                echo "
        >
            ";
                // line 23
                echo twig_call_macro($macros["UI"], "macro_app_logo", [($context["organization_name"] ?? null)], 23, $context, $this->getSourceContext());
                echo "
            ";
                // line 24
                echo twig_call_macro($macros["organizationSelector"], "macro_organization_name_and_logo", [($context["organization_name"] ?? null)], 24, $context, $this->getSourceContext());
                echo "
            ";
                // line 25
                $context["togglerId"] = uniqid("dropdown-");
                // line 26
                echo "            <span id=\"";
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\" class=\"dropdown-toggle btn-organization-switcher\" data-toggle=\"dropdown\"
                  role=\"button\" aria-label=\"";
                // line 27
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.security.organization.switch"), "html", null, true);
                echo "\" aria-haspopup=\"true\" aria-expanded=\"false\"></span>
            <ul class=\"dropdown-menu dropdown-organization-switcher\" role=\"menu\" aria-labelledby=\"";
                // line 28
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\">
                ";
                // line 29
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["organizations"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["organization"]) {
                    // line 30
                    echo "                    <li>
                        ";
                    // line 31
                    if ((($context["curr_organization"] ?? null) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["curr_organization"] ?? null), "getId", [], "method", false, false, false, 31) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["organization"], "id", [], "any", false, false, false, 31)))) {
                        // line 32
                        echo "                        <span class=\"dropdown-item-text selected\" aria-selected=\"true\"><b>";
                        echo twig_escape_filter($this->env, twig_trim_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["organization"], "name", [], "any", false, false, false, 32)), "html", null, true);
                        echo "</b></span>
                        ";
                    } else {
                        // line 34
                        echo "                        <a href=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_security_switch_organization", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["organization"], "id", [], "any", false, false, false, 34)]), "html", null, true);
                        echo "\"
                           class=\"dropdown-item organization-switcher no-hash\">";
                        // line 36
                        echo twig_escape_filter($this->env, twig_trim_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["organization"], "name", [], "any", false, false, false, 36)), "html", null, true);
                        // line 37
                        echo "</a>
                        ";
                    }
                    // line 39
                    echo "                    </li>
                    ";
                    // line 40
                    if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 40)) {
                        // line 41
                        echo "                    <li>
                        <div class=\"dropdown-divider\"></div>
                    </li>
                    ";
                    }
                    // line 45
                    echo "                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['organization'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 46
                echo "            </ul>
        </div>
    ";
            } else {
                // line 49
                echo "        <div class=\"nav logo-wrapper\">
            ";
                // line 50
                echo twig_call_macro($macros["UI"], "macro_app_logo", [($context["organization_name"] ?? null)], 50, $context, $this->getSourceContext());
                echo "
            ";
                // line 51
                echo twig_call_macro($macros["organizationSelector"], "macro_organization_name_and_logo", [($context["organization_name"] ?? null)], 51, $context, $this->getSourceContext());
                echo "
        </div>
    ";
            }
        } else {
            // line 55
            echo "    ";
            // line 56
            echo "    ";
            if ((twig_length_filter($this->env, ($context["organizations"] ?? null)) > 1)) {
                // line 57
                echo "        <div class=\"organization-switcher dropdown\"
            ";
                // line 58
                echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orosecurity/js/app/components/switch-organization-component", "options" => ["currentOrganizationId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 61
($context["curr_organization"] ?? null), "getId", [], "method", false, false, false, 61)]]], 58, $context, $this->getSourceContext());
                // line 63
                echo "
        >
            ";
                // line 65
                $context["togglerId"] = uniqid("dropdown-");
                // line 66
                echo "            <a id=\"";
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\" href=\"#\" role=\"button\" class=\"logo dropdown-toggle btn-organization-switcher\" data-toggle=\"dropdown\"
                aria-label=\"";
                // line 67
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.security.organization.switch"), "html", null, true);
                echo "\" aria-haspopup=\"true\" aria-expanded=\"false\">
                <span class=\"organization-name\">";
                // line 68
                echo twig_escape_filter($this->env, ($context["organization_name"] ?? null), "html", null, true);
                echo "</span>
            </a>
            <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"";
                // line 70
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\">
                ";
                // line 71
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["organizations"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["organization"]) {
                    // line 72
                    echo "                    ";
                    $context["is_current_organization"] = (($context["curr_organization"] ?? null) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["curr_organization"] ?? null), "getId", [], "method", false, false, false, 72) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["organization"], "id", [], "any", false, false, false, 72)));
                    // line 73
                    echo "                    <li ";
                    if (($context["is_current_organization"] ?? null)) {
                        echo "class=\"current\"";
                    }
                    echo ">
                        <a href=\"";
                    // line 74
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_security_switch_organization", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["organization"], "id", [], "any", false, false, false, 74)]), "html", null, true);
                    echo "\" class=\"dropdown-item no-hash\"
                           ";
                    // line 75
                    if (($context["is_current_organization"] ?? null)) {
                        echo "aria-selected=\"true\"";
                    }
                    echo ">";
                    // line 76
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["organization"], "name", [], "any", false, false, false, 76), "html", null, true);
                    // line 77
                    echo "</a>
                    </li>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['organization'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 80
                echo "            </ul>
        </div>
    ";
            } else {
                // line 83
                echo "        <h1 class=\"logo\">
            <a href=\"";
                // line 84
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_default");
                echo "\" title=\"";
                echo twig_escape_filter($this->env, twig_trim_filter(twig_striptags(($context["organization_name"] ?? null))), "html", null, true);
                echo "\" class=\"organization-name\">";
                // line 85
                echo twig_escape_filter($this->env, ($context["organization_name"] ?? null), "html", null, true);
                // line 86
                echo "</a>
        </h1>
    ";
            }
        }
        // line 90
        echo "
";
    }

    // line 91
    public function macro_organization_name_and_logo($__organization_name__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "organization_name" => $__organization_name__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 92
            echo "    ";
            if (twig_length_filter($this->env, ($context["organization_name"] ?? null))) {
                // line 93
                echo "        <div class=\"logo logo-text\">
            <a href=\"";
                // line 94
                echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_default");
                echo "\" title=\"";
                echo twig_escape_filter($this->env, ($context["organization_name"] ?? null), "html", null, true);
                echo "\" class=\"organization-name\">";
                // line 95
                echo twig_escape_filter($this->env, ($context["organization_name"] ?? null), "html", null, true);
                // line 96
                echo "</a>
        </div>
    ";
            } else {
                // line 99
                echo "        <span class=\"logo-placeholder\"></span>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroSecurity/Organization/selector.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  311 => 99,  306 => 96,  304 => 95,  299 => 94,  296 => 93,  293 => 92,  280 => 91,  275 => 90,  269 => 86,  267 => 85,  262 => 84,  259 => 83,  254 => 80,  246 => 77,  244 => 76,  239 => 75,  235 => 74,  228 => 73,  225 => 72,  221 => 71,  217 => 70,  212 => 68,  208 => 67,  203 => 66,  201 => 65,  197 => 63,  195 => 61,  194 => 58,  191 => 57,  188 => 56,  186 => 55,  179 => 51,  175 => 50,  172 => 49,  167 => 46,  153 => 45,  147 => 41,  145 => 40,  142 => 39,  138 => 37,  136 => 36,  131 => 34,  125 => 32,  123 => 31,  120 => 30,  103 => 29,  99 => 28,  95 => 27,  90 => 26,  88 => 25,  84 => 24,  80 => 23,  76 => 21,  74 => 19,  73 => 16,  70 => 15,  67 => 14,  65 => 13,  62 => 12,  59 => 8,  55 => 9,  53 => 8,  51 => 7,  48 => 6,  46 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSecurity/Organization/selector.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SecurityBundle/Resources/views/Organization/selector.html.twig");
    }
}
