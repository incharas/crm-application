<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataAudit/Datagrid/Property/new.html.twig */
class __TwigTemplate_54acbf25aa00d15d1680023102238c2e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataAudit"] = $this->macros["dataAudit"] = $this->loadTemplate("@OroDataAudit/macros.html.twig", "@OroDataAudit/Datagrid/Property/new.html.twig", 1)->unwrap();
        // line 3
        echo "<ul>";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["value"] ?? null));
        foreach ($context['_seq'] as $context["fieldKey"] => $context["fieldValue"]) {
            // line 5
            if (($context["fieldKey"] == "auditData")) {
                // line 6
                if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "new", [], "any", false, false, false, 6))) {
                    // line 7
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "new", [], "any", false, false, false, 7));
                    foreach ($context['_seq'] as $context["collKey"] => $context["collValue"]) {
                        // line 8
                        echo "<li>
                        <b>";
                        // line 9
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(_twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 9), $context["collKey"], "label"), $context["collKey"])), "html", null, true);
                        echo "&nbsp;</b>";
                        // line 10
                        echo twig_escape_filter($this->env, $context["collValue"], "html", null, true);
                        // line 11
                        echo "</li>";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['collKey'], $context['collValue'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                }
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
$context["fieldValue"], "collectionDiffs", [], "any", true, true, false, 14) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "collectionDiffs", [], "any", false, false, false, 14)))) {
                // line 15
                echo "<li>
                <b>";
                // line 16
                echo twig_call_macro($macros["dataAudit"], "macro_renderFieldName", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 16), $context["fieldKey"], $context["fieldValue"]], 16, $context, $this->getSourceContext());
                echo "&nbsp;</b>";
                // line 17
                echo twig_call_macro($macros["dataAudit"], "macro_renderCollection", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 17), $context["fieldKey"], $context["fieldValue"], [0 => "added", 1 => "changed"]], 17, $context, $this->getSourceContext());
                // line 18
                echo "</li>";
            } else {
                // line 20
                echo "<li>
                <b>";
                // line 21
                echo twig_call_macro($macros["dataAudit"], "macro_renderFieldName", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 21), $context["fieldKey"], $context["fieldValue"]], 21, $context, $this->getSourceContext());
                echo "&nbsp;</b>";
                // line 22
                echo twig_call_macro($macros["dataAudit"], "macro_renderFieldValue", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "new", [], "any", false, false, false, 22), $context["fieldValue"]], 22, $context, $this->getSourceContext());
                // line 23
                echo "</li>";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['fieldKey'], $context['fieldValue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "@OroDataAudit/Datagrid/Property/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 26,  89 => 23,  87 => 22,  84 => 21,  81 => 20,  78 => 18,  76 => 17,  73 => 16,  70 => 15,  68 => 14,  61 => 11,  59 => 10,  56 => 9,  53 => 8,  49 => 7,  47 => 6,  45 => 5,  41 => 4,  39 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataAudit/Datagrid/Property/new.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataAuditBundle/Resources/views/Datagrid/Property/new.html.twig");
    }
}
