<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Dashboard/update.html.twig */
class __TwigTemplate_14bf993d4fbad50b8368978c309bb786 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Dashboard/update.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.entity_label"), "%label%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "label", [], "any", false, false, false, 4)]]);
        // line 6
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig"], true);
        // line 7
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["form"] ?? null), "vars", [], "any", false, false, false, 8), "value", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroDashboard/Dashboard/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "value", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12)) {
            // line 13
            echo "        ";
            $context["breadcrumbs"] = ["entity" => [], "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.management_title"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["entity"] ?? null), "label", [], "any", false, false, false, 17)];
            // line 19
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 21
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.entity_label")]);
            // line 22
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroDashboard/Dashboard/update.html.twig", 22)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 23
            echo "    ";
        }
    }

    // line 26
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Dashboard/update.html.twig", 27)->unwrap();
        // line 28
        echo "
    ";
        // line 29
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_index")], 29, $context, $this->getSourceContext());
        echo "
    ";
        // line 30
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_dashboard_view", "params" => ["id" => "\$id", "change_dashboard" => true, "_enableContentProviders" => "mainMenu"]]], 30, $context, $this->getSourceContext());
        // line 38
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_dashboard_create")) {
            // line 39
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_dashboard_create"]], 39, $context, $this->getSourceContext()));
            // line 42
            echo "    ";
        }
        // line 43
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 43), "value", [], "any", false, false, false, 43), "id", [], "any", false, false, false, 43) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_dashboard_update"))) {
            // line 44
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_dashboard_update", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 44, $context, $this->getSourceContext()));
            // line 51
            echo "    ";
        }
        // line 52
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 52, $context, $this->getSourceContext());
        echo "
";
    }

    // line 55
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "    ";
        $context["id"] = "task-form";
        // line 57
        echo "
    ";
        // line 58
        ob_start(function () { return ''; });
        // line 59
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 59), 'row');
        echo "
        ";
        // line 60
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", true, true, false, 60)) {
            // line 61
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 61), 'row');
            echo "
        ";
        }
        // line 63
        echo "        ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "dashboardType", [], "any", true, true, false, 63)) {
            // line 64
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "dashboardType", [], "any", false, false, false, 64), 'row', ["group_attr" => ["class" => "dashboard-type"]]);
            echo "
        ";
        }
        // line 66
        echo "        ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "startDashboard", [], "any", true, true, false, 66)) {
            // line 67
            echo "            <div ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orodashboard/js/views/dashboard-type-view", "options" => ["startDashboardrField" => "#start-dashboard", "_sourceElement" => ".dashboard-type select"]]], 67, $context, $this->getSourceContext());
            // line 73
            echo ">
                ";
            // line 74
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "startDashboard", [], "any", false, false, false, 74), 'row', ["group_attr" => ["id" => "start-dashboard", "class" => "hide"]]);
            echo "
            </div>
        ";
        }
        // line 77
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 77));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 78
            echo "            ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, true, false, 78), "extra_field", [], "any", true, true, false, 78) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 78), "extra_field", [], "any", false, false, false, 78))) {
                // line 79
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'row');
                echo "
            ";
            }
            // line 81
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 82
        echo "    ";
        $context["dataBlock"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 83
        echo "
    ";
        // line 84
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 90
($context["dataBlock"] ?? null)]]]]];
        // line 95
        echo "
    ";
        // line 96
        $context["data"] = ["formErrors" => ((        // line 97
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 98
($context["dataBlocks"] ?? null)];
        // line 100
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Dashboard/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 100,  211 => 98,  210 => 97,  209 => 96,  206 => 95,  204 => 90,  203 => 84,  200 => 83,  197 => 82,  191 => 81,  185 => 79,  182 => 78,  177 => 77,  171 => 74,  168 => 73,  165 => 67,  162 => 66,  156 => 64,  153 => 63,  147 => 61,  145 => 60,  140 => 59,  138 => 58,  135 => 57,  132 => 56,  128 => 55,  121 => 52,  118 => 51,  115 => 44,  112 => 43,  109 => 42,  106 => 39,  103 => 38,  101 => 30,  97 => 29,  94 => 28,  91 => 27,  87 => 26,  82 => 23,  79 => 22,  76 => 21,  70 => 19,  68 => 17,  66 => 13,  63 => 12,  59 => 11,  54 => 1,  52 => 8,  51 => 7,  49 => 6,  47 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Dashboard/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Dashboard/update.html.twig");
    }
}
