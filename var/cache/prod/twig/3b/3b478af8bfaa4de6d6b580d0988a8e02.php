<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/multi-checkbox-view.js */
class __TwigTemplate_4e18008240b7711e2ca983ddfd86a019 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const BaseView = require('oroui/js/app/views/base/view');

    const MultiCheckboxView = BaseView.extend({
        defaults: {
            selectAttrs: {},
            value: [],
            items: null
        },

        template: require('tpl-loader!oroform/templates/multi-checkbox-view.html'),

        events: {
            'change input[type=checkbox]': 'onCheckboxToggle'
        },

        /**
         * @inheritdoc
         */
        constructor: function MultiCheckboxView(options) {
            MultiCheckboxView.__super__.constructor.call(this, options);
        },

        /**
         * @constructor
         *
         * @param {Object} options
         */
        initialize: function(options) {
            const opts = {};
            \$.extend(true, opts, this.defaults, options);
            _.extend(this, _.pick(opts, 'items', 'value', 'selectAttrs'));
            MultiCheckboxView.__super__.initialize.call(this, options);
        },

        getTemplateData: function() {
            const data = MultiCheckboxView.__super__.getTemplateData.call(this);
            data.name = this.selectAttrs.name || _.uniqueId('multi-checkbox');
            data.values = this.value;
            data.options = this.items;
            return data;
        },

        render: function() {
            MultiCheckboxView.__super__.render.call(this);
            this.getSelectElement().attr(this.selectAttrs);
            return this;
        },

        onCheckboxToggle: function(e) {
            let values = this.getValue();
            if (e.target.checked && _.indexOf(values, e.target.value) === -1) {
                values.push(e.target.value);
            } else if (!e.target.checked && _.indexOf(values, e.target.value) !== -1) {
                values = _.without(values, e.target.value);
            }
            this.setValue(values);
        },

        getSelectElement: function() {
            return this.\$('[data-name=\"multi-checkbox-value-keeper\"]');
        },

        getValue: function() {
            return this.getSelectElement().val();
        },

        setValue: function(values) {
            const oldValue = this.getValue();
            if (!_.haveEqualSet(oldValue, values)) {
                this.value = values;
                this.getSelectElement().val(values).trigger('change');
            }
        }
    });

    return MultiCheckboxView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/multi-checkbox-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/multi-checkbox-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/multi-checkbox-view.js");
    }
}
