<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroConfig/Form/fields.html.twig */
class __TwigTemplate_75dfd7ce0812b5959b823ea376f5f557 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_config_form_field_type_widget' => [$this, 'block_oro_config_form_field_type_widget'],
            'oro_config_form_field_type_row' => [$this, 'block_oro_config_form_field_type_row'],
            'oro_config_parent_scope_checkbox_type_row' => [$this, 'block_oro_config_parent_scope_checkbox_type_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_config_form_field_type_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('oro_config_form_field_type_row', $context, $blocks);
        // line 43
        echo "
";
        // line 44
        $this->displayBlock('oro_config_parent_scope_checkbox_type_row', $context, $blocks);
    }

    // line 1
    public function block_oro_config_form_field_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["valueContainerId"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 2), "vars", [], "any", false, false, false, 2), "id", [], "any", false, false, false, 2) . "_container");
        // line 3
        echo "    ";
        $context["valueContainerClass"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, true, false, 3), "vars", [], "any", false, true, false, 3), "attr", [], "any", false, true, false, 3), "class", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, true, false, 3), "vars", [], "any", false, true, false, 3), "attr", [], "any", false, true, false, 3), "class", [], "any", false, false, false, 3), "control-subgroup")) : ("control-subgroup"));
        // line 4
        echo "    ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 4), "vars", [], "any", false, false, false, 4), "errors", [], "any", false, false, false, 4)) > 0)) {
            // line 5
            echo "        ";
            $context["valueContainerClass"] = (($context["valueContainerClass"] ?? null) . " validation-error");
            // line 6
            echo "    ";
        }
        // line 7
        echo "
    <div id=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["valueContainerId"] ?? null), "html", null, true);
        echo "\" class=\"";
        echo twig_escape_filter($this->env, ($context["valueContainerClass"] ?? null), "html", null, true);
        echo "\">
        <div class=\"oro-clearfix\">
            ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 10), 'widget');
        echo "
            ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 11), 'errors');
        echo "
        </div>
    </div>

    ";
        // line 15
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_parent_scope_value", [], "any", true, true, false, 15)) {
            // line 16
            echo "        ";
            if (twig_in_filter("hidden", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_parent_scope_value", [], "any", false, false, false, 16), "vars", [], "any", false, false, false, 16), "block_prefixes", [], "any", false, false, false, 16))) {
                // line 17
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_parent_scope_value", [], "any", false, false, false, 17), 'row', ["attr" => ["data-role" => "changeUseDefault"]]);
                echo "
        ";
            } else {
                // line 19
                echo "            <div class=\"control-subgroup parent-scope-checkbox\">
                <div class=\"parent-scope-checkbox__wrapper\">
                    ";
                // line 21
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_parent_scope_value", [], "any", false, false, false, 21), 'row', ["attr" => ["data-role" => "changeUseDefault"]]);
                echo "
                </div>
            </div>
        ";
            }
            // line 25
            echo "    ";
        }
        // line 26
        echo "
    ";
        // line 27
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 27), "value_hint", [], "any", true, true, false, 27) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 27), "value_hint", [], "any", false, false, false, 27)))) {
            // line 28
            echo "        ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroConfig/Form/fields.html.twig", 28)->unwrap();
            // line 29
            echo "        <div class=\"control-subgroup value-hint-container\">
            ";
            // line 30
            echo twig_call_macro($macros["UI"], "macro_tooltip", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 30), "value_hint", [], "any", false, false, false, 30), [], null, false, null, null, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 30), "translatable_value_hint", [], "any", false, false, false, 30)], 30, $context, $this->getSourceContext());
            echo "
        </div>
    ";
        }
    }

    // line 35
    public function block_oro_config_form_field_type_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 36), "value_field_only", [], "any", false, false, false, 36)) {
            // line 37
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 37), 'row', ["label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 37), "label", [], "any", false, false, false, 37)]);
            echo "
    ";
        } else {
            // line 39
            echo "        ";
            $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["for" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 39), "vars", [], "any", false, false, false, 39), "id", [], "any", false, false, false, 39)]);
            // line 40
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row', ["label_attr" => ($context["label_attr"] ?? null)]);
            echo "
    ";
        }
    }

    // line 44
    public function block_oro_config_parent_scope_checkbox_type_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroConfig/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  171 => 46,  166 => 45,  162 => 44,  154 => 40,  151 => 39,  145 => 37,  142 => 36,  138 => 35,  130 => 30,  127 => 29,  124 => 28,  122 => 27,  119 => 26,  116 => 25,  109 => 21,  105 => 19,  99 => 17,  96 => 16,  94 => 15,  87 => 11,  83 => 10,  76 => 8,  73 => 7,  70 => 6,  67 => 5,  64 => 4,  61 => 3,  58 => 2,  54 => 1,  50 => 44,  47 => 43,  45 => 35,  42 => 34,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroConfig/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Resources/views/Form/fields.html.twig");
    }
}
