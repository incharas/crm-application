<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMessageQueue/Job/Datagrid/status.html.twig */
class __TwigTemplate_d2903e0e19b7e2fcd9d89c487f5d7635 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroMessageQueue/macros.html.twig", "@OroMessageQueue/Job/Datagrid/status.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["job"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "rootEntity", [], "any", false, false, false, 3);
        // line 4
        echo "
<span class=\"label ";
        // line 5
        echo twig_escape_filter($this->env, ("label-" . twig_call_macro($macros["UI"], "macro_getJobStatusClass", [($context["job"] ?? null)], 5, $context, $this->getSourceContext())), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["job"] ?? null), "status", [], "any", false, false, false, 5)), "html", null, true);
        echo "</span>
";
    }

    public function getTemplateName()
    {
        return "@OroMessageQueue/Job/Datagrid/status.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMessageQueue/Job/Datagrid/status.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MessageQueueBundle/Resources/views/Job/Datagrid/status.html.twig");
    }
}
