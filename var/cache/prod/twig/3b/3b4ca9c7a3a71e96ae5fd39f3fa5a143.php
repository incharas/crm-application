<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Dashboard/launchpad.html.twig */
class __TwigTemplate_fe805f9b62258d9067af0c357fb2b10d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("widgetAcl", $context) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(($context["widgetAcl"] ?? null)))) {
            // line 2
            echo "    <div class=\"widget-content launchpad-widget-content";
            if ((array_key_exists("widgetClass", $context) && ($context["widgetClass"] ?? null))) {
                echo " ";
                echo twig_escape_filter($this->env, ($context["widgetClass"] ?? null), "html", null, true);
                echo "-widget-content";
            }
            echo "\">
        <div class=\"launchpad-widget-content__title\"
            ";
            // line 4
            if (array_key_exists("item", $context)) {
                echo " title=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "label", [], "any", false, false, false, 4)), "html", null, true);
                echo "\" ";
            }
            echo ">
            ";
            // line 5
            if ((array_key_exists("widgetIcon", $context) && ($context["widgetIcon"] ?? null))) {
                // line 6
                echo "                <span class=\"fa-";
                echo twig_escape_filter($this->env, ($context["widgetIcon"] ?? null), "html", null, true);
                echo " launchpad-widget-content__icon\" aria-hidden=\"true\"></span>
            ";
            }
            // line 8
            echo "            <div class=\"launchpad-widget-content__label-wrapper\">
                <span class=\"launchpad-widget-content__label\">";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["widgetLabel"] ?? null)), "html", null, true);
            echo "</span>
            </div>
        </div>
        <ul class=\"launchpad-widget-content__list\">
            ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 14
                echo "                ";
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "acl", [], "any", false, false, false, 14))) {
                    // line 15
                    echo "                    <li class=\"launchpad-widget-content__list-item\">
                        <a href=\"";
                    // line 16
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "route", [], "any", false, false, false, 16), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "route_parameters", [], "any", true, true, false, 16)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "route_parameters", [], "any", false, false, false, 16), [])) : ([]))), "html", null, true);
                    echo "\"  class=\"launchpad-widget-content__link\">";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "label", [], "any", false, false, false, 16)), "html", null, true);
                    echo "</a>
                    </li>
                ";
                }
                // line 19
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "        </ul>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Dashboard/launchpad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 20,  93 => 19,  85 => 16,  82 => 15,  79 => 14,  75 => 13,  68 => 9,  65 => 8,  59 => 6,  57 => 5,  49 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Dashboard/launchpad.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Dashboard/launchpad.html.twig");
    }
}
