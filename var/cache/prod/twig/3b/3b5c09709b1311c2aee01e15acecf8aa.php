<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Widget/widget/transitionCustomForm.html.twig */
class __TwigTemplate_9cca19f01e393f240f838e131f305f04 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig", 1)->unwrap();
        // line 2
        $context["isWidgetContext"] = true;
        // line 3
        $context["formAction"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 3), "uri", [], "any", false, false, false, 3);
        // line 4
        $this->loadTemplate("@OroWorkflow/Widget/widget/transitionCustomForm.html.twig", "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig", 4, "590656529")->display(twig_array_merge($context, ["formAction" => ($context["formAction"] ?? null)]));
        // line 36
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 36,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/widget/transitionCustomForm.html.twig");
    }
}


/* @OroWorkflow/Widget/widget/transitionCustomForm.html.twig */
class __TwigTemplate_9cca19f01e393f240f838e131f305f04___590656529 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'widget_context' => [$this, 'block_widget_context'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 4
        return $this->loadTemplate(twig_get_attribute($this->env, $this->source, ($context["transition"] ?? null), "getFormTemplate", [], "method", false, false, false, 4), "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig", 4);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_widget_context($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "        ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig", 8)->unwrap();
        // line 9
        echo "        ";
        if (($context["saved"] ?? null)) {
            // line 10
            echo "            <div ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/widget-form-component", "options" => ["_wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["app"] ?? null), "request", [], "any", false, false, false, 13), "get", [0 => "_wid"], "method", false, false, false, 13), "data" => ((            // line 14
array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), null)) : (null))]]], 10, $context, $this->getSourceContext());
            // line 16
            echo "></div>
        ";
        } elseif ((twig_length_filter($this->env,         // line 17
($context["formErrors"] ?? null)) > 0)) {
            // line 18
            echo "            <div ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/widget-form-component", "options" => ["_wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["app"] ?? null), "request", [], "any", false, false, false, 21), "get", [0 => "_wid"], "method", false, false, false, 21), "formError" => true, "preventRemove" => true, "reloadLayout" => true]]], 18, $context, $this->getSourceContext());
            // line 26
            echo "></div>
            ";
            // line 27
            if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 27), "errors", [], "any", false, false, false, 27)) > 0)) {
                // line 28
                echo "                <div class=\"alert alert-error\" role=\"alert\">
                    ";
                // line 29
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
                echo "
                </div>
            ";
            }
            // line 32
            echo "        ";
        }
        // line 33
        echo "        ";
        $this->displayParentBlock("content", $context, $blocks);
        echo "
    ";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 33,  148 => 32,  142 => 29,  139 => 28,  137 => 27,  134 => 26,  132 => 21,  130 => 18,  128 => 17,  125 => 16,  123 => 14,  122 => 13,  120 => 10,  117 => 9,  114 => 8,  110 => 7,  106 => 6,  102 => 5,  92 => 4,  45 => 36,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/widget/transitionCustomForm.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/widget/transitionCustomForm.html.twig");
    }
}
