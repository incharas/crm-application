<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOrganization/Js/businessUnitResult.html.twig */
class __TwigTemplate_f782f4109f05dc9663e8d571950e1c60 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "&nbsp;<%= highlight(_.escape(name)) %>
";
    }

    public function getTemplateName()
    {
        return "@OroOrganization/Js/businessUnitResult.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOrganization/Js/businessUnitResult.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/OrganizationBundle/Resources/views/Js/businessUnitResult.html.twig");
    }
}
