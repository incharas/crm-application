<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Form/fields.html.twig */
class __TwigTemplate_2d51809c0c04435c96d2eeb04cef385c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_entity_create_or_select_inline_js_acl_user_autocomplete' => [$this, 'block_oro_entity_create_or_select_inline_js_acl_user_autocomplete'],
            'oro_type_widget_owners_row' => [$this, 'block_oro_type_widget_owners_row'],
            'oro_user_apikey_gen_row' => [$this, 'block_oro_user_apikey_gen_row'],
            'oro_user_apikey_gen_key_row' => [$this, 'block_oro_user_apikey_gen_key_row'],
            '_user_settings_oro_user___case_insensitive_email_addresses_enabled_value_errors' => [$this, 'block__user_settings_oro_user___case_insensitive_email_addresses_enabled_value_errors'],
            'oro_user_emailsettings_widget' => [$this, 'block_oro_user_emailsettings_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_entity_create_or_select_inline_js_acl_user_autocomplete', $context, $blocks);
        // line 43
        echo "
";
        // line 44
        $this->displayBlock('oro_type_widget_owners_row', $context, $blocks);
        // line 49
        echo "
";
        // line 50
        $this->displayBlock('oro_user_apikey_gen_row', $context, $blocks);
        // line 57
        echo "
";
        // line 58
        $this->displayBlock('oro_user_apikey_gen_key_row', $context, $blocks);
        // line 61
        echo "
";
        // line 62
        $this->displayBlock('_user_settings_oro_user___case_insensitive_email_addresses_enabled_value_errors', $context, $blocks);
        // line 73
        echo "
";
        // line 74
        $this->displayBlock('oro_user_emailsettings_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_entity_create_or_select_inline_js_acl_user_autocomplete($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Form/fields.html.twig", 2)->unwrap();
        // line 3
        echo "    ";
        if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 3), "configs", [], "any", false, true, false, 3), "async_dialogs", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 3), "configs", [], "any", false, true, false, 3), "async_dialogs", [], "any", false, false, false, 3), false)) : (false)) === true)) {
            // line 4
            echo "        ";
            $context["asyncNameSegment"] = "async-";
            // line 5
            echo "    ";
        }
        // line 6
        echo "    ";
        $context["gridParameters"] = twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 6), "grid_parameters", [], "any", false, false, false, 6), ["permission" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["configs"] ?? null), "permission", [], "any", false, false, false, 7), "entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["configs"] ?? null), "entity_name", [], "any", false, false, false, 8), "entity_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["configs"] ?? null), "entity_id", [], "any", false, false, false, 9)]);
        // line 11
        echo "
    ";
        // line 12
        $context["urlParts"] = ["grid" => ["route" => "oro_datagrid_widget", "parameters" => ["gridName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 16
($context["form"] ?? null), "vars", [], "any", false, false, false, 16), "grid_name", [], "any", false, false, false, 16), "params" =>         // line 17
($context["gridParameters"] ?? null), "renderParams" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 18
($context["form"] ?? null), "vars", [], "any", false, false, false, 18), "grid_render_parameters", [], "any", false, false, false, 18)]]];
        // line 22
        echo "
    ";
        // line 23
        if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 23), "create_enabled", [], "any", true, true, false, 23)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 23), "create_enabled", [], "any", false, false, false, 23), false)) : (false)) === true)) {
            // line 24
            echo "    ";
            $context["urlParts"] = twig_array_merge(($context["urlParts"] ?? null), ["create" => ["route" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "create_form_route", [], "any", false, false, false, 26), "parameters" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 27
($context["form"] ?? null), "vars", [], "any", false, false, false, 27), "create_form_route_parameters", [], "any", false, false, false, 27)]]);
            // line 30
            echo "    ";
        }
        // line 31
        echo "    <div ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => (("orocrmchannel/js/app/components/select-create-inline-type-" . ((        // line 32
array_key_exists("asyncNameSegment", $context)) ? (_twig_default_filter(($context["asyncNameSegment"] ?? null), "")) : (""))) . "component"), "options" => ["_sourceElement" => (("#" .         // line 34
($context["id"] ?? null)) . "-el"), "inputSelector" => ("#" .         // line 35
($context["id"] ?? null)), "entityLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 36
($context["label"] ?? null)), "urlParts" =>         // line 37
($context["urlParts"] ?? null), "existingEntityGridId" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 38
($context["form"] ?? null), "vars", [], "any", false, true, false, 38), "existing_entity_grid_id", [], "any", true, true, false, 38)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 38), "existing_entity_grid_id", [], "any", false, false, false, 38), "id")) : ("id")), "createEnabled" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 39
($context["form"] ?? null), "vars", [], "any", false, true, false, 39), "create_enabled", [], "any", true, true, false, 39)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 39), "create_enabled", [], "any", false, false, false, 39), false)) : (false))]]], 31, $context, $this->getSourceContext());
        // line 41
        echo " style=\"display: none\"></div>
";
    }

    // line 44
    public function block_oro_type_widget_owners_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 46
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'row');
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 50
    public function block_oro_user_apikey_gen_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start');
        echo "
    ";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
    ";
        // line 53
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "apiKey", [], "any", false, false, false, 53), 'row', ["apiKeyElementId" => ($context["apiKeyElementId"] ?? null)]);
        echo "
    <button class=\"btn btn-sm no-hash\" id=\"btn-apigen\" title=\"";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Generate key"), "html", null, true);
        echo "\" type=\"submit\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Generate key"), "html", null, true);
        echo "</button>
    ";
        // line 55
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
";
    }

    // line 58
    public function block_oro_user_apikey_gen_key_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    <span class=\"api-key\" id=\"";
        echo twig_escape_filter($this->env, ($context["apiKeyElementId"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, _twig_default_filter(twig_escape_filter($this->env, ($context["value"] ?? null)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")), "html", null, true);
        echo "</span>
";
    }

    // line 62
    public function block__user_settings_oro_user___case_insensitive_email_addresses_enabled_value_errors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        ob_start(function () { return ''; });
        // line 64
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 65
            echo "            ";
            $context["combinedError"] = "";
            // line 66
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 67
                echo "                ";
                $context["combinedError"] = (((($context["combinedError"] ?? null) != "")) ? (((($context["combinedError"] ?? null) . "; ") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 67))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 67)));
                // line 68
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 69
            echo "            <div><span class=\"validation-failed\"><span><span>";
            echo ($context["combinedError"] ?? null);
            echo "</span></span></span></div>
        ";
        }
        // line 71
        echo "    ";
        $___internal_parse_50_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 63
        echo twig_spaceless($___internal_parse_50_);
    }

    // line 74
    public function block_oro_user_emailsettings_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 75
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        ";
        // line 76
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 77
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  252 => 79,  243 => 77,  239 => 76,  234 => 75,  230 => 74,  226 => 63,  223 => 71,  217 => 69,  211 => 68,  208 => 67,  203 => 66,  200 => 65,  198 => 64,  196 => 63,  192 => 62,  183 => 59,  179 => 58,  173 => 55,  167 => 54,  163 => 53,  159 => 52,  154 => 51,  150 => 50,  139 => 46,  134 => 45,  130 => 44,  125 => 41,  123 => 39,  122 => 38,  121 => 37,  120 => 36,  119 => 35,  118 => 34,  117 => 32,  115 => 31,  112 => 30,  110 => 27,  109 => 26,  107 => 24,  105 => 23,  102 => 22,  100 => 18,  99 => 17,  98 => 16,  97 => 12,  94 => 11,  92 => 9,  91 => 8,  90 => 7,  88 => 6,  85 => 5,  82 => 4,  79 => 3,  76 => 2,  72 => 1,  68 => 74,  65 => 73,  63 => 62,  60 => 61,  58 => 58,  55 => 57,  53 => 50,  50 => 49,  48 => 44,  45 => 43,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Form/fields.html.twig");
    }
}
