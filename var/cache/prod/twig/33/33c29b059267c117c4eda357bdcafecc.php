<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAddress/Include/javascript.html.twig */
class __TwigTemplate_543bea1d38b5183e17ae80ee97580669 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_region_updater_js' => [$this, 'block_oro_region_updater_js'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_region_updater_js', $context, $blocks);
    }

    public function block_oro_region_updater_js($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <script type=\"text/template\" id=\"region-chooser-template\">
        <% _.each(regions, function(region, i) { %>
            <option value=\"<%- region.get('combinedCode') %>\"><%- region.get('name') %></option>
        <% }); %>
    </script>
";
    }

    public function getTemplateName()
    {
        return "@OroAddress/Include/javascript.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAddress/Include/javascript.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AddressBundle/Resources/views/Include/javascript.html.twig");
    }
}
