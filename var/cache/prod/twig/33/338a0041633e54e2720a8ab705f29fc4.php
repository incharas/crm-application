<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOrganization/viewOwnerInfo.html.twig */
class __TwigTemplate_8e955ceec635040b775b40ea8ff0d138 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["U"] = $this->macros["U"] = $this->loadTemplate("@OroUser/macros.html.twig", "@OroOrganization/viewOwnerInfo.html.twig", 1)->unwrap();
        // line 2
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getOwnerFieldName(($context["entity"] ?? null)))) {
            // line 3
            echo "    ";
            if ( !array_key_exists("bracket_parts", $context)) {
                // line 4
                echo "        ";
                $context["bracket_parts"] = [];
                // line 5
                echo "    ";
            }
            // line 6
            echo "    ";
            $context["ownerType"] = $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getOwnerType(($context["entity"] ?? null));
            // line 7
            echo "    ";
            if ((($context["ownerType"] ?? null) == "USER")) {
                // line 8
                echo "        ";
                $context["userOwner"] = $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getEntityOwner(($context["entity"] ?? null));
                // line 9
                echo "        ";
                if (($context["userOwner"] ?? null)) {
                    // line 10
                    echo "            ";
                    $context["businessUnitName"] = twig_call_macro($macros["U"], "macro_user_business_unit_name", [($context["userOwner"] ?? null), false], 10, $context, $this->getSourceContext());
                    // line 11
                    echo "            ";
                    if ( !twig_test_empty(($context["businessUnitName"] ?? null))) {
                        // line 12
                        echo "                ";
                        $context["bracket_parts"] = twig_array_merge([0 => ($context["businessUnitName"] ?? null)], ($context["bracket_parts"] ?? null));
                        // line 13
                        echo "            ";
                    }
                    // line 14
                    echo "        ";
                }
                // line 15
                echo "    ";
            } elseif ((($context["ownerType"] ?? null) == "ORGANIZATION")) {
                // line 16
                echo "        ";
                if ( !array_key_exists("organizationInfo", $context)) {
                    // line 17
                    echo "            ";
                    $context["organizationOwner"] = $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getEntityOwner(($context["entity"] ?? null));
                    // line 18
                    echo "            ";
                    if ( !(null === ($context["organizationOwner"] ?? null))) {
                        // line 19
                        echo "                ";
                        $context["organizationInfo"] = (($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.owner") . ": ") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["organizationOwner"] ?? null), "name", [], "any", false, false, false, 19));
                        // line 20
                        echo "            ";
                    } else {
                        // line 21
                        echo "                ";
                        $context["organizationInfo"] = "";
                        // line 22
                        echo "            ";
                    }
                    // line 23
                    echo "        ";
                }
                // line 24
                echo "        ";
                echo ($context["organizationInfo"] ?? null);
                echo "
    ";
            }
            // line 26
            echo "    ";
            if ( !twig_test_empty(($context["bracket_parts"] ?? null))) {
                // line 27
                echo "        (";
                echo twig_join_filter(($context["bracket_parts"] ?? null), ", ");
                echo ")
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "@OroOrganization/viewOwnerInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 27,  110 => 26,  104 => 24,  101 => 23,  98 => 22,  95 => 21,  92 => 20,  89 => 19,  86 => 18,  83 => 17,  80 => 16,  77 => 15,  74 => 14,  71 => 13,  68 => 12,  65 => 11,  62 => 10,  59 => 9,  56 => 8,  53 => 7,  50 => 6,  47 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOrganization/viewOwnerInfo.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/OrganizationBundle/Resources/views/viewOwnerInfo.html.twig");
    }
}
