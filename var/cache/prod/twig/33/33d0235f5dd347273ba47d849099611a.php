<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/AddressBook/update.html.twig */
class __TwigTemplate_928966e98703d2bab054ecbdd358c1b4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig"], true);
        // line 4
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_address_book_create");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroDotmailer/AddressBook/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $context["id"] = "oro_dotmailer_address_book_form";
        // line 8
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 13
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channel", [], "any", false, false, false, 13), 'row'), 1 =>         // line 14
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 14), 'row'), 2 =>         // line 15
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "visibility", [], "any", false, false, false, 15), 'row')]]]]];
        // line 19
        echo "
    ";
        // line 20
        $context["data"] = ["formErrors" => ((        // line 21
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 22
($context["dataBlocks"] ?? null)];
        // line 24
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/AddressBook/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  71 => 24,  69 => 22,  68 => 21,  67 => 20,  64 => 19,  62 => 15,  61 => 14,  60 => 13,  58 => 8,  55 => 7,  51 => 6,  46 => 1,  44 => 4,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/AddressBook/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/AddressBook/update.html.twig");
    }
}
