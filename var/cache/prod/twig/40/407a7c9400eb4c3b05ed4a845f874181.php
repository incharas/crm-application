<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/_tabs-content.html.twig */
class __TwigTemplate_b4957c2f7cf00c68043b84986633950b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'root' => [$this, 'block_root'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/_tabs-content.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/_tabs-content.html.twig", 4)->unwrap();
        // line 5
        echo "
    ";
        // line 6
        $context["listAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "childrenAttributes", [], "any", false, false, false, 6);
        // line 7
        echo "    ";
        $context["listAttributes"] = twig_array_merge(($context["listAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [($context["listAttributes"] ?? null), "class", [0 => "nav", 1 => "nav-pills"]], 7, $context, $this->getSourceContext())]);
        // line 8
        echo "
    <div class=\"tab-content\">
        ";
        // line 10
        $context["items"] = ($context["item"] ?? null);
        // line 11
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 12
            echo "            ";
            $context["showNonAuthorized"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, true, false, 12), "show_non_authorized", [], "any", true, true, false, 12) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, false, false, 12), "show_non_authorized", [], "any", false, false, false, 12));
            // line 13
            echo "            ";
            $context["displayable"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, false, false, 13), "isAllowed", [], "any", false, false, false, 13) || ($context["showNonAuthorized"] ?? null));
            // line 14
            echo "            ";
            if (((($context["displayable"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "hasChildren", [], "any", false, false, false, 14)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "displayChildren", [], "any", false, false, false, 14))) {
                // line 15
                echo "                ";
                $context["tabClasses"] = [0 => "tab-pane"];
                // line 16
                echo "                ";
                $context["tabClasses"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["matcher"] ?? null), "isAncestor", [0 => $context["item"], 1 => 2], "method", false, false, false, 16)) ? (twig_array_merge(($context["tabClasses"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "ancestorClass", [], "any", false, false, false, 16)])) : (($context["tabClasses"] ?? null)));
                // line 17
                echo "                ";
                $context["tabClasses"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["matcher"] ?? null), "isCurrent", [0 => $context["item"]], "method", false, false, false, 17)) ? (twig_array_merge(($context["tabClasses"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "currentClass", [], "any", false, false, false, 17)])) : (($context["tabClasses"] ?? null)));
                // line 18
                echo "                <div class=\"";
                echo twig_escape_filter($this->env, twig_join_filter(($context["tabClasses"] ?? null), " "), "html", null, true);
                echo "\"
                     id=\"";
                // line 19
                echo twig_escape_filter($this->env, twig_trim_filter(twig_lower_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 19), [" " => "_", "#" => "_"]))), "html", null, true);
                echo "\">
                    ";
                // line 20
                $this->displayBlock("list", $context, $blocks);
                // line 21
                echo "</div>
            ";
            }
            // line 23
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 24
        echo "        ";
        $context["item"] = ($context["items"] ?? null);
        // line 25
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/_tabs-content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 25,  132 => 24,  118 => 23,  114 => 21,  112 => 20,  108 => 19,  103 => 18,  100 => 17,  97 => 16,  94 => 15,  91 => 14,  88 => 13,  85 => 12,  67 => 11,  65 => 10,  61 => 8,  58 => 7,  56 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/_tabs-content.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/_tabs-content.html.twig");
    }
}
