<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Configuration/Mailbox/update.html.twig */
class __TwigTemplate_e76040a9d10e14fdd4780efb02a7690b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroConfig/configPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $macros["emailUI"] = $this->macros["emailUI"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 3)->unwrap();
        // line 4
        $macros["configUI"] = $this->macros["configUI"] = $this->loadTemplate("@OroConfig/macros.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 4)->unwrap();
        // line 6
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 6), "data", [], "any", false, false, false, 6), "id", [], "any", false, false, false, 6)) {
            // line 7
            $context["mailboxTitle"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "data", [], "any", false, false, false, 7), "label", [], "any", false, false, false, 7);
        } else {
            // line 9
            $context["mailboxTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.mailbox.action.new");
        }
        // line 12
        $context["pageTitle"] = [0 => ["link" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_config_configuration_system"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.config.menu.system_configuration.label")], 1 => ["link" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_config_configuration_system", ["activeGroup" => "platform", "activeSubGroup" => "email_configuration"]), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.system_configuration.email_configuration")], 2 =>         // line 24
($context["mailboxTitle"] ?? null)];
        // line 27
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 28
($context["app"] ?? null), "request", [], "any", false, false, false, 28), "attributes", [], "any", false, false, false, 28), "get", [0 => "_route"], "method", false, false, false, 28), twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 29
($context["app"] ?? null), "request", [], "any", false, false, false, 29), "attributes", [], "any", false, false, false, 29), "get", [0 => "_route_params"], "method", false, false, false, 29), ["redirectData" => ($context["redirectData"] ?? null)]));
        // line 32
        $context["routeName"] = "oro_config_configuration_system";
        // line 33
        $context["routeParameters"] = [];
        // line 35
        $macros["syncMacro"] = $this->macros["syncMacro"] = $this->loadTemplate("@OroSync/Include/contentTags.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 35)->unwrap();
        // line 36
        $macros["configUI"] = $this->macros["configUI"] = $this->loadTemplate("@OroConfig/macros.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 36)->unwrap();
        // line 37
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 37)->unwrap();
        // line 39
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => "@OroLocale/Form/fields.html.twig"], true);
        // line 41
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 41), "value", [], "any", false, false, false, 41), "id", [], "any", false, false, false, 41)) {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%label%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 42
($context["form"] ?? null), "vars", [], "any", false, false, false, 42), "value", [], "any", false, false, false, 42), "label", [], "any", false, false, false, 42)]]);
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroConfig/configPage.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 45
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "    ";
        $context["breadcrumbs"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.config.menu.system_configuration.label")], 1 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.system_configuration.email_configuration")]];
        // line 50
        echo "    ";
        $this->loadTemplate("@OroNavigation/Menu/breadcrumbs.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 50)->display($context);
    }

    // line 53
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 54)->unwrap();
        // line 55
        echo "
    ";
        // line 56
        $context["buttons"] = "";
        // line 57
        echo "    ";
        $context["html"] = "";
        // line 58
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 58), "value", [], "any", false, false, false, 58), "id", [], "any", false, false, false, 58) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 58), "value", [], "any", false, false, false, 58)))) {
            // line 59
            echo "        ";
            $context["buttons"] = (($context["buttons"] ?? null) . twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_mailbox_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 60
($context["form"] ?? null), "vars", [], "any", false, false, false, 60), "value", [], "any", false, false, false, 60), "id", [], "any", false, false, false, 60)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_config_configuration_system", ["activeGroup" => "platform", "activeSubGroup" => "email_configuration"]), "aCss" => "no-hash remove-button", "id" => "btn-remove-mailbox", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 67
($context["form"] ?? null), "vars", [], "any", false, false, false, 67), "value", [], "any", false, false, false, 67), "id", [], "any", false, false, false, 67), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.mailbox.entity_label")]], 59, $context, $this->getSourceContext()));
            // line 70
            echo "        ";
            $context["buttons"] = (($context["buttons"] ?? null) . twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 70, $context, $this->getSourceContext()));
            // line 71
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_email_mailbox_update", "params" => ["id" => "\$id"]]], 71, $context, $this->getSourceContext()));
            // line 72
            echo "    ";
        }
        // line 73
        echo "
    ";
        // line 74
        $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_config_configuration_system", "params" => ["activeGroup" => "platform", "activeSubGroup" => "email_configuration"]]], 74, $context, $this->getSourceContext()));
        // line 78
        echo "    ";
        $context["buttons"] = (($context["buttons"] ?? null) . twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 78, $context, $this->getSourceContext()));
        // line 79
        echo "
    ";
        // line 80
        $context["options"] = ["view" => "oroconfig/js/form/config-form"];
        // line 83
        echo "
    ";
        // line 84
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" =>         // line 85
($context["formAction"] ?? null), "attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 87
($context["form"] ?? null), "vars", [], "any", false, false, false, 87), "id", [], "any", false, false, false, 87), "data-collect" => "true", "data-page-component-view" => json_encode(        // line 89
($context["options"] ?? null))]]);
        // line 91
        echo "
        ";
        // line 92
        ob_start(function () { return ''; });
        // line 93
        echo "            ";
        $context["mailboxId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 93), "value", [], "any", false, false, false, 93), "id", [], "any", false, false, false, 93);
        // line 94
        echo "            ";
        $context["mailboxParams"] = [];
        // line 95
        echo "
            ";
        // line 96
        if (($context["mailboxId"] ?? null)) {
            // line 97
            echo "                ";
            $context["mailboxParams"] = twig_array_merge(($context["mailboxParams"] ?? null), ["mailbox" =>             // line 98
($context["mailboxId"] ?? null)]);
            // line 100
            echo "            ";
        }
        // line 101
        echo "
            <fieldset class=\"form-horizontal form-horizontal-large auto-response-rules\">
                <div class=\"auto-response-rule-header\">
                    <h5 class=\"user-fieldset\">";
        // line 104
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.autoresponserule.entity_plural_label"), "html", null, true);
        echo "</h5>
                    ";
        // line 105
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_autoresponserule_create",         // line 106
($context["mailboxParams"] ?? null)), "aCss" => "pull-right no-hash btn btn-primary", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.autoresponserule.action.add.label"), "widget" => ["type" => "dialog", "multiple" => false, "reload-grid-name" => "email-auto-response-rules", "options" => ["alias" => "auto-response-rules-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.autoresponserule.action.add.title"), "allowMaximize" => false, "allowMinimize" => false, "modal" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000]]]]], 105, $context, $this->getSourceContext());
        // line 126
        echo "
                </div>
                ";
        // line 128
        $macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroEmail/Configuration/Mailbox/update.html.twig", 128)->unwrap();
        // line 129
        echo "
                ";
        // line 130
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "unboundRules", [], "any", false, true, false, 130), "vars", [], "any", false, true, false, 130), "value", [], "any", true, true, false, 130)) {
            // line 131
            echo "                    ";
            $context["mailboxParams"] = twig_array_merge(($context["mailboxParams"] ?? null), ["ids" => twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 132
($context["form"] ?? null), "unboundRules", [], "any", false, false, false, 132), "vars", [], "any", false, false, false, 132), "value", [], "any", false, false, false, 132), ",")]);
            // line 134
            echo "                ";
        }
        // line 135
        echo "
                ";
        // line 136
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["email-auto-response-rules", ($context["mailboxParams"] ?? null)], 136, $context, $this->getSourceContext());
        echo "
            </fieldset>
        ";
        $context["autoResponseRules"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 139
        echo "        ";
        echo twig_call_macro($macros["emailUI"], "macro_renderMailboxConfigTitleAndButtons", [($context["pageTitle"] ?? null), ($context["buttons"] ?? null)], 139, $context, $this->getSourceContext());
        echo "
        ";
        // line 140
        echo twig_call_macro($macros["configUI"], "macro_renderConfigurationScrollData", [["configTree" =>         // line 141
($context["data"] ?? null), "form" =>         // line 142
($context["form"] ?? null), "content" => ["dataBlocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.system_configuration.mailbox_configuration.label"), "subblocks" => [0 =>         // line 147
        $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form'), 1 =>         // line 148
($context["autoResponseRules"] ?? null)]]]], "activeTabName" =>         // line 152
($context["activeGroup"] ?? null), "activeSubTabName" =>         // line 153
($context["activeSubGroup"] ?? null), "routeName" =>         // line 154
($context["routeName"] ?? null), "routeParameters" =>         // line 155
($context["routeParameters"] ?? null)]], 140, $context, $this->getSourceContext());
        // line 156
        echo "
    ";
        // line 157
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end', ["render_rest" => false]);
        echo "
    ";
        // line 158
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        echo "
    ";
        // line 159
        echo twig_call_macro($macros["syncMacro"], "macro_syncContentTags", [["name" => "system_configuration", "params" => [0 => ($context["activeGroup"] ?? null), 1 => ($context["activeSubGroup"] ?? null)]]], 159, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Configuration/Mailbox/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 159,  234 => 158,  230 => 157,  227 => 156,  225 => 155,  224 => 154,  223 => 153,  222 => 152,  221 => 148,  220 => 147,  219 => 142,  218 => 141,  217 => 140,  212 => 139,  206 => 136,  203 => 135,  200 => 134,  198 => 132,  196 => 131,  194 => 130,  191 => 129,  189 => 128,  185 => 126,  183 => 106,  182 => 105,  178 => 104,  173 => 101,  170 => 100,  168 => 98,  166 => 97,  164 => 96,  161 => 95,  158 => 94,  155 => 93,  153 => 92,  150 => 91,  148 => 89,  147 => 87,  146 => 85,  145 => 84,  142 => 83,  140 => 80,  137 => 79,  134 => 78,  132 => 74,  129 => 73,  126 => 72,  123 => 71,  120 => 70,  118 => 67,  117 => 60,  115 => 59,  112 => 58,  109 => 57,  107 => 56,  104 => 55,  101 => 54,  97 => 53,  92 => 50,  89 => 46,  85 => 45,  80 => 1,  77 => 42,  74 => 41,  72 => 39,  70 => 37,  68 => 36,  66 => 35,  64 => 33,  62 => 32,  60 => 29,  59 => 28,  58 => 27,  56 => 24,  55 => 12,  52 => 9,  49 => 7,  47 => 6,  45 => 4,  43 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Configuration/Mailbox/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Configuration/Mailbox/update.html.twig");
    }
}
