<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAccount/Account/view.html.twig */
class __TwigTemplate_412184c5020f74ca49305cd3f0341b74 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["hasGrantedNameView"] = $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "name");

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%account.name%" => ((        // line 5
($context["hasGrantedNameView"] ?? null)) ? (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "name", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 6), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("view %fieldName% not granted", ["%fieldName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.name.label")])))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroAccount/Account/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAccount/Account/view.html.twig", 11)->unwrap();
        // line 12
        echo "
    ";
        // line 13
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 14
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["entity"] ?? null), "id", [], "any", false, false, false, 15)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.entity_label")]], 14, $context, $this->getSourceContext());
            // line 17
            echo "
    ";
        }
        // line 19
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 20
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_account", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["entity"] ?? null), "id", [], "any", false, false, false, 21)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_index"), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 24
($context["entity"] ?? null), "id", [], "any", false, false, false, 24), "id" => "btn-remove-account", "dataMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.delete_confirm_cascade", ["%entity_label%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.entity_label")])]], 20, $context, $this->getSourceContext());
            // line 29
            echo "
    ";
        }
    }

    // line 33
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAccount/Account/view.html.twig", 34)->unwrap();
        // line 35
        echo "
    ";
        // line 36
        $context["breadcrumbs"] = ["entity" =>         // line 37
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.entity_plural_label"), "rawEntityTitle" =>  !        // line 40
($context["hasGrantedNameView"] ?? null), "entityTitle" => ((        // line 41
($context["hasGrantedNameView"] ?? null)) ? (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 42
($context["entity"] ?? null), "name", [], "any", true, true, false, 42)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 42), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))) : (twig_call_macro($macros["UI"], "macro_renderDisabledLabel", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("view %fieldName% not granted", ["%fieldName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.name.label")])], 43, $context, $this->getSourceContext())))];
        // line 45
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 48
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "    ";
        ob_start(function () { return ''; });
        // line 50
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.widgets.account_information"), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_widget_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 53
($context["entity"] ?? null), "id", [], "any", false, false, false, 53)])]);
        // line 54
        echo "
    ";
        $context["accountInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 56
        echo "
    ";
        // line 57
        $context["generalSectionBlocks"] = [0 => ["data" => [0 => ($context["accountInformationWidget"] ?? null)]]];
        // line 58
        echo "
    ";
        // line 59
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contact_view") && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "contacts"))) {
            // line 60
            echo "        ";
            ob_start(function () { return ''; });
            // line 61
            echo "            <div class=\"contact-widget-wrapper\">
                ";
            // line 62
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_widget_contacts", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 64
($context["entity"] ?? null), "id", [], "any", false, false, false, 64)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.widgets.account_contacts")]);
            // line 66
            echo "
            </div>
        ";
            $context["contactsInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 69
            echo "
        ";
            // line 70
            $context["generalSectionBlocks"] = twig_array_merge(($context["generalSectionBlocks"] ?? null), [0 => ["data" => [0 =>             // line 71
($context["contactsInformationWidget"] ?? null)]]]);
            // line 73
            echo "    ";
        }
        // line 74
        echo "
    ";
        // line 75
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.sections.general"), "subblocks" =>         // line 78
($context["generalSectionBlocks"] ?? null)]];
        // line 81
        echo "
    ";
        // line 83
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["channels"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
            // line 84
            ob_start(function () { return ''; });
            // line 85
            ob_start(function () { return ''; });
            // line 86
            echo "                ";
            $context["placeHolderName"] = ("oro_account_channel_info_" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["channel"], "channelType", [], "any", false, false, false, 86));
            // line 87
            echo "                ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ($context["placeHolderName"] ?? null), ["accountId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 87), "channelId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["channel"], "id", [], "any", false, false, false, 87)]);
            echo "
            ";
            $___internal_parse_94_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 85
            echo twig_spaceless($___internal_parse_94_);
            $context["accountChannelInfoSection"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 90
            if ( !twig_test_empty(twig_trim_filter(twig_striptags(($context["accountChannelInfoSection"] ?? null))))) {
                // line 91
                echo "            ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 93
$context["channel"], "name", [], "any", false, false, false, 93), "priority" => 255, "subblocks" => [0 => ["data" => [0 =>                 // line 96
($context["accountChannelInfoSection"] ?? null)]]]]]);
                // line 100
                echo "        ";
            }
            // line 101
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 102
        echo "
    ";
        // line 104
        echo "    ";
        ob_start(function () { return ''; });
        // line 105
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("oro_website_activity", $context)) ? (_twig_default_filter(($context["oro_website_activity"] ?? null), "oro_website_activity")) : ("oro_website_activity")), ["customers" => ($context["customers"] ?? null), "byChannel" => true]);
        $context["websiteActivity"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 107
        echo "    ";
        if ( !twig_test_empty(($context["websiteActivity"] ?? null))) {
            // line 108
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.sections.website_activity"), "priority" => 1050, "subblocks" => [0 => ["data" => [0 =>             // line 113
($context["websiteActivity"] ?? null)]]]]]);
            // line 117
            echo "    ";
        }
        // line 118
        echo "
    ";
        // line 119
        $context["id"] = "accountView";
        // line 120
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 121
        echo "
    ";
        // line 122
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroAccount/Account/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  236 => 122,  233 => 121,  230 => 120,  228 => 119,  225 => 118,  222 => 117,  220 => 113,  218 => 108,  215 => 107,  212 => 105,  209 => 104,  206 => 102,  200 => 101,  197 => 100,  195 => 96,  194 => 93,  192 => 91,  190 => 90,  187 => 85,  181 => 87,  178 => 86,  176 => 85,  174 => 84,  169 => 83,  166 => 81,  164 => 78,  163 => 75,  160 => 74,  157 => 73,  155 => 71,  154 => 70,  151 => 69,  146 => 66,  144 => 64,  143 => 62,  140 => 61,  137 => 60,  135 => 59,  132 => 58,  130 => 57,  127 => 56,  123 => 54,  121 => 53,  119 => 50,  116 => 49,  112 => 48,  105 => 45,  103 => 42,  102 => 41,  101 => 40,  100 => 37,  99 => 36,  96 => 35,  93 => 34,  89 => 33,  83 => 29,  81 => 24,  80 => 21,  78 => 20,  75 => 19,  71 => 17,  69 => 15,  67 => 14,  65 => 13,  62 => 12,  59 => 11,  55 => 10,  50 => 1,  48 => 6,  47 => 5,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAccount/Account/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AccountBundle/Resources/views/Account/view.html.twig");
    }
}
