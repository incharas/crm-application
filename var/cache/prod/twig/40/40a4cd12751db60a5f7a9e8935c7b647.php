<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/Form/Include/fields.html.twig */
class __TwigTemplate_6b85775d0b702d093af80603e891b9be extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_tag_config_choice_widget' => [$this, 'block_oro_tag_config_choice_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_tag_config_choice_widget', $context, $blocks);
    }

    public function block_oro_tag_config_choice_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $this->displayBlock("choice_widget", $context, $blocks);
        echo "

    ";
        // line 4
        if (($context["value"] ?? null)) {
            // line 5
            echo "        <div class=\"alert alert-danger tags-config\" role=\"alert\">
            ";
            // line 6
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.config.disable.alert"), "html", null, true);
            echo "
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroTag/Form/Include/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  56 => 6,  53 => 5,  51 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/Form/Include/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/Form/Include/fields.html.twig");
    }
}
