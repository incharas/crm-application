<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Datagrid/Column/direction.html.twig */
class __TwigTemplate_bd9a20237725533b37ef65e9a3d0e268 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((($context["value"] ?? null) == "outgoing")) {
            // line 2
            echo "    <i class=\"fa-sign-out out-call\"></i>
";
        } else {
            // line 4
            echo "    <i class=\"fa-sign-in in-call\"></i>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCall/Datagrid/Column/direction.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Datagrid/Column/direction.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Datagrid/Column/direction.html.twig");
    }
}
