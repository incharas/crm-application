<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/jquery.timepicker.js */
class __TwigTemplate_bcc3ab970f8289f41dacb6dd8a9a0e6c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const mask = require('oroui/js/dropdown-mask');
    require('oroui/js/jquery-timepicker-l10n');
    require('jquery.timepicker');

    const origTimepicker = \$.fn.timepicker;
    \$.fn.timepicker = function(method, ...args) {
        let options;
        let result;
        if (typeof method === 'object' || !method || method === 'init') {
            options = method === 'init' ? args[1] : method;
            options = \$.extend(true, {}, origTimepicker.defaults, options);
            if (_.isRTL()) {
                options.orientation = 'r';
            }
            result = origTimepicker.call(this, options);
        } else {
            args.unshift(method);
            result = origTimepicker.apply(this, args);
        }
        return result;
    };

    \$(document)
        .on('showTimepicker', function(e) {
            const zIndex = e.target.timepickerObj.list.css('zIndex');
            mask.show(zIndex - 1)
                .onhide(function() {
                    \$(e.target).timepicker('hide');
                });
        })
        .on('hideTimepicker', function(e) {
            mask.hide();
        });
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/jquery.timepicker.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/jquery.timepicker.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/jquery.timepicker.js");
    }
}
