<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReport/Segment/aggregated_field_condition.html.twig */
class __TwigTemplate_a0da8db27cff4a850109b093f958d41a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["aggregatedFieldConditionOptions"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(["filters" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 2
($context["params"] ?? null), "metadata", [], "any", false, true, false, 2), "filters", [], "any", true, true, false, 2)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "metadata", [], "any", false, true, false, 2), "filters", [], "any", false, false, false, 2), [])) : ([])), "hierarchy" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["params"] ?? null), "metadata", [], "any", false, true, false, 3), "hierarchy", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "metadata", [], "any", false, true, false, 3), "hierarchy", [], "any", false, false, false, 3), [])) : ([])), "fieldChoice" => ["filterPreset" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["params"] ?? null), "field_choice_filter_preset", [], "any", false, false, false, 5), "select2" => ["placeholder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.choose_entity_field"), "formatSelectionTemplateSelector" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["params"] ?? null), "column_chain_template_selector", [], "any", true, true, false, 8)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "column_chain_template_selector", [], "any", false, false, false, 8), null)) : (null))]]], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["params"] ?? null), "aggregatedFieldConditionOptions", [], "any", true, true, false, 11)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "aggregatedFieldConditionOptions", [], "any", false, false, false, 11), [])) : ([])));
        // line 12
        echo "
<li class=\"option\" data-criteria=\"aggregated-condition-item\"
    data-module=\"oroquerydesigner/js/app/views/aggregated-field-condition-view\"
    data-options=\"";
        // line 15
        echo twig_escape_filter($this->env, json_encode(($context["aggregatedFieldConditionOptions"] ?? null)), "html", null, true);
        echo "\">
    ";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.criteria.aggregated_field_condition"), "html", null, true);
        echo "
</li>
";
    }

    public function getTemplateName()
    {
        return "@OroReport/Segment/aggregated_field_condition.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 16,  49 => 15,  44 => 12,  42 => 11,  41 => 8,  40 => 5,  39 => 3,  38 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReport/Segment/aggregated_field_condition.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ReportBundle/Resources/views/Segment/aggregated_field_condition.html.twig");
    }
}
