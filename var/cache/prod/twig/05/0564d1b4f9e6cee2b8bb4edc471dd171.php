<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Widget/widget/entityWorkflows.html.twig */
class __TwigTemplate_263e8a9ec7ba509ef6d49aba16859969 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["workflowMacros"] = $this->macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/Widget/widget/entityWorkflows.html.twig", 1)->unwrap();
        // line 2
        if ((twig_length_filter($this->env, ($context["workflows"] ?? null)) > 0)) {
            // line 3
            echo "    ";
            $context["collapseContainerId"] = ("entityWorkflow" . ($context["entityId"] ?? null));
            // line 4
            echo "<div class=\"widget-content workflow-widget-content\">
    ";
            // line 5
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["workflows"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["workflow"]) {
                // line 6
                echo "        ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 6) == 2)) {
                    // line 7
                    echo "        <div class=\"collapse show\" id=\"";
                    echo twig_escape_filter($this->env, ($context["collapseContainerId"] ?? null), "html", null, true);
                    echo "\" data-collapsed-title=\"Expand\" data-expanded-title=\"Collapse\" aria-label=\"Expand / Collapse\">
        ";
                }
                // line 9
                echo "        ";
                if (((twig_length_filter($this->env, ($context["workflows"] ?? null)) > 1) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 9))) {
                    // line 10
                    echo "            <div class=\"workflow-collapse\">
                <button class=\"btn-light\" data-toggle=\"collapse\"
                        data-target=\"#";
                    // line 12
                    echo twig_escape_filter($this->env, ($context["collapseContainerId"] ?? null), "html", null, true);
                    echo "\" data-collapse-state-id=\"";
                    echo twig_escape_filter($this->env, ($context["collapseContainerId"] ?? null), "html", null, true);
                    echo "\"
                        aria-expanded=\"true\" title=\"Expand\">
                    <span class=\"fa-icon oro-collapse-icon\" aria-hidden=\"true\"></span>
                </button>
            </div>
        ";
                }
                // line 18
                echo "            <div class=\"workflow-entity ";
                if ((twig_length_filter($this->env, ($context["workflows"] ?? null)) == 1)) {
                    echo " only-workflow";
                }
                echo "\">
                <div class=\"workflow-label\">
                    <div class=\"workflow-label-text\" title=\"";
                // line 20
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "label", [], "any", false, false, false, 20), [], "workflows"), "html", null, true);
                echo "\">
                        ";
                // line 21
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "label", [], "any", false, false, false, 21), [], "workflows"), "html", null, true);
                echo ":
                    </div>
                </div>
                <div class=\"workflow-content-container\">
                    <table class=\"workflow-content\">
                        <tr>
                            <td class=\"workflow-steps\">
                                ";
                // line 28
                $context["isOrdered"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "stepsData", [], "any", false, false, false, 28), "isOrdered", [], "any", false, false, false, 28);
                // line 29
                echo "                                ";
                $context["steps"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "stepsData", [], "any", false, false, false, 29), "steps", [], "any", false, false, false, 29);
                // line 30
                echo "
                                ";
                // line 31
                if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "isStarted", [], "any", false, false, false, 31) && (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "transitionsData", [], "any", false, false, false, 31)) > 1))) {
                    // line 32
                    echo "                                    ";
                    $context["steps"] = [0 => ["active" => false, "label" => "...", "possibleStepsCount" => 0, "final" => false]];
                    // line 33
                    echo "                                ";
                }
                // line 34
                echo "
                                <div class=\"workflow-scroller\">
                                    <ul class=\"workflow-steps-list ";
                // line 36
                if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "isStarted", [], "any", false, false, false, 36)) {
                    echo "workflow-not-started";
                }
                echo "\">
                                        ";
                // line 37
                $context["activeRendered"] =  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "isStarted", [], "any", false, false, false, 37);
                // line 38
                echo "                                        ";
                $context["manyVariantsFound"] = false;
                // line 39
                echo "                                        ";
                $context["manyVariantsRendered"] = false;
                // line 40
                echo "
                                        ";
                // line 41
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["steps"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["step"]) {
                    // line 42
                    echo "                                            ";
                    if ( !($context["manyVariantsRendered"] ?? null)) {
                        // line 43
                        echo "                                                ";
                        $context["final"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["step"], "final", [], "any", false, false, false, 43);
                        // line 44
                        echo "                                                ";
                        if (($context["isOrdered"] ?? null)) {
                            // line 45
                            echo "                                                    ";
                            $context["active"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["step"], "active", [], "any", false, false, false, 45);
                            // line 46
                            echo "
                                                    ";
                            // line 47
                            if ((($context["activeRendered"] ?? null) && ($context["manyVariantsFound"] ?? null))) {
                                // line 48
                                echo "                                                        ";
                                $context["manyVariantsRendered"] = true;
                                // line 49
                                echo "                                                        ";
                                $context["step"] = twig_array_merge($context["step"], ["label" => "..."]);
                                // line 50
                                echo "                                                    ";
                            }
                            // line 51
                            echo "
                                                    ";
                            // line 52
                            if ((($context["active"] ?? null) &&  !($context["activeRendered"] ?? null))) {
                                // line 53
                                echo "                                                        ";
                                $context["activeRendered"] = true;
                                // line 54
                                echo "                                                    ";
                            }
                            // line 55
                            echo "
                                                    ";
                            // line 56
                            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["step"], "possibleStepsCount", [], "any", false, false, false, 56) > 1) && ($context["activeRendered"] ?? null))) {
                                // line 57
                                echo "                                                        ";
                                $context["manyVariantsFound"] = true;
                                // line 58
                                echo "                                                    ";
                            }
                            // line 59
                            echo "
                                                    <li class=\"workflow-steps-item workflow-ordered-steps";
                            // line 60
                            if (($context["active"] ?? null)) {
                                echo " active";
                            }
                            if (($context["final"] ?? null)) {
                                echo " workflow-step-final";
                            }
                            echo "\" title=\"";
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["step"], "label", [], "any", false, false, false, 60), [], "workflows"), "html", null, true);
                            echo "\">
                                                        <span class=\"workflow-step-name\">";
                            // line 61
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["step"], "label", [], "any", false, false, false, 61), [], "workflows"), "html", null, true);
                            echo "</span>
                                                    </li>
                                                ";
                        } else {
                            // line 64
                            echo "                                                    <li class=\"workflow-steps-item workflow-unordered-steps\">
                                                        <span class=\"workflow-unordered-step-name";
                            // line 65
                            if (($context["final"] ?? null)) {
                                echo " workflow-step-final-name";
                            }
                            echo "\">";
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["step"], "label", [], "any", false, false, false, 65), [], "workflows"), "html", null, true);
                            echo "</span>
                                                    </li>
                                                ";
                        }
                        // line 68
                        echo "                                            ";
                    }
                    // line 69
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['step'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "                                    </ul>
                                </div>
                            </td>
                            ";
                // line 73
                if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "transitionsData", [], "any", false, false, false, 73)) != 0)) {
                    // line 74
                    echo "                                <td class=\"workflow-controls\">
                                    <div class=\"workflow-buttons-scroller\">
                                        ";
                    // line 76
                    $this->loadTemplate("@OroWorkflow/Widget/widget/button.html.twig", "@OroWorkflow/Widget/widget/entityWorkflows.html.twig", 76)->display(twig_array_merge($context, ["workflow" => $context["workflow"], "entity_id" => ($context["entityId"] ?? null), "originalUrl" => ($context["originalUrl"] ?? null)]));
                    // line 77
                    echo "                                    </div>
                                </td>
                            ";
                }
                // line 80
                echo "                        </tr>
                    </table>
                </div>
            </div>
        ";
                // line 84
                if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 84) > 1) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 84))) {
                    // line 85
                    echo "        </div>
        ";
                }
                // line 87
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['workflow'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 88
            echo "</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/widget/entityWorkflows.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  288 => 88,  274 => 87,  270 => 85,  268 => 84,  262 => 80,  257 => 77,  255 => 76,  251 => 74,  249 => 73,  244 => 70,  238 => 69,  235 => 68,  225 => 65,  222 => 64,  216 => 61,  205 => 60,  202 => 59,  199 => 58,  196 => 57,  194 => 56,  191 => 55,  188 => 54,  185 => 53,  183 => 52,  180 => 51,  177 => 50,  174 => 49,  171 => 48,  169 => 47,  166 => 46,  163 => 45,  160 => 44,  157 => 43,  154 => 42,  150 => 41,  147 => 40,  144 => 39,  141 => 38,  139 => 37,  133 => 36,  129 => 34,  126 => 33,  123 => 32,  121 => 31,  118 => 30,  115 => 29,  113 => 28,  103 => 21,  99 => 20,  91 => 18,  80 => 12,  76 => 10,  73 => 9,  67 => 7,  64 => 6,  47 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/widget/entityWorkflows.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/widget/entityWorkflows.html.twig");
    }
}
