<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Attribute/create.html.twig */
class __TwigTemplate_f9f36306585c9526b63aa39aa4fc665f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroEntityExtend/ConfigFieldGrid/create.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute.entity_label")]]);
        $this->parent = $this->loadTemplate("@OroEntityExtend/ConfigFieldGrid/create.html.twig", "@OroEntityConfig/Attribute/create.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Attribute/create.html.twig", 6)->unwrap();
        // line 7
        echo "
    ";
        // line 8
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_index", ["alias" => ($context["entityAlias"] ?? null)])], 8, $context, $this->getSourceContext());
        echo "
    ";
        // line 9
        echo twig_call_macro($macros["UI"], "macro_saveActionButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Continue"), "route" => "oro_attribute_save", "params" => ["alias" =>         // line 12
($context["entityAlias"] ?? null)]]], 9, $context, $this->getSourceContext());
        // line 13
        echo "
";
    }

    // line 16
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_index", ["alias" =>         // line 19
($context["entityAlias"] ?? null)]), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 20
($context["attributesLabel"] ?? null)), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute.entity_label")])];
        // line 23
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "

";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Attribute/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 23,  78 => 20,  77 => 19,  75 => 17,  71 => 16,  66 => 13,  64 => 12,  63 => 9,  59 => 8,  56 => 7,  53 => 6,  49 => 5,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Attribute/create.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Attribute/create.html.twig");
    }
}
