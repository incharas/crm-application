<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/datepicker.scss */
class __TwigTemplate_9499d07baee5d73494c81a3ac035a319 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

.datepicker-box {
    position: \$datepicker-box-position;

    &__icon {
        position: \$datepicker-box-icon-position;
        left: \$datepicker-box-icon-left;
        top: \$datepicker-box-icon-top;
        z-index: \$datepicker-box-icon-z-index;

        font-size: \$datepicker-box-icon-font-size;
        line-height: \$datepicker-box-icon-line-height;

        color: \$datepicker-box-icon-color;

        pointer-events: \$datepicker-box-icon-pointer-events;
    }

    .datepicker-input {
        display: \$datepicker-input-display;
        max-width: \$datepicker-input-max-width;
        padding-left: \$datepicker-input-padding-left;
        position: \$datepicker-input-position;

        &::-webkit-calendar-picker-indicator {
            width: \$datepicker-input-padding-left;
            height: auto;
            position: absolute;
            top: 0;
            left: 0;
            right: auto;
            bottom: 0;
        }
    }

    &--form-mode {
        .datepicker-input {
            max-width: \$datepicker-box-form-mode-datepicker-input-max-width;
            width: \$datepicker-box-form-mode-datepicker-input-width;
        }

        .datepicker-box__icon {
            top: \$datepicker-input-form-mode-icon-top;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/datepicker.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/datepicker.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/datepicker.scss");
    }
}
