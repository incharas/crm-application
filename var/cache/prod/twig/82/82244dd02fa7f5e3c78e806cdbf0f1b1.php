<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/HashNav/hashNavAjax.html.twig */
class __TwigTemplate_604ca5ba56467022f42785f8bc816b88 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo json_encode($this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->getContent(((array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), [])) : ([]))));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/HashNav/hashNavAjax.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/HashNav/hashNavAjax.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/HashNav/hashNavAjax.html.twig");
    }
}
