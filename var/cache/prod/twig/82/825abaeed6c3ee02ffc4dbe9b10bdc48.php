<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/variables/oro-toolbar-config.scss */
class __TwigTemplate_22ac5a7ac9ae87162610dff2de4fab6b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

\$oro-toolbar-content-min-width: 45px !default;
\$oro-toolbar-toolbar-min-width: 95px !default;
\$oro-toolbar-icon-offset: 3px !default;
\$oro-toolbar-content-inner-offset: 0 \$offset-y-m !default;
\$oro-toolbar-content-line-height: (\$base-font-size - .1) * 2 !default;
\$oro-toolbar-content-font-family: \$base-font-minor !default;
\$oro-toolbar-content-text-align: center !default;
\$oro-toolbar-dropdown-inner-offset: 10px 15px !default;
\$oro-toolbar-dropdown-z-index: z('dropdown') + 30 !default;
\$oro-toolbar-list-item-offset: 2px !default;
\$oro-toolbar-link-color: get-color('additional', 'dark') !default;
\$oro-toolbar-link-hover-color: get-color('additional', 'dark') !default;

\$oro-toolbar-open-content-box-shadow: inset 1px 1px 3px 0 rgba(0 0 0 / 30%) !default;

\$oro-toolbar-fullscreen-link-border: 1px solid get-color('additional', 'light') !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/variables/oro-toolbar-config.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/variables/oro-toolbar-config.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/variables/oro-toolbar-config.scss");
    }
}
