<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAction/Widget/updateNavButtons.html.twig */
class __TwigTemplate_d7a5aea64ff896c22aa6446f6709de58 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAction/Widget/updateNavButtons.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["params"] = twig_array_merge($this->extensions['Oro\Bundle\ActionBundle\Twig\OperationExtension']->getActionParameters($context), ["group" => "update_navButtons"]);
        // line 4
        echo "
";
        // line 5
        ob_start(function () { return ''; });
        // line 6
        echo "    ";
        $this->loadTemplate("@OroAction/Widget/_widget.html.twig", "@OroAction/Widget/updateNavButtons.html.twig", 6)->display($context);
        $context["buttons"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 9
        if (twig_length_filter($this->env, twig_trim_filter(($context["buttons"] ?? null)))) {
            // line 10
            echo twig_escape_filter($this->env, ($context["buttons"] ?? null), "html", null, true);
            echo "

    ";
            // line 12
            if ($this->extensions['Oro\Bundle\ActionBundle\Twig\OperationExtension']->hasButtons(($context["params"] ?? null))) {
                // line 13
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 13, $context, $this->getSourceContext());
                echo "
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "@OroAction/Widget/updateNavButtons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 13,  60 => 12,  55 => 10,  53 => 9,  49 => 6,  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAction/Widget/updateNavButtons.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActionBundle/Resources/views/Widget/updateNavButtons.html.twig");
    }
}
