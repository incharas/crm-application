<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroComment/Comment/js/list.html.twig */
class __TwigTemplate_da3fd41815d672b9fbb30bd7a667018f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "<script type=\"text/html\" id=\"";
        echo twig_escape_filter($this->env, (($context["id"] ?? null) . "-form"), "html_attr");
        echo "\">
    ";
        // line 3
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->prepareJsTemplateContent($this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_comment_form")));
        echo "
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroComment/Comment/js/list.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 3,  37 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroComment/Comment/js/list.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CommentBundle/Resources/views/Comment/js/list.html.twig");
    }
}
