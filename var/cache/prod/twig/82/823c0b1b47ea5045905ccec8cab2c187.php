<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/sticky-element/sticky-element-view.js */
class __TwigTemplate_ceb5f64f28a81dcaf83e72e23070897b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const BaseView = require('oroui/js/app/views/base/view');
    const stickyElementMixin = require('oroui/js/app/views/sticky-element/sticky-element-mixin');

    const StickyElementView = BaseView.extend(_.extend({}, stickyElementMixin, {
        /**
         * @inheritdoc
         */
        constructor: function StickyElementView(options) {
            StickyElementView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            StickyElementView.__super__.initialize.call(this, options);

            this.initializeSticky({
                \$stickyElement: \$(options.el),
                stickyOptions: options.stickyOptions || {}
            });
        },

        /**
         * @inheritdoc
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }

            this.disposeSticky();

            StickyElementView.__super__.dispose.call(this);
        }
    }));

    return StickyElementView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/sticky-element/sticky-element-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/sticky-element/sticky-element-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/sticky-element/sticky-element-view.js");
    }
}
