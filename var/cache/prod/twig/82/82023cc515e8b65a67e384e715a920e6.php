<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImap/Form/accountTypeAuthorized.html.twig */
class __TwigTemplate_38759f785f71391146ca233e27add552 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<fieldset class=\"form-horizontal\">
    ";
        // line 2
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, true, false, 2), "userEmailOrigin", [], "any", true, true, false, 2)) {
            // line 3
            echo "        <div>";
            // line 4
            if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, false, false, 4), "userEmailOrigin", [], "any", false, false, false, 4), "parent", [], "any", false, false, false, 4))) {
                // line 5
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, false, false, 5), "userEmailOrigin", [], "any", false, false, false, 5), 'errors');
            }
            // line 8
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, true, false, 8), "userEmailOrigin", [], "any", false, true, false, 8), "check", [], "any", true, true, false, 8)) {
                // line 9
                echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
                // line 11
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, false, false, 11), "userEmailOrigin", [], "any", false, false, false, 11), "check", [], "any", false, false, false, 11), 'widget');
                echo "
                    </div>
                </div>
            ";
            }
            // line 15
            echo "
            ";
            // line 16
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, true, false, 16), "userEmailOrigin", [], "any", false, true, false, 16), "checkFolder", [], "any", true, true, false, 16)) {
                // line 17
                echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
                // line 19
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, false, false, 19), "userEmailOrigin", [], "any", false, false, false, 19), "checkFolder", [], "any", false, false, false, 19), 'widget');
                echo "
                    </div>
                </div>
            ";
            }
            // line 23
            echo "
            <div class=\"control-group\">
                <div class=\"controls\">
                    <div class=\"google-alert google-connection-status alert alert-error\" role=\"alert\" style=\"display: none\"></div>
                </div>
            </div>";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, false, false, 30), "userEmailOrigin", [], "any", false, false, false, 30), 'rest');
            // line 31
            echo "</div>
    ";
        }
        // line 33
        echo "</fieldset>
";
    }

    public function getTemplateName()
    {
        return "@OroImap/Form/accountTypeAuthorized.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 33,  87 => 31,  85 => 30,  78 => 23,  71 => 19,  67 => 17,  65 => 16,  62 => 15,  55 => 11,  51 => 9,  49 => 8,  46 => 5,  44 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImap/Form/accountTypeAuthorized.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImapBundle/Resources/views/Form/accountTypeAuthorized.html.twig");
    }
}
