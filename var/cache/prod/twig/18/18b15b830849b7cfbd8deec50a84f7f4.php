<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/no-data.scss */
class __TwigTemplate_518666f87e557c54b6b16fc2c8c3c566 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:selector';

// More about placeholders https://github.com/sass/sass/issues/2808#issuecomment-574413393
%base-no-data,
.no-data {
    margin: \$no-data-offset;
    padding: 0 \$content-padding;
    text-align: center;
    font-size: \$no-data-font-size;
    line-height: \$no-data-line-height;

    color: \$no-data-color;

    white-space: normal;

    // Floating the elements, such as quickly accessible buttons close to datagrids, are not allowed to float on both sides
    clear: both;

    @at-root #{selector.append(&, '__title')} {
        margin: 0;

        font-size: \$no-data-title-font-size;
        line-height: 1;
        font-weight: \$no-data-title-font-weight;

        color: \$no-data-title-color;

        @include fa-icon(\$no-data-icon, before, true) {
            display: inline-block;

            margin-right: \$no-data-icon-offset;
        }

        &:first-child {
            margin-bottom: \$no-data-title-offset;
        }

        &:last-child {
            margin-bottom: 0;
        }
    }

    @at-root #{selector.append(&, '__text')} {
        margin: 0;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/no-data.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/no-data.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/no-data.scss");
    }
}
