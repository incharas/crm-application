<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataAudit/Audit/widget/history.html.twig */
class __TwigTemplate_1cd1b9414c4922e8971495f5b7acc219 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroDataAudit/Audit/widget/history.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    ";
        // line 4
        $context["fieldName"] = ((array_key_exists("fieldName", $context)) ? (($context["fieldName"] ?? null)) : (""));
        // line 5
        echo "    ";
        $context["scope"] = ((($context["entityClass"] ?? null) . ($context["entityId"] ?? null)) . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, twig_date_converter($this->env), "timestamp", [], "any", false, false, false, 5));
        // line 6
        echo "
    <div class=\"container-fluid\">
        ";
        // line 8
        $this->displayBlock('content', $context, $blocks);
        // line 15
        echo "    </div>
</div>
";
    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "            ";
        // line 10
        echo "            ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [$this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName(        // line 11
($context["gridName"] ?? null), ($context["scope"] ?? null)), ["object_class" =>         // line 12
($context["entityClass"] ?? null), "object_id" => ($context["entityId"] ?? null), "field_name" => ($context["fieldName"] ?? null)]], 10, $context, $this->getSourceContext());
        // line 13
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "@OroDataAudit/Audit/widget/history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 13,  70 => 12,  69 => 11,  67 => 10,  65 => 9,  61 => 8,  55 => 15,  53 => 8,  49 => 6,  46 => 5,  44 => 4,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataAudit/Audit/widget/history.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataAuditBundle/Resources/views/Audit/widget/history.html.twig");
    }
}
