<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/SystemCalendar/widget/events.html.twig */
class __TwigTemplate_87847dcce4b0a89adc0a834cc438262e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroCalendar/SystemCalendar/widget/events.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    ";
        // line 4
        $context["scope"] = ("system-calendar-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 4));
        // line 5
        echo "    ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [$this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["entity"] ?? null), "public", [], "any", false, false, false, 7)) ? ("public-system-calendar-event-grid") : ("system-calendar-event-grid")),         // line 8
($context["scope"] ?? null)), ["calendarId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 10
($context["entity"] ?? null), "id", [], "any", false, false, false, 10)]], 5, $context, $this->getSourceContext());
        // line 11
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/SystemCalendar/widget/events.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 11,  49 => 10,  48 => 8,  47 => 7,  45 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/SystemCalendar/widget/events.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/SystemCalendar/widget/events.html.twig");
    }
}
