<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/application_menu_desktop_top.html.twig */
class __TwigTemplate_9f0c0725c968afe8f30e315cf11cef08 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'list_wrapper' => [$this, 'block_list_wrapper'],
            'item' => [$this, 'block_item'],
            'linkElement' => [$this, 'block_linkElement'],
            'children' => [$this, 'block_children'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/application_menu_desktop_top.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_list_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 4)) {
            // line 5
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 5) == 1)) {
                // line 6
                echo "            <div class=\"dropdown-menu-wrapper dropdown-menu-wrapper__placeholder\" data-role=\"sub-menu\" aria-hidden=\"true\">
                <div class=\"dropdown-menu-wrapper dropdown-menu-wrapper__scrollable\">";
                // line 8
                $this->displayBlock("list", $context, $blocks);
                // line 9
                echo "</div>
            </div>
        ";
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["item"] ?? null), "level", [], "any", false, false, false, 11) == 2)) {
                // line 12
                echo "            <div class=\"dropdown-menu-wrapper dropdown-menu-wrapper__child\" data-role=\"sub-menu\" aria-hidden=\"true\">";
                // line 13
                $this->displayBlock("list", $context, $blocks);
                // line 14
                echo "</div>
        ";
            } else {
                // line 16
                $this->displayBlock("list", $context, $blocks);
            }
            // line 18
            echo "    ";
        }
    }

    // line 21
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 22) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 22))) {
            // line 23
            $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => "dropdown", 1 => ("dropdown-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 23))]);
            // line 24
            $context["childrenClasses"] = twig_array_merge(($context["childrenClasses"] ?? null), [0 => "dropdown-menu", 1 => ("dropdown-menu-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 24))]);
        } else {
            // line 26
            $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => "dropdown-menu-single-item", 1 => ("dropdown-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 26))]);
        }
        // line 28
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", true, true, false, 28) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 28), "routes", [], "any", true, true, false, 28)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 28), "routes", [], "any", false, true, false, 28), 0, [], "array", true, true, false, 28))) {
            // line 29
            $context["itemAttributes"] = twig_array_merge(($context["itemAttributes"] ?? null), ["data-route" => (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 29), "routes", [], "any", false, false, false, 29)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null)]);
            // line 30
            if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 30), "routes", [], "any", false, false, false, 30)) > 1)) {
                // line 31
                $context["itemAttributes"] = twig_array_merge(($context["itemAttributes"] ?? null), ["data-routes" => json_encode(twig_slice($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 31), "routes", [], "any", false, false, false, 31), 1))]);
            }
        }
        // line 35
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 35) == 1)) {
            // line 36
            echo "        ";
            $context["hasValidChildren"] = false;
            // line 37
            echo "
        ";
            // line 38
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["item"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_item"]) {
                // line 39
                echo "            ";
                if ( !($context["hasValidChildren"] ?? null)) {
                    // line 40
                    $context["showNonAuthorized"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "extras", [], "any", false, true, false, 40), "show_non_authorized", [], "any", true, true, false, 40) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "extras", [], "any", false, false, false, 40), "show_non_authorized", [], "any", false, false, false, 40));
                    // line 41
                    $context["displayable"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "extras", [], "any", false, false, false, 41), "isAllowed", [], "any", false, false, false, 41) || ($context["showNonAuthorized"] ?? null));
                    // line 42
                    if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "displayed", [], "any", false, false, false, 42) && ($context["displayable"] ?? null)) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "getExtra", [0 => "divider"], "method", false, false, false, 42))) {
                        // line 43
                        echo "                    ";
                        $context["hasValidChildren"] = true;
                        // line 44
                        echo "                ";
                    }
                    // line 45
                    echo "            ";
                }
                // line 46
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 47
            echo "
        ";
            // line 48
            if (((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 48)) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 48) != "#")) || ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 48) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 48)) && ($context["hasValidChildren"] ?? null)))) {
                // line 49
                echo "            ";
                $this->displayBlock("item_renderer", $context, $blocks);
                echo "
        ";
            }
            // line 51
            echo "    ";
        } else {
            // line 52
            echo "        ";
            $this->displayBlock("item_renderer", $context, $blocks);
            echo "
    ";
        }
    }

    // line 56
    public function block_linkElement($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 57
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/application_menu_desktop_top.html.twig", 57)->unwrap();
        // line 58
        echo "
    ";
        // line 59
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 60
            echo "    ";
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [            // line 61
($context["linkAttributes"] ?? null), "class", [0 => "dropdown-toggle"]], 61, $context, $this->getSourceContext())]);
            // line 63
            echo "    ";
        }
        // line 64
        echo "
    ";
        // line 65
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 65) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 65)) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 65) === 0))) {
            // line 66
            echo "        ";
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["data-toggle" => "dropdown"]);
            // line 69
            echo "    ";
        }
        // line 70
        echo "
    ";
        // line 71
        if ((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 71)) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 71) == "#"))) {
            // line 72
            echo "        ";
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [            // line 73
($context["linkAttributes"] ?? null), "class", [0 => "unclickable"]], 73, $context, $this->getSourceContext())]);
            // line 75
            echo "    ";
        }
        // line 76
        echo "
    <a href=\"";
        // line 77
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 77), "html", null, true);
        echo "\"";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 77, $context, $this->getSourceContext());
        echo ">
        <span class=\"title ";
        // line 78
        echo twig_escape_filter($this->env, ("title-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 78)), "html", null, true);
        echo "\">";
        $this->displayBlock("label", $context, $blocks);
        echo "</span>
    </a>
";
    }

    // line 82
    public function block_children($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 83
        ob_start(function () { return ''; });
        // line 84
        $this->displayParentBlock("children", $context, $blocks);
        $context["content"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 86
        if ((($context["content"] ?? null) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 86) === 1))) {
            // line 87
            $context["labelAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "labelAttributes", [], "any", false, false, false, 87);
            // line 88
            echo "        <li class=\"dropdown-menu-title ";
            echo twig_escape_filter($this->env, ("dropdown-menu-title-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 88)), "html", null, true);
            echo "\">";
            $this->displayBlock("spanElement", $context, $blocks);
            echo "</li>
        <li class=\"divider\"><span></span></li>
    ";
        }
        // line 91
        echo ($context["content"] ?? null);
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/application_menu_desktop_top.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  249 => 91,  240 => 88,  238 => 87,  236 => 86,  233 => 84,  231 => 83,  227 => 82,  218 => 78,  212 => 77,  209 => 76,  206 => 75,  204 => 73,  202 => 72,  200 => 71,  197 => 70,  194 => 69,  191 => 66,  189 => 65,  186 => 64,  183 => 63,  181 => 61,  179 => 60,  177 => 59,  174 => 58,  171 => 57,  167 => 56,  159 => 52,  156 => 51,  150 => 49,  148 => 48,  145 => 47,  139 => 46,  136 => 45,  133 => 44,  130 => 43,  128 => 42,  126 => 41,  124 => 40,  121 => 39,  117 => 38,  114 => 37,  111 => 36,  109 => 35,  105 => 31,  103 => 30,  101 => 29,  99 => 28,  96 => 26,  93 => 24,  91 => 23,  89 => 22,  85 => 21,  80 => 18,  77 => 16,  73 => 14,  71 => 13,  69 => 12,  67 => 11,  63 => 9,  61 => 8,  58 => 6,  55 => 5,  53 => 4,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/application_menu_desktop_top.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/application_menu_desktop_top.html.twig");
    }
}
