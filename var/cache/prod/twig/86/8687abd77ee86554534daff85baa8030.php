<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/loading/loading-bar.scss */
class __TwigTemplate_c49395cdeb5a99278c23352f3b2df02f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.loading-bar {
    display: \$loading-bar-display;
    position: \$loading-bar-position;
    bottom: \$loading-bar-bottom;
    left: \$loading-bar-left;
    height: \$loading-bar-height;
    background: \$loading-bar-background;
    box-shadow: \$loading-bar-box-shadow;
    border-radius: \$loading-bar-border-radius;
    transition: \$loading-bar-transition;
    animation: \$loading-bar-animation;
    overflow: \$loading-bar-overflow;

    &::before {
        content: '';
        background: \$loading-bar-point-background;
        width: \$loading-bar-point-width;
        height: \$loading-bar-point-height;
        position: \$loading-bar-point-position;
        box-shadow: \$loading-bar-point-box-shadow;
        top: \$loading-bar-point-top;
        animation: \$loading-bar-point-animation;
    }

    &__actual-progress {
        position: \$loading-bar-block-process-position;
        animation: none;
    }
}

@keyframes line-point {
    0% {
        left: 0;
        transform: translateX(-100%);
    }

    100% {
        left: 100%;
        transform: translateX(0);
    }
}

@keyframes line-loader {
    0% {
        width: 1%;
    }

    5% {
        width: 5%;
    }

    10% {
        width: 15%;
    }

    15% {
        width: 25%;
    }

    25% {
        width: 35%;
    }

    50% {
        width: 50%;
    }

    80% {
        width: 75%;
    }

    100% {
        width: 85%;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/loading/loading-bar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/loading/loading-bar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/loading/loading-bar.scss");
    }
}
