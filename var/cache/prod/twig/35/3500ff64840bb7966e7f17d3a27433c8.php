<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Calendar/view.html.twig */
class __TwigTemplate_0cd4d11d5b6ec651e303ea5ad7ec7c5b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'calendar_connections' => [$this, 'block_calendar_connections'],
            'calendar_events' => [$this, 'block_calendar_events'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["calendarTemplates"] = $this->macros["calendarTemplates"] = $this->loadTemplate("@OroCalendar/templates.html.twig", "@OroCalendar/Calendar/view.html.twig", 2)->unwrap();
        // line 4
        $context["name"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 4)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"));

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%username%" =>         // line 5
($context["name"] ?? null), "%calendarname%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", true, true, false, 5)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 5), ($context["name"] ?? null))) : (($context["name"] ?? null)))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroCalendar/Calendar/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/Calendar/view.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_calendar_event_view")) {
            // line 11
            echo "        <div class=\"btn-group\">
            ";
            // line 12
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_index"), "iCss" => "fa-clock-o", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.view_events"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.view_events")]], 12, $context, $this->getSourceContext());
            // line 17
            echo "
        </div>
    ";
        }
    }

    // line 22
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "    ";
        if ( !array_key_exists("breadcrumbs", $context)) {
            // line 24
            echo "        ";
            $context["breadcrumbs"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.entity_label")]];
            // line 25
            echo "        ";
            if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 25))) {
                // line 26
                echo "            ";
                $context["breadcrumbs"] = twig_array_merge(($context["breadcrumbs"] ?? null), [0 => ["label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 26)]]);
                // line 27
                echo "        ";
            }
            // line 28
            echo "    ";
        }
        // line 29
        echo "    ";
        $this->loadTemplate("@OroNavigation/Menu/breadcrumbs.html.twig", "@OroCalendar/Calendar/view.html.twig", 29)->display($context);
    }

    // line 32
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "    <div class=\"calendar-title-wrapper navbar-extra\">
        ";
        // line 34
        if ( !array_key_exists("breadcrumbs", $context)) {
            // line 35
            echo "            ";
            $context["breadcrumbs"] = ["entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.entity_label")];
            // line 36
            echo "        ";
        }
        // line 37
        $this->displayParentBlock("pageHeader", $context, $blocks);
        // line 38
        echo "</div>
";
    }

    // line 41
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 43
    public function block_calendar_connections($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "    <div class=\"calendars\">
        ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["user_select_form"] ?? null), 'row');
        echo "
        <div class=\"calendar-connections\"></div>
    </div>
";
    }

    // line 50
    public function block_calendar_events($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    <div class=\"calendar-events\"></div>
";
    }

    // line 54
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 55
        echo "    ";
        $context["calendarOptions"] = ["calendar" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 56
($context["entity"] ?? null), "id", [], "any", false, false, false, 56), "calendarOptions" =>         // line 57
($context["calendar"] ?? null), "eventsItemsJson" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_calendarevents", ["calendar" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 58
($context["entity"] ?? null), "id", [], "any", false, false, false, 58), "start" => twig_date_format_filter($this->env, ($context["startDate"] ?? null), "c"), "end" => twig_date_format_filter($this->env, ($context["endDate"] ?? null), "c"), "subordinate" => true])), "connectionsItemsJson" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_calendar_connections", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 59
($context["entity"] ?? null), "id", [], "any", false, false, false, 59)])), "eventsOptions" => ["defaultDate" => twig_date_format_filter($this->env, "now", "c", $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()), "containerSelector" => ".calendar-events", "itemViewTemplateSelector" => "#template-view-calendar-event", "itemFormTemplateSelector" => "#template-calendar-event", "leftHeader" => "prev,next today", "centerHeader" => "title", "rightHeader" => "month,agendaWeek,agendaDay", "enableAttendeesInvitations" => $this->extensions['Oro\Bundle\CalendarBundle\Twig\AttendeesExtension']->isAttendeesInvitationEnabled()], "connectionsOptions" => ["modalContentTemplateId" => (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) ? ("connections-modal-content-template") : (null)), "containerSelector" => ".calendar-connections", "containerTemplateSelector" => "#template-calendar-connections", "itemTemplateSelector" => "#template-calendar-connection-item"], "colorManagerOptions" => ["colors" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_calendar.calendar_colors")], "invitationStatuses" => [0 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_ACCEPTED"), 1 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_TENTATIVE"), 2 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED")]];
        // line 86
        echo "
    <div class=\"sidebar-container\">
        <div id=\"calendar\" ";
        // line 88
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            echo " class=\"content-with-sidebar--container\"";
        }
        // line 89
        echo "             data-page-component-module=\"orocalendar/js/app/components/calendar-component\"
             data-page-component-options=\"";
        // line 90
        echo twig_escape_filter($this->env, json_encode(($context["calendarOptions"] ?? null)), "html", null, true);
        echo "\"
        >
        ";
        // line 92
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 93
            echo "            <div class=\"calendar-connections-header\">
                <div class=\"dropdown\">
                    <button data-role=\"show-connections-modal\" class=\"btn btn-large\">
                        <span class=\"fa-filter\" aria-hidden=\"true\"></span>
                        ";
            // line 97
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.select"), "html", null, true);
            echo "
                    </button>
                </div>
            </div>
            ";
            // line 101
            $this->displayBlock("calendar_events", $context, $blocks);
            echo "
            <script id=\"";
            // line 102
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["calendarOptions"] ?? null), "connectionsOptions", [], "any", false, false, false, 102), "modalContentTemplateId", [], "any", false, false, false, 102), "html", null, true);
            echo "\" type=\"text/template\">
                <div data-layout=\"separate\">";
            // line 103
            $this->displayBlock("calendar_connections", $context, $blocks);
            echo "</div>
            </script>
        ";
        } else {
            // line 106
            echo "            <div class=\"content-with-sidebar--sidebar calendar-sidebar\" data-role=\"calendar-sidebar\">
                ";
            // line 107
            $this->displayBlock("calendar_connections", $context, $blocks);
            echo "
            </div>
            <div class=\"content-with-sidebar--content\">
                <div class=\"category-data\">
                    ";
            // line 111
            $this->displayBlock("calendar_events", $context, $blocks);
            echo "
                </div>
            </div>
        ";
        }
        // line 115
        echo "        </div>
    </div>
    <script type=\"text/template\" id=\"template-calendar-menu\">
        ";
        // line 118
        echo $this->extensions['Oro\Bundle\NavigationBundle\Twig\MenuExtension']->render((($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) ? ("calendar_menu_mobile") : ("calendar_menu")));
        echo "
    </script>

    <script type=\"text/template\" id=\"template-calendar-connections\">
        <ul class=\"connection-container\">
        </ul>
    </script>

    <script type=\"text/template\" id=\"template-calendar-connection-item\">
        ";
        // line 127
        ob_start(function () { return ''; });
        // line 128
        echo "        <li class=\"connection-item<% if (visible) { %> active<% } %>\">
            <span class=\"connection-item-label checkbox-label\" data-role=\"connection-label\">
                <input data-role=\"color-checkbox\" type=\"checkbox\"
                    <% if (visible) { %> checked <% } %>
                    <% if (!_.isEmpty(backgroundColor)) { %> style=\"--checkbox-skin-color: <%- backgroundColor %>;\"<% } %>
                >
                <span class=\"user-calendar\" title=\"<%- calendarName %>\"><%- calendarName %></span>
            </span>
            <div class=\"connection-menu-container dropdown\">
                ";
        // line 137
        $context["connectionTogglerId"] = uniqid("connection-dropdown-");
        // line 138
        echo "                <button id=\"";
        echo twig_escape_filter($this->env, ($context["connectionTogglerId"] ?? null), "html", null, true);
        echo "\"
                        class=\"btn btn-square-light context-menu-button\"
                        data-toggle=\"dropdown\"
                        aria-haspopup=\"true\" aria-expanded=\"false\"
                        data-target=\"";
        // line 142
        echo twig_escape_filter($this->env, ($context["connectionTogglerId"] ?? null), "html", null, true);
        echo "\"
                        data-placement=\"bottom-end\"
                >
                    <span class=\"fa-ellipsis-h fa-offset-none\" aria-hidden=\"true\"></span>
                </button>
                <div class=\"dropdown-menu\" aria-labelledby=\"";
        // line 147
        echo twig_escape_filter($this->env, ($context["connectionTogglerId"] ?? null), "html", null, true);
        echo "\">
                    <div data-role=\"connection-menu-content\"></div>
                </div>
            </div>
        </li>
        ";
        $___internal_parse_85_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 127
        echo twig_spaceless($___internal_parse_85_);
        // line 153
        echo "    </script>

    ";
        // line 155
        echo twig_call_macro($macros["calendarTemplates"], "macro_calendar_event_view_template", ["template-view-calendar-event"], 155, $context, $this->getSourceContext());
        echo "
    ";
        // line 156
        echo twig_call_macro($macros["calendarTemplates"], "macro_calendar_event_form_template", ["template-calendar-event", ($context["event_form"] ?? null)], 156, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Calendar/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  303 => 156,  299 => 155,  295 => 153,  293 => 127,  284 => 147,  276 => 142,  268 => 138,  266 => 137,  255 => 128,  253 => 127,  241 => 118,  236 => 115,  229 => 111,  222 => 107,  219 => 106,  213 => 103,  209 => 102,  205 => 101,  198 => 97,  192 => 93,  190 => 92,  185 => 90,  182 => 89,  178 => 88,  174 => 86,  172 => 59,  171 => 58,  170 => 57,  169 => 56,  167 => 55,  163 => 54,  158 => 51,  154 => 50,  146 => 45,  143 => 44,  139 => 43,  133 => 41,  128 => 38,  126 => 37,  123 => 36,  120 => 35,  118 => 34,  115 => 33,  111 => 32,  106 => 29,  103 => 28,  100 => 27,  97 => 26,  94 => 25,  91 => 24,  88 => 23,  84 => 22,  77 => 17,  75 => 12,  72 => 11,  70 => 10,  67 => 9,  64 => 8,  60 => 7,  55 => 1,  53 => 5,  50 => 4,  48 => 2,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Calendar/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Calendar/view.html.twig");
    }
}
