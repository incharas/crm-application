<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMessageQueue/Job/Datagrid/percent.html.twig */
class __TwigTemplate_cd15548fa1bcd4e95359bd338f6b87d8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroMessageQueue/macros.html.twig", "@OroMessageQueue/Job/Datagrid/percent.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["job"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "rootEntity", [], "any", false, false, false, 3);
        // line 4
        $context["progress"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["job"] ?? null), "jobProgress", [], "any", false, false, false, 4) * 100);
        // line 5
        echo "
<div class=\"progress\">
    <div class=\"progress-bar ";
        // line 7
        echo twig_call_macro($macros["UI"], "macro_getJobStatusClass", [($context["job"] ?? null)], 7, $context, $this->getSourceContext());
        echo "\" style=\"width:";
        echo twig_escape_filter($this->env, ($context["progress"] ?? null), "html", null, true);
        echo "%;\"></div>
</div>
<b class=\"progress-label\"> ";
        // line 9
        echo twig_escape_filter($this->env, ($context["progress"] ?? null), "html", null, true);
        echo "%</b>
";
    }

    public function getTemplateName()
    {
        return "@OroMessageQueue/Job/Datagrid/percent.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 9,  50 => 7,  46 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMessageQueue/Job/Datagrid/percent.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MessageQueueBundle/Resources/views/Job/Datagrid/percent.html.twig");
    }
}
