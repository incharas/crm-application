<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/scrollspy.scss */
class __TwigTemplate_aefe8143f760421b20f8edfb5ec5cb4e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$scrollspy-position: relative !default;
\$scrollspy-overflow: auto !default;

\$scrollspy-nav-background: \$primary-860 !default;
\$scrollspy-nav-offset: null !default;
\$scrollspy-nav-border-radius: 16px !default;
\$scrollspy-nav-position: relative !default;
\$scrollspy-nav-z-index: 2 !default;

\$scrollspy-nav-gradient: linear-gradient(to bottom, #fff, rgba(255 255 255 / 0%)) !default;
\$scrollspy-nav-gradient-height: 16px !default;

\$scrollspy-nav-target-height: 20px !default;

\$scrollspy-nav-navbar-inner-nav-link-inner-offset: 6px 16px !default;
\$scrollspy-nav-navbar-inner-nav-link-color: \$primary-100 !default;
\$scrollspy-nav-navbar-inner-nav-link-border-radius: 16px !default;
\$scrollspy-nav-navbar-inner-nav-link-background-color: \$primary-860 !default;
\$scrollspy-nav-navbar-inner-nav-link-active-background-color: \$primary-800 !default;
\$scrollspy-nav-navbar-inner-nav-link-active-font-weight: font-weight('bold') !default;

\$scrollspy-title-background: \$primary-860 !default;
\$scrollspy-title-font-size: \$base-font-size !default;
\$scrollspy-title-font-weight: font-weight('bold') !default;
\$scrollspy-title-color: \$primary-100 !default;
\$scrollspy-title-offset: 0 !default;
\$scrollspy-title-inner-offset: 8px 16px !default;
\$scrollspy-title-border-radius: 4px !default;

\$scrollspy-data-scroll-focus-outline: 0 none !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/scrollspy.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/scrollspy.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/scrollspy.scss");
    }
}
