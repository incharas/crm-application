<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/badge.scss */
class __TwigTemplate_103629b3cd2e434c11d56e75e1799055 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@mixin badge(
    \$badge-size:        26px,
    \$badge-f-size:      14px,
    \$badge-bg:          get-color('primary', 'base'),
    \$badge-radius:      50%,
    \$badge-color:       get-color('additional', 'ultra'),
    \$badge-icon-align:  baseline,
    \$badge-icon-offset: 0,
    \$badge-icon-line-height: inherit
) {
    display: inline-block;
    vertical-align: middle;
    width: \$badge-size;
    min-width: \$badge-size;
    height: \$badge-size;

    text-align: center;
    font-size: \$badge-f-size;
    line-height: \$badge-size;

    background-color: \$badge-bg;
    border-radius: \$badge-radius;
    color: \$badge-color;

    [class^='fa-'] {
        margin: \$badge-icon-offset;

        line-height: \$badge-icon-line-height;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/badge.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/badge.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/badge.scss");
    }
}
