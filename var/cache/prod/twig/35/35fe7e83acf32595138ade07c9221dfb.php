<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Dashboard/forecastOfOpportunitiesSubwidget.html.twig */
class __TwigTemplate_d66c306936ab8a772af4e2d79bd238b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->loadTemplate("@OroSales/Dashboard/forecastOfOpportunitiesSimpleSubwidget.html.twig", "@OroSales/Dashboard/forecastOfOpportunitiesSubwidget.html.twig", 1)->display($context);
        // line 2
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, true, false, 2), "deviation", [], "any", true, true, false, 2)) {
            // line 3
            echo "<div class=\"deviation\">
    <span class=\"deviation ";
            // line 4
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, true, false, 4), "isPositive", [], "any", true, true, false, 4)) {
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 4), "isPositive", [], "any", false, false, false, 4)) {
                    echo "positive";
                } else {
                    echo "negative";
                }
            }
            echo "\">
        ";
            // line 5
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 5), "deviation", [], "any", false, false, false, 5));
            echo "
    </span>
</div>
<div class=\"deviation\">
    <span class=\"deviation\">
        ";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.dashboard.forecast_of_opportunities.compare_to"), "html", null, true);
            echo ":
    </span>
    <span class=\"date-range\">";
            // line 12
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 12), "previousRange", [], "any", false, false, false, 12), "html", null, true);
            echo "</span>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroSales/Dashboard/forecastOfOpportunitiesSubwidget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 12,  62 => 10,  54 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Dashboard/forecastOfOpportunitiesSubwidget.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Dashboard/forecastOfOpportunitiesSubwidget.html.twig");
    }
}
