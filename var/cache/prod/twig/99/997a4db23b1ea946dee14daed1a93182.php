<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/lib/simplecolorpicker/jquery.simplecolorpicker.css */
class __TwigTemplate_d7594533be55c91ee644dbf1ec406856 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/*
 * Very simple jQuery Color Picker
 * https://github.com/tkrotoff/jquery-simplecolorpicker
 *
 * Copyright (C) 2012-2013 Tanguy Krotoff <tkrotoff@gmail.com>
 *
 * Licensed under the MIT license
 */

/**
 * Inspired by Bootstrap Twitter.
 * See https://github.com/twbs/bootstrap/blob/master/less/navbar.less
 * See https://github.com/twbs/bootstrap/blob/master/less/dropdowns.less
 */

.simplecolorpicker.picker {
  position: absolute;
  top: 100%;
  left: 0;
  z-index: 1051; /* Above Bootstrap modal (@zindex-modal = 1050) */
  display: none;
  float: left;

  min-width: 160px;
  max-width: 283px; /* @popover-max-width = 276px + 7 */

  padding: 5px 0 0 5px;
  margin: 2px 0 0;
  list-style: none;
  background-color: #fff; /* @dropdown-bg */

  border: 1px solid #ccc; /* @dropdown-fallback-border */
  border: 1px solid rgba(0, 0, 0, .15); /* @dropdown-border */

  -webkit-border-radius: 4px; /* @border-radius-base */
     -moz-border-radius: 4px;
          border-radius: 4px;

  -webkit-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
     -moz-box-shadow: 0 6px 12px rgba(0, 0, 0, .175);
          box-shadow: 0 6px 12px rgba(0, 0, 0, .175);

  -webkit-background-clip: padding-box;
     -moz-background-clip: padding;
          background-clip: padding-box;
}

.simplecolorpicker.inline {
  display: inline-block;
  padding: 6px 0;
}

.simplecolorpicker span {
  margin: 0 5px 5px 0;
}

.simplecolorpicker.icon,
.simplecolorpicker span.color {
  display: inline-block;

  cursor: pointer;
  border: 1px solid transparent;
}

.simplecolorpicker.icon:after,
.simplecolorpicker span.color:after {
  content: '\\00a0\\00a0\\00a0\\00a0'; /* Spaces */
}

.simplecolorpicker.icon[data-disabled]:hover,
.simplecolorpicker span.color[data-disabled]:hover {
  cursor: not-allowed;
  border: 1px solid transparent;
}

.simplecolorpicker span.color:hover,
.simplecolorpicker span.color[data-selected],
.simplecolorpicker span.color[data-selected]:hover {
  border: 1px solid #222; /* @gray-dark */
}
.simplecolorpicker span.color[data-selected]:after {
  color: #fff;
}

/* Vertical separator, replaces optgroup. */
.simplecolorpicker span.vr {
  border-left: 1px solid #222; /* @gray-dark */
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/lib/simplecolorpicker/jquery.simplecolorpicker.css";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/lib/simplecolorpicker/jquery.simplecolorpicker.css", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/lib/simplecolorpicker/jquery.simplecolorpicker.css");
    }
}
