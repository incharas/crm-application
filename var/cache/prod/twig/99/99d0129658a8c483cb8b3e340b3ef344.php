<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/scriptjs.js */
class __TwigTemplate_3fd1937d7ac90416e4f5045dbb919836 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "const config = require('module-config').default(module.id);
const scriptjs = require('scriptjs');
let scriptjsExtend = scriptjs;

/**
 * Overrides original scriptjs loader to resolve path to local modules before loading
 * (Local modules are considered paths, that don't start with 'https://', 'http://' or '//')
 */
if ('bundlesPath' in config) {
    const scriptpath = config.bundlesPath;

    scriptjsExtend = function \$script(paths, idOrDone, optDone) {
        paths = Array.isArray(paths) ? paths : [paths];
        paths = paths.map(function(path) {
            if (!/^(?:https?:)?\\/\\//.test(path)) {
                path = path.indexOf('.js') === -1 ? scriptpath + path + '.js' : scriptpath + path;
            }
            return path;
        });

        scriptjs(paths, idOrDone, optDone);
    };

    Object.assign(scriptjsExtend, scriptjs);

    scriptjsExtend.order = function(scripts, id, done) {
        (function callback(s) {
            s = scripts.shift();
            !scripts.length ? scriptjsExtend(s, id, done) : scriptjsExtend(s, callback);
        }());
    };

    scriptjsExtend.ready = function(deps, ready, req) {
        scriptjs.ready(deps, ready, req);
        return scriptjsExtend;
    };
}

module.exports = scriptjsExtend;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/scriptjs.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/scriptjs.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/scriptjs.js");
    }
}
