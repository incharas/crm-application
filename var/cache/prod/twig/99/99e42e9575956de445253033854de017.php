<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/bootstrap.js */
class __TwigTemplate_48d1344ce86aa59b9310084d1fb21b50 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    require('./bootstrap/bootstrap-collapse');
    require('./bootstrap/bootstrap-dropdown');
    require('./bootstrap/bootstrap-modal');
    require('./bootstrap/bootstrap-popover');
    require('./bootstrap/bootstrap-scrollspy');
    require('./bootstrap/bootstrap-tooltip');
    require('./bootstrap/bootstrap-typeahead');
    require('./bootstrap/bootstrap-tabs');
    require('bootstrap');
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/bootstrap.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/bootstrap.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/bootstrap.js");
    }
}
