<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataAudit/macros.html.twig */
class __TwigTemplate_9f387f60e5ab5368633630acba9955b7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_renderFieldValue($__fieldValue__ = null, $__field__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "fieldValue" => $__fieldValue__,
            "field" => $__field__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            $macros["AuditMacro"] = $this;
            // line 4
            $context["type"] = null;
            // line 5
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "type", [], "any", true, true, false, 5)) {
                // line 6
                $context["type"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "type", [], "any", false, false, false, 6);
            }
            // line 8
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "value", [], "any", true, true, false, 8)) {
                // line 9
                $context["fieldValue"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "value", [], "any", false, false, false, 9);
            }
            // line 12
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "timestamp", [], "any", true, true, false, 12) && (($context["type"] ?? null) == "date"))) {
                // line 13
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(($context["fieldValue"] ?? null)), "html", null, true);
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["fieldValue"] ?? null), "timestamp", [], "any", true, true, false, 14) && twig_in_filter(($context["type"] ?? null), [0 => "datetime", 1 => "datetimetz"]))) {
                // line 15
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(($context["fieldValue"] ?? null)), "html", null, true);
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 16
($context["fieldValue"] ?? null), "timestamp", [], "any", true, true, false, 16) && (($context["type"] ?? null) == "time"))) {
                // line 17
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatTime(($context["fieldValue"] ?? null)), "html", null, true);
            } elseif (((            // line 18
($context["type"] ?? null) == "array") || (($context["type"] ?? null) == "jsonarray"))) {
                // line 19
                (( !(null === ($context["fieldValue"] ?? null))) ? (print (twig_escape_filter($this->env, json_encode(($context["fieldValue"] ?? null), twig_constant("JSON_FORCE_OBJECT")), "html", null, true))) : (print ("")));
            } elseif ((((            // line 20
($context["type"] ?? null) == "boolean") || (($context["fieldValue"] ?? null) === true)) || (($context["fieldValue"] ?? null) === false))) {
                // line 21
                echo twig_escape_filter($this->env, ((($context["fieldValue"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No"))), "html", null, true);
            } else {
                // line 23
                if (((twig_test_iterable(($context["fieldValue"] ?? null)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_class", [], "any", true, true, false, 23)) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_class", [], "any", false, false, false, 23)))) {
                    // line 24
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dataaudit.record", ["%record%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_class", [], "any", false, false, false, 24), "label")), "%entity_name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_name", [], "any", true, true, false, 24)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_name", [], "any", false, false, false, 24), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_id", [], "any", true, true, false, 24)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_id", [], "any", false, false, false, 24), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))))) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_id", [], "any", true, true, false, 24)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_id", [], "any", false, false, false, 24), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty")))))]), "html", null, true);
                    // line 25
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "change_set", [], "any", true, true, false, 25) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "change_set", [], "any", false, false, false, 25)))) {
                        // line 26
                        echo "<dd>
                    <ul>";
                        // line 28
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "change_set", [], "any", false, false, false, 28));
                        foreach ($context['_seq'] as $context["diffFieldName"] => $context["diffFieldValues"]) {
                            // line 29
                            echo "<li>";
                            // line 30
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(_twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "entity_class", [], "any", false, false, false, 30), $context["diffFieldName"], "label"), $context["diffFieldName"])), "html", null, true);
                            echo ":
                                <s>";
                            // line 31
                            echo twig_call_macro($macros["AuditMacro"], "macro_renderFieldValue", [twig_first($this->env, $context["diffFieldValues"])], 31, $context, $this->getSourceContext());
                            echo "</s>&nbsp;";
                            echo twig_call_macro($macros["AuditMacro"], "macro_renderFieldValue", [twig_last($this->env, $context["diffFieldValues"])], 31, $context, $this->getSourceContext());
                            // line 32
                            echo "</li>";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['diffFieldName'], $context['diffFieldValues'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 34
                        echo "</ul>
                </dd>";
                    }
                } elseif (twig_test_iterable(                // line 37
($context["fieldValue"] ?? null))) {
                    // line 38
                    $context["result"] = [];
                    // line 39
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(($context["fieldValue"] ?? null));
                    foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                        // line 40
                        $context["result"] = twig_array_merge(($context["result"] ?? null), [0 => twig_call_macro($macros["AuditMacro"], "macro_renderFieldValue", [$context["item"]], 40, $context, $this->getSourceContext())]);
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 42
                    echo twig_join_filter(($context["result"] ?? null), ", ");
                } else {
                    // line 44
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "translationDomain", [], "any", true, true, false, 44)) {
                        // line 45
                        echo twig_escape_filter($this->env, _twig_default_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["fieldValue"] ?? null), [], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "translationDomain", [], "any", false, false, false, 45)), ""), "html", null, true);
                    } else {
                        // line 47
                        echo twig_escape_filter($this->env, ((array_key_exists("fieldValue", $context)) ? (_twig_default_filter(($context["fieldValue"] ?? null), "")) : ("")), "html", null, true);
                    }
                }
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 53
    public function macro_renderFieldName($__objectClass__ = null, $__fieldKey__ = null, $__fieldValue__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "objectClass" => $__objectClass__,
            "fieldKey" => $__fieldKey__,
            "fieldValue" => $__fieldValue__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 54
            $context["fieldLabel"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(twig_first($this->env, twig_split_filter($this->env, ($context["fieldKey"] ?? null), "::")), twig_last($this->env, twig_split_filter($this->env, ($context["fieldKey"] ?? null), "::")), "label"), ($context["fieldKey"] ?? null));
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(_twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(($context["objectClass"] ?? null), ($context["fieldKey"] ?? null), "label"), ($context["fieldLabel"] ?? null)), [], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "translationDomain", [], "any", true, true, false, 55)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "translationDomain", [], "any", false, false, false, 55), "messages")) : ("messages"))), "html", null, true);
            echo ":";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 58
    public function macro_renderCollection($__objectClass__ = null, $__fieldKey__ = null, $__fieldValue__ = null, $__allowedDiffTypes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "objectClass" => $__objectClass__,
            "fieldKey" => $__fieldKey__,
            "fieldValue" => $__fieldValue__,
            "allowedDiffTypes" => $__allowedDiffTypes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 59
            $macros["AuditMacro"] = $this;
            // line 61
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "collectionDiffs", [], "any", false, false, false, 61));
            foreach ($context['_seq'] as $context["diffType"] => $context["diffValues"]) {
                // line 62
                if (( !twig_test_empty($context["diffValues"]) && twig_in_filter($context["diffType"], ($context["allowedDiffTypes"] ?? null)))) {
                    // line 63
                    echo "<dl class=\"audit-list\">";
                    // line 64
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable($context["diffValues"]);
                    foreach ($context['_seq'] as $context["_key"] => $context["diffValue"]) {
                        // line 65
                        $context["record"] = $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["diffValue"], "entity_class", [], "any", false, false, false, 65), "label");
                        // line 66
                        if ((($context["record"] ?? null) == $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["record"] ?? null)))) {
                            // line 67
                            $context["record"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(twig_first($this->env, twig_split_filter($this->env, ($context["fieldKey"] ?? null), "::")), twig_last($this->env, twig_split_filter($this->env, ($context["fieldKey"] ?? null), "::")), "label"), ($context["fieldKey"] ?? null));
                            // line 68
                            $context["record"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(_twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(($context["objectClass"] ?? null), ($context["fieldKey"] ?? null), "label"), ($context["record"] ?? null)), [], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "translationDomain", [], "any", true, true, false, 68)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "translationDomain", [], "any", false, false, false, 68), "messages")) : ("messages")));
                        }
                        // line 70
                        echo "<dt>";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("oro.dataaudit.collection." . $context["diffType"]), ["%record%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["record"] ?? null)), "%entity_name%" => _twig_default_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["diffValue"], "entity_name", [], "any", false, false, false, 70)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))]), "html", null, true);
                        // line 71
                        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["diffValue"], "change_set", [], "any", false, false, false, 71))) {
                            // line 72
                            echo ":
                        <dd>
                            <ul>";
                            // line 75
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(twig_sort_filter($this->env, twig_get_array_keys_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["diffValue"], "change_set", [], "any", false, false, false, 75))));
                            foreach ($context['_seq'] as $context["_key"] => $context["diffFieldName"]) {
                                // line 76
                                $context["diffFieldValues"] = (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["diffValue"], "change_set", [], "any", false, false, false, 76)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[$context["diffFieldName"]] ?? null) : null);
                                // line 77
                                echo "<li>";
                                // line 78
                                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(_twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["diffValue"], "entity_class", [], "any", false, false, false, 78), $context["diffFieldName"], "label"), $context["diffFieldName"])), "html", null, true);
                                echo ":
                                        ";
                                // line 79
                                if (!twig_in_filter("added", ($context["allowedDiffTypes"] ?? null))) {
                                    // line 80
                                    echo "                                            <s>";
                                    echo twig_call_macro($macros["AuditMacro"], "macro_renderFieldValue", [twig_first($this->env, ($context["diffFieldValues"] ?? null))], 80, $context, $this->getSourceContext());
                                    echo "</s>";
                                    // line 81
                                    if (( !twig_test_empty(twig_first($this->env, ($context["diffFieldValues"] ?? null))) &&  !twig_test_empty(twig_last($this->env, ($context["diffFieldValues"] ?? null))))) {
                                        echo "&nbsp;";
                                    }
                                    // line 82
                                    echo "                                        ";
                                } elseif (!twig_in_filter("removed", ($context["allowedDiffTypes"] ?? null))) {
                                    // line 83
                                    echo twig_call_macro($macros["AuditMacro"], "macro_renderFieldValue", [twig_last($this->env, ($context["diffFieldValues"] ?? null))], 83, $context, $this->getSourceContext());
                                } else {
                                    // line 85
                                    echo "                                            <s>";
                                    echo twig_call_macro($macros["AuditMacro"], "macro_renderFieldValue", [twig_first($this->env, ($context["diffFieldValues"] ?? null))], 85, $context, $this->getSourceContext());
                                    echo "</s>";
                                    // line 86
                                    if (( !twig_test_empty(twig_first($this->env, ($context["diffFieldValues"] ?? null))) &&  !twig_test_empty(twig_last($this->env, ($context["diffFieldValues"] ?? null))))) {
                                        echo "&nbsp;";
                                    }
                                    // line 87
                                    echo twig_call_macro($macros["AuditMacro"], "macro_renderFieldValue", [twig_last($this->env, ($context["diffFieldValues"] ?? null))], 87, $context, $this->getSourceContext());
                                }
                                // line 89
                                echo "                                    </li>";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['diffFieldName'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 91
                            echo "</ul>
                        </dd>";
                        }
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['diffValue'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 95
                    echo "</dl>";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['diffType'], $context['diffValues'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroDataAudit/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 95,  275 => 91,  269 => 89,  266 => 87,  262 => 86,  258 => 85,  255 => 83,  252 => 82,  248 => 81,  244 => 80,  242 => 79,  238 => 78,  236 => 77,  234 => 76,  230 => 75,  226 => 72,  224 => 71,  221 => 70,  218 => 68,  216 => 67,  214 => 66,  212 => 65,  208 => 64,  206 => 63,  204 => 62,  200 => 61,  198 => 59,  182 => 58,  172 => 55,  170 => 54,  155 => 53,  143 => 47,  140 => 45,  138 => 44,  135 => 42,  129 => 40,  125 => 39,  123 => 38,  121 => 37,  117 => 34,  111 => 32,  107 => 31,  103 => 30,  101 => 29,  97 => 28,  94 => 26,  92 => 25,  90 => 24,  88 => 23,  85 => 21,  83 => 20,  81 => 19,  79 => 18,  77 => 17,  75 => 16,  73 => 15,  71 => 14,  69 => 13,  67 => 12,  64 => 9,  62 => 8,  59 => 6,  57 => 5,  55 => 4,  53 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataAudit/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataAuditBundle/Resources/views/macros.html.twig");
    }
}
