<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCase/Case/update.html.twig */
class __TwigTemplate_b222b56642a8d75507db861d9335f1fa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.subject%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 2
($context["entity"] ?? null), "subject", [], "any", false, false, false, 2), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.entity_label")]]);
        // line 4
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 4);
        // line 5
        $context["formAction"] = ((($context["entityId"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_update", ["id" => ($context["entityId"] ?? null)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroCase/Case/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCase/Case/update.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_case_view", "params" => ["id" => "\$id"]]], 10, $context, $this->getSourceContext());
        // line 14
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_case_create")) {
            // line 15
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_case_create"]], 15, $context, $this->getSourceContext()));
            // line 18
            echo "    ";
        }
        // line 19
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_case_update")) {
            // line 20
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_case_update", "params" => ["id" => "\$id"]]], 20, $context, $this->getSourceContext()));
            // line 24
            echo "    ";
        }
        // line 25
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 25, $context, $this->getSourceContext());
        echo "
    ";
        // line 26
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_index")], 26, $context, $this->getSourceContext());
        echo "
";
    }

    // line 29
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 31
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 32
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 35
($context["entity"] ?? null), "subject", [], "any", false, false, false, 35)];
            // line 37
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 39
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.entity_label")]);
            // line 40
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroCase/Case/update.html.twig", 40)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 41
            echo "    ";
        }
    }

    // line 44
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        $context["id"] = "case-form";
        // line 46
        echo "
    ";
        // line 47
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.block.general"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 53
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subject", [], "any", false, false, false, 53), 'row'), 1 =>         // line 54
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 54), 'row'), 2 =>         // line 55
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "resolution", [], "any", false, false, false, 55), 'row'), 3 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 56
($context["form"] ?? null), "owner", [], "any", true, true, false, 56)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 56), 'row')) : ("")), 4 =>         // line 57
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "assignedTo", [], "any", false, false, false, 57), 'row'), 5 =>         // line 58
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "source", [], "any", false, false, false, 58), 'row'), 6 =>         // line 59
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "status", [], "any", false, false, false, 59), 'row'), 7 =>         // line 60
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "priority", [], "any", false, false, false, 60), 'row'), 8 =>         // line 61
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "relatedContact", [], "any", false, false, false, 61), 'row'), 9 =>         // line 62
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "relatedAccount", [], "any", false, false, false, 62), 'row')]]]]];
        // line 67
        echo "
    ";
        // line 68
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.block.additional")));
        // line 69
        echo "
    ";
        // line 70
        $context["data"] = ["formErrors" => ((        // line 71
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 72
($context["dataBlocks"] ?? null)];
        // line 74
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCase/Case/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  160 => 74,  158 => 72,  157 => 71,  156 => 70,  153 => 69,  151 => 68,  148 => 67,  146 => 62,  145 => 61,  144 => 60,  143 => 59,  142 => 58,  141 => 57,  140 => 56,  139 => 55,  138 => 54,  137 => 53,  136 => 47,  133 => 46,  130 => 45,  126 => 44,  121 => 41,  118 => 40,  115 => 39,  109 => 37,  107 => 35,  106 => 32,  104 => 31,  101 => 30,  97 => 29,  91 => 26,  86 => 25,  83 => 24,  80 => 20,  77 => 19,  74 => 18,  71 => 15,  68 => 14,  66 => 10,  63 => 9,  60 => 8,  56 => 7,  51 => 1,  49 => 5,  47 => 4,  45 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCase/Case/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/CaseBundle/Resources/views/Case/update.html.twig");
    }
}
