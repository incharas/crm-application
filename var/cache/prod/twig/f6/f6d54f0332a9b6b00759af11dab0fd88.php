<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntitySerializedFields/Datagrid/Property/serialized.html.twig */
class __TwigTemplate_0e03a68287e83a5dfb76b5e0739054d5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (((array_key_exists("field_name", $context) && array_key_exists("field_type", $context)) && array_key_exists("value", $context))) {
            // line 2
            echo "    ";
            $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroEntitySerializedFields/Datagrid/Property/serialized.html.twig", 2)->unwrap();
            // line 3
            echo "
    ";
            // line 4
            echo twig_call_macro($macros["ui"], "macro_formatDynamicFieldValue", [null, null, ($context["field_name"] ?? null), ["value" =>             // line 5
($context["value"] ?? null), "type" =>             // line 6
($context["field_type"] ?? null)]], 4, $context, $this->getSourceContext());
            // line 7
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntitySerializedFields/Datagrid/Property/serialized.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  49 => 7,  47 => 6,  46 => 5,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntitySerializedFields/Datagrid/Property/serialized.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform-serialised-fields/Resources/views/Datagrid/Property/serialized.html.twig");
    }
}
