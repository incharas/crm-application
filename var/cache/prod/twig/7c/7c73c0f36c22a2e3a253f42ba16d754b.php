<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSegment/macros.html.twig */
class __TwigTemplate_7352951a9c31078ae2ecb37bc5ef1bab extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "
";
        // line 60
        echo "
";
        // line 84
        echo "
";
    }

    // line 1
    public function macro_query_designer_condition_builder($__params__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["segmentMacros"] = $this;
            // line 3
            echo "    ";
            $context["params"] = twig_array_merge(["column_chain_template_selector" => "#column-chain-template", "field_choice_filter_preset" => "querydesigner"],             // line 6
($context["params"] ?? null));
            // line 7
            echo "    ";
            $context["segment_selection_template_id"] = "segment-template";
            // line 8
            echo "    ";
            $context["segmentConditionOptions"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(["filters" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["params"] ?? null), "metadata", [], "any", false, true, false, 9), "filters", [], "any", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "metadata", [], "any", false, true, false, 9), "filters", [], "any", false, false, false, 9), [])) : ([])), "segmentChoice" => ["select2" => ["placeholder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.condition_builder.choose_entity_segment"), "formatSelectionTemplateSelector" => ("#" .             // line 13
($context["segment_selection_template_id"] ?? null)), "ajax" => ["url" => "oro_api_get_segment_items", "quietMillis" => 100], "pageLimit" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["params"] ?? null), "page_limit", [], "any", true, true, false, 18)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "page_limit", [], "any", false, false, false, 18), 10)) : (10))], "currentSegment" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["params"] ?? null), "currentSegmentId", [], "any", true, true, false, 20)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "currentSegmentId", [], "any", false, false, false, 20), null)) : (null))]], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["params"] ?? null), "segmentConditionOptions", [], "any", true, true, false, 22)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "segmentConditionOptions", [], "any", false, false, false, 22), [])) : ([])));
            // line 23
            echo "    ";
            $context["conditionBuilderOptions"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(["fieldsRelatedCriteria" => [0 => "condition-item", 1 => "condition-segment"]], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 25
($context["params"] ?? null), "conditionBuilderOptions", [], "any", true, true, false, 25)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "conditionBuilderOptions", [], "any", false, false, false, 25), [])) : ([])));
            // line 26
            echo "    ";
            $context["conditionBuilderOptions"] = $this->extensions['Oro\Bundle\SegmentBundle\Twig\SegmentExtension']->updateSegmentConditionBuilderOptions(($context["conditionBuilderOptions"] ?? null));
            // line 27
            echo "
    ";
            // line 28
            echo twig_call_macro($macros["segmentMacros"], "macro_query_designer_segment_template", [($context["segment_selection_template_id"] ?? null)], 28, $context, $this->getSourceContext());
            echo "
    <div class=\"condition-builder\"
         data-page-component-module=\"oroquerydesigner/js/app/components/condition-builder-component\"
         data-page-component-options=\"";
            // line 31
            echo twig_escape_filter($this->env, json_encode(($context["conditionBuilderOptions"] ?? null)), "html", null, true);
            echo "\"
         data-page-component-name=\"";
            // line 32
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "componentName", [], "any", true, true, false, 32)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "componentName", [], "any", false, false, false, 32), "condition-builder")) : ("condition-builder")), "html", null, true);
            echo "\">
        <div class=\"row-fluid\">
            <div class=\"criteria-list-container filter-criteria\">
                <ul class=\"criteria-list\">
                    ";
            // line 36
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("segment_criteria_list", $context)) ? (_twig_default_filter(($context["segment_criteria_list"] ?? null), "segment_criteria_list")) : ("segment_criteria_list")), ["params" => ($context["params"] ?? null)]);
            // line 37
            echo "                    <li class=\"option\" data-criteria=\"condition-segment\"
                        data-module=\"orosegment/js/app/views/segment-condition-view\"
                        data-options=\"";
            // line 39
            echo twig_escape_filter($this->env, json_encode(($context["segmentConditionOptions"] ?? null)), "html", null, true);
            echo "\">
                        ";
            // line 40
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.condition_builder.criteria.segment_condition"), "html", null, true);
            echo "
                    </li>
                    <li class=\"option\" data-criteria=\"conditions-group\">
                        ";
            // line 43
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.criteria.conditions_group"), "html", null, true);
            echo "
                    </li>
                </ul>
            </div>
            <div class=\"condition-container\" data-ignore-form-state-change>
                <div class=\"drag-n-drop-hint\"><div>";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.condition_builder.drag_n_drop_hint"), "html", null, true);
            echo "</div></div>
            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 54
    public function macro_query_designer_segment_template($__id__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "id" => $__id__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 55
            echo "    <script type=\"text/html\" id=\"";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "\">
        ";
            // line 56
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Apply segment", [], "messages");
            // line 57
            echo "        <%= obj.text %>
    </script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 67
    public function macro_runButton($__entity__ = null, $__reloadRequired__ = false, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "reloadRequired" => $__reloadRequired__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 68
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSegment/macros.html.twig", 68)->unwrap();
            // line 69
            echo "    ";
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 69) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 70
($context["entity"] ?? null), "type", [], "any", false, false, false, 70), "name", [], "any", false, false, false, 70) == twig_constant("Oro\\Bundle\\SegmentBundle\\Entity\\SegmentType::TYPE_STATIC")))) {
                // line 72
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_post_segment_run", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 73
($context["entity"] ?? null), "id", [], "any", false, false, false, 73)]), "aCss" => "no-hash run-button btn", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.action.refresh"), "iCss" => "fa-refresh", "dataAttributes" => ["page-component-module" => "orosegment/js/app/components/refresh-button", "page-component-options" => json_encode(["reloadRequired" =>                 // line 79
($context["reloadRequired"] ?? null)])]]], 72, $context, $this->getSourceContext());
                // line 81
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 93
    public function macro_initJsWidgets($__type__ = null, $__form__ = null, $__entities__ = null, $__metadata__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "type" => $__type__,
            "form" => $__form__,
            "entities" => $__entities__,
            "metadata" => $__metadata__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 94
            echo "    ";
            $context["widgetOptions"] = ["valueSource" => (("[data-ftid=" .             // line 95
($context["type"] ?? null)) . "_form_definition]"), "entityChoice" => (("[data-ftid=" .             // line 96
($context["type"] ?? null)) . "_form_entity]"), "entityChangeConfirmMessage" => (twig_replace_filter(            // line 97
($context["type"] ?? null), ["_" => "."]) . ".change_entity_confirmation"), "column" => ["editor" => ["namePattern" => (("^" .             // line 99
($context["type"] ?? null)) . "_form\\[column\\]\\[([\\w\\W]*)\\]\$")], "form" => (("#" .             // line 100
($context["type"] ?? null)) . "-column-form"), "itemContainer" => (("#" .             // line 101
($context["type"] ?? null)) . "-column-list .item-container"), "itemTemplate" => (("#" .             // line 102
($context["type"] ?? null)) . "-column-row")], "select2FieldChoiceTemplate" => "#column-chain-template", "metadata" =>             // line 105
($context["metadata"] ?? null)];
            // line 107
            echo "
    ";
            // line 108
            if ((($context["type"] ?? null) == "oro_report")) {
                // line 109
                echo "        ";
                $context["widgetOptions"] = twig_array_merge(($context["widgetOptions"] ?? null), ["grouping" => ["editor" => ["mapping" => ["name" => "oro_report_form[grouping][columnNames]"]], "form" => "#oro_report-grouping-form", "itemContainer" => "#oro_report-grouping-list .grouping-item-container", "itemTemplate" => "#oro_report-grouping-item-row"]]);
                // line 119
                echo "    ";
            }
            // line 120
            echo "
    ";
            // line 121
            $context["widgetOptions"] = $this->extensions['Oro\Bundle\SegmentBundle\Twig\SegmentExtension']->updateSegmentWidgetOptions(($context["widgetOptions"] ?? null), ($context["type"] ?? null));
            // line 122
            echo "
    <div data-page-component-module=\"orosegment/js/app/components/segment-component\"
         data-page-component-options=\"";
            // line 124
            echo twig_escape_filter($this->env, json_encode(($context["widgetOptions"] ?? null)), "html", null, true);
            echo "\"></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroSegment/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  259 => 124,  255 => 122,  253 => 121,  250 => 120,  247 => 119,  244 => 109,  242 => 108,  239 => 107,  237 => 105,  236 => 102,  235 => 101,  234 => 100,  233 => 99,  232 => 97,  231 => 96,  230 => 95,  228 => 94,  212 => 93,  201 => 81,  199 => 79,  198 => 73,  196 => 72,  194 => 70,  192 => 69,  189 => 68,  175 => 67,  164 => 57,  162 => 56,  157 => 55,  144 => 54,  130 => 48,  122 => 43,  116 => 40,  112 => 39,  108 => 37,  106 => 36,  99 => 32,  95 => 31,  89 => 28,  86 => 27,  83 => 26,  81 => 25,  79 => 23,  77 => 22,  76 => 20,  75 => 18,  74 => 13,  73 => 9,  71 => 8,  68 => 7,  66 => 6,  64 => 3,  61 => 2,  48 => 1,  43 => 84,  40 => 60,  37 => 53,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSegment/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SegmentBundle/Resources/views/macros.html.twig");
    }
}
