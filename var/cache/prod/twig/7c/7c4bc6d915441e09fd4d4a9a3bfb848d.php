<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSegment/Segment/view.html.twig */
class __TwigTemplate_286112d74aeabc02300833db245407ff extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSegment/Segment/view.html.twig", 2)->unwrap();
        // line 3
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroSegment/Segment/view.html.twig", 3)->unwrap();
        // line 4
        $macros["segmentQD"] = $this->macros["segmentQD"] = $this->loadTemplate("@OroSegment/macros.html.twig", "@OroSegment/Segment/view.html.twig", 4)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%segment.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["entity"] ?? null), "name", [], "any", false, false, false, 5), "%segment.group%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["segmentGroup"] ?? null))]]);
        // line 6
        $context["pageTitle"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 6);
        // line 7
        $context["displaySQL"] = ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_report.display_sql_query") && $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop());
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroSegment/Segment/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSegment/Segment/view.html.twig", 10)->unwrap();
        // line 11
        echo "
    ";
        // line 12
        echo twig_call_macro($macros["segmentQD"], "macro_runButton", [($context["entity"] ?? null), true], 12, $context, $this->getSourceContext());
        echo "
    ";
        // line 13
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)))) {
            // line 14
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["entity"] ?? null), "id", [], "any", false, false, false, 15)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.entity_label")]], 14, $context, $this->getSourceContext());
            // line 17
            echo "
    ";
        }
        // line 19
        echo "    ";
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("CREATE", ($context["entity"] ?? null)))) {
            // line 20
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_clone", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["entity"] ?? null), "id", [], "any", false, false, false, 21)]), "iCss" => "fa-files-o", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.action.clone.button.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.action.clone.button.label")]], 20, $context, $this->getSourceContext());
            // line 25
            echo "
    ";
        }
        // line 27
        echo "    ";
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)))) {
            // line 28
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_segment", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 29
($context["entity"] ?? null), "id", [], "any", false, false, false, 29)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_index"), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 32
($context["entity"] ?? null), "id", [], "any", false, false, false, 32), "id" => "btn-remove-segment", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.entity_label")]], 28, $context, $this->getSourceContext());
            // line 35
            echo "
    ";
        }
    }

    // line 39
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 41
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 44
($context["entity"] ?? null), "name", [], "any", false, false, false, 44)];
        // line 46
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 49
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 50
        echo "    ";
        if ((array_key_exists("gridName", $context) && ($context["gridName"] ?? null))) {
            // line 51
            echo "        ";
            $context["renderParams"] = twig_array_merge(((array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])), ["enableViews" => false]);
            // line 52
            echo "        ";
            $context["gridParams"] = ["_grid_view" => ["_disabled" => true], "_tags" => ["_disabled" => true]];
            // line 56
            echo "        ";
            if (($context["displaySQL"] ?? null)) {
                // line 57
                echo "            ";
                $context["gridParams"] = twig_array_merge(($context["gridParams"] ?? null), ["display_sql_query" => true]);
                // line 58
                echo "        ";
            }
            // line 59
            echo "        ";
            $context["params"] = twig_array_merge(((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), ($context["gridParams"] ?? null));
            // line 60
            echo "        ";
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ($context["params"] ?? null), ($context["renderParams"] ?? null)], 60, $context, $this->getSourceContext());
            echo "
    ";
        } else {
            // line 62
            echo "        <div class=\"container-fluid\">
            <div class=\"grid-container\">
                <div class=\"no-data\">
                    ";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Invalid segment configuration"), "html", null, true);
            echo "
                </div>
            </div>
        </div>
    ";
        }
    }

    // line 72
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "    ";
        $this->displayParentBlock("content", $context, $blocks);
        echo "
    ";
        // line 74
        if (($context["displaySQL"] ?? null)) {
            // line 75
            echo "        <div class=\"sql-query-panel\" data-role=\"sql-query-panel\"></div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroSegment/Segment/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 75,  183 => 74,  178 => 73,  174 => 72,  164 => 65,  159 => 62,  153 => 60,  150 => 59,  147 => 58,  144 => 57,  141 => 56,  138 => 52,  135 => 51,  132 => 50,  128 => 49,  121 => 46,  119 => 44,  118 => 41,  116 => 40,  112 => 39,  106 => 35,  104 => 32,  103 => 29,  101 => 28,  98 => 27,  94 => 25,  92 => 21,  90 => 20,  87 => 19,  83 => 17,  81 => 15,  79 => 14,  77 => 13,  73 => 12,  70 => 11,  67 => 10,  63 => 9,  58 => 1,  56 => 7,  54 => 6,  52 => 5,  49 => 4,  47 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSegment/Segment/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SegmentBundle/Resources/views/Segment/view.html.twig");
    }
}
