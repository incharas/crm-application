<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/macros.html.twig */
class __TwigTemplate_8ecde7b37d4443c44b98965cf8544404 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "
";
        // line 58
        echo "
";
        // line 87
        echo "
";
        // line 113
        echo "
";
        // line 131
        echo "
";
        // line 146
        echo "
";
        // line 164
        echo "
";
        // line 178
        echo "
";
        // line 195
        echo "
";
        // line 222
        echo "
";
        // line 254
        echo "
";
        // line 272
        echo "
";
        // line 288
        echo "
";
        // line 343
        echo "
";
        // line 362
        echo "
";
        // line 399
        echo "
";
        // line 415
        echo "
";
        // line 449
        echo "
";
        // line 465
        echo "
";
        // line 480
        echo "
";
        // line 527
        echo "
";
        // line 560
        echo "
";
        // line 595
        echo "
";
        // line 642
        echo "
";
        // line 681
        echo "
";
        // line 762
        echo "
";
        // line 786
        echo "
";
        // line 828
        echo "
";
        // line 843
        echo "
";
        // line 861
        echo "
";
        // line 870
        echo "
";
        // line 896
        echo "
";
        // line 922
        echo "
";
        // line 956
        echo "
";
        // line 978
        echo "
";
        // line 996
        echo "
";
        // line 1013
        echo "
";
        // line 1029
        echo "
";
        // line 1047
        echo "
";
        // line 1088
        echo "
";
        // line 1115
        echo "
";
        // line 1124
        echo "
";
        // line 1154
        echo "
";
        // line 1200
        echo "
";
        // line 1288
        echo "
";
        // line 1309
        echo "
";
        // line 1433
        echo "
";
        // line 1533
        echo "
";
        // line 1541
        echo "
";
        // line 1603
        echo "
";
        // line 1623
        echo "
";
        // line 1633
        echo "
";
        // line 1666
        echo "
";
        // line 1672
        echo "
";
        // line 1735
        echo "
";
        // line 1758
        echo "
";
    }

    // line 1
    public function macro_collection_prototype($__widget__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            if (twig_in_filter("prototype", twig_get_array_keys_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 2)))) {
                // line 3
                echo "        ";
                $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 3), "prototype", [], "any", false, false, false, 3);
                // line 4
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 4), "prototype", [], "any", false, false, false, 4), "vars", [], "any", false, false, false, 4), "name", [], "any", false, false, false, 4);
                // line 5
                echo "    ";
            } else {
                // line 6
                echo "        ";
                $context["form"] = ($context["widget"] ?? null);
                // line 7
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 7), "full_name", [], "any", false, false, false, 7);
                // line 8
                echo "    ";
            }
            // line 9
            echo "
    <div data-content=\"";
            // line 10
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\">
        <div class=\"row-oro oro-multiselect-holder\">
            <div class=\"float-holder \">
                ";
            // line 13
            if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 13))) {
                // line 14
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 15
                    echo "                        ";
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
                    echo "
                        ";
                    // line 16
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'errors');
                    echo "
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 18
                echo "                ";
            } else {
                // line 19
                echo "                    ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
                echo "
                ";
            }
            // line 21
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                ";
            // line 22
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
            </div>
            ";
            // line 24
            if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, true, false, 24), "vars", [], "any", false, true, false, 24), "allow_delete", [], "any", true, true, false, 24) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, false, false, 24), "vars", [], "any", false, false, false, 24), "allow_delete", [], "any", false, false, false, 24))) {
                // line 25
                echo "<button class=\"removeRow btn btn-icon btn-square-light\" type=\"button\" data-related=\"";
                echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
                echo "\"><span class=\"fa-close\"></span></button>";
            }
            // line 27
            echo "        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 31
    public function macro_tooltip($__tooltip_raw__ = null, $__tooltip_parameters__ = null, $__tooltip_placement__ = null, $__details_enabled__ = null, $__details_link__ = null, $__details_anchor__ = null, $__tooltip_translatable__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "tooltip_raw" => $__tooltip_raw__,
            "tooltip_parameters" => $__tooltip_parameters__,
            "tooltip_placement" => $__tooltip_placement__,
            "details_enabled" => $__details_enabled__,
            "details_link" => $__details_link__,
            "details_anchor" => $__details_anchor__,
            "tooltip_translatable" => $__tooltip_translatable__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 32
            $context["tooltip"] = ($context["tooltip_raw"] ?? null);
            // line 33
            if ( !(($context["tooltip_translatable"] ?? null) === false)) {
                // line 34
                $context["tooltip"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["tooltip_raw"] ?? null), ((array_key_exists("tooltip_parameters", $context)) ? (_twig_default_filter(($context["tooltip_parameters"] ?? null), [])) : ([])));
            }
            // line 36
            if ( !twig_test_empty(($context["tooltip"] ?? null))) {
                // line 37
                $context["details_anchor"] = ((array_key_exists("details_anchor", $context)) ? (_twig_default_filter(($context["details_anchor"] ?? null), null)) : (null));
                // line 38
                echo "        ";
                $context["details_link"] = ((array_key_exists("details_link", $context)) ? (_twig_default_filter(($context["details_link"] ?? null), null)) : (null));
                // line 39
                echo "        ";
                $context["details_enabled"] = ((array_key_exists("details_enabled", $context)) ? (_twig_default_filter(($context["details_enabled"] ?? null), false)) : (false));
                // line 40
                echo "        ";
                $context["tooltip_placement"] = (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) ? ("top") : (((array_key_exists("tooltip_placement", $context)) ? (_twig_default_filter(($context["tooltip_placement"] ?? null), null)) : (null))));
                // line 41
                echo "        ";
                if (((($context["details_enabled"] ?? null) || ($context["details_anchor"] ?? null)) || ($context["details_link"] ?? null))) {
                    // line 42
                    echo "            ";
                    $context["helpLink"] = ((array_key_exists("details_link", $context)) ? (_twig_default_filter(($context["details_link"] ?? null), $this->extensions['Oro\Bundle\HelpBundle\Twig\HelpExtension']->getHelpLinkUrl())) : ($this->extensions['Oro\Bundle\HelpBundle\Twig\HelpExtension']->getHelpLinkUrl()));
                    // line 43
                    echo "            ";
                    if (($context["details_anchor"] ?? null)) {
                        // line 44
                        echo "                ";
                        $context["helpLink"] = ((($context["helpLink"] ?? null) . "#") . ($context["details_anchor"] ?? null));
                        // line 45
                        echo "            ";
                    }
                    // line 46
                    echo "            ";
                    $context["tooltip"] = (((((($context["tooltip"] ?? null) . "<div class=\"clearfix\"><div class=\"pull-right\"><a href=\"") .                     // line 47
($context["helpLink"] ?? null)) . "\">") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.tooltip.read_more")) . "</a></div></div>");
                    // line 50
                    echo "        ";
                }
                // line 51
                echo "        ";
                $context["tooltip"] = (("<div class=\"oro-popover-content\">" . ($context["tooltip"] ?? null)) . "</div>");
                // line 52
                echo "<i class=\"fa-info-circle tooltip-icon\"
           ";
                // line 53
                if (($context["tooltip_placement"] ?? null)) {
                    echo "data-placement=\"";
                    echo twig_escape_filter($this->env, ($context["tooltip_placement"] ?? null), "html", null, true);
                    echo "\"";
                }
                // line 54
                echo "           data-content=\"";
                echo twig_escape_filter($this->env, ($context["tooltip"] ?? null), "html", null, true);
                echo "\"
           data-toggle=\"popover\"></i>";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 66
    public function macro_attibuteRow($__title__ = null, $__value__ = null, $__additionalData__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "value" => $__value__,
            "additionalData" => $__additionalData__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 67
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 68
            echo "
    ";
            // line 69
            ob_start(function () { return ''; });
            // line 70
            echo "        <div class=\"clearfix-oro\">
            ";
            // line 71
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "value", [], "any", true, true, false, 71)) {
                // line 72
                echo "                <div class=\"control-label\">";
                echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
                echo "</div>
            ";
            } else {
                // line 74
                echo "                <div class=\"control-label\">";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "value", [], "any", false, false, false, 74), "html", null, true);
                echo " <strong>";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "hint", [], "any", false, false, false, 74), "html", null, true);
                echo "</strong></div>
            ";
            }
            // line 76
            echo "        </div>
        ";
            // line 77
            if (twig_length_filter($this->env, ($context["additionalData"] ?? null))) {
                // line 78
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["additionalData"] ?? null), "data", [], "any", false, false, false, 78));
                foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
                    // line 79
                    echo "                <div class=\"clearfix-oro\">
                    <div class=\"control-label\">";
                    // line 80
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["additionalData"] ?? null), "field", [], "any", false, false, false, 80), [], "any", false, false, false, 80), "html", null, true);
                    echo "</div>
                </div>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 83
                echo "        ";
            }
            // line 84
            echo "    ";
            $context["attributeValue"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 85
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_renderAttribute", [($context["title"] ?? null), ($context["attributeValue"] ?? null)], 85, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 95
    public function macro_renderAttribute($__title__ = null, $__data__ = null, $__options__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 96
            echo "    ";
            $context["options"] = twig_array_merge(["rootClass" => null, "termClass" => null, "descriptionClass" => null, "tooltipHTML" => null, "dir" => null], ((            // line 102
array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([])));
            // line 103
            echo "
    <div class=\"attribute-item";
            // line 104
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "rootClass", [], "any", false, false, false, 104)) {
                echo " ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "rootClass", [], "any", false, false, false, 104), "html", null, true);
            }
            echo "\">
        <label class=\"attribute-item__term";
            // line 105
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "termClass", [], "any", false, false, false, 105)) {
                echo " ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "termClass", [], "any", false, false, false, 105), "html", null, true);
            }
            echo "\">";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "tooltipHTML", [], "any", false, false, false, 105)) {
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "tooltipHTML", [], "any", false, false, false, 105);
                echo " ";
            }
            echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
            echo "</label>
        <div class=\"attribute-item__description";
            // line 106
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "descriptionClass", [], "any", false, false, false, 106)) {
                echo " ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "descriptionClass", [], "any", false, false, false, 106), "html", null, true);
            }
            echo "\"
            ";
            // line 107
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "dir", [], "any", false, false, false, 107)) {
                echo "dir=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "dir", [], "any", false, false, false, 107), "html", null, true);
                echo "\" ";
            }
            // line 108
            echo "        >
            ";
            // line 109
            echo ($context["data"] ?? null);
            echo "
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 120
    public function macro_renderControlGroup($__title__ = null, $__data__ = null, $__options__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 121
            echo "    ";
            $context["options"] = twig_array_merge(["rootClass" => "attribute-row"], ((            // line 123
array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([])));
            // line 124
            echo "    <div class=\"control-group";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "rootClass", [], "any", false, false, false, 124)) {
                echo " ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "rootClass", [], "any", false, false, false, 124), "html", null, true);
            }
            echo "\">
        <label class=\"control-label\">";
            // line 125
            echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
            echo "</label>
        <div class=\"controls\">
            ";
            // line 127
            echo ($context["data"] ?? null);
            echo "
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 141
    public function macro_renderProperty($__title__ = null, $__data__ = null, $__entity__ = null, $__fieldName__ = null, $__options__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 142
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 143
            echo "
    ";
            // line 144
            echo twig_call_macro($macros["UIMacro"], "macro_renderHtmlProperty", [($context["title"] ?? null), twig_escape_filter($this->env, ($context["data"] ?? null)), ($context["entity"] ?? null), ($context["fieldName"] ?? null), ($context["options"] ?? null)], 144, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 156
    public function macro_renderHtmlProperty($__title__ = null, $__data__ = null, $__entity__ = null, $__fieldName__ = null, $__options__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 157
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 158
            echo "
    ";
            // line 159
            if (((array_key_exists("entity", $context) && array_key_exists("fieldName", $context)) &&  !$this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["fieldName"] ?? null)))) {
                // line 160
                echo "    ";
            } else {
                // line 161
                echo "        ";
                echo twig_call_macro($macros["UIMacro"], "macro_renderAttribute", [($context["title"] ?? null), (("<div class=\"control-label\">" . ((array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty")))) . "</div>"), ($context["options"] ?? null)], 161, $context, $this->getSourceContext());
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 173
    public function macro_renderPropertyControlGroup($__title__ = null, $__data__ = null, $__entity__ = null, $__fieldName__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 174
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 175
            echo "
    ";
            // line 176
            echo twig_call_macro($macros["UIMacro"], "macro_renderHtmlPropertyControlGroup", [($context["title"] ?? null), twig_escape_filter($this->env, ($context["data"] ?? null)), ($context["entity"] ?? null), ($context["fieldName"] ?? null)], 176, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 187
    public function macro_renderHtmlPropertyControlGroup($__title__ = null, $__data__ = null, $__entity__ = null, $__fieldName__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 188
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 189
            echo "
    ";
            // line 190
            if (((array_key_exists("entity", $context) && array_key_exists("fieldName", $context)) &&  !$this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["fieldName"] ?? null)))) {
                // line 191
                echo "    ";
            } else {
                // line 192
                echo "        ";
                echo twig_call_macro($macros["UIMacro"], "macro_renderControlGroup", [($context["title"] ?? null), (("<div class=\"control-label\">" . ((array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty")))) . "</div>")], 192, $context, $this->getSourceContext());
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 204
    public function macro_renderCollapsibleHtmlProperty($__title__ = null, $__data__ = null, $__entity__ = null, $__fieldName__ = null, $__moreText__ = "Show more", $__lessText__ = "Show less", ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "moreText" => $__moreText__,
            "lessText" => $__lessText__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 205
            echo "    ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["fieldName"] ?? null))) {
                // line 206
                echo "        ";
                $context["collapseId"] = uniqid("collapse-");
                // line 207
                echo "        ";
                $context["collapseKey"] = (((((("collapseBlock[" . $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null))) . "][") . ($context["title"] ?? null)) . "][") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 207)) . "]");
                // line 208
                echo "        <div class=\"collapse-block\">
            ";
                // line 209
                $macros["UIMacro"] = $this;
                // line 210
                echo "
            ";
                // line 211
                echo twig_call_macro($macros["UIMacro"], "macro_renderAttribute", [($context["title"] ?? null), ((((((((((((((((((("<div class=\"control-label collapse-overflow collapse no-transition\" data-toggle=\"false\" data-check-overflow=\"true\" " . "id=\"") .                 // line 213
($context["collapseId"] ?? null)) . "\" data-state-id=\"") . ($context["collapseKey"] ?? null)) . "\" data-collapsed-text=\"") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["moreText"] ?? null))) . "\" data-expanded-text=\"") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["lessText"] ?? null))) . "\">") . ((                // line 214
array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty")))) . "</div>") . "<a href=\"#\" role=\"button\" class=\"control-label collapse-toggle\" data-toggle=\"collapse\" ") . "data-target=\"#") .                 // line 217
($context["collapseId"] ?? null)) . "\" aria-expanded=\"false\" aria-controls=\"") . ($context["collapseId"] ?? null)) . "\"><span data-text>") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["moreText"] ?? null))) . "</span></a>")], 211, $context, $this->getSourceContext());
                // line 218
                echo "
        </div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 230
    public function macro_renderCollapsibleHtml($__data__ = null, $__entity__ = null, $__fieldName__ = null, $__moreText__ = "Show more", $__lessText__ = "Show less", ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "data" => $__data__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "moreText" => $__moreText__,
            "lessText" => $__lessText__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 231
            echo "    ";
            if (( !twig_test_empty(($context["data"] ?? null)) && ((twig_test_empty(($context["entity"] ?? null)) || twig_test_empty(($context["fieldName"] ?? null))) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["fieldName"] ?? null))))) {
                // line 232
                echo "        <div class=\"collapse-block\">
            ";
                // line 233
                $context["collapseId"] = uniqid("collapse-");
                // line 234
                echo "            <div id=\"";
                echo twig_escape_filter($this->env, ($context["collapseId"] ?? null), "html", null, true);
                echo "\" class=\"control-label collapse-overflow collapse no-transition\"
                 data-collapsed-text=\"";
                // line 235
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["moreText"] ?? null)), "html", null, true);
                echo "\"
                 data-expanded-text=\"";
                // line 236
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["lessText"] ?? null)), "html", null, true);
                echo "\"
                 data-check-overflow=\"true\"
                 data-toggle=\"false\"
            ";
                // line 239
                if (( !twig_test_empty(($context["entity"] ?? null)) &&  !twig_test_empty(($context["fieldName"] ?? null)))) {
                    // line 240
                    echo "                 data-state-id=\"";
                    echo twig_escape_filter($this->env, (((((("collapseBlock[" . $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null))) . "][") . ($context["fieldName"] ?? null)) . "][") . ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", true, true, false, 240)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 240), 0)) : (0))) . "]"), "html", null, true);
                    echo "\"
            ";
                }
                // line 242
                echo "            >";
                echo ($context["data"] ?? null);
                echo "</div>
            <a href=\"#\"
               role=\"button\"
               class=\"control-label collapse-toggle\"
               data-toggle=\"collapse\"
               data-target=\"";
                // line 247
                echo twig_escape_filter($this->env, ("#" . ($context["collapseId"] ?? null)), "html", null, true);
                echo "\"
               aria-expanded=\"false\"
               aria-controls=\"";
                // line 249
                echo twig_escape_filter($this->env, ($context["collapseId"] ?? null), "html", null, true);
                echo "\"
            ><span data-text>";
                // line 250
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["moreText"] ?? null)), "html", null, true);
                echo "</span></a>
        </div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 261
    public function macro_renderSwitchableHtmlProperty($__title__ = null, $__data__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 262
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 263
            echo "
    ";
            // line 264
            if ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_form.wysiwyg_enabled")) {
                // line 265
                echo "        ";
                $context["data"] = $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(($context["data"] ?? null));
                // line 266
                echo "    ";
            } else {
                // line 267
                echo "        ";
                $context["data"] = twig_nl2br(twig_escape_filter($this->env, twig_striptags(($context["data"] ?? null)), "html", null, true));
                // line 268
                echo "    ";
            }
            // line 269
            echo "
    ";
            // line 270
            echo twig_call_macro($macros["UIMacro"], "macro_renderAttribute", [($context["title"] ?? null), (("<div class=\"control-label html-property\">" . ((array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty")))) . "</div>")], 270, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 280
    public function macro_renderColorProperty($__title__ = null, $__data__ = null, $__empty__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "empty" => $__empty__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 281
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 282
            echo "
    ";
            // line 283
            if ( !(null === ($context["data"] ?? null))) {
                // line 284
                echo "       ";
                $context["data"] = (((((("<i class=\"color hide-text\" title=\"" . ($context["data"] ?? null)) . "\" style=\"background-color: ") . ($context["data"] ?? null)) . ";\">") . ($context["data"] ?? null)) . "</i>");
                // line 285
                echo "    ";
            }
            // line 286
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_renderAttribute", [($context["title"] ?? null), (("<div class=\"control-label\">" . _twig_default_filter(((array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), ($context["empty"] ?? null))) : (($context["empty"] ?? null))), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))) . "</div>")], 286, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 303
    public function macro_link($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 304
            echo "    ";
            // line 305
            echo "    ";
            $context["iconHtml"] = "";
            // line 306
            echo "    ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 306) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 306))) {
                // line 307
                echo "        ";
                ob_start(function () { return ''; });
                // line 308
                echo "        <span class=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 308), "html", null, true);
                echo "\" aria-hidden=\"true\">
            ";
                // line 309
                if ( !((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "noIconText", [], "any", true, true, false, 309)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "noIconText", [], "any", false, false, false, 309), false)) : (false))) {
                    // line 310
                    echo "                <span class=\"sr-only\">";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 310), "html", null, true);
                    echo "</span>
            ";
                }
                // line 312
                echo "        </span>
        ";
                $context["iconHtml"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 314
                echo "    ";
            } else {
                // line 315
                echo "    ";
            }
            // line 316
            echo "    ";
            ob_start(function () { return ''; });
            // line 317
            echo "    <a href=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "path", [], "any", false, false, false, 317), "html", null, true);
            echo "\"
        ";
            // line 318
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "id", [], "any", true, true, false, 318)) {
                // line 319
                echo "            id=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "id", [], "any", false, false, false, 319), "html", null, true);
                echo "\"
        ";
            }
            // line 321
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", true, true, false, 321)) {
                // line 322
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", false, false, false, 322));
                foreach ($context['_seq'] as $context["dataItemName"] => $context["dataItemValue"]) {
                    // line 323
                    echo "                data-";
                    echo twig_escape_filter($this->env, $context["dataItemName"], "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, $context["dataItemValue"], "html_attr");
                    echo "\"
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['dataItemName'], $context['dataItemValue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 325
                echo "        ";
            }
            // line 326
            echo "        ";
            $context["classes"] = "";
            // line 327
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", true, true, false, 327)) {
                // line 328
                echo "            ";
                $context["classes"] = ((($context["classes"] ?? null) . " ") . twig_trim_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 328)));
                // line 329
                echo "        ";
            }
            // line 330
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 330)) {
                // line 331
                echo "            ";
                $context["classes"] = ((($context["classes"] ?? null) . " ") . twig_trim_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 331)));
                // line 332
                echo "        ";
            }
            // line 333
            echo "        ";
            if (twig_length_filter($this->env, ($context["classes"] ?? null))) {
                // line 334
                echo "            class=\"";
                echo twig_escape_filter($this->env, twig_join_filter(array_unique(twig_split_filter($this->env, ($context["classes"] ?? null), " ")), " "), "html", null, true);
                echo "\"
        ";
            }
            // line 336
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "role", [], "any", true, true, false, 336)) {
                echo "role=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "role", [], "any", false, false, false, 336), "html", null, true);
                echo "\"";
            }
            // line 337
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "target", [], "any", true, true, false, 337)) {
                echo "target=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "target", [], "any", false, false, false, 337), "html", null, true);
                echo "\"";
            }
            // line 338
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", true, true, false, 338) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 338)))) {
                echo "title=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 338), "html", null, true);
                echo "\"";
            }
            echo ">";
            echo twig_trim_filter(($context["iconHtml"] ?? null));
            // line 339
            echo twig_escape_filter($this->env, twig_trim_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 339)), "html", null, true);
            echo "
    </a>
    ";
            $___internal_parse_24_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 316
            echo twig_spaceless($___internal_parse_24_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 355
    public function macro_button($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 356
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 357
            echo "
    <div class=\"pull-left btn-group icons-holder\">
        ";
            // line 359
            echo twig_call_macro($macros["UIMacro"], "macro_link", [twig_array_merge(($context["parameters"] ?? null), ["class" => "btn back icons-holder-text", "role" => "button"])], 359, $context, $this->getSourceContext());
            echo "
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 373
    public function macro_dropdownButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 374
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 375
            echo "
    ";
            // line 376
            $context["togglerId"] = uniqid("dropdown-");
            // line 377
            echo "    <div class=\"btn-group\">
        <a href=\"#\" role=\"button\" id=\"";
            // line 378
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\"
           aria-haspopup=\"true\" aria-expanded=\"false\" data-placement=\"";
            // line 379
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "placement", [], "any", true, true, false, 379)) ? (print (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "placement", [], "any", false, false, false, 379), "html", null, true))) : (print ("bottom-end")));
            echo "\"
           data-inherit-parent-width=\"loosely\"
        >
            ";
            // line 382
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 382)) {
                // line 383
                echo "                <span class=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 383), "html", null, true);
                echo "\" aria-hidden=\"true\"></span>
            ";
            }
            // line 385
            echo "            ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 385), "html", null, true);
            echo "
        </a>
        <ul class=\"dropdown-menu ";
            // line 387
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 387)) ? (print (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 387), "html", null, true))) : (print ("")));
            echo "\" role=\"menu\" aria-labelledby=\"";
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\">
            ";
            // line 388
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "elements", [], "any", true, true, false, 388) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "elements", [], "any", false, false, false, 388)))) {
                // line 389
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "elements", [], "any", false, false, false, 389));
                foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                    // line 390
                    echo "                    ";
                    echo twig_call_macro($macros["UIMacro"], "macro_dropdownItem", [$context["item"]], 390, $context, $this->getSourceContext());
                    echo "
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 392
                echo "            ";
            }
            // line 393
            echo "            ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "html", [], "any", true, true, false, 393) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "html", [], "any", false, false, false, 393)))) {
                // line 394
                echo "                ";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "html", [], "any", false, false, false, 394);
                echo "
            ";
            }
            // line 396
            echo "        </ul>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 411
    public function macro_dropdownItem($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 412
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 413
            echo "    <li>";
            echo twig_call_macro($macros["UIMacro"], "macro_link", [($context["parameters"] ?? null)], 413, $context, $this->getSourceContext());
            echo "</li>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 425
    public function macro_pinnedDropdownButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 426
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 427
            echo "
    ";
            // line 428
            if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() || ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "mobileEnabled", [], "any", true, true, false, 428)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "mobileEnabled", [], "any", false, false, false, 428), false)) : (false)))) {
                // line 429
                echo "        ";
                $context["options"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "options", [], "any", true, true, false, 429)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "options", [], "any", false, false, false, 429), [])) : ([]));
                // line 430
                echo "        ";
                $context["options"] = twig_array_merge(($context["options"] ?? null), ["widgetModule" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 431
($context["options"] ?? null), "widgetModule", [], "any", true, true, false, 431)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "widgetModule", [], "any", false, false, false, 431), "oroui/js/content-processor/pinned-dropdown-button")) : ("oroui/js/content-processor/pinned-dropdown-button")), "widgetName" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 432
($context["options"] ?? null), "widgetName", [], "any", true, true, false, 432)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "widgetName", [], "any", false, false, false, 432), "pinnedDropdownButtonProcessor")) : ("pinnedDropdownButtonProcessor")), "groupKey" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 433
($context["parameters"] ?? null), "groupKey", [], "any", true, true, false, 433)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "groupKey", [], "any", false, false, false, 433)) : ("")), "useMainButtonsClone" => true]);
                // line 436
                echo "        ";
                ob_start(function () { return ''; });
                // line 437
                echo "            <div class=\"pull-right pinned-dropdown\"
                 ";
                // line 438
                echo twig_call_macro($macros["UIMacro"], "macro_renderAttributes", [twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", true, true, false, 438)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", false, false, false, 438), [])) : ([])), ["page-component-module" => "oroui/js/app/components/jquery-widget-component", "page-component-options" =>                 // line 440
($context["options"] ?? null)])], 438, $context, $this->getSourceContext());
                // line 441
                echo ">
                ";
                // line 442
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "html", [], "any", false, false, false, 442);
                echo "
            </div>
        ";
                $___internal_parse_25_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 436
                echo twig_spaceless($___internal_parse_25_);
                // line 445
                echo "    ";
            } else {
                // line 446
                echo "        ";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "html", [], "any", false, false, false, 446);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 453
    public function macro_dropdownSaveButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 454
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 455
            echo "    ";
            $context["parameters"] = twig_array_merge(["groupKey" => "saveButtons", "options" => ["moreButtonAttrs" => ["class" => "btn-success"]]], ((            // line 462
array_key_exists("parameters", $context)) ? (_twig_default_filter(($context["parameters"] ?? null), [])) : ([])));
            // line 463
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_pinnedDropdownButton", [($context["parameters"] ?? null)], 463, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 472
    public function macro_cancelButton($__path__ = null, $__label__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "path" => $__path__,
            "label" => $__label__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 473
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 474
            echo "
    ";
            // line 475
            if (twig_test_empty(($context["label"] ?? null))) {
                // line 476
                echo "        ";
                $context["label"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel");
                // line 477
                echo "    ";
            }
            // line 478
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_button", [["path" => ($context["path"] ?? null), "title" => ($context["label"] ?? null), "label" => ($context["label"] ?? null), "data" => ["action" => "cancel"]]], 478, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 492
    public function macro_editButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 493
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 494
            echo "
    ";
            // line 495
            $context["iCss"] = [0 => "fa-pencil-square-o"];
            // line 496
            echo "    ";
            $context["aCss"] = [0 => "edit-button", 1 => "main-group"];
            // line 497
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 497)) {
                // line 498
                echo "        ";
                $context["iCss"] = twig_array_merge(twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 498), " "), ($context["iCss"] ?? null));
                // line 499
                echo "    ";
            }
            // line 500
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 500)) {
                // line 501
                echo "        ";
                $context["aCss"] = twig_array_merge(twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 501), " "), ($context["aCss"] ?? null));
                // line 502
                echo "    ";
            }
            // line 503
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", true, true, false, 503)) {
                // line 504
                echo "        ";
                $context["title"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 504);
                // line 505
                echo "    ";
            } else {
                // line 506
                echo "        ";
                $context["title"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "entity_label", [], "any", true, true, false, 506)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.edit_entity", ["%entityName%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 507
($context["parameters"] ?? null), "entity_label", [], "any", false, false, false, 507)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.edit")));
                // line 510
                echo "    ";
            }
            // line 511
            echo "    ";
            $context["label"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", true, true, false, 511)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 512
($context["parameters"] ?? null), "label", [], "any", false, false, false, 512)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.edit")));
            // line 515
            echo "    ";
            $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["iCss" => twig_join_filter(            // line 516
($context["iCss"] ?? null), " "), "aCss" => twig_join_filter(            // line 517
($context["aCss"] ?? null), " "), "title" =>             // line 518
($context["title"] ?? null), "label" =>             // line 519
($context["label"] ?? null)]);
            // line 521
            echo "
    ";
            // line 523
            echo "    ";
            $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["path" => $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->addUrlQuery((($__internal_compile_0 = ($context["parameters"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["path"] ?? null) : null))]);
            // line 524
            echo "
    ";
            // line 525
            echo twig_call_macro($macros["UIMacro"], "macro_button", [($context["parameters"] ?? null)], 525, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 539
    public function macro_addButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 540
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", true, true, false, 540)) {
                // line 541
                echo "        ";
                $context["label"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 541);
                // line 542
                echo "    ";
            } else {
                // line 543
                echo "        ";
                $context["label"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "entity_label", [], "any", true, true, false, 543)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 544
($context["parameters"] ?? null), "entity_label", [], "any", false, false, false, 544)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create")));
                // line 547
                echo "    ";
            }
            // line 548
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", true, true, false, 548)) {
                // line 549
                echo "        ";
                $context["title"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 549);
                // line 550
                echo "    ";
            } else {
                // line 551
                echo "        ";
                $context["title"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "entity_label", [], "any", true, true, false, 551)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 552
($context["parameters"] ?? null), "entity_label", [], "any", false, false, false, 552)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create")));
                // line 555
                echo "    ";
            }
            // line 556
            echo "
    ";
            // line 557
            $macros["UIMacro"] = $this;
            // line 558
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_link", [twig_array_merge(["class" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", true, true, false, 558)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 558), "btn main-group btn-primary pull-right")) : ("btn main-group btn-primary pull-right")), "role" => "button", "label" => ($context["label"] ?? null), "title" => ($context["title"] ?? null)], ($context["parameters"] ?? null))], 558, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 576
    public function macro_deleteButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 577
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 578
            echo "
    ";
            // line 579
            $context["aCss"] = "btn icons-holder-text";
            // line 580
            echo "
    ";
            // line 581
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "disabled", [], "any", true, true, false, 581) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "disabled", [], "any", false, false, false, 581))) {
                // line 582
                echo "        ";
                $context["aCss"] = (($context["aCss"] ?? null) . " disabled");
                // line 583
                echo "    ";
            }
            // line 584
            echo "
    ";
            // line 585
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 585)) {
                // line 586
                echo "        ";
                $context["aCss"] = ((($context["aCss"] ?? null) . " ") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 586));
                // line 587
                echo "    ";
            }
            // line 588
            echo "
    ";
            // line 589
            $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["aCss" => ($context["aCss"] ?? null)]);
            // line 590
            echo "
    <div class=\"pull-left btn-group icons-holder\">
        ";
            // line 592
            echo twig_call_macro($macros["UIMacro"], "macro_deleteLink", [($context["parameters"] ?? null)], 592, $context, $this->getSourceContext());
            echo "
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 596
    public function macro_deleteLink($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 597
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 598
            echo "
    ";
            // line 599
            $context["entityLabel"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "entity_label", [], "any", true, true, false, 599)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "entity_label", [], "any", false, false, false, 599)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.item")));
            // line 600
            echo "    ";
            $context["label"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", true, true, false, 600)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 600)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.delete")));
            // line 601
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", true, true, false, 601)) {
                // line 602
                echo "        ";
                $context["title"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 602);
                // line 603
                echo "    ";
            } else {
                // line 604
                echo "        ";
                $context["title"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "entity_label", [], "any", true, true, false, 604)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.delete_entity", ["%entityName%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 605
($context["parameters"] ?? null), "entity_label", [], "any", false, false, false, 605)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.delete")));
                // line 608
                echo "    ";
            }
            // line 609
            echo "
    ";
            // line 610
            $context["message"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataMessage", [], "any", true, true, false, 610)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataMessage", [], "any", false, false, false, 610)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.delete_confirm", ["%entity_label%" => ($context["entityLabel"] ?? null)])));
            // line 611
            echo "    ";
            $context["successMessage"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "successMessage", [], "any", true, true, false, 611)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "successMessage", [], "any", false, false, false, 611)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.delete_message", ["%entity_label%" => ($context["entityLabel"] ?? null)])));
            // line 612
            echo "    ";
            $context["url"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrl", [], "any", true, true, false, 612)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrl", [], "any", false, false, false, 612)) : (""));
            // line 613
            echo "    ";
            $context["linkParams"] = ["data" => ["message" =>             // line 615
($context["message"] ?? null), "success-message" =>             // line 616
($context["successMessage"] ?? null), "url" =>             // line 617
($context["url"] ?? null)], "iCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 619
($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 619)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 619)) : ("fa-trash-o")), "aCss" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 620
($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 620), "title" =>             // line 621
($context["title"] ?? null), "label" =>             // line 622
($context["label"] ?? null), "path" => "#"];
            // line 625
            echo "
    ";
            // line 626
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataId", [], "any", true, true, false, 626)) {
                // line 627
                echo "        ";
                $context["data"] = twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["linkParams"] ?? null), "data", [], "any", false, false, false, 627), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataId", [], "any", false, false, false, 627)]);
                // line 628
                echo "        ";
                $context["linkParams"] = twig_array_merge(($context["linkParams"] ?? null), ["data" => ($context["data"] ?? null)]);
                // line 629
                echo "    ";
            }
            // line 630
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataRedirect", [], "any", true, true, false, 630)) {
                // line 631
                echo "        ";
                $context["data"] = twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["linkParams"] ?? null), "data", [], "any", false, false, false, 631), ["redirect" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataRedirect", [], "any", false, false, false, 631)]);
                // line 632
                echo "        ";
                $context["linkParams"] = twig_array_merge(($context["linkParams"] ?? null), ["data" => ($context["data"] ?? null)]);
                // line 633
                echo "    ";
            }
            // line 634
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", true, true, false, 634)) {
                // line 635
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", false, false, false, 635));
                foreach ($context['_seq'] as $context["dataItemName"] => $context["dataItemValue"]) {
                    // line 636
                    echo "            ";
                    $context["data"] = twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["linkParams"] ?? null), "data", [], "any", false, false, false, 636), [$context["dataItemName"] => $context["dataItemValue"]]);
                    // line 637
                    echo "            ";
                    $context["linkParams"] = twig_array_merge(($context["linkParams"] ?? null), ["data" => ($context["data"] ?? null)]);
                    // line 638
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['dataItemName'], $context['dataItemValue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 639
                echo "    ";
            }
            // line 640
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_link", [($context["linkParams"] ?? null)], 640, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 663
    public function macro_clientLink($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 664
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 665
            echo "    ";
            $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 666
($context["parameters"] ?? null), "class", [], "any", true, true, false, 666)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 666), "")) : ("")) . ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 666)) ? ((" " . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 666))) : ("")))]);
            // line 668
            echo "    ";
            ob_start(function () { return ''; });
            // line 669
            echo "        <a href=\"#\"";
            // line 670
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrlRaw", [], "any", true, true, false, 670)) {
                // line 671
                echo "data-url=\"";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrlRaw", [], "any", false, false, false, 671);
                echo "\"
            ";
            } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 672
($context["parameters"] ?? null), "dataUrl", [], "any", true, true, false, 672)) {
                // line 673
                echo "                data-url=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrl", [], "any", false, false, false, 673), "html", null, true);
                echo "\"";
            }
            // line 675
            echo twig_call_macro($macros["UIMacro"], "macro_clientControlAttrs", [($context["parameters"] ?? null)], 675, $context, $this->getSourceContext());
            echo ">";
            // line 676
            echo twig_call_macro($macros["UIMacro"], "macro_clientControlIcon", [($context["parameters"] ?? null)], 676, $context, $this->getSourceContext());
            // line 677
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", true, true, false, 677)) ? (print (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 677), "html", null, true))) : (print ("")));
            // line 678
            echo "</a>
    ";
            $___internal_parse_26_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 668
            echo twig_spaceless($___internal_parse_26_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 702
    public function macro_clientControlAttrs($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 703
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 704
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", true, true, false, 704)) {
                // line 705
                echo "        class=\"";
                echo twig_escape_filter($this->env, twig_join_filter(array_unique(twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 705), " ")), " "), "html", null, true);
                echo "\"
    ";
            }
            // line 707
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "id", [], "any", true, true, false, 707)) {
                // line 708
                echo "        id=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "id", [], "any", false, false, false, 708), "html", null, true);
                echo "\"
    ";
            }
            // line 710
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", true, true, false, 710)) {
                // line 711
                echo "        title=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 711), "html", null, true);
                echo "\"
    ";
            }
            // line 713
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataId", [], "any", true, true, false, 713)) {
                // line 714
                echo "        data-id=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataId", [], "any", false, false, false, 714), "html", null, true);
                echo "\"
    ";
            }
            // line 716
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataIntention", [], "any", true, true, false, 716)) {
                // line 717
                echo "        data-intention=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataIntention", [], "any", false, false, false, 717), "html", null, true);
                echo "\"
    ";
            }
            // line 719
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "successMessage", [], "any", true, true, false, 719)) {
                // line 720
                echo "        data-success-message=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "successMessage", [], "any", false, false, false, 720), "html", null, true);
                echo "\"
    ";
            }
            // line 722
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataRedirect", [], "any", true, true, false, 722)) {
                // line 723
                echo "        data-redirect=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataRedirect", [], "any", false, false, false, 723), "html", null, true);
                echo "\"
    ";
            }
            // line 725
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "role", [], "any", true, true, false, 725)) {
                // line 726
                echo "        role=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "role", [], "any", false, false, false, 726), "html", null, true);
                echo "\"
    ";
            }
            // line 728
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dir", [], "any", true, true, false, 728)) {
                // line 729
                echo "        dir=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dir", [], "any", false, false, false, 729), "html", null, true);
                echo "\"
    ";
            }
            // line 731
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaSelected", [], "any", true, true, false, 731)) {
                // line 732
                echo "        aria-selected=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaSelected", [], "any", false, false, false, 732), "html", null, true);
                echo "\"
    ";
            }
            // line 734
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaControls", [], "any", true, true, false, 734)) {
                // line 735
                echo "        aria-controls=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaControls", [], "any", false, false, false, 735), "html", null, true);
                echo "\"
    ";
            }
            // line 737
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaLabel", [], "any", true, true, false, 737)) {
                // line 738
                echo "        aria-label=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaLabel", [], "any", false, false, false, 738), "html", null, true);
                echo "\"
    ";
            }
            // line 740
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaHaspopup", [], "any", true, true, false, 740)) {
                // line 741
                echo "        aria-haspopup=\"";
                echo ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "ariaHaspopup", [], "any", false, false, false, 741)) ? ("true") : ("false"));
                echo "\"
    ";
            }
            // line 743
            echo "    ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "visible", [], "any", true, true, false, 743) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "visible", [], "any", false, false, false, 743))) {
                // line 744
                echo "        style=\"display: none\"
    ";
            }
            // line 746
            echo "    ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "widget", [], "any", true, true, false, 746) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "widget", [], "any", false, false, false, 746)))) {
                // line 747
                echo "        ";
                $context["options"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "widget", [], "any", false, false, false, 747);
                // line 748
                if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "createOnEvent", [], "any", true, true, false, 748)) {
                    // line 749
                    $context["options"] = twig_array_merge(($context["options"] ?? null), ["createOnEvent" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 750
($context["options"] ?? null), "event", [], "any", true, true, false, 750)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "event", [], "any", false, false, false, 750), "click")) : ("click"))]);
                }
                // line 753
                echo twig_call_macro($macros["UIMacro"], "macro_renderWidgetAttributes", [($context["options"] ?? null)], 753, $context, $this->getSourceContext());
                echo "
    ";
            }
            // line 755
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "pageComponent", [], "any", true, true, false, 755)) {
                // line 756
                echo twig_call_macro($macros["UIMacro"], "macro_renderPageComponentAttributes", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "pageComponent", [], "any", false, false, false, 756)], 756, $context, $this->getSourceContext());
            }
            // line 758
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", true, true, false, 758) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", false, false, false, 758)))) {
                // line 759
                echo twig_call_macro($macros["UIMacro"], "macro_renderAttributes", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", false, false, false, 759)], 759, $context, $this->getSourceContext());
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 773
    public function macro_clientControlIcon($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 774
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 775
            echo "    ";
            ob_start(function () { return ''; });
            // line 776
            $context["labelInIcon"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "labelInIcon", [], "any", true, true, false, 776)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "labelInIcon", [], "any", false, false, false, 776)) : (true));
            // line 777
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 777)) {
                // line 778
                echo "<span class=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 778), "html", null, true);
                echo "\" aria-hidden=\"true\">";
                // line 779
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", true, true, false, 779) && ($context["labelInIcon"] ?? null))) {
                    // line 780
                    echo "<span class=\"sr-only\">";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 780), "html", null, true);
                    echo "</span>";
                }
                // line 782
                echo "</span>";
            }
            $___internal_parse_27_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 775
            echo twig_spaceless($___internal_parse_27_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 805
    public function macro_clientBtn($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 806
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 807
            echo "    ";
            $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["class" => ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 808
($context["parameters"] ?? null), "class", [], "any", true, true, false, 808)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 808), "")) : ("")) . " ") . ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "btnCss", [], "any", true, true, false, 808)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "btnCss", [], "any", false, false, false, 808), "btn")) : ("btn")))]);
            // line 810
            echo "    ";
            $context["type"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "type", [], "any", true, true, false, 810)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "type", [], "any", false, false, false, 810)) : ("button"));
            // line 811
            echo "    ";
            ob_start(function () { return ''; });
            // line 812
            echo "        <button";
            echo twig_call_macro($macros["UIMacro"], "macro_clientControlAttrs", [($context["parameters"] ?? null)], 812, $context, $this->getSourceContext());
            // line 813
            echo "type=\"";
            echo twig_escape_filter($this->env, ($context["type"] ?? null), "html", null, true);
            echo "\"
        ";
            // line 814
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", true, true, false, 814)) {
                // line 815
                echo "            title=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 815), "html", null, true);
                echo "\"
        ";
            }
            // line 817
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrlRaw", [], "any", true, true, false, 817)) {
                // line 818
                echo "data-url=\"";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrlRaw", [], "any", false, false, false, 818);
                echo "\"
        ";
            } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 819
($context["parameters"] ?? null), "dataUrl", [], "any", true, true, false, 819)) {
                // line 820
                echo "            data-url=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataUrl", [], "any", false, false, false, 820), "html", null, true);
                echo "\"";
            }
            // line 822
            echo ">";
            // line 823
            echo twig_call_macro($macros["UIMacro"], "macro_clientControlIcon", [($context["parameters"] ?? null)], 823, $context, $this->getSourceContext());
            // line 824
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", true, true, false, 824)) ? (print (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 824), "html", null, true))) : (print ("")));
            // line 825
            echo "</button>
    ";
            $___internal_parse_28_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 811
            echo twig_spaceless($___internal_parse_28_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 832
    public function macro_renderPageComponentAttributes($__pageComponent__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "pageComponent" => $__pageComponent__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 833
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["pageComponent"] ?? null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 834
                echo "        ";
                if (($context["key"] == "layout")) {
                    // line 835
                    echo "            data-layout=\"";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageComponent"] ?? null), "layout", [], "any", false, false, false, 835), "html", null, true);
                    echo "\"
        ";
                } elseif (twig_test_iterable(                // line 836
$context["value"])) {
                    // line 837
                    echo "            data-page-component-";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, json_encode($context["value"]), "html", null, true);
                    echo "\"
        ";
                } else {
                    // line 839
                    echo "            data-page-component-";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "\"
        ";
                }
                // line 841
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 849
    public function macro_renderWidgetAttributes($__options__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 850
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 851
            echo "
    ";
            // line 852
            $context["pageComponent"] = ["module" => "oroui/js/app/components/widget-component", "options" =>             // line 854
($context["options"] ?? null)];
            // line 856
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "options", [], "any", false, true, false, 856), "pageComponentName", [], "any", true, true, false, 856)) {
                // line 857
                echo "        ";
                $context["pageComponent"] = twig_array_merge(($context["pageComponent"] ?? null), ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "options", [], "any", false, false, false, 857), "pageComponentName", [], "any", false, false, false, 857)]);
                // line 858
                echo "    ";
            }
            // line 859
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_renderPageComponentAttributes", [($context["pageComponent"] ?? null)], 859, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 862
    public function macro_renderAttributes($__options__ = null, $__prefix__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "options" => $__options__,
            "prefix" => $__prefix__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 863
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
            foreach ($context['_seq'] as $context["name"] => $context["value"]) {
                // line 864
                echo "        ";
                if (twig_test_iterable($context["value"])) {
                    // line 865
                    echo "            ";
                    $context["value"] = json_encode($context["value"], twig_constant("JSON_FORCE_OBJECT"));
                    // line 866
                    echo "        ";
                }
                // line 867
                echo "        data-";
                if ( !twig_test_empty(($context["prefix"] ?? null))) {
                    echo twig_escape_filter($this->env, ($context["prefix"] ?? null), "html", null, true);
                    echo "-";
                }
                echo twig_escape_filter($this->env, $context["name"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "\"
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 887
    public function macro_clientButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 888
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 889
            echo "
    ";
            // line 891
            echo "        <div class=\"pull-left btn-group icons-holder\">
            ";
            // line 892
            echo twig_call_macro($macros["UIMacro"], "macro_clientLink", [twig_array_merge(($context["parameters"] ?? null), ["class" => "btn icons-holder-text", "role" => "button"])], 892, $context, $this->getSourceContext());
            echo "
        </div>
    ";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 915
    public function macro_ajaxButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 916
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 917
            echo "
    <div class=\"pull-left btn-group icons-holder\">
        ";
            // line 919
            echo twig_call_macro($macros["UIMacro"], "macro_ajaxLink", [twig_array_merge(($context["parameters"] ?? null), ["class" => "btn icons-holder-text", "role" => "button"])], 919, $context, $this->getSourceContext());
            echo "
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 941
    public function macro_ajaxLink($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 942
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 943
            echo "
    ";
            // line 944
            $context["additionalParameters"] = ["pageComponent" => ["module" => "oroui/js/app/components/ajax-button"], "dataAttributes" => ["method" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 949
($context["parameters"] ?? null), "dataMethod", [], "any", true, true, false, 949)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataMethod", [], "any", false, false, false, 949)) : ("GET")), "error-message" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 950
($context["parameters"] ?? null), "errorMessage", [], "any", true, true, false, 950)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "errorMessage", [], "any", false, false, false, 950)) : ("oro.ui.unexpected_error"))]];
            // line 953
            echo "
    ";
            // line 954
            echo twig_call_macro($macros["UIMacro"], "macro_clientLink", [twig_array_merge(($context["parameters"] ?? null), ($context["additionalParameters"] ?? null))], 954, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 973
    public function macro_dropdownClientItem($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 974
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 975
            echo "
    <li>";
            // line 976
            echo twig_call_macro($macros["UIMacro"], "macro_clientLink", [($context["parameters"] ?? null)], 976, $context, $this->getSourceContext());
            echo "</li>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 983
    public function macro_saveAndCloseButton($__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 984
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 985
            echo "
    ";
            // line 986
            $context["defaultParameters"] = ["class" => "btn-success", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save and Close"), "action" => "save_and_close"];
            // line 991
            echo "
    ";
            // line 992
            $context["parameters"] = twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null));
            // line 993
            echo "
    ";
            // line 994
            echo twig_call_macro($macros["UIMacro"], "macro_saveActionButton", [($context["parameters"] ?? null)], 994, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1001
    public function macro_saveAndStayButton($__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1002
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1003
            echo "
    ";
            // line 1004
            $context["defaultParameters"] = ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "action" => "save_and_stay"];
            // line 1008
            echo "
    ";
            // line 1009
            $context["parameters"] = twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null));
            // line 1010
            echo "
    ";
            // line 1011
            echo twig_call_macro($macros["UIMacro"], "macro_saveActionButton", [($context["parameters"] ?? null)], 1011, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1018
    public function macro_saveAndNewButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1019
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1020
            echo "
    ";
            // line 1021
            $context["defaultParameters"] = ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save and New")];
            // line 1024
            echo "
    ";
            // line 1025
            $context["parameters"] = twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null));
            // line 1026
            echo "
    ";
            // line 1027
            echo twig_call_macro($macros["UIMacro"], "macro_saveActionButton", [($context["parameters"] ?? null)], 1027, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1034
    public function macro_saveAndReturnButton($__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1035
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1036
            echo "
    ";
            // line 1037
            $context["defaultParameters"] = ["class" => "btn-success", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save and Return"), "action" => "save_and_return"];
            // line 1042
            echo "
    ";
            // line 1043
            $context["parameters"] = twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null));
            // line 1044
            echo "
    ";
            // line 1045
            echo twig_call_macro($macros["UIMacro"], "macro_saveActionButton", [($context["parameters"] ?? null)], 1045, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1066
    public function macro_saveActionButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1067
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1068
            echo "
    ";
            // line 1069
            $context["defaultParameters"] = ["type" => "submit", "class" => "btn-success main-group", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save")];
            // line 1074
            echo "
    ";
            // line 1075
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "route", [], "any", true, true, false, 1075)) {
                // line 1076
                echo "        ";
                // line 1077
                echo "        ";
                $context["action"] = ["route" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "route", [], "any", false, false, false, 1077)];
                // line 1078
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "params", [], "any", true, true, false, 1078)) {
                    // line 1079
                    echo "            ";
                    $context["action"] = twig_array_merge(($context["action"] ?? null), ["params" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "params", [], "any", false, false, false, 1079)]);
                    // line 1080
                    echo "        ";
                }
                // line 1081
                echo "        ";
                $context["parameters"] = twig_array_merge(($context["parameters"] ?? null), ["action" => json_encode(($context["action"] ?? null))]);
                // line 1082
                echo "    ";
            }
            // line 1083
            echo "
    ";
            // line 1084
            $context["parameters"] = twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null));
            // line 1085
            echo "
    ";
            // line 1086
            echo twig_call_macro($macros["UIMacro"], "macro_buttonType", [($context["parameters"] ?? null)], 1086, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1098
    public function macro_buttonType($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1099
            echo "    ";
            $context["defaultParameters"] = ["type" => "button"];
            // line 1102
            echo "    ";
            $context["parameters"] = twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null));
            // line 1103
            echo "    <div class=\"btn-group\">
        <button type=\"";
            // line 1104
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "type", [], "any", false, false, false, 1104), "html", null, true);
            echo "\" class=\"btn ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", true, true, false, 1104)) {
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 1104), "html", null, true);
            }
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "action", [], "any", true, true, false, 1104)) {
                echo " action-button";
            }
            echo "\"
                ";
            // line 1105
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "action", [], "any", true, true, false, 1105)) {
                echo "data-action=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "action", [], "any", false, false, false, 1105), "html", null, true);
                echo "\"";
            }
            // line 1106
            echo "                ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", true, true, false, 1106)) {
                // line 1107
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", false, false, false, 1107));
                foreach ($context['_seq'] as $context["dataItemName"] => $context["dataItemValue"]) {
                    // line 1108
                    echo "                        data-";
                    echo twig_escape_filter($this->env, $context["dataItemName"], "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, $context["dataItemValue"], "html_attr");
                    echo "\"
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['dataItemName'], $context['dataItemValue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1110
                echo "                ";
            }
            echo ">
            ";
            // line 1111
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 1111), "html", null, true);
            echo "
        </button>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1119
    public function macro_buttonSeparator(...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1120
            echo "    <div class=\"pull-left\" aria-hidden=\"true\">
        <div class=\"separator-btn\"></div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1134
    public function macro_scrollSubblock($__title__ = null, $__data__ = null, $__isForm__ = null, $__useSpan__ = null, $__spanClass__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "isForm" => $__isForm__,
            "useSpan" => $__useSpan__,
            "spanClass" => $__spanClass__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1135
            echo "    ";
            $context["spanClass"] = ((array_key_exists("spanClass", $context)) ? (_twig_default_filter(($context["spanClass"] ?? null), "responsive-cell")) : ("responsive-cell"));
            // line 1136
            echo "    ";
            // line 1144
            echo "    <div class=\"";
            echo twig_escape_filter($this->env, ($context["spanClass"] ?? null), "html", null, true);
            echo " clearfix\">
    ";
            // line 1145
            if (twig_length_filter($this->env, ($context["title"] ?? null))) {
                echo "<h5 class=\"user-fieldset\"><span>";
                echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
                echo "</span></h5>";
            }
            // line 1146
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["dataBlock"]) {
                // line 1147
                echo "        ";
                echo $context["dataBlock"];
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dataBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1149
            echo "    </div>
    ";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1174
    public function macro_scrollBlock($__blockId__ = null, $__title__ = null, $__subblocks__ = null, $__isForm__ = null, $__contentAttributes__ = null, $__useSubBlockDivider__ = null, $__headerLinkContent__ = "", $__options__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "blockId" => $__blockId__,
            "title" => $__title__,
            "subblocks" => $__subblocks__,
            "isForm" => $__isForm__,
            "contentAttributes" => $__contentAttributes__,
            "useSubBlockDivider" => $__useSubBlockDivider__,
            "headerLinkContent" => $__headerLinkContent__,
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1175
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1176
            echo "
    ";
            // line 1177
            $context["cols"] = twig_length_filter($this->env, ($context["subblocks"] ?? null));
            // line 1178
            echo "    <div class=\"responsive-section\"";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "initSectionInstantly", [], "any", true, true, false, 1178) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "initSectionInstantly", [], "any", false, false, false, 1178))) {
                echo " data-init-section-instantly";
            }
            echo ">
        <h4 class=\"scrollspy-title\">";
            // line 1179
            echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
            if (array_key_exists("headerLinkContent", $context)) {
                echo twig_escape_filter($this->env, ($context["headerLinkContent"] ?? null), "html", null, true);
            }
            echo "</h4>
        <div id=\"";
            // line 1180
            echo twig_escape_filter($this->env, ($context["blockId"] ?? null), "html", null, true);
            echo "\" class=\"scrollspy-nav-target\"></div>
        <div class=\"section-content\">
            <div class=\"row-fluid";
            // line 1182
            if (((array_key_exists("contentAttributes", $context) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contentAttributes"] ?? null), "class", [], "any", true, true, false, 1182)) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contentAttributes"] ?? null), "class", [], "any", false, false, false, 1182)))) {
                echo " ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contentAttributes"] ?? null), "class", [], "any", false, false, false, 1182), "html", null, true);
            }
            if (((($context["cols"] ?? null) > 1) && ( !array_key_exists("useSubBlockDivider", $context) || (($context["useSubBlockDivider"] ?? null) == true)))) {
                echo " row-fluid-divider";
            }
            echo "\" ";
            echo twig_call_macro($macros["UIMacro"], "macro_attributes", [($context["contentAttributes"] ?? null), [0 => "class"]], 1182, $context, $this->getSourceContext());
            echo ">
                ";
            // line 1183
            if ((array_key_exists("isForm", $context) && (($context["isForm"] ?? null) == true))) {
                // line 1184
                echo "                    <fieldset class=\"form-horizontal\">
                ";
            } else {
                // line 1186
                echo "                    <div class=\"form-horizontal\">
                ";
            }
            // line 1188
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["subblocks"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["subblock"]) {
                // line 1189
                echo "                        ";
                echo twig_call_macro($macros["UIMacro"], "macro_scrollSubblock", [(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "title", [], "any", true, true, false, 1189) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "title", [], "any", false, false, false, 1189)))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "title", [], "any", false, false, false, 1189)) : (null)), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "data", [], "any", false, false, false, 1189), ($context["isForm"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "useSpan", [], "any", true, true, false, 1189)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "useSpan", [], "any", false, false, false, 1189)) : (true)), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "spanClass", [], "any", true, true, false, 1189)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["subblock"], "spanClass", [], "any", false, false, false, 1189)) : (""))], 1189, $context, $this->getSourceContext());
                echo "
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['subblock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1191
            echo "                ";
            if ((array_key_exists("isForm", $context) && (($context["isForm"] ?? null) == true))) {
                // line 1192
                echo "                    </fieldset>
                ";
            } else {
                // line 1194
                echo "                    </div>
                ";
            }
            // line 1196
            echo "            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1225
    public function macro_scrollData($__dataTarget__ = null, $__data__ = null, $__entity__ = null, $__form__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "dataTarget" => $__dataTarget__,
            "data" => $__data__,
            "entity" => $__entity__,
            "form" => $__form__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1226
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1227
            echo "
    ";
            // line 1228
            $context["data"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->scrollDataBefore($this->env, ($context["dataTarget"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null), ($context["form"] ?? null));
            // line 1229
            echo "
    ";
            // line 1230
            if ((array_key_exists("form", $context) && ($context["form"] ?? null))) {
                // line 1231
                echo "        ";
                $context["isForm"] = true;
                // line 1232
                echo "    ";
            } else {
                // line 1233
                echo "        ";
                $context["isForm"] = false;
                // line 1234
                echo "    ";
            }
            // line 1235
            echo "
    ";
            // line 1236
            $context["dataBlocks"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->sortBy(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "dataBlocks", [], "any", false, false, false, 1236));
            // line 1237
            echo "
    ";
            // line 1238
            if ( !$this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                // line 1239
                echo "        <div id=\"";
                echo twig_escape_filter($this->env, ($context["dataTarget"] ?? null), "html", null, true);
                echo "\" class=\"navbar navbar-static scrollspy-nav\">
            <nav class=\"nav\">
                ";
                // line 1241
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["dataBlocks"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["navElement"]) {
                    // line 1242
                    echo "                    <a class=\"nav-link";
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 1242)) {
                        echo " active ";
                    }
                    echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["navElement"], "class", [], "any", true, true, false, 1242)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["navElement"], "class", [], "any", false, false, false, 1242), "")) : ("")), "html", null, true);
                    echo "\"
                       href=\"#scroll-";
                    // line 1243
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 1243), "html", null, true);
                    echo "\"
                    >";
                    // line 1244
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["navElement"], "title", [], "any", false, false, false, 1244), "html", null, true);
                    echo "</a>
                ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['navElement'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1246
                echo "            </nav>
        </div>
    ";
            }
            // line 1249
            echo "    <div class=\"scrollspy-main-container clearfix\">
        ";
            // line 1250
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "formErrors", [], "any", true, true, false, 1250) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "formErrors", [], "any", false, false, false, 1250)))) {
                // line 1251
                echo "            <div class=\"customer-info-actions container-fluid well-small alert-wrap\" role=\"alert\">
                <div class=\"alert alert-error alert-dismissible\">
                    <button class=\"close\" type=\"button\" data-dismiss=\"alert\" data-target=\".alert-wrap\" aria-label=\"";
                // line 1253
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Close"), "html", null, true);
                echo "\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    ";
                // line 1256
                echo twig_nl2br(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "formErrors", [], "any", false, false, false, 1256));
                echo "
                </div>
            </div>
        ";
            }
            // line 1260
            echo "        <div data-spy=\"scroll\" data-target=\"#";
            echo twig_escape_filter($this->env, ($context["dataTarget"] ?? null), "html", null, true);
            echo "\" data-offset=\"1\" class=\"scrollspy scrollable-container";
            if (($context["isForm"] ?? null)) {
                echo " form-container";
            }
            echo "\">
            <div class=\"container-fluid\">
            ";
            // line 1262
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["dataBlocks"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["scrollBlock"]) {
                // line 1263
                echo "                ";
                echo twig_call_macro($macros["UIMacro"], "macro_scrollBlock", [("scroll-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1264
$context["loop"], "index", [], "any", false, false, false, 1264)), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1265
$context["scrollBlock"], "title", [], "any", false, false, false, 1265), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1266
$context["scrollBlock"], "subblocks", [], "any", false, false, false, 1266),                 // line 1267
($context["isForm"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1268
$context["scrollBlock"], "content_attr", [], "any", true, true, false, 1268)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "content_attr", [], "any", false, false, false, 1268)) : (null)), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1269
$context["scrollBlock"], "useSubBlockDivider", [], "any", true, true, false, 1269)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "useSubBlockDivider", [], "any", false, false, false, 1269)) : (true)), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1270
$context["scrollBlock"], "headerLinkContent", [], "any", true, true, false, 1270)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "headerLinkContent", [], "any", false, false, false, 1270)) : (null)), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1271
$context["scrollBlock"], "options", [], "any", true, true, false, 1271)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scrollBlock"], "options", [], "any", false, false, false, 1271)) : ([]))], 1263, $context, $this->getSourceContext());
                // line 1272
                echo "
            ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['scrollBlock'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1274
            echo "            ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "hiddenData", [], "any", true, true, false, 1274) || ($context["isForm"] ?? null))) {
                // line 1275
                echo "                <div class=\"hide\" data-skip-input-widgets data-layout=\"separate\">
                    ";
                // line 1276
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "hiddenData", [], "any", true, true, false, 1276)) {
                    // line 1277
                    echo "                        ";
                    echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "hiddenData", [], "any", false, false, false, 1277);
                    echo "
                    ";
                }
                // line 1279
                echo "                    ";
                if (($context["isForm"] ?? null)) {
                    // line 1280
                    echo "                        ";
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
                    echo "
                    ";
                }
                // line 1282
                echo "                </div>
            ";
            }
            // line 1284
            echo "            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1295
    public function macro_attributes($__attr__ = null, $__excludes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attr" => $__attr__,
            "excludes" => $__excludes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1296
            echo "    ";
            ob_start(function () { return ''; });
            // line 1297
            echo "        ";
            $context["attr"] = ((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([]));
            // line 1298
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attr"] ?? null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                // line 1299
                echo "            ";
                if (( !array_key_exists("excludes", $context) ||  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["excludes"] ?? null), $context["attrname"], [], "array", true, true, false, 1299))) {
                    // line 1300
                    echo "                ";
                    if ((twig_in_filter($context["attrname"], [0 => "placeholder", 1 => "title"]) && array_key_exists("translation_domain", $context))) {
                        // line 1301
                        echo "                    ";
                        echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                        echo "=\"";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["attrvalue"], [], ($context["translation_domain"] ?? null)), "html", null, true);
                        echo "\"
                ";
                    } else {
                        // line 1303
                        echo "                    ";
                        echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                        echo "=\"";
                        echo twig_escape_filter($this->env, ((twig_test_iterable($context["attrvalue"])) ? (json_encode($context["attrvalue"])) : ($context["attrvalue"])), "html", null, true);
                        echo "\"
                ";
                    }
                    // line 1305
                    echo "            ";
                }
                // line 1306
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1307
            echo "    ";
            $___internal_parse_29_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 1296
            echo twig_spaceless($___internal_parse_29_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1316
    public function macro_entityOwnerLink($__entity__ = null, $__renderLabel__ = true, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "renderLabel" => $__renderLabel__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1317
            $macros["UIMacro"] = $this;
            // line 1318
            echo "
    ";
            // line 1319
            ob_start(function () { return ''; });
            // line 1320
            if (($context["entity"] ?? null)) {
                // line 1321
                echo "            ";
                $context["ownerType"] = $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getOwnerType(($context["entity"] ?? null));
                // line 1322
                if (($context["ownerType"] ?? null)) {
                    // line 1323
                    echo "                ";
                    if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getOwnerFieldName(($context["entity"] ?? null)))) {
                        // line 1324
                        echo "                    ";
                        $context["owner"] = $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getEntityOwner(($context["entity"] ?? null));
                        // line 1325
                        echo "                    ";
                        if (($context["owner"] ?? null)) {
                            // line 1326
                            echo "                        ";
                            if ((($context["ownerType"] ?? null) == "USER")) {
                                // line 1327
                                echo "                            ";
                                $context["ownerPath"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["owner"] ?? null), "id", [], "any", false, false, false, 1327)]);
                                // line 1328
                                echo "                            ";
                                $context["ownerName"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["owner"] ?? null));
                                // line 1329
                                echo "                        ";
                            } elseif ((($context["ownerType"] ?? null) == "BUSINESS_UNIT")) {
                                // line 1330
                                echo "                            ";
                                $context["ownerPath"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_business_unit_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["owner"] ?? null), "id", [], "any", false, false, false, 1330)]);
                                // line 1331
                                echo "                            ";
                                $context["ownerName"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["owner"] ?? null), "name", [], "any", false, false, false, 1331);
                                // line 1332
                                echo "                        ";
                            }
                            // line 1333
                            echo "                        ";
                            if (array_key_exists("ownerName", $context)) {
                                // line 1334
                                echo "                            ";
                                if (($context["renderLabel"] ?? null)) {
                                    // line 1335
                                    echo "                                ";
                                    $context["entityClassName"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null));
                                    // line 1336
                                    echo "                                ";
                                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(                                    // line 1337
($context["entityClassName"] ?? null), $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(                                    // line 1338
($context["entityClassName"] ?? null), "owner_field_name", "ownership"), "label")), "html", null, true);
                                    // line 1341
                                    echo ":
                            ";
                                }
                                // line 1343
                                echo "                            ";
                                if ((array_key_exists("ownerPath", $context) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["owner"] ?? null)))) {
                                    // line 1344
                                    echo "                                ";
                                    echo twig_call_macro($macros["UIMacro"], "macro_renderUrl", [($context["ownerPath"] ?? null), ($context["ownerName"] ?? null)], 1344, $context, $this->getSourceContext());
                                    echo "
                            ";
                                } else {
                                    // line 1346
                                    echo "                                ";
                                    echo twig_escape_filter($this->env, ($context["ownerName"] ?? null), "html", null, true);
                                    echo "
                            ";
                                }
                                // line 1348
                                echo "                        ";
                            }
                            // line 1349
                            echo "                    ";
                        }
                        // line 1350
                        echo "                ";
                    }
                    // line 1351
                    echo "            ";
                }
            }
            $___internal_parse_30_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 1319
            echo twig_spaceless($___internal_parse_30_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1356
    public function macro_renderUrl($__url__ = null, $__text__ = null, $__class__ = null, $__title__ = null, $__attributes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "url" => $__url__,
            "text" => $__text__,
            "class" => $__class__,
            "title" => $__title__,
            "attributes" => $__attributes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1357
            ob_start(function () { return ''; });
            // line 1358
            echo "        ";
            if (twig_test_empty(($context["text"] ?? null))) {
                // line 1359
                echo "            ";
                $context["text"] = ($context["url"] ?? null);
                // line 1360
                echo "        ";
            }
            // line 1361
            echo "        ";
            if (twig_test_empty(($context["title"] ?? null))) {
                // line 1362
                echo "            ";
                $context["title"] = ($context["text"] ?? null);
                // line 1363
                echo "        ";
            }
            // line 1364
            echo "        ";
            if (twig_test_empty(($context["class"] ?? null))) {
                // line 1365
                echo "            ";
                $context["class"] = "";
                // line 1366
                echo "        ";
            }
            // line 1367
            echo "        ";
            if ( !twig_test_empty(($context["url"] ?? null))) {
                // line 1368
                echo "            <a href=\"";
                echo twig_escape_filter($this->env, ($context["url"] ?? null), "html_attr");
                echo "\" title=\"";
                echo twig_escape_filter($this->env, ($context["title"] ?? null), "html_attr");
                echo "\" class=\"";
                echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
                echo "\"
            ";
                // line 1369
                if ( !$this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->isUrlLocal(($context["url"] ?? null))) {
                    echo " target=\"_blank\"";
                }
                // line 1370
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(((array_key_exists("attributes", $context)) ? (_twig_default_filter(($context["attributes"] ?? null), [])) : ([])));
                foreach ($context['_seq'] as $context["name"] => $context["value"]) {
                    // line 1371
                    echo "                ";
                    echo twig_escape_filter($this->env, $context["name"], "html", null, true);
                    echo "=\"";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "\"
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1373
                echo "            >";
                echo twig_escape_filter($this->env, ($context["text"] ?? null), "html", null, true);
                echo "</a>
        ";
            }
            // line 1375
            echo "    ";
            $___internal_parse_31_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 1357
            echo twig_spaceless($___internal_parse_31_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1378
    public function macro_renderUrlWithActions($__parameters__ = null, $__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1379
            $macros["UIMacro"] = $this;
            // line 1380
            echo "
    ";
            // line 1381
            $context["url"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "url", [], "any", true, true, false, 1381)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "url", [], "any", false, false, false, 1381))) : (""));
            // line 1382
            ob_start(function () { return ''; });
            // line 1383
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("url_actions", $context)) ? (_twig_default_filter(($context["url_actions"] ?? null), "url_actions")) : ("url_actions")), ["data" => ($context["url"] ?? null), "entity" => ($context["entity"] ?? null)]);
            $context["actions"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 1385
            $context["actions"] = twig_trim_filter(($context["actions"] ?? null));
            // line 1386
            echo "    <span class=\"inline-actions-element truncate";
            if (twig_test_empty(($context["actions"] ?? null))) {
                echo " inline-actions-element_no-actions";
            }
            echo "\">
        <span class=\"inline-actions-element_wrapper\">
            ";
            // line 1388
            echo twig_call_macro($macros["UIMacro"], "macro_renderUrl", [            // line 1389
($context["url"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 1390
($context["parameters"] ?? null), "text", [], "any", true, true, false, 1390)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "text", [], "any", false, false, false, 1390))) : ("")), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 1391
($context["parameters"] ?? null), "class", [], "any", true, true, false, 1391)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 1391))) : ("")), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 1392
($context["parameters"] ?? null), "title", [], "any", true, true, false, 1392)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 1392))) : ("")), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 1393
($context["parameters"] ?? null), "attributes", [], "any", true, true, false, 1393)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "attributes", [], "any", false, false, false, 1393))) : (""))], 1388, $context, $this->getSourceContext());
            // line 1394
            echo "
        </span>
        ";
            // line 1396
            if ( !twig_test_empty(($context["actions"] ?? null))) {
                // line 1397
                echo "<span class=\"inline-actions-element_actions url-actions\">";
                echo ($context["actions"] ?? null);
                echo "</span>";
            }
            // line 1399
            echo "    </span>";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1402
    public function macro_renderPhone($__phone__ = null, $__title__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "phone" => $__phone__,
            "title" => $__title__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1403
            if (twig_test_empty(($context["title"] ?? null))) {
                // line 1404
                echo "        ";
                $context["title"] = ($context["phone"] ?? null);
                // line 1405
                echo "    ";
            }
            // line 1406
            echo "    ";
            if ( !twig_test_empty(($context["phone"] ?? null))) {
                // line 1407
                echo "        <a href=\"tel:";
                echo twig_escape_filter($this->env, ($context["phone"] ?? null), "html_attr");
                echo "\" title=\"";
                echo twig_escape_filter($this->env, ($context["title"] ?? null), "html_attr");
                echo "\" class=\"phone nowrap\"><bdo dir=\"ltr\">";
                echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
                echo "</bdo></a>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1417
    public function macro_renderPhoneWithActions($__phone__ = null, $__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "phone" => $__phone__,
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1418
            $macros["UIMacro"] = $this;
            // line 1419
            echo "
    ";
            // line 1420
            if ( !twig_test_empty(($context["phone"] ?? null))) {
                // line 1421
                ob_start(function () { return ''; });
                // line 1422
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("phone_actions", $context)) ? (_twig_default_filter(($context["phone_actions"] ?? null), "phone_actions")) : ("phone_actions")), ["phone" => ($context["phone"] ?? null), "entity" => ($context["entity"] ?? null)]);
                $context["actions"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 1424
                $context["actions"] = twig_trim_filter(($context["actions"] ?? null));
                // line 1425
                echo "        <span class=\"inline-actions-element";
                if (twig_test_empty(($context["actions"] ?? null))) {
                    echo " inline-actions-element_no-actions";
                }
                echo "\">
            <span class=\"inline-actions-element_wrapper\">";
                // line 1426
                echo twig_call_macro($macros["UIMacro"], "macro_renderPhone", [($context["phone"] ?? null)], 1426, $context, $this->getSourceContext());
                echo "</span>
            ";
                // line 1427
                if ( !twig_test_empty(($context["actions"] ?? null))) {
                    // line 1428
                    echo "<span class=\"inline-actions-element_actions phone-actions\">";
                    echo ($context["actions"] ?? null);
                    echo "</span>";
                }
                // line 1430
                echo "        </span>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1434
    public function macro_getApplicableForUnderscore($__str__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "str" => $__str__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1435
            echo "    ";
            echo twig_replace_filter(($context["str"] ?? null), ["<script" => "<% print(\"<sc\" + \"ript\"); %>", "</script" => "<% print(\"</sc\" + \"ript\"); %>", "<%" => "<% print(\"<\" + \"%\"); %>", "%>" => "<% print(\"%\" + \">\"); %>"]);
            // line 1440
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1443
    public function macro_renderList($__elements__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "elements" => $__elements__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1444
            echo "<ul class=\"extra-list\">";
            // line 1445
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["elements"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 1446
                echo "            <li class=\"extra-list-element\">";
                echo twig_escape_filter($this->env, $context["element"], "html", null, true);
                echo "</li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1448
            echo "</ul>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1451
    public function macro_renderTable($__titles__ = null, $__rows__ = null, $__style__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "titles" => $__titles__,
            "rows" => $__rows__,
            "style" => $__style__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1452
            echo "<table class=\"";
            echo twig_escape_filter($this->env, ($context["style"] ?? null), "html", null, true);
            echo "\">
     <thead>
     <tr>";
            // line 1455
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["titles"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
                // line 1456
                echo "        <th>";
                echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                echo "</th>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1458
            echo "</tr>
     </thead>";
            // line 1460
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["rows"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["row"]) {
                // line 1461
                echo "        <tr>";
                // line 1462
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["row"]);
                foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                    // line 1463
                    echo "                <td>";
                    echo twig_escape_filter($this->env, $context["element"], "html", null, true);
                    echo "</td>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1465
                echo "</tr>
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['row'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1467
            echo "</table>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1470
    public function macro_entityViewLink($__entity__ = null, $__label__ = null, $__route__ = null, $__permission__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "label" => $__label__,
            "route" => $__route__,
            "permission" => $__permission__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1471
            $macros["UIMacro"] = $this;
            // line 1472
            echo "
    ";
            // line 1473
            if (($context["entity"] ?? null)) {
                // line 1474
                echo "        ";
                if (((($context["route"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(((array_key_exists("permission", $context)) ? (_twig_default_filter(($context["permission"] ?? null), "VIEW")) : ("VIEW")), ($context["entity"] ?? null))) && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled(($context["route"] ?? null), "routes"))) {
                    // line 1475
                    echo "            ";
                    echo twig_call_macro($macros["UIMacro"], "macro_renderUrl", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["route"] ?? null), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 1475)]), ($context["label"] ?? null)], 1475, $context, $this->getSourceContext());
                    echo "
        ";
                } else {
                    // line 1477
                    echo "            ";
                    echo twig_escape_filter($this->env, ($context["label"] ?? null));
                    echo "
        ";
                }
                // line 1479
                echo "    ";
            } else {
                // line 1480
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"), "html", null, true);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1484
    public function macro_entityViewLinks($__entities__ = null, $__labelProperty__ = null, $__route__ = null, $__permission__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entities" => $__entities__,
            "labelProperty" => $__labelProperty__,
            "route" => $__route__,
            "permission" => $__permission__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1485
            $macros["UIMacro"] = $this;
            // line 1486
            echo "
    ";
            // line 1487
            $context["links"] = [];
            // line 1488
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["entities"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["entity"]) {
                // line 1489
                echo "        ";
                $context["links"] = twig_array_merge(($context["links"] ?? null), [0 => twig_call_macro($macros["UIMacro"], "macro_entityViewLink", [$context["entity"], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["entity"], ($context["labelProperty"] ?? null), [], "any", false, false, false, 1489), ($context["route"] ?? null), ($context["permission"] ?? null)], 1489, $context, $this->getSourceContext())]);
                // line 1490
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entity'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 1491
            echo "    ";
            echo twig_call_macro($macros["UIMacro"], "macro_renderList", [($context["links"] ?? null)], 1491, $context, $this->getSourceContext());

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1499
    public function macro_renderDisabledLabel($__labelText__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "labelText" => $__labelText__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1500
            echo "<i>";
            echo twig_escape_filter($this->env, ($context["labelText"] ?? null), "html", null, true);
            echo "</i>";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1511
    public function macro_renderEntityViewLabel($__entity__ = null, $__fieldName__ = null, $__entityLabelIfNotGranted__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "entityLabelIfNotGranted" => $__entityLabelIfNotGranted__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1512
            if (( !(null === ($context["entity"] ?? null)) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["fieldName"] ?? null)))) {
                // line 1513
                echo "        ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), ($context["fieldName"] ?? null), [], "any", false, false, false, 1513), "html", null, true);
                echo "
    ";
            } else {
                // line 1515
                echo "        ";
                if ( !(null === ($context["entityLabelIfNotGranted"] ?? null))) {
                    // line 1516
                    echo "            ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("view %entityName%", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["entityLabelIfNotGranted"] ?? null))]), "html", null, true);
                    echo "
        ";
                }
                // line 1518
                echo "    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1521
    public function macro_renderJsTree($__data__ = null, $__actions__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "data" => $__data__,
            "actions" => $__actions__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1522
            $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/macros.html.twig", 1522, "118842245")->display(twig_array_merge($context, ["data" =>             // line 1523
($context["data"] ?? null), "actions" =>             // line 1524
($context["actions"] ?? null)]));

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1529
    public function macro_app_logo($__organization_name__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "organization_name" => $__organization_name__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1530
            echo "    ";
            $context["organization_name"] = ((array_key_exists("organization_name", $context)) ? (_twig_default_filter(($context["organization_name"] ?? null), "")) : (""));
            // line 1531
            echo "    <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_default");
            echo "\" class=\"app-logo\" title=\"";
            echo twig_escape_filter($this->env, twig_trim_filter(($context["organization_name"] ?? null)), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_trim_filter(($context["organization_name"] ?? null)), "html", null, true);
            echo "</a>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1534
    public function macro_insertIcon($__classNames__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "classNames" => $__classNames__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1535
            echo "    ";
            $context["classNames"] = ((array_key_exists("classNames", $context)) ? (_twig_default_filter(($context["classNames"] ?? null), "")) : (""));
            // line 1536
            echo "
    ";
            // line 1537
            if (($context["classNames"] ?? null)) {
                // line 1538
                echo "        <span class=\"";
                echo twig_escape_filter($this->env, ($context["classNames"] ?? null), "html", null, true);
                echo "\" aria-hidden=\"true\"></span>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1555
    public function macro_sortable_list_view($__widget__ = null, $__attr__ = [], $__options__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "attr" => $__attr__,
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1556
            echo "    ";
            $context["originalAttr"] = ($context["attr"] ?? null);
            // line 1557
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1558
            echo "
    ";
            // line 1559
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, true, false, 1559), "prototype", [], "any", true, true, false, 1559)) {
                // line 1560
                echo "        ";
                $context["prototype_html"] = twig_call_macro($macros["UIMacro"], "macro_sortable_list_view_collection_prototype", [($context["widget"] ?? null), ($context["options"] ?? null), ($context["attr"] ?? null)], 1560, $context, $this->getSourceContext());
                // line 1561
                echo "    ";
            }
            // line 1562
            echo "
    ";
            // line 1563
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 1563)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 1563) . " ")) : ("")) . "oro-item-collection grid-container")]);
            // line 1564
            echo "    ";
            $context["id"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 1564), "id", [], "any", false, false, false, 1564) . "_collection");
            // line 1565
            echo "    <div class=\"";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "collection_class", [], "any", true, true, false, 1565)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "collection_class", [], "any", false, false, false, 1565), "")) : ("")), "html", null, true);
            echo " drag-n-drop-sorting-view\" ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_page_component_module", [], "any", true, true, false, 1565)) {
                echo " data-page-component-module=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_page_component_module", [], "any", false, false, false, 1565));
                echo "\"";
            }
            echo ">
        <div class=\"drag-n-drop-sorting-view__wrapper row-oro\" ";
            // line 1566
            echo twig_call_macro($macros["UIMacro"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["view" => "oroui/js/drag-n-drop-sorting", "autoRender" => true]]], 1566, $context, $this->getSourceContext());
            // line 1572
            echo ">
            ";
            // line 1573
            $context["prototype_name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 1573), "prototype_name", [], "any", false, false, false, 1573);
            // line 1574
            echo "           <div ";
            echo twig_call_macro($macros["UIMacro"], "macro_attributes", [($context["attr"] ?? null)], 1574, $context, $this->getSourceContext());
            echo ">
                <table class=\"grid grid-main-container table-hover table table-bordered\">
                    <thead>
                    <tr>
                        <th class=\"drag-n-drop-sorting-view__column\" scope=\"col\"><span>";
            // line 1578
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "column_label", [], "any", true, true, false, 1578)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "column_label", [], "any", false, false, false, 1578), "")) : ("")), "html", null, true);
            echo "</span></th>
                        <th class=\"drag-n-drop-sorting-view__column_options\" scope=\"col\"><span>";
            // line 1579
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "column_options_label", [], "any", true, true, false, 1579)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "column_options_label", [], "any", false, false, false, 1579), "")) : ("")), "html", null, true);
            echo "</span></th>
                        ";
            // line 1580
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mergeAllowed", [], "any", true, true, false, 1580)) {
                // line 1581
                echo "                            <th></th>
                        ";
            }
            // line 1583
            echo "                    </tr>
                    </thead>
                    <tbody class=\"sortable-wrapper\" data-last-index=\"";
            // line 1585
            echo twig_escape_filter($this->env, twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "children", [], "any", false, false, false, 1585)), "html", null, true);
            echo "\" data-prototype-name=\"";
            echo twig_escape_filter($this->env, ($context["prototype_name"] ?? null), "html", null, true);
            echo "\"";
            if (array_key_exists("prototype_html", $context)) {
                echo " data-prototype=\"";
                echo twig_escape_filter($this->env, ($context["prototype_html"] ?? null));
                echo "\"";
            }
            echo ">
                    ";
            // line 1586
            if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "children", [], "any", false, false, false, 1586))) {
                // line 1587
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "children", [], "any", false, false, false, 1587));
                foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                    // line 1588
                    echo "                            ";
                    $context["widgetContainerAttributes"] = twig_array_filter($this->env, ($context["originalAttr"] ?? null), function ($__v__, $__k__) use ($context, $macros) { $context["v"] = $__v__; $context["k"] = $__k__; return !twig_in_filter(($context["k"] ?? null), [0 => "id", 1 => "class"]); });
                    // line 1589
                    echo "                            ";
                    echo twig_call_macro($macros["UIMacro"], "macro_sortable_list_view_collection_prototype", [$context["child"], ($context["options"] ?? null), ($context["widgetContainerAttributes"] ?? null)], 1589, $context, $this->getSourceContext());
                    echo "
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 1591
                echo "                    ";
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "show_form_when_empty", [], "any", true, true, false, 1591) && array_key_exists("prototype_html", $context))) {
                // line 1592
                echo "                        ";
                echo twig_replace_filter(($context["prototype_html"] ?? null), [($context["prototype_name"] ?? null) => "0"]);
                echo "
                    ";
            }
            // line 1594
            echo "                    </tbody>
                </table>
            </div>
            <button type=\"button\" class=\"btn add-list-item\" data-container=\".oro-item-collection tbody\">
                ";
            // line 1598
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "add_btn_label", [], "any", true, true, false, 1598)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "add_btn_label", [], "any", false, false, false, 1598), "")) : ("")), "html", null, true);
            echo "
            </button>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1604
    public function macro_sortable_list_view_collection_prototype($__widget__ = null, $__options__ = null, $__widgetContainerAttributes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "options" => $__options__,
            "widgetContainerAttributes" => $__widgetContainerAttributes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1605
            echo "    ";
            if (twig_in_filter("collection", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 1605), "block_prefixes", [], "any", false, false, false, 1605))) {
                // line 1606
                echo "        ";
                $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 1606), "prototype", [], "any", false, false, false, 1606);
                // line 1607
                echo "        ";
                $context["name"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 1607), "full_name", [], "any", false, false, false, 1607) . "[") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 1607), "prototype", [], "any", false, false, false, 1607), "vars", [], "any", false, false, false, 1607), "name", [], "any", false, false, false, 1607)) . "]");
                // line 1608
                echo "    ";
            } else {
                // line 1609
                echo "        ";
                $context["form"] = ($context["widget"] ?? null);
                // line 1610
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 1610), "full_name", [], "any", false, false, false, 1610);
                // line 1611
                echo "    ";
            }
            // line 1612
            echo "    ";
            ob_start(function () { return ''; });
            // line 1613
            echo "        ";
            $macros["UIMacro"] = $this;
            // line 1614
            echo "        ";
            if (!twig_in_filter("data-validation-optional-group", ($context["widgetContainerAttributes"] ?? null))) {
                // line 1615
                echo "            ";
                $context["widgetContainerAttributes"] = twig_array_merge(($context["widgetContainerAttributes"] ?? null), ["data-validation-optional-group" => ""]);
                // line 1616
                echo "        ";
            }
            // line 1617
            echo "        <tr data-content=\"";
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\" ";
            echo twig_call_macro($macros["UIMacro"], "macro_attributes", [($context["widgetContainerAttributes"] ?? null)], 1617, $context, $this->getSourceContext());
            echo "
            class=\"";
            // line 1618
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "collection_class", [], "any", true, true, false, 1618)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "collection_class", [], "any", false, false, false, 1618), "")) : ("")), "html", null, true);
            echo "\">
        ";
            // line 1619
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
    </tr>
    ";
            $___internal_parse_32_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 1612
            echo twig_spaceless($___internal_parse_32_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1624
    public function macro_render_birthday($__birthday__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "birthday" => $__birthday__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1625
            echo "    ";
            if ( !twig_test_empty(($context["birthday"] ?? null))) {
                // line 1626
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(($context["birthday"] ?? null)), "html", null, true);
                echo "
        ";
                // line 1627
                $context["age"] = $this->extensions['Oro\Bundle\UIBundle\Twig\FormatExtension']->getAgeAsString(($context["birthday"] ?? null));
                // line 1628
                echo "        ";
                (( !twig_test_empty(($context["age"] ?? null))) ? (print (twig_escape_filter($this->env, (("(" . ($context["age"] ?? null)) . ")"), "html", null, true))) : (print (null)));
                echo "
    ";
            } else {
                // line 1630
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1634
    public function macro_badge($__label__ = null, $__badgeClass__ = "info", $__iconClass__ = "fa-circle", ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "label" => $__label__,
            "badgeClass" => $__badgeClass__,
            "iconClass" => $__iconClass__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1635
            echo "    <span class=\"badge badge-pill badge-";
            echo twig_escape_filter($this->env, ($context["badgeClass"] ?? null), "html", null, true);
            echo " status-";
            echo twig_escape_filter($this->env, ($context["badgeClass"] ?? null), "html", null, true);
            echo "\">
        <i class=\"icon-status-";
            // line 1636
            echo twig_escape_filter($this->env, ($context["badgeClass"] ?? null), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ($context["iconClass"] ?? null), "html", null, true);
            echo "\" aria-hidden=\"true\"></i>";
            echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
            echo "
    </span>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1640
    public function macro_renderWysiwygContentPreview($__content__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "content" => $__content__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1641
            ob_start(function () { return ''; });
            // line 1642
            if ( !twig_test_empty(($context["content"] ?? null))) {
                // line 1643
                echo "            <div data-page-component-view=\"oroentityconfig/js/views/wysiwyg-content-preview\">";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->env->getFilter('render_content')->getCallable()(($context["content"] ?? null)));
                echo "</div>
        ";
            }
            $___internal_parse_33_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 1641
            echo twig_spaceless($___internal_parse_33_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1648
    public function macro_renderCollapsibleWysiwygContentPreview($__content__ = null, $__entity__ = null, $__fieldName__ = null, $__previewMessage__ = "oro.ui.simplified_preview.message", $__moreText__ = "oro.ui.collapse.more", $__lessText__ = "oro.ui.collapse.less", $__noDataMessage__ = "oro.ui.empty", ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "content" => $__content__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "previewMessage" => $__previewMessage__,
            "moreText" => $__moreText__,
            "lessText" => $__lessText__,
            "noDataMessage" => $__noDataMessage__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1649
            if (((twig_test_empty(($context["entity"] ?? null)) || twig_test_empty(($context["fieldName"] ?? null))) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), ($context["fieldName"] ?? null)))) {
                // line 1650
                $macros["UIMacro"] = $this;
                // line 1651
                if ( !twig_test_empty(twig_trim_filter(($context["content"] ?? null)))) {
                    // line 1652
                    if ( !twig_test_empty(($context["previewMessage"] ?? null))) {
                        // line 1653
                        echo "<div class=\"alert alert-info alert--compact\" role=\"alert\">
                    <span class=\"fa-info alert-icon\" aria-hidden=\"true\"></span>
                    ";
                        // line 1655
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["previewMessage"] ?? null)), "html", null, true);
                        echo "
                </div>";
                    }
                    // line 1658
                    echo "<div class=\"cms-content\">";
                    // line 1659
                    echo twig_call_macro($macros["UIMacro"], "macro_renderCollapsibleHtml", [twig_call_macro($macros["UIMacro"], "macro_renderWysiwygContentPreview", [($context["content"] ?? null)], 1659, $context, $this->getSourceContext()), ($context["entity"] ?? null), ($context["fieldName"] ?? null), ($context["moreText"] ?? null), ($context["lessText"] ?? null)], 1659, $context, $this->getSourceContext());
                    // line 1660
                    echo "</div>";
                } elseif ( !twig_test_empty(                // line 1661
($context["noDataMessage"] ?? null))) {
                    // line 1662
                    echo "<div class=\"no-data\">";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["noDataMessage"] ?? null)), "html", null, true);
                    echo "</div>";
                }
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1667
    public function macro_renderButtonsRow($__content__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "content" => $__content__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1668
            echo "<div class=\"buttons-row ";
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
                echo "buttons-row--start-offset buttons-row--pull-end";
            } else {
                echo "buttons-row--bottom-offset";
            }
            echo "\">
    ";
            // line 1669
            echo twig_escape_filter($this->env, ($context["content"] ?? null), "html", null, true);
            echo "
</div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1693
    public function macro_quickAccessLink($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1694
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1695
            echo "
    ";
            // line 1696
            $context["quickCreateAction"] = $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_ui.quick_create_actions");
            // line 1697
            echo "
    ";
            // line 1698
            $context["defaultParameters"] = ["iCss" => "fa-plus", "class" => "dropdown-item"];
            // line 1702
            echo "
    ";
            // line 1703
            if ((($context["quickCreateAction"] ?? null) == "popup")) {
                // line 1704
                echo "        ";
                $context["clientLinkParameters"] = twig_array_merge(($context["defaultParameters"] ?? null), ["widget" => ["type" => "dialog", "multiple" => false, "options" => ["dialogOptions" => ["allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "autoResize" => true, "width" => 812, "minWidth" => "expanded", "modal" => false]]]]);
                // line 1722
                echo "        ";
                echo twig_call_macro($macros["UIMacro"], "macro_clientLink", [Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(($context["clientLinkParameters"] ?? null), ($context["parameters"] ?? null))], 1722, $context, $this->getSourceContext());
                echo "
    ";
            } else {
                // line 1724
                echo "        ";
                $context["linkParameters"] = twig_array_filter($this->env, twig_array_merge(twig_array_merge(($context["defaultParameters"] ?? null),                 // line 1725
($context["parameters"] ?? null)), ["path" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 1727
($context["parameters"] ?? null), "dataUrl", [], "any", false, false, false, 1727), "target" => (((                // line 1728
($context["quickCreateAction"] ?? null) == "current_page")) ? ("_self") : ("_blank"))]),                 // line 1730
function ($__v__, $__k__) use ($context, $macros) { $context["v"] = $__v__; $context["k"] = $__k__; return ((($context["k"] ?? null) != "widget") && (($context["k"] ?? null) != "dataUrl")); });
                // line 1732
                echo "        ";
                echo twig_call_macro($macros["UIMacro"], "macro_link", [($context["linkParameters"] ?? null)], 1732, $context, $this->getSourceContext());
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1748
    public function macro_quickAccessAddButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1749
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1750
            echo "
    ";
            // line 1751
            $context["defaultParameters"] = ["class" => "btn main-group btn-primary pull-right", "role" => "button"];
            // line 1755
            echo "
    ";
            // line 1756
            echo twig_call_macro($macros["UIMacro"], "macro_quickAccessLink", [twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null))], 1756, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1771
    public function macro_quickAccessButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 1772
            echo "    ";
            $macros["UIMacro"] = $this;
            // line 1773
            echo "
    ";
            // line 1774
            $context["defaultParameters"] = ["class" => "btn back icons-holder-text", "role" => "button"];
            // line 1778
            echo "
    <div class=\"pull-left btn-group icons-holder\">
        ";
            // line 1780
            echo twig_call_macro($macros["UIMacro"], "macro_quickAccessLink", [twig_array_merge(($context["defaultParameters"] ?? null), ($context["parameters"] ?? null))], 1780, $context, $this->getSourceContext());
            echo "
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroUI/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  4467 => 1780,  4463 => 1778,  4461 => 1774,  4458 => 1773,  4455 => 1772,  4442 => 1771,  4431 => 1756,  4428 => 1755,  4426 => 1751,  4423 => 1750,  4420 => 1749,  4407 => 1748,  4394 => 1732,  4392 => 1730,  4391 => 1728,  4390 => 1727,  4389 => 1725,  4387 => 1724,  4381 => 1722,  4378 => 1704,  4376 => 1703,  4373 => 1702,  4371 => 1698,  4368 => 1697,  4366 => 1696,  4363 => 1695,  4360 => 1694,  4347 => 1693,  4335 => 1669,  4326 => 1668,  4313 => 1667,  4300 => 1662,  4298 => 1661,  4296 => 1660,  4294 => 1659,  4292 => 1658,  4287 => 1655,  4283 => 1653,  4281 => 1652,  4279 => 1651,  4277 => 1650,  4275 => 1649,  4256 => 1648,  4247 => 1641,  4240 => 1643,  4238 => 1642,  4236 => 1641,  4223 => 1640,  4207 => 1636,  4200 => 1635,  4185 => 1634,  4172 => 1630,  4166 => 1628,  4164 => 1627,  4159 => 1626,  4156 => 1625,  4143 => 1624,  4134 => 1612,  4128 => 1619,  4124 => 1618,  4117 => 1617,  4114 => 1616,  4111 => 1615,  4108 => 1614,  4105 => 1613,  4102 => 1612,  4099 => 1611,  4096 => 1610,  4093 => 1609,  4090 => 1608,  4087 => 1607,  4084 => 1606,  4081 => 1605,  4066 => 1604,  4052 => 1598,  4046 => 1594,  4040 => 1592,  4037 => 1591,  4028 => 1589,  4025 => 1588,  4020 => 1587,  4018 => 1586,  4006 => 1585,  4002 => 1583,  3998 => 1581,  3996 => 1580,  3992 => 1579,  3988 => 1578,  3980 => 1574,  3978 => 1573,  3975 => 1572,  3973 => 1566,  3962 => 1565,  3959 => 1564,  3957 => 1563,  3954 => 1562,  3951 => 1561,  3948 => 1560,  3946 => 1559,  3943 => 1558,  3940 => 1557,  3937 => 1556,  3922 => 1555,  3909 => 1538,  3907 => 1537,  3904 => 1536,  3901 => 1535,  3888 => 1534,  3872 => 1531,  3869 => 1530,  3856 => 1529,  3847 => 1524,  3846 => 1523,  3845 => 1522,  3831 => 1521,  3821 => 1518,  3815 => 1516,  3812 => 1515,  3806 => 1513,  3804 => 1512,  3789 => 1511,  3778 => 1500,  3765 => 1499,  3755 => 1491,  3749 => 1490,  3746 => 1489,  3741 => 1488,  3739 => 1487,  3736 => 1486,  3734 => 1485,  3718 => 1484,  3705 => 1480,  3702 => 1479,  3696 => 1477,  3690 => 1475,  3687 => 1474,  3685 => 1473,  3682 => 1472,  3680 => 1471,  3664 => 1470,  3654 => 1467,  3647 => 1465,  3638 => 1463,  3634 => 1462,  3632 => 1461,  3628 => 1460,  3625 => 1458,  3616 => 1456,  3612 => 1455,  3606 => 1452,  3591 => 1451,  3581 => 1448,  3572 => 1446,  3568 => 1445,  3566 => 1444,  3553 => 1443,  3543 => 1440,  3540 => 1435,  3527 => 1434,  3516 => 1430,  3511 => 1428,  3509 => 1427,  3505 => 1426,  3498 => 1425,  3496 => 1424,  3493 => 1422,  3491 => 1421,  3489 => 1420,  3486 => 1419,  3484 => 1418,  3470 => 1417,  3453 => 1407,  3450 => 1406,  3447 => 1405,  3444 => 1404,  3442 => 1403,  3428 => 1402,  3419 => 1399,  3414 => 1397,  3412 => 1396,  3408 => 1394,  3406 => 1393,  3405 => 1392,  3404 => 1391,  3403 => 1390,  3402 => 1389,  3401 => 1388,  3393 => 1386,  3391 => 1385,  3388 => 1383,  3386 => 1382,  3384 => 1381,  3381 => 1380,  3379 => 1379,  3365 => 1378,  3356 => 1357,  3353 => 1375,  3347 => 1373,  3336 => 1371,  3331 => 1370,  3327 => 1369,  3318 => 1368,  3315 => 1367,  3312 => 1366,  3309 => 1365,  3306 => 1364,  3303 => 1363,  3300 => 1362,  3297 => 1361,  3294 => 1360,  3291 => 1359,  3288 => 1358,  3286 => 1357,  3269 => 1356,  3260 => 1319,  3255 => 1351,  3252 => 1350,  3249 => 1349,  3246 => 1348,  3240 => 1346,  3234 => 1344,  3231 => 1343,  3227 => 1341,  3225 => 1338,  3224 => 1337,  3222 => 1336,  3219 => 1335,  3216 => 1334,  3213 => 1333,  3210 => 1332,  3207 => 1331,  3204 => 1330,  3201 => 1329,  3198 => 1328,  3195 => 1327,  3192 => 1326,  3189 => 1325,  3186 => 1324,  3183 => 1323,  3181 => 1322,  3178 => 1321,  3176 => 1320,  3174 => 1319,  3171 => 1318,  3169 => 1317,  3155 => 1316,  3146 => 1296,  3143 => 1307,  3137 => 1306,  3134 => 1305,  3126 => 1303,  3118 => 1301,  3115 => 1300,  3112 => 1299,  3107 => 1298,  3104 => 1297,  3101 => 1296,  3087 => 1295,  3075 => 1284,  3071 => 1282,  3065 => 1280,  3062 => 1279,  3056 => 1277,  3054 => 1276,  3051 => 1275,  3048 => 1274,  3033 => 1272,  3031 => 1271,  3030 => 1270,  3029 => 1269,  3028 => 1268,  3027 => 1267,  3026 => 1266,  3025 => 1265,  3024 => 1264,  3022 => 1263,  3005 => 1262,  2995 => 1260,  2988 => 1256,  2982 => 1253,  2978 => 1251,  2976 => 1250,  2973 => 1249,  2968 => 1246,  2952 => 1244,  2948 => 1243,  2940 => 1242,  2923 => 1241,  2917 => 1239,  2915 => 1238,  2912 => 1237,  2910 => 1236,  2907 => 1235,  2904 => 1234,  2901 => 1233,  2898 => 1232,  2895 => 1231,  2893 => 1230,  2890 => 1229,  2888 => 1228,  2885 => 1227,  2882 => 1226,  2866 => 1225,  2854 => 1196,  2850 => 1194,  2846 => 1192,  2843 => 1191,  2834 => 1189,  2829 => 1188,  2825 => 1186,  2821 => 1184,  2819 => 1183,  2807 => 1182,  2802 => 1180,  2795 => 1179,  2788 => 1178,  2786 => 1177,  2783 => 1176,  2780 => 1175,  2760 => 1174,  2750 => 1149,  2741 => 1147,  2736 => 1146,  2730 => 1145,  2725 => 1144,  2723 => 1136,  2720 => 1135,  2703 => 1134,  2691 => 1120,  2679 => 1119,  2666 => 1111,  2661 => 1110,  2650 => 1108,  2645 => 1107,  2642 => 1106,  2636 => 1105,  2625 => 1104,  2622 => 1103,  2619 => 1102,  2616 => 1099,  2603 => 1098,  2592 => 1086,  2589 => 1085,  2587 => 1084,  2584 => 1083,  2581 => 1082,  2578 => 1081,  2575 => 1080,  2572 => 1079,  2569 => 1078,  2566 => 1077,  2564 => 1076,  2562 => 1075,  2559 => 1074,  2557 => 1069,  2554 => 1068,  2551 => 1067,  2538 => 1066,  2527 => 1045,  2524 => 1044,  2522 => 1043,  2519 => 1042,  2517 => 1037,  2514 => 1036,  2511 => 1035,  2498 => 1034,  2487 => 1027,  2484 => 1026,  2482 => 1025,  2479 => 1024,  2477 => 1021,  2474 => 1020,  2471 => 1019,  2458 => 1018,  2447 => 1011,  2444 => 1010,  2442 => 1009,  2439 => 1008,  2437 => 1004,  2434 => 1003,  2431 => 1002,  2418 => 1001,  2407 => 994,  2404 => 993,  2402 => 992,  2399 => 991,  2397 => 986,  2394 => 985,  2391 => 984,  2378 => 983,  2367 => 976,  2364 => 975,  2361 => 974,  2348 => 973,  2337 => 954,  2334 => 953,  2332 => 950,  2331 => 949,  2330 => 944,  2327 => 943,  2324 => 942,  2311 => 941,  2299 => 919,  2295 => 917,  2292 => 916,  2279 => 915,  2267 => 892,  2264 => 891,  2261 => 889,  2258 => 888,  2245 => 887,  2223 => 867,  2220 => 866,  2217 => 865,  2214 => 864,  2209 => 863,  2195 => 862,  2183 => 859,  2180 => 858,  2177 => 857,  2174 => 856,  2172 => 854,  2171 => 852,  2168 => 851,  2165 => 850,  2152 => 849,  2139 => 841,  2131 => 839,  2123 => 837,  2121 => 836,  2116 => 835,  2113 => 834,  2108 => 833,  2095 => 832,  2086 => 811,  2082 => 825,  2080 => 824,  2078 => 823,  2076 => 822,  2071 => 820,  2069 => 819,  2064 => 818,  2062 => 817,  2056 => 815,  2054 => 814,  2049 => 813,  2046 => 812,  2043 => 811,  2040 => 810,  2038 => 808,  2036 => 807,  2033 => 806,  2020 => 805,  2011 => 775,  2007 => 782,  2002 => 780,  2000 => 779,  1996 => 778,  1994 => 777,  1992 => 776,  1989 => 775,  1986 => 774,  1973 => 773,  1963 => 759,  1961 => 758,  1958 => 756,  1956 => 755,  1951 => 753,  1948 => 750,  1947 => 749,  1945 => 748,  1942 => 747,  1939 => 746,  1935 => 744,  1932 => 743,  1926 => 741,  1923 => 740,  1917 => 738,  1914 => 737,  1908 => 735,  1905 => 734,  1899 => 732,  1896 => 731,  1890 => 729,  1887 => 728,  1881 => 726,  1878 => 725,  1872 => 723,  1869 => 722,  1863 => 720,  1860 => 719,  1854 => 717,  1851 => 716,  1845 => 714,  1842 => 713,  1836 => 711,  1833 => 710,  1827 => 708,  1824 => 707,  1818 => 705,  1815 => 704,  1812 => 703,  1799 => 702,  1790 => 668,  1786 => 678,  1784 => 677,  1782 => 676,  1779 => 675,  1774 => 673,  1772 => 672,  1767 => 671,  1765 => 670,  1763 => 669,  1760 => 668,  1758 => 666,  1756 => 665,  1753 => 664,  1740 => 663,  1728 => 640,  1725 => 639,  1719 => 638,  1716 => 637,  1713 => 636,  1708 => 635,  1705 => 634,  1702 => 633,  1699 => 632,  1696 => 631,  1693 => 630,  1690 => 629,  1687 => 628,  1684 => 627,  1682 => 626,  1679 => 625,  1677 => 622,  1676 => 621,  1675 => 620,  1674 => 619,  1673 => 617,  1672 => 616,  1671 => 615,  1669 => 613,  1666 => 612,  1663 => 611,  1661 => 610,  1658 => 609,  1655 => 608,  1653 => 605,  1651 => 604,  1648 => 603,  1645 => 602,  1642 => 601,  1639 => 600,  1637 => 599,  1634 => 598,  1631 => 597,  1618 => 596,  1606 => 592,  1602 => 590,  1600 => 589,  1597 => 588,  1594 => 587,  1591 => 586,  1589 => 585,  1586 => 584,  1583 => 583,  1580 => 582,  1578 => 581,  1575 => 580,  1573 => 579,  1570 => 578,  1567 => 577,  1554 => 576,  1542 => 558,  1540 => 557,  1537 => 556,  1534 => 555,  1532 => 552,  1530 => 551,  1527 => 550,  1524 => 549,  1521 => 548,  1518 => 547,  1516 => 544,  1514 => 543,  1511 => 542,  1508 => 541,  1505 => 540,  1492 => 539,  1481 => 525,  1478 => 524,  1475 => 523,  1472 => 521,  1470 => 519,  1469 => 518,  1468 => 517,  1467 => 516,  1465 => 515,  1463 => 512,  1461 => 511,  1458 => 510,  1456 => 507,  1454 => 506,  1451 => 505,  1448 => 504,  1445 => 503,  1442 => 502,  1439 => 501,  1436 => 500,  1433 => 499,  1430 => 498,  1427 => 497,  1424 => 496,  1422 => 495,  1419 => 494,  1416 => 493,  1403 => 492,  1391 => 478,  1388 => 477,  1385 => 476,  1383 => 475,  1380 => 474,  1377 => 473,  1363 => 472,  1351 => 463,  1349 => 462,  1347 => 455,  1344 => 454,  1331 => 453,  1318 => 446,  1315 => 445,  1313 => 436,  1307 => 442,  1304 => 441,  1302 => 440,  1301 => 438,  1298 => 437,  1295 => 436,  1293 => 433,  1292 => 432,  1291 => 431,  1289 => 430,  1286 => 429,  1284 => 428,  1281 => 427,  1278 => 426,  1265 => 425,  1253 => 413,  1250 => 412,  1237 => 411,  1226 => 396,  1220 => 394,  1217 => 393,  1214 => 392,  1205 => 390,  1200 => 389,  1198 => 388,  1192 => 387,  1186 => 385,  1180 => 383,  1178 => 382,  1172 => 379,  1168 => 378,  1165 => 377,  1163 => 376,  1160 => 375,  1157 => 374,  1144 => 373,  1132 => 359,  1128 => 357,  1125 => 356,  1112 => 355,  1103 => 316,  1097 => 339,  1088 => 338,  1081 => 337,  1074 => 336,  1068 => 334,  1065 => 333,  1062 => 332,  1059 => 331,  1056 => 330,  1053 => 329,  1050 => 328,  1047 => 327,  1044 => 326,  1041 => 325,  1030 => 323,  1025 => 322,  1022 => 321,  1016 => 319,  1014 => 318,  1009 => 317,  1006 => 316,  1003 => 315,  1000 => 314,  996 => 312,  990 => 310,  988 => 309,  983 => 308,  980 => 307,  977 => 306,  974 => 305,  972 => 304,  959 => 303,  947 => 286,  944 => 285,  941 => 284,  939 => 283,  936 => 282,  933 => 281,  918 => 280,  907 => 270,  904 => 269,  901 => 268,  898 => 267,  895 => 266,  892 => 265,  890 => 264,  887 => 263,  884 => 262,  870 => 261,  857 => 250,  853 => 249,  848 => 247,  839 => 242,  833 => 240,  831 => 239,  825 => 236,  821 => 235,  816 => 234,  814 => 233,  811 => 232,  808 => 231,  791 => 230,  779 => 218,  777 => 217,  776 => 214,  775 => 213,  774 => 211,  771 => 210,  769 => 209,  766 => 208,  763 => 207,  760 => 206,  757 => 205,  739 => 204,  726 => 192,  723 => 191,  721 => 190,  718 => 189,  715 => 188,  699 => 187,  688 => 176,  685 => 175,  682 => 174,  666 => 173,  653 => 161,  650 => 160,  648 => 159,  645 => 158,  642 => 157,  625 => 156,  614 => 144,  611 => 143,  608 => 142,  591 => 141,  578 => 127,  573 => 125,  565 => 124,  563 => 123,  561 => 121,  546 => 120,  533 => 109,  530 => 108,  524 => 107,  517 => 106,  504 => 105,  497 => 104,  494 => 103,  492 => 102,  490 => 96,  475 => 95,  463 => 85,  460 => 84,  457 => 83,  448 => 80,  445 => 79,  440 => 78,  438 => 77,  435 => 76,  427 => 74,  421 => 72,  419 => 71,  416 => 70,  414 => 69,  411 => 68,  408 => 67,  393 => 66,  380 => 54,  374 => 53,  371 => 52,  368 => 51,  365 => 50,  363 => 47,  361 => 46,  358 => 45,  355 => 44,  352 => 43,  349 => 42,  346 => 41,  343 => 40,  340 => 39,  337 => 38,  335 => 37,  333 => 36,  330 => 34,  328 => 33,  326 => 32,  307 => 31,  296 => 27,  291 => 25,  289 => 24,  284 => 22,  279 => 21,  273 => 19,  270 => 18,  262 => 16,  257 => 15,  252 => 14,  250 => 13,  244 => 10,  241 => 9,  238 => 8,  235 => 7,  232 => 6,  229 => 5,  226 => 4,  223 => 3,  220 => 2,  207 => 1,  202 => 1758,  199 => 1735,  196 => 1672,  193 => 1666,  190 => 1633,  187 => 1623,  184 => 1603,  181 => 1541,  178 => 1533,  175 => 1433,  172 => 1309,  169 => 1288,  166 => 1200,  163 => 1154,  160 => 1124,  157 => 1115,  154 => 1088,  151 => 1047,  148 => 1029,  145 => 1013,  142 => 996,  139 => 978,  136 => 956,  133 => 922,  130 => 896,  127 => 870,  124 => 861,  121 => 843,  118 => 828,  115 => 786,  112 => 762,  109 => 681,  106 => 642,  103 => 595,  100 => 560,  97 => 527,  94 => 480,  91 => 465,  88 => 449,  85 => 415,  82 => 399,  79 => 362,  76 => 343,  73 => 288,  70 => 272,  67 => 254,  64 => 222,  61 => 195,  58 => 178,  55 => 164,  52 => 146,  49 => 131,  46 => 113,  43 => 87,  40 => 58,  37 => 30,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/macros.html.twig");
    }
}


/* @OroUI/macros.html.twig */
class __TwigTemplate_8ecde7b37d4443c44b98965cf8544404___118842245 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1522
        return "@OroUI/jstree.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUI/jstree.html.twig", "@OroUI/macros.html.twig", 1522);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroUI/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  4519 => 1522,  4467 => 1780,  4463 => 1778,  4461 => 1774,  4458 => 1773,  4455 => 1772,  4442 => 1771,  4431 => 1756,  4428 => 1755,  4426 => 1751,  4423 => 1750,  4420 => 1749,  4407 => 1748,  4394 => 1732,  4392 => 1730,  4391 => 1728,  4390 => 1727,  4389 => 1725,  4387 => 1724,  4381 => 1722,  4378 => 1704,  4376 => 1703,  4373 => 1702,  4371 => 1698,  4368 => 1697,  4366 => 1696,  4363 => 1695,  4360 => 1694,  4347 => 1693,  4335 => 1669,  4326 => 1668,  4313 => 1667,  4300 => 1662,  4298 => 1661,  4296 => 1660,  4294 => 1659,  4292 => 1658,  4287 => 1655,  4283 => 1653,  4281 => 1652,  4279 => 1651,  4277 => 1650,  4275 => 1649,  4256 => 1648,  4247 => 1641,  4240 => 1643,  4238 => 1642,  4236 => 1641,  4223 => 1640,  4207 => 1636,  4200 => 1635,  4185 => 1634,  4172 => 1630,  4166 => 1628,  4164 => 1627,  4159 => 1626,  4156 => 1625,  4143 => 1624,  4134 => 1612,  4128 => 1619,  4124 => 1618,  4117 => 1617,  4114 => 1616,  4111 => 1615,  4108 => 1614,  4105 => 1613,  4102 => 1612,  4099 => 1611,  4096 => 1610,  4093 => 1609,  4090 => 1608,  4087 => 1607,  4084 => 1606,  4081 => 1605,  4066 => 1604,  4052 => 1598,  4046 => 1594,  4040 => 1592,  4037 => 1591,  4028 => 1589,  4025 => 1588,  4020 => 1587,  4018 => 1586,  4006 => 1585,  4002 => 1583,  3998 => 1581,  3996 => 1580,  3992 => 1579,  3988 => 1578,  3980 => 1574,  3978 => 1573,  3975 => 1572,  3973 => 1566,  3962 => 1565,  3959 => 1564,  3957 => 1563,  3954 => 1562,  3951 => 1561,  3948 => 1560,  3946 => 1559,  3943 => 1558,  3940 => 1557,  3937 => 1556,  3922 => 1555,  3909 => 1538,  3907 => 1537,  3904 => 1536,  3901 => 1535,  3888 => 1534,  3872 => 1531,  3869 => 1530,  3856 => 1529,  3847 => 1524,  3846 => 1523,  3845 => 1522,  3831 => 1521,  3821 => 1518,  3815 => 1516,  3812 => 1515,  3806 => 1513,  3804 => 1512,  3789 => 1511,  3778 => 1500,  3765 => 1499,  3755 => 1491,  3749 => 1490,  3746 => 1489,  3741 => 1488,  3739 => 1487,  3736 => 1486,  3734 => 1485,  3718 => 1484,  3705 => 1480,  3702 => 1479,  3696 => 1477,  3690 => 1475,  3687 => 1474,  3685 => 1473,  3682 => 1472,  3680 => 1471,  3664 => 1470,  3654 => 1467,  3647 => 1465,  3638 => 1463,  3634 => 1462,  3632 => 1461,  3628 => 1460,  3625 => 1458,  3616 => 1456,  3612 => 1455,  3606 => 1452,  3591 => 1451,  3581 => 1448,  3572 => 1446,  3568 => 1445,  3566 => 1444,  3553 => 1443,  3543 => 1440,  3540 => 1435,  3527 => 1434,  3516 => 1430,  3511 => 1428,  3509 => 1427,  3505 => 1426,  3498 => 1425,  3496 => 1424,  3493 => 1422,  3491 => 1421,  3489 => 1420,  3486 => 1419,  3484 => 1418,  3470 => 1417,  3453 => 1407,  3450 => 1406,  3447 => 1405,  3444 => 1404,  3442 => 1403,  3428 => 1402,  3419 => 1399,  3414 => 1397,  3412 => 1396,  3408 => 1394,  3406 => 1393,  3405 => 1392,  3404 => 1391,  3403 => 1390,  3402 => 1389,  3401 => 1388,  3393 => 1386,  3391 => 1385,  3388 => 1383,  3386 => 1382,  3384 => 1381,  3381 => 1380,  3379 => 1379,  3365 => 1378,  3356 => 1357,  3353 => 1375,  3347 => 1373,  3336 => 1371,  3331 => 1370,  3327 => 1369,  3318 => 1368,  3315 => 1367,  3312 => 1366,  3309 => 1365,  3306 => 1364,  3303 => 1363,  3300 => 1362,  3297 => 1361,  3294 => 1360,  3291 => 1359,  3288 => 1358,  3286 => 1357,  3269 => 1356,  3260 => 1319,  3255 => 1351,  3252 => 1350,  3249 => 1349,  3246 => 1348,  3240 => 1346,  3234 => 1344,  3231 => 1343,  3227 => 1341,  3225 => 1338,  3224 => 1337,  3222 => 1336,  3219 => 1335,  3216 => 1334,  3213 => 1333,  3210 => 1332,  3207 => 1331,  3204 => 1330,  3201 => 1329,  3198 => 1328,  3195 => 1327,  3192 => 1326,  3189 => 1325,  3186 => 1324,  3183 => 1323,  3181 => 1322,  3178 => 1321,  3176 => 1320,  3174 => 1319,  3171 => 1318,  3169 => 1317,  3155 => 1316,  3146 => 1296,  3143 => 1307,  3137 => 1306,  3134 => 1305,  3126 => 1303,  3118 => 1301,  3115 => 1300,  3112 => 1299,  3107 => 1298,  3104 => 1297,  3101 => 1296,  3087 => 1295,  3075 => 1284,  3071 => 1282,  3065 => 1280,  3062 => 1279,  3056 => 1277,  3054 => 1276,  3051 => 1275,  3048 => 1274,  3033 => 1272,  3031 => 1271,  3030 => 1270,  3029 => 1269,  3028 => 1268,  3027 => 1267,  3026 => 1266,  3025 => 1265,  3024 => 1264,  3022 => 1263,  3005 => 1262,  2995 => 1260,  2988 => 1256,  2982 => 1253,  2978 => 1251,  2976 => 1250,  2973 => 1249,  2968 => 1246,  2952 => 1244,  2948 => 1243,  2940 => 1242,  2923 => 1241,  2917 => 1239,  2915 => 1238,  2912 => 1237,  2910 => 1236,  2907 => 1235,  2904 => 1234,  2901 => 1233,  2898 => 1232,  2895 => 1231,  2893 => 1230,  2890 => 1229,  2888 => 1228,  2885 => 1227,  2882 => 1226,  2866 => 1225,  2854 => 1196,  2850 => 1194,  2846 => 1192,  2843 => 1191,  2834 => 1189,  2829 => 1188,  2825 => 1186,  2821 => 1184,  2819 => 1183,  2807 => 1182,  2802 => 1180,  2795 => 1179,  2788 => 1178,  2786 => 1177,  2783 => 1176,  2780 => 1175,  2760 => 1174,  2750 => 1149,  2741 => 1147,  2736 => 1146,  2730 => 1145,  2725 => 1144,  2723 => 1136,  2720 => 1135,  2703 => 1134,  2691 => 1120,  2679 => 1119,  2666 => 1111,  2661 => 1110,  2650 => 1108,  2645 => 1107,  2642 => 1106,  2636 => 1105,  2625 => 1104,  2622 => 1103,  2619 => 1102,  2616 => 1099,  2603 => 1098,  2592 => 1086,  2589 => 1085,  2587 => 1084,  2584 => 1083,  2581 => 1082,  2578 => 1081,  2575 => 1080,  2572 => 1079,  2569 => 1078,  2566 => 1077,  2564 => 1076,  2562 => 1075,  2559 => 1074,  2557 => 1069,  2554 => 1068,  2551 => 1067,  2538 => 1066,  2527 => 1045,  2524 => 1044,  2522 => 1043,  2519 => 1042,  2517 => 1037,  2514 => 1036,  2511 => 1035,  2498 => 1034,  2487 => 1027,  2484 => 1026,  2482 => 1025,  2479 => 1024,  2477 => 1021,  2474 => 1020,  2471 => 1019,  2458 => 1018,  2447 => 1011,  2444 => 1010,  2442 => 1009,  2439 => 1008,  2437 => 1004,  2434 => 1003,  2431 => 1002,  2418 => 1001,  2407 => 994,  2404 => 993,  2402 => 992,  2399 => 991,  2397 => 986,  2394 => 985,  2391 => 984,  2378 => 983,  2367 => 976,  2364 => 975,  2361 => 974,  2348 => 973,  2337 => 954,  2334 => 953,  2332 => 950,  2331 => 949,  2330 => 944,  2327 => 943,  2324 => 942,  2311 => 941,  2299 => 919,  2295 => 917,  2292 => 916,  2279 => 915,  2267 => 892,  2264 => 891,  2261 => 889,  2258 => 888,  2245 => 887,  2223 => 867,  2220 => 866,  2217 => 865,  2214 => 864,  2209 => 863,  2195 => 862,  2183 => 859,  2180 => 858,  2177 => 857,  2174 => 856,  2172 => 854,  2171 => 852,  2168 => 851,  2165 => 850,  2152 => 849,  2139 => 841,  2131 => 839,  2123 => 837,  2121 => 836,  2116 => 835,  2113 => 834,  2108 => 833,  2095 => 832,  2086 => 811,  2082 => 825,  2080 => 824,  2078 => 823,  2076 => 822,  2071 => 820,  2069 => 819,  2064 => 818,  2062 => 817,  2056 => 815,  2054 => 814,  2049 => 813,  2046 => 812,  2043 => 811,  2040 => 810,  2038 => 808,  2036 => 807,  2033 => 806,  2020 => 805,  2011 => 775,  2007 => 782,  2002 => 780,  2000 => 779,  1996 => 778,  1994 => 777,  1992 => 776,  1989 => 775,  1986 => 774,  1973 => 773,  1963 => 759,  1961 => 758,  1958 => 756,  1956 => 755,  1951 => 753,  1948 => 750,  1947 => 749,  1945 => 748,  1942 => 747,  1939 => 746,  1935 => 744,  1932 => 743,  1926 => 741,  1923 => 740,  1917 => 738,  1914 => 737,  1908 => 735,  1905 => 734,  1899 => 732,  1896 => 731,  1890 => 729,  1887 => 728,  1881 => 726,  1878 => 725,  1872 => 723,  1869 => 722,  1863 => 720,  1860 => 719,  1854 => 717,  1851 => 716,  1845 => 714,  1842 => 713,  1836 => 711,  1833 => 710,  1827 => 708,  1824 => 707,  1818 => 705,  1815 => 704,  1812 => 703,  1799 => 702,  1790 => 668,  1786 => 678,  1784 => 677,  1782 => 676,  1779 => 675,  1774 => 673,  1772 => 672,  1767 => 671,  1765 => 670,  1763 => 669,  1760 => 668,  1758 => 666,  1756 => 665,  1753 => 664,  1740 => 663,  1728 => 640,  1725 => 639,  1719 => 638,  1716 => 637,  1713 => 636,  1708 => 635,  1705 => 634,  1702 => 633,  1699 => 632,  1696 => 631,  1693 => 630,  1690 => 629,  1687 => 628,  1684 => 627,  1682 => 626,  1679 => 625,  1677 => 622,  1676 => 621,  1675 => 620,  1674 => 619,  1673 => 617,  1672 => 616,  1671 => 615,  1669 => 613,  1666 => 612,  1663 => 611,  1661 => 610,  1658 => 609,  1655 => 608,  1653 => 605,  1651 => 604,  1648 => 603,  1645 => 602,  1642 => 601,  1639 => 600,  1637 => 599,  1634 => 598,  1631 => 597,  1618 => 596,  1606 => 592,  1602 => 590,  1600 => 589,  1597 => 588,  1594 => 587,  1591 => 586,  1589 => 585,  1586 => 584,  1583 => 583,  1580 => 582,  1578 => 581,  1575 => 580,  1573 => 579,  1570 => 578,  1567 => 577,  1554 => 576,  1542 => 558,  1540 => 557,  1537 => 556,  1534 => 555,  1532 => 552,  1530 => 551,  1527 => 550,  1524 => 549,  1521 => 548,  1518 => 547,  1516 => 544,  1514 => 543,  1511 => 542,  1508 => 541,  1505 => 540,  1492 => 539,  1481 => 525,  1478 => 524,  1475 => 523,  1472 => 521,  1470 => 519,  1469 => 518,  1468 => 517,  1467 => 516,  1465 => 515,  1463 => 512,  1461 => 511,  1458 => 510,  1456 => 507,  1454 => 506,  1451 => 505,  1448 => 504,  1445 => 503,  1442 => 502,  1439 => 501,  1436 => 500,  1433 => 499,  1430 => 498,  1427 => 497,  1424 => 496,  1422 => 495,  1419 => 494,  1416 => 493,  1403 => 492,  1391 => 478,  1388 => 477,  1385 => 476,  1383 => 475,  1380 => 474,  1377 => 473,  1363 => 472,  1351 => 463,  1349 => 462,  1347 => 455,  1344 => 454,  1331 => 453,  1318 => 446,  1315 => 445,  1313 => 436,  1307 => 442,  1304 => 441,  1302 => 440,  1301 => 438,  1298 => 437,  1295 => 436,  1293 => 433,  1292 => 432,  1291 => 431,  1289 => 430,  1286 => 429,  1284 => 428,  1281 => 427,  1278 => 426,  1265 => 425,  1253 => 413,  1250 => 412,  1237 => 411,  1226 => 396,  1220 => 394,  1217 => 393,  1214 => 392,  1205 => 390,  1200 => 389,  1198 => 388,  1192 => 387,  1186 => 385,  1180 => 383,  1178 => 382,  1172 => 379,  1168 => 378,  1165 => 377,  1163 => 376,  1160 => 375,  1157 => 374,  1144 => 373,  1132 => 359,  1128 => 357,  1125 => 356,  1112 => 355,  1103 => 316,  1097 => 339,  1088 => 338,  1081 => 337,  1074 => 336,  1068 => 334,  1065 => 333,  1062 => 332,  1059 => 331,  1056 => 330,  1053 => 329,  1050 => 328,  1047 => 327,  1044 => 326,  1041 => 325,  1030 => 323,  1025 => 322,  1022 => 321,  1016 => 319,  1014 => 318,  1009 => 317,  1006 => 316,  1003 => 315,  1000 => 314,  996 => 312,  990 => 310,  988 => 309,  983 => 308,  980 => 307,  977 => 306,  974 => 305,  972 => 304,  959 => 303,  947 => 286,  944 => 285,  941 => 284,  939 => 283,  936 => 282,  933 => 281,  918 => 280,  907 => 270,  904 => 269,  901 => 268,  898 => 267,  895 => 266,  892 => 265,  890 => 264,  887 => 263,  884 => 262,  870 => 261,  857 => 250,  853 => 249,  848 => 247,  839 => 242,  833 => 240,  831 => 239,  825 => 236,  821 => 235,  816 => 234,  814 => 233,  811 => 232,  808 => 231,  791 => 230,  779 => 218,  777 => 217,  776 => 214,  775 => 213,  774 => 211,  771 => 210,  769 => 209,  766 => 208,  763 => 207,  760 => 206,  757 => 205,  739 => 204,  726 => 192,  723 => 191,  721 => 190,  718 => 189,  715 => 188,  699 => 187,  688 => 176,  685 => 175,  682 => 174,  666 => 173,  653 => 161,  650 => 160,  648 => 159,  645 => 158,  642 => 157,  625 => 156,  614 => 144,  611 => 143,  608 => 142,  591 => 141,  578 => 127,  573 => 125,  565 => 124,  563 => 123,  561 => 121,  546 => 120,  533 => 109,  530 => 108,  524 => 107,  517 => 106,  504 => 105,  497 => 104,  494 => 103,  492 => 102,  490 => 96,  475 => 95,  463 => 85,  460 => 84,  457 => 83,  448 => 80,  445 => 79,  440 => 78,  438 => 77,  435 => 76,  427 => 74,  421 => 72,  419 => 71,  416 => 70,  414 => 69,  411 => 68,  408 => 67,  393 => 66,  380 => 54,  374 => 53,  371 => 52,  368 => 51,  365 => 50,  363 => 47,  361 => 46,  358 => 45,  355 => 44,  352 => 43,  349 => 42,  346 => 41,  343 => 40,  340 => 39,  337 => 38,  335 => 37,  333 => 36,  330 => 34,  328 => 33,  326 => 32,  307 => 31,  296 => 27,  291 => 25,  289 => 24,  284 => 22,  279 => 21,  273 => 19,  270 => 18,  262 => 16,  257 => 15,  252 => 14,  250 => 13,  244 => 10,  241 => 9,  238 => 8,  235 => 7,  232 => 6,  229 => 5,  226 => 4,  223 => 3,  220 => 2,  207 => 1,  202 => 1758,  199 => 1735,  196 => 1672,  193 => 1666,  190 => 1633,  187 => 1623,  184 => 1603,  181 => 1541,  178 => 1533,  175 => 1433,  172 => 1309,  169 => 1288,  166 => 1200,  163 => 1154,  160 => 1124,  157 => 1115,  154 => 1088,  151 => 1047,  148 => 1029,  145 => 1013,  142 => 996,  139 => 978,  136 => 956,  133 => 922,  130 => 896,  127 => 870,  124 => 861,  121 => 843,  118 => 828,  115 => 786,  112 => 762,  109 => 681,  106 => 642,  103 => 595,  100 => 560,  97 => 527,  94 => 480,  91 => 465,  88 => 449,  85 => 415,  82 => 399,  79 => 362,  76 => 343,  73 => 288,  70 => 272,  67 => 254,  64 => 222,  61 => 195,  58 => 178,  55 => 164,  52 => 146,  49 => 131,  46 => 113,  43 => 87,  40 => 58,  37 => 30,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/macros.html.twig");
    }
}
