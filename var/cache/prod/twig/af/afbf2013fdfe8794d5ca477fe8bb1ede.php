<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/js_modules_config.html.twig */
class __TwigTemplate_3f126ddbb9e133d640a722a66ce24c23 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroEmail/js_modules_config.html.twig", 1)->unwrap();
        // line 2
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["oroemail/js/app/components/new-email-message-component" => ["wsChannel" => $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailWSChannel()], "oroemail/js/app/components/sidebar-recent-emails-component" => ["wsChannel" => $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailWSChannel()], "oroemail/js/app/unread-emails-state-holder" => ["wsChannel" => $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailWSChannel()], "oroemail/js/util/unread-email-count-cache" => ["unreadEmailsCount" => $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getUnreadEmailsCount()]]], 2, $context, $this->getSourceContext());
        // line 15
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 15,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/js_modules_config.html.twig");
    }
}
