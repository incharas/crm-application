<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/dropdown-mask.js */
class __TwigTemplate_d81f3c66d9887acc8006613f8dc19f29 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(['jquery'], function(\$) {
    'use strict';

    let \$mask;
    let onHide;

    function createMask() {
        \$mask = \$('<div></div>');
        \$mask.attr('id', 'oro-dropdown-mask').attr('class', 'oro-dropdown-mask');
        \$mask.hide();
        \$mask.appendTo('body');
        \$mask.on('mousedown touchstart click', function(e) {
            e.preventDefault();
            e.stopPropagation();
            \$mask.hide();
            if (onHide) {
                onHide();
            }
        });
    }

    return {
        show: function(zIndex) {
            if (!\$mask) {
                createMask();
            }
            \$mask.css('zIndex', zIndex === void 0 ? '' : zIndex).show();
            return {
                onhide: function(callback) {
                    onHide = callback;
                }
            };
        },

        hide: function() {
            if (\$mask) {
                \$mask.hide();
                onHide = null;
            }
        }
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/dropdown-mask.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/dropdown-mask.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/dropdown-mask.js");
    }
}
