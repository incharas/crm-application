<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Security/login_frontend.html.twig */
class __TwigTemplate_489c6192bfadd51043a494043b8943eb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'bodyClass' => [$this, 'block_bodyClass'],
            'messages' => [$this, 'block_messages'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUser/layout.html.twig", "@OroOAuth2Server/Security/login_frontend.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        // line 5
        echo "    <style type=\"text/css\">
        #login-page-loader {
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 800;
            background: white url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzgiIGhlaWdodD0iMzgiIHZpZXdCb3g9IjAgMCAzOCAzOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCB4MT0iOC4wNDIlIiB5MT0iMCUiIHgyPSI2NS42ODIlIiB5Mj0iMjMuODY1JSIgaWQ9ImEiPgogICAgICAgICAgICA8c3RvcCBzdG9wLWNvbG9yPSIjODg4IiBzdG9wLW9wYWNpdHk9IjAiIG9mZnNldD0iMCUiLz4KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzg4OCIgc3RvcC1vcGFjaXR5PSIuNjMxIiBvZmZzZXQ9IjYzLjE0NiUiLz4KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzg4OCIgb2Zmc2V0PSIxMDAlIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSI+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0zNiAxOGMwLTkuOTQtOC4wNi0xOC0xOC0xOCIgaWQ9Ik92YWwtMiIgc3Ryb2tlPSJ1cmwoI2EpIiBzdHJva2Utd2lkdGg9IjIiPgogICAgICAgICAgICAgICAgPGFuaW1hdGVUcmFuc2Zvcm0KICAgICAgICAgICAgICAgICAgICBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iCiAgICAgICAgICAgICAgICAgICAgdHlwZT0icm90YXRlIgogICAgICAgICAgICAgICAgICAgIGZyb209IjAgMTggMTgiCiAgICAgICAgICAgICAgICAgICAgdG89IjM2MCAxOCAxOCIKICAgICAgICAgICAgICAgICAgICBkdXI9IjAuOXMiCiAgICAgICAgICAgICAgICAgICAgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIC8+CiAgICAgICAgICAgIDwvcGF0aD4KICAgICAgICAgICAgPGNpcmNsZSBmaWxsPSIjZmZmIiBjeD0iMzYiIGN5PSIxOCIgcj0iMSI+CiAgICAgICAgICAgICAgICA8YW5pbWF0ZVRyYW5zZm9ybQogICAgICAgICAgICAgICAgICAgIGF0dHJpYnV0ZU5hbWU9InRyYW5zZm9ybSIKICAgICAgICAgICAgICAgICAgICB0eXBlPSJyb3RhdGUiCiAgICAgICAgICAgICAgICAgICAgZnJvbT0iMCAxOCAxOCIKICAgICAgICAgICAgICAgICAgICB0bz0iMzYwIDE4IDE4IgogICAgICAgICAgICAgICAgICAgIGR1cj0iMC45cyIKICAgICAgICAgICAgICAgICAgICByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgLz4KICAgICAgICAgICAgPC9jaXJjbGU+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K') center center no-repeat;
        }
        .form-wrapper {
            margin:auto;
            background: none;
            justify-content: center;
        }
        .form_container {
            background: hsla(0,0%,100%,.8);
            display: flex;
            flex-grow: .2;
            flex-direction: column;
        }
        .login-copyright{
            padding: 16px;
        }
    </style>
";
    }

    // line 31
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "login-page";
    }

    // line 32
    public function block_messages($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 34
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "<div id=\"login-page-loader\"></div>
<div class=\"container\">
    <div class=\"form-wrapper\">
        <div class=\"form_container\">
            <div class=\"form-wrapper__inner\">
                ";
        // line 40
        $context["usernameLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Username");
        // line 41
        echo "                ";
        $context["passwordLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Password");
        // line 42
        echo "                ";
        $context["showLabels"] = ((twig_length_filter($this->env, ($context["usernameLabel"] ?? null)) <= 9) && (twig_length_filter($this->env, ($context["passwordLabel"] ?? null)) <= 9));
        // line 43
        echo "                ";
        $context["layoutName"] = ((($context["showLabels"] ?? null)) ? ("form-row-layout") : ("form-column-layout"));
        // line 44
        echo "                <form id=\"login-form\" action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_oauth2_server_frontend_login_check");
        echo "\" method=\"post\" class=\"form-signin form-signin--login ";
        echo twig_escape_filter($this->env, ($context["layoutName"] ?? null), "html", null, true);
        echo "\">
                    <div class=\"form-description\">
                        <div class=\"form-description__logo\">
                            ";
        // line 47
        $context["src"] = $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/orofrontend/default/images/logo/demob2b-logo.svg");
        // line 48
        echo "                            <img src=\"";
        echo twig_escape_filter($this->env, ($context["src"] ?? null), "html", null, true);
        echo "\"
                                 alt=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.auth.description.logo"), "html", null, true);
        echo "\"
                                 class=\"form-description__logo-img\">
                        </div>
                        <div class=\"form-description__main\">";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.auth_code.login_message", ["%app_name%" => ($context["appName"] ?? null)]), "html", null, true);
        echo "</div>
                    </div>
                    <div class=\"title-box\">
                        <h2 class=\"title\">";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Login"), "html", null, true);
        echo "</h2>
                    </div>
                    <fieldset class=\"field-set\">
                        ";
        // line 58
        if (($context["error"] ?? null)) {
            // line 59
            echo "                            <div class=\"alert alert-error\" role=\"alert\">
                                <div>";
            // line 60
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", [], "any", false, false, false, 60), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", [], "any", false, false, false, 60), "security"), "html", null, true);
            echo "</div>
                            </div>
                        ";
        }
        // line 63
        echo "                        ";
        echo twig_escape_filter($this->env, ($context["messagesContent"] ?? null), "html", null, true);
        echo "
                        <div class=\"input-field-group\">
                            <div class=\"input-prepend\">
                                <label for=\"prependedInput\" class=\"add-on\">";
        // line 66
        echo twig_escape_filter($this->env, ($context["usernameLabel"] ?? null), "html", null, true);
        echo "</label>
                                <input type=\"text\" id=\"prependedInput\" class=\"input\" autocomplete=\"username\" name=\"_username\" value=\"";
        // line 67
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" required=\"required\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Username or Email"), "html", null, true);
        echo "\" autofocus>
                            </div>
                            <div class=\"input-prepend input-prepend--last\">
                                <label for=\"prependedInput2\" class=\"add-on\">";
        // line 70
        echo twig_escape_filter($this->env, ($context["passwordLabel"] ?? null), "html", null, true);
        echo "</label>
                                <input type=\"password\" id=\"prependedInput2\" class=\"input\" autocomplete=\"off\" name=\"_password\" required=\"required\" placeholder=\"";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Password"), "html", null, true);
        echo "\">
                            </div>
                        </div>
                        <div class=\"form-signin__footer\">
                            <button type=\"submit\" class=\"btn extra-submit btn-uppercase btn-primary\" id=\"_submit\" name=\"_submit\">";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Log in"), "html", null, true);
        echo "</button>
                        </div>
                    </fieldset>
                    <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 78
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">
                </form>
            </div>
            <div class=\"login-copyright\">";
        // line 81
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.copyright", ["{{year}}" => twig_date_format_filter($this->env, "now", "Y")]), "html", null, true);
        echo "</div>
        </div>
    </div>
    <script type=\"text/javascript\">
        document.getElementById('login-form').addEventListener('submit', function (event) {
            var buttons = event.target.getElementsByTagName('button');
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].setAttribute('disabled', 'disabled');
            }
        });
        window.addEventListener('load', function() {
            var loader = document.getElementById('login-page-loader');
            if (loader) {
                loader.parentNode.removeChild(loader);
            }
        });
    </script>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Security/login_frontend.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 81,  203 => 78,  197 => 75,  190 => 71,  186 => 70,  178 => 67,  174 => 66,  167 => 63,  161 => 60,  158 => 59,  156 => 58,  150 => 55,  144 => 52,  138 => 49,  133 => 48,  131 => 47,  122 => 44,  119 => 43,  116 => 42,  113 => 41,  111 => 40,  104 => 35,  100 => 34,  94 => 32,  87 => 31,  58 => 5,  53 => 3,  49 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Security/login_frontend.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Security/login_frontend.html.twig");
    }
}
