<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/B2bCustomer/accountCustomersInfo.html.twig */
class __TwigTemplate_d977aea178a68b8dfbcfc8962fc92da4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_sales_b2bcustomer_view") && (twig_length_filter($this->env, ($context["customers"] ?? null)) > 0))) {
            // line 2
            echo "    ";
            $context["tabs"] = [];
            // line 3
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["customers"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["customer"]) {
                // line 4
                echo "        ";
                $context["tabs"] = twig_array_merge(($context["tabs"] ?? null), [0 => ["alias" => ((("oro_sales_b2bcustomer_info_customer_" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 6
$context["customer"], "id", [], "any", false, false, false, 6)) . "_channel_") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["channel"] ?? null), "id", [], "any", false, false, false, 6)), "widgetType" => "customer-info", "label" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(                // line 8
$context["customer"]), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_widget_b2bcustomer_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 10
$context["customer"], "id", [], "any", false, false, false, 10), "channelId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["channel"] ?? null), "id", [], "any", false, false, false, 10)])]]);
                // line 14
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['customer'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 15
            echo "    ";
            if ((twig_length_filter($this->env, ($context["tabs"] ?? null)) > 1)) {
                // line 16
                echo "        ";
                if ( !$this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                    // line 17
                    echo "            ";
                    $context["tabsOptions"] = ["verticalTabs" => true, "useDropdown" => false, "subtitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.customer.entity_plural_label")];
                    // line 22
                    echo "        ";
                }
                // line 23
                echo "        ";
                $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/B2bCustomer/accountCustomersInfo.html.twig", 23)->unwrap();
                // line 24
                echo "        <div class=\"widget-content account-customer-info multicustomer\" ";
                echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["view" => "orosales/js/app/views/account-multicustomer-view"]]], 24, $context, $this->getSourceContext());
                // line 29
                echo ">
            ";
                // line 30
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\TabExtension']->tabPanel($this->env, ($context["tabs"] ?? null), ((array_key_exists("tabsOptions", $context)) ? (_twig_default_filter(($context["tabsOptions"] ?? null), [])) : ([])));
                echo "
        </div>
    ";
            } else {
                // line 33
                echo "        <div class=\"widget-content account-customer-info\">
            ";
                // line 34
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, (($__internal_compile_0 = ($context["tabs"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null));
                echo "
        </div>
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "@OroSales/B2bCustomer/accountCustomersInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  89 => 34,  86 => 33,  80 => 30,  77 => 29,  74 => 24,  71 => 23,  68 => 22,  65 => 17,  62 => 16,  59 => 15,  53 => 14,  51 => 10,  50 => 8,  49 => 6,  47 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/B2bCustomer/accountCustomersInfo.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/B2bCustomer/accountCustomersInfo.html.twig");
    }
}
