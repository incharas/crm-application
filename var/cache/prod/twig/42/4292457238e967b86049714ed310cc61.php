<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCurrency/macros.html.twig */
class __TwigTemplate_44f29c9c8d7f20e32857b7848ff1ade3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "
";
    }

    // line 9
    public function macro_convert_to_base_currency($__multicurrency__ = null, $__title__ = null, $__entity__ = null, $__fieldName__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "multicurrency" => $__multicurrency__,
            "title" => $__title__,
            "entity" => $__entity__,
            "fieldName" => $__fieldName__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 10
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCurrency/macros.html.twig", 10)->unwrap();
            // line 11
            echo "    ";
            $context["value"] = $this->extensions['Oro\Bundle\CurrencyBundle\Twig\RateConverterExtension']->convert(($context["multicurrency"] ?? null));
            // line 12
            echo "    ";
            if (twig_length_filter($this->env, ($context["value"] ?? null))) {
                // line 13
                echo "        <div class=\"base-currency-wrapper base-currency-wrapper--convert\">
            ";
                // line 14
                echo twig_call_macro($macros["ui"], "macro_renderProperty", [((($context["title"] ?? null)) ? (($context["title"] ?? null)) : ("")), ($context["value"] ?? null), ($context["entity"] ?? null), ($context["fieldName"] ?? null)], 14, $context, $this->getSourceContext());
                echo "
        </div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroCurrency/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 14,  67 => 13,  64 => 12,  61 => 11,  58 => 10,  42 => 9,  37 => 18,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCurrency/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CurrencyBundle/Resources/views/macros.html.twig");
    }
}
