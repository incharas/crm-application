<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataAudit/Datagrid/Property/data.html.twig */
class __TwigTemplate_85447f66da7c7d8e9c0c131ab4e7c030 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataAudit"] = $this->macros["dataAudit"] = $this->loadTemplate("@OroDataAudit/macros.html.twig", "@OroDataAudit/Datagrid/Property/data.html.twig", 1)->unwrap();
        // line 3
        echo "<ul>";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["value"] ?? null));
        foreach ($context['_seq'] as $context["fieldKey"] => $context["fieldValue"]) {
            // line 5
            if (($context["fieldKey"] == "auditData")) {
                // line 6
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "new", [], "any", false, false, false, 6));
                foreach ($context['_seq'] as $context["collKey"] => $context["collValue"]) {
                    // line 7
                    echo "<li>";
                    // line 8
                    if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "old", [], "any", false, false, false, 8))) {
                        // line 9
                        echo "<b>";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(_twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 9), $context["collKey"], "label"), $context["collKey"])), "html", null, true);
                        echo "</b>&nbsp;
                        <s>";
                        // line 10
                        echo twig_escape_filter($this->env, (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "old", [], "any", false, false, false, 10)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[$context["collKey"]] ?? null) : null), "html", null, true);
                        echo "</s>&nbsp;";
                        // line 11
                        echo twig_escape_filter($this->env, $context["collValue"], "html", null, true);
                    } else {
                        // line 13
                        echo "<b>";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(_twig_default_filter($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getFieldConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 13), $context["collKey"], "label"), $context["collKey"])), "html", null, true);
                        echo "</b>&nbsp;";
                        // line 14
                        echo twig_escape_filter($this->env, $context["collValue"], "html", null, true);
                    }
                    // line 16
                    echo "</li>";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['collKey'], $context['collValue'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
$context["fieldValue"], "collectionDiffs", [], "any", true, true, false, 18) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "collectionDiffs", [], "any", false, false, false, 18)))) {
                // line 19
                echo "<li>
                <b>";
                // line 20
                echo twig_call_macro($macros["dataAudit"], "macro_renderFieldName", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 20), $context["fieldKey"], $context["fieldValue"]], 20, $context, $this->getSourceContext());
                echo "</b>&nbsp;";
                // line 21
                echo twig_call_macro($macros["dataAudit"], "macro_renderCollection", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 21), $context["fieldKey"], $context["fieldValue"], [0 => "added", 1 => "changed", 2 => "removed"]], 21, $context, $this->getSourceContext());
                // line 22
                echo "</li>";
            } else {
                // line 24
                echo "<li>
                <b>";
                // line 25
                echo twig_call_macro($macros["dataAudit"], "macro_renderFieldName", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "objectClass"], "method", false, false, false, 25), $context["fieldKey"], $context["fieldValue"]], 25, $context, $this->getSourceContext());
                echo "</b>&nbsp;
                <s>";
                // line 26
                echo twig_call_macro($macros["dataAudit"], "macro_renderFieldValue", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "old", [], "any", false, false, false, 26), $context["fieldValue"]], 26, $context, $this->getSourceContext());
                echo "</s>&nbsp;";
                // line 27
                echo twig_call_macro($macros["dataAudit"], "macro_renderFieldValue", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["fieldValue"], "new", [], "any", false, false, false, 27), $context["fieldValue"]], 27, $context, $this->getSourceContext());
                // line 28
                echo "</li>";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['fieldKey'], $context['fieldValue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "</ul>
";
    }

    public function getTemplateName()
    {
        return "@OroDataAudit/Datagrid/Property/data.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 31,  104 => 28,  102 => 27,  99 => 26,  95 => 25,  92 => 24,  89 => 22,  87 => 21,  84 => 20,  81 => 19,  79 => 18,  73 => 16,  70 => 14,  66 => 13,  63 => 11,  60 => 10,  55 => 9,  53 => 8,  51 => 7,  47 => 6,  45 => 5,  41 => 4,  39 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataAudit/Datagrid/Property/data.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataAuditBundle/Resources/views/Datagrid/Property/data.html.twig");
    }
}
