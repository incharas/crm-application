<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/plugins/styled-scroll-bar.js */
class __TwigTemplate_0d003543e25131f49fe089fe87073cf5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require, exports, module) {
    'use strict';

    const NAME = 'styledScrollBar';
    const DATA_KEY = 'oro.' + NAME;

    const \$ = require('jquery');
    const _ = require('underscore');
    const BaseClass = require('oroui/js/base-class');
    const error = require('oroui/js/error');
    const OverlayScrollBars = require('overlayScrollbars');
    const config = require('module-config').default(module.id);

    const allowedConfig = _.omit(config, 'callbacks');

    const ScrollBar = BaseClass.extend({
        scrollBar: null,

        /**
         * @inheritdoc
         */
        cidPrefix: 'scrollBar',

        /**
         * @inheritdoc
         */
        listen: function() {
            const listenTo = {};

            listenTo['layout:reposition mediator'] = _.debounce(this.update.bind(this),
                this.options('autoUpdateInterval'));
            return listenTo;
        },

        /**
         * @inheritdoc
         */
        constructor: function ScrollBar(options, element) {
            this.element = element;
            ScrollBar.__super__.constructor.call(this, options, element);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this.defaults = \$.extend(true, {}, \$.fn[NAME].defaults, allowedConfig, options || {});
            this.scrollBar = new OverlayScrollBars(this.element, this.defaults);
        },

        /**
         * Destroy plugin
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }

            if (this.scrollBar) {
                this.scrollBar.destroy();
                delete this.scrollBar;
            }

            \$.removeData(this.element, DATA_KEY);

            delete this.defaults;
            ScrollBar.__super__.dispose.call(this);
        },

        /**
         * Proxy for OverlayScrollBars.options method
         * @returns {Object|undefined}
         */
        options: function(...args) {
            return this.scrollBar.options(...args);
        },

        /**
         * Proxy for OverlayScrollBars.update method
         */
        update: function(...args) {
            if (this.disposed) {
                return;
            }
            this.scrollBar.update(...args);
        },

        /**
         * Proxy for OverlayScrollBars.sleep method
         */
        sleep: function() {
            this.scrollBar.sleep();
        },

        /**
         * Proxy for OverlayScrollBars.scroll method
         * @returns {Object|undefined}
         */
        scroll: function(...args) {
            return this.scrollBar.scroll(...args);
        },

        /**
         * Proxy for OverlayScrollBars.scrollStop method
         * @returns {Object}
         */
        scrollStop: function() {
            return this.scrollBar.scrollStop();
        },

        /**
         * Proxy for OverlayScrollBars.getElements method
         * @returns {Object}
         */
        getElements: function() {
            return this.scrollBar.getElements();
        },

        /**
         * Proxy for OverlayScrollBars.getState method
         * @returns {Object}
         */
        getState: function() {
            return this.scrollBar.getState();
        },

        /**
         * @returns {Object}
         */
        getDefaults: function() {
            return \$.extend(true, {}, \$.fn[NAME].defaults);
        }
    });

    \$.fn[NAME] = function(options, ...args) {
        const isMethodCall = typeof options === 'string';
        let response = this;

        this.each(function(index) {
            const \$element = \$(this);
            const instance = \$element.data(DATA_KEY);

            if (!instance) {
                \$element.data(DATA_KEY, new ScrollBar(options, this));
                return;
            }

            if (isMethodCall) {
                if (options === 'instance') {
                    response = instance;
                    return response;
                }

                if (!_.isFunction(instance[options]) || options.charAt(0) === '_') {
                    error.showErrorInConsole(new Error('Instance ' + NAME + ' doesn\\'t support method ' + options ));
                    return false;
                }

                const result = instance[options](...args);

                if (result !== void 0 && index === 0) {
                    response = result;
                }
            }
        });

        return response;
    };

    \$.fn[NAME].constructor = ScrollBar;
    \$.fn[NAME].defaults = {
        className: 'os-theme-dark',
        autoUpdateInterval: 50
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/plugins/styled-scroll-bar.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/plugins/styled-scroll-bar.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/plugins/styled-scroll-bar.js");
    }
}
