<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Attribute/attributeCollapsibleView.html.twig */
class __TwigTemplate_d54cff214b0a509e4d8ec5cf4c108f1d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroEntityConfig/Attribute/attributeCollapsibleView.html.twig", 1)->unwrap();
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Attribute/attributeCollapsibleView.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        $context["dynamicField"] = $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\DynamicFieldsExtensionAttributeDecorator']->getField(($context["entity"] ?? null), ($context["field"] ?? null));
        // line 5
        if (($context["dynamicField"] ?? null)) {
            // line 6
            echo "    ";
            echo twig_call_macro($macros["entityConfig"], "macro_formatDynamicFieldValue", [            // line 7
($context["entity"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 8
($context["field"] ?? null), "entity", [], "any", false, false, false, 8), "className", [], "any", false, false, false, 8), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["field"] ?? null), "fieldName", [], "any", false, false, false, 9),             // line 10
($context["dynamicField"] ?? null)], 6, $context, $this->getSourceContext());
            // line 11
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Attribute/attributeCollapsibleView.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 11,  53 => 10,  52 => 9,  51 => 8,  50 => 7,  48 => 6,  46 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Attribute/attributeCollapsibleView.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Attribute/attributeCollapsibleView.html.twig");
    }
}
