<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/layouts/default/block_types.html.twig */
class __TwigTemplate_1eb9d7c86da1f5795a460d74c2f5ed0b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'layout_subtree_update_widget' => [$this, 'block_layout_subtree_update_widget'],
            'page_subtitle_widget' => [$this, 'block_page_subtitle_widget'],
            'breadcrumbs_widget' => [$this, 'block_breadcrumbs_widget'],
            'logo_widget' => [$this, 'block_logo_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('layout_subtree_update_widget', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('page_subtitle_widget', $context, $blocks);
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('breadcrumbs_widget', $context, $blocks);
        // line 78
        echo "
";
        // line 79
        $this->displayBlock('logo_widget', $context, $blocks);
    }

    // line 1
    public function block_layout_subtree_update_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["content"] = $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        // line 3
        echo "    ";
        if (( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "children", [], "any", false, false, false, 3)) && (twig_length_filter($this->env, twig_trim_filter(($context["content"] ?? null))) > 0))) {
            // line 4
            echo "    ";
            $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["data-page-component-module" => "oroui/js/app/components/view-component", "~data-page-component-options" => ["view" => "oroui/js/app/views/layout-subtree-view", "blockId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 8
($context["block"] ?? null), "vars", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8), "reloadEvents" =>             // line 9
($context["reloadEvents"] ?? null), "restoreFormState" =>             // line 10
($context["restoreFormState"] ?? null), "showLoading" =>             // line 11
($context["showLoading"] ?? null), "onLoadingCssClass" =>             // line 12
($context["onLoadingCssClass"] ?? null), "disableControls" =>             // line 13
($context["disableControls"] ?? null)]]);
            // line 16
            echo "    <div ";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo ">
        ";
            // line 17
            echo ($context["content"] ?? null);
            echo "
    </div>
    ";
        }
    }

    // line 22
    public function block_page_subtitle_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-subtitle"]);
        // line 24
        echo "    <h2 ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 25
        if (($context["icon"] ?? null)) {
            // line 26
            echo "            <span class=\"badge ";
            ((($context["badge"] ?? null)) ? (print (twig_escape_filter($this->env, ("badge--" . ($context["badge"] ?? null)), "html", null, true))) : (print ("")));
            echo "\" aria-hidden=\"true\">
                <span class=\"fa-";
            // line 27
            echo twig_escape_filter($this->env, ($context["icon"] ?? null), "html", null, true);
            echo "\"></span>
            </span>
        ";
        }
        // line 30
        echo "        <span class=\"page-subtitle__text\">";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->processText(($context["text"] ?? null), ($context["translation_domain"] ?? null)), "html", null, true);
        echo "</span>
        ";
        // line 31
        $this->displayBlock("container_widget", $context, $blocks);
        echo "
    </h2>
";
    }

    // line 35
    public function block_breadcrumbs_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    ";
        if (twig_test_empty(($context["breadcrumbs"] ?? null))) {
            // line 37
            echo "        ";
            $context["breadcrumbs"] = twig_reverse_filter($this->env, twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["blocks"] ?? null), "title", [], "any", false, false, false, 37), "vars", [], "any", false, false, false, 37), "value", [], "any", false, false, false, 37), " - "));
            // line 38
            echo "    ";
        }
        // line 39
        echo "
    ";
        // line 40
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " breadcrumbs"]);
        // line 43
        echo "    <nav aria-label=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.breadcrumb.aria_label"), "html", null, true);
        echo "\">
        <ol ";
        // line 44
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
            ";
        // line 45
        if ( !twig_test_iterable(($context["breadcrumbs"] ?? null))) {
            // line 46
            echo "                <li class=\"breadcrumbs__item\">
                    <a href=\"\" class=\"breadcrumbs__link\" aria-current=\"location\">";
            // line 47
            echo twig_escape_filter($this->env, ($context["breadcrumbs"] ?? null), "html", null, true);
            echo "</a>
                </li>
            ";
        } else {
            // line 50
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["breadcrumbs"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                // line 51
                echo "                    <li class=\"breadcrumbs__item\">
                        ";
                // line 52
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "label", [], "any", true, true, false, 52)) {
                    // line 53
                    echo "                            ";
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "uri", [], "any", true, true, false, 53)) {
                        // line 54
                        echo "                                ";
                        $context["url"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "uri", [], "any", false, false, false, 54);
                        // line 55
                        echo "                            ";
                    } else {
                        // line 56
                        echo "                                ";
                        $context["url"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "url", [], "any", false, false, false, 56);
                        // line 57
                        echo "                            ";
                    }
                    // line 58
                    echo "
                            ";
                    // line 59
                    if ((($context["url"] ?? null) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 59))) {
                        // line 60
                        echo "                                <a href=\"";
                        echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
                        echo "\" class=\"breadcrumbs__link\">";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "label", [], "any", false, false, false, 60)), "html", null, true);
                        echo "</a>
                                <span aria-hidden=\"true\">/</span>
                            ";
                    } else {
                        // line 63
                        echo "                                <a href=\"";
                        echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
                        echo "\" class=\"breadcrumbs__link\" aria-current=\"location\">";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "label", [], "any", false, false, false, 63)), "html", null, true);
                        echo "</a>
                            ";
                    }
                    // line 65
                    echo "                        ";
                } else {
                    // line 66
                    echo "                            ";
                    if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 66)) {
                        // line 67
                        echo "                                <span aria-hidden=\"true\">/</span>
                            ";
                    }
                    // line 69
                    echo "                            <a href=\"\" class=\"breadcrumbs__link\" aria-current=\"location\">";
                    echo twig_escape_filter($this->env, $context["breadcrumb"], "html", null, true);
                    echo "</a>
                        ";
                }
                // line 71
                echo "                    </li>
                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 73
            echo "            ";
        }
        // line 74
        echo "        </ol>
        ";
        // line 75
        $this->displayBlock("container_widget", $context, $blocks);
        echo "
    </nav>
";
    }

    // line 79
    public function block_logo_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 80
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/layouts/default/block_types.html.twig", 80)->unwrap();
        // line 81
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " logo"]);
        // line 84
        echo "
    ";
        // line 85
        $context["src"] = ((array_key_exists("src", $context)) ? (_twig_default_filter(($context["src"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/orofrontend/default/images/logo/demob2b-logo.svg"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/orofrontend/default/images/logo/demob2b-logo.svg")));
        // line 86
        echo "
    ";
        // line 87
        $context["attr_img"] = ((array_key_exists("attr_img", $context)) ? (_twig_default_filter(($context["attr_img"] ?? null), [])) : ([]));
        // line 88
        echo "    ";
        $context["attr_img"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr_img"] ?? null), ["src" =>         // line 89
($context["src"] ?? null), "~class" => " logo-img", "alt" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro_frontend.logo.alt.label")]);
        // line 93
        echo "
    ";
        // line 94
        if ((array_key_exists("width", $context) &&  !(null === ($context["width"] ?? null)))) {
            // line 95
            echo "        ";
            $context["attr_img"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr_img"] ?? null), ["width" =>             // line 96
($context["width"] ?? null)]);
            // line 98
            echo "    ";
        }
        // line 99
        echo "
    ";
        // line 100
        if ((array_key_exists("height", $context) &&  !(null === ($context["height"] ?? null)))) {
            // line 101
            echo "        ";
            $context["attr_img"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr_img"] ?? null), ["height" =>             // line 102
($context["height"] ?? null)]);
            // line 104
            echo "    ";
        }
        // line 105
        echo "
    ";
        // line 106
        if ((($context["renderLink"] ?? null) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 106), "attributes", [], "any", false, false, false, 106), "get", [0 => "_route"], "method", false, false, false, 106) != ($context["route"] ?? null)))) {
            // line 107
            echo "        <a href=\"";
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["route"] ?? null));
            echo "\" ";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo " aria-label=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro_frontend.logo.aria_label"), "html", null, true);
            echo "\">
            <img ";
            // line 108
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr_img"] ?? null)], 108, $context, $this->getSourceContext());
            echo ">
        </a>
    ";
        } else {
            // line 111
            echo "        <div ";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo ">
            <img ";
            // line 112
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr_img"] ?? null)], 112, $context, $this->getSourceContext());
            echo ">
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroUI/layouts/default/block_types.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  344 => 112,  339 => 111,  333 => 108,  324 => 107,  322 => 106,  319 => 105,  316 => 104,  314 => 102,  312 => 101,  310 => 100,  307 => 99,  304 => 98,  302 => 96,  300 => 95,  298 => 94,  295 => 93,  293 => 89,  291 => 88,  289 => 87,  286 => 86,  284 => 85,  281 => 84,  278 => 81,  275 => 80,  271 => 79,  264 => 75,  261 => 74,  258 => 73,  243 => 71,  237 => 69,  233 => 67,  230 => 66,  227 => 65,  219 => 63,  210 => 60,  208 => 59,  205 => 58,  202 => 57,  199 => 56,  196 => 55,  193 => 54,  190 => 53,  188 => 52,  185 => 51,  167 => 50,  161 => 47,  158 => 46,  156 => 45,  152 => 44,  147 => 43,  145 => 40,  142 => 39,  139 => 38,  136 => 37,  133 => 36,  129 => 35,  122 => 31,  117 => 30,  111 => 27,  106 => 26,  104 => 25,  99 => 24,  96 => 23,  92 => 22,  84 => 17,  79 => 16,  77 => 13,  76 => 12,  75 => 11,  74 => 10,  73 => 9,  72 => 8,  70 => 4,  67 => 3,  64 => 2,  60 => 1,  56 => 79,  53 => 78,  51 => 35,  48 => 34,  46 => 22,  43 => 21,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/layouts/default/block_types.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/block_types.html.twig");
    }
}
