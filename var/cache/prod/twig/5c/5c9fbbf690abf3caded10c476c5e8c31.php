<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAddress/macros.html.twig */
class __TwigTemplate_038bd147821fdd3155694a87bbafc742 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_renderAddress($__address__ = null, $__formatWithHtml__ = false, $__renderBlankValue__ = true, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "address" => $__address__,
            "formatWithHtml" => $__formatWithHtml__,
            "renderBlankValue" => $__renderBlankValue__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            if (($context["address"] ?? null)) {
                // line 3
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["address"] ?? null), "label", [], "any", false, false, false, 3)) {
                    // line 4
                    echo "            <b>";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["address"] ?? null), "label", [], "any", false, false, false, 4));
                    echo "</b>
            <br />
        ";
                }
                // line 7
                if (($context["formatWithHtml"] ?? null)) {
                    // line 8
                    echo $this->extensions['Oro\Bundle\LocaleBundle\Twig\AddressExtension']->formatAddressHtml($this->env, ($context["address"] ?? null));
                } else {
                    // line 10
                    echo twig_nl2br(twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\AddressExtension']->formatAddress(($context["address"] ?? null)), "html", null, true));
                }
            } elseif (            // line 12
($context["renderBlankValue"] ?? null)) {
                // line 13
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"), "html", null, true);
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroAddress/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 13,  75 => 12,  72 => 10,  69 => 8,  67 => 7,  60 => 4,  57 => 3,  54 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAddress/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AddressBundle/Resources/views/macros.html.twig");
    }
}
