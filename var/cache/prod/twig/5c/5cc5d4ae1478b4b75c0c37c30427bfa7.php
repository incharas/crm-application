<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/validation-message-handler/select2-validation-message-handler-view.js */
class __TwigTemplate_c27317661ebfb3f90895da576d7dfcad extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const AbstractValidationMessageHandlerView =
        require('oroform/js/app/views/validation-message-handler/abstract-validation-message-handler-view');

    const Select2ValidationMessageHandlerView = AbstractValidationMessageHandlerView.extend({
        events: {
            'select2-close': 'onSelect2DialogReposition',
            'select2:dialogReposition': 'onSelect2DialogReposition'
        },

        /**
         * @inheritdoc
         */
        constructor: function Select2ValidationMessageHandlerView(options) {
            Select2ValidationMessageHandlerView.__super__.constructor.call(this, options);
        },

        isActive: function() {
            const select2Instance = this.\$el.data('select2');

            return select2Instance.opened() && !select2Instance.container.hasClass('select2-drop-above');
        },

        getPopperReferenceElement: function() {
            return this.\$el.data('select2').container;
        },

        onSelect2DialogReposition: function(e, position) {
            this.active = position === 'below';
            this.update();
        }
    }, {
        test: function(element) {
            return \$(element).data('select2') !== void 0;
        }
    });

    return Select2ValidationMessageHandlerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/validation-message-handler/select2-validation-message-handler-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/validation-message-handler/select2-validation-message-handler-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/validation-message-handler/select2-validation-message-handler-view.js");
    }
}
