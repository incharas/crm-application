<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/jstree-actions-wrapper.html */
class __TwigTemplate_660b073b5ad7c4bb3c8e38ef4704f60c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% var togglerId = _.uniqueId('dropdown-') %>
<button type=\"button\" class=\"jstree-actions__handle btn btn-icon btn-square-light dropdown-toggle dropdown-toggle--no-caret\"
        data-toggle=\"dropdown\" data-placement=\"bottom-end\" id=\"<%- togglerId %>\"
        title=\"<%- _.__('oro.ui.jstree.actions.label') %>\" aria-label=\"<%- _.__('oro.ui.jstree.actions.label') %>\"
        aria-haspopup=\"true\" aria-expanded=\"false\">
    <span class=\"fa-ellipsis-h\" aria-hidden=\"true\"></span>
</button>
<ul class=\"jstree-actions__menu dropdown-menu dropdown-menu__action-column\" data-role=\"jstree-actions\"
    aria-labelledby=\"<%- togglerId %>\" role=\"menu\"></ul>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/jstree-actions-wrapper.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/jstree-actions-wrapper.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/jstree-actions-wrapper.html");
    }
}
