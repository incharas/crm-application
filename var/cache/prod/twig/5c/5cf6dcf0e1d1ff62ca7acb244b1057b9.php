<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/jstree/base-tree-manage-view.js */
class __TwigTemplate_926171b05d984939f2013e34983b43ee extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const widgetManager = require('oroui/js/widget-manager');
    const mediator = require('oroui/js/mediator');
    const messenger = require('oroui/js/messenger');
    const routing = require('routing');
    const BaseTreeView = require('oroui/js/app/views/jstree/base-tree-view');

    const BaseTreeManageView = BaseTreeView.extend({
        treeEvents: {
            'move_node.jstree': 'onMove',
            'select-subtree-item:change': 'onSelectSubtreeChange'
        },

        /**
         * @property {Boolean}
         */
        updateAllowed: false,

        /**
         * @property {Boolean}
         */
        moveTriggered: false,

        /**
         * @property {String}
         */
        reloadWidget: '',

        /**
         * @property {String}
         */
        onMoveRoute: '',

        /**
         * @property {Boolean}
         */
        checkboxEnabled: true,

        /**
         * @inheritdoc
         */
        constructor: function BaseTreeManageView(options) {
            BaseTreeManageView.__super__.constructor.call(this, options);
        },

        /**
         * @param {Object} options
         */
        initialize: function(options) {
            BaseTreeManageView.__super__.initialize.call(this, options);

            if (!this.\$tree) {
                return;
            }

            this.updateAllowed = options.updateAllowed;
            this.reloadWidget = options.reloadWidget;
            this.onMoveRoute = options.onMoveRoute;
        },

        /**
         * @param {Object} options
         * @param {Object} config
         * @returns {Object}
         */
        customizeTreeConfig: function(options, config) {
            BaseTreeManageView.__super__.customizeTreeConfig.call(this, options, config);
            if (options.updateAllowed) {
                config.plugins.push('dnd');
                config.dnd = {
                    copy: false
                };
            }
            return config;
        },

        /**
         * Triggers after switching selectSubTree
         *
         * @param {Object} event
         * @param {Object} data
         */
        onSelectSubtreeChange: function(event, data) {
            this.jsTreeConfig.checkbox.cascade = data.selectSubTree ? 'up+down+undetermined' : 'undetermined';
            this.render();
        },

        /**
         * Triggers after node selection in tree
         *
         * @param {Event} e
         * @param {Object} selected
         */
        onSelect: function(e, selected) {
            BaseTreeManageView.__super__.onSelect.call(this, e, selected);

            if (this.initialization || !this.updateAllowed) {
                return;
            }
            let url;
            if (this.onRootSelectRoute && selected.node.parent === '#') {
                url = routing.generate(this.onRootSelectRoute, {id: selected.node.id});
            } else {
                url = routing.generate(this.onSelectRoute, {id: selected.node.id});
            }
            mediator.execute('redirectTo', {url: url});
        },

        /**
         * Triggers after node move
         *
         * @param {Object} e
         * @param {Object} data
         */
        onMove: function(e, data) {
            if (this.moveTriggered) {
                return;
            }

            const self = this;
            \$.ajax({
                async: false,
                type: 'PUT',
                url: routing.generate(self.onMoveRoute),
                data: {
                    id: data.node.id,
                    parent: data.parent,
                    position: data.position
                },
                success: function(result) {
                    if (!result.status) {
                        self.rollback(data);
                        messenger.notificationFlashMessage(
                            'error',
                            _.__('oro.ui.jstree.move_node_error', {nodeText: data.node.text})
                        );
                    } else if (self.reloadWidget) {
                        widgetManager.getWidgetInstanceByAlias(self.reloadWidget, function(widget) {
                            widget.render();
                        });
                    }
                }
            });
        },

        /**
         * Rollback node move
         *
         * @param {Object} data
         */
        rollback: function(data) {
            if (data.old_position > data.position) {
                data.old_position++;
            }
            this.moveTriggered = true;
            this.\$tree.jstree('move_node', data.node, data.old_parent, data.old_position);
            this.moveTriggered = false;
        },

        /**
         * Off events
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }

            BaseTreeManageView.__super__.dispose.call(this);
        }
    });

    return BaseTreeManageView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/jstree/base-tree-manage-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/jstree/base-tree-manage-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/jstree/base-tree-manage-view.js");
    }
}
