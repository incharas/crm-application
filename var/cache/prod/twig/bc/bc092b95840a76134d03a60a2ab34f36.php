<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/models/autocomplete-results-collection.js */
class __TwigTemplate_6688dc1304bdabd1dc1ccfc02cc2bdbe extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const RoutingCollection = require('oroui/js/app/models/base/routing-collection');

    const AutocompleteResultsCollection = RoutingCollection.extend({
        routeDefaults: {
            routeName: 'oro_form_autocomplete_search',
            routeQueryParameterNames: ['page', 'per_page', 'name', 'query', 'search_by_id']
        },

        stateDefaults: {
            page: 1,
            per_page: 10
        },

        /**
         * @inheritdoc
         */
        constructor: function AutocompleteResultsCollection(options) {
            AutocompleteResultsCollection.__super__.constructor.call(this, options);
        },

        parse: function(response) {
            return response.results;
        },

        setQuery: function(query) {
            this._route.set('query', query);
        },

        setPage: function(page) {
            this._route.set('page', page);
        }
    });

    return AutocompleteResultsCollection;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/models/autocomplete-results-collection.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/models/autocomplete-results-collection.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/models/autocomplete-results-collection.js");
    }
}
