<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Form/fields.html.twig */
class __TwigTemplate_17eb10768a186daefbef6394d39de820 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'form_row' => [$this, 'block_form_row'],
            'group_attributes' => [$this, 'block_group_attributes'],
            'hint_attributes' => [$this, 'block_hint_attributes'],
            'widget_attributes' => [$this, 'block_widget_attributes'],
            'widget_container_attributes' => [$this, 'block_widget_container_attributes'],
            'form_errors' => [$this, 'block_form_errors'],
            'date_widget' => [$this, 'block_date_widget'],
            'choice_widget_expanded' => [$this, 'block_choice_widget_expanded'],
            'choice_widget_options' => [$this, 'block_choice_widget_options'],
            'choice_widget_option_attributes' => [$this, 'block_choice_widget_option_attributes'],
            'collection_render' => [$this, 'block_collection_render'],
            '_oro_entity_config_config_field_type_widget' => [$this, 'block__oro_entity_config_config_field_type_widget'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("form_div_layout.html.twig", "@OroUI/Form/fields.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_form_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        ob_start(function () { return ''; });
        // line 5
        echo "        <div class=\"control-group";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 5)) {
            echo " ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 5), "html", null, true);
        }
        if (array_key_exists("block_prefixes", $context)) {
            echo " control-group-";
            echo twig_escape_filter($this->env, (($__internal_compile_0 = ($context["block_prefixes"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[1] ?? null) : null), "html", null, true);
        }
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["group_attr"] ?? null), "class", [], "any", true, true, false, 5)) {
            echo " ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["group_attr"] ?? null), "class", [], "any", false, false, false, 5), "html", null, true);
        }
        echo "\"";
        if (array_key_exists("group_attr", $context)) {
            $this->displayBlock("group_attributes", $context, $blocks);
        }
        echo ">
            ";
        // line 6
        if ((((array_key_exists("hint", $context)) ? (_twig_default_filter(($context["hint"] ?? null))) : ("")) && (((array_key_exists("hint_position", $context)) ? (_twig_default_filter(($context["hint_position"] ?? null))) : ("")) == "above"))) {
            // line 7
            echo "                <div";
            $this->displayBlock("hint_attributes", $context, $blocks);
            echo ">";
            echo ($context["hint"] ?? null);
            echo "</div>
            ";
        }
        // line 9
        echo "            ";
        if ( !(($context["label"] ?? null) === false)) {
            // line 10
            echo "                <div class=\"control-label wrap\">
                    ";
            // line 11
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["label_attr" => ($context["label_attr"] ?? null)]);
            echo "
                </div>
            ";
        }
        // line 14
        echo "            <div class=\"controls";
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            echo " validation-error";
        }
        echo "\">
                ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
                ";
        // line 16
        if ((((array_key_exists("hint", $context)) ? (_twig_default_filter(($context["hint"] ?? null))) : ("")) && (((array_key_exists("hint_position", $context)) ? (_twig_default_filter(($context["hint_position"] ?? null))) : ("")) == "after_input"))) {
            // line 17
            echo "                    <div";
            $this->displayBlock("hint_attributes", $context, $blocks);
            echo ">";
            echo ($context["hint"] ?? null);
            echo "</div>
                ";
        }
        // line 19
        echo "                ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
            </div>
            ";
        // line 21
        if ((((array_key_exists("hint", $context)) ? (_twig_default_filter(($context["hint"] ?? null))) : ("")) && (((array_key_exists("hint_position", $context)) ? (_twig_default_filter(($context["hint_position"] ?? null))) : ("")) == "below"))) {
            // line 22
            echo "                <div";
            $this->displayBlock("hint_attributes", $context, $blocks);
            echo ">";
            echo ($context["hint"] ?? null);
            echo "</div>
            ";
        }
        // line 24
        echo "        </div>
    ";
        $___internal_parse_11_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 4
        echo twig_spaceless($___internal_parse_11_);
    }

    // line 28
    public function block_group_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["group_attr"] ?? null));
        foreach ($context['_seq'] as $context["n"] => $context["v"]) {
            // line 30
            if (($context["v"] === true)) {
                echo " ";
                echo twig_escape_filter($this->env, $context["n"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["n"], "html", null, true);
                echo "\"";
            } elseif (( !(            // line 31
$context["n"] === "class") &&  !($context["v"] === false))) {
                echo " ";
                echo twig_escape_filter($this->env, $context["n"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['n'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 36
    public function block_hint_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["hint_attr"] ?? null));
        foreach ($context['_seq'] as $context["n"] => $context["v"]) {
            // line 38
            if (($context["v"] === true)) {
                echo " ";
                echo twig_escape_filter($this->env, $context["n"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["n"], "html", null, true);
                echo "\"";
            } elseif ( !(            // line 39
$context["v"] === false)) {
                echo " ";
                echo twig_escape_filter($this->env, $context["n"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["v"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['n'], $context['v'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 44
    public function block_widget_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 46
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 46)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 46) . " error")) : ("error"))]);
            // line 47
            echo "    ";
        }
        // line 48
        echo "    ";
        $this->displayParentBlock("widget_attributes", $context, $blocks);
        echo "
";
    }

    // line 51
    public function block_widget_container_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "    ";
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 53
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 53)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 53) . " error")) : ("error"))]);
            // line 54
            echo "    ";
        }
        // line 55
        echo "    ";
        $this->displayParentBlock("widget_container_attributes", $context, $blocks);
        echo "
";
    }

    // line 58
    public function block_form_errors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        ob_start(function () { return ''; });
        // line 60
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 61
            echo "            ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 61)) {
                // line 62
                echo "                ";
                $context["combinedError"] = "";
                // line 63
                echo "                ";
                $context["newErrors"] = [];
                // line 64
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 65
                    echo "                    ";
                    if (!twig_in_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 65), ($context["newErrors"] ?? null))) {
                        // line 66
                        echo "                        ";
                        $context["newErrors"] = twig_array_merge(($context["newErrors"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 66)]);
                        // line 67
                        echo "                    ";
                    }
                    // line 68
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 69
                echo "                ";
                $context["combinedError"] = twig_join_filter(($context["newErrors"] ?? null), "; ");
                // line 70
                echo "                <span class=\"validation-failed\"><span><span>";
                echo twig_escape_filter($this->env, ($context["combinedError"] ?? null), "html", null, true);
                echo "</span></span></span>
            ";
            } else {
                // line 72
                echo "                ";
                $this->displayParentBlock("form_errors", $context, $blocks);
                echo "
            ";
            }
            // line 74
            echo "        ";
        }
        // line 75
        echo "    ";
        $___internal_parse_12_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 59
        echo twig_spaceless($___internal_parse_12_);
    }

    // line 78
    public function block_date_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 79
        echo "    ";
        ob_start(function () { return ''; });
        // line 80
        echo "        ";
        $context["type"] = "text";
        // line 81
        echo "        ";
        if ((($context["widget"] ?? null) == "single_text")) {
            // line 82
            echo "            ";
            $this->displayBlock("form_widget_simple", $context, $blocks);
            echo "
        ";
        } else {
            // line 84
            echo "            <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
                ";
            // line 85
            echo twig_replace_filter(($context["date_pattern"] ?? null), ["{{ year }}" =>             // line 86
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "year", [], "any", false, false, false, 86), 'widget'), "{{ month }}" =>             // line 87
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "month", [], "any", false, false, false, 87), 'widget'), "{{ day }}" =>             // line 88
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "day", [], "any", false, false, false, 88), 'widget')]);
            // line 89
            echo "
            </div>
        ";
        }
        // line 92
        echo "    ";
        $___internal_parse_13_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 79
        echo twig_spaceless($___internal_parse_13_);
    }

    // line 95
    public function block_choice_widget_expanded($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "    ";
        ob_start(function () { return ''; });
        // line 97
        echo "        ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 97)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 97) . " horizontal choice-widget-expanded")) : ("horizontal choice-widget-expanded"))]);
        // line 98
        echo "        ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 98) . " validate-group")]);
        // line 99
        echo "        ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => twig_join_filter(array_unique(twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 99), " ")), " ")]);
        // line 100
        echo "        <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
            ";
        // line 101
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 102
            echo "                <div class=\"choice-widget-expanded__item\">
                    ";
            // line 103
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
            echo "
                    ";
            // line 104
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'label', ["raw_label" => ((array_key_exists("raw_label", $context)) ? (($context["raw_label"] ?? null)) : (false)), "translatable_label" => ((array_key_exists("translatable_options", $context)) ? (($context["translatable_options"] ?? null)) : (true))]);
            echo "
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 107
        echo "        </div>
    ";
        $___internal_parse_14_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 96
        echo twig_spaceless($___internal_parse_14_);
    }

    // line 111
    public function block_choice_widget_options($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 112
        echo "    ";
        ob_start(function () { return ''; });
        // line 113
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 114
            echo "            ";
            if (twig_test_iterable($context["choice"])) {
                // line 115
                echo "                <optgroup label=\"";
                echo twig_escape_filter($this->env, (((array_key_exists("translatable_groups", $context) &&  !($context["translatable_groups"] ?? null))) ? ($context["group_label"]) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["group_label"], [], ($context["translation_domain"] ?? null)))), "html", null, true);
                echo "\">
                    ";
                // line 116
                $context["options"] = $context["choice"];
                // line 117
                echo "                    ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
                </optgroup>
            ";
            } else {
                // line 120
                echo "                ";
                $context["label"] = (((array_key_exists("translatable_options", $context) &&  !($context["translatable_options"] ?? null))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 120)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 120), [], ($context["translation_domain"] ?? null))));
                // line 121
                echo "                <option ";
                $this->displayBlock("choice_widget_option_attributes", $context, $blocks);
                echo " value=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "value", [], "any", false, false, false, 121), "html", null, true);
                echo "\"";
                if (Symfony\Bridge\Twig\Extension\twig_is_selected_choice($context["choice"], ($context["value"] ?? null))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "is_safe", [], "any", true, true, false, 121) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "is_safe", [], "any", false, false, false, 121))) {
                    echo ($context["label"] ?? null);
                } else {
                    echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                }
                echo "</option>
            ";
            }
            // line 123
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 124
        echo "    ";
        $___internal_parse_15_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 112
        echo twig_spaceless($___internal_parse_15_);
    }

    // line 127
    public function block_choice_widget_option_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 128
        ob_start(function () { return ''; });
        // line 129
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["choice"] ?? null), "attr", [], "any", true, true, false, 129) && (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["choice"] ?? null), "attr", [], "any", false, false, false, 129)) > 0))) {
            // line 130
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["choice"] ?? null), "attr", [], "any", false, false, false, 130));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\" ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 131
            echo "    ";
        }
        $___internal_parse_16_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 128
        echo twig_spaceless($___internal_parse_16_);
    }

    // line 135
    public function block_collection_render($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 136
        echo "    ";
        ob_start(function () { return ''; });
        // line 137
        echo "        ";
        $macros["__internal_parse_18"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/Form/fields.html.twig", 137)->unwrap();
        // line 138
        echo "        <div class=\"row-oro\">
            <div class=\"collection-fields-list\" data-prototype=\"";
        // line 139
        echo twig_escape_filter($this->env, twig_call_macro($macros["__internal_parse_18"], "macro_collection_prototype", [($context["subform"] ?? null)], 139, $context, $this->getSourceContext()));
        echo "\">
                ";
        // line 140
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["subform"] ?? null), "children", [], "any", false, false, false, 140));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 141
            echo "                    ";
            echo twig_call_macro($macros["__internal_parse_18"], "macro_collection_prototype", [$context["field"]], 141, $context, $this->getSourceContext());
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 143
        echo "            </div>
            <a class=\"btn add-list-item\" href=\"#\"><i class=\"fa-plus\"></i>";
        // line 144
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add"), "html", null, true);
        echo "</a>
        </div>
    ";
        $___internal_parse_17_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 136
        echo twig_spaceless($___internal_parse_17_);
    }

    // line 149
    public function block__oro_entity_config_config_field_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 150
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 150));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 151
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["field"], 'widget');
            echo "
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "@OroUI/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  567 => 151,  562 => 150,  558 => 149,  554 => 136,  548 => 144,  545 => 143,  536 => 141,  532 => 140,  528 => 139,  525 => 138,  522 => 137,  519 => 136,  515 => 135,  511 => 128,  507 => 131,  494 => 130,  491 => 129,  489 => 128,  485 => 127,  481 => 112,  478 => 124,  464 => 123,  446 => 121,  443 => 120,  436 => 117,  434 => 116,  429 => 115,  426 => 114,  408 => 113,  405 => 112,  401 => 111,  397 => 96,  393 => 107,  384 => 104,  380 => 103,  377 => 102,  373 => 101,  368 => 100,  365 => 99,  362 => 98,  359 => 97,  356 => 96,  352 => 95,  348 => 79,  345 => 92,  340 => 89,  338 => 88,  337 => 87,  336 => 86,  335 => 85,  330 => 84,  324 => 82,  321 => 81,  318 => 80,  315 => 79,  311 => 78,  307 => 59,  304 => 75,  301 => 74,  295 => 72,  289 => 70,  286 => 69,  280 => 68,  277 => 67,  274 => 66,  271 => 65,  266 => 64,  263 => 63,  260 => 62,  257 => 61,  255 => 60,  253 => 59,  249 => 58,  242 => 55,  239 => 54,  236 => 53,  233 => 52,  229 => 51,  222 => 48,  219 => 47,  216 => 46,  213 => 45,  209 => 44,  195 => 39,  188 => 38,  184 => 37,  180 => 36,  166 => 31,  159 => 30,  155 => 29,  151 => 28,  147 => 4,  143 => 24,  135 => 22,  133 => 21,  127 => 19,  119 => 17,  117 => 16,  113 => 15,  106 => 14,  100 => 11,  97 => 10,  94 => 9,  86 => 7,  84 => 6,  64 => 5,  61 => 4,  57 => 3,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Form/fields.html.twig");
    }
}
