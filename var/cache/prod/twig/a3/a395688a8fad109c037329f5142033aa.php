<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/select2-autosizer.js */
class __TwigTemplate_729df170b9e855f65a7f533aefbc2a9a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function() {
    'use strict';
    return {
        applyTo: function(select2el, marginsData) {
            const choices = select2el.find('.select2-search-choice');
            const widthPieces = Math.ceil(Math.pow(choices.length, 0.6));
            const widthes = choices.map(function(i, item) {
                return item.clientWidth;
            });
            widthes.sort();
            const percentile90 = widthes[Math.floor(widthes.length * 0.9)];
            select2el.find('.select2-choices').width(
                (percentile90 + marginsData.SELECTED_ITEMS_H_MARGIN_BETWEEN) * widthPieces +
                marginsData.SELECTED_ITEMS_H_INCREMENT
            );
        }
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/select2-autosizer.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/select2-autosizer.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/select2-autosizer.js");
    }
}
