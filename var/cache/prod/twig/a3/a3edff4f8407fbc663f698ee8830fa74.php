<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntitySerializedFields/Form/fields.html.twig */
class __TwigTemplate_cc9e5351e870e0ec34f449e5401e057f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_serialized_fields_is_serialized_type_widget' => [$this, 'block_oro_serialized_fields_is_serialized_type_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_serialized_fields_is_serialized_type_widget', $context, $blocks);
    }

    public function block_oro_serialized_fields_is_serialized_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        ob_start(function () { return ''; });
        // line 3
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
        <script type=\"text/javascript\">
            loadModules(['jquery', 'oroui/js/mediator'], function (\$, mediator) {
                \$(function () {
                    var storageTypeSelector  = 'form[name=oro_entity_extend_field_type] select[data-ftid=oro_entity_extend_field_type_is_serialized]';
                    var fieldTypeSelector    = 'form[name=oro_entity_extend_field_type] select[data-ftid=oro_entity_extend_field_type_type]';
                    var _onChangeStorageType = function () {
                        var serializableTypes = ";
        // line 10
        echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "serializableTypes", [], "any", false, false, false, 10));
        echo ";
                        var excludeTypes = [];
                        ";
        // line 12
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 12), "vars", [], "any", false, true, false, 12), "excludeTypes", [], "any", true, true, false, 12)) {
            // line 13
            echo "                            excludeTypes = ";
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 13), "vars", [], "any", false, false, false, 13), "excludeTypes", [], "any", false, false, false, 13));
            echo ";
                        ";
        }
        // line 15
        echo "                        \$(fieldTypeSelector + ' optgroup').each(function (index, value) {
                            \$.each(\$(value).find('option'), function (index, value) {
                                \$(value).removeAttr('disabled');
                                var selectedStorageType = \$(storageTypeSelector).val();
                                if (selectedStorageType == 1) {
                                    if (serializableTypes.indexOf(\$(value).val()) === -1) {
                                        \$(value).attr('disabled', 'disabled');
                                    }
                                } else {
                                    if (excludeTypes.indexOf(\$(value).val()) !== -1) {
                                        \$(value).attr('disabled', 'disabled');
                                    }
                                }
                            })
                        });
                        \$(fieldTypeSelector).select2('val', '');
                    };

                    \$(storageTypeSelector).on('change.changeStorageType', _onChangeStorageType);

                    mediator.once('page:beforeChange', function () {
                        \$(storageTypeSelector).off('change.changeStorageType', _onChangeStorageType);
                    });
                })
            });
        </script>
    ";
        $___internal_parse_96_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 2
        echo twig_spaceless($___internal_parse_96_);
    }

    public function getTemplateName()
    {
        return "@OroEntitySerializedFields/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  101 => 2,  72 => 15,  66 => 13,  64 => 12,  59 => 10,  48 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntitySerializedFields/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform-serialised-fields/Resources/views/Form/fields.html.twig");
    }
}
