<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReportCRM/Report/index.html.twig */
class __TwigTemplate_38a525f18074b1d603a8cd4db708eaf6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroReportCRM/Report/index.html.twig", 3)->unwrap();
        // line 5
        $context["reportGroupName"] = (("oro.reportcrm.menu." . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "reportGroupName", [], "any", false, false, false, 5)) . "_report_tab.label");

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%reportName%" => twig_title_string_filter($this->env,         // line 6
($context["pageTitle"] ?? null)), "%reportGroup%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["reportGroupName"] ?? null))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroReportCRM/Report/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $context["buttons"] = [];
        // line 10
        echo "    ";
        $context["renderParams"] = twig_array_merge(["enableViews" => false], ((array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])));
        // line 11
        echo "    ";
        $context["params"] = twig_array_merge(((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), ["_grid_view" => ["_disabled" => true], "_tags" => ["_disabled" => true]]);
        // line 20
        echo "    ";
        $this->displayParentBlock("content", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroReportCRM/Report/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 20,  64 => 11,  61 => 10,  58 => 9,  54 => 8,  49 => 1,  47 => 6,  44 => 5,  42 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReportCRM/Report/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ReportCRMBundle/Resources/views/Report/index.html.twig");
    }
}
