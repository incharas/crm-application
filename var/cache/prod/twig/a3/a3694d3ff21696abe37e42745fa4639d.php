<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/menu.html.twig */
class __TwigTemplate_bc18e9bf56f4f8ace0bb49fb14d9530f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'compressed_root' => [$this, 'block_compressed_root'],
            'root' => [$this, 'block_root'],
            'list' => [$this, 'block_list'],
            'children' => [$this, 'block_children'],
            'item' => [$this, 'block_item'],
            'item_renderer' => [$this, 'block_item_renderer'],
            'item_content' => [$this, 'block_item_content'],
            'list_wrapper' => [$this, 'block_list_wrapper'],
            'linkElement' => [$this, 'block_linkElement'],
            'spanElement' => [$this, 'block_spanElement'],
            'label' => [$this, 'block_label'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/Menu/menu.html.twig", 2)->unwrap();
        // line 3
        $macros["Navigation"] = $this->macros["Navigation"] = $this->loadTemplate("@OroNavigation/macros.html.twig", "@OroNavigation/Menu/menu.html.twig", 3)->unwrap();
        // line 1
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu_base.html.twig", "@OroNavigation/Menu/menu.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 19
    public function block_compressed_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "    ";
        ob_start(function () { return ''; });
        // line 21
        echo "        ";
        $this->displayBlock("root", $context, $blocks);
        echo "
    ";
        $___internal_parse_93_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 20
        echo twig_spaceless($___internal_parse_93_);
    }

    // line 25
    public function block_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    ";
        $context["listAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "childrenAttributes", [], "any", false, false, false, 26);
        // line 27
        echo "
    ";
        // line 28
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "rootClass", [], "any", true, true, false, 28)) {
            // line 29
            echo "        ";
            $macros["oro_menu"] = $this;
            // line 30
            echo "        ";
            $context["listAttributes"] = twig_array_merge(($context["listAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [($context["listAttributes"] ?? null), "class", [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "rootClass", [], "any", false, false, false, 30)]], 30, $context, $this->getSourceContext())]);
            // line 31
            echo "    ";
        }
        // line 32
        echo "    ";
        $this->displayBlock("list", $context, $blocks);
    }

    // line 35
    public function block_list($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 36) &&  !(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 36) === 0)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 36))) {
            // line 37
            $macros["oro_menu"] = $this;
            // line 38
            $context["childrenContent"] =             $this->renderBlock("children", $context, $blocks);
            // line 39
            if (($context["childrenContent"] ?? null)) {
                // line 40
                echo "<ul";
                echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["listAttributes"] ?? null)], 40, $context, $this->getSourceContext());
                echo ">
                ";
                // line 41
                echo ($context["childrenContent"] ?? null);
                echo "
            </ul>";
            }
        }
    }

    // line 47
    public function block_children($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        $context["currentOptions"] = ($context["options"] ?? null);
        // line 50
        $context["currentItem"] = ($context["item"] ?? null);
        // line 52
        if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 52))) {
            // line 53
            $context["options"] = twig_array_merge(($context["currentOptions"] ?? null), ["depth" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["currentOptions"] ?? null), "depth", [], "any", false, false, false, 53) - 1)]);
        }
        // line 55
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["currentItem"] ?? null), "children", [], "any", false, false, false, 55));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 56
            $context["itemAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "attributes", [], "any", false, false, false, 56);
            // line 57
            $context["childrenAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "childrenAttributes", [], "any", false, false, false, 57);
            // line 58
            $context["classes"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["itemAttributes"] ?? null), "class", [], "any", true, true, false, 58)) ? (twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["itemAttributes"] ?? null), "class", [], "any", false, false, false, 58), " ")) : ([]));
            // line 59
            $context["childrenClasses"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["childrenAttributes"] ?? null), "class", [], "any", true, true, false, 59)) ? (twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["childrenAttributes"] ?? null), "class", [], "any", false, false, false, 59), " ")) : ([]));
            // line 60
            ob_start(function () { return ''; });
            // line 61
            $this->displayBlock("item", $context, $blocks);
            $context["itemContent"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 63
            if ((($context["itemContent"] ?? null) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "uri", [], "any", false, false, false, 63) != "#"))) {
                // line 64
                echo ($context["itemContent"] ?? null);
            }
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        $context["item"] = ($context["currentItem"] ?? null);
        // line 69
        $context["options"] = ($context["currentOptions"] ?? null);
    }

    // line 72
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "    ";
        $this->displayBlock("item_renderer", $context, $blocks);
        echo "
";
    }

    // line 76
    public function block_item_renderer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 77
        $context["showNonAuthorized"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 77), "show_non_authorized", [], "any", true, true, false, 77) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 77), "show_non_authorized", [], "any", false, false, false, 77));
        // line 78
        $context["displayable"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 78), "isAllowed", [], "any", false, false, false, 78) || ($context["showNonAuthorized"] ?? null));
        // line 79
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayed", [], "any", false, false, false, 79) && ($context["displayable"] ?? null))) {
            // line 81
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["matcher"] ?? null), "isCurrent", [0 => ($context["item"] ?? null)], "method", false, false, false, 81)) {
                // line 82
                $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "currentClass", [], "any", false, false, false, 82)]);
            } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 83
($context["matcher"] ?? null), "isAncestor", [0 => ($context["item"] ?? null), 1 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 83)], "method", false, false, false, 83)) {
                // line 84
                $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "ancestorClass", [], "any", false, false, false, 84)]);
            }
            // line 86
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "actsLikeFirst", [], "any", false, false, false, 86)) {
                // line 87
                $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "firstClass", [], "any", false, false, false, 87)]);
            }
            // line 89
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "actsLikeLast", [], "any", false, false, false, 89)) {
                // line 90
                $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "lastClass", [], "any", false, false, false, 90)]);
            }
            // line 92
            if ( !twig_test_empty(($context["classes"] ?? null))) {
                // line 93
                $context["itemAttributes"] = twig_array_merge(($context["itemAttributes"] ?? null), ["class" => twig_join_filter(array_unique(($context["classes"] ?? null)), " ")]);
            }
            // line 95
            echo "        ";
            // line 96
            echo "        ";
            $macros["oro_menu"] = $this;
            // line 97
            echo "        <li";
            echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["itemAttributes"] ?? null)], 97, $context, $this->getSourceContext());
            echo ">
            ";
            // line 98
            $this->displayBlock("item_content", $context, $blocks);
            echo "
        </li>";
        }
    }

    // line 103
    public function block_item_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 104
        $context["linkAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "linkAttributes", [], "any", false, false, false, 104);
        // line 105
        $context["labelAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "labelAttributes", [], "any", false, false, false, 105);
        // line 106
        ob_start(function () { return ''; });
        // line 107
        if (( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 107)) && ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["matcher"] ?? null), "isCurrent", [0 => ($context["item"] ?? null)], "method", false, false, false, 107) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "currentAsLink", [], "any", false, false, false, 107)))) {
            // line 108
            echo "            ";
            $this->displayBlock("linkElement", $context, $blocks);
        } else {
            // line 110
            echo "            ";
            $this->displayBlock("spanElement", $context, $blocks);
        }
        $context["elementContent"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 114
        $context["childrenClasses"] = twig_array_merge(($context["childrenClasses"] ?? null), [0 => "menu", 1 => ("menu-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 114))]);
        // line 115
        $context["listAttributes"] = twig_array_merge(($context["childrenAttributes"] ?? null), ["class" => twig_join_filter(array_unique(($context["childrenClasses"] ?? null)), " ")]);
        // line 116
        $context["wrapperContent"] =         $this->renderBlock("list_wrapper", $context, $blocks);
        // line 117
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 117) != "#") || ($context["wrapperContent"] ?? null))) {
            // line 118
            echo ($context["elementContent"] ?? null);
            // line 119
            echo ($context["wrapperContent"] ?? null);
        }
    }

    // line 124
    public function block_list_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 125
        echo "    ";
        $this->displayBlock("list", $context, $blocks);
        echo "
";
    }

    // line 128
    public function block_linkElement($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 129
        echo "    ";
        $macros["oro_menu"] = $this;
        // line 130
        echo "    ";
        $context["extras"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 130);
        // line 131
        echo "
    ";
        // line 132
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["extras"] ?? null), "dialog", [], "any", true, true, false, 132) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["extras"] ?? null), "dialog", [], "any", false, false, false, 132))) {
            // line 133
            echo "        ";
            echo twig_call_macro($macros["Navigation"], "macro_renderClientLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["extras"] ?? null), "dialog_config", [], "any", false, false, false, 133), ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 134
($context["app"] ?? null), "user", [], "any", false, false, false, 134), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 135
($context["app"] ?? null), "user", [], "any", false, false, false, 135), "id", [], "any", false, false, false, 135)]], 133, $context, $this->getSourceContext());
        } else {
            // line 138
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 138), "html", null, true);
            echo "\"";
            echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 138, $context, $this->getSourceContext());
            echo ">";
            $this->displayBlock("label", $context, $blocks);
            echo "</a>
    ";
        }
        // line 140
        echo "
";
    }

    // line 143
    public function block_spanElement($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 144
        echo "    ";
        $macros["oro_menu"] = $this;
        // line 145
        echo "    <span";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["labelAttributes"] ?? null)], 145, $context, $this->getSourceContext());
        echo ">";
        $this->displayBlock("label", $context, $blocks);
        echo "</span>
";
    }

    // line 148
    public function block_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 149
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "icon"], "method", false, false, false, 149)) {
            // line 150
            echo "        <span class=\"";
            echo twig_escape_filter($this->env, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "icon"], "method", false, false, false, 150) . " menu-icon"), "html", null, true);
            echo "\" aria-hidden=\"true\"></span>
    ";
        }
        // line 152
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "allow_safe_labels", [], "any", false, false, false, 152) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "safe_label", 1 => false], "method", false, false, false, 152))) {
            // line 153
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getLabel", [], "method", false, false, false, 153)));
        } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 154
($context["item"] ?? null), "getExtra", [0 => "translate_disabled", 1 => false], "method", false, false, false, 154) == false)) {
            // line 155
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getLabel", [], "method", false, false, false, 155), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "translateParams", 1 => []], "method", false, false, false, 155), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "translateDomain", 1 => "messages"], "method", false, false, false, 155)), "html", null, true);
        } else {
            // line 157
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getLabel", [], "method", false, false, false, 157), "html", null, true);
        }
    }

    // line 5
    public function macro_attributes($__attributes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attributes" => $__attributes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 6
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attributes"] ?? null));
            foreach ($context['_seq'] as $context["name"] => $context["value"]) {
                // line 7
                if (( !(null === $context["value"]) &&  !($context["value"] === false))) {
                    // line 8
                    echo twig_sprintf(" %s=\"%s\"", $context["name"], ((($context["value"] === true)) ? (twig_escape_filter($this->env, $context["name"])) : (twig_escape_filter($this->env, $context["value"]))));
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 13
    public function macro_add_attribute_values($__attributes__ = null, $__attribute__ = null, $__values__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attributes" => $__attributes__,
            "attribute" => $__attribute__,
            "values" => $__values__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 14
            $context["_values"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attributes"] ?? null), ($context["attribute"] ?? null), [], "array", true, true, false, 14)) ? (twig_split_filter($this->env, (($__internal_compile_0 = ($context["attributes"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[($context["attribute"] ?? null)] ?? null) : null), " ")) : ([]));
            // line 15
            $context["_values"] = twig_array_merge(($context["_values"] ?? null), ($context["values"] ?? null));
            // line 16
            echo twig_escape_filter($this->env, twig_join_filter(($context["_values"] ?? null), " "), "html", null, true);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  435 => 16,  433 => 15,  431 => 14,  416 => 13,  402 => 8,  400 => 7,  395 => 6,  382 => 5,  377 => 157,  374 => 155,  372 => 154,  370 => 153,  368 => 152,  362 => 150,  360 => 149,  356 => 148,  347 => 145,  344 => 144,  340 => 143,  335 => 140,  325 => 138,  322 => 135,  321 => 134,  319 => 133,  317 => 132,  314 => 131,  311 => 130,  308 => 129,  304 => 128,  297 => 125,  293 => 124,  288 => 119,  286 => 118,  284 => 117,  282 => 116,  280 => 115,  278 => 114,  273 => 110,  269 => 108,  267 => 107,  265 => 106,  263 => 105,  261 => 104,  257 => 103,  250 => 98,  245 => 97,  242 => 96,  240 => 95,  237 => 93,  235 => 92,  232 => 90,  230 => 89,  227 => 87,  225 => 86,  222 => 84,  220 => 83,  218 => 82,  216 => 81,  214 => 79,  212 => 78,  210 => 77,  206 => 76,  199 => 73,  195 => 72,  191 => 69,  189 => 68,  174 => 64,  172 => 63,  169 => 61,  167 => 60,  165 => 59,  163 => 58,  161 => 57,  159 => 56,  142 => 55,  139 => 53,  137 => 52,  135 => 50,  133 => 49,  129 => 47,  121 => 41,  116 => 40,  114 => 39,  112 => 38,  110 => 37,  108 => 36,  104 => 35,  99 => 32,  96 => 31,  93 => 30,  90 => 29,  88 => 28,  85 => 27,  82 => 26,  78 => 25,  74 => 20,  68 => 21,  65 => 20,  61 => 19,  56 => 1,  54 => 3,  52 => 2,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/menu.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/menu.html.twig");
    }
}
