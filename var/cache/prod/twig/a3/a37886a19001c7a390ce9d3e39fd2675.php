<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/date.js */
class __TwigTemplate_10cd2bb62d0bb9be907609bedd136d19 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const __ = require('orotranslation/js/translator');
    const datetimeFormatter = require('orolocale/js/formatter/datetime');

    const defaultParam = {
        message: 'This value is not a valid date.'
    };

    /**
     * @export oroform/js/validator/date
     */
    return [
        'Date',
        function(value, element) {
            const format = element.getAttribute('data-format');
            return this.optional(element) || element.type === 'date' ||
                datetimeFormatter.isDateValid(String(value)) ||
                format === 'backend' && datetimeFormatter.isBackendDateValid(String(value));
        },
        function(param, element) {
            const value = String(this.elementValue(element));
            const placeholders = {};
            param = Object.assign({}, defaultParam, param);
            placeholders.value = value;
            return __(param.message, placeholders);
        }
    ];
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/date.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/date.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/date.js");
    }
}
