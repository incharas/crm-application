<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/jstree/expand-action-view.js */
class __TwigTemplate_48d1467ca29cbf3585eca55e20c884e5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const AbstractActionView = require('oroui/js/app/views/jstree/abstract-action-view');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');

    const ExpandActionView = AbstractActionView.extend({
        options: _.extend({}, AbstractActionView.prototype.options, {
            icon: 'plus-square-o',
            label: __('oro.ui.jstree.actions.expand')
        }),

        /**
         * @inheritdoc
         */
        constructor: function ExpandActionView(options) {
            ExpandActionView.__super__.constructor.call(this, options);
        },

        onClick: function() {
            const \$tree = this.options.\$tree;
            const settings = \$tree.jstree().settings;
            const autohideNeighbors = settings.autohideNeighbors;

            settings.autohideNeighbors = false;
            const afterOpenAll = _.debounce(function() {
                settings.autohideNeighbors = autohideNeighbors;
                \$tree.off('open_all.jstree', afterOpenAll);
            }, 100);

            \$tree.on('open_all.jstree', afterOpenAll);

            \$tree.jstree('open_all');
        }
    });

    return ExpandActionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/jstree/expand-action-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/jstree/expand-action-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/jstree/expand-action-view.js");
    }
}
