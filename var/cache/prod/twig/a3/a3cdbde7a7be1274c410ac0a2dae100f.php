<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Js/items.html.twig */
class __TwigTemplate_e2e8fbf81c04fa1fc4fe3f67c3211539 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script id=\"widget-items-item-select-template\" type=\"text/template\">
    <div class=\"pull-left items-selector-widget\">
        <select class=\"item-select filter-select-oro\">
            <option></option>
            <% items.each(function (item) { %>
                <option value=\"<%- item.get('id') %>\"><%- item.get('label') %></option>
            <% }); %>
        </select>
    </div>
</script>

<script id=\"widget-items-item-template\" type=\"text/template\">
    <tr class=\"widget-items-item-row\" data-cid=\"<%- cid %>\">
        <td><%- label %></td>
        <td class=\"action-cell\">
            <input type=\"hidden\" name=\"<%- namePrefix %>[id]\" value=\"<%- id %>\">
            <input data-name=\"order\" type=\"hidden\" class=\"order\" name=\"<%- namePrefix %>[order]\" value=\"<%- order %>\">
            <input class=\"hide\" data-name=\"show\" type=\"checkbox\" name=\"<%- namePrefix %>[show]\" <%- show ? 'checked' : '' %>>
            <% if (_.isMobile()) { %>
            <span class=\"btn btn-icon delete-button\" data-collection-action=\"delete\" title=\"";
        // line 20
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.actions.move_column_up.label"), "html", null, true);
        echo "\" aria-hidden=\"true\">
                <i class=\"fa-trash-o\"></i>
            </span>
            <span class=\"btn btn-icon move-up<% if (isFirst) { %> disabled<% } %>\" data-collection-action=\"moveUp\" title=\"";
        // line 23
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.actions.move_column_up.label"), "html", null, true);
        echo "\" aria-hidden=\"true\">
                <i class=\"fa-chevron-up\"></i>
            </span>
            <span class=\"btn btn-icon move-down<% if (isLast) { %> disabled<% } %>\" data-collection-action=\"moveDown\" title=\"";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.actions.move_column_down.label"), "html", null, true);
        echo "\" aria-hidden=\"true\">
                <i class=\"fa-chevron-down\"></i>
            </span>
            <% } else { %>
            <a href=\"#\" role=\"button\" class=\"action btn-icon no-hash delete-button\"
               title=\"";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.actions.delete.label"), "html", null, true);
        echo "\"
               data-collection-action=\"delete\"
               aria-hidden=\"true\">
                <i class=\"fa-trash-o hide-text\"></i>
            </a>
            <span class=\"btn-icon\" title=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.actions.move_column.label"), "html", null, true);
        echo "\" aria-hidden=\"true\">
                <i class=\"fa-arrows-v handle\"></i>
            </span>
            <% } %>
        </td>
    </tr>
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Js/items.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 36,  78 => 31,  70 => 26,  64 => 23,  58 => 20,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Js/items.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Js/items.html.twig");
    }
}
