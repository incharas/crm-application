<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDigitalAsset/DigitalAsset/Datagrid/Property/image.html.twig */
class __TwigTemplate_440a5979502fdf32a81195ce25dbf195 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "rootEntity", [], "any", false, false, false, 1), "sourceFile", [], "any", false, false, false, 1)) {
            // line 2
            echo "    ";
            $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroDigitalAsset/DigitalAsset/Datagrid/Property/image.html.twig", 2)->display(twig_array_merge($context, ["file" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 3
($context["record"] ?? null), "rootEntity", [], "any", false, false, false, 3), "sourceFile", [], "any", false, false, false, 3), "filter" => "digital_asset_in_dialog", "img_attrs" => ["class" => "digital-asset-thumbnail", "alt" => $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileTitle(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 7
($context["record"] ?? null), "rootEntity", [], "any", false, false, false, 7), "sourceFile", [], "any", false, false, false, 7))]]));
        }
    }

    public function getTemplateName()
    {
        return "@OroDigitalAsset/DigitalAsset/Datagrid/Property/image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 7,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDigitalAsset/DigitalAsset/Datagrid/Property/image.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DigitalAssetBundle/Resources/views/DigitalAsset/Datagrid/Property/image.html.twig");
    }
}
