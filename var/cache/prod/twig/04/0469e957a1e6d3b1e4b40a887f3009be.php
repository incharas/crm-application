<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/simple-color-picker-view.js */
class __TwigTemplate_4260c03d37973c97ef8f2f911abaa920 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(['underscore', 'oroform/js/app/views/base-simple-color-picker-view'
], function(_, BaseSimpleColorPickerView) {
    'use strict';

    const SimpleColorPickerView = BaseSimpleColorPickerView.extend({
        /**
         * @inheritdoc
         */
        constructor: function SimpleColorPickerView(options) {
            SimpleColorPickerView.__super__.constructor.call(this, options);
        },

        /**
         * @constructor
         * @param {object} options
         */
        initialize: function(options) {
            SimpleColorPickerView.__super__.initialize.call(this, options);
        },

        /**
         * @inheritdoc
         */
        _processOptions: function(options) {
            SimpleColorPickerView.__super__._processOptions.call(this, options);

            const selectedVal = this.\$el.val();
            const customIndex = _.findIndex(options.data, function(item) {
                return item.class === 'custom-color';
            });

            if (customIndex !== -1) {
                if (_.isMobile()) {
                    if (customIndex > 0 && _.isEmpty(options.data[customIndex - 1])) {
                        options.data.splice(customIndex - 1, 2);
                    } else {
                        options.data.splice(customIndex, 1);
                    }
                } else {
                    // set custom color
                    const selectedIndex = _.findIndex(options.data, function(item) {
                        return item.id === selectedVal;
                    });

                    options.data[customIndex].id = selectedVal && selectedIndex === -1 ? selectedVal : '#FFFFFF';
                }
            }
        },

        /**
         * @inheritdoc
         */
        _getSimpleColorPickerOptions: function(options) {
            options = SimpleColorPickerView.__super__._getSimpleColorPickerOptions.call(this, options);
            return _.defaults(_.omit(options, ['custom_color']), {
                emptyColor: '#FFFFFF'
            });
        },

        /**
         * @inheritdoc
         */
        _getPickerOptions: function(options) {
            return SimpleColorPickerView.__super__._getPickerOptions.call(this, options.custom_color);
        },

        /**
         * @inheritdoc
         */
        _getPicker: function() {
            return this.\$parent.find('span.custom-color');
        }
    });

    return SimpleColorPickerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/simple-color-picker-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/simple-color-picker-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/simple-color-picker-view.js");
    }
}
