<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContactUs/layouts/embedded_default/oro_embedded_form_submit/form.html.twig */
class __TwigTemplate_23287edd58c5f3a261596e5da464e903 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'fieldset_widget' => [$this, 'block_fieldset_widget'],
            'form_field_widget' => [$this, 'block_form_field_widget'],
            'form_fields_widget' => [$this, 'block_form_fields_widget'],
            '_embedded_form_firstName_widget' => [$this, 'block__embedded_form_firstName_widget'],
            '_embedded_form_lastName_widget' => [$this, 'block__embedded_form_lastName_widget'],
            '_embedded_form_comment_widget' => [$this, 'block__embedded_form_comment_widget'],
            '_embedded_form_submit_widget' => [$this, 'block__embedded_form_submit_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('fieldset_widget', $context, $blocks);
        // line 6
        echo "
";
        // line 7
        $this->displayBlock('form_field_widget', $context, $blocks);
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('form_fields_widget', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('_embedded_form_firstName_widget', $context, $blocks);
        // line 29
        echo "
";
        // line 30
        $this->displayBlock('_embedded_form_lastName_widget', $context, $blocks);
        // line 35
        echo "
";
        // line 36
        $this->displayBlock('_embedded_form_comment_widget', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('_embedded_form_submit_widget', $context, $blocks);
    }

    // line 1
    public function block_fieldset_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <div class=\"row-group\">
        ";
        // line 3
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 7
    public function block_form_field_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<div class=\"row-group\"";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 8), "extra_field", [], "any", true, true, false, 8) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 8), "extra_field", [], "any", false, false, false, 8))) {
            echo " id=\"";
            echo twig_escape_filter($this->env, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8) . "_holder"), "html", null, true);
            echo "\" ";
        }
        echo " >
    <div class=\"box\">
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
    </div>
</div>
";
    }

    // line 15
    public function block_form_fields_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    ";
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroContactUs/fields.html.twig"], true);
        // line 17
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 17), "id", [], "any", false, false, false, 17), "name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 17), "name", [], "any", false, false, false, 17), "novalidate" => "novalidate"]]);
        echo "
    <div class=\"wrapper\">
        ";
        // line 19
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
    ";
        // line 21
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
";
    }

    // line 24
    public function block__embedded_form_firstName_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    <div class=\"box\">
        ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
    </div>
";
    }

    // line 30
    public function block__embedded_form_lastName_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "    <div class=\"box\">
        ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
    </div>
";
    }

    // line 36
    public function block__embedded_form_comment_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    <div class=\"row-group\">
        ";
        // line 38
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
    </div>
";
    }

    // line 42
    public function block__embedded_form_submit_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    <div class=\"row-group\">
        ";
        // line 44
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroContactUs/layouts/embedded_default/oro_embedded_form_submit/form.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  187 => 44,  184 => 43,  180 => 42,  173 => 38,  170 => 37,  166 => 36,  159 => 32,  156 => 31,  152 => 30,  145 => 26,  142 => 25,  138 => 24,  132 => 21,  127 => 19,  121 => 17,  118 => 16,  114 => 15,  106 => 10,  96 => 8,  92 => 7,  85 => 3,  82 => 2,  78 => 1,  74 => 42,  71 => 41,  69 => 36,  66 => 35,  64 => 30,  61 => 29,  59 => 24,  56 => 23,  54 => 15,  51 => 14,  49 => 7,  46 => 6,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContactUs/layouts/embedded_default/oro_embedded_form_submit/form.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactUsBundle/Resources/views/layouts/embedded_default/oro_embedded_form_submit/form.html.twig");
    }
}
