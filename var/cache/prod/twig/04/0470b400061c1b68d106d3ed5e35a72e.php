<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/js/bootstrap.js */
class __TwigTemplate_1994f26fae62861cf1219beed9058c18 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    require('bootstrap-alert');
    require('bootstrap-dropdown');
    require('bootstrap-modal');
    require('bootstrap-popover');
    require('bootstrap-scrollspy');
    require('bootstrap-tab');
    require('bootstrap-tooltip');
    require('bootstrap-typeahead');

    return \$;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/js/bootstrap.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/js/bootstrap.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/js/bootstrap.js");
    }
}
