<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables/dialog-variables.scss */
class __TwigTemplate_3801282fe4caca34b3d609081d10babc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$dialog-mobile-max-width: 100vw !default;
\$dialog-mobile-min-width: initial !default;
\$dialog-mobile-width: 100% !default;
\$dialog-mobile-height: auto !default;
\$dialog-mobile-min-height: 100% !default;
\$dialog-mobile-box-sizing: border-box !default;
\$dialog-mobile-position: inherit !default;
\$dialog-mobile-top: 0 !default;
\$dialog-mobile-left: 0 !default;
\$dialog-mobile-border: none !default;
\$dialog-mobile-border-radius: 0 !default;
\$dialog-mobile-box-shadow: none !default;
\$dialog-mobile-overflow: visible !default;

\$dialog-form-mobile-min-width: initial !default;

\$dialog-mobile-header-position: fixed !default;
\$dialog-mobile-header-width: 100% !default;
\$dialog-mobile-header-top: 0 !default;
\$dialog-mobile-header-left: 0 !default;
\$dialog-mobile-header-z-index: \$oro-mobile-header-zindex !default;
\$dialog-mobile-header-border-radius: 0 !default;
\$dialog-mobile-header-inner-offset: 10px 21px 10px !default;
\$dialog-mobile-header-outer-offset: 0 !default;

\$dialog-mobile-content-position: inherit !default;
\$dialog-mobile-content-min-height: 0 !default;
\$dialog-mobile-content-max-height: none !default;
\$dialog-mobile-content-height: auto !default;
\$dialog-mobile-content-outer-offset: 0 !default;
\$dialog-mobile-content-inner-offset: 64px 16px 16px !default;

\$dialog-mobile-titlebar-icons-display: none !default;
\$dialog-mobile-dialog-on-background-display: none !default;
\$dialog-mobile-dialog-not-buttons-display: none !default;
\$dialog-widget-mobile-overlay-display: none !default;

\$dialog-mobile-actions-btn-font-size: \$base-font-size !default;
\$dialog-mobile-actions-btn-height: 40px !default;
\$dialog-mobile-actions-btn-line-height: 40px !default;
\$dialog-mobile-actions-btn-inner-offset: 0 16px !default;
\$dialog-mobile-actions-btn-border-radius: 20px !default;
\$dialog-mobile-actions-btn-border: none !default;
\$dialog-mobile-actions-btn-outer-offset: 8px !default;

\$dialog-mobile-inner-grid-min-width: 0 !default;

\$dialog-resizable-handle-mobile-display: none !default;

\$dialog-actions-mobile-text-align: center !default;
\$dialog-actions-mobile-float: none !default;
\$dialog-actions-mobile-inner-offset: 14px 16px;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables/dialog-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables/dialog-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables/dialog-variables.scss");
    }
}
