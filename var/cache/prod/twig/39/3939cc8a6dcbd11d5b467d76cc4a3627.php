<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCampaign/Campaign/widget/view.html.twig */
class __TwigTemplate_a1ac48363640f2fac4d650cd3c82933c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCampaign/Campaign/widget/view.html.twig", 1)->unwrap();
        // line 2
        $macros["email"] = $this->macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroCampaign/Campaign/widget/view.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroCampaign/Campaign/widget/view.html.twig", 3)->unwrap();
        // line 4
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 8
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 8)], 8, $context, $this->getSourceContext());
        // line 9
        ob_start(function () { return ''; });
        // line 10
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "code", [], "any", false, false, false, 10), "html", null, true);
        echo "
                ";
        // line 11
        if ( !twig_test_empty(($context["codes_history"] ?? null))) {
            echo " (";
            echo twig_escape_filter($this->env, twig_join_filter(($context["codes_history"] ?? null), ", "), "html", null, true);
            echo ") ";
            echo twig_call_macro($macros["ui"], "macro_tooltip", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.code.tooltip")], 11, $context, $this->getSourceContext());
        }
        $context["codesData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 13
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.code.label"), ($context["codesData"] ?? null), null, null, ["dir" => "ltr"]], 13, $context, $this->getSourceContext());
        echo "
            ";
        // line 14
        echo twig_call_macro($macros["ui"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 14)], 14, $context, $this->getSourceContext());
        echo "
        </div>

        <div class=\"responsive-block\">
            ";
        // line 18
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.budget.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatCurrency(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "budget", [], "any", false, false, false, 18))], 18, $context, $this->getSourceContext());
        echo "
            ";
        // line 19
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.start_date.label"), ((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startDate", [], "any", false, false, false, 19))) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")) : ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startDate", [], "any", false, false, false, 19))))], 19, $context, $this->getSourceContext());
        echo "
            ";
        // line 20
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.end_date.label"), ((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "endDate", [], "any", false, false, false, 20))) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")) : ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "endDate", [], "any", false, false, false, 20))))], 20, $context, $this->getSourceContext());
        echo "
            ";
        // line 21
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.report_period.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("oro.campaign.report_period." . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "reportPeriod", [], "any", false, false, false, 21)))], 21, $context, $this->getSourceContext());
        echo "
        </div>

        <div class=\"responsive-block\">
            ";
        // line 25
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 25, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroCampaign/Campaign/widget/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  88 => 21,  84 => 20,  80 => 19,  76 => 18,  69 => 14,  65 => 13,  57 => 11,  53 => 10,  51 => 9,  49 => 8,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCampaign/Campaign/widget/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/CampaignBundle/Resources/views/Campaign/widget/view.html.twig");
    }
}
