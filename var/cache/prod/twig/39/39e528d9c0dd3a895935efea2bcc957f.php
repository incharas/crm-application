<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/jstree/collapse-action-view.js */
class __TwigTemplate_fa2ac2e8f01980afc7cc1f7f3e719a66 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const AbstractActionView = require('oroui/js/app/views/jstree/abstract-action-view');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');

    const CollapseActionView = AbstractActionView.extend({
        options: _.extend({}, AbstractActionView.prototype.options, {
            icon: 'minus-square-o',
            label: __('oro.ui.jstree.actions.collapse')
        }),

        /**
         * @inheritdoc
         */
        constructor: function CollapseActionView(options) {
            CollapseActionView.__super__.constructor.call(this, options);
        },

        onClick: function() {
            this.options.\$tree.jstree('close_all');
        }
    });

    return CollapseActionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/jstree/collapse-action-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/jstree/collapse-action-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/jstree/collapse-action-view.js");
    }
}
