<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/jstree/jstree-wrapper.scss */
class __TwigTemplate_0f18a933b4e783889da074908842541f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.jstree-wrapper {
    position: \$jstree-wrapper-position;

    min-height: \$jstree-wrapper-min-height;
    padding: \$jstree-wrapper-inner-offset;

    width: \$jstree-wrapper-width;

    .controls & {
        max-width: \$field-width;
        padding-top: 0;
    }

    &__inner {
        height: \$jstree-wrapper-inner-height;
        display: \$jstree-wrapper-inner-display;
        flex-direction: \$jstree-wrapper-inner-flex-direction;
    }

    &__title {
        display: \$jstree-wrapper-title-display;

        align-items: \$jstree-wrapper-title-align-items;
        justify-content: \$jstree-wrapper-title-justify-content;
    }

    &__label {
        display: \$jstree-wrapper-label-display;
        margin-top: \$jstree-wrapper-label-offset-top;
        margin-bottom: \$jstree-wrapper-label-offset-bottom;
    }

    &__content {
        min-height: \$jstree-wrapper-content-min-height;
        display: \$jstree-wrapper-content-display;
        flex-direction: \$jstree-wrapper-content-flex-direction;
    }

    &.expanded .jstree-actions {
        display: \$jstree-wrapper-label-expanded-jstree-actions-display;
    }

    &__text {
        font-size: \$jstree-wrapper-text-font-size;
        font-weight: \$jstree-wrapper-text-font-weight;

        cursor: \$jstree-wrapper-text-cursor;

        color: \$jstree-wrapper-text-color;
    }

    &__checkbox {
        display: \$jstree-wrapper-checkbox-display;
    }

    .jstree {
        margin: \$jstree-wrapper-jstree-offset;
        outline: \$jstree-wrapper-jstree-outline;
    }
}

.content-with-sidebar--sidebar {
    .jstree-wrapper {
        height: \$content-with-sidebar-jstree-wrapper-height;
        padding: \$content-with-sidebar-jstree-wrapper-inner-offset;
    }
}

.sidebar-container {
    .content-with-sidebar {
        &--sidebar {
            .jstree-container {
                min-height: \$sidebar-container-content-with-sidebar-jstree-wrapper-min-height;
                overflow: \$sidebar-container-content-with-sidebar-jstree-wrapper-overflow;
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/jstree/jstree-wrapper.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/jstree/jstree-wrapper.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/jstree/jstree-wrapper.scss");
    }
}
