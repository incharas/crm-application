<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMicrosoftIntegration/Form/fields.html.twig */
class __TwigTemplate_a2ed38ca13cef202912f0b4fe8768529 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_microsoft_integration_enable_config_checkbox_widget' => [$this, 'block_oro_microsoft_integration_enable_config_checkbox_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_microsoft_integration_enable_config_checkbox_widget', $context, $blocks);
    }

    public function block_oro_microsoft_integration_enable_config_checkbox_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <div data-page-component-module=\"oromicrosoftintegration/js/app/components/enable-checkbox-component\">
        ";
        // line 3
        $this->displayBlock("checkbox_widget", $context, $blocks);
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroMicrosoftIntegration/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  48 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMicrosoftIntegration/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MicrosoftIntegrationBundle/Resources/views/Form/fields.html.twig");
    }
}
