<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Form/fields.html.twig */
class __TwigTemplate_e1bf552db0888a57fa06e3429beeead7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_type_widget_datetime_range_widget' => [$this, 'block_oro_type_widget_datetime_range_widget'],
            'oro_type_widget_date_range_widget' => [$this, 'block_oro_type_widget_date_range_widget'],
            'oro_type_current_date_widget_date_range_widget' => [$this, 'block_oro_type_current_date_widget_date_range_widget'],
            'oro_type_dependent_date_widget_date_range_widget' => [$this, 'block_oro_type_dependent_date_widget_date_range_widget'],
            'oro_type_widget_title_widget' => [$this, 'block_oro_type_widget_title_widget'],
            'oro_type_widget_items_row' => [$this, 'block_oro_type_widget_items_row'],
            'oro_type_widget_items_javascript' => [$this, 'block_oro_type_widget_items_javascript'],
            'oro_type_widget_date_widget' => [$this, 'block_oro_type_widget_date_widget'],
            'oro_dashboard_query_filter_row' => [$this, 'block_oro_dashboard_query_filter_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_type_widget_datetime_range_widget', $context, $blocks);
        // line 8
        echo "
";
        // line 9
        $this->displayBlock('oro_type_widget_date_range_widget', $context, $blocks);
        // line 17
        echo "
";
        // line 18
        $this->displayBlock('oro_type_current_date_widget_date_range_widget', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('oro_type_dependent_date_widget_date_range_widget', $context, $blocks);
        // line 33
        echo "
";
        // line 34
        $this->displayBlock('oro_type_widget_title_widget', $context, $blocks);
        // line 52
        echo "
";
        // line 53
        $this->displayBlock('oro_type_widget_items_row', $context, $blocks);
        // line 83
        echo "
";
        // line 84
        $this->displayBlock('oro_type_widget_items_javascript', $context, $blocks);
        // line 103
        echo "
";
        // line 104
        $this->displayBlock('oro_type_widget_date_widget', $context, $blocks);
        // line 127
        echo "
";
        // line 128
        $this->displayBlock('oro_dashboard_query_filter_row', $context, $blocks);
    }

    // line 1
    public function block_oro_type_widget_datetime_range_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["valueType"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 2), "value", [], "any", false, false, false, 2), "type", [], "any", false, false, false, 2) != "")) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 2), "value", [], "any", false, false, false, 2), "type", [], "any", false, false, false, 2)) : (1));
        // line 3
        echo "
    ";
        // line 4
        $macros["UI"] = $this->loadTemplate("@OroDashboard/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 4)->unwrap();
        // line 5
        echo "
    ";
        // line 6
        echo twig_call_macro($macros["UI"], "macro_renderDateWidgeView", [($context["form"] ?? null), ($context["valueType"] ?? null), "datetime", "orodashboard/js/app/views/widget-datetime-range-view"], 6, $context, $this->getSourceContext());
        echo "
";
    }

    // line 9
    public function block_oro_type_widget_date_range_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $context["monthType"] = twig_constant("Oro\\Bundle\\FilterBundle\\Form\\Type\\Filter\\AbstractDateFilterType::TYPE_THIS_MONTH");
        // line 11
        echo "    ";
        $context["valueType"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 11), "value", [], "any", false, false, false, 11), "type", [], "any", false, false, false, 11) != "")) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 11), "value", [], "any", false, false, false, 11), "type", [], "any", false, false, false, 11)) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 11), "datetime_range_metadata", [], "any", false, false, false, 11), "valueTypes", [], "any", false, false, false, 11)) ? (($context["monthType"] ?? null)) : (1))));
        // line 12
        echo "
    ";
        // line 13
        $macros["UI"] = $this->loadTemplate("@OroDashboard/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 13)->unwrap();
        // line 14
        echo "
    ";
        // line 15
        echo twig_call_macro($macros["UI"], "macro_renderDateWidgeView", [($context["form"] ?? null), ($context["valueType"] ?? null), "date", "orodashboard/js/app/views/widget-date-range-view"], 15, $context, $this->getSourceContext());
        echo "
";
    }

    // line 18
    public function block_oro_type_current_date_widget_date_range_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $context["valueType"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 19), "value", [], "any", false, true, false, 19), "type", [], "any", true, true, false, 19)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 19), "value", [], "any", false, true, false, 19), "type", [], "any", false, false, false, 19), twig_constant("Oro\\Bundle\\FilterBundle\\Form\\Type\\Filter\\AbstractDateFilterType::TYPE_THIS_MONTH"))) : (twig_constant("Oro\\Bundle\\FilterBundle\\Form\\Type\\Filter\\AbstractDateFilterType::TYPE_THIS_MONTH")));
        // line 20
        echo "
    ";
        // line 21
        $macros["UI"] = $this->loadTemplate("@OroDashboard/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 21)->unwrap();
        // line 22
        echo "
    ";
        // line 23
        echo twig_call_macro($macros["UI"], "macro_renderDateWidgeView", [($context["form"] ?? null), ($context["valueType"] ?? null), "date", "orodashboard/js/app/views/current-date-widget-date-range-view"], 23, $context, $this->getSourceContext());
        echo "
";
    }

    // line 26
    public function block_oro_type_dependent_date_widget_date_range_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    ";
        $context["valueType"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 27), "value", [], "any", false, true, false, 27), "type", [], "any", true, true, false, 27)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 27), "value", [], "any", false, true, false, 27), "type", [], "any", false, false, false, 27), twig_constant("Oro\\Bundle\\FilterBundle\\Form\\Type\\Filter\\AbstractDateFilterType::TYPE_NONE"))) : (twig_constant("Oro\\Bundle\\FilterBundle\\Form\\Type\\Filter\\AbstractDateFilterType::TYPE_NONE")));
        // line 28
        echo "
    ";
        // line 29
        $macros["UI"] = $this->loadTemplate("@OroDashboard/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 29)->unwrap();
        // line 30
        echo "
    ";
        // line 31
        echo twig_call_macro($macros["UI"], "macro_renderDateWidgeView", [($context["form"] ?? null), ($context["valueType"] ?? null), "date", "orodashboard/js/app/views/widget-date-range-view"], 31, $context, $this->getSourceContext());
        echo "
";
    }

    // line 34
    public function block_oro_type_widget_title_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 35)->unwrap();
        // line 36
        echo "
    <div class=\"widget-title-container\" ";
        // line 37
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroform/js/app/views/default-field-value-view", "options" => ["fieldSelector" => ("input#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 40
($context["form"] ?? null), "title", [], "any", false, false, false, 40), "vars", [], "any", false, false, false, 40), "id", [], "any", false, false, false, 40)), "prepareTinymce" => false]]], 37, $context, $this->getSourceContext());
        // line 43
        echo ">
        <div class=\"widget-title-widget\">
            ";
        // line 45
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 45), 'widget', ["attr" => ["class" => "widget-title-input"]]);
        echo "
        </div>
        ";
        // line 47
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "useDefault", [], "any", false, false, false, 47), 'row', ["attr" => ["data-role" => "changeUseDefault"]]);
        echo "
    </div>


";
    }

    // line 53
    public function block_oro_type_widget_items_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 54)->unwrap();
        // line 55
        echo "    ";
        $context["rowId"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 55), "id", [], "any", false, false, false, 55) . "Row");
        // line 56
        echo "
    <div id=\"";
        // line 57
        echo twig_escape_filter($this->env, ($context["rowId"] ?? null), "html", null, true);
        echo "\" class=\"control-group dashboard-widget-items\">
        <label class=\"control-label\">";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
        echo "</label>
        <div class=\"controls items-table-container\">
            <div class=\"actions-wrap\">
                ";
        // line 61
        echo twig_call_macro($macros["UI"], "macro_clientButton", [["aCss" => "no-hash add-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.actions.add.label")]], 61, $context, $this->getSourceContext());
        // line 64
        echo "
                ";
        // line 65
        echo twig_call_macro($macros["UI"], "macro_clientButton", [["aCss" => "no-hash btn-primary add-all-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.actions.add_all.label")]], 65, $context, $this->getSourceContext());
        // line 68
        echo "
            </div>
            <table id=\"";
        // line 70
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 70), "id", [], "any", false, false, false, 70), "html", null, true);
        echo "\" class=\"grid grid-main-container table table-bordered table-condensed\">
                <thead>
                <tr>
                    <th><span>";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 73), "item_label", [], "any", false, false, false, 73)), "html", null, true);
        echo "</span></th>
                    <th class=\"action-column\"><span>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.datagrid.columns.actions"), "html", null, true);
        echo "</span></th>
                </tr>
                </thead>
                <tbody class=\"item-container\"></tbody>
            </table>
        </div>
    </div>
    ";
        // line 81
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderJavascript(($context["form"] ?? null));
        echo "
";
    }

    // line 84
    public function block_oro_type_widget_items_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 85
        echo "    ";
        $context["options"] = ["_sourceElement" => (("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 86
($context["form"] ?? null), "vars", [], "any", false, false, false, 86), "id", [], "any", false, false, false, 86)) . "Row"), "itemsData" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 87
($context["form"] ?? null), "children", [], "any", false, false, false, 87), "items", [], "any", false, false, false, 87), "vars", [], "any", false, false, false, 87), "value", [], "any", false, false, false, 87), "baseName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 88
($context["form"] ?? null), "children", [], "any", false, false, false, 88), "items", [], "any", false, false, false, 88), "vars", [], "any", false, false, false, 88), "full_name", [], "any", false, false, false, 88)];
        // line 90
        echo "    ";
        if (( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 90), "attr", [], "any", false, false, false, 90)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 90), "attr", [], "any", false, true, false, 90), "placeholder", [], "any", true, true, false, 90))) {
            // line 91
            echo "        ";
            $context["options"] = twig_array_merge(($context["options"] ?? null), ["placeholder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 92
($context["form"] ?? null), "vars", [], "any", false, false, false, 92), "attr", [], "any", false, false, false, 92), "placeholder", [], "any", false, false, false, 92))]);
            // line 94
            echo "    ";
        }
        // line 95
        echo "
    ";
        // line 96
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 96)->unwrap();
        // line 97
        echo "
    <div ";
        // line 98
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orodashboard/js/items/view", "options" =>         // line 100
($context["options"] ?? null)]], 98, $context, $this->getSourceContext());
        // line 101
        echo "></div>
";
    }

    // line 104
    public function block_oro_type_widget_date_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 105
        echo "    <div class=\"widget-date-compare\">
        <div class=\"widget-date-widget\">
            ";
        // line 107
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "useDate", [], "any", false, false, false, 107), 'widget', ["attr" => ["data-role" => "updateDatapicker"]]);
        echo "
        </div>
        ";
        // line 109
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "date", [], "any", true, true, false, 109)) {
            // line 110
            echo "            <div class=\"widget-date-input-widget\">
                ";
            // line 111
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "date", [], "any", false, false, false, 111), 'widget');
            echo "
            </div>

            ";
            // line 114
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 114)->unwrap();
            // line 115
            echo "
            <div ";
            // line 116
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orodashboard/js/app/views/widget-date-compare-view", "options" => ["_sourceElement" => ".widget-date-compare", "useDateSelector" => ("input#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 120
($context["form"] ?? null), "useDate", [], "any", false, false, false, 120), "vars", [], "any", false, false, false, 120), "id", [], "any", false, false, false, 120)), "dateSelector" => ("input#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 121
($context["form"] ?? null), "date", [], "any", false, false, false, 121), "vars", [], "any", false, false, false, 121), "id", [], "any", false, false, false, 121))]]], 116, $context, $this->getSourceContext());
            // line 123
            echo "></div>
        ";
        }
        // line 125
        echo "    </div>
";
    }

    // line 128
    public function block_oro_dashboard_query_filter_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 129
        echo "    ";
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 130
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 130), 'row');
            echo "
        ";
            // line 131
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "definition", [], "any", false, false, false, 131), 'row');
            echo "
    ";
        } else {
            // line 133
            echo "        <div class=\"control-group\">
            <label class=\"control-label\">";
            // line 134
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
            echo "</label>
            <div class=\"controls query-filter-row\">
            ";
            // line 136
            if (($context["collapsible"] ?? null)) {
                // line 137
                echo "                ";
                $context["togglerId"] = uniqid("toggler-");
                // line 138
                echo "                ";
                $context["collapseId"] = uniqid("collapse-");
                // line 139
                echo "                <a id=\"";
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\" role=\"button\"
                   class=\"";
                // line 140
                if (($context["collapsed"] ?? null)) {
                    echo " collapsed";
                }
                echo "\"
                   data-toggle=\"collapse\" href=\"#";
                // line 141
                echo twig_escape_filter($this->env, ($context["collapseId"] ?? null), "html", null, true);
                echo "\"
                   aria-expanded=\"";
                // line 142
                echo ((($context["collapsed"] ?? null)) ? ("false") : ("true"));
                echo "\"
                   aria-controls=\"";
                // line 143
                echo twig_escape_filter($this->env, ($context["collapseId"] ?? null), "html", null, true);
                echo "\"
                >
                    <span data-text>";
                // line 145
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("oro.dashboard.query_filter." . ((($context["collapsed"] ?? null)) ? ("expand") : ("collapse")))), "html", null, true);
                echo "</span>
                </a>
            ";
            }
            // line 148
            echo "                <div";
            if (($context["collapsible"] ?? null)) {
                // line 149
                echo "                    id=\"";
                echo twig_escape_filter($this->env, ($context["collapseId"] ?? null), "html", null, true);
                echo "\"
                    data-expanded-text=\"";
                // line 150
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.query_filter.collapse"), "html", null, true);
                echo "\"
                    data-collapsed-text=\"";
                // line 151
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.query_filter.expand"), "html", null, true);
                echo "\"
                    class=\"dashboard-query-filter collapse";
                // line 152
                if ( !($context["collapsed"] ?? null)) {
                    echo " show";
                }
                echo "\"
                    aria-labelledby=\"";
                // line 153
                echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                echo "\"
                    ";
            }
            // line 155
            echo "                >
                    ";
            // line 156
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 156), 'row');
            echo "
                    ";
            // line 157
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "definition", [], "any", false, false, false, 157), 'row');
            echo "
                    ";
            // line 158
            $context["metadata"] = $this->extensions['Oro\Bundle\DashboardBundle\Twig\DashboardExtension']->getQueryFilterMetadata();
            // line 159
            echo "                    ";
            $context["column_chain_template_id"] = "column-chain-template";
            // line 160
            echo "                    ";
            $macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 160)->unwrap();
            // line 161
            echo "                    ";
            echo twig_call_macro($macros["QD"], "macro_query_designer_column_chain_template", [($context["column_chain_template_id"] ?? null)], 161, $context, $this->getSourceContext());
            echo "
                    ";
            // line 162
            $macros["segmentQD"] = $this->loadTemplate("@OroSegment/macros.html.twig", "@OroDashboard/Form/fields.html.twig", 162)->unwrap();
            // line 163
            echo "                    ";
            echo twig_call_macro($macros["segmentQD"], "macro_query_designer_condition_builder", [["id" => (            // line 164
($context["name"] ?? null) . "-condition-builder"), "disable_audit" => true, "metadata" =>             // line 166
($context["metadata"] ?? null), "column_chain_template_selector" => ("#" .             // line 167
($context["column_chain_template_id"] ?? null))]], 163, $context, $this->getSourceContext());
            // line 168
            echo "

                    ";
            // line 170
            $context["widgetOptions"] = ["valueSource" => (((("[data-ftid=" .             // line 171
($context["widgetType"] ?? null)) . "_") . ($context["name"] ?? null)) . "_definition]"), "entityChoice" => (((("[data-ftid=" .             // line 172
($context["widgetType"] ?? null)) . "_") . ($context["name"] ?? null)) . "_entity]"), "entityChangeConfirmMessage" => (twig_replace_filter(            // line 173
($context["name"] ?? null), ["_" => "."]) . ".change_entity_confirmation"), "metadata" =>             // line 174
($context["metadata"] ?? null), "disable_audit" => true, "initEntityChangeEvents" => false, "select2FieldChoiceTemplate" => ("#" .             // line 177
($context["column_chain_template_id"] ?? null))];
            // line 179
            echo "                    ";
            $context["widgetOptions"] = $this->extensions['Oro\Bundle\SegmentBundle\Twig\SegmentExtension']->updateSegmentWidgetOptions(($context["widgetOptions"] ?? null), ($context["name"] ?? null));
            // line 180
            echo "                    <div
                        data-page-component-module=\"orosegment/js/app/components/segment-component\"
                        data-page-component-options=\"";
            // line 182
            echo twig_escape_filter($this->env, json_encode(($context["widgetOptions"] ?? null)), "html", null, true);
            echo "\">
                    </div>
                </div>
            </div>
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  486 => 182,  482 => 180,  479 => 179,  477 => 177,  476 => 174,  475 => 173,  474 => 172,  473 => 171,  472 => 170,  468 => 168,  466 => 167,  465 => 166,  464 => 164,  462 => 163,  460 => 162,  455 => 161,  452 => 160,  449 => 159,  447 => 158,  443 => 157,  439 => 156,  436 => 155,  431 => 153,  425 => 152,  421 => 151,  417 => 150,  412 => 149,  409 => 148,  403 => 145,  398 => 143,  394 => 142,  390 => 141,  384 => 140,  379 => 139,  376 => 138,  373 => 137,  371 => 136,  366 => 134,  363 => 133,  358 => 131,  353 => 130,  350 => 129,  346 => 128,  341 => 125,  337 => 123,  335 => 121,  334 => 120,  333 => 116,  330 => 115,  328 => 114,  322 => 111,  319 => 110,  317 => 109,  312 => 107,  308 => 105,  304 => 104,  299 => 101,  297 => 100,  296 => 98,  293 => 97,  291 => 96,  288 => 95,  285 => 94,  283 => 92,  281 => 91,  278 => 90,  276 => 88,  275 => 87,  274 => 86,  272 => 85,  268 => 84,  262 => 81,  252 => 74,  248 => 73,  242 => 70,  238 => 68,  236 => 65,  233 => 64,  231 => 61,  225 => 58,  221 => 57,  218 => 56,  215 => 55,  212 => 54,  208 => 53,  199 => 47,  194 => 45,  190 => 43,  188 => 40,  187 => 37,  184 => 36,  181 => 35,  177 => 34,  171 => 31,  168 => 30,  166 => 29,  163 => 28,  160 => 27,  156 => 26,  150 => 23,  147 => 22,  145 => 21,  142 => 20,  139 => 19,  135 => 18,  129 => 15,  126 => 14,  124 => 13,  121 => 12,  118 => 11,  115 => 10,  111 => 9,  105 => 6,  102 => 5,  100 => 4,  97 => 3,  94 => 2,  90 => 1,  86 => 128,  83 => 127,  81 => 104,  78 => 103,  76 => 84,  73 => 83,  71 => 53,  68 => 52,  66 => 34,  63 => 33,  61 => 26,  58 => 25,  56 => 18,  53 => 17,  51 => 9,  48 => 8,  46 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Form/fields.html.twig");
    }
}
