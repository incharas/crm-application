<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCampaign/Campaign/view.html.twig */
class __TwigTemplate_c645b2fe453c2f9d534173ca1a5e1380 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroCampaign/Campaign/view.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "N/A")) : ("N/A"))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroCampaign/Campaign/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCampaign/Campaign/view.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 10
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_campaign_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["entity"] ?? null), "id", [], "any", false, false, false, 11)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.entity_label")]], 10, $context, $this->getSourceContext());
            // line 13
            echo "
    ";
        }
        // line 15
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_navButtons", $context)) ? (_twig_default_filter(($context["update_navButtons"] ?? null), "update_navButtons")) : ("update_navButtons")), ["entity" => ($context["entity"] ?? null)]);
    }

    // line 18
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 20
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_campaign_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 23
($context["entity"] ?? null), "name", [], "any", true, true, false, 23)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 23), "N/A")) : ("N/A"))];
        // line 25
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 28
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 29), "createdAt", [], "any", false, false, false, 29)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 29), "createdAt", [], "any", false, false, false, 29)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 30
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.start_date.label"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 30), "startDate", [], "any", false, false, false, 30)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 30), "startDate", [], "any", false, false, false, 30)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 31
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.end_date.label"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 31), "endDate", [], "any", false, false, false, 31)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 31), "endDate", [], "any", false, false, false, 31)), "html", null, true))) : (print ("N/A")));
        echo "</li>
";
    }

    // line 34
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    ";
        ob_start(function () { return ''; });
        // line 36
        echo "        ";
        $this->loadTemplate("@OroCampaign/Campaign/widget/view.html.twig", "@OroCampaign/Campaign/view.html.twig", 36)->display($context);
        // line 37
        echo "    ";
        $context["campaignInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 38
        echo "
    ";
        // line 39
        ob_start(function () { return ''; });
        // line 40
        echo "        ";
        $context["trackingTotalGrid"] = "campaign-tracking-total-report-grid";
        // line 41
        echo "        ";
        if ( !(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "reportPeriod", [], "any", false, false, false, 41) === constant("Oro\\Bundle\\CampaignBundle\\Entity\\Campaign::PERIOD_HOURLY"))) {
            // line 42
            echo "            ";
            $context["trackingTotalGrid"] = (($context["trackingTotalGrid"] ?? null) . "-precalculated");
            // line 43
            echo "        ";
        }
        // line 44
        echo "        ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["trackingTotalGrid"] ?? null), ["campaign" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 44)], ["cssClass" => "inner-grid"]], 44, $context, $this->getSourceContext());
        echo "
    ";
        $context["trackingEventsReport"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 46
        echo "
    ";
        // line 47
        ob_start(function () { return ''; });
        // line 48
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_campaign_event_plot", ["period" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 53
($context["entity"] ?? null), "reportPeriod", [], "any", false, false, false, 53), "campaign" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 54
($context["entity"] ?? null), "id", [], "any", false, false, false, 54)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.block.detailed_report")]);
        // line 58
        echo "
    ";
        $context["trackingPlot"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 60
        echo "
    ";
        // line 61
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.block.general"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 66
($context["campaignInformationWidget"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.block.events"), "subblocks" => [0 => ["data" => [0 =>         // line 73
($context["trackingPlot"] ?? null), 1 =>         // line 74
($context["trackingEventsReport"] ?? null)]]]], 2 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.tracking_code.label"), "subblocks" => [0 => ["data" => [0 => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.tracking_code.info", ["%campaignCode%" => twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 82
($context["entity"] ?? null), "code", [], "any", false, false, false, 82))])]]]]];
        // line 88
        ob_start(function () { return ''; });
        // line 89
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_marketing_activities_summary", $context)) ? (_twig_default_filter(($context["view_content_data_marketing_activities_summary"] ?? null), "view_content_data_marketing_activities_summary")) : ("view_content_data_marketing_activities_summary")), ["campaignId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 89)]);
        $context["marketingActivitiesSummary"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 92
        if ( !twig_test_empty(($context["marketingActivitiesSummary"] ?? null))) {
            // line 93
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.summary.label"), "priority" => 1000, "subblocks" => [0 => ["spanClass" => "empty", "data" => [0 =>             // line 99
($context["marketingActivitiesSummary"] ?? null)]]]]]);
            // line 103
            echo "    ";
        }
        // line 104
        echo "
    ";
        // line 105
        $context["id"] = "campaignView";
        // line 106
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 107
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCampaign/Campaign/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 107,  199 => 106,  197 => 105,  194 => 104,  191 => 103,  189 => 99,  187 => 93,  185 => 92,  182 => 89,  180 => 88,  178 => 82,  177 => 74,  176 => 73,  175 => 66,  174 => 61,  171 => 60,  167 => 58,  165 => 54,  164 => 53,  162 => 48,  160 => 47,  157 => 46,  151 => 44,  148 => 43,  145 => 42,  142 => 41,  139 => 40,  137 => 39,  134 => 38,  131 => 37,  128 => 36,  125 => 35,  121 => 34,  113 => 31,  107 => 30,  100 => 29,  96 => 28,  89 => 25,  87 => 23,  86 => 20,  84 => 19,  80 => 18,  75 => 15,  71 => 13,  69 => 11,  67 => 10,  65 => 9,  62 => 8,  59 => 7,  55 => 6,  50 => 1,  48 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCampaign/Campaign/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/CampaignBundle/Resources/views/Campaign/view.html.twig");
    }
}
