<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/macros.html.twig */
class __TwigTemplate_b54e554dc8ec9180df8b5830208cf349 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "
";
    }

    // line 9
    public function macro_renderGrid($__name__ = null, $__params__ = [], $__renderParams__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "name" => $__name__,
            "params" => $__params__,
            "renderParams" => $__renderParams__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 10
            echo "    ";
            $context["datagrid"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->getGrid(($context["name"] ?? null), ($context["params"] ?? null));
            // line 11
            echo "    ";
            if (($context["datagrid"] ?? null)) {
                // line 12
                echo "        ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 12), "get", [0 => "_widgetContainer"], "method", false, false, false, 12) == "dialog")) {
                    // line 13
                    echo "            ";
                    $context["renderParams"] = twig_array_merge(["enableViews" => false], ($context["renderParams"] ?? null));
                    // line 14
                    echo "        ";
                }
                // line 15
                echo "        ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "_grid_view", [], "any", false, true, false, 15), "_disabled", [], "any", true, true, false, 15) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "_grid_view", [], "any", false, false, false, 15), "_disabled", [], "any", false, false, false, 15))) {
                    // line 16
                    echo "            ";
                    $context["renderParams"] = twig_array_merge(($context["renderParams"] ?? null), ["enableViews" => false]);
                    // line 17
                    echo "        ";
                }
                // line 18
                echo "        ";
                $context["metaData"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->getGridMetadata(($context["datagrid"] ?? null), ($context["params"] ?? null));
                // line 19
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "routerEnabled", [], "any", true, true, false, 19)) {
                    // line 20
                    echo "            ";
                    $context["metadataOptions"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["metaData"] ?? null), "options", [], "any", true, true, false, 20)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["metaData"] ?? null), "options", [], "any", false, false, false, 20)) : ([]));
                    // line 21
                    echo "            ";
                    $context["metadataOptions"] = twig_array_merge(($context["metadataOptions"] ?? null), ["routerEnabled" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "routerEnabled", [], "any", false, false, false, 21)]);
                    // line 22
                    echo "            ";
                    $context["metaData"] = twig_array_merge(($context["metaData"] ?? null), ["options" => ($context["metadataOptions"] ?? null)]);
                    // line 23
                    echo "        ";
                }
                // line 24
                echo "        ";
                if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableFullScreenLayout", [], "any", true, true, false, 24) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 25
($context["renderParams"] ?? null), "enableFullScreenLayout", [], "any", false, false, false, 25)) && $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_data_grid.full_screen_layout_enabled"))) {
                    // line 27
                    echo "            ";
                    $context["metaData"] = twig_array_merge(($context["metaData"] ?? null), ["enableFullScreenLayout" => true]);
                    // line 28
                    echo "        ";
                }
                // line 29
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableFloatingHeaderPlugin", [], "any", true, true, false, 29)) {
                    // line 30
                    echo "            ";
                    $context["metaData"] = twig_array_merge(($context["metaData"] ?? null), ["enableFloatingHeaderPlugin" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableFloatingHeaderPlugin", [], "any", false, false, false, 30)]);
                    // line 31
                    echo "        ";
                } else {
                    // line 32
                    echo "            ";
                    $context["metaData"] = twig_array_merge(($context["metaData"] ?? null), ["enableFloatingHeaderPlugin" => $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()]);
                    // line 33
                    echo "        ";
                }
                // line 34
                echo "        ";
                $context["metaData"] = twig_array_merge(($context["metaData"] ?? null), ["responsiveGrids" => ["enable" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 36
($context["renderParams"] ?? null), "responsiveGrids", [], "any", false, true, false, 36), "enable", [], "any", true, true, false, 36)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "responsiveGrids", [], "any", false, true, false, 36), "enable", [], "any", false, false, false, 36), false)) : (false))], "swipeActionsGrid" => ["enable" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 39
($context["renderParams"] ?? null), "swipeActionsGrid", [], "any", false, true, false, 39), "enable", [], "any", true, true, false, 39)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "swipeActionsGrid", [], "any", false, true, false, 39), "enable", [], "any", false, false, false, 39), false)) : (false))], "rowLinkEnabled" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_data_grid.row_link_enabled")]);
                // line 43
                echo "        ";
                $context["data"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->getGridData(($context["datagrid"] ?? null));
                // line 44
                echo "        ";
                $context["gridId"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->generateGridElementId(($context["datagrid"] ?? null));
                // line 45
                echo "        ";
                $context["enableFilters"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableFilters", [], "any", true, true, false, 45)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableFilters", [], "any", false, false, false, 45)) : (true));
                // line 46
                echo "        ";
                if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile() &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableToggleFilters", [], "any", true, true, false, 46))) {
                    // line 47
                    echo "            ";
                    $context["enableToggleFilters"] = false;
                    // line 48
                    echo "        ";
                } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableToggleFilters", [], "any", true, true, false, 48)) {
                    // line 49
                    echo "            ";
                    $context["enableToggleFilters"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableToggleFilters", [], "any", false, false, false, 49);
                    // line 50
                    echo "        ";
                } else {
                    // line 51
                    echo "            ";
                    $context["enableToggleFilters"] = true;
                    // line 52
                    echo "        ";
                }
                // line 53
                echo "        ";
                $context["gridBuildersOptions"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["metaData"] ?? null), "options", [], "any", false, true, false, 53), "gridBuildersOptions", [], "any", true, true, false, 53)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["metaData"] ?? null), "options", [], "any", false, true, false, 53), "gridBuildersOptions", [], "any", false, false, false, 53), [])) : ([]));
                // line 54
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "gridBuildersOptions", [], "any", true, true, false, 54)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "gridBuildersOptions", [], "any", false, false, false, 54), [])) : ([])));
                foreach ($context['_seq'] as $context["builderName"] => $context["buildersOptions"]) {
                    // line 55
                    echo "            ";
                    $context["gridBuildersOptions"] = twig_array_merge(($context["gridBuildersOptions"] ?? null), [                    // line 56
$context["builderName"] => twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["gridBuildersOptions"] ?? null), $context["builderName"], [], "array", true, true, false, 56)) ? (_twig_default_filter((($__internal_compile_0 = ($context["gridBuildersOptions"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[$context["builderName"]] ?? null) : null), [])) : ([])), $context["buildersOptions"])]);
                    // line 58
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['builderName'], $context['buildersOptions'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 59
                echo "        ";
                $context["options"] = ["el" => ("#" .                 // line 60
($context["gridId"] ?? null)), "gridName" => $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 61
($context["datagrid"] ?? null), "name", [], "any", false, false, false, 61), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["datagrid"] ?? null), "scope", [], "any", false, false, false, 61)), "builders" => twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 62
($context["metaData"] ?? null), "jsmodules", [], "any", false, false, false, 62), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "jsmodules", [], "any", true, true, false, 62)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "jsmodules", [], "any", false, false, false, 62), [])) : ([]))), "metadata" =>                 // line 63
($context["metaData"] ?? null), "data" =>                 // line 64
($context["data"] ?? null), "enableFilters" =>                 // line 65
($context["enableFilters"] ?? null), "enableToggleFilters" =>                 // line 66
($context["enableToggleFilters"] ?? null), "filterContainerSelector" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 67
($context["renderParams"] ?? null), "filterContainerSelector", [], "any", true, true, false, 67)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "filterContainerSelector", [], "any", false, false, false, 67)) : (null)), "filtersStateElement" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 68
($context["renderParams"] ?? null), "filtersStateElement", [], "any", true, true, false, 68)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "filtersStateElement", [], "any", false, false, false, 68)) : (null)), "enableViews" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 69
($context["renderParams"] ?? null), "enableViews", [], "any", true, true, false, 69)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "enableViews", [], "any", false, false, false, 69)) : (($context["enableFilters"] ?? null))), "showViewsInNavbar" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 70
($context["renderParams"] ?? null), "showViewsInNavbar", [], "any", true, true, false, 70)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "showViewsInNavbar", [], "any", false, false, false, 70)) : (false)), "showViewsInCustomElement" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 71
($context["renderParams"] ?? null), "showViewsInCustomElement", [], "any", true, true, false, 71)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "showViewsInCustomElement", [], "any", false, false, false, 71)) : (false)), "inputName" => $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridInputName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 72
($context["datagrid"] ?? null), "name", [], "any", false, false, false, 72)), "themeOptions" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 73
($context["renderParams"] ?? null), "themeOptions", [], "any", true, true, false, 73)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "themeOptions", [], "any", false, false, false, 73), [])) : ([])), "toolbarOptions" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 74
($context["renderParams"] ?? null), "toolbarOptions", [], "any", true, true, false, 74)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "toolbarOptions", [], "any", false, false, false, 74), [])) : ([])), "gridViewsOptions" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 75
($context["renderParams"] ?? null), "gridViewsOptions", [], "any", true, true, false, 75)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "gridViewsOptions", [], "any", false, false, false, 75), [])) : ([])), "gridBuildersOptions" =>                 // line 76
($context["gridBuildersOptions"] ?? null)];
                // line 78
                echo "        <div id=\"";
                echo twig_escape_filter($this->env, ($context["gridId"] ?? null), "html", null, true);
                echo "\"
             data-page-component-module=\"";
                // line 79
                echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "datagridComponent", [], "any", true, true, false, 79)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "datagridComponent", [], "any", false, false, false, 79), "orodatagrid/js/app/components/datagrid-component")) : ("orodatagrid/js/app/components/datagrid-component")), "html", null, true);
                echo "\"
             data-page-component-name=\"";
                // line 80
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "gridName", [], "any", false, false, false, 80), "html", null, true);
                echo "\"
             data-page-component-options=\"";
                // line 81
                echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
                echo "\"
             ";
                // line 82
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "cssClass", [], "any", true, true, false, 82)) {
                    echo " class=\"";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "cssClass", [], "any", false, false, false, 82), "html", null, true);
                    echo "\" ";
                }
                echo ">
             ";
                // line 83
                if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "viewLoading", [], "any", true, true, false, 83)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "viewLoading", [], "any", false, false, false, 83), false)) : (false))) {
                    // line 84
                    echo "                 ";
                    $this->loadTemplate("@OroUI/view_loading.html.twig", "@OroDataGrid/macros.html.twig", 84)->display($context);
                    // line 85
                    echo "             ";
                }
                // line 86
                echo "        </div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 96
    public function macro_renderBlankCellValue($__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 97
            echo "    <span ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", true, true, false, 97)) {
                echo "class=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 97), "html", null, true);
                echo "\"";
            }
            // line 98
            echo "          aria-label=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.datagrid.cell.blank.aria_label"), "html", null, true);
            echo "\"
          data-blank-content=\"";
            // line 99
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.datagrid.cell.blank.placeholder"), "html", null, true);
            echo "\"></span>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 99,  261 => 98,  254 => 97,  241 => 96,  230 => 86,  227 => 85,  224 => 84,  222 => 83,  214 => 82,  210 => 81,  206 => 80,  202 => 79,  197 => 78,  195 => 76,  194 => 75,  193 => 74,  192 => 73,  191 => 72,  190 => 71,  189 => 70,  188 => 69,  187 => 68,  186 => 67,  185 => 66,  184 => 65,  183 => 64,  182 => 63,  181 => 62,  180 => 61,  179 => 60,  177 => 59,  171 => 58,  169 => 56,  167 => 55,  162 => 54,  159 => 53,  156 => 52,  153 => 51,  150 => 50,  147 => 49,  144 => 48,  141 => 47,  138 => 46,  135 => 45,  132 => 44,  129 => 43,  127 => 39,  126 => 36,  124 => 34,  121 => 33,  118 => 32,  115 => 31,  112 => 30,  109 => 29,  106 => 28,  103 => 27,  101 => 25,  99 => 24,  96 => 23,  93 => 22,  90 => 21,  87 => 20,  84 => 19,  81 => 18,  78 => 17,  75 => 16,  72 => 15,  69 => 14,  66 => 13,  63 => 12,  60 => 11,  57 => 10,  42 => 9,  37 => 89,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/macros.html.twig");
    }
}
