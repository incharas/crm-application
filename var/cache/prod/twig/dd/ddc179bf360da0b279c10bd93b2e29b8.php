<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/main.scss */
class __TwigTemplate_bf784fa6f5274ec5922635496b5430dc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import 'tinymce/tinymce';
@import 'multi-checkbox-editor';
@import 'inline-editing';
@import 'inline-editing-validation';
@import 'form';
@import 'form-layout';
@import 'inline-editable-wrapper';
@import 'expression-editor';
@import 'desktop/main';
@import 'mobile/main';
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/main.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/main.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/main.scss");
    }
}
