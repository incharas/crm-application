<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/templates/editor/text-editor.html */
class __TwigTemplate_ef39e2dcc5089518debb53eb571f9b36 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"inline-editor__inner\">
    <div class=\"inline-editor__fields\">
        <input name=\"value\" type=\"<%- inputType %>\" value=\"<%- value %>\"
               class=\"form-control\" placeholder=\"<%- placeholder %>\"
               autocomplete=\"off\">
    </div>
    <div class=\"inline-editor__actions\">
        <button class=\"btn btn-icon btn-square-light inline-editor__action-item\" type=\"submit\"
                title=\"<%- _.__('oro.form.inlineEditing.action.save') %>\">
            <span class=\"fa-check\" aria-hidden=\"true\"></span>
        </button>
        <button class=\"btn btn-icon btn-square-light inline-editor__action-item\" data-action=\"cancel\"
                title=\"<%- _.__('oro.form.inlineEditing.action.cancel') %>\">
            <span class=\"fa-ban\" aria-hidden=\"true\"></span>
        </button>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/templates/editor/text-editor.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/templates/editor/text-editor.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/templates/editor/text-editor.html");
    }
}
