<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Lead/Autocomplete/result.html.twig */
class __TwigTemplate_bff55ab39f78d10dffee7253c0bf05eb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<%= highlight(_.escape(name)) %><%if (typeof firstName != 'undefined' && typeof lastName != 'undefined') { %> - <%= highlight(_.escape(firstName)) %> <%= highlight(_.escape(lastName)) %><% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Lead/Autocomplete/result.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Lead/Autocomplete/result.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Lead/Autocomplete/result.html.twig");
    }
}
