<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/form.scss */
class __TwigTemplate_3a66a2cdb028b6283059510b1af30a5f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:math';

.oro-item-collection {
    &input[type='email'],
    &input[type='text'] {
        width: \$form-item-collection-field-width;
    }

    .input-append,
    .input-prepend {
        input[type='email'],
        input[type='text'] {
            width: \$form-item-collection-field-append-width;
        }
    }

    .input-append-sortable,
    .input-prepend-sortable {
        input[type='email'],
        input[type='text'] {
            width: \$form-item-collection-field-append-sortable-width;
        }
    }

    .collection-element-other {
        margin-bottom: \$form-item-collection-element-offset-bottom;

        input[type='email'],
        input[type='text'],
        textarea {
            width: \$form-item-collection-element-width;
        }

        select,
        .selector {
            width: \$form-item-collection-selector-width;
            margin-left: \$form-item-collection-selector-offset-left;
        }

        .select2-container {
            width: \$form-item-collection-select2-width;
        }
    }

    .action-cell {
        text-align: center;
    }
}

.oro-item-collection.not-removable {
    input[type='email'],
    input[type='text'] {
        width: \$form-item-collection-removable-field-width;
    }

    .input-append,
    .input-prepend {
        input[type='email'],
        input[type='text'] {
            width: \$form-item-collection-removable-append-field-width;
        }
    }

    .input-append-sortable,
    .input-prepend-sortable {
        input[type='email'],
        input[type='text'] {
            width: \$form-item-collection-removable-append-sortable-field-width;
        }
    }
}

input[type='text'].hasDatepicker {
    min-width: \$form-item-collection-datepicker-field-min-width;
}

.clearable-input {
    &__container--clear .clearable-input__placeholder-icon {
        font-size: \$icon-font-size;
        top: 0;
        width: \$field-size;
        height: \$field-size;
        line-height: \$field-size;
    }

    &__clear {
        top: 0;
    }

    &__container > input {
        padding-right: \$field-size;
    }
}

.form-flex {
    display: flex;
    width: 100%;
    margin-bottom: 28px;

    > .control-group {
        padding: 0 8px;

        &:first-child {
            padding-left: 0;
        }

        &:last-child {
            padding-right: 0;
        }
    }

    .form-buttons {
        padding-top: 21px;
        white-space: nowrap;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/form.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/form.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/form.scss");
    }
}
