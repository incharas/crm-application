<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityMerge/Form/mergeValue.html.twig */
class __TwigTemplate_335e037398d892ce73b4f310b69b0a25 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["macros"] = $this->macros["macros"] = $this;
        // line 10
        ob_start(function () { return ''; });
        // line 11
        echo "    <span class=\"empty\">-- ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_merge.form.empty"), "html", null, true);
        echo " --</span>
";
        $context["empty_cell"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 13
        echo "
";
        // line 14
        $context["escapeStrategy"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["metadata"] ?? null), "has", [0 => "autoescape"], "method", false, false, false, 14)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["metadata"] ?? null), "get", [0 => "autoescape"], "method", false, false, false, 14)) : (true));
        // line 15
        echo "
";
        // line 16
        if (twig_length_filter($this->env, ($context["convertedValue"] ?? null))) {
            // line 17
            echo "    ";
            if (($context["isListValue"] ?? null)) {
                // line 18
                echo "    ";
                $context["hasVisibleValues"] = false;
                // line 19
                echo "    <ul>
        ";
                // line 20
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["convertedValue"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                    // line 21
                    echo "            ";
                    if (twig_length_filter($this->env, $context["value"])) {
                        // line 22
                        echo "                ";
                        $context["hasVisibleValues"] = true;
                        // line 23
                        echo "                <li>
                    ";
                        // line 24
                        echo twig_call_macro($macros["macros"], "macro_escape", [$context["value"], ($context["escapeStrategy"] ?? null)], 24, $context, $this->getSourceContext());
                        echo "
                </li>
            ";
                    }
                    // line 27
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 28
                echo "
        ";
                // line 29
                if ( !($context["hasVisibleValues"] ?? null)) {
                    // line 30
                    echo "            <li>";
                    echo twig_escape_filter($this->env, ($context["empty_cell"] ?? null), "html", null, true);
                    echo "</li>
        ";
                }
                // line 32
                echo "    </ul>
    ";
            } else {
                // line 34
                echo "        ";
                echo twig_call_macro($macros["macros"], "macro_escape", [($context["convertedValue"] ?? null), ($context["escapeStrategy"] ?? null)], 34, $context, $this->getSourceContext());
                echo "
    ";
            }
        } else {
            // line 37
            echo "    ";
            echo twig_escape_filter($this->env, ($context["empty_cell"] ?? null), "html", null, true);
            echo "
";
        }
        // line 39
        echo "
";
    }

    // line 40
    public function macro_escape($__value__ = null, $__escapeStrategy__ = true, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "escapeStrategy" => $__escapeStrategy__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 41
            echo "    ";
            if (($context["escapeStrategy"] ?? null)) {
                // line 42
                echo "        ";
                echo twig_escape_filter($this->env, ($context["value"] ?? null), ($context["escapeStrategy"] ?? null));
                echo "
    ";
            } else {
                // line 44
                echo "        ";
                echo ($context["value"] ?? null);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityMerge/Form/mergeValue.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  147 => 44,  141 => 42,  138 => 41,  124 => 40,  119 => 39,  113 => 37,  106 => 34,  102 => 32,  96 => 30,  94 => 29,  91 => 28,  85 => 27,  79 => 24,  76 => 23,  73 => 22,  70 => 21,  66 => 20,  63 => 19,  60 => 18,  57 => 17,  55 => 16,  52 => 15,  50 => 14,  47 => 13,  41 => 11,  39 => 10,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityMerge/Form/mergeValue.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityMergeBundle/Resources/views/Form/mergeValue.html.twig");
    }
}
