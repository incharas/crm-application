<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroComment/Comment/form.html.twig */
class __TwigTemplate_89146edb8263fc087b36d3b47dc4ad9c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => $this->getTemplateName()], true);
        // line 2
        echo "
<form method=\"post\" class=\"comment-form\">
    <fieldset class=\"form-horizontal\">
        <div class=\"control-group\">
            <label class=\"comment-message-label\">
                ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "message", [], "any", false, false, false, 7), 'widget');
        echo "
            </label>
        </div>
        ";
        // line 10
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "attachment", [], "any", false, false, false, 10), 'widget');
        echo "
        <% if (attachmentURL) { %>
        <div class=\"control-group\">
            <div class=\"attachment-item\">
                <span class=\"attachment-item__icon fa-paperclip\" aria-hidden=\"true\"></span>
                <a href=\"<%- attachmentURL.url %>\" class=\"no-hash attachment-item__filename\" title=\"<%- attachmentFileName %>\"><%- attachmentFileName %></a>
                <span class=\"attachment-item__file-size\">(<%- attachmentSize %>)</span>
                <button class=\"btn btn-icon btn-square-lighter attachment-item__remove\" type=\"button\" data-role=\"remove\">
                    <span class=\"fa-close\" aria-hidden=\"true\"></span>
                </button>
            </div>
        </div>
        <% } %>
        <div class=\"widget-actions\">
            <button class=\"btn cancel-comment-button\" type=\"reset\">
                <%- _.__('oro.comment.from.button.cancel_comment.label')  %>
            </button>
            <% if (!isNew) { %>
            <button class=\"btn btn-primary\" type=\"submit\">
                <%- _.__('oro.comment.from.button.edit_comment.label') %>
            </button>
            <% } else { %>
            <button class=\"btn btn-primary\" type=\"submit\">
                <%- _.__('oro.comment.from.button.send_comment.label') %>
            </button>
            <% } %>
        </div>
    </fieldset>
</form>
";
    }

    public function getTemplateName()
    {
        return "@OroComment/Comment/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 10,  46 => 7,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroComment/Comment/form.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CommentBundle/Resources/views/Comment/form.html.twig");
    }
}
