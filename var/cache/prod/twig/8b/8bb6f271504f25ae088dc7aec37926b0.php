<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/highlight-text.scss */
class __TwigTemplate_33557941244d830174d0933a49132276 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.highlight {
    &-text {
        @at-root mark#{&} {
            text-decoration: underline;
            color: inherit;
            background-color: \$highlight-text-color;
            font-family: inherit;
            font-weight: font-weight('bold');
            font-size: inherit;
            line-height: inherit;

            margin: 0;
            padding: 0;
        }
    }

    &-element {
        &::first-letter {
            text-transform: none;
        }

        @at-root label#{&} {
            mark {
                font-weight: font-weight('light');
            }
        }
    }

    &-element[class^='fa-'],
    &-element[class*=' fa-'] {
        background-color: \$highlight-text-color;
        box-shadow: \$highlight-box-shadow;

        &.tooltip-icon {
            color: \$primary-550;
        }
    }

    &-element.selector {
        background: \$highlight-text-color;
    }

    &-element > .select2-container {
        background: \$highlight-text-color;
    }

    &-not-found {
        display: none;
    }

    &-items-switcher {
        padding: \$highlight-text-items-switcher-inner-offset;

        &__control {
            font-size: \$base-font-size;

            &:last-child {
                display: none;
            }
        }

        &.highlighted-only & {
            &__control {
                &:first-child {
                    display: none;
                }

                &:last-child {
                    display: block;
                }
            }
        }
    }
}

.select2-container-disabled,
.selector.disabled {
    mark {
        background-color: transparent;
    }
}

input,
textarea,
select[multiple] {
    &.highlight-element:not([disabled]) {
        background: \$highlight-text-color;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/highlight-text.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/highlight-text.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/highlight-text.scss");
    }
}
