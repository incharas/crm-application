<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSegment/Segment/update.html.twig */
class __TwigTemplate_dd66ae6f255a500f2c4409b8a21c0ab1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head_script' => [$this, 'block_head_script'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig"], true);
        // line 3
        $macros["QD"] = $this->macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroSegment/Segment/update.html.twig", 3)->unwrap();
        // line 4
        $macros["segmentQD"] = $this->macros["segmentQD"] = $this->loadTemplate("@OroSegment/macros.html.twig", "@OroSegment/Segment/update.html.twig", 4)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%segment.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "name", [], "any", false, false, false, 6)]]);
        // line 7
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroSegment/Segment/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_head_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $this->displayParentBlock("head_script", $context, $blocks);
        echo "

    ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
    }

    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'stylesheet');
        echo "
    ";
    }

    // line 17
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSegment/Segment/update.html.twig", 18)->unwrap();
        // line 19
        echo "
    ";
        // line 20
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20), "id", [], "any", false, false, false, 20) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("CREATE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20)))) {
            // line 21
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_clone", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["form"] ?? null), "vars", [], "any", false, false, false, 22), "value", [], "any", false, false, false, 22), "id", [], "any", false, false, false, 22)]), "iCss" => "fa-files-o", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.action.clone.button.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.action.clone.button.label")]], 21, $context, $this->getSourceContext());
            // line 26
            echo "
    ";
        }
        // line 28
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 28), "value", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 28), "value", [], "any", false, false, false, 28)))) {
            // line 29
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_segment", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 30
($context["form"] ?? null), "vars", [], "any", false, false, false, 30), "value", [], "any", false, false, false, 30), "id", [], "any", false, false, false, 30)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-segment", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 34
($context["form"] ?? null), "vars", [], "any", false, false, false, 34), "value", [], "any", false, false, false, 34), "id", [], "any", false, false, false, 34), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.entity_label")]], 29, $context, $this->getSourceContext());
            // line 36
            echo "

        ";
            // line 38
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 38, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 40
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_index")], 40, $context, $this->getSourceContext());
        echo "
    ";
        // line 41
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_segment_view", "params" => ["id" => "\$id"]]], 41, $context, $this->getSourceContext());
        // line 45
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_segment_create")) {
            // line 46
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_segment_create"]], 46, $context, $this->getSourceContext()));
            // line 49
            echo "    ";
        }
        // line 50
        echo "    ";
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 50) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "type", [], "any", false, false, false, 50), "name", [], "any", false, false, false, 50) == twig_constant("Oro\\Bundle\\SegmentBundle\\Entity\\SegmentType::TYPE_STATIC")))) {
            // line 51
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveActionButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save and refresh"), "route" => "oro_segment_refresh", "params" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 54
($context["entity"] ?? null), "id", [], "any", false, false, false, 54)]]], 51, $context, $this->getSourceContext()));
            // line 56
            echo "    ";
        }
        // line 57
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 57), "value", [], "any", false, false, false, 57), "id", [], "any", false, false, false, 57) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_segment_update"))) {
            // line 58
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_segment_update", "params" => ["id" => "\$id"]]], 58, $context, $this->getSourceContext()));
            // line 62
            echo "    ";
        }
        // line 63
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 63, $context, $this->getSourceContext());
        echo "
";
    }

    // line 66
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 67), "value", [], "any", false, false, false, 67), "id", [], "any", false, false, false, 67)) {
            // line 68
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 69
($context["form"] ?? null), "vars", [], "any", false, false, false, 69), "value", [], "any", false, false, false, 69), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_segment_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 72
($context["entity"] ?? null), "name", [], "any", false, false, false, 72)];
            // line 74
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 76
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.entity_label")]);
            // line 77
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroSegment/Segment/update.html.twig", 77)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 78
            echo "    ";
        }
    }

    // line 81
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 82
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSegment/Segment/update.html.twig", 82)->unwrap();
        // line 83
        echo "
    ";
        // line 84
        $context["id"] = "segment-profile";
        // line 85
        echo "    ";
        $context["ownerDataBlock"] = ["dataBlocks" => [0 => ["subblocks" => [0 => ["data" => []]]]]];
        // line 92
        echo "
    ";
        // line 93
        $context["ownerDataBlock"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->processForm($this->env, ($context["ownerDataBlock"] ?? null), ($context["form"] ?? null));
        // line 94
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 101
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 101), 'row', ["label" => "oro.segment.name.label"]), 1 =>         // line 102
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 102), 'row', ["label" => "oro.segment.description.label", "attr" => ["class" => "segment-descr"]])]], 1 => ["title" => "", "data" => twig_array_merge([0 =>         // line 113
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 113), 'row', ["label" => "oro.segment.entity.label"]), 1 =>         // line 114
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 114), 'row', ["label" => "oro.segment.type.label"]), 2 =>         // line 115
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recordsLimit", [], "any", false, false, false, 115), 'row', ["label" => "oro.segment.records_limit.label"])], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_1 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 116
($context["ownerDataBlock"] ?? null), "dataBlocks", [], "any", false, false, false, 116)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[0] ?? null) : null), "subblocks", [], "any", false, false, false, 116)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null), "data", [], "any", false, false, false, 116))]]]];
        // line 121
        echo "
    ";
        // line 122
        $context["type"] = "oro_segment";
        // line 123
        echo "    ";
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.form.designer"), "content_attr" => ["id" => (        // line 125
($context["type"] ?? null) . "-designer")], "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.form.columns"), "spanClass" => (        // line 129
($context["type"] ?? null) . "-columns responsive-cell"), "data" => [0 => twig_call_macro($macros["QD"], "macro_query_designer_column_form", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 132
($context["form"] ?? null), "column", [], "any", false, false, false, 132), ["id" => (        // line 133
($context["type"] ?? null) . "-column-form")], [], [0 => "column", 1 => "label", 2 => "sorting", 3 => "action"]], 131, $context, $this->getSourceContext()), 1 => twig_call_macro($macros["QD"], "macro_query_designer_column_list", [["id" => (        // line 138
($context["type"] ?? null) . "-column-list"), "rowId" => (($context["type"] ?? null) . "-column-row")], [0 => "column", 1 => "label", 2 => "sorting", 3 => "action"]], 137, $context, $this->getSourceContext())]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.form.filters"), "spanClass" => (        // line 145
($context["type"] ?? null) . "-filters responsive-cell"), "data" => [0 => twig_call_macro($macros["segmentQD"], "macro_query_designer_condition_builder", [["id" => (        // line 148
($context["type"] ?? null) . "-condition-builder"), "currentSegmentId" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 149
($context["entity"] ?? null), "id", [], "any", true, true, false, 149)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 149), null)) : (null)), "page_limit" => twig_constant("\\Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager::PER_PAGE"), "metadata" =>         // line 151
($context["metadata"] ?? null), "fieldConditionOptions" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 152
($context["form"] ?? null), "vars", [], "any", false, false, false, 152), "field_condition_options", [], "any", false, false, false, 152)]], 147, $context, $this->getSourceContext())]]]]]);
        // line 158
        echo "
    ";
        // line 159
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 160
        echo "
    ";
        // line 161
        $context["data"] = ["formErrors" => ((        // line 162
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 163
($context["dataBlocks"] ?? null), "hiddenData" =>         // line 164
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "definition", [], "any", false, false, false, 164), 'widget')];
        // line 166
        echo "
    ";
        // line 167
        echo twig_call_macro($macros["UI"], "macro_scrollData", [($context["id"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null), ($context["form"] ?? null)], 167, $context, $this->getSourceContext());
        echo "

    ";
        // line 169
        echo twig_call_macro($macros["QD"], "macro_query_designer_column_chain_template", ["column-chain-template"], 169, $context, $this->getSourceContext());
        echo "
    ";
        // line 170
        echo twig_call_macro($macros["segmentQD"], "macro_initJsWidgets", [($context["type"] ?? null), ($context["form"] ?? null), ($context["entities"] ?? null), ($context["metadata"] ?? null)], 170, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroSegment/Segment/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  266 => 170,  262 => 169,  257 => 167,  254 => 166,  252 => 164,  251 => 163,  250 => 162,  249 => 161,  246 => 160,  244 => 159,  241 => 158,  239 => 152,  238 => 151,  237 => 149,  236 => 148,  235 => 145,  234 => 138,  233 => 133,  232 => 132,  231 => 129,  230 => 125,  228 => 123,  226 => 122,  223 => 121,  221 => 116,  220 => 115,  219 => 114,  218 => 113,  217 => 102,  216 => 101,  214 => 94,  212 => 93,  209 => 92,  206 => 85,  204 => 84,  201 => 83,  198 => 82,  194 => 81,  189 => 78,  186 => 77,  183 => 76,  177 => 74,  175 => 72,  174 => 69,  172 => 68,  169 => 67,  165 => 66,  158 => 63,  155 => 62,  152 => 58,  149 => 57,  146 => 56,  144 => 54,  142 => 51,  139 => 50,  136 => 49,  133 => 46,  130 => 45,  128 => 41,  123 => 40,  118 => 38,  114 => 36,  112 => 34,  111 => 30,  109 => 29,  106 => 28,  102 => 26,  100 => 22,  98 => 21,  96 => 20,  93 => 19,  90 => 18,  86 => 17,  79 => 13,  72 => 12,  66 => 10,  62 => 9,  57 => 1,  55 => 7,  53 => 6,  50 => 4,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSegment/Segment/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SegmentBundle/Resources/views/Segment/update.html.twig");
    }
}
