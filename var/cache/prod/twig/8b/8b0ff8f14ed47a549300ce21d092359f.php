<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMessageQueue/Collector/message_queue.html.twig */
class __TwigTemplate_8481b63126978b22768b982f57848edd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'menu' => [$this, 'block_menu'],
            'panel' => [$this, 'block_panel'],
            'sentMessages' => [$this, 'block_sentMessages'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@OroMessageQueue/Collector/message_queue.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_toolbar($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 6
    public function block_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <span class=\"label ";
        echo ((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "sentMessages", [], "any", false, false, false, 7))) ? ("disabled") : (""));
        echo "\">
        <strong>Message Queue</strong>
    </span>
";
    }

    // line 12
    public function block_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "sentMessages", [], "any", false, false, false, 13))) {
            // line 14
            echo "        ";
            $this->displayBlock("sentMessages", $context, $blocks);
            echo "
    ";
        } else {
            // line 16
            echo "        <h2>Message Queue</h2>
        <div class=\"empty\">
            <p>No messages were sent during this request.</p>
        </div>
    ";
        }
    }

    // line 23
    public function block_sentMessages($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    <h2>Sent messages</h2>
    <table>
        <thead>
            <tr>
                <th>#</th>
                <th>Topic</th>
                <th>Message</th>
                <th>Priority</th>
            </tr>
        </thead>
        <tbody>
            ";
        // line 35
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "sentMessages", [], "any", false, false, false, 35));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["sentMessage"]) {
            // line 36
            echo "                <tr>
                    <td>";
            // line 37
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 37), "html", null, true);
            echo "</td>
                    <td>";
            // line 38
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sentMessage"], "topic", [], "any", false, false, false, 38), "html", null, true);
            echo "</td>
                    <td><pre>";
            // line 39
            echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "prettyPrintMessage", [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sentMessage"], "message", [], "any", false, false, false, 39)], "method", false, false, false, 39);
            echo "</pre></td>
                    <td><span title=\"";
            // line 40
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sentMessage"], "priority", [], "any", true, true, false, 40)) ? (print (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sentMessage"], "priority", [], "any", false, false, false, 40), "html", null, true))) : (print ("")));
            echo "\">";
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sentMessage"], "priority", [], "any", true, true, false, 40)) ? (print (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "prettyPrintPriority", [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sentMessage"], "priority", [], "any", false, false, false, 40)], "method", false, false, false, 40), "html", null, true))) : (print ("")));
            echo "</span></td>
                </tr>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sentMessage'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 43
        echo "        </tbody>

    </table>
";
    }

    public function getTemplateName()
    {
        return "@OroMessageQueue/Collector/message_queue.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  158 => 43,  139 => 40,  135 => 39,  131 => 38,  127 => 37,  124 => 36,  107 => 35,  94 => 24,  90 => 23,  81 => 16,  75 => 14,  72 => 13,  68 => 12,  59 => 7,  55 => 6,  49 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMessageQueue/Collector/message_queue.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MessageQueueBundle/Resources/views/Collector/message_queue.html.twig");
    }
}
