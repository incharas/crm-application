<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/login.scss */
class __TwigTemplate_e0cd97d68369bea0192ee396c36436af extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

&.login-page {
    height: 100%;
    overflow: visible;

    .app-page {
        &__central-panel {
            padding-top: 0;
            height: 100%;
        }
    }

    .btn,
    .btn.disabled,
    .btn[disabled],
    .btn.disabled:active,
    .btn[disabled]:active,
    .ui-datepicker .ui-datepicker-buttonpane button {
        font-size: 14px;
        line-height: 38px;
        height: 40px;
        border-radius: 20px;
    }
}

.form-wrapper {
    width: 100%;
    max-width: 100%;
    margin: 0 auto;
    padding: 0 0 30px;

    &__title {
        background: \$primary-300;
        font-size: 17px;
        font-weight: font-weight('bold');
        color: \$primary-inverse;
        text-transform: uppercase;
        display: block;
        margin-bottom: 16px;
        padding: 14px 16px;
    }

    &__inner {
        margin-top: 0;
        padding: 25px 15px;
        max-width: 575px;
        min-width: inherit;
        width: 100%;
    }
}

.form-signin {
    margin: 0 auto;

    &__info {
        margin: 16px 0;

        .alert {
            padding: 8px;
        }
    }

    &__fieldset {
        max-width: none;
    }

    &__footer {
        padding-left: 0;
    }

    &__roles-item {
        display: block;
    }

    .title-box {
        margin: 0 0 5px;

        .title {
            font-size: 20px;
            line-height: 1.5;
        }
    }

    &.form-signin--forgot {
        width: \$signin-forgot-mobile-width;
        max-width: 100%;
        
        input[type='text'] {
            width: 100%;
        }
    }

    .add-on {
        width: auto;
        height: auto;
        line-height: normal;
        text-align: left;
        padding-right: 0;

        + input[type='text'],
        + input[type='email'],
        + input[type='password'] {
            width: 100%;
        }

        + select {
            width: 174px;
            height: 36px;
            box-sizing: border-box;
        }
    }

    .oro-remember-me {
        padding-left: 0;
    }

    span.validation-failed {
        font-size: 13px;
    }

    .input-field-group {
        margin-bottom: 15px;
    }

    .input-append,
    .input-prepend {
        display: block;
    }

    &--reset {
        // stylelint-disable-next-line declaration-no-important
        max-width: 320px !important;

        .add-on {
            width: 120px;
            font-size: 12px;

            + input[type='text'],
            + input[type='email'],
            + input[type='password'] {
                width: 138px;
            }
        }
    }

    &--login.form-row-layout {
        .form-signin__footer {
            padding-left: 0;
        }
    }

    &.form-row-layout {
        .form-signin__oauth {
            padding-left: 0;
        }
    }
}

@media only screen and (min-width: 600px) {
    .form-signin {
        &__roles {
            display: flex;
            justify-content: space-around;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/login.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/login.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/login.scss");
    }
}
