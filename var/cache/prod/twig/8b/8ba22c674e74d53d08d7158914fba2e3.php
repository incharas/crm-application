<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Dashboard/recentCalls.html.twig */
class __TwigTemplate_35fd6573f63354f9d135046c613a32f5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'actions' => [$this, 'block_actions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/grid.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroCall/Dashboard/recentCalls.html.twig", 2)->unwrap();
        // line 3
        $context["gridName"] = "dashboard-recent-calls-grid";
        // line 1
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/grid.html.twig", "@OroCall/Dashboard/recentCalls.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), twig_array_merge(["routerEnabled" => false, "enableFilters" => false], ((        // line 9
array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])))], 6, $context, $this->getSourceContext());
        echo "
";
    }

    // line 12
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $context["actions"] = [0 => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_call_index"), "type" => "link", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.recent_calls.view_all")]];
        // line 18
        echo "
    ";
        // line 19
        $this->displayParentBlock("actions", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCall/Dashboard/recentCalls.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 19,  71 => 18,  68 => 13,  64 => 12,  58 => 9,  56 => 6,  52 => 5,  47 => 1,  45 => 3,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Dashboard/recentCalls.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Dashboard/recentCalls.html.twig");
    }
}
