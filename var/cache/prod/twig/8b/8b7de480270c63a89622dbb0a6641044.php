<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSearch/Search/searchBar.html.twig */
class __TwigTemplate_8cadedd7d52f669a5841f268b1177496 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_search")) {
            // line 2
            echo "    ";
            $context["searchTogglerId"] = uniqid("dropdown-");
            // line 3
            echo "    <div class=\"dropdown header-dropdown-search\">
        <button id=\"";
            // line 4
            echo twig_escape_filter($this->env, ($context["searchTogglerId"] ?? null), "html", null, true);
            echo "\" class=\"dropdown-toggle dropdown-toggle--no-caret\" data-toggle=\"dropdown\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Search"), "html", null, true);
            echo "\"
                type=\"button\"
                aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Search"), "html", null, true);
            echo "\"
                data-prevent-close-on-menu-click=\"true\">
            <span class=\"fa-search\" aria-hidden=\"true\"></span>
        </button>
        <ul class=\"dropdown-menu\" aria-labelledby=\"";
            // line 10
            echo twig_escape_filter($this->env, ($context["searchTogglerId"] ?? null), "html", null, true);
            echo "\"
            data-page-component-view=\"orosearch/js/app/views/search-suggestion-view\">
            ";
            // line 12
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                // line 13
                echo "                <li class=\"nav-header nav-header-title\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Search"), "html", null, true);
                echo "
                    <button class=\"btn-link btn-close fa-close hide-text\" data-role=\"close\">";
                // line 14
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Close"), "html", null, true);
                echo "</button>
                </li>
            ";
            }
            // line 17
            echo "            <li class=\"nav-content\">
                <form method=\"get\" action=\"";
            // line 18
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_search_results");
            echo "\" class=\"search-form\">
                    <div class=\"header-search\">
                        <select data-page-component-module=\"oro/select2-component\" name=\"from\" aria-label=\"";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.quick_search.select.aria_label"), "html", null, true);
            echo "\">
                            <option value=\"\">";
            // line 21
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("All"), "html", null, true);
            echo "</option>
                            ";
            // line 22
            $context["searchEntities"] = [];
            // line 23
            echo "                            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["entities"] ?? null));
            foreach ($context['_seq'] as $context["className"] => $context["alias"]) {
                // line 24
                echo "                                ";
                $context["entityType"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue($context["className"], "label"));
                // line 25
                echo "                                ";
                $context["searchEntities"] = twig_array_merge(($context["searchEntities"] ?? null), [$context["alias"] => ($context["entityType"] ?? null)]);
                // line 26
                echo "                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['className'], $context['alias'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 27
            echo "
                            ";
            // line 28
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(twig_sort_filter($this->env, ($context["searchEntities"] ?? null)));
            foreach ($context['_seq'] as $context["alias"] => $context["name"]) {
                // line 29
                echo "                                <option value=\"";
                echo twig_escape_filter($this->env, $context["alias"], "html", null, true);
                echo "\" data-alias=\"";
                echo twig_escape_filter($this->env, $context["alias"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["name"], "html", null, true);
                echo "</option>
                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['alias'], $context['name'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "                        </select>
                        <div class=\"search-field-container\">
                            <input type=\"text\" class=\"span2 search\" placeholder=\"\" name=\"search\" value=\"\" autocomplete=\"off\" data-autofocus aria-label=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.quick_search.input.aria_label"), "html", null, true);
            echo "\">
                            <ul class=\"search-suggestion-list\" data-role=\"search-suggestion-list\">
                                <li class=\"loading\" data-role=\"loading\">
                                    <div class=\"loader-mask\"><div class=\"loader-frame\"></div></div>
                                </li>
                            </ul>
                            <div class=\"no-data\" data-role=\"fallback\">";
            // line 39
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.quick_search.noresults"), "html", null, true);
            echo "</div>
                        </div>
                        ";
            // line 41
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                // line 42
                echo "                        <button type=\"submit\" class=\"btn btn-primary btn-icon\" aria-label=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Search"), "html", null, true);
                echo "\">
                            <span class=\"fa-search\" aria-hidden=\"true\"></span>
                        </button>
                        ";
            } else {
                // line 46
                echo "                        <button type=\"submit\" class=\"btn btn-primary btn-search\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Search"), "html", null, true);
                echo "</button>
                        ";
            }
            // line 48
            echo "                    </div>
                </form>
            </li>
        </ul>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroSearch/Search/searchBar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  166 => 48,  160 => 46,  152 => 42,  150 => 41,  145 => 39,  136 => 33,  132 => 31,  119 => 29,  115 => 28,  112 => 27,  106 => 26,  103 => 25,  100 => 24,  95 => 23,  93 => 22,  89 => 21,  85 => 20,  80 => 18,  77 => 17,  71 => 14,  66 => 13,  64 => 12,  59 => 10,  52 => 6,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSearch/Search/searchBar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SearchBundle/Resources/views/Search/searchBar.html.twig");
    }
}
