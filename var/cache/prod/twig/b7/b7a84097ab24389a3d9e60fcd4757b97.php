<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/WorkflowDefinition/widget/info.html.twig */
class __TwigTemplate_35dc91519e21d98a1dcde2d3ac44ef2a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["workflowMacros"] = $this->macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/WorkflowDefinition/widget/info.html.twig", 2)->unwrap();
        // line 3
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 7
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.label.label"), (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "label", [], "any", false, false, false, 7), [], "workflows")) . twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["translateLinks"] ?? null), "label", [], "any", false, false, false, 7)], 7, $context, $this->getSourceContext()))], 7, $context, $this->getSourceContext());
        echo "

            ";
        // line 9
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.related_entity.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["entity"] ?? null), "relatedEntity", [], "any", false, false, false, 11), "label"))], 9, $context, $this->getSourceContext());
        // line 12
        echo "
            ";
        // line 13
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.view.workflow.default_step"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["entity"] ?? null), "startStep", [], "any", false, false, false, 15)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startStep", [], "any", false, false, false, 15), "label", [], "any", false, false, false, 15), [], "workflows")) : (""))], 13, $context, $this->getSourceContext());
        // line 16
        echo "
            ";
        // line 17
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.steps_display_ordered.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 19
($context["entity"] ?? null), "stepsDisplayOrdered", [], "any", false, false, false, 19)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No")))], 17, $context, $this->getSourceContext());
        // line 20
        echo "
            ";
        // line 21
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.priority.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 23
($context["entity"] ?? null), "priority", [], "any", false, false, false, 23)], 21, $context, $this->getSourceContext());
        // line 24
        echo "

            ";
        // line 26
        ob_start(function () { return ''; });
        // line 27
        echo "                ";
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveActiveGroups", [], "any", false, false, false, 27))) {
            // line 28
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderList", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveActiveGroups", [], "any", false, false, false, 28)], 28, $context, $this->getSourceContext());
            echo "
                ";
        } else {
            // line 30
            echo "                    ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "
                ";
        }
        // line 32
        echo "            ";
        $context["activeGroups"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 33
        echo "
            ";
        // line 34
        ob_start(function () { return ''; });
        // line 35
        echo "                ";
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveRecordGroups", [], "any", false, false, false, 35))) {
            // line 36
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderList", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "exclusiveRecordGroups", [], "any", false, false, false, 36)], 36, $context, $this->getSourceContext());
            echo "
                ";
        } else {
            // line 38
            echo "                    ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "
                ";
        }
        // line 40
        echo "            ";
        $context["recordGroups"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 41
        echo "
            ";
        // line 42
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.exclusive_active_groups.label"), ($context["activeGroups"] ?? null)], 42, $context, $this->getSourceContext());
        echo "
            ";
        // line 43
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.exclusive_record_groups.label"), ($context["recordGroups"] ?? null)], 43, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/WorkflowDefinition/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 43,  123 => 42,  120 => 41,  117 => 40,  111 => 38,  105 => 36,  102 => 35,  100 => 34,  97 => 33,  94 => 32,  88 => 30,  82 => 28,  79 => 27,  77 => 26,  73 => 24,  71 => 23,  70 => 21,  67 => 20,  65 => 19,  64 => 17,  61 => 16,  59 => 15,  58 => 13,  55 => 12,  53 => 11,  52 => 9,  47 => 7,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/WorkflowDefinition/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/WorkflowDefinition/widget/info.html.twig");
    }
}
