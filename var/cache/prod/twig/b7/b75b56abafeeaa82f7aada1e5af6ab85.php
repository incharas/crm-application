<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/ApplicationMenu/pinButton.html.twig */
class __TwigTemplate_4ce6341418c547f799245ea0775ea9a7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/ApplicationMenu/pinButton.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 4
            echo "
";
            // line 5
            $context["contentProviderContent"] = twig_first($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->getContent(null, [0 => "navigationElements"]));
            // line 6
            echo "<div id=\"bookmark-buttons\">
    <div class=\"navigation clearfix\">
        <div class=\"top-action-box\">
            ";
            // line 9
            ob_start(function () { return ''; });
            // line 10
            echo "            <button class=\"btn btn-icon btn-light-custom favorite-button";
            if (((($__internal_compile_0 = ($context["contentProviderContent"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["favoriteButton"] ?? null) : null) == false)) {
                echo " hide";
            }
            echo "\"
                    type=\"button\"
                    data-title=\"";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->renderSerialized(), "html", null, true);
            echo "\"
                    data-title-rendered=\"";
            // line 13
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render(), "html", null, true);
            echo "\"
                    data-title-rendered-short=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->renderShort(), "html", null, true);
            echo "\"
                    title=\"";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.favorites.button.title"), "html", null, true);
            echo "\"
                    ";
            // line 16
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oronavigation/js/app/components/favorite-component", "options" => ["dataSource" => "#favorite-content [data-data]", "buttonOptions" => ["navigationElementType" => "favoriteButton"], "tabOptions" => ["el" => "#favorite-content", "listSelector" => ".extra-list", "fallbackSelector" => ".no-data"]]]], 16, $context, $this->getSourceContext());
            // line 29
            echo ">
                        <span class=\"fa-star-o\"></span>";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.favorites.button"), "html", null, true);
            // line 32
            echo "</button>
            <button class=\"btn btn-icon btn-light-custom minimize-button";
            // line 33
            if (((($__internal_compile_1 = ($context["contentProviderContent"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["pinButton"] ?? null) : null) == false)) {
                echo " hide";
            }
            echo "\"
                    type=\"button\"
                    data-title=\"";
            // line 35
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->renderSerialized(), "html", null, true);
            echo "\"
                    data-title-rendered=\"";
            // line 36
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render(), "html", null, true);
            echo "\"
                    data-title-rendered-short=\"";
            // line 37
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->renderShort(), "html", null, true);
            echo "\"
                    title=\"";
            // line 38
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.pins.button.title"), "html", null, true);
            echo "\"
                    ";
            // line 39
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oronavigation/js/app/components/pin-component", "options" => ["dataSource" => "#pinbar [data-data]", "buttonOptions" => ["navigationElementType" => "pinButton"], "dropdownOptions" => ["el" => "#pinbar .show-more", "listSelector" => "[data-role=\"pin-bar-dropdown-items\"]"], "barOptions" => ["el" => "#pinbar", "listSelector" => ".list-bar ul", "fallbackSelector" => ".pin-bar-empty"]]]], 39, $context, $this->getSourceContext());
            // line 56
            echo ">
                        <span class=\"fa-thumb-tack\"></span>";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.pins.button"), "html", null, true);
            // line 59
            echo "</button>
            ";
            $___internal_parse_88_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 9
            echo twig_spaceless($___internal_parse_88_);
            // line 61
            echo "        </div>
    </div>
</div>

";
        }
        // line 66
        echo "<script ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oronavigation/js/app/views/page-state-view", "name" => "page-state-component", "options" => ["el" => "#container", "keepElement" => true]]], 66, $context, $this->getSourceContext());
        // line 73
        echo "></script>
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/ApplicationMenu/pinButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 73,  133 => 66,  126 => 61,  124 => 9,  120 => 59,  118 => 58,  115 => 56,  113 => 39,  109 => 38,  105 => 37,  101 => 36,  97 => 35,  90 => 33,  87 => 32,  85 => 31,  82 => 29,  80 => 16,  76 => 15,  72 => 14,  68 => 13,  64 => 12,  56 => 10,  54 => 9,  49 => 6,  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/ApplicationMenu/pinButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/ApplicationMenu/pinButton.html.twig");
    }
}
