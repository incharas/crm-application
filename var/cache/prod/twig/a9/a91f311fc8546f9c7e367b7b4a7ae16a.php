<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/not-blank-group.js */
class __TwigTemplate_04bf21bed64b061022f92c2658af158f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import __ from 'orotranslation/js/translator';

const defaultParam = {
    message: 'Please add at least one item',
    selector: 'input'
};

/**
 * @export oroform/js/validator/not-blank-group
 */
export default [
    'NotBlankGroup',
    function(value, element, param) {
        param = Object.assign({}, defaultParam, param);
        const collectionElement = element.closest('[data-prototype-name]');
        return [...collectionElement.querySelectorAll(param.selector)].some(item => item.value);
    },
    function(param) {
        param = Object.assign({}, defaultParam, param);
        return __(param.message);
    }
];
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/not-blank-group.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/not-blank-group.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/not-blank-group.js");
    }
}
