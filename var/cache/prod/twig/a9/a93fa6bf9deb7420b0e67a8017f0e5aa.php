<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChannel/Form/fields.html.twig */
class __TwigTemplate_e9cd0d6897cbe55979fc00b72224a68d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_channel_datasource_form_row' => [$this, 'block_oro_channel_datasource_form_row'],
            'oro_channel_datasource_form_widget' => [$this, 'block_oro_channel_datasource_form_widget'],
            'oro_channel_entity_choice_form_row' => [$this, 'block_oro_channel_entity_choice_form_row'],
            '_oro_channel_form_channelType_widget' => [$this, 'block__oro_channel_form_channelType_widget'],
            'oro_multiple_entity_js_channel_aware' => [$this, 'block_oro_multiple_entity_js_channel_aware'],
            'oro_entity_create_or_select_inline_js_autocomplete_channel_aware' => [$this, 'block_oro_entity_create_or_select_inline_js_autocomplete_channel_aware'],
            'oro_entity_create_or_select_inline_js_grid_channel_aware' => [$this, 'block_oro_entity_create_or_select_inline_js_grid_channel_aware'],
            'oro_entity_create_or_select_inline_js_channel_aware' => [$this, 'block_oro_entity_create_or_select_inline_js_channel_aware'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_channel_datasource_form_row', $context, $blocks);
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('oro_channel_datasource_form_widget', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('oro_channel_entity_choice_form_row', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('_oro_channel_form_channelType_widget', $context, $blocks);
        // line 37
        echo "
";
        // line 38
        $this->displayBlock('oro_multiple_entity_js_channel_aware', $context, $blocks);
        // line 57
        echo "
";
        // line 58
        $this->displayBlock('oro_entity_create_or_select_inline_js_autocomplete_channel_aware', $context, $blocks);
        // line 61
        echo "
";
        // line 62
        $this->displayBlock('oro_entity_create_or_select_inline_js_grid_channel_aware', $context, $blocks);
        // line 65
        echo "
";
        // line 66
        $this->displayBlock('oro_entity_create_or_select_inline_js_channel_aware', $context, $blocks);
    }

    // line 1
    public function block_oro_channel_datasource_form_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        echo "
";
    }

    // line 5
    public function block_oro_channel_datasource_form_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 6)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 6) . " ")) : ("")) . "orocrm-channel-datasource-field")]);
        // line 7
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ($context["attr"] ?? null)]);
        echo "

    ";
        // line 9
        $context["options"] = ["el" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 10
($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)), "idEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["form"] ?? null), "identifier", [], "any", false, false, false, 11), "vars", [], "any", false, false, false, 11), "id", [], "any", false, false, false, 11)), "dataEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["form"] ?? null), "data", [], "any", false, false, false, 12), "vars", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12)), "typeEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["form"] ?? null), "type", [], "any", false, false, false, 13), "vars", [], "any", false, false, false, 13), "id", [], "any", false, false, false, 13)), "nameEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 14
($context["form"] ?? null), "name", [], "any", false, false, false, 14), "vars", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14)), "channelNameEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["form"] ?? null), "parent", [], "any", false, false, false, 15), "name", [], "any", false, false, false, 15), "vars", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15))];
        // line 17
        echo "
    <div data-page-component-module=\"orochannel/js/app/components/integration-widget\"
         data-page-component-options=\"";
        // line 19
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"></div>
";
    }

    // line 22
    public function block_oro_channel_entity_choice_form_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
";
    }

    // line 26
    public function block__oro_channel_form_channelType_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 27), "disabled", [], "any", false, false, false, 27)) {
            // line 28
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["choices"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
                // line 29
                echo "            ";
                if (Symfony\Bridge\Twig\Extension\twig_is_selected_choice($context["choice"], ($context["value"] ?? null))) {
                    // line 30
                    echo "                <span class=\"inline-text\" id=\"";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 30), "id", [], "any", false, false, false, 30), "html", null, true);
                    echo "\" data-value=\"";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 30), "value", [], "any", false, false, false, 30), "html", null, true);
                    echo "\" data-disabled>";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 30)), "html", null, true);
                    echo "</span>
            ";
                }
                // line 32
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 33
            echo "    ";
        } else {
            // line 34
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
    ";
        }
    }

    // line 38
    public function block_oro_multiple_entity_js_channel_aware($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "    var \$channelSelector = \$('select[name=\"' + ";
        echo json_encode(($context["channel_field_name"] ?? null));
        echo " + '\"]'),
        presetChannelId = ";
        // line 40
        echo json_encode(($context["channel_id"] ?? null));
        echo ",
        changeHandler = function(\$el) {
            var channelIds = [\$el.val()];

            if (presetChannelId) {
                channelIds.push(presetChannelId);
            }

            widget.options.selectionRouteParams.channelIds = channelIds.join(',');
        };

        \$channelSelector.change(function() {
            changeHandler(\$(this));
            widget.removeAll();
        });
        changeHandler(\$channelSelector);
";
    }

    // line 58
    public function block_oro_entity_create_or_select_inline_js_autocomplete_channel_aware($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    ";
        $this->displayBlock("oro_entity_create_or_select_inline_js_channel_aware", $context, $blocks);
        echo "
";
    }

    // line 62
    public function block_oro_entity_create_or_select_inline_js_grid_channel_aware($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "    ";
        $this->displayBlock("oro_entity_create_or_select_inline_js_channel_aware", $context, $blocks);
        echo "
";
    }

    // line 66
    public function block_oro_entity_create_or_select_inline_js_channel_aware($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/Form/fields.html.twig", 67)->unwrap();
        // line 68
        echo "    ";
        if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 68), "configs", [], "any", false, true, false, 68), "async_dialogs", [], "any", true, true, false, 68)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 68), "configs", [], "any", false, true, false, 68), "async_dialogs", [], "any", false, false, false, 68), false)) : (false)) === true)) {
            // line 69
            echo "        ";
            $context["asyncNameSegment"] = "async-";
            // line 70
            echo "    ";
        }
        // line 71
        echo "    ";
        $context["urlParts"] = ["grid" => ["route" => "oro_datagrid_widget", "parameters" => ["gridName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 75
($context["form"] ?? null), "vars", [], "any", false, false, false, 75), "grid_name", [], "any", false, false, false, 75), "params" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 76
($context["form"] ?? null), "vars", [], "any", false, false, false, 76), "grid_parameters", [], "any", false, false, false, 76), "renderParams" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 77
($context["form"] ?? null), "vars", [], "any", false, false, false, 77), "grid_render_parameters", [], "any", false, false, false, 77)]]];
        // line 81
        echo "
    ";
        // line 82
        if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 82), "create_enabled", [], "any", true, true, false, 82)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 82), "create_enabled", [], "any", false, false, false, 82), false)) : (false)) === true)) {
            // line 83
            echo "        ";
            $context["urlParts"] = twig_array_merge(($context["urlParts"] ?? null), ["create" => ["route" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 85
($context["form"] ?? null), "vars", [], "any", false, false, false, 85), "create_form_route", [], "any", false, false, false, 85), "parameters" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 86
($context["form"] ?? null), "vars", [], "any", false, false, false, 86), "create_form_route_parameters", [], "any", false, false, false, 86)]]);
            // line 89
            echo "    ";
        }
        // line 90
        echo "    <div ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => (("orochannel/js/app/components/select-create-inline-type-" . ((        // line 91
array_key_exists("asyncNameSegment", $context)) ? (_twig_default_filter(($context["asyncNameSegment"] ?? null), "")) : (""))) . "component"), "options" => ["_sourceElement" => (("#" .         // line 93
($context["id"] ?? null)) . "-el"), "inputSelector" => ("#" .         // line 94
($context["id"] ?? null)), "entityLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 95
($context["label"] ?? null)), "urlParts" =>         // line 96
($context["urlParts"] ?? null), "existingEntityGridId" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 97
($context["form"] ?? null), "vars", [], "any", false, true, false, 97), "existing_entity_grid_id", [], "any", true, true, false, 97)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 97), "existing_entity_grid_id", [], "any", false, false, false, 97), "id")) : ("id")), "createEnabled" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 98
($context["form"] ?? null), "vars", [], "any", false, true, false, 98), "create_enabled", [], "any", true, true, false, 98)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 98), "create_enabled", [], "any", false, false, false, 98), false)) : (false)), "channelFieldSelector" => (("select[name=\"" .         // line 99
($context["channel_field_name"] ?? null)) . "\"]"), "channelRequired" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 100
($context["form"] ?? null), "vars", [], "any", false, true, false, 100), "channel_required", [], "any", true, true, false, 100)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 100), "channel_required", [], "any", false, false, false, 100), false)) : (false)), "presetChannelId" =>         // line 101
($context["channel_id"] ?? null)]]], 90, $context, $this->getSourceContext());
        // line 103
        echo " style=\"display: none\"></div>
";
    }

    public function getTemplateName()
    {
        return "@OroChannel/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  279 => 103,  277 => 101,  276 => 100,  275 => 99,  274 => 98,  273 => 97,  272 => 96,  271 => 95,  270 => 94,  269 => 93,  268 => 91,  266 => 90,  263 => 89,  261 => 86,  260 => 85,  258 => 83,  256 => 82,  253 => 81,  251 => 77,  250 => 76,  249 => 75,  247 => 71,  244 => 70,  241 => 69,  238 => 68,  235 => 67,  231 => 66,  224 => 63,  220 => 62,  213 => 59,  209 => 58,  188 => 40,  183 => 39,  179 => 38,  171 => 34,  168 => 33,  162 => 32,  152 => 30,  149 => 29,  144 => 28,  141 => 27,  137 => 26,  130 => 23,  126 => 22,  120 => 19,  116 => 17,  114 => 15,  113 => 14,  112 => 13,  111 => 12,  110 => 11,  109 => 10,  108 => 9,  102 => 7,  99 => 6,  95 => 5,  88 => 2,  84 => 1,  80 => 66,  77 => 65,  75 => 62,  72 => 61,  70 => 58,  67 => 57,  65 => 38,  62 => 37,  60 => 26,  57 => 25,  55 => 22,  52 => 21,  50 => 5,  47 => 4,  45 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChannel/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ChannelBundle/Resources/views/Form/fields.html.twig");
    }
}
