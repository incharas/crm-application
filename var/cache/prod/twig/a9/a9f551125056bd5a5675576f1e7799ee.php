<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/validation-message-handler/datetime-validation-message-handler-view.js */
class __TwigTemplate_a1eb1d195442f87dd74d90d6179abc4e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const AbstractValidationMessageHandlerView =
        require('oroform/js/app/views/validation-message-handler/abstract-validation-message-handler-view');

    const DateTimeValidationMessageHandlerView = AbstractValidationMessageHandlerView.extend({
        /**
         * @inheritdoc
         */
        constructor: function DateTimeValidationMessageHandlerView(options) {
            DateTimeValidationMessageHandlerView.__super__.constructor.call(this, options);
        },

        delegateEvents: function() {
            DateTimeValidationMessageHandlerView.__super__.delegateEvents.call(this);
            const datepickerEvents = ['datepicker:dialogHide', 'datepicker:dialogReposition'].map(eventName => {
                return eventName + this.eventNamespace();
            }, this);
            this.\$el.parent().find('.datepicker-input').on(datepickerEvents.join(' '),
                this.onDatepickerDialogReposition.bind(this));
            const timepickerEvents = ['showTimepicker', 'hideTimepicker'].map(eventName => {
                return eventName + this.eventNamespace();
            }, this);

            this.\$el.parent().find('.timepicker-input').on(timepickerEvents.join(' '),
                this.onTimepickerDialogToggle.bind(this));
        },

        undelegateEvents: function() {
            \$(this.el).parent().find('.datepicker-input').off(this.eventNamespace());
            \$(this.el).parent().find('.timepicker-input').off(this.eventNamespace());

            DateTimeValidationMessageHandlerView.__super__.undelegateEvents.call(this);
        },

        isActive: function() {
            return this.isDatepickerActive() || this.isTimepickerActive();
        },

        isDatepickerActive: function() {
            const \$input = \$(this.el).parent().find('.datepicker-input');

            return \$input.data('datepicker').dpDiv.is(':visible') && \$input.is(\$.datepicker._lastInput) &&
                \$input.hasClass('ui-datepicker-dialog-is-below');
        },

        isTimepickerActive: function() {
            const {list} = this.\$el.parent().find('.timepicker-input')[0].timepickerObj || {};

            return list && list.is(':visible') && !list.hasClass('ui-timepicker-positioned-top');
        },

        onDatepickerDialogReposition: function(e, position) {
            this.active = position === 'below';
            this.update();
        },

        onTimepickerDialogToggle: function(e) {
            this.active = this.isTimepickerActive();
            this.update();
        },

        getPopperReferenceElement: function() {
            return this.\$el.parent();
        }
    }, {
        test: function(element) {
            return \$(element).data('type') === 'datetime';
        }
    });

    return DateTimeValidationMessageHandlerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/validation-message-handler/datetime-validation-message-handler-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/validation-message-handler/datetime-validation-message-handler-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/validation-message-handler/datetime-validation-message-handler-view.js");
    }
}
