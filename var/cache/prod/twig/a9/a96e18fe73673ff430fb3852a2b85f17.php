<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTask/Task/js/activityItemTemplate.html.twig */
class __TwigTemplate_c81ed49c2a10b66ce74255a509b69c8a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'activityDetails' => [$this, 'block_activityDetails'],
            'activityShortMessage' => [$this, 'block_activityShortMessage'],
            'activityActions' => [$this, 'block_activityActions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroTask/Task/js/activityItemTemplate.html.twig", 2)->unwrap();
        // line 4
        $context["entityClass"] = "Oro\\Bundle\\TaskBundle\\Entity\\Task";
        // line 5
        $context["entityName"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["entityClass"] ?? null), "label"));
        // line 1
        $this->parent = $this->loadTemplate("@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", "@OroTask/Task/js/activityItemTemplate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_activityDetails($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, ($context["entityName"] ?? null), "html", null, true);
        echo "
    <% var template = (verb == 'create')
        ? ";
        // line 10
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.activity_item.created_by"));
        echo "
        : ";
        // line 11
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.activity_item.changed_by"));
        echo ";
    %>
    <%= _.template(template, { interpolate: /\\{\\{(.+?)\\}\\}/g })({
        user: owner_url ? '<a class=\"user\" href=\"' + owner_url + '\">' +  _.escape(owner) + '</a>' :  '<span class=\"user\">' + _.escape(owner) + '</span>',
        date: '<i class=\"date\">' + createdAt + '</i>',
        editor: editor_url ? '<a class=\"user\" href=\"' + editor_url + '\">' +  _.escape(editor) + '</a>' : _.escape(editor),
        editor_date: '<i class=\"date\">' + updatedAt + '</i>'
    }) %>
";
    }

    // line 21
    public function block_activityShortMessage($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    <% if (!_.isUndefined(data.statusId) && data.statusId) { %>
        <div class=\"activity-short-message\">
            <% if (data.statusId === 'closed') { %>
                <div class=\"badge badge-pill badge-disabled status-disabled\"><span class=\"icon-status-disabled fa-circle\" aria-hidden=\"true\"></span>
                    <%- data.statusName %></div>
            <% } else if (data.statusId === 'in_progress') { %>
                <div class=\"badge badge-pill badge-tentatively status-tentatively\"><span class=\"icon-status-tentatively fa-circle\" aria-hidden=\"true\"></span>
                    <%- data.statusName %></div>
            <% } else { %>
                <div class=\"badge badge-pill badge-enabled status-enabled\"><span class=\"icon-status-enabled fa-circle\" aria-hidden=\"true\"></span>
                    <%- data.statusName %></div>
            <% } %>
        </div>
    <% } %>
    ";
        // line 36
        $this->displayParentBlock("activityShortMessage", $context, $blocks);
        echo "
";
    }

    // line 39
    public function block_activityActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroTask/Task/js/activityItemTemplate.html.twig", 40)->unwrap();
        // line 41
        echo "
    ";
        // line 42
        ob_start(function () { return ''; });
        // line 43
        echo "        ";
        // line 44
        echo "        <% if (editable) { %>
            ";
        // line 45
        echo twig_call_macro($macros["AC"], "macro_activity_context_link", [], 45, $context, $this->getSourceContext());
        echo "
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 48
        echo "    ";
        $context["actions"] = [0 => ($context["action"] ?? null)];
        // line 49
        echo "
    ";
        // line 50
        ob_start(function () { return ''; });
        // line 51
        echo "        <a href=\"<%- routing.generate('oro_task_view', {'id': relatedActivityId}) %>\"
           class=\"dropdown-item\"
           title=\"";
        // line 53
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.view_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\"><span
                class=\"fa-eye hide-text\" aria-hidden=\"true\">";
        // line 54
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.view_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 55
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.view_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 58
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 59
        echo "
    ";
        // line 60
        ob_start(function () { return ''; });
        // line 61
        echo "        <% if (editable) { %>
        <a href=\"#\" class=\"dropdown-item action item-edit-button\"
           title=\"";
        // line 63
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.update_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\"
           data-action-extra-options=\"";
        // line 64
        echo twig_escape_filter($this->env, json_encode(["dialogOptions" => ["width" => 1000]]), "html", null, true);
        echo "\">
            <span class=\"fa-pencil-square-o hide-text\" aria-hidden=\"true\">";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.update_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.update_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 70
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 71
        echo "
    ";
        // line 72
        ob_start(function () { return ''; });
        // line 73
        echo "        <% if (removable) { %>
        <a href=\"#\" class=\"dropdown-item action item-remove-button\"
           title=\"";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.delete_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\">
            <span class=\"fa-trash-o hide-text\" aria-hidden=\"true\">";
        // line 76
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.delete_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 77
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.delete_task", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 81
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 82
        echo "
    ";
        // line 83
        $this->displayParentBlock("activityActions", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTask/Task/js/activityItemTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  221 => 83,  218 => 82,  215 => 81,  208 => 77,  204 => 76,  200 => 75,  196 => 73,  194 => 72,  191 => 71,  188 => 70,  181 => 66,  177 => 65,  173 => 64,  169 => 63,  165 => 61,  163 => 60,  160 => 59,  157 => 58,  151 => 55,  147 => 54,  143 => 53,  139 => 51,  137 => 50,  134 => 49,  131 => 48,  125 => 45,  122 => 44,  120 => 43,  118 => 42,  115 => 41,  112 => 40,  108 => 39,  102 => 36,  86 => 22,  82 => 21,  69 => 11,  65 => 10,  59 => 8,  55 => 7,  50 => 1,  48 => 5,  46 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTask/Task/js/activityItemTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-task-bundle/Resources/views/Task/js/activityItemTemplate.html.twig");
    }
}
