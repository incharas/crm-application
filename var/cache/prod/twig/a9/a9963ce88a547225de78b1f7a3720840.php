<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/variables/pin-bar.scss */
class __TwigTemplate_dc751d951a9d876b7392c8415640e8fc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:color';

\$pin-item-active-bg-color: \$extra-200 !default;
\$pin-item-close-color: \$primary-700 !default;
\$pin-item-close-hover-color: color.adjust(\$pin-item-close-color, \$lightness: -8%) !default;
\$pin-item-outdated-color: \$warning-dark !default;
\$pin-item-outdated-hover-color: color.adjust(\$pin-item-outdated-color, \$lightness: -8%) !default;

\$pin-item-show-more-menu-width: 20px !default;
\$pin-item-show-more-menu-offset: -8px !default;
\$pin-item-show-more-menu-arrow-end: 12px !default;

\$pin-show-more-gradient: linear-gradient(to left, \$primary-900, rgba(\$primary-900, 0)) !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/variables/pin-bar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/variables/pin-bar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/variables/pin-bar.scss");
    }
}
