<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/actions/dialog/move.html.twig */
class __TwigTemplate_f80bdccf7cd5166ad8279d392d2a3932 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("saved", $context) && ($context["saved"] ?? null))) {
            // line 2
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.jstree.move.success.label", ["%nodes%" =>             // line 4
($context["nodesName"] ?? null)]), "triggerSuccess" => true, "trigger" => [0 => ["eventBroker" => "widget", "name" => "formSave", "args" => [0 =>             // line 9
($context["changed"] ?? null)]]], "remove" => true]];
            // line 14
            echo "
    ";
            // line 15
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
";
        } else {
            // line 17
            echo "    ";
            $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => $this->getTemplateName()], true);
            // line 18
            echo "    <div class=\"widget-content\">
        <div class=\"form-container\">
            ";
            // line 20
            $context["formAction"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 20), "uri", [], "any", false, false, false, 20);
            // line 21
            echo "            <form id=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "id", [], "any", false, false, false, 21), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "name", [], "any", false, false, false, 21), "html", null, true);
            echo "\" action=\"";
            echo twig_escape_filter($this->env, ($context["formAction"] ?? null), "html", null, true);
            echo "\" method=\"post\">
                <fieldset class=\"form form-horizontal\">
                    ";
            // line 23
            $context["formErrors"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            // line 24
            echo "
                    ";
            // line 25
            if (($context["formErrors"] ?? null)) {
                // line 26
                echo "                        <div class=\"alert alert-error alert-dismissible\" role=\"alert\">
                            <button class=\"close\" type=\"button\" data-dismiss=\"alert\" aria-label=\"";
                // line 27
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Close"), "html", null, true);
                echo "\"><span aria-hidden=\"true\">&times;</span></button>
                            ";
                // line 28
                echo ($context["formErrors"] ?? null);
                echo "
                        </div>
                    ";
            }
            // line 31
            echo "                    <div class=\"span6\">
                        <div class=\"control-group control-group-hidden\">
                            <div class=\"control-label wrap\">";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.jstree.move.source.label"), "html", null, true);
            echo "</div>
                            <div class=\"controls\">
                                ";
            // line 35
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 35), "value", [], "any", false, false, false, 35), "source", [], "any", false, false, false, 35));
            foreach ($context['_seq'] as $context["_key"] => $context["source"]) {
                // line 36
                echo "                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["source"], "parents", [], "any", false, false, false, 36));
                foreach ($context['_seq'] as $context["_key"] => $context["parent"]) {
                    // line 37
                    echo "                                        ";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["parent"], "label", [], "any", false, false, false, 37), "html", null, true);
                    echo "&nbsp;&gt;&nbsp;
                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['parent'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 39
                echo "                                    ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["source"], "label", [], "any", false, false, false, 39), "html", null, true);
                echo "
                                    <br />
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['source'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 42
            echo "                            </div>
                        </div>
                        ";
            // line 44
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
                    </div>

                    <div class=\"widget-actions form-actions\" style=\"display: none;\">
                        <button class=\"btn\" type=\"reset\">";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
            echo "</button>
                        <button class=\"btn btn-success\" type=\"submit\">";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "html", null, true);
            echo "</button>
                    </div>
                </fieldset>
            </form>
            ";
            // line 53
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroUI/actions/dialog/move.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  150 => 53,  143 => 49,  139 => 48,  132 => 44,  128 => 42,  118 => 39,  109 => 37,  104 => 36,  100 => 35,  95 => 33,  91 => 31,  85 => 28,  81 => 27,  78 => 26,  76 => 25,  73 => 24,  71 => 23,  61 => 21,  59 => 20,  55 => 18,  52 => 17,  47 => 15,  44 => 14,  42 => 9,  41 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/actions/dialog/move.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/actions/dialog/move.html.twig");
    }
}
