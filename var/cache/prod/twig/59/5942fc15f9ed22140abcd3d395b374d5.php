<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCron/macros.html.twig */
class __TwigTemplate_3f53f6dfd6c9155b8434aac1b5f98e59 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_scheduleIntervalsInfo($__schedules__ = null, $__labels__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "schedules" => $__schedules__,
            "labels" => $__labels__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $context["defaultLabels"] = ["wasActivated" => "oro.cron.schedule_interval.was_activated", "activeNow" => "oro.cron.schedule_interval.active_now", "notActiveNow" => "oro.cron.schedule_interval.not_active_now", "willBeActivated" => "oro.cron.schedule_interval.will_be_acitivated", "wasDeactivated" => "oro.cron.schedule_interval.was_deactivated", "willBeDeactivated" => "oro.cron.schedule_interval.will_be_deacitivated"];
            // line 10
            echo "    ";
            $context["labels"] = twig_array_merge(($context["defaultLabels"] ?? null), ($context["labels"] ?? null));
            // line 11
            echo "    ";
            $context["now"] = twig_date_converter($this->env, "now", "UTC");
            // line 12
            echo "    <ul class=\"schedule-list\">
        ";
            // line 13
            $context["activityShown"] = false;
            // line 14
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["schedules"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["schedule"]) {
                // line 15
                echo "            ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "activeAt", [], "any", false, false, false, 15) < ($context["now"] ?? null))) {
                    // line 16
                    echo "                ";
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "activeAt", [], "any", false, false, false, 16)) {
                        // line 17
                        echo "                    <li>
                        ";
                        // line 18
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["labels"] ?? null), "wasActivated", [], "any", false, false, false, 18), ["%date%" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "activeAt", [], "any", false, false, false, 18))]), "html", null, true);
                        echo "
                    </li>
                ";
                    }
                    // line 21
                    echo "
                ";
                    // line 22
                    if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "deactivateAt", [], "any", false, false, false, 22) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "deactivateAt", [], "any", false, false, false, 22) > ($context["now"] ?? null)))) {
                        // line 23
                        echo "                    ";
                        $context["activityShown"] = true;
                        // line 24
                        echo "                    <li>
                        ";
                        // line 25
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["labels"] ?? null), "activeNow", [], "any", false, false, false, 25)), "html", null, true);
                        echo "
                    </li>
                ";
                    }
                    // line 28
                    echo "            ";
                }
                // line 29
                echo "
            ";
                // line 30
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "activeAt", [], "any", false, false, false, 30) > ($context["now"] ?? null))) {
                    // line 31
                    echo "                ";
                    if ( !($context["activityShown"] ?? null)) {
                        // line 32
                        echo "                    ";
                        $context["activityShown"] = true;
                        // line 33
                        echo "                    <li>
                        ";
                        // line 34
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["labels"] ?? null), "notActiveNow", [], "any", false, false, false, 34)), "html", null, true);
                        echo "
                    </li>
                ";
                    }
                    // line 37
                    echo "                <li>
                    ";
                    // line 38
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["labels"] ?? null), "willBeActivated", [], "any", false, false, false, 38), ["%date%" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "activeAt", [], "any", false, false, false, 38))]), "html", null, true);
                    echo "
                </li>
            ";
                }
                // line 41
                echo "
            ";
                // line 42
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "deactivateAt", [], "any", false, false, false, 42)) {
                    // line 43
                    echo "                ";
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "deactivateAt", [], "any", false, false, false, 43) < ($context["now"] ?? null))) {
                        // line 44
                        echo "                    <li>
                        ";
                        // line 45
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["labels"] ?? null), "wasDeactivated", [], "any", false, false, false, 45), ["%date%" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "deactivateAt", [], "any", false, false, false, 45))]), "html", null, true);
                        echo "
                    </li>
                ";
                    } else {
                        // line 48
                        echo "                    <li>
                        ";
                        // line 49
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["labels"] ?? null), "willBeDeactivated", [], "any", false, false, false, 49), ["%date%" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["schedule"], "deactivateAt", [], "any", false, false, false, 49))]), "html", null, true);
                        echo "
                    </li>
                ";
                    }
                    // line 52
                    echo "            ";
                }
                // line 53
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['schedule'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 54
            echo "
        ";
            // line 55
            if ( !($context["activityShown"] ?? null)) {
                // line 56
                echo "            <li>
                ";
                // line 57
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["labels"] ?? null), "notActiveNow", [], "any", false, false, false, 57)), "html", null, true);
                echo "
            </li>
        ";
            }
            // line 60
            echo "    </ul>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroCron/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 60,  179 => 57,  176 => 56,  174 => 55,  171 => 54,  165 => 53,  162 => 52,  156 => 49,  153 => 48,  147 => 45,  144 => 44,  141 => 43,  139 => 42,  136 => 41,  130 => 38,  127 => 37,  121 => 34,  118 => 33,  115 => 32,  112 => 31,  110 => 30,  107 => 29,  104 => 28,  98 => 25,  95 => 24,  92 => 23,  90 => 22,  87 => 21,  81 => 18,  78 => 17,  75 => 16,  72 => 15,  67 => 14,  65 => 13,  62 => 12,  59 => 11,  56 => 10,  53 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCron/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CronBundle/Resources/views/macros.html.twig");
    }
}
