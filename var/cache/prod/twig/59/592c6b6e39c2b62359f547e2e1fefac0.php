<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/HashNav/redirect.html.twig */
class __TwigTemplate_02746a37fe1d7f63019a352ec9d57844 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["data"] = ["redirect" => true, "fullRedirect" =>         // line 3
($context["full_redirect"] ?? null), "location" =>         // line 4
($context["location"] ?? null)];
        // line 6
        echo json_encode(($context["data"] ?? null));
    }

    public function getTemplateName()
    {
        return "@OroNavigation/HashNav/redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 6,  39 => 4,  38 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/HashNav/redirect.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/HashNav/redirect.html.twig");
    }
}
