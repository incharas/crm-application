<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/slick-dots.scss */
class __TwigTemplate_0a7b14181a3ff7ad2cf0413ed9d4d346 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

// mixin for Slick slider dots navigation
@mixin slick-dots(
    \$slick-dots-class:                    '.slick-dots',
    \$slick-dots-bottom-pos:               \$offset-y,
    \$slick-dots-item-offset:              0 \$offset-x-s,
    \$slick-dots-button-size:              10px,
    \$slick-dots-button-bg:                get-color('additional', 'light'),
    \$slick-dots-button-border:            1px solid get-color('additional', 'middle'),
    \$slick-dots-button-box-shadow:        null,
    \$slick-dots-button-border-radius:     50%,
    \$slick-dots-button-active-border:     1px solid get-color('ui', 'focus'),
    \$slick-dots-button-active-bg:         get-color('ui', 'focus'),
    \$slick-dots-button-active-box-shadow: 0 0 7px get-color('ui', 'focus')
) {
    #{\$slick-dots-class} {
        position: absolute;
        bottom: \$slick-dots-bottom-pos;

        width: 100%;
        margin: 0 auto;
        padding: 0;

        text-align: center;
        vertical-align: bottom;

        li {
            display: inline-block;

            margin: \$slick-dots-item-offset;

            &.slick-active {
                button {
                    background: \$slick-dots-button-active-bg;
                    border: \$slick-dots-button-active-border;
                    box-shadow: \$slick-dots-button-active-box-shadow;
                }
            }
        }

        button {
            width: \$slick-dots-button-size;
            height: \$slick-dots-button-size;
            padding: 0;

            font-size: 0;

            background: \$slick-dots-button-bg;
            border: \$slick-dots-button-border;
            box-shadow: \$slick-dots-button-box-shadow;

            border-radius: \$slick-dots-button-border-radius;

            cursor: pointer;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/slick-dots.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/slick-dots.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/slick-dots.scss");
    }
}
