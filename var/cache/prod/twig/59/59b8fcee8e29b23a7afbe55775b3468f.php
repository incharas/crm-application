<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/macros.html.twig */
class __TwigTemplate_b6d9b420e71dc781f36ba33b3fd75f49 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "
";
    }

    // line 1
    public function macro_renderView($__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $context["tagCloudElId"] = ("tags-" . twig_random($this->env));
            // line 3
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTag/macros.html.twig", 3)->unwrap();
            // line 4
            echo "    <div ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroform/js/app/components/inline-editable-view-component", "options" => ["frontend_type" => "tags", "value" => $this->extensions['Oro\Bundle\TagBundle\Twig\TagExtension']->getList(            // line 8
($context["entity"] ?? null)), "fieldName" => "tags", "metadata" => ["inline_editing" => ["enable" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_tag_assign_unassign"), "save_api_accessor" => ["route" => "oro_api_post_taggable", "http_method" => "POST", "default_route_parameters" => ["entity" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(            // line 17
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["entity"] ?? null), "id", [], "any", false, false, false, 18)]], "autocomplete_api_accessor" => ["class" => "oroui/js/tools/search-api-accessor", "search_handler_name" => "tags", "label_field_name" => "name"], "editor" => ["view_options" => ["permissions" => ["oro_tag_create" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_tag_create")]]]]]]]], 4, $context, $this->getSourceContext());
            // line 36
            echo "></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 42
    public function macro_tagSortActions(...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 43
            echo "    <ul class=\"tag-sort-actions inline\">
        <li>
            <a href=\"#\" class=\"no-hash active\">";
            // line 45
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.menu.all_tags"), "html", null, true);
            echo "</a>
        </li>
        <li>
            <a href=\"#\" data-type=\"owner\" class=\"no-hash\">";
            // line 48
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.menu.my_tags"), "html", null, true);
            echo "</a>
        </li>
    </ul>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroTag/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 48,  93 => 45,  89 => 43,  77 => 42,  67 => 36,  65 => 18,  64 => 17,  63 => 8,  61 => 4,  58 => 3,  55 => 2,  42 => 1,  37 => 38,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/macros.html.twig");
    }
}
