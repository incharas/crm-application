<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntity/Entities/relation.html.twig */
class __TwigTemplate_d97856ec5b2e1ba45dae540969ba9ecc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroEntity/Entities/relation.html.twig", 1)->unwrap();
        // line 2
        echo "<div class=\"widget-content\">
    ";
        // line 3
        $context["gridName"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName("entity-relation-grid", ((($context["entity_name"] ?? null) . "-") . ($context["field_name"] ?? null)));
        // line 4
        echo "
    ";
        // line 5
        $context["gridParams"] = ["_parameters" => ["data_in" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["app"] ?? null), "request", [], "any", false, false, false, 7), "get", [0 => "added"], "method", false, false, false, 7)) ? (twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 7), "get", [0 => "added"], "method", false, false, false, 7), ",")) : ([])), "data_not_in" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["app"] ?? null), "request", [], "any", false, false, false, 8), "get", [0 => "removed"], "method", false, false, false, 8)) ? (twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 8), "get", [0 => "removed"], "method", false, false, false, 8), ",")) : ([]))], "class_name" =>         // line 10
($context["entity_class"] ?? null), "field_name" =>         // line 11
($context["field_name"] ?? null), "id" =>         // line 12
($context["id"] ?? null)];
        // line 14
        echo "
    ";
        // line 15
        $this->displayBlock('content', $context, $blocks);
        // line 20
        echo "
    ";
        // line 21
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntity/Entities/relation.html.twig", 21)->unwrap();
        // line 22
        echo "
    ";
        // line 23
        $context["extraData"] = [];
        // line 24
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["relation"] ?? null), "get", [0 => "target_grid"], "method", false, false, false, 24));
        foreach ($context['_seq'] as $context["_key"] => $context["fieldName"]) {
            // line 25
            echo "        ";
            $context["extraData"] = twig_array_merge(($context["extraData"] ?? null), [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["entity_provider"] ?? null), "getConfig", [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["relation"] ?? null), "get", [0 => "target_entity"], "method", false, false, false, 26), 1 => $context["fieldName"]], "method", false, false, false, 26), "get", [0 => "label"], "method", false, false, false, 26)), "value" =>             // line 27
$context["fieldName"]]]);
            // line 29
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['fieldName'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "
    <div ";
        // line 31
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroform/js/multiple-entity/component", "options" => ["wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["app"] ?? null), "request", [], "any", false, false, false, 34), "get", [0 => "_wid"], "method", false, false, false, 34), "gridName" =>         // line 35
($context["gridName"] ?? null), "addedVal" => "#appendRelation", "removedVal" => "#removeRelation", "columnName" => "assigned", "fieldTitles" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 39
($context["relation"] ?? null), "get", [0 => "target_title"], "method", false, false, false, 39), "entityName" => twig_replace_filter(        // line 40
($context["entity_class"] ?? null), ["\\" => "_"]), "fieldName" =>         // line 41
($context["field_name"] ?? null), "extraData" =>         // line 42
($context["extraData"] ?? null), "link" => "oro_entity_detailed"]]], 31, $context, $this->getSourceContext());
        // line 45
        echo "></div>

    <div class=\"widget-actions\">
        <button type=\"reset\" class=\"btn\">";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
        <button type=\"button\" class=\"btn btn-primary\" data-action-name=\"select\">";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Select"), "html", null, true);
        echo "</button>
    </div>
</div>
";
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "        ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ($context["gridParams"] ?? null)], 16, $context, $this->getSourceContext());
        echo "
        <input type=\"hidden\" name=\"appendRelation\" id=\"appendRelation\" value=\"";
        // line 17
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 17), "get", [0 => "added"], "method", false, false, false, 17), "html", null, true);
        echo "\" />
        <input type=\"hidden\" name=\"removeRelation\" id=\"removeRelation\" value=\"";
        // line 18
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 18), "get", [0 => "removed"], "method", false, false, false, 18), "html", null, true);
        echo "\" />
    ";
    }

    public function getTemplateName()
    {
        return "@OroEntity/Entities/relation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 18,  123 => 17,  118 => 16,  114 => 15,  106 => 49,  102 => 48,  97 => 45,  95 => 42,  94 => 41,  93 => 40,  92 => 39,  91 => 35,  90 => 34,  89 => 31,  86 => 30,  80 => 29,  78 => 27,  77 => 26,  75 => 25,  70 => 24,  68 => 23,  65 => 22,  63 => 21,  60 => 20,  58 => 15,  55 => 14,  53 => 12,  52 => 11,  51 => 10,  50 => 8,  49 => 7,  48 => 5,  45 => 4,  43 => 3,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntity/Entities/relation.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Resources/views/Entities/relation.html.twig");
    }
}
