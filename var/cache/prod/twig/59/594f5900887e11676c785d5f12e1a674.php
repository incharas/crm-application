<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/jquery-timepicker-l10n.js */
class __TwigTemplate_28b1c174f37b0d6e81de04e94c5e34b4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const __ = require('orotranslation/js/translator');
    const localeSettings = require('orolocale/js/locale-settings');
    require('jquery.timepicker');

    const decimal = localeSettings.getNumberFormats('decimal').decimal_separator_symbol;
    const timeFormat = localeSettings.getVendorDateTimeFormat('php', 'time', 'g:i A');

    \$.fn.timepicker.defaults = {
        timeFormat: timeFormat,
        lang: {
            am: __('oro.ui.timepicker.am'),
            pm: __('oro.ui.timepicker.pm'),
            AM: __('oro.ui.timepicker.AM'),
            PM: __('oro.ui.timepicker.PM'),
            decimal: decimal,
            mins: __('oro.ui.timepicker.mins'),
            hr: __('oro.ui.timepicker.hr'),
            hrs: __('oro.ui.timepicker.hrs')
        }
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/jquery-timepicker-l10n.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/jquery-timepicker-l10n.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/jquery-timepicker-l10n.js");
    }
}
