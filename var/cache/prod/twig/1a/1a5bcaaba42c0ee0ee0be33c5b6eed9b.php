<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/layouts/default/page/page-footer.html.twig */
class __TwigTemplate_700d574795222230038f7192fe62aca7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_page_footer_widget' => [$this, 'block__page_footer_widget'],
            '_page_footer_container_widget' => [$this, 'block__page_footer_container_widget'],
            '_page_footer_base_widget' => [$this, 'block__page_footer_base_widget'],
            '_page_footer_side_widget' => [$this, 'block__page_footer_side_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_page_footer_widget', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('_page_footer_container_widget', $context, $blocks);
        // line 20
        echo "
";
        // line 21
        $this->displayBlock('_page_footer_base_widget', $context, $blocks);
        // line 29
        echo "
";
        // line 30
        $this->displayBlock('_page_footer_side_widget', $context, $blocks);
    }

    // line 1
    public function block__page_footer_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-footer", "data-page-footer" => ""]);
        // line 6
        echo "
    <footer ";
        // line 7
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 8
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </footer>
";
    }

    // line 12
    public function block__page_footer_container_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-footer-container"]);
        // line 16
        echo "    <div ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 17
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 21
    public function block__page_footer_base_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-footer-container__base"]);
        // line 25
        echo "    <div ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 26
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 30
    public function block__page_footer_side_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-footer-container__side"]);
        // line 34
        echo "    <div ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 35
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/layouts/default/page/page-footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  131 => 35,  126 => 34,  123 => 31,  119 => 30,  112 => 26,  107 => 25,  104 => 22,  100 => 21,  93 => 17,  88 => 16,  85 => 13,  81 => 12,  74 => 8,  70 => 7,  67 => 6,  64 => 2,  60 => 1,  56 => 30,  53 => 29,  51 => 21,  48 => 20,  46 => 12,  43 => 11,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/layouts/default/page/page-footer.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/page-footer.html.twig");
    }
}
