<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntity/Datagrid/Property/entityFallbackValue.html.twig */
class __TwigTemplate_7d262fa8b8f2b7a07e277016f236821f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["entity"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityReference(($context["entityClassName"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "id"], "method", false, false, false, 1));
        // line 2
        $context["fallbackType"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getFallbackType(($context["entity"] ?? null), ($context["fieldName"] ?? null));
        // line 3
        $context["fallbackValue"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getFallbackValue(($context["entity"] ?? null), ($context["fieldName"] ?? null));
        // line 4
        echo "
";
        // line 5
        if ((($context["fallbackType"] ?? null) == "boolean")) {
            // line 6
            echo "    ";
            echo twig_escape_filter($this->env, ((($context["fallbackValue"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No"))), "html", null, true);
            echo "
";
        } elseif ((        // line 7
($context["fallbackType"] ?? null) == "array")) {
            // line 8
            echo "    ";
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntity/Datagrid/Property/entityFallbackValue.html.twig", 8)->unwrap();
            // line 9
            echo "    ";
            echo twig_call_macro($macros["UI"], "macro_renderList", [($context["fallbackValue"] ?? null)], 9, $context, $this->getSourceContext());
            echo "
";
        } else {
            // line 11
            echo "    ";
            echo twig_escape_filter($this->env, ((array_key_exists("fallbackValue", $context)) ? (_twig_default_filter(($context["fallbackValue"] ?? null), "")) : ("")), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntity/Datagrid/Property/entityFallbackValue.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 11,  58 => 9,  55 => 8,  53 => 7,  48 => 6,  46 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntity/Datagrid/Property/entityFallbackValue.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Resources/views/Datagrid/Property/entityFallbackValue.html.twig");
    }
}
