<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/caret.scss */
class __TwigTemplate_4f0be935a15f75a1da0af6c7ffad8679 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

// Mixin for include caret triangle anywhere
// @param \$size set caret size
// @param \$color set caret color
@mixin custom-caret(\$size: 4px, \$color: null) {
    &::after {
        content: '';
        display: inline-block;
        width: 0;
        height: 0;
        border-top: \$size solid \$color;
        border-right: \$size solid transparent;
        border-left: \$size solid transparent;
        vertical-align: middle;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/caret.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/caret.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/caret.scss");
    }
}
