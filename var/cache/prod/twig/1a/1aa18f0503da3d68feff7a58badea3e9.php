<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/highlighter/highlighter-title.js */
class __TwigTemplate_656f4bc607c4a044aec8be2ad5ea737a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require, exports, module) {
    'use strict';

    const config = require('module-config').default(module.id);
    const _ = require('underscore');
    const BaseClass = require('oroui/js/base-class');

    const defaults = {
        prefix: '● '
    };

    const HighlighterTitle = BaseClass.extend({
        /**
         * @inheritdoc
         */
        constructor: function HighlighterTitle(options) {
            HighlighterTitle.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            const names = _.keys(defaults);
            _.extend(this, defaults, _.pick(config, names), _.pick(options, names));

            HighlighterTitle.__super__.initialize.call(this, options);
        },

        /**
         * @inheritdoc
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }

            this.unhighlight();

            HighlighterTitle.__super__.dispose.call(this);
        },

        highlight: function() {
            if (document.title.substr(0, this.prefix.length) !== this.prefix) {
                document.title = this.prefix + document.title;
            }
        },

        unhighlight: function() {
            if (document.title.substr(0, this.prefix.length) === this.prefix) {
                document.title = document.title.substr(this.prefix.length);
            }
        }
    });

    return HighlighterTitle;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/highlighter/highlighter-title.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/highlighter/highlighter-title.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/highlighter/highlighter-title.js");
    }
}
