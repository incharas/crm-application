<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImportExport/ImportExport/widget/configurableTemplateExport.html.twig */
class __TwigTemplate_d22b845c9c27f413cc627981c6920ce1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroImportExport/ImportExport/widget/configurableTemplateExport.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["exportConfigurableTemplateWidgetViewOptions"] = ["view" => ["view" => "oroimportexport/js/app/views/export-configurable-template-widget-view", "wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["app"] ?? null), "request", [], "any", false, false, false, 6), "get", [0 => "_wid"], "method", false, false, false, 6), "downloadMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Downloading data template...")]];
        // line 10
        echo "
<div class=\"widget-content import-widget-content\">
    <div class=\"form-container\" ";
        // line 12
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["exportConfigurableTemplateWidgetViewOptions"] ?? null)], 12, $context, $this->getSourceContext());
        echo ">
        ";
        // line 13
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_importexport_export_template_config", ["entity" =>         // line 14
($context["entityName"] ?? null), "options" => ($context["options"] ?? null)]), "attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 16
($context["form"] ?? null), "vars", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16), "data-nohash" => "true", "class" => "form-horizontal"]]);
        // line 20
        echo "

            <fieldset class=\"form\">
                ";
        // line 23
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
            </fieldset>

            <div class=\"widget-actions\">
                <button class=\"btn\" type=\"reset\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
                <button class=\"btn btn-primary\" type=\"submit\">
                    ";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Download"), "html", null, true);
        echo "
                </button>
            </div>
        ";
        // line 32
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
        ";
        // line 33
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroImportExport/ImportExport/widget/configurableTemplateExport.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  84 => 33,  80 => 32,  74 => 29,  69 => 27,  62 => 23,  57 => 20,  55 => 16,  54 => 14,  53 => 13,  49 => 12,  45 => 10,  43 => 6,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImportExport/ImportExport/widget/configurableTemplateExport.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Resources/views/ImportExport/widget/configurableTemplateExport.html.twig");
    }
}
