<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/history-model.js */
class __TwigTemplate_0bcc091bb0ad9510a32e5f7dd21fbb41 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const MAX_LENGTH = 64;
    const _ = require('underscore');
    const BaseModel = require('oroui/js/app/models/base/model');
    const HistoryStateCollection = require('./history-state-collection');

    const HistoryModel = BaseModel.extend({
        defaults: {
            states: null,
            index: -1
        },

        /**
         * @inheritdoc
         */
        constructor: function HistoryModel(attrs, options) {
            HistoryModel.__super__.constructor.call(this, attrs, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(attrs, options) {
            this.options = _.defaults({}, options, {
                maxLength: MAX_LENGTH
            });
            this.set('states', new HistoryStateCollection());
            this.set('index', -1);
        },

        pushState: function(state) {
            const states = this.get('states');
            const index = this.get('index');
            if (states.length > index + 1) {
                states.reset(states.first(index + 1));
            }
            if (states.length >= this.options.maxLength) {
                states.reset(states.last(this.options.maxLength - 1));
            }
            states.add(state);
            this.set('index', states.length - 1);
        },

        getCurrentState: function() {
            return this.get('states').at(this.get('index'));
        },

        setIndex: function(index) {
            if (index >= 0 && index < this.get('states').length) {
                this.set('index', index);
                return true;
            }
        }
    });
    return HistoryModel;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/history-model.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/history-model.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/history-model.js");
    }
}
