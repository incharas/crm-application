<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/create-select-entity.scss */
class __TwigTemplate_cd21ece496a14e89716b54dec448b9b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.create-select-entity {
    .box-type1 .title,
    h5 {
        height: 1px;
        margin-bottom: 20px;
        padding: 0;
        position: relative;
        text-align: left;
        border-bottom: \$primary-800 1px solid;
        overflow: visible;
        width: 90%;
    }

    .box-type1 .title .widget-title,
    h5 span {
        background: \$primary-inverse;
        display: inline-block;
        line-height: 15px;
        padding-right: 15px;
        position: relative;
        top: -.6em;
    }

    /** create-select buttons visibility */
    .entity-create-block,
    .entity-select-block,
    .entity-view-block {
        display: none;
    }

    &.create .entity-create-block,
    &.grid .entity-select-block,
    &.view .entity-view-block {
        display: block;
    }

    &.create .entity-cancel-btn,
    &.view .entity-cancel-btn,
    &.create .entity-create-btn,
    &.grid .entity-select-btn {
        display: none;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/create-select-entity.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/create-select-entity.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/create-select-entity.scss");
    }
}
