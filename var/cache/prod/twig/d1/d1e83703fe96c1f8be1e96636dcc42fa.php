<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/base-class.js */
class __TwigTemplate_8e1c2e7f9805bba7346b3c7fe3e92f16 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const Backbone = require('backbone');
    const Chaplin = require('chaplin');

    /**
     * Base class that implement extending in backbone way.
     * Implements [Backbone events API](http://backbonejs.org/#Events), Chaplin's
     * [declarative event bindings](https://github.com/chaplinjs/chaplin/blob/master/docs/chaplin.view.md#listen) and
     * [Chaplin.EventBroker API](https://github.com/chaplinjs/chaplin/blob/master/docs/chaplin.event_broker.md)
     *
     *
     * @class
     * @param {Object} options - Options container
     * @param {Object} options.listen - Optional. Events to bind
     */
    function BaseClass(options) {
        this.cid = _.uniqueId(this.cidPrefix);
        if (!options) {
            options = {};
        }
        this.initialize(options);
        if (options.listen) {
            this.on(options.listen);
        }
        this.delegateListeners();
    }

    BaseClass.prototype = {
        cidPrefix: 'class',

        constructor: BaseClass,

        /**
         * Flag shows if the class is disposed or not
         * @type {boolean}
         */
        disposed: false,

        initialize: function(options) {
            // should be defined in descendants
        },

        dispose: function() {
            if (this.disposed) {
                return;
            }
            this.trigger('dispose', this);
            this.unsubscribeAllEvents();
            this.stopListening();
            this.off();

            this.disposed = true;
            return typeof Object.freeze === 'function' ? Object.freeze(this) : void 0;
        }
    };

    _.extend(BaseClass.prototype,
        Backbone.Events,
        Chaplin.EventBroker,
        _.pick(Chaplin.View.prototype, ['delegateListeners', 'delegateListener'])
    );

    BaseClass.extend = Backbone.Model.extend;

    return BaseClass;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/base-class.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/base-class.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/base-class.js");
    }
}
