<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/macros.html.twig */
class __TwigTemplate_be2599ce8f1ed198234e6a2ee096070c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_renderAttributeWithTooltip($__title__ = null, $__data__ = null, $__tooltip__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "title" => $__title__,
            "data" => $__data__,
            "tooltip" => $__tooltip__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/macros.html.twig", 2)->unwrap();
            // line 3
            echo "    <div class=\"control-group\">
        <label class=\"control-label\">";
            // line 4
            echo twig_call_macro($macros["ui"], "macro_tooltip", [($context["tooltip"] ?? null)], 4, $context, $this->getSourceContext());
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["title"] ?? null)), "html", null, true);
            echo "</label>
        <div class=\"controls\">
            <div class=\"control-label\">
                ";
            // line 7
            echo twig_escape_filter($this->env, _twig_default_filter(twig_escape_filter($this->env, ($context["data"] ?? null)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty")), "html", null, true);
            echo "
            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 7,  60 => 4,  57 => 3,  54 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/macros.html.twig");
    }
}
