<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTracking/TrackingWebsite/update.html.twig */
class __TwigTemplate_64316fe502799a1ad04053797affefe0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.entity_label")]]);
        // line 7
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 7);
        // line 9
        $context["formAction"] = ((($context["entityId"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tracking_website_update", ["id" =>         // line 10
($context["entityId"] ?? null)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tracking_website_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroTracking/TrackingWebsite/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 13
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTracking/TrackingWebsite/update.html.twig", 14)->unwrap();
        // line 15
        echo "
    ";
        // line 16
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_tracking_website_view", "params" => ["id" => "\$id"]]], 16, $context, $this->getSourceContext());
        // line 20
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_tracking_website_create")) {
            // line 21
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_tracking_website_create"]], 21, $context, $this->getSourceContext()));
            // line 24
            echo "    ";
        }
        // line 25
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_tracking_website_update")) {
            // line 26
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_tracking_website_update", "params" => ["id" => "\$id"]]], 26, $context, $this->getSourceContext()));
            // line 30
            echo "    ";
        }
        // line 31
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 31, $context, $this->getSourceContext());
        echo "
    ";
        // line 32
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tracking_website_index")], 32, $context, $this->getSourceContext());
        echo "
";
    }

    // line 35
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 37
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 38
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tracking_website_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 41
($context["entity"] ?? null), "name", [], "any", false, false, false, 41)];
            // line 43
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 45
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.entity_label")]);
            // line 46
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroTracking/TrackingWebsite/update.html.twig", 46)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 47
            echo "    ";
        }
    }

    // line 50
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    ";
        $context["id"] = "tracking-website-form";
        // line 52
        echo "
    ";
        // line 53
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.block.general"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 59
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 59), 'row'), 1 =>         // line 60
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "identifier", [], "any", false, false, false, 60), 'row'), 2 =>         // line 61
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "url", [], "any", false, false, false, 61), 'row')]]]]];
        // line 65
        echo "
    ";
        // line 66
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.block.additional")));
        // line 67
        echo "
    ";
        // line 68
        $context["data"] = ["formErrors" => ((        // line 69
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 70
($context["dataBlocks"] ?? null)];
        // line 72
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTracking/TrackingWebsite/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  154 => 72,  152 => 70,  151 => 69,  150 => 68,  147 => 67,  145 => 66,  142 => 65,  140 => 61,  139 => 60,  138 => 59,  137 => 53,  134 => 52,  131 => 51,  127 => 50,  122 => 47,  119 => 46,  116 => 45,  110 => 43,  108 => 41,  107 => 38,  105 => 37,  102 => 36,  98 => 35,  92 => 32,  87 => 31,  84 => 30,  81 => 26,  78 => 25,  75 => 24,  72 => 21,  69 => 20,  67 => 16,  64 => 15,  61 => 14,  57 => 13,  52 => 1,  50 => 10,  49 => 9,  47 => 7,  45 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTracking/TrackingWebsite/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/TrackingBundle/Resources/views/TrackingWebsite/update.html.twig");
    }
}
