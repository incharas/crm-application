<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNote/Note/widget/info.html.twig */
class __TwigTemplate_051df8503dec51b0c80f54bec52ae4d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNote/Note/widget/info.html.twig", 2)->unwrap();
        // line 3
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroNote/Note/widget/info.html.twig", 3)->unwrap();
        // line 4
        echo "
<div class=\"widget-content form-horizontal box-content row-fluid\">
    <div class=\"responsive-block\">
        <div class=\"activity-context-activity-list\">
            ";
        // line 8
        echo twig_call_macro($macros["AC"], "macro_activity_contexts", [($context["entity"] ?? null), ($context["target"] ?? null), true, "oronote/js/app/components/note-context-component"], 8, $context, $this->getSourceContext());
        echo "
        </div>

        ";
        // line 11
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.message.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "message", [], "any", false, false, false, 11)], 11, $context, $this->getSourceContext());
        echo "
    </div>
    ";
        // line 13
        if (( !twig_test_empty(($context["attachment"] ?? null)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentURL", [], "any", false, false, false, 13))) {
            // line 14
            echo "    <div class=\"note-attachments\">
        <div class=\"control-group\">
            <label class=\"control-label\">
                ";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Attachment"), "html", null, true);
            echo "
            </label>
            <div class=\"attachment-item attachment-item--note\">
                <div class=\"thumbnail\">
                    ";
            // line 21
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentThumbnailPicture", [], "any", false, false, false, 21), "src", [], "any", false, false, false, 21)) {
                // line 22
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentURL", [], "any", false, false, false, 22), "url", [], "any", false, false, false, 22), "html", null, true);
                echo "\" data-gallery=\"note-view-";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 22), "html", null, true);
                echo "\" class=\"no-hash\" title=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentFileName", [], "any", false, false, false, 22), "html_attr");
                echo "\" data-sources=\"";
                echo twig_escape_filter($this->env, json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentURL", [], "any", false, false, false, 22), "sources", [], "any", false, false, false, 22)), "html", null, true);
                echo "\">
                            ";
                // line 23
                $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroNote/Note/widget/info.html.twig", 23)->display(twig_array_merge($context, ["sources" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 24
($context["attachment"] ?? null), "attachmentThumbnailPicture", [], "any", false, false, false, 24), "sources", [], "any", false, false, false, 24), "img_attrs" => ["src" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 26
($context["attachment"] ?? null), "attachmentThumbnailPicture", [], "any", false, false, false, 26), "src", [], "any", false, false, false, 26), "class" => "thumbnail"]]));
                // line 30
                echo "                        </a>
                    ";
            } else {
                // line 32
                echo "                        <span class=\"fa ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentIcon", [], "any", false, false, false, 32), "html", null, true);
                echo " fa-offset-none\" aria-hidden=\"true\"></span>
                    ";
            }
            // line 34
            echo "                </div>
                <div class=\"dropdown link-to-record\">
                    ";
            // line 36
            $context["togglerId"] = uniqid("dropdown-");
            // line 37
            echo "                    <a id=\"";
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\" class=\"no-hash attachment-item__filename dropdown-toggle dropdown-toggle--no-caret file-menu\" href=\"#\" role=\"button\" data-toggle=\"dropdown\"
                       aria-haspopup=\"true\" aria-expanded=\"false\">
                        <span class=\"fa ";
            // line 39
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentIcon", [], "any", false, false, false, 39), "html", null, true);
            echo "\" aria-hidden=\"true\"></span> ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentFileName", [], "any", false, false, false, 39), "html", null, true);
            echo "
                    </a>
                    <ul class=\"dropdown-menu file-menu\" role=\"menu\" aria-labelledby=\"";
            // line 41
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\">
                        <li>
                            <a class=\"dropdown-item no-hash\" tabindex=\"-1\" href=\"";
            // line 43
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentURL", [], "any", false, false, false, 43), "downloadUrl", [], "any", false, false, false, 43), "html", null, true);
            echo "\">
                                ";
            // line 44
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.translation.action.download"), "html", null, true);
            echo " <span>";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "attachmentSize", [], "any", false, false, false, 44), "html", null, true);
            echo "</span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>

    </div>
    ";
        }
        // line 54
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "@OroNote/Note/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  142 => 54,  127 => 44,  123 => 43,  118 => 41,  111 => 39,  105 => 37,  103 => 36,  99 => 34,  93 => 32,  89 => 30,  87 => 26,  86 => 24,  85 => 23,  74 => 22,  72 => 21,  65 => 17,  60 => 14,  58 => 13,  53 => 11,  47 => 8,  41 => 4,  39 => 3,  37 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNote/Note/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NoteBundle/Resources/views/Note/widget/info.html.twig");
    }
}
