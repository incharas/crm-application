<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Datagrid/Property/subject.html.twig */
class __TwigTemplate_5dc10b13cc4e3b36b3d05fdb9a53c46d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/Datagrid/Property/subject.html.twig", 1)->unwrap();
        // line 2
        $context["emailBody"] = ["textBody" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "body_content"], "method", false, false, false, 2)];
        // line 3
        $context["isNew"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "is_new"], "method", false, false, false, 3);
        // line 4
        $context["valueToShow"] = ((($context["value"] ?? null)) ? (($context["value"] ?? null)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.subject.no_subject.label")));
        // line 5
        echo "
<div class=\"nowrap-ellipsis\">
    <div>
        ";
        // line 8
        if (($context["isNew"] ?? null)) {
            // line 9
            echo "            <strong class=\"email-subject\">";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["valueToShow"] ?? null));
            echo "</strong>
        ";
        } else {
            // line 11
            echo "            <span class=\"email-subject\">";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["valueToShow"] ?? null));
            echo "</span>
        ";
        }
        // line 13
        echo "        ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailBody"] ?? null), "textBody", [], "any", true, true, false, 13)) {
            // line 14
            echo "            <div class=\"email-body\">";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(twig_call_macro($macros["EA"], "macro_email_short_body", [($context["emailBody"] ?? null)], 14, $context, $this->getSourceContext()));
            echo "</div>
        ";
        }
        // line 16
        echo "    </div>
</div>
<div class=\"td-expander\"></div>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Datagrid/Property/subject.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 16,  67 => 14,  64 => 13,  58 => 11,  52 => 9,  50 => 8,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Datagrid/Property/subject.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Datagrid/Property/subject.html.twig");
    }
}
