<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/base/stateful-model.js */
class __TwigTemplate_aee41b95246b50f2f9ad0efa63c7eef2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const Backbone = require('backbone');
    const BaseModel = require('./model');
    const HistoryModel = require('../history-model');
    const HistoryStateModel = require('../history-state-model');

    const StatefulModel = BaseModel.extend({
        /**
         * @inheritdoc
         */
        constructor: function StatefulModel(attrs, options) {
            StatefulModel.__super__.constructor.call(this, attrs, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(attrs, options) {
            const historyOptions = {};
            StatefulModel.__super__.initialize.call(this, attrs, options);
            if ('MAX_HISTORY_LENGTH' in this) {
                historyOptions.maxLength = this.MAX_HISTORY_LENGTH;
            }
            this.history = new HistoryModel({}, historyOptions);
            this.boundEvents = [];
            this.debouncedOnStateChange = _.debounce(this.onStateChange.bind(this), 50);
            this.bindEvents();
        },

        destroy: function() {
            StatefulModel.__super__.destroy.call(this);
            this.unbindEvents();
        },

        bindEvents: function() {
            if ('observedAttributes' in this) {
                _.each(this.observedAttributes, function(attrName) {
                    const prop = this.get(attrName);
                    if (prop instanceof Backbone.Collection) {
                        const eventName = 'change add remove';
                        prop.on(eventName, this.debouncedOnStateChange, this);
                        this.on('change:' + attrName, function(model, collection) {
                            prop.off(eventName, this.debouncedOnStateChange, this);
                            collection.on(eventName, this.debouncedOnStateChange, this);
                        }, this);
                    } else {
                        this.on('change:' + attrName, this.debouncedOnStateChange, this);
                    }
                }, this);
            } else {
                this.on('change', this.debouncedOnStateChange, this);
            }
        },

        unbindEvents: function() {
            if ('observedAttributes' in this) {
                _.each(this.observedAttributes, function(attrName) {
                    const prop = this.get(attrName);
                    if (prop instanceof Backbone.Collection) {
                        prop.off('change add remove', this.debouncedOnStateChange, this);
                        this.off('change:' + attrName, null, this);
                    } else {
                        this.off('change:' + attrName, this.debouncedOnStateChange, this);
                    }
                }, this);
            } else {
                this.off('change', this.debouncedOnStateChange, this);
            }
        },

        onStateChange: function() {
            const state = new HistoryStateModel({
                data: this.getState()
            });
            this.history.pushState(state);
        },

        getState: function() {
            throw new Error('Method getState should be defined in a inherited class.');
        },

        setState: function() {
            throw new Error('Method getState should be defined in a inherited class.');
        }
    });

    return StatefulModel;
});

";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/base/stateful-model.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/base/stateful-model.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/base/stateful-model.js");
    }
}
