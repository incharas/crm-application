<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/controllers/base/controller.js */
class __TwigTemplate_b7ae233638f6444ae14a21dd663ec208 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const Chaplin = require('chaplin');
    const beforeActionPromises = [
        // add DOM Ready promise to loads promises,
        // in order to prevent route action execution before the page is ready
        \$.ready
    ];

    const BaseController = Chaplin.Controller.extend({
        /**
         * Handles before-action activity
         *  - initializes cache (on first route)
         *  - when all compositions are ready to reuse, resolves the deferred to execute action
         *
         * @override
         */
        beforeAction: function(params, route, options) {
            BaseController.__super__.beforeAction.call(this, params, route, options);

            // if it's first time route
            if (!route.previous) {
                // initializes page cache
                this.cache.init(route);
            }

            return \$.when(...beforeActionPromises);
        },

        /**
         * Combines full URL for the route
         *
         * @param {Object} route
         * @returns {string}
         * @private
         */
        _combineRouteUrl: function(route) {
            const url = Chaplin.mediator.execute('combineFullUrl', route.path, route.query);
            return url;
        },

        /**
         * Cache accessor
         */
        cache: {
            /**
             * Executes 'init' handler for pageCache manager
             *
             * @param {Object} route
             */
            init: function(route) {
                const path = route.path;
                const query = route.query;
                const userName = Chaplin.mediator.execute('retrieveOption', 'userName') || false;
                Chaplin.mediator.execute({
                    name: 'pageCache:init',
                    silent: true
                }, path, query, userName);
            },

            /**
             * Executes 'get' handler for pageCache manager
             *
             * @param {string=} path
             * @returns {Object|undefined}
             */
            get: function(path) {
                return Chaplin.mediator.execute({
                    name: 'pageCache:get',
                    silent: true
                }, path);
            }
        }
    }, {
        /**
         * Adds custom promise object in to beforeAction promises collection
         *
         * @param {Promise} promise
         * @static
         */
        addBeforeActionPromise: function(promise) {
            beforeActionPromises.push(promise);
        }
    });

    return BaseController;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/controllers/base/controller.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/controllers/base/controller.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/controllers/base/controller.js");
    }
}
