<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/focus-visible.scss */
class __TwigTemplate_80b7052787cd013601de4e051e38733a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@if \$focus-visible-style {
    .focus-visible:focus {
        box-shadow: \$focus-visible-style;

        &[data-skip-focus-decoration],
        [data-skip-focus-decoration-inner-elements] &,
        &:disabled,
        &.disabled {
            box-shadow: none;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/focus-visible.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/focus-visible.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/focus-visible.scss");
    }
}
