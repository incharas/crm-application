<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/User/widget/info.html.twig */
class __TwigTemplate_fe1afa80645accdecd993724477702ae extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/User/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["tag"] = $this->macros["tag"] = $this->loadTemplate("@OroTag/macros.html.twig", "@OroUser/User/widget/info.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroUser/User/widget/info.html.twig", 3)->unwrap();
        // line 4
        $macros["userInfo"] = $this->macros["userInfo"] = $this;
        // line 5
        echo "
";
        // line 12
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 16
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.username.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "username", [], "any", false, false, false, 16)], 16, $context, $this->getSourceContext());
        echo "
            ";
        // line 17
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.birthday.label"), twig_call_macro($macros["ui"], "macro_render_birthday", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "birthday", [], "any", false, false, false, 17)], 17, $context, $this->getSourceContext())], 17, $context, $this->getSourceContext());
        echo "

            ";
        // line 19
        $context["emails"] = [0 => twig_call_macro($macros["userInfo"], "macro_renderEmail", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "email", [], "any", false, false, false, 19), true, ($context["entity"] ?? null)], 19, $context, $this->getSourceContext())];
        // line 20
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emails", [], "any", false, false, false, 20));
        foreach ($context['_seq'] as $context["_key"] => $context["emailEntity"]) {
            // line 21
            echo "                ";
            $context["emails"] = twig_array_merge(($context["emails"] ?? null), [0 => twig_call_macro($macros["userInfo"], "macro_renderEmail", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["emailEntity"], "email", [], "any", false, false, false, 21), false, ($context["entity"] ?? null)], 21, $context, $this->getSourceContext())]);
            // line 22
            echo "            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['emailEntity'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "            ";
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.emails.label"), twig_join_filter(($context["emails"] ?? null), "<br />")], 23, $context, $this->getSourceContext());
        echo "

            ";
        // line 25
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.phone.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phone", [], "any", false, false, false, 25)) ? (twig_call_macro($macros["ui"], "macro_renderPhoneWithActions", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phone", [], "any", false, false, false, 25), ($context["entity"] ?? null)], 25, $context, $this->getSourceContext())) : (null))], 25, $context, $this->getSourceContext());
        echo "

            ";
        // line 27
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_role_view")) {
            // line 28
            echo "                ";
            $context["roles"] = [];
            // line 29
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "userRoles", [], "any", false, false, false, 29));
            foreach ($context['_seq'] as $context["_key"] => $context["entityRole"]) {
                // line 30
                echo "                    ";
                $context["roles"] = twig_array_merge(($context["roles"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["entityRole"], "label", [], "any", false, false, false, 30)]);
                // line 31
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entityRole'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 32
            echo "                ";
            echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.roles.label"), twig_nl2br(twig_escape_filter($this->env, twig_join_filter(($context["roles"] ?? null), "
")))], 32, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 34
        echo "
            ";
        // line 35
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_group_view") && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isFeatureEnabled("manage_user_groups"))) {
            // line 36
            echo "                ";
            echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.groups.label"), twig_nl2br(twig_escape_filter($this->env, twig_join_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "groups", [], "any", false, false, false, 36), "
")))], 36, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 38
        echo "
            ";
        // line 39
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_business_unit_view")) {
            // line 40
            echo "                ";
            echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.business_units.label"), twig_nl2br(twig_escape_filter($this->env, twig_join_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "businessUnits", [], "any", false, false, false, 40), "
")))], 40, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 42
        echo "
            ";
        // line 43
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 43, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
</div>

";
    }

    // line 6
    public function macro_renderEmail($__emailAddress__ = null, $__isPrimary__ = null, $__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailAddress" => $__emailAddress__,
            "isPrimary" => $__isPrimary__,
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 7
            echo "    ";
            $macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroUser/User/widget/info.html.twig", 7)->unwrap();
            // line 8
            echo "    ";
            if (($context["isPrimary"] ?? null)) {
                echo "<strong>";
            }
            // line 9
            echo "        ";
            echo twig_call_macro($macros["email"], "macro_renderEmailWithActions", [($context["emailAddress"] ?? null), ($context["entity"] ?? null)], 9, $context, $this->getSourceContext());
            echo "
    ";
            // line 10
            if (($context["isPrimary"] ?? null)) {
                echo "</strong>";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroUser/User/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  181 => 10,  176 => 9,  171 => 8,  168 => 7,  153 => 6,  143 => 43,  140 => 42,  133 => 40,  131 => 39,  128 => 38,  121 => 36,  119 => 35,  116 => 34,  109 => 32,  103 => 31,  100 => 30,  95 => 29,  92 => 28,  90 => 27,  85 => 25,  79 => 23,  73 => 22,  70 => 21,  65 => 20,  63 => 19,  58 => 17,  54 => 16,  48 => 12,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/User/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/User/widget/info.html.twig");
    }
}
