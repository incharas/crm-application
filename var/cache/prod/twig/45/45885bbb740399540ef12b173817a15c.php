<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSearch/Search/searchResultItem.html.twig */
class __TwigTemplate_3df33890fa64f81e1d4ab99a16e4efad extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'additional_info' => [$this, 'block_additional_info'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"page-title\">
    <div class=\"row clearfix\">
        <div class=\"pull-left pull-left-extra\">
            <div class=\"page-title__icon\">
                ";
        // line 5
        if (array_key_exists("recordUrl", $context)) {
            // line 6
            echo "<a href=\"";
            echo twig_escape_filter($this->env, ($context["recordUrl"] ?? null), "html", null, true);
            echo "\" ";
            if (array_key_exists("recordUrlCssClass", $context)) {
                echo "class=\"";
                echo twig_escape_filter($this->env, ($context["recordUrlCssClass"] ?? null), "html", null, true);
                echo "\"";
            }
            echo ">";
        }
        // line 8
        if (((array_key_exists("showImage", $context)) ? (_twig_default_filter(($context["showImage"] ?? null), false)) : (false))) {
            // line 9
            $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroSearch/Search/searchResultItem.html.twig", 9)->display(twig_array_merge($context, ["sources" => ((((            // line 10
array_key_exists("image", $context)) ? (_twig_default_filter(($context["image"] ?? null), null)) : (null))) ? ($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(($context["image"] ?? null), "avatar_med")) : ($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((("bundles/oroui/img/info-" . ((array_key_exists("iconType", $context)) ? (_twig_default_filter(($context["iconType"] ?? null), "noimage")) : ("noimage"))) . ".png")))), "filter" => "avatar_med", "img_attrs" => ["alt" =>             // line 12
($context["title"] ?? null)]]));
        }
        // line 15
        if (array_key_exists("recordUrl", $context)) {
            // line 16
            echo "</a>";
        }
        // line 18
        echo "            </div>
            <div class=\"";
        // line 19
        if (((array_key_exists("showImage", $context)) ? (_twig_default_filter(($context["showImage"] ?? null), false)) : (false))) {
            echo "page-title__path";
        }
        echo " pull-left\">
                <div class=\"clearfix\">
                    <div class=\"page-title__entity-title-wrapper\">
                        <h1 class=\"page-title__entity-title\">
                            ";
        // line 23
        if (array_key_exists("recordUrl", $context)) {
            echo "<a href=\"";
            echo twig_escape_filter($this->env, ($context["recordUrl"] ?? null), "html", null, true);
            echo "\" ";
            if (array_key_exists("recordUrlCssClass", $context)) {
                echo "class=\"";
                echo twig_escape_filter($this->env, ($context["recordUrlCssClass"] ?? null), "html", null, true);
                echo "\"";
            }
            echo ">";
        }
        // line 24
        echo "                                ";
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "
                            ";
        // line 25
        if (array_key_exists("recordUrl", $context)) {
            echo "</a>";
        }
        // line 26
        echo "                        </h1>
                    </div>
                    ";
        // line 28
        $this->displayBlock('additional_info', $context, $blocks);
        // line 30
        echo "                </div>";
        // line 31
        ob_start(function () { return ''; });
        // line 32
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("search_item_entity_info", $context)) ? (_twig_default_filter(($context["search_item_entity_info"] ?? null), "search_item_entity_info")) : ("search_item_entity_info")), ["entity" => ($context["entity"] ?? null)]);
        $context["searchItemEntityInfo"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 34
        echo "</div>
        </div>
        <div class=\"pull-right\">
            <div class=\"sub-title\">";
        // line 37
        echo twig_escape_filter($this->env, ($context["entityType"] ?? null), "html", null, true);
        echo "</div>
        </div>
    </div>
    <div class=\"row inline-info\">
        <div class=\"pull-left-extra\">
            ";
        // line 42
        if (((array_key_exists("entityInfo", $context) && twig_length_filter($this->env, ($context["entityInfo"] ?? null))) ||  !(null === ($context["searchItemEntityInfo"] ?? null)))) {
            // line 43
            echo "                <ul class=\"inline\">
                    ";
            // line 44
            if ( !(null === ($context["searchItemEntityInfo"] ?? null))) {
                // line 45
                echo "                        ";
                echo twig_escape_filter($this->env, ($context["searchItemEntityInfo"] ?? null), "html", null, true);
                echo "
                    ";
            }
            // line 47
            echo "                    ";
            if ((array_key_exists("entityInfo", $context) && twig_length_filter($this->env, ($context["entityInfo"] ?? null)))) {
                // line 48
                echo "                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["entityInfo"] ?? null));
                foreach ($context['_seq'] as $context["_key"] => $context["info"]) {
                    // line 49
                    echo "                            <li>";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["info"], "title", [], "any", false, false, false, 49), "html", null, true);
                    echo ": ";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["info"], "value", [], "any", false, false, false, 49), "html", null, true);
                    echo "</li>
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['info'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 51
                echo "                    ";
            }
            // line 52
            echo "                </ul>
            ";
        }
        // line 54
        echo "        </div>
    </div>
</div>
";
    }

    // line 28
    public function block_additional_info($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "                    ";
    }

    public function getTemplateName()
    {
        return "@OroSearch/Search/searchResultItem.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  178 => 29,  174 => 28,  167 => 54,  163 => 52,  160 => 51,  149 => 49,  144 => 48,  141 => 47,  135 => 45,  133 => 44,  130 => 43,  128 => 42,  120 => 37,  115 => 34,  112 => 32,  110 => 31,  108 => 30,  106 => 28,  102 => 26,  98 => 25,  93 => 24,  81 => 23,  72 => 19,  69 => 18,  66 => 16,  64 => 15,  61 => 12,  60 => 10,  59 => 9,  57 => 8,  46 => 6,  44 => 5,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSearch/Search/searchResultItem.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SearchBundle/Resources/views/Search/searchResultItem.html.twig");
    }
}
