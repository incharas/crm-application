<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/input-widget/uniform-file.js */
class __TwigTemplate_74c410020612fb15cdbbff2fadd49778 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const AbstractInputWidgetView = require('oroui/js/app/views/input-widget/abstract');
    const __ = require('orotranslation/js/translator');
    const \$ = require('jquery');
    const clearButtonTemplate = require('tpl-loader!oroui/templates/clear_button.html');
    require('jquery.uniform');

    const UniformFileInputWidgetView = AbstractInputWidgetView.extend({
        widgetFunctionName: 'uniform',

        initializeOptions: {
            fileDefaultHtml: __('Please select a file...'),
            fileButtonHtml: __('Choose File')
        },

        refreshOptions: 'update',

        containerClassSuffix: 'file',

        /** @property {jQuery} */
        \$clearButton: null,

        /**
         * @inheritdoc
         */
        constructor: function UniformFileInputWidgetView(options) {
            UniformFileInputWidgetView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initializeWidget: function(options) {
            UniformFileInputWidgetView.__super__.initializeWidget.call(this, options);
            if (this.\$el.is('.error')) {
                this.\$el.removeClass('error');
                this.getContainer().addClass('error');
            }

            this.getContainer().append(this.getClearButton());

            this.toggleEmptyState();

            this.\$el.on(`change\${this.eventNamespace()}`, () => this.toggleEmptyState());

            this.getClearButton().on(`click\${this.eventNamespace()}`, () => {
                this.\$el.val('').trigger('change').trigger('focus');
            });
        },

        /**
         * @inheritdoc
         */
        disposeWidget: function() {
            this.getClearButton().off(this.eventNamespace());
            this.\$el.off(this.eventNamespace());
            this.\$el.uniform.restore(this.\$el);
            UniformFileInputWidgetView.__super__.disposeWidget.call(this);
        },

        /**
         * Get widget root element
         *
         * @returns {jQuery}
         */
        getClearButton: function() {
            if (this.\$clearButton) {
                return this.\$clearButton;
            }

            this.\$clearButton = \$(clearButtonTemplate({
                ariaLabel: __('Clear')
            }));

            return this.\$clearButton;
        },

        toggleEmptyState: function() {
            this.getContainer().toggleClass('empty', this.isEmpty());
        },

        isEmpty: function() {
            return !this.\$el.val().length;
        },

        getFilenameButton: function() {
            return this.getContainer().find('.filename');
        },

        /**
         * @inheritdoc
         */
        findContainer: function() {
            return this.\$el.parent('.uploader');
        }
    });

    return UniformFileInputWidgetView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/input-widget/uniform-file.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/input-widget/uniform-file.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/input-widget/uniform-file.js");
    }
}
