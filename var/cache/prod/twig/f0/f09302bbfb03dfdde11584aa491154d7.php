<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroPlatform/have_request.html.twig */
class __TwigTemplate_8339d8f968be4a258c8ec8cca48a6296 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script>
    loadModules(['jquery', 'oroui/js/error'], function (\$, error) {
        \$(function () {
            var options = {
                dataType: 'script',
                cache:     true,
                url:       '";
        // line 7
        echo twig_escape_filter($this->env, ((array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), (twig_constant("Oro\\Bundle\\PlatformBundle\\Form\\UrlGenerator::URL") . twig_constant("Oro\\Bundle\\PlatformBundle\\Form\\UrlGenerator::FORM_JS")))) : ((twig_constant("Oro\\Bundle\\PlatformBundle\\Form\\UrlGenerator::URL") . twig_constant("Oro\\Bundle\\PlatformBundle\\Form\\UrlGenerator::FORM_JS")))), "html", null, true);
        echo "',
                timeout:   2000,
                errorHandlerMessage: false
            };

            \$.ajax(options).then(function() {
                new ORO.EmbedRequestForm(options);
            }, function() {
                error.showErrorInConsole('Unable to load external script.');
            });
        });
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroPlatform/have_request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroPlatform/have_request.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/PlatformBundle/Resources/views/have_request.html.twig");
    }
}
