<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Attribute/update.html.twig */
class __TwigTemplate_aeaba09d216b7bb044d0a91d9a2a0c90 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroEntityConfig/Config/fieldUpdate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "id", [], "any", false, false, false, 3)) {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%attributeName%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 4
($context["field"] ?? null), "fieldName", [], "any", false, false, false, 4)]]);
        } else {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute.entity_label")]]);
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroEntityConfig/Config/fieldUpdate.html.twig", "@OroEntityConfig/Attribute/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Attribute/update.html.twig", 10)->unwrap();
        // line 11
        echo "
    ";
        // line 12
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_index", ["alias" => ($context["entityAlias"] ?? null)])], 12, $context, $this->getSourceContext());
        echo "
    ";
        // line 13
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_attribute_create")) {
            // line 14
            echo "        ";
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_attribute_index", "params" => ["alias" => ($context["entityAlias"] ?? null)]]], 14, $context, $this->getSourceContext());
            // line 15
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_attribute_create", "params" => ["alias" =>             // line 17
($context["entityAlias"] ?? null)]]], 15, $context, $this->getSourceContext()));
            // line 19
            echo "    ";
        }
        // line 20
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_attribute_update")) {
            // line 21
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_attribute_update", "params" => ["id" => "\$id"]]], 21, $context, $this->getSourceContext()));
            // line 25
            echo "    ";
        }
        // line 26
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 26, $context, $this->getSourceContext());
        echo "
";
    }

    // line 29
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $context["entityTitle"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "id", [], "any", false, false, false, 30)) ? (_twig_default_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 31
($context["field_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 31)), twig_capitalize_string_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "fieldName", [], "any", false, false, false, 31)))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.attribute.entity_label")])));
        // line 34
        echo "
    ";
        // line 35
        $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_attribute_index", ["alias" =>         // line 37
($context["entityAlias"] ?? null)]), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 38
($context["attributesLabel"] ?? null)), "entityTitle" =>         // line 39
($context["entityTitle"] ?? null)];
        // line 41
        echo "
    ";
        // line 42
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Attribute/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 42,  115 => 41,  113 => 39,  112 => 38,  111 => 37,  110 => 35,  107 => 34,  105 => 31,  103 => 30,  99 => 29,  92 => 26,  89 => 25,  86 => 21,  83 => 20,  80 => 19,  78 => 17,  76 => 15,  73 => 14,  71 => 13,  67 => 12,  64 => 11,  61 => 10,  57 => 9,  52 => 1,  46 => 4,  43 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Attribute/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Attribute/update.html.twig");
    }
}
