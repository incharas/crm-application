<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/dropdown-control.html */
class __TwigTemplate_562e9a32c4b1c989f48b6d440419d343 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% var togglerId = _.uniqueId('dropdown-') %>
<li class=\"nav-item dropdown\" data-dropdown>
    <a href=\"#\" role=\"button\" id=\"<%- togglerId %>\" class=\"nav-link dropdown-toggle\" data-toggle=\"dropdown\"
       data-dropdown-placeholder=\"<%- label %>\"
       data-flip=\"true\"
       data-position-fixed=\"true\"
       aria-label=\"<%- label %>\"
       aria-haspopup=\"true\" aria-expanded=\"false\"><span data-dropdown-label><%- label %></span></a>
    <ul class=\"dropdown-menu dropdown-menu-right\" role=\"menu\" aria-labelledby=\"<%- togglerId %>\" data-dropdown-wrapper></ul>
</li>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/dropdown-control.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/dropdown-control.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/dropdown-control.html");
    }
}
