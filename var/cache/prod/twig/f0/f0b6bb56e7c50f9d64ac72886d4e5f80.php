<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/bootstrap.scss */
class __TwigTemplate_b5940249673cfa17e566c925c3793889 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */
// Base file for import of customized bootstrap settings

// Required
@import 'functions';
@import 'variables';
@import 'mixins';

// Optional
@import '~@oroinc/bootstrap/scss/root';
@import 'reboot';
@import '~@oroinc/bootstrap/scss/type';
@import '~@oroinc/bootstrap/scss/images';
@import '~@oroinc/bootstrap/scss/code';
@import '~@oroinc/bootstrap/scss/forms';
@import '~@oroinc/bootstrap/scss/transitions';
@import 'dropdown';
@import 'buttons';
@import 'button-group';
@import '~@oroinc/bootstrap/scss/input-group';
@import '~@oroinc/bootstrap/scss/custom-forms';
@import '~@oroinc/bootstrap/scss/nav';
@import '~@oroinc/bootstrap/scss/card';
@import 'breadcrumb';
@import 'pagination';
@import 'badge';
@import 'alert';
@import '~@oroinc/bootstrap/scss/progress';
@import '~@oroinc/bootstrap/scss/media';
@import '~@oroinc/bootstrap/scss/list-group';
@import '~@oroinc/bootstrap/scss/close';
@import 'modal';
@import '~@oroinc/bootstrap/scss/tooltip';
@import 'popover';
@import '~@oroinc/bootstrap/scss/carousel';
@import '~@oroinc/bootstrap/scss/utilities';
@import '~@oroinc/bootstrap/scss/print';

// Must be the last in the list
@import 'placeholders';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/bootstrap.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/bootstrap.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/bootstrap.scss");
    }
}
