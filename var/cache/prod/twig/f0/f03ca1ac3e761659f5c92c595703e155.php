<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/module-config.js */
class __TwigTemplate_62d133f63b632b35c52e903d8cec60ef extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import staticConfig from 'configs.json';

let config;

export default function(moduleId) {
    if (!config) {
        config = combineConfig();
    }
    if (!config[moduleId] && console && console.warn) {
        console.warn('Missing config for module \"' + moduleId + '\"');
    }
    return config[moduleId] || {};
};

/**
 * Combines config by from statically defined part and
 * config extends defined within page HTML
 *
 * @return {Object}
 */
function combineConfig() {
    const config = mergeConfig({}, staticConfig);
    // makes temp config mep where keys are module names but values are same config options
    const tempConfig = Object.values(config)
        .map(function(moduleConfig) {
            const moduleName = moduleConfig.__moduleName;
            delete moduleConfig.__moduleName;
            return [moduleName, moduleConfig];
        })
        .reduce(function(obj, entry) {
            obj[entry[0]] = entry[1];
            return obj;
        }, {});
    mergeConfig(tempConfig, fetchConfigExtends());
    return config;
}

/**
 * Merges objects recursively,
 * arrays are treated as scalars -- previous values gets overwritten
 *
 * @param {Object} config
 * @param {Object} update
 * @return {Object}
 */
function mergeConfig(config, update) {
    let propName;
    for (propName in update) {
        if (!update.hasOwnProperty(propName)) {
            continue;
        }
        if (update[propName] != null && update[propName].toString() === '[object Object]') {
            if (!config[propName]) {
                config[propName] = {};
            }
            mergeConfig(config[propName], update[propName]);
        } else {
            config[propName] = update[propName];
        }
    }

    return config;
}

/**
 * Fetches config defined in HTML
 *
 * @return {Object}
 */
function fetchConfigExtends() {
    const configExtends = {};
    const selector = 'script[type=\"application/json\"][data-role=\"config\"]';
    const nodes = document.querySelectorAll(selector);

    Array.prototype.forEach.call(nodes, function(node) {
        let configItem;
        try {
            configItem = JSON.parse(node.text);
        } catch (e) {
            console.warn('Ignored invalid inline config extend');
        }

        mergeConfig(configExtends, configItem);
    });

    return configExtends;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/module-config.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/module-config.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/module-config.js");
    }
}
