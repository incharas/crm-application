<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/main-menu/main-menu-toggler.scss */
class __TwigTemplate_b3bc12a476b6cd717fbec7bfd0898bfa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.main-menu-toggler {
    position: absolute;
    bottom: 0;
    right: 0;

    width: 48px;
    padding-top: 14px;
    padding-bottom: 14px;

    background-color: transparent;

    @extend %main-menu-trigger;

    @include fa-icon(\$menu-icon-toggler, before, true, true) {
        display: inline-block;
        transform: rotate(\$menu-icon-toggler-transform);
    }

    &:hover,
    &:focus {
        background-color: \$menu-dropdown-background-color-active;
    }
}

.minimized {
    .main-menu-toggler {
        width: 100%;

        &::before {
            transform: rotate(\$menu-icon-toggler-transform-active);
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/main-menu/main-menu-toggler.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/main-menu/main-menu-toggler.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/main-menu/main-menu-toggler.scss");
    }
}
