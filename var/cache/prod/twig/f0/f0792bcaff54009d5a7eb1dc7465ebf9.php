<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmbeddedForm/layouts/embedded_default/form_inline.html.twig */
class __TwigTemplate_b28f916abc86ab718722899b3dda80fd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_form_css_widget' => [$this, 'block__form_css_widget'],
            'embed_form_success_widget' => [$this, 'block_embed_form_success_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_form_css_widget', $context, $blocks);
        // line 5
        echo "
";
        // line 6
        $this->displayBlock('embed_form_success_widget', $context, $blocks);
    }

    // line 1
    public function block__form_css_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["content"] = twig_striptags(($context["content"] ?? null));
        // line 3
        echo "    ";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget', ["content" => ($context["content"] ?? null)]);
        echo "
";
    }

    // line 6
    public function block_embed_form_success_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->extensions['Oro\Bundle\EmbeddedFormBundle\Twig\BackLinkExtension']->backLinkFilter(((array_key_exists("message", $context)) ? (_twig_default_filter(($context["message"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.success_message.default"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.success_message.default")))));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmbeddedForm/layouts/embedded_default/form_inline.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  66 => 7,  62 => 6,  55 => 3,  52 => 2,  48 => 1,  44 => 6,  41 => 5,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmbeddedForm/layouts/embedded_default/form_inline.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/form_inline.html.twig");
    }
}
