<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables/scrollspy-variables.scss */
class __TwigTemplate_360ccb38bdd048648f2c21deb9afc592 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$scrollspy-mobile-accordion-group-offset: 8px !default;

\$scrollspy-mobile-toggle-offset: 8px 16px !default;
\$scrollspy-mobile-border-radius: 4px !default;

\$scrollspy-mobile-toggle-icon-width: 12px !default;
\$scrollspy-mobile-toggle-icon-offset: 0 8px 0 0 !default;
\$scrollspy-mobile-toggle-icon-font-size: 18px !default;
\$scrollspy-mobile-toggle-icon-color: \$primary-200 !default;
\$scrollspy-mobile-toggle-icon-text-align: center !default;

\$scrollspy-mobile-toggle-icon: \$fa-var-angle-down !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables/scrollspy-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables/scrollspy-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables/scrollspy-variables.scss");
    }
}
