<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroIntegration/Form/fields.html.twig */
class __TwigTemplate_2f6830cd9613e92e56c5912b0bd898f1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_integration_channel_form_widget' => [$this, 'block_oro_integration_channel_form_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        $this->displayBlock('oro_integration_channel_form_widget', $context, $blocks);
    }

    public function block_oro_integration_channel_form_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "
    ";
        // line 8
        if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 8), "vars", [], "any", false, false, false, 8), "choices", [], "any", false, false, false, 8))) {
            // line 9
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 9), 'row', ["attr" => ["disabled" => true]]);
            echo "
    ";
        } else {
            // line 11
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 11), 'row');
            echo "
    ";
        }
        // line 13
        echo "
    ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 14), 'row');
        echo "

    ";
        // line 16
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", true, true, false, 16)) {
            // line 17
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", false, false, false, 17), 'row');
            echo "
    ";
        }
        // line 19
        echo "
    ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "transportType", [], "any", false, false, false, 20), 'row');
        echo "

    ";
        // line 22
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "transport", [], "any", true, true, false, 22)) {
            // line 23
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "transport", [], "any", false, false, false, 23), 'widget', ["attr" => ["class" => "control-group-container"]]);
            echo "
    ";
        }
        // line 25
        echo "
    ";
        // line 26
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "synchronizationSettings", [], "any", true, true, false, 26) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "synchronizationSettings", [], "any", false, false, false, 26), "rendered", [], "any", false, false, false, 26))) {
            // line 27
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "synchronizationSettings", [], "any", false, false, false, 27), 'widget');
            echo "
    ";
        }
        // line 29
        echo "
    ";
        // line 30
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mappingSettings", [], "any", true, true, false, 30) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mappingSettings", [], "any", false, false, false, 30), "rendered", [], "any", false, false, false, 30))) {
            // line 31
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mappingSettings", [], "any", false, false, false, 31), 'widget');
            echo "
    ";
        }
        // line 33
        echo "
    ";
        // line 34
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "connectors", [], "any", true, true, false, 34)) {
            // line 35
            echo "        <div class=\"control-group-container ";
            if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "connectors", [], "any", false, false, false, 35), "vars", [], "any", false, false, false, 35), "choices", [], "any", false, false, false, 35))) {
                echo "hide";
            }
            echo "\">
            ";
            // line 36
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "connectors", [], "any", false, false, false, 36), 'row');
            echo "
        </div>
    ";
        }
        // line 39
        echo "
    ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "

    ";
        // line 42
        $macros["jsRenderer"] = $this->loadTemplate("@OroIntegration/Form/javascript.html.twig", "@OroIntegration/Form/fields.html.twig", 42)->unwrap();
        // line 43
        echo "    ";
        echo twig_call_macro($macros["jsRenderer"], "macro_renderIntegrationFormJS", [($context["form"] ?? null)], 43, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroIntegration/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  144 => 43,  142 => 42,  137 => 40,  134 => 39,  128 => 36,  121 => 35,  119 => 34,  116 => 33,  110 => 31,  108 => 30,  105 => 29,  99 => 27,  97 => 26,  94 => 25,  88 => 23,  86 => 22,  81 => 20,  78 => 19,  72 => 17,  70 => 16,  65 => 14,  62 => 13,  56 => 11,  50 => 9,  48 => 8,  45 => 7,  38 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroIntegration/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Resources/views/Form/fields.html.twig");
    }
}
