<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAnalytics/RFMCategory/channelView.html.twig */
class __TwigTemplate_c7ed5d6e81910e29e5e72884fbb2cb37 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"responsive-cell clearfix\">
    <div class=\"span6\">
        <h4>";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.rfm_setting.title"), "html", null, true);
        echo "</h4>
        <div class=\"rfm-settings rfm-enabled\">
            <div class=\"rfm-settings-data\">
                ";
        // line 6
        if ((($context["rfmCategoriesCount"] ?? null) > 1)) {
            // line 7
            echo "                <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\">
                    <thead>
                    <tr>
                        <th><label>";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.score.title"), "html", null, true);
            echo "</label></th>
                        <th>
                            <label>";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.recency.label"), "html", null, true);
            echo "</label>
                            ";
            // line 13
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.hint.recency"), "html", null, true);
            echo "
                        </th>
                        <th>
                            <label>";
            // line 16
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.frequency.label"), "html", null, true);
            echo "</label>
                            ";
            // line 17
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.hint.frequency"), "html", null, true);
            echo "
                        </th>
                        <th>
                            <label>";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.monetary.label"), "html", null, true);
            echo "</label>
                            ";
            // line 21
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.hint.monetary"), "html", null, true);
            echo "
                        </th>
                    </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class=\"rfm-cell-index\">1
                                <br><small>";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.best"), "html", null, true);
            echo "</small>
                            </td>
                            <td class=\"rfm-cell-recency\">
                                <span>";
            // line 31
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.less"), "html", null, true);
            echo " </span>
                                <strong>";
            // line 32
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "recency", [], "any", false, false, false, 32)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null), "maxValue", [], "any", false, false, false, 32), "html", null, true);
            echo "</strong>
                            </td>
                            <td class=\"rfm-cell-frequency\">
                                <span>";
            // line 35
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.more"), "html", null, true);
            echo " </span>
                                <strong>";
            // line 36
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_1 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "frequency", [], "any", false, false, false, 36)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[0] ?? null) : null), "minValue", [], "any", false, false, false, 36), "html", null, true);
            echo "</strong>
                            <td class=\"rfm-cell-monetary\">
                                <span>";
            // line 38
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.more"), "html", null, true);
            echo " </span>
                                <strong>";
            // line 39
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_2 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "monetary", [], "any", false, false, false, 39)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2[0] ?? null) : null), "minValue", [], "any", false, false, false, 39), "html", null, true);
            echo "</strong>
                            </td>
                        </tr>

                        ";
            // line 43
            if ((($context["rfmCategoriesCount"] ?? null) > 2)) {
                // line 44
                echo "                            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(twig_slice($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "recency", [], "any", false, false, false, 44), 1,  -1));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["rfmCategory"]) {
                    // line 45
                    echo "                                <tr>
                                    <td class=\"rfm-cell-index\">";
                    // line 46
                    echo twig_escape_filter($this->env, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 46) + 1), "html", null, true);
                    echo "</td>
                                    <td class=\"rfm-cell-recency\">
                                        <span>";
                    // line 48
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.from"), "html", null, true);
                    echo "</span>
                                        <strong>";
                    // line 49
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_3 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "recency", [], "any", false, false, false, 49)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 49)] ?? null) : null), "minValue", [], "any", false, false, false, 49), "html", null, true);
                    echo "</strong>
                                        <br><span>";
                    // line 50
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.to"), "html", null, true);
                    echo "</span>
                                        <strong>";
                    // line 51
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_4 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "recency", [], "any", false, false, false, 51)) && is_array($__internal_compile_4) || $__internal_compile_4 instanceof ArrayAccess ? ($__internal_compile_4[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 51)] ?? null) : null), "maxValue", [], "any", false, false, false, 51), "html", null, true);
                    echo "</strong>
                                    </td>
                                    <td class=\"rfm-cell-frequency\">
                                        <span>";
                    // line 54
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.from"), "html", null, true);
                    echo "</span>
                                        <strong>";
                    // line 55
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_5 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "frequency", [], "any", false, false, false, 55)) && is_array($__internal_compile_5) || $__internal_compile_5 instanceof ArrayAccess ? ($__internal_compile_5[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 55)] ?? null) : null), "minValue", [], "any", false, false, false, 55), "html", null, true);
                    echo "</strong>
                                        <br><span>";
                    // line 56
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.to"), "html", null, true);
                    echo "</span>
                                        <strong>";
                    // line 57
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_6 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "frequency", [], "any", false, false, false, 57)) && is_array($__internal_compile_6) || $__internal_compile_6 instanceof ArrayAccess ? ($__internal_compile_6[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 57)] ?? null) : null), "maxValue", [], "any", false, false, false, 57), "html", null, true);
                    echo "</strong>
                                    <td class=\"rfm-cell-monetary\">
                                        <span>";
                    // line 59
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.from"), "html", null, true);
                    echo "</span>
                                        <strong>";
                    // line 60
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_7 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "monetary", [], "any", false, false, false, 60)) && is_array($__internal_compile_7) || $__internal_compile_7 instanceof ArrayAccess ? ($__internal_compile_7[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 60)] ?? null) : null), "minValue", [], "any", false, false, false, 60), "html", null, true);
                    echo "</strong>
                                        <br><span>";
                    // line 61
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.to"), "html", null, true);
                    echo "</span>
                                        <strong>";
                    // line 62
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_8 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "monetary", [], "any", false, false, false, 62)) && is_array($__internal_compile_8) || $__internal_compile_8 instanceof ArrayAccess ? ($__internal_compile_8[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 62)] ?? null) : null), "maxValue", [], "any", false, false, false, 62), "html", null, true);
                    echo "</strong>
                                    </td>
                                </tr>
                            ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rfmCategory'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 66
                echo "                        ";
            }
            // line 67
            echo "
                        <tr>
                            <td class=\"rfm-cell-index\">";
            // line 69
            echo twig_escape_filter($this->env, ($context["rfmCategoriesCount"] ?? null), "html", null, true);
            echo "
                                <br><small>";
            // line 70
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.worst"), "html", null, true);
            echo "</small>
                            </td>
                            <td class=\"rfm-cell-recency\">
                                <span>";
            // line 73
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.more"), "html", null, true);
            echo " </span>
                                <strong>";
            // line 74
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_9 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "recency", [], "any", false, false, false, 74)) && is_array($__internal_compile_9) || $__internal_compile_9 instanceof ArrayAccess ? ($__internal_compile_9[(($context["rfmCategoriesCount"] ?? null) - 1)] ?? null) : null), "minValue", [], "any", false, false, false, 74), "html", null, true);
            echo "</strong>
                            </td>
                            <td class=\"rfm-cell-frequency\">
                                <span>";
            // line 77
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.less"), "html", null, true);
            echo " </span>
                                <strong>";
            // line 78
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_10 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "frequency", [], "any", false, false, false, 78)) && is_array($__internal_compile_10) || $__internal_compile_10 instanceof ArrayAccess ? ($__internal_compile_10[(($context["rfmCategoriesCount"] ?? null) - 1)] ?? null) : null), "maxValue", [], "any", false, false, false, 78), "html", null, true);
            echo "</strong>
                            <td class=\"rfm-cell-monetary\">
                                <span>";
            // line 80
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.less"), "html", null, true);
            echo " </span>
                                <strong>";
            // line 81
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_11 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["rfmCategories"] ?? null), "monetary", [], "any", false, false, false, 81)) && is_array($__internal_compile_11) || $__internal_compile_11 instanceof ArrayAccess ? ($__internal_compile_11[(($context["rfmCategoriesCount"] ?? null) - 1)] ?? null) : null), "maxValue", [], "any", false, false, false, 81), "html", null, true);
            echo "</strong>
                            </td>
                        </tr>
                    </tbody>
                </table>
                ";
        }
        // line 87
        echo "            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroAnalytics/RFMCategory/channelView.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 87,  262 => 81,  258 => 80,  253 => 78,  249 => 77,  243 => 74,  239 => 73,  233 => 70,  229 => 69,  225 => 67,  222 => 66,  204 => 62,  200 => 61,  196 => 60,  192 => 59,  187 => 57,  183 => 56,  179 => 55,  175 => 54,  169 => 51,  165 => 50,  161 => 49,  157 => 48,  152 => 46,  149 => 45,  131 => 44,  129 => 43,  122 => 39,  118 => 38,  113 => 36,  109 => 35,  103 => 32,  99 => 31,  93 => 28,  83 => 21,  79 => 20,  73 => 17,  69 => 16,  63 => 13,  59 => 12,  54 => 10,  49 => 7,  47 => 6,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAnalytics/RFMCategory/channelView.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AnalyticsBundle/Resources/views/RFMCategory/channelView.html.twig");
    }
}
