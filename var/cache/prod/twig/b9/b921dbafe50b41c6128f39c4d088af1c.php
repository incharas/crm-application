<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/update.html.twig */
class __TwigTemplate_4cfd6198b3f2d560c1226bcae42b0bf7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["invitations"] = $this->macros["invitations"] = $this->loadTemplate("@OroCalendar/invitations.html.twig", "@OroCalendar/CalendarEvent/update.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.title%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "title", [], "any", false, false, false, 4), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_label")]]);
        // line 5
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroCalendar/CalendarEvent/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/update.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_calendar_event_view", "params" => ["id" => "\$id"]]], 10, $context, $this->getSourceContext());
        // line 14
        echo "
    ";
        // line 15
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_calendar_event_create")) {
            // line 16
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_calendar_event_create"]], 16, $context, $this->getSourceContext()));
            // line 19
            echo "    ";
        }
        // line 20
        echo "
    ";
        // line 21
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_calendar_event_update")) {
            // line 22
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_calendar_event_update", "params" => ["id" => "\$id"]]], 22, $context, $this->getSourceContext()));
            // line 26
            echo "    ";
        }
        // line 27
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 27, $context, $this->getSourceContext());
        echo "
    ";
        // line 28
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_index")], 28, $context, $this->getSourceContext());
        echo "
";
    }

    // line 31
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 33
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 34
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 37
($context["entity"] ?? null), "title", [], "any", false, false, false, 37)];
            // line 39
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 41
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_label")]);
            // line 42
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroCalendar/CalendarEvent/update.html.twig", 42)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 43
            echo "    ";
        }
    }

    // line 46
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/update.html.twig", 47)->unwrap();
        // line 48
        echo "
    ";
        // line 49
        $context["id"] = "calendarevent-form";
        // line 50
        echo "    ";
        $context["calendarEventDateRange"] = ["module" => "orocalendar/js/app/components/calendar-event-date-range-component", "name" => "calendar-event-date-range", "options" => ["nativeMode" => $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()]];
        // line 57
        echo "
    ";
        // line 58
        $context["general_subblocks_data"] = [];
        // line 59
        echo "
    ";
        // line 60
        if (( !($context["entityId"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendar", [], "any", true, true, false, 60))) {
            // line 61
            echo "        ";
            $context["general_subblocks_data"] = twig_array_merge(($context["general_subblocks_data"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendar", [], "any", false, false, false, 61), 'row')]);
            // line 62
            echo "    ";
        }
        // line 63
        echo "
    ";
        // line 64
        $context["general_subblocks_data"] = twig_array_merge(($context["general_subblocks_data"] ?? null), [0 =>         // line 65
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 65), 'row'), 1 =>         // line 66
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 66), 'row'), 2 =>         // line 67
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "backgroundColor", [], "any", false, false, false, 67), 'row'), 3 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 68
($context["form"] ?? null), "calendarUid", [], "any", true, true, false, 68)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendarUid", [], "any", false, false, false, 68), 'row')) : (null)), 4 =>         // line 69
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "attendees", [], "any", false, false, false, 69), 'row'), 5 => (((null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 70
($context["entity"] ?? null), "recurrence", [], "any", false, false, false, 70))) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "reminders", [], "any", false, false, false, 70), 'row')) : (null)), 6 =>         // line 71
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "notifyAttendees", [], "any", false, false, false, 71), 'row'), 7 => twig_call_macro($macros["invitations"], "macro_notify_attendees_component", [], 72, $context, $this->getSourceContext())]);
        // line 74
        echo "
    ";
        // line 76
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" =>         // line 82
($context["general_subblocks_data"] ?? null)], 1 => ["title" => "", "data" => [0 => (((((("<div " . twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [        // line 87
($context["calendarEventDateRange"] ?? null)], 87, $context, $this->getSourceContext())) . ">") .         // line 88
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "start", [], "any", false, false, false, 88), 'row')) .         // line 89
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "end", [], "any", false, false, false, 89), 'row')) .         // line 90
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "allDay", [], "any", false, false, false, 90), 'row')) . "</div>"), 1 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 92
($context["form"] ?? null), "recurrence", [], "any", true, true, false, 92)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recurrence", [], "any", false, false, false, 92), 'row')) : (null))]]]]];
        // line 97
        echo "
    ";
        // line 98
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 99
        echo "
    ";
        // line 100
        $context["data"] = ["formErrors" => ((        // line 101
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 102
($context["dataBlocks"] ?? null)];
        // line 104
        echo "
    ";
        // line 105
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  200 => 105,  197 => 104,  195 => 102,  194 => 101,  193 => 100,  190 => 99,  188 => 98,  185 => 97,  183 => 92,  182 => 90,  181 => 89,  180 => 88,  179 => 87,  178 => 82,  176 => 76,  173 => 74,  171 => 71,  170 => 70,  169 => 69,  168 => 68,  167 => 67,  166 => 66,  165 => 65,  164 => 64,  161 => 63,  158 => 62,  155 => 61,  153 => 60,  150 => 59,  148 => 58,  145 => 57,  142 => 50,  140 => 49,  137 => 48,  134 => 47,  130 => 46,  125 => 43,  122 => 42,  119 => 41,  113 => 39,  111 => 37,  110 => 34,  108 => 33,  105 => 32,  101 => 31,  95 => 28,  90 => 27,  87 => 26,  84 => 22,  82 => 21,  79 => 20,  76 => 19,  73 => 16,  71 => 15,  68 => 14,  66 => 10,  63 => 9,  60 => 8,  56 => 7,  51 => 1,  49 => 5,  47 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/update.html.twig");
    }
}
