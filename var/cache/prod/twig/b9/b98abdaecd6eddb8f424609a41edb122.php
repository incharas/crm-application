<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/themes/demo/settings.yml */
class __TwigTemplate_f966057c85e8bd1c831513cca1cb259d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "label: Demo Theme
logo: bundles/oroui/themes/oro/images/oro-platform-logo.svg
icon: bundles/oroui/themes/oro/images/favicon.ico
styles:
    css:
        inputs:
            - bundles/oroui/themes/demo/css/scss/settings/primary-settings
        output: css/oro.css
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/themes/demo/settings.yml";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/themes/demo/settings.yml", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/themes/demo/settings.yml");
    }
}
