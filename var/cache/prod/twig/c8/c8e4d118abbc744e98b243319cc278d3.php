<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/widget-picker-component.js */
class __TwigTemplate_184e3d1645f746f3cf3e352260503178 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseComponent = require('oroui/js/app/components/base/component');
    const WidgetPickerFilterModel = require('oroui/js/app/models/widget-picker/widget-picker-filter-model');
    const WidgetPickerCollectionView = require('oroui/js/app/views/widget-picker/widget-picker-collection-view');
    const WidgetPickerFilterView = require('oroui/js/app/views/widget-picker/widget-picker-filter-view');
    const _ = require('underscore');

    /**
     * @export oroui/js/app/components/widget-picker-component
     * @extends oroui.app.components.base.Component
     * @class oroui.app.components.WidgetPickerComponent
     */
    const WidgetPickerComponent = BaseComponent.extend({
        /**
         * @inheritdoc
         */
        constructor: function WidgetPickerComponent(options) {
            WidgetPickerComponent.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this._createViews(options);
            WidgetPickerComponent.__super__.initialize.call(this, options);
        },

        /**
         *
         * @param {Array} options
         * @protected
         */
        _createViews: function(options) {
            const \$el = options._sourceElement;
            const widgetPickerFilterModel = new WidgetPickerFilterModel();
            this.widgetPickerCollectionView = new WidgetPickerCollectionView(
                _.defaults(options, {
                    el: \$el.find('[data-role=\"widget-picker-container\"]'),
                    filterModel: widgetPickerFilterModel,
                    listSelector: '[data-role=\"widget-picker-results\"]',
                    fallbackSelector: '[data-role=\"widget-picker-no-results-found\"]'
                })
            );
            this.widgetPickerFilterView = new WidgetPickerFilterView({
                el: \$el.find('[data-role=\"widget-picker-filter\"]'),
                model: widgetPickerFilterModel
            });
        }
    });

    return WidgetPickerComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/widget-picker-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/widget-picker-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/widget-picker-component.js");
    }
}
