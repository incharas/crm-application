<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/Extension/Formatter/Property/linkProperty.html.twig */
class __TwigTemplate_8325ca6923849802b30c8a35518ea1b9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ( !twig_test_empty(($context["url"] ?? null))) {
            // line 2
            echo "    <a href=\"";
            echo twig_escape_filter($this->env, ($context["url"] ?? null), "html", null, true);
            echo "\">
        ";
            // line 3
            echo twig_escape_filter($this->env, ((array_key_exists("label", $context)) ? (_twig_default_filter(($context["label"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.grid.open_link"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.grid.open_link"))), "html", null, true);
            echo "
    </a>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/Extension/Formatter/Property/linkProperty.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/Extension/Formatter/Property/linkProperty.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/Extension/Formatter/Property/linkProperty.html.twig");
    }
}
