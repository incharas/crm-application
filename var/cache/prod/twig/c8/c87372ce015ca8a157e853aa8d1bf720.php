<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Call/widget/update.html.twig */
class __TwigTemplate_1a3f95f3519c99b61df631ce58b5286f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("saved", $context) && ($context["saved"] ?? null))) {
            // line 2
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Call logged successfully"), "triggerSuccess" => true, "remove" => true]];
            // line 9
            echo "
    ";
            // line 10
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
";
        } else {
            // line 12
            echo "    <div class=\"widget-content\">
        ";
            // line 13
            if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "valid", [], "any", false, false, false, 13) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "errors", [], "any", false, false, false, 13)))) {
                // line 14
                echo "            <div class=\"alert alert-error\" role=\"alert\">
                <div class=\"message\">
                    ";
                // line 16
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
                echo "
                </div>
            </div>
        ";
            }
            // line 20
            echo "        <div class=\"form-container\">
            <form id=\"";
            // line 21
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "id", [], "any", false, false, false, 21), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "name", [], "any", false, false, false, 21), "html", null, true);
            echo "\" action=\"";
            echo twig_escape_filter($this->env, ($context["formAction"] ?? null), "html", null, true);
            echo "\" method=\"post\">
                <fieldset class=\"form form-horizontal\">
                    <div class=\"span6\">
                        ";
            // line 24
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", true, true, false, 24)) {
                // line 25
                echo "                            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 25), 'row');
                echo "
                        ";
            }
            // line 27
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subject", [], "any", false, false, false, 27), 'row');
            echo "
                        ";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "notes", [], "any", false, false, false, 28), 'row', ["attr" => ["class" => "narrow-text-field"]]);
            echo "
                    </div>
                    <div class=\"span6\">
                        ";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "callDateTime", [], "any", false, false, false, 31), 'row');
            echo "
                        ";
            // line 32
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phoneNumber", [], "any", false, false, false, 32), 'row');
            echo "
                        ";
            // line 33
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "direction", [], "any", false, false, false, 33), 'row');
            echo "
                        ";
            // line 34
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "duration", [], "any", false, false, false, 34), 'row');
            echo "
                        <div class=\"hide\">
                            ";
            // line 36
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "callStatus", [], "any", false, false, false, 36), 'row');
            echo "
                        </div>
                    </div>
                    <div class=\"span6\">
                        ";
            // line 40
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
                    </div>
                    <div class=\"span6\">
                        ";
            // line 43
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("call_log_widget_additional_properties", $context)) ? (_twig_default_filter(($context["call_log_widget_additional_properties"] ?? null), "call_log_widget_additional_properties")) : ("call_log_widget_additional_properties")), ["entity" => ($context["entity"] ?? null), "form" => ($context["form"] ?? null), "formAction" => ($context["formAction"] ?? null), "saved" => ($context["saved"] ?? null)]);
            // line 44
            echo "                    </div>

                    <div class=\"widget-actions form-actions\">
                        <button class=\"btn\" type=\"reset\">";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
            echo "</button>
                        ";
            // line 48
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 48), "value", [], "any", false, false, false, 48), "id", [], "any", false, false, false, 48)) {
                // line 49
                echo "                        <button class=\"btn btn-success\" type=\"submit\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.update_log_call"), "html", null, true);
                echo "</button>
                        ";
            } else {
                // line 51
                echo "                        <button class=\"btn btn-success\" type=\"submit\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.log_call"), "html", null, true);
                echo "</button>
                        ";
            }
            // line 53
            echo "                        ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("call_log_widget_update_actions", $context)) ? (_twig_default_filter(($context["call_log_widget_update_actions"] ?? null), "call_log_widget_update_actions")) : ("call_log_widget_update_actions")), ["entity" => ($context["entity"] ?? null), "form" => ($context["form"] ?? null), "formAction" => ($context["formAction"] ?? null), "saved" => ($context["saved"] ?? null)]);
            // line 54
            echo "                    </div>
                </fieldset>
            </form>
            ";
            // line 57
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCall/Call/widget/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 57,  156 => 54,  153 => 53,  147 => 51,  141 => 49,  139 => 48,  135 => 47,  130 => 44,  128 => 43,  122 => 40,  115 => 36,  110 => 34,  106 => 33,  102 => 32,  98 => 31,  92 => 28,  87 => 27,  81 => 25,  79 => 24,  69 => 21,  66 => 20,  59 => 16,  55 => 14,  53 => 13,  50 => 12,  45 => 10,  42 => 9,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Call/widget/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Call/widget/update.html.twig");
    }
}
