<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/history.html.twig */
class __TwigTemplate_2de682284435dded6ee1cdb3c9f2271b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'list' => [$this, 'block_list'],
            'label' => [$this, 'block_label'],
            'list_wrapper' => [$this, 'block_list_wrapper'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/history.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_list($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/history.html.twig", 4)->unwrap();
        // line 5
        echo "    <ul";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["listAttributes"] ?? null)], 5, $context, $this->getSourceContext());
        echo ">";
        // line 6
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 6) &&  !(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 6) === 0)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 6))) {
            // line 7
            echo "            ";
            $macros["oro_menu"] = $this;
            // line 8
            echo "            ";
            $this->displayBlock("children", $context, $blocks);
            echo "
        ";
        }
        // line 10
        echo "</ul>
    ";
        // line 11
        if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 11) && (null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "parent", [], "any", false, false, false, 11)))) {
            // line 12
            echo "        <div class=\"no-data\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.messages.no_items"), "html", null, true);
            echo "</div>
    ";
        }
    }

    // line 16
    public function block_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "label", [], "any", false, false, false, 17)), "html", null, true);
        echo "
";
    }

    // line 20
    public function block_list_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 21) && (null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "parent", [], "any", false, false, false, 21)))) {
            // line 22
            echo "        ";
            $this->displayBlock("list", $context, $blocks);
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/history.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  101 => 22,  98 => 21,  94 => 20,  87 => 17,  83 => 16,  75 => 12,  73 => 11,  70 => 10,  64 => 8,  61 => 7,  59 => 6,  55 => 5,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/history.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/history.html.twig");
    }
}
