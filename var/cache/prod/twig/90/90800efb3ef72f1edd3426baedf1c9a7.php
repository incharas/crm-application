<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables/widget-picker-variables.scss */
class __TwigTemplate_78feae2a97534e3156b5c51121a7c212 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$widget-picker-mobile-description-cell-margin: 11px 7px 2px 32px !default;
\$widget-picker-mobile-item-padding-bottom: 57px !default;
\$widget-picker-mobile-summary-row-margin-bottom: 5px !default;
\$widget-picker-mobile-actions-column-bottom: 16px !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables/widget-picker-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables/widget-picker-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables/widget-picker-variables.scss");
    }
}
