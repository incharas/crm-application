<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Thread/view.html.twig */
class __TwigTemplate_e84d4459127412d8783a8ab41123b0d9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'pageActions' => [$this, 'block_pageActions'],
            'navButtons' => [$this, 'block_navButtons'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroEmail/Email/Thread/view.html.twig", 2)->unwrap();
        // line 3
        $macros["Actions"] = $this->macros["Actions"] = $this->loadTemplate("@OroEmail/actions.html.twig", "@OroEmail/Email/Thread/view.html.twig", 3)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%subject%" => $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["entity"] ?? null), "subject", [], "any", false, false, false, 5))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroEmail/Email/Thread/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 9
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_user_emails"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.entity_plural_label"), "entityTitle" => $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["entity"] ?? null), "subject", [], "any", false, false, false, 12))];
        // line 14
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 17
    public function block_pageActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    <li class=\"pull-right email-thread-action-panel\"></li>
";
    }

    // line 21
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroEmail/Email/Thread/view.html.twig", 22)->unwrap();
        // line 23
        echo "    ";
        $macros["Actions"] = $this->loadTemplate("@OroEmail/actions.html.twig", "@OroEmail/Email/Thread/view.html.twig", 23)->unwrap();
        // line 24
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/Thread/view.html.twig", 24)->unwrap();
        // line 25
        echo "
    ";
        // line 26
        ob_start(function () { return ''; });
        // line 27
        echo "        ";
        // line 28
        echo "        ";
        echo twig_call_macro($macros["AC"], "macro_addContextButton", [($context["entity"] ?? null)], 28, $context, $this->getSourceContext());
        echo "
        ";
        // line 29
        echo twig_call_macro($macros["Actions"], "macro_addMarkUnreadButton", [($context["entity"] ?? null)], 29, $context, $this->getSourceContext());
        echo "
    ";
        $context["buttonsHtml"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 31
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_pinnedDropdownButton", [["html" =>         // line 32
($context["buttonsHtml"] ?? null)]], 31, $context, $this->getSourceContext());
        // line 33
        echo "
";
    }

    // line 36
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    ";
        $context["breadcrumbs"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.menu.user_emails")]];
        // line 40
        echo "    ";
        $this->loadTemplate("@OroNavigation/Menu/breadcrumbs.html.twig", "@OroEmail/Email/Thread/view.html.twig", 40)->display($context);
    }

    // line 43
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroEmail/Email/Thread/view.html.twig", 44)->unwrap();
        // line 45
        echo "
    ";
        // line 47
        echo "    <li class=\"context-data activity-context-activity-block\">
        ";
        // line 48
        echo twig_call_macro($macros["AC"], "macro_activity_contexts", [($context["entity"] ?? null)], 48, $context, $this->getSourceContext());
        echo "
    </li>
";
    }

    // line 52
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/Thread/view.html.twig", 53)->unwrap();
        // line 54
        echo "
    ";
        // line 55
        ob_start(function () { return ''; });
        // line 56
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "wid" => "thread-view", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_thread_widget", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 59
($context["entity"] ?? null), "id", [], "any", false, false, false, 59), "renderContexts" => false, "showSingleEmail" =>  !$this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_email.threads_grouping")]), "alias" => "thread-view", "contextsRendered" => true]);
        // line 63
        echo "
    ";
        $context["emailInfoWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 65
        echo "
    ";
        // line 66
        ob_start(function () { return ''; });
        // line 67
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_activities", $context)) ? (_twig_default_filter(($context["view_content_data_activities"] ?? null), "view_content_data_activities")) : ("view_content_data_activities")), ["entity" => ($context["entity"] ?? null)]);
        // line 68
        echo "    ";
        $context["activitiesData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 69
        echo "
    ";
        // line 70
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 75
($context["emailInfoWidget"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.sections.activities"), "subblocks" => [0 => ["spanClass" => "widget-content email-activity-widget", "data" => [0 =>         // line 82
($context["activitiesData"] ?? null)]]]]];
        // line 86
        echo "
    ";
        // line 87
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 88
        echo "    ";
        $context["id"] = "threadEmails";
        // line 89
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_scrollData", [($context["id"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null)], 89, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Thread/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 89,  198 => 88,  196 => 87,  193 => 86,  191 => 82,  190 => 75,  189 => 70,  186 => 69,  183 => 68,  180 => 67,  178 => 66,  175 => 65,  171 => 63,  169 => 59,  167 => 56,  165 => 55,  162 => 54,  159 => 53,  155 => 52,  148 => 48,  145 => 47,  142 => 45,  139 => 44,  135 => 43,  130 => 40,  127 => 37,  123 => 36,  118 => 33,  116 => 32,  114 => 31,  109 => 29,  104 => 28,  102 => 27,  100 => 26,  97 => 25,  94 => 24,  91 => 23,  88 => 22,  84 => 21,  79 => 18,  75 => 17,  68 => 14,  66 => 12,  65 => 9,  63 => 8,  59 => 7,  54 => 1,  52 => 5,  49 => 3,  47 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Thread/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Thread/view.html.twig");
    }
}
