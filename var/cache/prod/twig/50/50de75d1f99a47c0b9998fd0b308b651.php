<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/AddressBook/connectionButtons.html.twig */
class __TwigTemplate_e2370386377c2fcdfe633c1b27dd7944 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/AddressBook/connectionButtons.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"btn-group pull-left dotmailer-group\">
    ";
        // line 4
        if (($context["addressBook"] ?? null)) {
            // line 5
            echo "        ";
            ob_start(function () { return ''; });
            // line 6
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_dropdownItem", [["path" => "#", "data" => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_synchronize_adddress_book", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["addressBook"] ?? null), "id", [], "any", false, false, false, 9)]), "success-message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.message.syncronize_scheduled"), "fail-message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.message.syncronize_schedule_failed"), "action" => "sync-with-dotmailer"], "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.button.synchronize"), "class" => "no-hash dotmailer-sync-btn", "iCss" => "fa-refresh"]], 6, $context, $this->getSourceContext());
            // line 17
            echo "

        ";
            // line 19
            echo twig_call_macro($macros["UI"], "macro_dropdownItem", [["path" => "#", "data" => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_synchronize_adddress_book_datafields", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["addressBook"] ?? null), "id", [], "any", false, false, false, 22)]), "action" => "sync-with-dotmailer"], "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.addressbook.sync_datafields"), "class" => "no-hash dotmailer-sync-btn", "iCss" => "fa-refresh"]], 19, $context, $this->getSourceContext());
            // line 28
            echo "

        ";
            // line 30
            echo twig_call_macro($macros["UI"], "macro_dropdownItem", [["path" => "#", "data" => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_marketing_list_connect", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 33
($context["marketingList"] ?? null), "id", [], "any", false, false, false, 33)]), "action" => "connect-with-dotmailer-setting-update", "message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.message.update"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.dialog.title.update")], "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.button.manage_connection"), "class" => "no-hash dotmailer-sync-btn", "iCss" => "fa-pencil-square-o"]], 30, $context, $this->getSourceContext());
            // line 41
            echo "

        <li>
            ";
            // line 44
            echo twig_call_macro($macros["UI"], "macro_deleteLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_marketing_list_disconnect", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 45
($context["addressBook"] ?? null), "id", [], "any", false, false, false, 45)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 46
($context["marketingList"] ?? null), "id", [], "any", false, false, false, 46)]), "dataMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.confirmation.disconnect"), "aCss" => "no-hash remove-button", "id" => "btn-remove-dotmailer-connection", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.name"), "successMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.message.disconnect"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.button.disconnect"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.button.disconnect")]], 44, $context, $this->getSourceContext());
            // line 54
            echo "
        </li>
        ";
            $context["html"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 57
            echo "
        ";
            // line 58
            echo twig_call_macro($macros["UI"], "macro_dropdownButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.button.group_name"), "iCss" => "fa-cog", "html" =>             // line 61
($context["html"] ?? null)]], 58, $context, $this->getSourceContext());
            // line 62
            echo "
    ";
        } else {
            // line 64
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_link", [["path" => "#", "data" => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_marketing_list_connect", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 67
($context["marketingList"] ?? null), "id", [], "any", false, false, false, 67)]), "action" => "connect-with-dotmailer-setting-update", "message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.message.connect"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.dialog.title.connect")], "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.button.connect"), "class" => "no-hash btn dotmailer-sync-btn", "iCss" => "icon-dotmailer"]], 64, $context, $this->getSourceContext());
            // line 75
            echo "
    ";
        }
        // line 77
        echo "</div>
<script type=\"text/javascript\">
    loadModules(['orodotmailer/js/sync-buttons-handler'], function(Handler){
        new Handler('.dotmailer-sync-btn');
    });
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/AddressBook/connectionButtons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 77,  94 => 75,  92 => 67,  90 => 64,  86 => 62,  84 => 61,  83 => 58,  80 => 57,  75 => 54,  73 => 46,  72 => 45,  71 => 44,  66 => 41,  64 => 33,  63 => 30,  59 => 28,  57 => 22,  56 => 19,  52 => 17,  50 => 9,  48 => 6,  45 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/AddressBook/connectionButtons.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/AddressBook/connectionButtons.html.twig");
    }
}
