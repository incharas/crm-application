<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/validation-message-handler/time-validation-message-handler-view.js */
class __TwigTemplate_e6165eeaf9c122bae3d7d04dece2ef51 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const AbstractValidationMessageHandlerView =
        require('oroform/js/app/views/validation-message-handler/abstract-validation-message-handler-view');

    const TimeValidationMessageHandlerView = AbstractValidationMessageHandlerView.extend({
        events: {
            showTimepicker: 'onTimepickerDialogToggle',
            hideTimepicker: 'onTimepickerDialogToggle'
        },

        /**
         * @inheritdoc
         */
        constructor: function TimeValidationMessageHandlerView(options) {
            TimeValidationMessageHandlerView.__super__.constructor.call(this, options);
        },

        isActive: function() {
            const {list} = this.\$el[0].timepickerObj || {};

            return list && list.is(':visible') && !list.hasClass('ui-timepicker-positioned-top');
        },

        getPopperReferenceElement: function() {
            return this.\$el;
        },

        onTimepickerDialogToggle: function(e) {
            this.active = this.isActive();
            this.update();
        }
    }, {
        test: function(element) {
            return \$(element).hasClass('timepicker-input');
        }
    });

    return TimeValidationMessageHandlerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/validation-message-handler/time-validation-message-handler-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/validation-message-handler/time-validation-message-handler-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/validation-message-handler/time-validation-message-handler-view.js");
    }
}
