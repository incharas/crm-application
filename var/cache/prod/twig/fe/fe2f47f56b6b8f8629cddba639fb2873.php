<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/settings/mixins/base-ui-element.scss */
class __TwigTemplate_d838a571f8e6d1901c3a36597ceaccec extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@mixin base-ui-element(
    \$use-base-style:   false,
    \$padding:          \$base-ui-element-offset,
    \$font-size:        \$base-ui-element-font-size,
    \$font-family:      \$base-ui-element-font-family,
    \$line-height:      \$base-ui-element-line-height,
    \$border:           \$base-ui-element-border,
    \$border-radius:    \$base-ui-element-border-radius,
    \$background-color: \$base-ui-element-bg-color,
    \$color:            \$base-ui-element-color,
    \$appearance:       true
) {
    @if (\$use-base-style) {
        display: inline-block;
        padding: \$padding;
        max-width: 100%;

        font-size: \$font-size;
        font-family: \$font-family;
        line-height: \$line-height;

        border: \$border;
        border-radius: \$border-radius;
        background-color: \$background-color;
        color: \$color;

        vertical-align: middle;

        text-decoration: none;

        touch-action: manipulation;

        box-sizing: border-box;

        @if (\$appearance) {
            @include appearance();
        }

        &--full,
        &.full {
            width: 100%;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/settings/mixins/base-ui-element.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/settings/mixins/base-ui-element.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/settings/mixins/base-ui-element.scss");
    }
}
