<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/attribute-item.scss */
class __TwigTemplate_5cc99b1ea0ddc4e2b0a173677f4d2a53 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.attribute-item {
    margin: \$attribute-item-offset;

    &__term {
        width: \$attribute-item-term-width;
        max-width: \$attribute-item-term-max-width;
        text-align: \$attribute-item-term-text-align;
    }

    &__description {
        margin-left: \$attribute-item-description-offset-start;
        word-break: \$attribute-item-description-word-break;
    }

    &--mobile-full {
        flex-wrap: wrap;

        .attribute-item {
            &__term {
                width: 100%;
                margin-bottom: \$attribute-item-full-description-offset-bottom;
            }

            &__description {
                width: 100%;
                margin-left: 0;
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/attribute-item.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/attribute-item.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/attribute-item.scss");
    }
}
