<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSync/maintenance_js.html.twig */
class __TwigTemplate_54d8ba972b595111b10c316182d291d3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->checkWsConnected()) {
            // line 2
            echo "<script>
    loadModules(['orosync/js/sync', 'oroui/js/modal', 'orotranslation/js/translator'],
    function(sync, Modal, __) {
        var dialog = null;

        sync.subscribe('oro/maintenance', function (response) {
            var userId = null;
            ";
            // line 9
            if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 9))) {
                // line 10
                echo "                userId = ";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10);
                echo ";
            ";
            }
            // line 12
            echo "
            if (response.isOn && (!userId || parseInt(userId) != parseInt(response.userId))) {
                var regExp = new RegExp('\\n', 'g');
                if (dialog) {
                    dialog.close();
                    dialog.remove();
                }
                dialog = new Modal({
                    content: __('oro.platform.maintenance_mode_on_message').replace(regExp, '<br/>'),
                    className: 'modal oro-modal-danger oro-modal-maintenance',
                    allowCancel: false,
                    title: __('oro.platform.maintenance_mode_on_title'),
                    attributes: {
                        role: 'alertdialog'
                    }
                });
                dialog.open();
            } else if (dialog) {
                dialog.close();
            }
        });
    });
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroSync/maintenance_js.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 12,  50 => 10,  48 => 9,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSync/maintenance_js.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SyncBundle/Resources/views/maintenance_js.html.twig");
    }
}
