<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/message-item.html */
class __TwigTemplate_f562ece060b380e3ee0e33bc317d9f34 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<%
    const classes = ['alert'];
    if (type) classes.push(`alert-\${type}`);
    if (dismissible) classes.push('alert-dismissible');
    if (showIcon) classes.push('alert-icon');
    if (animation) classes.push('fade');
    classes.push('show', 'top-messages');
%>
<div class=\"<%= classes.join(' ') %>\" role=\"alert\">
    <% if (dismissible) { %><button type=\"button\" class=\"fa-close close\" data-dismiss=\"alert\" aria-label=\"<%- _.__('Close') %>\"></button><% } %>
    <div class=\"message\"><%= message %></div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/message-item.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/message-item.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/message-item.html");
    }
}
