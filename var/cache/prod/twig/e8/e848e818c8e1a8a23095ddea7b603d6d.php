<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Default/index.html.twig */
class __TwigTemplate_db3e470c0116554f824695ead90cc6b3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        // line 183
        $_trait_0 = $this->loadTemplate("@OroUI/Default/navbar/blocks.html.twig", "@OroUI/Default/index.html.twig", 183);
        if (!$_trait_0->isTraitable()) {
            throw new RuntimeError('Template "'."@OroUI/Default/navbar/blocks.html.twig".'" cannot be used as a trait.', 183, $this->source);
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            [
                'head' => [$this, 'block_head'],
                'head_style' => [$this, 'block_head_style'],
                'script' => [$this, 'block_script'],
                'scripts_before' => [$this, 'block_scripts_before'],
                'application' => [$this, 'block_application'],
                'head_script' => [$this, 'block_head_script'],
                'bodyClass' => [$this, 'block_bodyClass'],
                'content_side' => [$this, 'block_content_side'],
                'header' => [$this, 'block_header'],
                'left_panel' => [$this, 'block_left_panel'],
                'main' => [$this, 'block_main'],
                'before_content' => [$this, 'block_before_content'],
                'messages' => [$this, 'block_messages'],
                'breadcrumb' => [$this, 'block_breadcrumb'],
                'before_content_addition' => [$this, 'block_before_content_addition'],
                'pin_button' => [$this, 'block_pin_button'],
                'page_container' => [$this, 'block_page_container'],
                'content' => [$this, 'block_content'],
                'right_panel' => [$this, 'block_right_panel'],
                'footer' => [$this, 'block_footer'],
            ]
        );
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/Default/index.html.twig", 1)->unwrap();
        // line 2
        if ( !$this->extensions['Oro\Bundle\NavigationBundle\Twig\HashNavExtension']->checkIsHashNavigation()) {
            // line 3
            echo "    <!DOCTYPE html>
    <html lang=\"";
            // line 4
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getLanguage(), 0, 2), "html", null, true);
            echo "\"";
            echo (($this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->isRtlMode()) ? (" dir=\"rtl\"") : (""));
            echo "
          class=\"";
            // line 5
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                echo "mobile";
            } else {
                echo "desktop";
            }
            echo "-version\">
    <head>
        ";
            // line 7
            $this->displayBlock('head', $context, $blocks);
            // line 39
            echo "    </head>
    <body class=\"";
            // line 40
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                echo "mobile";
            } else {
                echo "desktop";
            }
            echo "-version lang-";
            echo twig_escape_filter($this->env, twig_slice($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getLanguage(), 0, 2), "html", null, true);
            echo " ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "debug", [], "any", false, false, false, 40)) {
                echo "dev-mode ";
            }
            $this->displayBlock('bodyClass', $context, $blocks);
            echo "\">
    ";
            // line 41
            $this->loadTemplate("@OroUI/Default/noscript.html.twig", "@OroUI/Default/index.html.twig", 41)->display($context);
            // line 42
            echo "    <div ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/app-loading-mask-component", "options" => ["showOnStartup" => true]]], 42, $context, $this->getSourceContext());
            // line 45
            echo "></div>
    <div ";
            // line 46
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/app-loading-bar-component", "options" => ["viewOptions" => ["container" => "#oroplatform-header"]]]], 46, $context, $this->getSourceContext());
            // line 49
            echo "></div>
    ";
            // line 50
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("after_body_start", $context)) ? (_twig_default_filter(($context["after_body_start"] ?? null), "after_body_start")) : ("after_body_start")), array());
            // line 51
            echo "    <div id=\"progressbar\" class=\"progress-bar-container\">
        <h3 class=\"progress-title\">";
            // line 52
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.loading"), "html", null, true);
            echo "</h3>
        <div class=\"progress infinite\">
            <progress class=\"progress-bar\" aria-valuetext=\"";
            // line 54
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.page_loading"), "html", null, true);
            echo "\"></progress>
        </div>
    </div>
    <div id=\"page\" style=\"display:none;\" class=\"app-page\">
        ";
            // line 58
            $this->displayBlock('content_side', $context, $blocks);
            // line 63
            echo "        <div class=\"app-page__content\">
            ";
            // line 64
            $this->displayBlock('header', $context, $blocks);
            // line 93
            echo "            <main class=\"app-page__main\">
                ";
            // line 94
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
                // line 95
                echo "                    ";
                $this->displayBlock('left_panel', $context, $blocks);
                // line 100
                echo "                ";
            }
            // line 101
            echo "                ";
            $this->displayBlock('main', $context, $blocks);
            // line 160
            echo "                ";
            if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
                // line 161
                echo "                    ";
                $this->displayBlock('right_panel', $context, $blocks);
                // line 166
                echo "                ";
            }
            // line 167
            echo "            </main>
        ";
            // line 168
            $this->displayBlock('footer', $context, $blocks);
            // line 174
            echo "        </div>
    </div>
    ";
            // line 176
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("before_body_end", $context)) ? (_twig_default_filter(($context["before_body_end"] ?? null), "before_body_end")) : ("before_body_end")), array());
            // line 177
            echo "    </body>
    </html>
";
        } else {
            // line 180
            echo "    ";
            // line 181
            echo "    ";
            $context["title"] = $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render();
            // line 182
            echo "    ";
            // line 183
            echo "    ";
            // line 184
            echo "    ";
            $this->loadTemplate("@OroNavigation/HashNav/hashNavAjax.html.twig", "@OroUI/Default/index.html.twig", 184)->display(twig_array_merge($context, ["data" => ["scripts" =>             $this->renderBlock("head_script", $context, $blocks), "content" =>             $this->renderBlock("page_container", $context, $blocks), "breadcrumb" => twig_trim_filter(            $this->renderBlock("breadcrumb", $context, $blocks)), "beforeContentAddition" =>             $this->renderBlock("before_content_addition", $context, $blocks)]]));
        }
    }

    // line 7
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "            <title>";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Loading...", [], "messages");
        echo "</title>
            <script id=\"page-title\" type=\"text/html\">";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render(), "html", null, true);
        echo "</script>
            <meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1.0";
        // line 10
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            echo ", user-scalable=no, viewport-fit=cover";
        }
        echo "\">
            <meta http-equiv=\"cache-control\" content=\"max-age=0\">
            <meta http-equiv=\"cache-control\" content=\"no-cache\">
            <meta http-equiv=\"expires\" content=\"0\">
            <meta http-equiv=\"pragma\" content=\"no-cache\">
            ";
        // line 15
        $this->displayBlock('head_style', $context, $blocks);
        // line 21
        echo "
            ";
        // line 22
        $this->displayBlock('script', $context, $blocks);
        // line 38
        echo "        ";
    }

    // line 15
    public function block_head_style($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "                ";
        if ($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeIcon()) {
            // line 17
            echo "                    <link rel=\"shortcut icon\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeIcon()), "html", null, true);
            echo "\">
                ";
        }
        // line 19
        echo "               ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("head_style", $context)) ? (_twig_default_filter(($context["head_style"] ?? null), "head_style")) : ("head_style")), array());
        // line 20
        echo "            ";
    }

    // line 22
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "                ";
        $this->displayBlock('scripts_before', $context, $blocks);
        // line 26
        echo "                ";
        $this->displayBlock('application', $context, $blocks);
        // line 29
        echo "                <script>
                ";
        // line 30
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 31
            echo "                    loadModules(['oroui/js/mobile/layout'], function (layout) {layout.init();});
                ";
        }
        // line 33
        echo "                </script>
                ";
        // line 34
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("scripts_after", $context)) ? (_twig_default_filter(($context["scripts_after"] ?? null), "scripts_after")) : ("scripts_after")), array());
        // line 35
        echo "                ";
        $this->displayBlock('head_script', $context, $blocks);
        // line 37
        echo "            ";
    }

    // line 23
    public function block_scripts_before($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "                    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("scripts_before", $context)) ? (_twig_default_filter(($context["scripts_before"] ?? null), "scripts_before")) : ("scripts_before")), array());
        // line 25
        echo "                ";
    }

    // line 26
    public function block_application($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "                    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("application", $context)) ? (_twig_default_filter(($context["application"] ?? null), "application")) : ("application")), array());
        // line 28
        echo "                ";
    }

    // line 35
    public function block_head_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "                ";
    }

    // line 40
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 58
    public function block_content_side($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "            <div class=\"app-page__content-side\">
                ";
        // line 60
        $this->loadTemplate("@OroUI/Default/navbar/sided.html.twig", "@OroUI/Default/index.html.twig", 60)->display($context);
        // line 61
        echo "            </div>
        ";
    }

    // line 64
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 65
        echo "                <header class=\"app-header";
        if (($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_ui.navbar_position") != "top")) {
            echo " thick";
        }
        echo "\" id=\"oroplatform-header\"
                    ";
        // line 66
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 67
            echo "                        ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/sticky-element/sticky-element-view", "options" => ["stickyOptions" => ["enabled" => true]]]], 67, $context, $this->getSourceContext());
            // line 74
            echo "
                    ";
        }
        // line 75
        echo ">
                    ";
        // line 76
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("before_navigation", $context)) ? (_twig_default_filter(($context["before_navigation"] ?? null), "before_navigation")) : ("before_navigation")), array());
        // line 77
        echo "                    <div class=\"app-header__inner\">
                        <div class=\"app-header__container\">
                            ";
        // line 79
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("header_logo", $context)) ? (_twig_default_filter(($context["header_logo"] ?? null), "header_logo")) : ("header_logo")), array());
        // line 80
        echo "                            <ul class=\"user-menu\">
                                ";
        // line 81
        $this->displayBlock("user_menu", $context, $blocks);
        echo "
                            </ul>
                            <div class=\"app-header__search-and-shortcuts\">
                                ";
        // line 84
        $this->displayBlock("navbar", $context, $blocks);
        echo "
                            </div>
                            ";
        // line 86
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("pin_tabs_list", $context)) ? (_twig_default_filter(($context["pin_tabs_list"] ?? null), "pin_tabs_list")) : ("pin_tabs_list")), array());
        // line 87
        echo "                        </div>
                    </div>
                    ";
        // line 89
        $this->loadTemplate("@OroUI/Default/navbar/top.html.twig", "@OroUI/Default/index.html.twig", 89)->display($context);
        // line 90
        echo "                    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("after_navigation", $context)) ? (_twig_default_filter(($context["after_navigation"] ?? null), "after_navigation")) : ("after_navigation")), array());
        // line 91
        echo "                </header>
            ";
    }

    // line 95
    public function block_left_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "                        <div id=\"left-panel\" class=\"app-page__left-panel\">
                            ";
        // line 97
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("left_panel", $context)) ? (_twig_default_filter(($context["left_panel"] ?? null), "left_panel")) : ("left_panel")), ["placement" => "left"]);
        // line 98
        echo "                        </div>
                    ";
    }

    // line 101
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 102
        echo "                    <div id=\"central-panel\" class=\"app-page__central-panel\">
                        ";
        // line 103
        $this->displayBlock('before_content', $context, $blocks);
        // line 148
        echo "                        <div id=\"container\"";
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            echo " class=\"scrollable-container\"";
        }
        echo " data-layout=\"separate\"
                             ";
        // line 149
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/content-view"]], 149, $context, $this->getSourceContext());
        echo ">
                            ";
        // line 150
        $this->displayBlock('page_container', $context, $blocks);
        // line 157
        echo "                        </div>
                    </div>
                ";
    }

    // line 103
    public function block_before_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 104
        echo "                        <div id=\"flash-messages\">
                            ";
        // line 105
        $this->displayBlock('messages', $context, $blocks);
        // line 126
        echo "                        </div>
                        <div class=\"page-toolbar\">
                            ";
        // line 128
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("breadcrumb_pin_before", $context)) ? (_twig_default_filter(($context["breadcrumb_pin_before"] ?? null), "breadcrumb_pin_before")) : ("breadcrumb_pin_before")), array());
        // line 129
        echo "                            <div id=\"breadcrumb\"
                                class=\"page-toolbar_breadcrumb\"
                                 ";
        // line 131
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/breadcrumb-view"]], 131, $context, $this->getSourceContext());
        echo ">
                                ";
        // line 132
        $this->displayBlock('breadcrumb', $context, $blocks);
        // line 135
        echo "                            </div>
                            <div id=\"before-content-addition\" class=\"page-toolbar_before-content-addition\" data-layout=\"separate\"
                                 ";
        // line 137
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/before-content-addition-view"]], 137, $context, $this->getSourceContext());
        echo ">
                                ";
        // line 138
        $this->displayBlock('before_content_addition', $context, $blocks);
        // line 139
        echo "                            </div>
                            ";
        // line 140
        $this->displayBlock('pin_button', $context, $blocks);
        // line 145
        echo "                            ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("breadcrumb_pin_after", $context)) ? (_twig_default_filter(($context["breadcrumb_pin_after"] ?? null), "breadcrumb_pin_after")) : ("breadcrumb_pin_after")), array());
        // line 146
        echo "                        </div>
                        ";
    }

    // line 105
    public function block_messages($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 106
        echo "                                ";
        $context["flashMessages"] = [];
        // line 107
        echo "                                ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 107), "flashbag", [], "any", false, false, false, 107), "peekAll", [], "any", false, false, false, 107)) > 0)) {
            // line 108
            echo "                                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 108), "flashbag", [], "any", false, false, false, 108), "all", [], "any", false, false, false, 108));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 109
                echo "                                        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 110
                    echo "                                            ";
                    $context["flashMessages"] = twig_array_merge(($context["flashMessages"] ?? null), [0 => ["type" => $context["type"], "message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["message"])]]);
                    // line 111
                    echo "                                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 112
                echo "                                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 113
            echo "                                ";
        }
        // line 114
        echo "                                <div class=\"flash-messages-frame\">
                                    <div class=\"flash-messages-holder\"
                                         ";
        // line 116
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/messages-view", "options" => ["autoRender" => true, "initializeMessenger" => true, "messages" =>         // line 121
($context["flashMessages"] ?? null)]]], 116, $context, $this->getSourceContext());
        // line 123
        echo "></div>
                                </div>
                            ";
    }

    // line 132
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 133
        echo "                                    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("breadcrumb", $context)) ? (_twig_default_filter(($context["breadcrumb"] ?? null), "breadcrumb")) : ("breadcrumb")), array());
        // line 134
        echo "                                ";
    }

    // line 138
    public function block_before_content_addition($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 140
    public function block_pin_button($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 141
        echo "                                <div class=\"page-toolbar_bookmark-buttons\">
                                    ";
        // line 142
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("pin_button", $context)) ? (_twig_default_filter(($context["pin_button"] ?? null), "pin_button")) : ("pin_button")), array());
        // line 143
        echo "                                </div>
                            ";
    }

    // line 150
    public function block_page_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 151
        echo "                                ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("content_before", $context)) ? (_twig_default_filter(($context["content_before"] ?? null), "content_before")) : ("content_before")), array());
        // line 152
        echo "                                ";
        $this->displayBlock('content', $context, $blocks);
        // line 155
        echo "                                ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("content_after", $context)) ? (_twig_default_filter(($context["content_after"] ?? null), "content_after")) : ("content_after")), array());
        // line 156
        echo "                            ";
    }

    // line 152
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 153
        echo "                                    ";
        // line 154
        echo "                                ";
    }

    // line 161
    public function block_right_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 162
        echo "                        <div id=\"right-panel\" class=\"app-page__right-panel\">
                            ";
        // line 163
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("right_panel", $context)) ? (_twig_default_filter(($context["right_panel"] ?? null), "right_panel")) : ("right_panel")), ["placement" => "right"]);
        // line 164
        echo "                        </div>
                    ";
    }

    // line 168
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 169
        echo "            <div id=\"dialog-extend-fixed-container\" class=\"ui-dialog-minimize-container\"></div>
            <footer id=\"footer\" class=\"footer\">
                ";
        // line 171
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("footer_inner", $context)) ? (_twig_default_filter(($context["footer_inner"] ?? null), "footer_inner")) : ("footer_inner")), array());
        // line 172
        echo "            </footer>
        ";
    }

    public function getTemplateName()
    {
        return "@OroUI/Default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  621 => 172,  619 => 171,  615 => 169,  611 => 168,  606 => 164,  604 => 163,  601 => 162,  597 => 161,  593 => 154,  591 => 153,  587 => 152,  583 => 156,  580 => 155,  577 => 152,  574 => 151,  570 => 150,  565 => 143,  563 => 142,  560 => 141,  556 => 140,  550 => 138,  546 => 134,  543 => 133,  539 => 132,  533 => 123,  531 => 121,  530 => 116,  526 => 114,  523 => 113,  517 => 112,  511 => 111,  508 => 110,  503 => 109,  498 => 108,  495 => 107,  492 => 106,  488 => 105,  483 => 146,  480 => 145,  478 => 140,  475 => 139,  473 => 138,  469 => 137,  465 => 135,  463 => 132,  459 => 131,  455 => 129,  453 => 128,  449 => 126,  447 => 105,  444 => 104,  440 => 103,  434 => 157,  432 => 150,  428 => 149,  421 => 148,  419 => 103,  416 => 102,  412 => 101,  407 => 98,  405 => 97,  402 => 96,  398 => 95,  393 => 91,  390 => 90,  388 => 89,  384 => 87,  382 => 86,  377 => 84,  371 => 81,  368 => 80,  366 => 79,  362 => 77,  360 => 76,  357 => 75,  353 => 74,  350 => 67,  348 => 66,  341 => 65,  337 => 64,  332 => 61,  330 => 60,  327 => 59,  323 => 58,  317 => 40,  313 => 36,  309 => 35,  305 => 28,  302 => 27,  298 => 26,  294 => 25,  291 => 24,  287 => 23,  283 => 37,  280 => 35,  278 => 34,  275 => 33,  271 => 31,  269 => 30,  266 => 29,  263 => 26,  260 => 23,  256 => 22,  252 => 20,  249 => 19,  243 => 17,  240 => 16,  236 => 15,  232 => 38,  230 => 22,  227 => 21,  225 => 15,  215 => 10,  211 => 9,  206 => 8,  202 => 7,  196 => 184,  194 => 183,  192 => 182,  189 => 181,  187 => 180,  182 => 177,  180 => 176,  176 => 174,  174 => 168,  171 => 167,  168 => 166,  165 => 161,  162 => 160,  159 => 101,  156 => 100,  153 => 95,  151 => 94,  148 => 93,  146 => 64,  143 => 63,  141 => 58,  134 => 54,  129 => 52,  126 => 51,  124 => 50,  121 => 49,  119 => 46,  116 => 45,  113 => 42,  111 => 41,  96 => 40,  93 => 39,  91 => 7,  82 => 5,  76 => 4,  73 => 3,  71 => 2,  69 => 1,  30 => 183,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Default/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Default/index.html.twig");
    }
}
