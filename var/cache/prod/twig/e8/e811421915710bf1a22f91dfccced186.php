<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Call/activityLink.html.twig */
class __TwigTemplate_0cc6d58ae83fd95fe2f13cb7c6313fa6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'action_controll' => [$this, 'block_action_controll'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["options"] = ["dataUrl" => (((        // line 2
array_key_exists("entity", $context) &&  !(null === ($context["entity"] ?? null)))) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_call_create", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 5
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "id", [], "any", false, false, false, 6), "phone" => ((        // line 7
array_key_exists("phone", $context)) ? (twig_escape_filter($this->env, ($context["phone"] ?? null), "html")) : (null))])) : (null)), "class" => ((        // line 10
array_key_exists("cssClass", $context)) ? (($context["cssClass"] ?? null)) : ("")), "role" => ((        // line 11
array_key_exists("role", $context)) ? (($context["role"] ?? null)) : ("")), "aCss" => "dropdown-item no-hash", "iCss" => "fa-phone-square", "dataId" => (( !twig_test_empty(        // line 14
($context["entity"] ?? null))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 14)) : (null)), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.log_call"), "widget" => ["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "options" => ["alias" => "call-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.log_call"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]];
        // line 34
        echo "
";
        // line 35
        $this->displayBlock('action_controll', $context, $blocks);
    }

    public function block_action_controll($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCall/Call/activityLink.html.twig", 36)->unwrap();
        // line 37
        echo "
    ";
        // line 38
        echo twig_call_macro($macros["UI"], "macro_clientLink", [($context["options"] ?? null)], 38, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCall/Call/activityLink.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 38,  60 => 37,  57 => 36,  50 => 35,  47 => 34,  45 => 14,  44 => 11,  43 => 10,  42 => 7,  41 => 6,  40 => 5,  39 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Call/activityLink.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Call/activityLink.html.twig");
    }
}
