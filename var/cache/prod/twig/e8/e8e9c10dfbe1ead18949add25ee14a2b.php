<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/AddressBook/widget/addressBookConnectionUpdate.html.twig */
class __TwigTemplate_6fd7bab1d800e8d76501dcd4c0abf296 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("savedId", $context) && ($context["savedId"] ?? null))) {
            // line 2
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["trigger" => [0 => ["eventBroker" => "widget", "name" => "formSave", "args" => [0 =>             // line 7
($context["savedId"] ?? null)]]]]];
            // line 11
            echo "
    ";
            // line 12
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
";
        } else {
            // line 14
            echo "    <div class=\"widget-content\">
        ";
            // line 15
            if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 15), "errors", [], "any", false, false, false, 15)) > 0)) {
                // line 16
                echo "            <div class=\"alert alert-error\" role=\"alert\">
                ";
                // line 17
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
                echo "
            </div>
        ";
            }
            // line 20
            echo "        <form method=\"post\"
              data-nohash=\"true\"
              data-disable-autofocus=\"true\"
              id=\"";
            // line 23
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 23), "id", [], "any", false, false, false, 23), "html", null, true);
            echo "\"
              action=\"";
            // line 24
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 24), "uri", [], "any", false, false, false, 24), "html", null, true);
            echo "\"
              class=\"form-dialog\"
        >
            <fieldset class=\"form-horizontal connection-form\">
                ";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channel", [], "any", false, false, false, 28), 'row');
            echo "
                ";
            // line 29
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "addressBook", [], "any", false, false, false, 29), 'row');
            echo "
                ";
            // line 30
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "createEntities", [], "any", false, false, false, 30), 'row');
            echo "
            </fieldset>
            ";
            // line 32
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", false, false, false, 32), 'row');
            echo "

            <div class=\"widget-actions\">
                <button type=\"reset\" class=\"btn\">";
            // line 35
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.dialog.button.cancel"), "html", null, true);
            echo "</button>
                <button type=\"submit\" class=\"btn btn-success\">
                    ";
            // line 37
            if ((($context["entity"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 37))) {
                // line 38
                echo "                        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.dialog.button.update"), "html", null, true);
                echo "
                    ";
            } else {
                // line 40
                echo "                        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.connection.dialog.button.connect"), "html", null, true);
                echo "
                    ";
            }
            // line 42
            echo "                </button>
            </div>
        </form>
        ";
            // line 45
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/AddressBook/widget/addressBookConnectionUpdate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 45,  119 => 42,  113 => 40,  107 => 38,  105 => 37,  100 => 35,  94 => 32,  89 => 30,  85 => 29,  81 => 28,  74 => 24,  70 => 23,  65 => 20,  59 => 17,  56 => 16,  54 => 15,  51 => 14,  46 => 12,  43 => 11,  41 => 7,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/AddressBook/widget/addressBookConnectionUpdate.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/AddressBook/widget/addressBookConnectionUpdate.html.twig");
    }
}
