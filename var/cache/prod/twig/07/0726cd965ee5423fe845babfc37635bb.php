<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/ProcessDefinition/view.html.twig */
class __TwigTemplate_6ed7e16fa7546a7d1ac175dbd8aaafba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'stats' => [$this, 'block_stats'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%process_definition.label%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "label", [], "any", false, false, false, 3)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroWorkflow/ProcessDefinition/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/ProcessDefinition/view.html.twig", 6)->unwrap();
        // line 7
        echo "
    ";
        // line 8
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_process_definition_update")) {
            // line 9
            echo "        ";
            $context["idButton"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 9) . "-process-deactivate-btn");
            // line 10
            echo "        ";
            $context["options"] = ["data" => ["role" => "status-toggle"]];
            // line 15
            echo "        <div ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroworkflow/js/app/views/process-status-toggle-btn-view"]], 15, $context, $this->getSourceContext());
            // line 17
            echo ">
            ";
            // line 18
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "enabled", [], "any", false, false, false, 18)) {
                // line 19
                echo "                ";
                echo twig_call_macro($macros["UI"], "macro_button", [twig_array_merge(($context["options"] ?? null), ["aCss" => "no-hash btn-danger", "iCss" => "fa-close", "id" =>                 // line 22
($context["idButton"] ?? null), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.action.process.deactivate"), "path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_process_deactivate", ["processDefinition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 24
($context["entity"] ?? null), "name", [], "any", false, false, false, 24)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.action.process.deactivate")])], 19, $context, $this->getSourceContext());
                // line 26
                echo "
            ";
            } else {
                // line 28
                echo "                ";
                echo twig_call_macro($macros["UI"], "macro_button", [twig_array_merge(($context["options"] ?? null), ["iCss" => "fa-check", "aCss" => "no-hash btn-success", "id" =>                 // line 31
($context["idButton"] ?? null), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.action.process.activate"), "path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_process_activate", ["processDefinition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 33
($context["entity"] ?? null), "name", [], "any", false, false, false, 33)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.action.process.activate")])], 28, $context, $this->getSourceContext());
                // line 35
                echo "
            ";
            }
            // line 37
            echo "        </div>
    ";
        }
    }

    // line 41
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/ProcessDefinition/view.html.twig", 42)->unwrap();
        // line 43
        echo "
    ";
        // line 44
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <span class=\"page-title__status\">
        ";
        // line 46
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "enabled", [], "any", false, false, false, 46)) {
            // line 47
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Active"), "enabled"], 47, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 49
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Inactive"), "disabled"], 49, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 51
        echo "    </span>
";
    }

    // line 54
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 55
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 55)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 55)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 56)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 56)), "html", null, true))) : (print ("N/A")));
        echo "</li>
";
    }

    // line 59
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 61
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_process_definition_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.processdefinition.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 64
($context["entity"] ?? null), "label", [], "any", false, false, false, 64)];
        // line 66
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 69
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 70
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/ProcessDefinition/view.html.twig", 70)->unwrap();
        // line 72
        ob_start(function () { return ''; });
        // line 73
        $context["entityConfig"] = $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfig(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "relatedEntity", [], "any", false, false, false, 73));
        // line 74
        echo "        ";
        $context["relatedEntityLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entityConfig"] ?? null), "label", [], "any", true, true, false, 74)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entityConfig"] ?? null), "label", [], "any", false, false, false, 74), "")) : ("")));
        // line 76
        ob_start(function () { return ''; });
        // line 77
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["triggers"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["trigger"]) {
            // line 78
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["trigger"], "event", [], "any", false, false, false, 78)) {
                // line 79
                echo "                    ";
                $context["event"] = ("oro.workflow.block.view.process.trigger.event." . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["trigger"], "event", [], "any", false, false, false, 79));
                // line 80
                echo "                    ";
                $context["when"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["trigger"], "queued", [], "any", false, false, false, 80)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDuration(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 81
$context["trigger"], "timeShift", [], "any", false, false, false, 81))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.view.process.trigger.when.immediately")));
                // line 84
                echo "
                    ";
                // line 85
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["trigger"], "field", [], "any", false, false, false, 85)) {
                    // line 86
                    echo "                        ";
                    $context["field"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entityConfig"] ?? null), "trigger", [], "any", false, true, false, 86), "field", [], "any", false, true, false, 86), "lable", [], "any", true, true, false, 86)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entityConfig"] ?? null), "trigger", [], "any", false, true, false, 86), "field", [], "any", false, true, false, 86), "lable", [], "any", false, false, false, 86), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["trigger"], "field", [], "any", false, false, false, 86))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["trigger"], "field", [], "any", false, false, false, 86)));
                    // line 87
                    echo "                        ";
                    $context["after"] = ((("<b>" . ($context["field"] ?? null)) . "</b> ") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.view.process.trigger.after.property"));
                    // line 88
                    echo "                    ";
                } else {
                    // line 89
                    echo "                        ";
                    $context["after"] = ((("<b>" . ($context["relatedEntityLabel"] ?? null)) . "</b> ") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.view.process.trigger.after.entity"));
                    // line 90
                    echo "                    ";
                }
                // line 91
                echo "
                    ";
                // line 92
                echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.view.process.trigger.description", ["%after%" =>                 // line 93
($context["after"] ?? null), "%event%" => (("<b>" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(                // line 94
($context["event"] ?? null))) . "</b>"), "%when%" => (("<b>" .                 // line 95
($context["when"] ?? null)) . "</b>")]);
                // line 97
                echo "
                    <br />
                ";
            } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 99
$context["trigger"], "cron", [], "any", false, false, false, 99)) {
                // line 100
                echo "                    ";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.view.process.trigger.cron.description", ["{{ cron }}" => (("<b>" . $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 101
$context["trigger"], "cron", [], "any", false, false, false, 101))) . "</b>")]);
                // line 102
                echo "
                    <br />
                ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['trigger'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        $context["triggerData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 108
        echo "<div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 110
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.processdefinition.label.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "label", [], "any", false, false, false, 110)], 110, $context, $this->getSourceContext());
        echo "
                ";
        // line 111
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.processdefinition.related_entity.label"), ($context["relatedEntityLabel"] ?? null)], 111, $context, $this->getSourceContext());
        echo "
                ";
        // line 112
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.processdefinition.execution_order.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "executionOrder", [], "any", false, false, false, 112)], 112, $context, $this->getSourceContext());
        echo "
                ";
        // line 113
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.processtrigger.entity_plural_label"), ($context["triggerData"] ?? null)], 113, $context, $this->getSourceContext());
        echo "
            </div>
        </div>";
        $context["processDefinitionInfo"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 118
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.title.process_info"), "subblocks" => [0 => ["data" => [0 =>         // line 122
($context["processDefinitionInfo"] ?? null)]]]]];
        // line 126
        echo "
    ";
        // line 127
        $context["id"] = "processDefinitionView";
        // line 128
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 129
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/ProcessDefinition/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  283 => 129,  280 => 128,  278 => 127,  275 => 126,  273 => 122,  272 => 118,  266 => 113,  262 => 112,  258 => 111,  254 => 110,  250 => 108,  240 => 102,  238 => 101,  236 => 100,  234 => 99,  230 => 97,  228 => 95,  227 => 94,  226 => 93,  225 => 92,  222 => 91,  219 => 90,  216 => 89,  213 => 88,  210 => 87,  207 => 86,  205 => 85,  202 => 84,  200 => 81,  198 => 80,  195 => 79,  193 => 78,  189 => 77,  187 => 76,  184 => 74,  182 => 73,  180 => 72,  177 => 70,  173 => 69,  166 => 66,  164 => 64,  163 => 61,  161 => 60,  157 => 59,  149 => 56,  142 => 55,  138 => 54,  133 => 51,  127 => 49,  121 => 47,  119 => 46,  114 => 44,  111 => 43,  108 => 42,  104 => 41,  98 => 37,  94 => 35,  92 => 33,  91 => 31,  89 => 28,  85 => 26,  83 => 24,  82 => 22,  80 => 19,  78 => 18,  75 => 17,  72 => 15,  69 => 10,  66 => 9,  64 => 8,  61 => 7,  58 => 6,  54 => 5,  49 => 1,  47 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/ProcessDefinition/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/ProcessDefinition/view.html.twig");
    }
}
