<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/utilites.scss */
class __TwigTemplate_ffef543e8c69f31d354088cdf6661ada extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

%hide {
    // override inline styles
    // stylelint-disable-next-line declaration-no-important
    display: none !important;
}

.hide {
    @extend %hide;
}

.nowrap {
    white-space: nowrap;
}

.direction-ltr {
    /* rtl:ignore */
    direction: ltr;
    unicode-bidi: bidi-override;
}

// extend bootstrap .list-unstyled class
// link: https://github.com/twbs/bootstrap/blob/v4.1.0/scss/_type.scss#L84
.list-unstyled {
    margin-bottom: 0;
}

.no-transition {
    // stylelint-disable-next-line declaration-no-important
    transition: none !important;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/utilites.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/utilites.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/utilites.scss");
    }
}
