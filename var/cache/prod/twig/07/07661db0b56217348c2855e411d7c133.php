<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/formatter/field.js */
class __TwigTemplate_d51d4a59b2659cf5a8b4c20d5a67a3e0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'underscore', 'orotranslation/js/translator', 'orolocale/js/formatter/number', 'orolocale/js/formatter/datetime'
], function(_, __, numberFormatter, dateTimeFormatter) {
    'use strict';

    function nl2br(val) {
        return val.replace(/\\n/g, '<br />\\n');
    }

    /**
     * @export  oroform/js/formatter/field
     * @name    oroform.formatter.field
     */
    return {
        /**
         * @param {*} val
         * @returns {string}
         */
        bool: function(val) {
            return val ? __('Yes') : __('No');
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        string: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : _.escape(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        text: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : nl2br(_.escape(val));
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        html: function(val) {
            if (_.isNull(val) || _.isUndefined(val)) {
                return __('N/A');
            } else {
                const htmlWrapper = document.createElement('div');
                htmlWrapper.innerHTML = val;

                if (htmlWrapper.childElementCount > 0) {
                    return val;
                } else {
                    return nl2br(val);
                }
            }
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        integer: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : numberFormatter.formatInteger(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        decimal: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : numberFormatter.formatDecimal(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        percent: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : numberFormatter.formatPercent(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        currency: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : numberFormatter.formatCurrency(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        date: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : dateTimeFormatter.formatDate(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        time: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : dateTimeFormatter.formatTime(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        dateTime: function(val) {
            return _.isNull(val) || _.isUndefined(val) ? __('N/A') : dateTimeFormatter.formatDateTime(val);
        },

        /**
         * @param {*} val
         * @returns {string}
         */
        color: function(val) {
            return !val ? __('N/A') : '<i class=\"color hide-text\" title=\"' +
                val + '\" style=\"background-color: ' + val + ';\">' + val + '</i>';
        }
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/formatter/field.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/formatter/field.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/formatter/field.js");
    }
}
