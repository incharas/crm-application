<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/base/component-container-mixin.js */
class __TwigTemplate_77b934d104118fe12182ea5169499172 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const ComponentManager = require('oroui/js/app/components/component-manager');
    const \$ = require('jquery');

    const componentContainerMixin = {
        /**
         * @returns {jQuery}
         */
        getLayoutElement: function() {
            throw Error('\"getLayoutElement\" method have to be defined in the component container');
        },

        /**
         * Getter for component manager
         *
         * @returns {ComponentManager}
         */
        _getComponentManager: function() {
            if (!this.componentManager) {
                this.componentManager = new ComponentManager(this.getLayoutElement());
            }
            return this.componentManager;
        },

        /**
         * Getter/setter for components
         *
         * @param {string} name
         * @param {BaseComponent=} component to set
         * @param {HTMLElement=} el
         */
        pageComponent: function(name, component, el) {
            if (this.disposed) {
                component.dispose();
                return;
            }

            if (name && component) {
                if (!el) {
                    throw Error('The element related to the component is required');
                }
                return this._getComponentManager().add(name, component, el);
            } else {
                return this._getComponentManager().get(name);
            }
        },

        /**
         * @param {string} name component name to remove
         */
        removePageComponent: function(name) {
            this._getComponentManager().remove(name);
        },

        /**
         * Applies callback function to all component
         *
         * @param {Function} callback
         * @param {Object?} context
         */
        forEachComponent: function(callback, context) {
            this._getComponentManager().forEachComponent(callback, context || this);
        },

        /**
         * @returns {boolean}
         */
        hasOwnLayout: function() {
            return this.getLayoutElement().is('[data-layout=\"separate\"]');
        },

        /**
         * Initializes all linked page components
         * @param {Object|null} options
         */
        initPageComponents: function(options) {
            if (!this.hasOwnLayout()) {
                const error = new Error('PageComponents can not be initialized for the element without own layout');
                return \$.Deferred().reject(error);
            }
            return this._getComponentManager().init(options);
        },

        /**
         * Destroys all linked page components
         */
        disposePageComponents: function() {
            if (this.disposed) {
                return;
            }
            if (this.componentManager) {
                this._getComponentManager().dispose();
                delete this.componentManager;
            }
        }
    };

    return componentContainerMixin;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/base/component-container-mixin.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/base/component-container-mixin.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/base/component-container-mixin.js");
    }
}
