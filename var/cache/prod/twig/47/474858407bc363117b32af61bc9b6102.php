<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Default/logo.html.twig */
class __TwigTemplate_bddc1b17d6eea02e38595f09785cd56b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/Default/logo.html.twig", 1)->unwrap();
        // line 2
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 3
            echo "    <span id=\"main-menu-toggle\" class=\"main-menu-toggler\">
        <i class=\"fa-bars\"></i>
    </span>
";
        }
        // line 7
        echo "<div id=\"organization-switcher\" class=\"organization-logo-wrapper\"
    ";
        // line 8
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/organization-switch-view"]], 8, $context, $this->getSourceContext());
        echo "
    >";
        // line 9
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("organization_selector", $context)) ? (_twig_default_filter(($context["organization_selector"] ?? null), "organization_selector")) : ("organization_selector")), array());
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/Default/logo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 9,  50 => 8,  47 => 7,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Default/logo.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Default/logo.html.twig");
    }
}
