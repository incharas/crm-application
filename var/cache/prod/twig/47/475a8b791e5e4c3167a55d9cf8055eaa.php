<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/select2-l10n.js */
class __TwigTemplate_cb0c89c8f2f196db9ad305fb713c7281 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const __ = require('orotranslation/js/translator');
    const \$ = require('jquery');
    require('jquery.select2');

    \$.fn.select2.defaults = \$.extend(\$.fn.select2.defaults, {
        formatNoMatches: function() {
            return __('No matches found');
        },
        formatInputTooShort: function(input, min) {
            const number = min - input.length;
            return __('oro.ui.select2.input_too_short', {number: number}, number);
        },
        formatInputTooLong: function(input, max) {
            const number = input.length - max;
            return __('oro.ui.select2.input_too_long', {number: number}, number);
        },
        formatSelectionTooBig: function(limit) {
            return __('oro.ui.select2.selection_too_big', {limit: limit}, limit);
        },
        formatLoadMore: function() {
            return __('oro.ui.select2.load_more');
        },
        formatSearching: function() {
            return __('Searching...');
        }
    });
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/select2-l10n.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/select2-l10n.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/select2-l10n.js");
    }
}
