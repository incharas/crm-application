<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/history-navigation-component.js */
class __TwigTemplate_9d69d568d6964e71e8ef2c212146024c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/** @exports HistoryNavigationComponent */
define(function(require) {
    'use strict';

    const BaseComponent = require('../components/base/component');
    const StatefulModel = require('../models/base/stateful-model');
    const HistoryNavigationView = require('../views/history-navigation-view');

    /**
     * Builds history controls for undo/redo capability.
     *
     * @class HistoryNavigationComponent
     * @augments BaseComponent
     */
    const HistoryNavigationComponent = BaseComponent.extend(/** @lends HistoryNavigationComponent.prototype */{
        history: null,

        observedModel: null,

        /**
         * @inheritdoc
         */
        constructor: function HistoryNavigationComponent(options) {
            HistoryNavigationComponent.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            if (options.observedModel instanceof StatefulModel === false) {
                throw new Error('Observed object should be instance of StatefulModel');
            }
            this.observedModel = options.observedModel;
            this.historyView = new HistoryNavigationView({
                model: this.observedModel.history,
                el: options._sourceElement
            });
            this.historyView.on('navigate', this.onHistoryNavigate, this);
        },

        onHistoryNavigate: function(index) {
            const history = this.observedModel.history;
            if (history.setIndex(index)) {
                const state = history.getCurrentState();
                this.observedModel.setState(state.get('data'));
            }
        }

    });

    return HistoryNavigationComponent;
});

";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/history-navigation-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/history-navigation-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/history-navigation-component.js");
    }
}
