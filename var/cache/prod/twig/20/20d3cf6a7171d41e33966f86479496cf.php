<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/settings/mixins/loading.scss */
class __TwigTemplate_3c61fcfc80bbc8cdae7bdda1cd8007f9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@keyframes spin-loading {
    100% {
        transform: rotate(360deg);
    }
}

@mixin loader(\$size: \$loader-size, \$border-width: \$loader-width, \$color: \$loader-color) {
    height: \$size;
    width: \$size;

    border: \$border-width solid \$color;

    border-bottom-color: transparent;
    border-left-color: transparent;

    border-radius: 50%;
    animation: spin-loading .85s steps(40, end) infinite;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/settings/mixins/loading.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/settings/mixins/loading.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/settings/mixins/loading.scss");
    }
}
