<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/horizontal_tabs.html.twig */
class __TwigTemplate_b45538f1c953d968cfe3df9c3b67c7e3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'compressed_root' => [$this, 'block_compressed_root'],
            'root' => [$this, 'block_root'],
            'navbar_tabs' => [$this, 'block_navbar_tabs'],
            'navbar_tabs_content' => [$this, 'block_navbar_tabs_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu_base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu_base.html.twig", "@OroNavigation/Menu/horizontal_tabs.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_compressed_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        ob_start(function () { return ''; });
        // line 4
        echo "        ";
        $this->displayBlock("root", $context, $blocks);
        echo "
    ";
        $___internal_parse_91_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 3
        echo twig_spaceless($___internal_parse_91_);
    }

    // line 8
    public function block_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 9) &&  !(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 9) === 0)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 9))) {
            // line 10
            echo "        ";
            $this->displayBlock("navbar_tabs", $context, $blocks);
            echo "
        ";
            // line 11
            $this->displayBlock("navbar_tabs_content", $context, $blocks);
            echo "
    ";
        }
    }

    // line 15
    public function block_navbar_tabs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    ";
        $context["options"] = twig_array_merge(($context["options"] ?? null), ["template" => "@OroNavigation/Menu/_htabs.html.twig", "depth" => 1]);
        // line 17
        echo "    ";
        echo $this->extensions['Knp\Menu\Twig\MenuExtension']->render(($context["item"] ?? null), ($context["options"] ?? null));
        echo "
";
    }

    // line 20
    public function block_navbar_tabs_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        $context["options"] = twig_array_merge(($context["options"] ?? null), ["template" => "@OroNavigation/Menu/_tabs-content.html.twig", "depth" => 1]);
        // line 22
        echo "    ";
        echo $this->extensions['Knp\Menu\Twig\MenuExtension']->render(($context["item"] ?? null), ($context["options"] ?? null));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/horizontal_tabs.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 22,  103 => 21,  99 => 20,  92 => 17,  89 => 16,  85 => 15,  78 => 11,  73 => 10,  70 => 9,  66 => 8,  62 => 3,  56 => 4,  53 => 3,  49 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/horizontal_tabs.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/horizontal_tabs.html.twig");
    }
}
