<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Shortcut/actionslist.html.twig */
class __TwigTemplate_47ce3b3ea4ef6c7896e961dbfcf4abc5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroNavigation/Shortcut/actionslist.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/Shortcut/actionslist.html.twig", 2)->unwrap();
        // line 3
        $macros["Navigation"] = $this->macros["Navigation"] = $this->loadTemplate("@OroNavigation/macros.html.twig", "@OroNavigation/Shortcut/actionslist.html.twig", 3)->unwrap();
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Shortcut Actions List", [], "messages");
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "<div class=\"container-fluid\">
    <div class=\"clearfix\">
        <h3>";
        // line 9
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Shortcut Actions List", [], "messages");
        echo "</h3>

        ";
        // line 11
        if ((twig_length_filter($this->env, ($context["actionsList"] ?? null)) > 0)) {
            // line 12
            echo "            <table class=\"table table-bordered table-striped\">
                <thead>
                    <tr>
                        <th>";
            // line 15
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Action Name", [], "messages");
            echo "</th>
                        <th>";
            // line 16
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Description", [], "messages");
            echo "</th>
                    </tr>
                </thead>
                <tbody>
                ";
            // line 20
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["actionsList"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
                // line 21
                echo "                    ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "url", [], "any", false, false, false, 21)) {
                    // line 22
                    echo "                        <tr>
                            ";
                    // line 23
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "dialog", [], "any", true, true, false, 23) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "dialog", [], "any", false, false, false, 23))) {
                        // line 24
                        echo "                                <td>
                                    ";
                        // line 25
                        echo twig_call_macro($macros["Navigation"], "macro_renderClientLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "dialog_config", [], "any", false, false, false, 25), ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 26
($context["app"] ?? null), "user", [], "any", false, false, false, 26), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 27
($context["app"] ?? null), "user", [], "any", false, false, false, 27), "id", [], "any", false, false, false, 27)]], 25, $context, $this->getSourceContext());
                        // line 28
                        echo "
                                </td>
                                <td>";
                        // line 30
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "description", [], "any", false, false, false, 30)), "html", null, true);
                        echo "</td>
                            ";
                    } else {
                        // line 32
                        echo "                                <td><a href=\"";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "url", [], "any", false, false, false, 32), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "label", [], "any", false, false, false, 32)), "html", null, true);
                        echo "</a></td>
                                <td>";
                        // line 33
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "description", [], "any", false, false, false, 33)), "html", null, true);
                        echo "</td>
                            ";
                    }
                    // line 35
                    echo "
                        </tr>
                    ";
                }
                // line 38
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 39
            echo "                </tbody>
            </table>
        ";
        } else {
            // line 42
            echo "            <h2>";
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Sorry, you don't have any action", [], "messages");
            echo "</h2>
        ";
        }
        // line 44
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Shortcut/actionslist.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 44,  145 => 42,  140 => 39,  134 => 38,  129 => 35,  124 => 33,  117 => 32,  112 => 30,  108 => 28,  106 => 27,  105 => 26,  104 => 25,  101 => 24,  99 => 23,  96 => 22,  93 => 21,  89 => 20,  82 => 16,  78 => 15,  73 => 12,  71 => 11,  66 => 9,  62 => 7,  58 => 6,  51 => 5,  47 => 1,  45 => 3,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Shortcut/actionslist.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Shortcut/actionslist.html.twig");
    }
}
