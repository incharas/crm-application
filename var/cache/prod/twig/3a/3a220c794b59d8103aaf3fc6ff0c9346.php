<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Dashboard/opportunitiesList.html.twig */
class __TwigTemplate_c4d35d6e8114d5c68e6a7ca035afd6ee extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/grid.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["gridName"] = "dashboard-my-sales-opportunity-grid";
        // line 1
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/grid.html.twig", "@OroSales/Dashboard/opportunitiesList.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    ";
        $macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroSales/Dashboard/opportunitiesList.html.twig", 5)->unwrap();
        // line 6
        echo "    ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), twig_array_merge(["routerEnabled" => false, "enableFilters" => false], ((        // line 9
array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])))], 6, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Dashboard/opportunitiesList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 9,  56 => 6,  53 => 5,  49 => 4,  44 => 1,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Dashboard/opportunitiesList.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Dashboard/opportunitiesList.html.twig");
    }
}
