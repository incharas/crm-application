<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/base/collection.js */
class __TwigTemplate_8a93170dc477e928d54bf6a140f37df3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const Chaplin = require('chaplin');
    const BaseModel = require('./model');

    /**
     * @class BaseCollection
     * @extends Chaplin.Collection
     */
    const BaseCollection = Chaplin.Collection.extend(/** @lends BaseCollection.prototype */{
        cidPrefix: 'cl',

        model: BaseModel,

        constructor: function BaseCollection(data, options) {
            this.cid = _.uniqueId(this.cidPrefix);
            BaseCollection.__super__.constructor.call(this, data, options);
        },

        /**
         * Returns additional parameters to be merged into serialized object
         */
        serializeExtraData: function() {
            return {};
        },

        modelId(attrs) {
            return attrs[this.model.prototype.idAttribute || 'id'];
        }
    });

    return BaseCollection;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/base/collection.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/base/collection.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/base/collection.js");
    }
}
