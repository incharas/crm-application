<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOrganization/BusinessUnit/update.html.twig */
class __TwigTemplate_929ccb47a65c15e69de8b9d766f61f8a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head_script' => [$this, 'block_head_script'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroOrganization/BusinessUnit/update.html.twig", 2)->unwrap();
        // line 4
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig"], true);
        // line 6
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 6), "value", [], "any", false, false, false, 6), "id", [], "any", false, false, false, 6);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%business_unit.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "name", [], "any", false, false, false, 7)]]);
        // line 9
        $context["gridName"] = "bu-update-users-grid";
        // line 10
        $context["formAction"] = ((($context["entityId"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_business_unit_update", ["id" => ($context["entityId"] ?? null)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_business_unit_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroOrganization/BusinessUnit/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 12
    public function block_head_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $this->displayParentBlock("head_script", $context, $blocks);
        echo "
    ";
        // line 14
        $context["listenerParameters"] = ["columnName" => "has_business_unit", "selectors" => ["included" => "#businessUnitAppendUsers", "excluded" => "#businessUnitRemoveUsers"]];
    }

    // line 23
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOrganization/BusinessUnit/update.html.twig", 24)->unwrap();
        // line 25
        echo "
    ";
        // line 26
        if (((($context["entityId"] ?? null) && ($this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getBusinessUnitCount() > 1)) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "value", [], "any", false, false, false, 26)))) {
            // line 27
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_businessunit", ["id" =>             // line 28
($context["entityId"] ?? null)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_business_unit_index"), "aCss" => "no-hash remove-button", "dataId" =>             // line 31
($context["entityId"] ?? null), "id" => "btn-remove-business_unit", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.organization.businessunit.entity_label"), "disabled" =>  !            // line 34
($context["allow_delete"] ?? null)]], 27, $context, $this->getSourceContext());
            // line 35
            echo "
        ";
            // line 36
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 36, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 38
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_business_unit_index")], 38, $context, $this->getSourceContext());
        echo "
    ";
        // line 39
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_business_unit_view", "params" => ["id" => "\$id"]]], 39, $context, $this->getSourceContext());
        // line 43
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_business_unit_create")) {
            // line 44
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_business_unit_create"]], 44, $context, $this->getSourceContext()));
            // line 47
            echo "    ";
        }
        // line 48
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 48), "value", [], "any", false, false, false, 48), "id", [], "any", false, false, false, 48) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_business_unit_update"))) {
            // line 49
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_business_unit_update", "params" => ["id" => "\$id"]]], 49, $context, $this->getSourceContext()));
            // line 53
            echo "    ";
        }
        // line 54
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 54, $context, $this->getSourceContext());
        echo "
";
    }

    // line 57
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 59
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 60
($context["form"] ?? null), "vars", [], "any", false, false, false, 60), "value", [], "any", false, false, false, 60), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_business_unit_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.organization.businessunit.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 63
($context["form"] ?? null), "vars", [], "any", false, false, false, 63), "value", [], "any", false, false, false, 63), "name", [], "any", false, false, false, 63)];
            // line 65
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 67
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.organization.businessunit.entity_label")]);
            // line 68
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroOrganization/BusinessUnit/update.html.twig", 68)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 69
            echo "    ";
        }
    }

    // line 72
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "    ";
        $context["id"] = "business_unit-profile";
        // line 74
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 80
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "appendUsers", [], "any", false, false, false, 80), 'widget', ["id" => "businessUnitAppendUsers"]), 1 =>         // line 81
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "removeUsers", [], "any", false, false, false, 81), 'widget', ["id" => "businessUnitRemoveUsers"]), 2 =>         // line 82
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 82), 'row'), 3 =>         // line 84
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parentBusinessUnit", [], "any", false, false, false, 84), 'row'), 4 =>         // line 86
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phone", [], "any", false, false, false, 86), 'row'), 5 =>         // line 87
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "website", [], "any", false, false, false, 87), 'row'), 6 =>         // line 88
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 88), 'row'), 7 =>         // line 89
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "fax", [], "any", false, false, false, 89), 'row')]]]]];
        // line 93
        echo "
    ";
        // line 94
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 95
        echo "
    ";
        // line 96
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.organization.businessunit.users.label"), "subblocks" => [0 => ["title" => null, "useSpan" => false, "data" => [0 => twig_call_macro($macros["dataGrid"], "macro_renderGrid", [        // line 101
($context["gridName"] ?? null), ["business_unit_id" => ($context["entityId"] ?? null)], ["cssClass" => "inner-grid"]], 101, $context, $this->getSourceContext())]]]]]);
        // line 104
        echo "
    ";
        // line 105
        $context["data"] = ["formErrors" => ((        // line 106
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 107
($context["dataBlocks"] ?? null)];
        // line 109
        echo "
    ";
        // line 110
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroOrganization/BusinessUnit/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 110,  199 => 109,  197 => 107,  196 => 106,  195 => 105,  192 => 104,  190 => 101,  189 => 96,  186 => 95,  184 => 94,  181 => 93,  179 => 89,  178 => 88,  177 => 87,  176 => 86,  175 => 84,  174 => 82,  173 => 81,  172 => 80,  170 => 74,  167 => 73,  163 => 72,  158 => 69,  155 => 68,  152 => 67,  146 => 65,  144 => 63,  143 => 60,  141 => 59,  138 => 58,  134 => 57,  127 => 54,  124 => 53,  121 => 49,  118 => 48,  115 => 47,  112 => 44,  109 => 43,  107 => 39,  102 => 38,  97 => 36,  94 => 35,  92 => 34,  91 => 31,  90 => 28,  88 => 27,  86 => 26,  83 => 25,  80 => 24,  76 => 23,  72 => 14,  67 => 13,  63 => 12,  58 => 1,  56 => 10,  54 => 9,  52 => 7,  49 => 6,  47 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOrganization/BusinessUnit/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/OrganizationBundle/Resources/views/BusinessUnit/update.html.twig");
    }
}
