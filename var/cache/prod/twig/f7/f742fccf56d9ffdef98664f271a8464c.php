<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/week-day-picker-view.js */
class __TwigTemplate_352091f2d6c44fafe15cdc0a24262f1c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const localeSettings = require('orolocale/js/locale-settings');
    const MultiCheckboxView = require('oroform/js/app/views/multi-checkbox-view');

    const WeekDayPickerView = MultiCheckboxView.extend({
        /**
         * @inheritdoc
         */
        constructor: function WeekDayPickerView(options) {
            WeekDayPickerView.__super__.constructor.call(this, options);
        },

        /**
         * @constructor
         *
         * @param {Object} options
         */
        initialize: function(options) {
            const items = this.createItems();
            WeekDayPickerView.__super__.initialize.call(this, _.extend({items: items}, options));
        },

        createItems: function() {
            const keys = localeSettings.getSortedDayOfWeekNames('mnemonic');
            const texts = localeSettings.getSortedDayOfWeekNames('narrow');
            return _.map(_.object(keys, texts), function(text, key) {
                return {
                    value: key,
                    text: text
                };
            });
        }
    });

    return WeekDayPickerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/week-day-picker-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/week-day-picker-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/week-day-picker-view.js");
    }
}
