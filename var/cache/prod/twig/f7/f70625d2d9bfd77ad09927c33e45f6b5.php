<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSearch/Datagrid/itemContainer.html.twig */
class __TwigTemplate_e26d90ad2f246a5205edfc99a2aaa5b1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        ob_start(function () { return ''; });
        // line 3
        $context["indexer_item"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "indexer_item"], "method", false, false, false, 3);
        // line 4
        echo "
";
        // line 6
        $context["entity"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "entity"], "method", false, false, false, 6);
        // line 7
        echo "
";
        // line 8
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["indexer_item"] ?? null), "entityConfig", [], "any", false, false, false, 8), "search_template", [], "any", false, false, false, 8)) {
            // line 9
            echo "    ";
            $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["indexer_item"] ?? null), "entityConfig", [], "any", false, false, false, 9), "search_template", [], "any", false, false, false, 9), "@OroSearch/Datagrid/itemContainer.html.twig", 9)->display(twig_array_merge($context, ["entity" => ($context["entity"] ?? null), "indexer_item" => ($context["indexer_item"] ?? null)]));
        } else {
            // line 11
            echo "    ";
            echo twig_escape_filter($this->env, ($context["entity"] ?? null), "html", null, true);
            echo "
";
        }
        $___internal_parse_80_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        echo twig_spaceless($___internal_parse_80_);
    }

    public function getTemplateName()
    {
        return "@OroSearch/Datagrid/itemContainer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 1,  55 => 11,  51 => 9,  49 => 8,  46 => 7,  44 => 6,  41 => 4,  39 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSearch/Datagrid/itemContainer.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SearchBundle/Resources/views/Datagrid/itemContainer.html.twig");
    }
}
