<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/macros.html.twig */
class __TwigTemplate_09d4fb7ebb131f1ee3fe6c1a5328d3e2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_collection_prototype($__widget__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            if (twig_in_filter("prototype", twig_get_array_keys_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 2)))) {
                // line 3
                echo "        ";
                $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 3), "prototype", [], "any", false, false, false, 3);
                // line 4
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 4), "prototype", [], "any", false, false, false, 4), "vars", [], "any", false, false, false, 4), "name", [], "any", false, false, false, 4);
                // line 5
                echo "    ";
            } else {
                // line 6
                echo "        ";
                $context["form"] = ($context["widget"] ?? null);
                // line 7
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 7), "full_name", [], "any", false, false, false, 7);
                // line 8
                echo "    ";
            }
            // line 9
            echo "    <div data-content=\"";
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\">
        <div class=\"row-oro\">
            ";
            // line 11
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
            ";
            // line 12
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 13
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'errors');
                echo "
                ";
                // line 14
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
            <button class=\"removeRow btn btn-action btn-link\" type=\"button\" data-related=\"";
            // line 17
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\">×</button>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 22
    public function macro_user_business_unit_name($__user__ = null, $__addBrackets__ = true, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "user" => $__user__,
            "addBrackets" => $__addBrackets__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 23
            ob_start(function () { return ''; });
            // line 24
            echo "        ";
            $context["businessUnit"] = $this->extensions['Oro\Bundle\OrganizationBundle\Twig\OrganizationExtension']->getEntityOwner(($context["user"] ?? null));
            // line 25
            echo "        ";
            if ( !(null === ($context["businessUnit"] ?? null))) {
                // line 26
                echo "            ";
                if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["businessUnit"] ?? null))) {
                    // line 27
                    echo "                ";
                    $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/macros.html.twig", 27)->unwrap();
                    // line 28
                    echo "                ";
                    $context["buView"] = twig_call_macro($macros["UI"], "macro_renderUrl", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_business_unit_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["businessUnit"] ?? null), "id", [], "any", false, false, false, 28)]), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["businessUnit"] ?? null), "name", [], "any", false, false, false, 28)], 28, $context, $this->getSourceContext());
                    // line 29
                    echo "            ";
                } else {
                    // line 30
                    echo "                ";
                    $context["buView"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["businessUnit"] ?? null), "name", [], "any", false, false, false, 30);
                    // line 31
                    echo "            ";
                }
                // line 32
                echo "            ";
                if (($context["addBrackets"] ?? null)) {
                    // line 33
                    echo "                (";
                    echo twig_escape_filter($this->env, ($context["buView"] ?? null), "html", null, true);
                    echo ")
            ";
                } else {
                    // line 35
                    echo "                ";
                    echo twig_escape_filter($this->env, ($context["buView"] ?? null), "html", null, true);
                    echo "
            ";
                }
                // line 37
                echo "        ";
            }
            // line 38
            echo "    ";
            $___internal_parse_51_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 23
            echo twig_spaceless($___internal_parse_51_);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 41
    public function macro_render_user_name($__user__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "user" => $__user__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 42
            echo "    ";
            $context["userName"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["user"] ?? null));
            // line 43
            echo "    ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["user"] ?? null))) {
                // line 44
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/macros.html.twig", 44)->unwrap();
                // line 45
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_renderUrl", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 45)]), ($context["userName"] ?? null)], 45, $context, $this->getSourceContext());
                echo "
    ";
            } else {
                // line 47
                echo "        ";
                echo twig_escape_filter($this->env, ($context["userName"] ?? null), "html", null, true);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroUser/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  216 => 47,  210 => 45,  207 => 44,  204 => 43,  201 => 42,  188 => 41,  179 => 23,  176 => 38,  173 => 37,  167 => 35,  161 => 33,  158 => 32,  155 => 31,  152 => 30,  149 => 29,  146 => 28,  143 => 27,  140 => 26,  137 => 25,  134 => 24,  132 => 23,  118 => 22,  105 => 17,  100 => 16,  92 => 14,  87 => 13,  83 => 12,  79 => 11,  73 => 9,  70 => 8,  67 => 7,  64 => 6,  61 => 5,  58 => 4,  55 => 3,  52 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/macros.html.twig");
    }
}
