<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/page/user-menu-view.js */
class __TwigTemplate_26e5c58fb741a3baca3a7fc35b97e1b8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    './../base/page-region-view'
], function(PageRegionView) {
    'use strict';

    const PageUserMenuView = PageRegionView.extend({
        template: function(data) {
            return data.usermenu;
        },

        pageItems: ['usermenu'],

        /**
         * @inheritdoc
         */
        constructor: function PageUserMenuView(options) {
            PageUserMenuView.__super__.constructor.call(this, options);
        }
    });

    return PageUserMenuView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/page/user-menu-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/page/user-menu-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/page/user-menu-view.js");
    }
}
