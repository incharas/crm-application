<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroForm/layouts/default/form_theme.html.twig */
class __TwigTemplate_7011e1aa1fd77d1cc1e45b3099bc8d75 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'form_row' => [$this, 'block_form_row'],
            'form_label' => [$this, 'block_form_label'],
            'form_errors' => [$this, 'block_form_errors'],
            'attributes' => [$this, 'block_attributes'],
            'choice_widget_collapsed' => [$this, 'block_choice_widget_collapsed'],
            'number_widget' => [$this, 'block_number_widget'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "form_div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("form_div_layout.html.twig", "@OroForm/layouts/default/form_theme.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_form_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<div class=\"form-row\">
        <div class=\"form-row__label\">";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["label_attr" => ["class" => "label"]]);
        // line 7
        echo "</div>
        <div class=\"form-row__content\">";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ["class" => "input input--full"]]);
        // line 14
        echo "</div>";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        // line 16
        echo "</div>";
    }

    // line 19
    public function block_form_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        ob_start(function () { return ''; });
        // line 21
        echo "        ";
        if ( !(($context["label"] ?? null) === false)) {
            // line 22
            echo "            ";
            if ( !($context["compound"] ?? null)) {
                // line 23
                echo "                ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["for" => ($context["id"] ?? null)]);
                // line 24
                echo "            ";
            }
            // line 25
            echo "            ";
            if (($context["required"] ?? null)) {
                // line 26
                echo "                ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 26)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 26), "")) : ("")) . " required"))]);
                // line 27
                echo "            ";
            }
            // line 28
            echo "            ";
            if (twig_test_empty(($context["label"] ?? null))) {
                // line 29
                echo "                ";
                if ( !twig_test_empty(($context["label_format"] ?? null))) {
                    // line 30
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? null), ["%name%" => ($context["name"] ?? null), "%id%" => ($context["id"] ?? null)]);
                    // line 31
                    echo "                ";
                } else {
                    // line 32
                    echo "                    ";
                    $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
                    // line 33
                    echo "                ";
                }
                // line 34
                echo "            ";
            }
            // line 35
            echo "            ";
            $context["isRadioLabel"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 35), "vars", [], "any", false, true, false, 35), "expanded", [], "any", true, true, false, 35)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 35), "vars", [], "any", false, true, false, 35), "expanded", [], "any", false, false, false, 35), false)) : (false)) && array_key_exists("checked", $context));
            // line 36
            echo "
            <label";
            // line 37
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 38
            if ((array_key_exists("translatable_label", $context) &&  !($context["translatable_label"] ?? null))) {
                // line 39
                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
            } elseif ((            // line 40
array_key_exists("raw_label", $context) && ($context["raw_label"] ?? null))) {
                // line 41
                echo ($context["label"] ?? null);
            } else {
                // line 43
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            }
            // line 45
            echo "<em aria-hidden=\"true\">";
            if ((($context["required"] ?? null) &&  !($context["isRadioLabel"] ?? null))) {
                echo "*";
            } else {
                echo "&nbsp;";
            }
            echo "</em>
            </label>";
        }
        $___internal_parse_67_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 20
        echo twig_spaceless($___internal_parse_67_);
    }

    // line 51
    public function block_form_errors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 53
            echo "<ul class=\"notifications notifications--error\">";
            // line 54
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 55
                echo "<li>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 55)), "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 57
            echo "</ul>";
        }
    }

    // line 61
    public function block_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 62
        $context["attributesThatContainsUri"] = [0 => "src", 1 => "href", 2 => "action", 3 => "cite", 4 => "data", 5 => "poster"];
        // line 63
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attr"] ?? null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 64
            if (twig_test_iterable($context["attrvalue"])) {
                // line 65
                $context["attr"] = twig_array_merge(($context["attr"] ?? null), [$context["attrname"] => json_encode($context["attrvalue"])]);
                // line 66
                echo "        ";
            } elseif (twig_in_filter($context["attrname"], ($context["attributesThatContainsUri"] ?? null))) {
                // line 67
                echo "            ";
                $context["attr"] = twig_array_merge(($context["attr"] ?? null), [$context["attrname"] => twig_replace_filter(twig_escape_filter($this->env, $context["attrvalue"], "html"), ["&amp;" => "&"])]);
                // line 68
                echo "        ";
            }
            // line 69
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attr"] ?? null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            // line 72
            echo " ";
            // line 73
            if (twig_in_filter($context["attrname"], [0 => "placeholder", 1 => "title", 2 => "aria-label"])) {
                // line 74
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, (((($context["translation_domain"] ?? null) === false)) ? ($context["attrvalue"]) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["attrvalue"], ($context["attr_translation_parameters"] ?? null), ($context["translation_domain"] ?? null)))), "html", null, true);
                echo "\"";
            } elseif ((            // line 75
$context["attrvalue"] === true)) {
                // line 76
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "\"";
            } elseif ( !(            // line 77
$context["attrvalue"] === false)) {
                // line 78
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 83
    public function block_choice_widget_collapsed($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 84
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " select select--full"],         // line 86
($context["attr"] ?? null));
        // line 87
        echo "
    ";
        // line 88
        $this->displayParentBlock("choice_widget_collapsed", $context, $blocks);
        echo "
";
    }

    // line 91
    public function block_number_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["step" => "any", "min" => "0"]);
        // line 93
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter(($context["type"] ?? null), "number")) : ("number"));
        // line 94
        $this->displayBlock("form_widget_simple", $context, $blocks);
    }

    public function getTemplateName()
    {
        return "@OroForm/layouts/default/form_theme.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  281 => 94,  279 => 93,  277 => 92,  273 => 91,  267 => 88,  264 => 87,  262 => 86,  260 => 84,  256 => 83,  244 => 78,  242 => 77,  237 => 76,  235 => 75,  230 => 74,  228 => 73,  226 => 72,  222 => 71,  216 => 69,  213 => 68,  210 => 67,  207 => 66,  205 => 65,  203 => 64,  198 => 63,  196 => 62,  192 => 61,  187 => 57,  179 => 55,  175 => 54,  173 => 53,  171 => 52,  167 => 51,  163 => 20,  152 => 45,  149 => 43,  146 => 41,  144 => 40,  142 => 39,  140 => 38,  126 => 37,  123 => 36,  120 => 35,  117 => 34,  114 => 33,  111 => 32,  108 => 31,  106 => 30,  103 => 29,  100 => 28,  97 => 27,  94 => 26,  91 => 25,  88 => 24,  85 => 23,  82 => 22,  79 => 21,  77 => 20,  73 => 19,  69 => 16,  67 => 15,  65 => 14,  63 => 9,  60 => 7,  58 => 6,  55 => 4,  51 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroForm/layouts/default/form_theme.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/views/layouts/default/form_theme.html.twig");
    }
}
