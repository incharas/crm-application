<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/styled-scroll-bar-module.js */
class __TwigTemplate_90c5347615e9eaefc24897286bc6a0bd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import \$ from 'jquery';
import 'styled-scroll-bar';

const scrollBarName = 'styled-scrollbar';

\$(document)
    .on('initLayout content:changed', function(e) {
        \$(e.target).find('[data-' + scrollBarName + ']').each(function() {
            const data = \$(this).data(scrollBarName);

            \$(this).styledScrollBar(typeof data === 'object' ? data : {});
        });
    })
    .on('disposeLayout content:remove', function(e) {
        \$(e.target).find('[data-' + scrollBarName + ']').each(function() {
            if (\$(this).data('oro.styledScrollBar')) {
                \$(this).styledScrollBar('dispose');
            }
        });
    });
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/styled-scroll-bar-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/styled-scroll-bar-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/styled-scroll-bar-module.js");
    }
}
