<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/multi-use-resource-manager.js */
class __TwigTemplate_edd4f0e063ef975108ad6ed065803a2c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseClass = require('../base-class');

    /**
     * Allows to create/remove resource that could be used by multiple holders.
     *
     * Use case:
     * ```javascript
     * const backdropManager = new MultiUseResourceManager({
     *     listen: {
     *         'constructResource': function() {
     *             \$(document.body).addClass('backdrop');
     *         },
     *         'disposeResource': function() {
     *             \$(document.body).removeClass('backdrop');
     *         }
     *     }
     * });
     *
     * // 1. case with Ids
     * const holderId = backdropManager.hold();
     * // then somewhere
     * backdropManager.release(holderId);
     *
     * // 2. case with holder object
     * backdropManager.hold(this);
     * // then somewhere, please note that link to the same object should be provided
     * backdropManager.release(this);
     *
     * // 2. case with holder identifier
     * backdropManager.hold(this.cid);
     * // then somewhere, please note that link to the same object should be provided
     * backdropManager.release(this.cid);
     * ```
     *
     * @class
     * @augments [BaseClass](./base-class.md)
     * @exports MultiUseResourceManager
     */
    const MultiUseResourceManager = BaseClass.extend(/** @lends MultiUseResourceManager.prototype */{
        /**
         * Holders counter
         * @type {number}
         * @protected
         */
        counter: 1, // should start from 1 for easier usage (0 == false)
        /**
         * True if resource is created
         * @type {boolean}
         */
        isCreated: false,
        /**
         * Array of ids of current resource holders
         * @type {Array}
         */
        holders: null,

        /**
         * @inheritdoc
         */
        constructor: function MultiUseResourceManager(options) {
            this.holders = [];
            MultiUseResourceManager.__super__.constructor.call(this, options);
        },

        /**
         * Holds resource
         *
         * @param holder {*} holder identifier
         * @returns {*} holder identifier
         */
        hold: function(holder) {
            if (!holder) {
                holder = this.counter;
                this.counter = holder + 1;
            }
            this.holders.push(holder);
            this.checkState();
            return holder;
        },

        /**
         * Releases resource
         *
         * @param id {*} holder identifier
         */
        release: function(id) {
            const index = this.holders.indexOf(id);
            if (index !== -1) {
                this.holders.splice(index, 1);
            }
            this.checkState();
        },

        /**
         * Returns true if resource holder has been already released
         *
         * @param id {*} holder identifier
         * @returns {boolean}
         */
        isReleased: function(id) {
            return this.holders.indexOf(id) === -1;
        },

        /**
         * Check state, creates or disposes resource if required
         *
         * @protected
         */
        checkState: function() {
            if (this.holders.length > 0 && !this.isCreated) {
                this.isCreated = true;
                this.trigger('constructResource');
                return;
            }
            if (this.holders.length <= 0 && this.isCreated) {
                this.isCreated = false;
                this.trigger('disposeResource');
            }
        },

        /**
         * @inheritdoc
         */
        dispose: function() {
            if (this.isCreated) {
                this.trigger('disposeResource');
                this.isCreated = false;
            }
            MultiUseResourceManager.__super__.dispose.call(this);
        }
    });

    return MultiUseResourceManager;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/multi-use-resource-manager.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/multi-use-resource-manager.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/multi-use-resource-manager.js");
    }
}
