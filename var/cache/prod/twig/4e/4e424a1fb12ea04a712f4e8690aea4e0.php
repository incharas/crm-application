<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/mixins/buttons.scss */
class __TwigTemplate_253dbfb1cd0f47a96bed56ec4595609a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@mixin button-custom-variant(
    \$background,
    \$border,
    \$color: color-yiq(\$background),
    \$hover-background: darken(\$background, 8%),
    \$hover-border: darken(\$border, 8%),
    \$hover-color: color-yiq(\$hover-background),
    \$active-background: darken(\$background, 8%),
    \$active-border: darken(\$border, 8%),
    \$active-color: color-yiq(\$active-background),
    \$active-box-shadow: null,
    \$focus-background: null,
    \$focus-border: null,
    \$focus-color: null,
    \$focus-box-shadow: null,
    \$disabled-color: \$color,
    \$disabled-background: \$background,
    \$disabled-border: \$border,
    \$disabled-opacity: null
) {
    color: \$color;

    @include gradient-bg(\$background);

    border-color: \$border;

    @include box-shadow(\$btn-box-shadow);

    @include hover {
        color: \$hover-color;

        @include gradient-bg(\$hover-background);

        border-color: \$hover-border;
    }

    &:focus,
    &.focus {
        color: \$focus-color;
        background-color: \$focus-background;
        border-color: \$focus-border;

        // Avoid using mixin so we can pass custom focus shadow properly
        @if \$enable-shadows {
            box-shadow: \$btn-box-shadow;
        } @else {
            box-shadow: \$focus-box-shadow;
        }
    }

    // Disabled comes first so active can properly restyle
    &.disabled,
    &:disabled {
        color: \$disabled-color;
        background-color: \$disabled-background;
        border-color: \$disabled-border;
        opacity: \$disabled-opacity;
    }

    &:not(:disabled):not(.disabled):active,
    &:not(:disabled):not(.disabled).active,
    .show > &.dropdown-toggle {
        color: \$active-color;
        background-color: \$active-background;

        // Remove the gradient for the pressed/active state
        @if \$enable-gradients {
            background-image: none;
        }

        border-color: \$active-border;
        box-shadow: \$active-box-shadow;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/mixins/buttons.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/mixins/buttons.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/mixins/buttons.scss");
    }
}
