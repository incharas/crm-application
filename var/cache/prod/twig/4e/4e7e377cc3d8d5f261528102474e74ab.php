<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Reset/request.html.twig */
class __TwigTemplate_1838baa69d095cab35488f6248a9f3ff extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'bodyClass' => [$this, 'block_bodyClass'],
            'messages' => [$this, 'block_messages'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUser/layout.html.twig", "@OroUser/Reset/request.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "login-page";
    }

    // line 4
    public function block_messages($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    ";
        $context["messagesContent"] = $this->renderParentBlock("messages", $context, $blocks);
    }

    // line 8
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "<div class=\"container\">
    <div class=\"form-wrapper\">
        <div class=\"form-wrapper__inner\">
            ";
        // line 12
        $context["requestLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Request");
        // line 13
        echo "            ";
        $context["returnToLoginLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Return to Login");
        // line 14
        echo "            ";
        $context["showLabels"] = ((twig_length_filter($this->env, ($context["requestLabel"] ?? null)) <= 9) && (twig_length_filter($this->env, ($context["returnToLoginLabel"] ?? null)) <= 15));
        // line 15
        echo "            ";
        $context["layoutName"] = ((($context["showLabels"] ?? null)) ? ("form-row-layout") : ("form-column-layout"));
        // line 16
        echo "            <form action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_reset_send_email");
        echo "\" method=\"post\" class=\"form-signin form-signin--forgot ";
        echo twig_escape_filter($this->env, ($context["layoutName"] ?? null), "html", null, true);
        echo "\">
                <div class=\"title-box\">
                    <h2 class=\"title\">";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Forgot Password"), "html", null, true);
        echo "</h2>
                </div>
                <fieldset class=\"form-signin__fieldset\">
                    ";
        // line 21
        echo twig_escape_filter($this->env, ($context["messagesContent"] ?? null), "html", null, true);
        echo "
                    <input type=\"text\" id=\"prependedInput\" name=\"username\" required=\"required\" placeholder=\"";
        // line 22
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Username or Email"), "html", null, true);
        echo "\" autofocus/>
                    <input type=\"hidden\" name=\"frontend\" value=\"1\" />
                    <div class=\"form-row form-signin__footer form-signin__footer\">
                        <button type=\"submit\" class=\"btn extra-submit btn-uppercase btn-primary\">";
        // line 25
        echo twig_escape_filter($this->env, ($context["requestLabel"] ?? null), "html", null, true);
        echo "</button>
                        <a href=\"";
        // line 26
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_security_login");
        echo "\">&laquo; ";
        echo twig_escape_filter($this->env, ($context["returnToLoginLabel"] ?? null), "html", null, true);
        echo "</a>
                    </div>
                </fieldset>
                <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 29
        echo twig_escape_filter($this->env, $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderCsrfToken("oro-user-password-reset-request"), "html", null, true);
        echo "\"/>
            </form>
        </div>
        <div class=\"login-copyright\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.copyright", ["{{year}}" => twig_date_format_filter($this->env, "now", "Y")]), "html", null, true);
        echo "</div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Reset/request.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 32,  120 => 29,  112 => 26,  108 => 25,  102 => 22,  98 => 21,  92 => 18,  84 => 16,  81 => 15,  78 => 14,  75 => 13,  73 => 12,  68 => 9,  64 => 8,  59 => 5,  55 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Reset/request.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Reset/request.html.twig");
    }
}
