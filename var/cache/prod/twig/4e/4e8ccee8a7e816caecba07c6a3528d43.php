<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/userEmails.html.twig */
class __TwigTemplate_5bac45ed8223a7798df2eeb0b257b87c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content_datagrid' => [$this, 'block_content_datagrid'],
            'navButtons' => [$this, 'block_navButtons'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["EmailActions"] = $this->macros["EmailActions"] = $this->loadTemplate("@OroEmail/actions.html.twig", "@OroEmail/Email/userEmails.html.twig", 2)->unwrap();
        // line 4
        $context["name"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 4)), "N/A");

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%username%" =>         // line 5
($context["name"] ?? null)]]);
        // line 8
        $context["params"] = ["userId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8)];
        // line 9
        $context["gridName"] = "user-email-grid";
        // line 11
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 12
            $context["renderParams"] = ["themeOptions" => ["tagName" => "div", "headerHide" => true, "showMassActionOnToolbar" => true, "bodyClassName" => "user-emails-grid", "rowTemplateSelector" => "#template-user-email-grid-row"], "toolbarOptions" => ["placement" => ["bottom" => true]]];
        }
        // line 56
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.entity_plural_label");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroEmail/Email/userEmails.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 29
    public function block_content_datagrid($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $this->displayParentBlock("content_datagrid", $context, $blocks);
        echo "
    ";
        // line 31
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 32
            echo "        <script id=\"template-user-email-grid-row\" type=\"text/template\">
            <div class=\"user-emails-row\">
                <div class=\"user-emails-row__top clearfix\">
                    <div class=\"user-emails-row__select-row\">
                        <input type=\"checkbox\" data-role=\"select-row\">
                    </div>
                    <div class=\"user-emails-row__contacts\">
                        <%= model.contacts %>
                    </div>
                    <div class=\"user-emails-row__date\">
                        <%= model.sentAt %>
                    </div>
                    <div class=\"user-emails-row__attachments\">
                        <%= model.attachments %>
                    </div>
                </div>
                <div class=\"user-emails-row__subject\">
                    <%= model.subject %>
                </div>
            </div>
        </script>
    ";
        }
    }

    // line 58
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    ";
        $macros["EmailActions"] = $this->loadTemplate("@OroEmail/actions.html.twig", "@OroEmail/Email/userEmails.html.twig", 59)->unwrap();
        // line 60
        echo "
    ";
        // line 61
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
            // line 62
            echo "        <div class=\"btn-group\">
            ";
            // line 63
            echo twig_call_macro($macros["EmailActions"], "macro_createEmailButton", [["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 64
($context["app"] ?? null), "user", [], "any", false, false, false, 64), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 65
($context["app"] ?? null), "user", [], "any", false, false, false, 65), "id", [], "any", false, false, false, 65)]], 63, $context, $this->getSourceContext());
            // line 66
            echo "
        </div>
    ";
        }
        // line 70
        $context["actionSync"] = ((("<i class=\"fa-refresh hide-text\">" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.action.sync")) . "</i>") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.action.sync"));
        // line 76
        $context["actionProcessing"] = (((("<i class=\"fa-refresh hide-text\">" . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.action.processing")) . "</i>") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.action.processing")) . "&nbsp;<span class=\"loading-dots\"></span>");
        // line 83
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/userEmails.html.twig", 83)->unwrap();
        // line 84
        echo "
    <div class=\"btn-group\" ";
        // line 85
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroemail/js/app/views/email-sync-view", "options" => ["syncPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_user_sync_emails"), "processingMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.action.message.processing"), "errorHandlerMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.action.message.error"), "actionProcessing" =>         // line 91
($context["actionProcessing"] ?? null), "actionSync" =>         // line 92
($context["actionSync"] ?? null)]]], 85, $context, $this->getSourceContext());
        // line 94
        echo ">
        <a href=\"#\" role=\"button\" class=\"btn sync-btn icons-holder-text no-hash\" data-role=\"sync\">
            ";
        // line 96
        echo ($context["actionSync"] ?? null);
        echo "
        </a>
    </div>
";
    }

    // line 101
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 102
        echo "    ";
        $context["breadcrumbs"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.menu.user_emails")]];
        // line 105
        echo "    ";
        $this->loadTemplate("@OroNavigation/Menu/breadcrumbs.html.twig", "@OroEmail/Email/userEmails.html.twig", 105)->display($context);
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/userEmails.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 105,  158 => 102,  154 => 101,  146 => 96,  142 => 94,  140 => 92,  139 => 91,  138 => 85,  135 => 84,  132 => 83,  130 => 76,  128 => 70,  123 => 66,  121 => 65,  120 => 64,  119 => 63,  116 => 62,  114 => 61,  111 => 60,  108 => 59,  104 => 58,  78 => 32,  76 => 31,  71 => 30,  67 => 29,  62 => 1,  60 => 56,  57 => 12,  55 => 11,  53 => 9,  51 => 8,  49 => 5,  46 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/userEmails.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/userEmails.html.twig");
    }
}
