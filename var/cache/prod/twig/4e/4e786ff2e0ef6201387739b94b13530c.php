<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/widget-picker/widget-picker-filter-model.js */
class __TwigTemplate_582b7e9a029fa6365d6df00a47658bae extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseModel = require('oroui/js/app/models/base/model');

    const WidgetPickerFilterModel = BaseModel.extend({
        defaults: {
            search: ''
        },

        /**
         * @inheritdoc
         */
        constructor: function WidgetPickerFilterModel(attrs, options) {
            WidgetPickerFilterModel.__super__.constructor.call(this, attrs, options);
        },

        /**
         * @param {WidgetPickerModel} item
         * @returns {boolean} true if item included and false otherwise
         */
        filterer: function(item) {
            const search = this.get('search').toLowerCase();
            const title = item.get('title').toLowerCase();
            const description = item.get('description').toLowerCase();
            if (search.length === 0) {
                return true;
            }
            return title.indexOf(search) !== -1 || description.indexOf(search) !== -1;
        }
    });

    return WidgetPickerFilterModel;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/widget-picker/widget-picker-filter-model.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/widget-picker/widget-picker-filter-model.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/widget-picker/widget-picker-filter-model.js");
    }
}
