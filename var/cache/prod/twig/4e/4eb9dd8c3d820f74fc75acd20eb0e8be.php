<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Thread/userEmails.html.twig */
class __TwigTemplate_d0ecdf4f2a1f1d9260351db6a2b1f548 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroEmail/Email/Thread/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroEmail/Email/Thread/view.html.twig", "@OroEmail/Email/Thread/userEmails.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/Thread/userEmails.html.twig", 4)->unwrap();
        // line 5
        echo "    ";
        ob_start(function () { return ''; });
        // line 6
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "wid" => "thread-view", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_user_thread_widget", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["entity"] ?? null), "id", [], "any", false, false, false, 9), "renderContexts" => false, "showSingleEmail" =>  !$this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_email.threads_grouping")]), "alias" => "thread-view", "contextsRendered" => true]);
        // line 13
        echo "
    ";
        $context["emailInfoWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 15
        echo "
    ";
        // line 16
        ob_start(function () { return ''; });
        // line 17
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_activities", $context)) ? (_twig_default_filter(($context["view_content_data_activities"] ?? null), "view_content_data_activities")) : ("view_content_data_activities")), ["entity" => ($context["entity"] ?? null)]);
        // line 18
        echo "    ";
        $context["activitiesData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 19
        echo "
    ";
        // line 20
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 25
($context["emailInfoWidget"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.sections.activities"), "subblocks" => [0 => ["spanClass" => "widget-content email-activity-widget", "data" => [0 =>         // line 32
($context["activitiesData"] ?? null)]]]]];
        // line 36
        echo "
    ";
        // line 37
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 38
        echo "    ";
        $context["id"] = "userEmails";
        // line 39
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_scrollData", [($context["id"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null)], 39, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Thread/userEmails.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 39,  87 => 38,  85 => 37,  82 => 36,  80 => 32,  79 => 25,  78 => 20,  75 => 19,  72 => 18,  69 => 17,  67 => 16,  64 => 15,  60 => 13,  58 => 9,  56 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Thread/userEmails.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Thread/userEmails.html.twig");
    }
}
