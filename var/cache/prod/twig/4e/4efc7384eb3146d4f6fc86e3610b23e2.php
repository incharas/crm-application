<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityExtend/ConfigEntityGrid/create.html.twig */
class __TwigTemplate_056b227a67a0f1e51d5ace39b5fadaa0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), "@OroForm/Form/fields.html.twig", true);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")]]);
        // line 5
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityextend_entity_create");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEntityExtend/ConfigEntityGrid/create.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityExtend/ConfigEntityGrid/create.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index")], 10, $context, $this->getSourceContext());
        echo "
    ";
        // line 11
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_entityconfig_view", "params" => ["id" => "\$id"]]], 11, $context, $this->getSourceContext());
        // line 15
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_entityextend_entity_create")) {
            // line 16
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_entityextend_entity_create"]], 16, $context, $this->getSourceContext()));
            // line 19
            echo "    ";
        }
        // line 20
        echo "    ";
        $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_entityconfig_update", "params" => ["id" => "\$id"]]], 20, $context, $this->getSourceContext()));
        // line 24
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 24, $context, $this->getSourceContext());
        echo "
";
    }

    // line 27
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    ";
        $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.config_grid.entities"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.config_grid.new_entity")];
        // line 34
        echo "
    ";
        // line 35
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 38
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "    ";
        $this->displayParentBlock("stats", $context, $blocks);
        echo "
";
    }

    // line 42
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    ";
        $context["id"] = "configentity-create";
        // line 44
        echo "    ";
        $context["dataBlocks"] = $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormDataBlocks($this->env, $context, ($context["form"] ?? null));
        // line 45
        echo "    ";
        $context["data"] = ["formErrors" => ((        // line 46
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 47
($context["dataBlocks"] ?? null), "hiddenData" =>         // line 48
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest')];
        // line 50
        echo "
    ";
        // line 51
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityExtend/ConfigEntityGrid/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 51,  134 => 50,  132 => 48,  131 => 47,  130 => 46,  128 => 45,  125 => 44,  122 => 43,  118 => 42,  111 => 39,  107 => 38,  101 => 35,  98 => 34,  95 => 28,  91 => 27,  84 => 24,  81 => 20,  78 => 19,  75 => 16,  72 => 15,  70 => 11,  66 => 10,  63 => 9,  60 => 8,  56 => 7,  51 => 1,  49 => 5,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityExtend/ConfigEntityGrid/create.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityExtendBundle/Resources/views/ConfigEntityGrid/create.html.twig");
    }
}
