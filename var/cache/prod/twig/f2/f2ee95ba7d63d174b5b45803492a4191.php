<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/dialog/update.html.twig */
class __TwigTemplate_3ba91fbff92266052403b02d3f83a183 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'page_container' => [$this, 'block_page_container'],
            'page_container_before_form' => [$this, 'block_page_container_before_form'],
            'page_container_form_actions' => [$this, 'block_page_container_form_actions'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => "@OroEmail/Form/fields.html.twig"], true);
        // line 8
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/dialog/update.html.twig", 8)->unwrap();
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('page_container', $context, $blocks);
    }

    public function block_page_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        if (($context["saved"] ?? null)) {
            // line 12
            echo "        ";
            $context["widgetResponse"] = ["widget" => ["message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.the_email_was_sent"), "triggerSuccess" => true, "trigger" => [0 => "datagrid:doRefresh:attachment-grid", 1 => "widget:doRefresh:email-thread"], "remove" => true]];
            // line 20
            echo "
        ";
            // line 21
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
    ";
        } else {
            // line 23
            echo "        <div class=\"widget-content email-form\">
            ";
            // line 24
            $this->displayBlock('page_container_before_form', $context, $blocks);
            // line 25
            echo "            ";
            if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 25), "valid", [], "any", false, false, false, 25) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 25), "errors", [], "any", false, false, false, 25)))) {
                // line 26
                echo "                <div class=\"alert alert-error\" role=\"alert\">
                    <div class=\"message\">
                        ";
                // line 28
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
                echo "
                    </div>
                </div>
            ";
            }
            // line 32
            echo "            <div class=\"form-container\">
                <form id=\"";
            // line 33
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 33), "id", [], "any", false, false, false, 33), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 33), "name", [], "any", false, false, false, 33), "html", null, true);
            echo "\"
                      method=\"post\" action=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->urlAddQueryParameters(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 34), "uri", [], "any", false, false, false, 34), ["entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "entityId", [], "any", false, false, false, 34)]), "html", null, true);
            echo "\" enctype=\"multipart/form-data\">
                    ";
            // line 35
            $context["emailEditorOptions"] = ["entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 36
($context["entity"] ?? null), "entityId", [], "any", false, false, false, 36), "to" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 37
($context["entity"] ?? null), "to", [], "any", false, false, false, 37), "cc" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 38
($context["entity"] ?? null), "cc", [], "any", false, false, false, 38), "bcc" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 39
($context["entity"] ?? null), "bcc", [], "any", false, false, false, 39), "appendSignature" =>             // line 40
($context["appendSignature"] ?? null), "minimalWysiwygEditorHeight" => 150, "isSignatureEditable" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_user_update")];
            // line 44
            echo "                    <fieldset class=\"form-horizontal\"
                              data-page-component-module=\"oroemail/js/app/components/email-editor-component\"
                              data-page-component-options=\"";
            // line 46
            echo twig_escape_filter($this->env, json_encode(($context["emailEditorOptions"] ?? null)), "html", null, true);
            echo "\"
                              data-layout=\"separate\"
                            >
                        ";
            // line 49
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "gridName", [], "any", false, false, false, 49), 'row');
            echo "
                        ";
            // line 50
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "origin", [], "any", false, false, false, 50), 'row');
            echo "
                        ";
            // line 51
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "to", [], "any", false, false, false, 51), 'row');
            echo "
                        ";
            // line 52
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "cc", [], "any", false, false, false, 52), 'row');
            echo "
                        ";
            // line 53
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "bcc", [], "any", false, false, false, 53), 'row');
            echo "
                        ";
            // line 54
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subject", [], "any", false, false, false, 54), 'row');
            echo "

                        ";
            // line 56
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "template", [], "any", true, true, false, 56)) {
                // line 57
                echo "                            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "template", [], "any", false, false, false, 57), 'row', ["includeNonEntity" => true, "includeSystemTemplates" => false]);
                echo "
                        ";
            }
            // line 59
            echo "
                        ";
            // line 60
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 60), 'row');
            echo "
                        ";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "body", [], "any", false, false, false, 61), 'row');
            echo "

                        <div class=\"control-group email-body-actions\">
                            <div class=\"controls\">
                                <span class=\"email-body-action\"><a id=\"add-signature\" href=\"#\">";
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.form.add_signature"), "html", null, true);
            echo "</a></span>

                                <span>";
            // line 67
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.form.attach_file"), "html", null, true);
            echo ": </span>
                                <div class=\"dropup\" style=\"display: inline-block\">
                                    <a class=\"attach-file dropdown-toggle\" href=\"#\" aria-expanded=\"true\">";
            // line 69
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.form.from_record"), "html", null, true);
            echo "</a>
                                    <div class=\"dropdown-menu attachment-list-popup\" role=\"menu\" aria-labelledby=\"attach-file\"></div>
                                </div>
                                <a class=\"upload-new divider\" href=\"#\">";
            // line 72
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.form.upload"), "html", null, true);
            echo "</a>
                            </div>
                        </div>

                        ";
            // line 77
            echo "                        ";
            $context["emailAttachmentOptions"] = ["popupTriggerButton" => ".attach-file", "uploadNewButton" => ".upload-new", "popupContentEl" => ".attachment-list-popup", "entityAttachments" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 81
($context["entity"] ?? null), "attachments", [], "any", false, false, false, 81), "toArray", [], "any", false, false, false, 81), "attachmentsAvailable" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 82
($context["entity"] ?? null), "attachmentsAvailable", [], "any", false, false, false, 82)];
            // line 84
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "attachments", [], "any", false, false, false, 84), 'row', ["options" => ($context["emailAttachmentOptions"] ?? null)]);
            echo "

                        ";
            // line 86
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
                        <div class=\"widget-actions form-actions\" style=\"display: none;\">
                            ";
            // line 88
            $this->displayBlock('page_container_form_actions', $context, $blocks);
            // line 92
            echo "                        </div>
                    </fieldset>
                </form>
                ";
            // line 95
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
            </div>
        </div>
    ";
        }
    }

    // line 24
    public function block_page_container_before_form($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 88
    public function block_page_container_form_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "                                <button class=\"btn\" type=\"reset\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
                                <button class=\"btn btn-success\" type=\"submit\">";
        // line 90
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Send"), "html", null, true);
        echo "</button>
                            ";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/dialog/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 90,  229 => 89,  225 => 88,  219 => 24,  210 => 95,  205 => 92,  203 => 88,  198 => 86,  192 => 84,  190 => 82,  189 => 81,  187 => 77,  180 => 72,  174 => 69,  169 => 67,  164 => 65,  157 => 61,  153 => 60,  150 => 59,  144 => 57,  142 => 56,  137 => 54,  133 => 53,  129 => 52,  125 => 51,  121 => 50,  117 => 49,  111 => 46,  107 => 44,  105 => 40,  104 => 39,  103 => 38,  102 => 37,  101 => 36,  100 => 35,  96 => 34,  90 => 33,  87 => 32,  80 => 28,  76 => 26,  73 => 25,  71 => 24,  68 => 23,  63 => 21,  60 => 20,  57 => 12,  54 => 11,  47 => 10,  44 => 9,  42 => 8,  40 => 7,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/dialog/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/dialog/update.html.twig");
    }
}
