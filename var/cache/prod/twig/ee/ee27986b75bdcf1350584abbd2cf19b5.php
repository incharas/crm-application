<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChannel/Channel/index.html.twig */
class __TwigTemplate_0d2fd409f274a57f1004be812457ccb6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/Channel/index.html.twig", 3)->unwrap();
        // line 5
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_plural_label");
        // line 6
        $context["gridName"] = "oro-channels-grid";
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroChannel/Channel/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_channel_create")) {
            // line 10
            echo "        ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/Channel/index.html.twig", 10)->unwrap();
            // line 11
            echo "
        ";
            // line 12
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_create"), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_label")]], 12, $context, $this->getSourceContext());
            // line 15
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroChannel/Channel/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  68 => 15,  66 => 12,  63 => 11,  60 => 10,  57 => 9,  53 => 8,  48 => 1,  46 => 6,  44 => 5,  42 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChannel/Channel/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ChannelBundle/Resources/views/Channel/index.html.twig");
    }
}
