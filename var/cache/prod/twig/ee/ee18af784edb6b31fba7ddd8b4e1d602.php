<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCurrency/Datagrid/Column/baseCurrency.html.twig */
class __TwigTemplate_75a5df8756b2549e18a8a871d4a5e904 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["data"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => ($context["base_currency_field"] ?? null)], "method", false, false, false, 1);
        // line 2
        if (($context["data"] ?? null)) {
            // line 3
            echo "    ";
            echo twig_escape_filter($this->env, ($context["currency"] ?? null), "html", null, true);
            echo twig_escape_filter($this->env, twig_number_format_filter($this->env, ($context["data"] ?? null), 2), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCurrency/Datagrid/Column/baseCurrency.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCurrency/Datagrid/Column/baseCurrency.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CurrencyBundle/Resources/views/Datagrid/Column/baseCurrency.html.twig");
    }
}
