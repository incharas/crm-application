<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables/accordion-variables.scss */
class __TwigTemplate_f4b2b5a0b5450b9600d0dff75d836907 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$accordion-toggle-background: \$primary-900 !default;
\$accordion-toggle-font-size: 14px !default;
\$accordion-toggle-font-weight: font-weight('bold') !default;
\$accordion-toggle-offset: 0 0 8px 0 !default;

\$accordion-toggle-before-content: '\\f107' !default;
\$accordion-toggle-before-font-family: \$fa-font-family !default;
\$accordion-toggle-before-font-size: 16px !default;
\$accordion-toggle-before-offset: 0 0 8px 0 !default;
\$accordion-toggle-before-width: 10px !default;
\$accordion-toggle-before-display: inline-block !default;

\$accordion-toggle-collapsed-before-content: '\\f105' !default;
\$accordion-toggle-collapsed-before-offset: 0 4px 0 0 !default;
\$accordion-toggle-collapsed-before-inner-offset: 0 0 0 4px !default;

\$accordion-group-responsive-section-border-radius: 0 !default;
\$accordion-group-responsive-section-offset-bottom: 0 !default;
\$accordion-group-responsive-section-border: none !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables/accordion-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables/accordion-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables/accordion-variables.scss");
    }
}
