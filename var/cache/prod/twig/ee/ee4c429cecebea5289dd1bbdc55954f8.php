<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNotification/NotificationAlert/index.html.twig */
class __TwigTemplate_9d0cf2d0465a8ec0a2cdf5dd91eb012a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNotification/NotificationAlert/index.html.twig", 3)->unwrap();
        // line 5
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.notificationalert.entity_plural_label");
        // line 6
        $context["gridName"] = "oro-notification-alerts-grid";
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroNotification/NotificationAlert/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroNotification/NotificationAlert/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 1,  45 => 6,  43 => 5,  41 => 3,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNotification/NotificationAlert/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NotificationBundle/Resources/views/NotificationAlert/index.html.twig");
    }
}
