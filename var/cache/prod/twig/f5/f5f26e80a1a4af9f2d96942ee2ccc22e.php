<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/multi-checkbox-editor.scss */
class __TwigTemplate_596f1af0f62619b9bc8b11035f416b4b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.inline-editor-wrapper.multi-checkbox-editor {
    // stylelint-disable-next-line declaration-no-important
    z-index: auto !important;
    min-width: 190px;

    .inline-editor__fields {
        height: 0;
        overflow: hidden;
    }

    button.ui-multiselect {
        // stylelint-disable-next-line declaration-no-important
        width: 100% !important;
        height: 0;
        border: 0 none;
        padding: 0;
        overflow: hidden;
    }
}

.ui-multiselect-menu.multi-checkbox-editor {
    margin: 0;
    padding: 0;
    border: 0 none;
    z-index: 10001;
    background: transparent;

    &::after {
        position: absolute;
        content: '';

        border: \$select2-drop-border;
        background: \$select2-drop-background;
        border-radius: \$select2-drop-border-radius;
        box-shadow: \$select2-drop-box-shadow;
        top: \$oro-datagrid-editable-action-item-size;
        left: \$select2-container-border-width;
        right: \$select2-container-border-width;
        height: calc(100% - #{\$oro-datagrid-editable-action-item-size});
    }

    .ui-widget-header {
        padding: 0;
        background: transparent;
        border: 0 none;
        margin: 0;
    }

    .ui-multiselect-filter {
        float: none;
        margin-right: 0;

        input {
            width: 100%;
            margin: 0;
            padding: \$oro-datagrid-editable-action-input-offset;
            height: \$oro-datagrid-editable-action-item-size;

            @include font-size(\$input-font-size);

            font-weight: \$input-font-weight;
            color: \$input-color;
            line-height: \$oro-datagrid-editable-action-item-line-height;
            border-radius: \$oro-datagrid-select-container-border-radius;
            border: \$select2-container-border;

            &:focus {
                border-color: \$input-focus-border-color;
            }
        }
    }

    .ui-multiselect-checkboxes {
        max-height: \$select2-results-max-height;
        padding: \$select2-results-inner-offset;
        margin: \$select2-results-offset;
        position: \$select2-results-position;
        overflow-x: \$select2-results-overflow-x;
        overflow-y: \$select2-results-overflow-y;
        z-index: 1;

        label {
            margin: 0;
            padding: \$select2-result-label-inner-offset;
            display: flex;
            gap: 8px;
            cursor: pointer;

            &.ui-state-hover {
                background-image: none;
                background-color: \$select2-result-highlighted-background;
            }

            &.ui-state-active {
                background-image: none;
                background-color: \$select2-result-highlighted-background;
            }
        }

        li {
            position: relative;
            color: \$select2-result-label-color;
            font-size: \$select-filter-widget-li-font-size;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/multi-checkbox-editor.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/multi-checkbox-editor.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/multi-checkbox-editor.scss");
    }
}
