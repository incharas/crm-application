<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @GosWebSocket/Collector/websocket.html.twig */
class __TwigTemplate_5095072d7ce4741945c88de2f22cc923 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'menu' => [$this, 'block_menu'],
            'panel' => [$this, 'block_panel'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        @trigger_error("The \"@GosWebSocket/Collector/websocket.html.twig\" template is deprecated and will be removed in GosWebSocketBundle 4.0."." (\"@GosWebSocket/Collector/websocket.html.twig\" at line 1).", E_USER_DEPRECATED);
        // line 2
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@GosWebSocket/Collector/websocket.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_toolbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "pushTotal", [], "any", false, false, false, 5)) {
            // line 6
            echo "        ";
            ob_start(function () { return ''; });
            // line 7
            echo "            ";
            echo twig_include($this->env, $context, "@GosWebSocket/Collector/icon.svg");
            echo "
            <span class=\"sf-toolbar-value\">";
            // line 8
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "pushTotal", [], "any", false, false, false, 8), "html", null, true);
            echo "</span>
        ";
            $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 10
            echo "
        ";
            // line 11
            ob_start(function () { return ''; });
            // line 12
            echo "            <div class=\"sf-toolbar-info-piece\">
                <b>Pushes</b>
                <span class=\"sf-toolbar-status\">";
            // line 14
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "pushTotal", [], "any", false, false, false, 14), "html", null, true);
            echo "</span>
            </div>

            <div class=\"sf-toolbar-info-piece\">
                <b>Duration</b>
                <span class=\"sf-toolbar-status\">";
            // line 19
            echo twig_escape_filter($this->env, twig_sprintf("%0.2f", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "totalDuration", [], "any", false, false, false, 19)), "html", null, true);
            echo " ms</span>
            </div>
        ";
            $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 22
            echo "
        ";
            // line 23
            echo twig_include($this->env, $context, "@WebProfiler/Profiler/toolbar_item.html.twig", ["link" => true]);
            echo "
    ";
        }
    }

    // line 27
    public function block_menu($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    <span class=\"label ";
        echo (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "pushTotal", [], "any", false, false, false, 28) == 0)) ? ("disabled") : (""));
        echo "\">
        <span class=\"icon\">";
        // line 29
        echo twig_include($this->env, $context, "@GosWebSocket/Collector/icon.svg");
        echo "</span>
        <strong>Websocket</strong>
    </span>
";
    }

    // line 34
    public function block_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    <h2>Websocket Pushes</h2>

    ";
        // line 37
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "pushTotal", [], "any", false, false, false, 37) == 0)) {
            // line 38
            echo "        <div class=\"empty\">
            <p>No messages were pushed.</p>
        </div>
    ";
        } else {
            // line 42
            echo "        <div class=\"metrics\">
            <div class=\"metric\">
                <span class=\"value\">";
            // line 44
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "pushTotal", [], "any", false, false, false, 44), "html", null, true);
            echo "</span>
                <span class=\"label\">Total Pushes</span>
            </div>

            <div class=\"metric\">
                <span class=\"value\">";
            // line 49
            echo twig_escape_filter($this->env, twig_sprintf("%0.2f", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "totalDuration", [], "any", false, false, false, 49)), "html", null, true);
            echo " <span class=\"unit\">ms</span></span>
                <span class=\"label\">Duration</span>
            </div>
        </div>

        ";
            // line 54
            if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "durations", [], "any", false, false, false, 54))) {
                // line 55
                echo "            <h2>Push Durations</h2>

            <div class=\"metrics\">
                ";
                // line 58
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "durations", [], "any", false, false, false, 58));
                foreach ($context['_seq'] as $context["pusher"] => $context["duration"]) {
                    // line 59
                    echo "                    <div class=\"metric\">
                        <span class=\"value\">";
                    // line 60
                    echo twig_escape_filter($this->env, twig_sprintf("%0.2f", $context["duration"]), "html", null, true);
                    echo " <span class=\"unit\">ms</span></span>
                        <span class=\"label\">";
                    // line 61
                    echo twig_escape_filter($this->env, twig_upper_filter($this->env, $context["pusher"]), "html", null, true);
                    echo " (";
                    echo twig_escape_filter($this->env, (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "pusherCounts", [], "any", false, false, false, 61)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[$context["pusher"]] ?? null) : null), "html", null, true);
                    echo ")</span>
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['pusher'], $context['duration'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 64
                echo "            </div>
        ";
            }
            // line 66
            echo "    ";
        }
    }

    public function getTemplateName()
    {
        return "@GosWebSocket/Collector/websocket.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  190 => 66,  186 => 64,  175 => 61,  171 => 60,  168 => 59,  164 => 58,  159 => 55,  157 => 54,  149 => 49,  141 => 44,  137 => 42,  131 => 38,  129 => 37,  125 => 35,  121 => 34,  113 => 29,  108 => 28,  104 => 27,  97 => 23,  94 => 22,  88 => 19,  80 => 14,  76 => 12,  74 => 11,  71 => 10,  66 => 8,  61 => 7,  58 => 6,  55 => 5,  51 => 4,  46 => 2,  44 => 1,  37 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@GosWebSocket/Collector/websocket.html.twig", "/websites/frogdata/crm-application/vendor/gos/web-socket-bundle/templates/Collector/websocket.html.twig");
    }
}
