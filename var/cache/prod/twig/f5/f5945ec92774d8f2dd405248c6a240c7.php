<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFilter/js_modules_config.html.twig */
class __TwigTemplate_0f3f1e861e0f2c371ff6a458dc478dbb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroFilter/js_modules_config.html.twig", 1)->unwrap();
        // line 2
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["orofilter/js/filters-manager" => ["filtersStateStorageKey" => "management-console-filters-state"]]], 2, $context, $this->getSourceContext());
        // line 6
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroFilter/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFilter/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FilterBundle/Resources/views/js_modules_config.html.twig");
    }
}
