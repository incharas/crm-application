<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/scroll-hints.scss */
class __TwigTemplate_b4658a74323af3725bc748b8a8eee46e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.scroll-hint-top,
.scroll-hint-bottom {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    z-index: \$scrollspy-nav-z-index;

    height: \$scroll-hint-size;

    pointer-events: none;
}

.scroll-hint-top {
    background: \$scroll-hint-top-bg;
}

.scroll-hint-bottom {
    margin-top: -\$scroll-hint-size;

    background: \$scroll-bottom-top-bg;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/scroll-hints.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/scroll-hints.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/scroll-hints.scss");
    }
}
