<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChannel/Channel/widget/info.html.twig */
class __TwigTemplate_f8c87cd2059cc84451021caef7a3a6dd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/Channel/widget/info.html.twig", 1)->unwrap();
        // line 2
        $context["channelTypes"] = $this->extensions['Oro\Bundle\ChannelBundle\Twig\ChannelExtension']->getChannelTypeMetadata();
        // line 3
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block\">
            ";
        // line 7
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["channel"] ?? null), "name", [], "any", false, false, false, 7)], 7, $context, $this->getSourceContext());
        echo "
            ";
        // line 8
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.channel_type.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((($__internal_compile_0 = ($context["channelTypes"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["channel"] ?? null), "channelType", [], "any", false, false, false, 8)] ?? null) : null))], 8, $context, $this->getSourceContext());
        echo "
            ";
        // line 9
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["channel"] ?? null), "dataSource", [], "any", false, false, false, 9)) {
            // line 10
            echo "                ";
            $context["inegrationLink"] = twig_call_macro($macros["UI"], "macro_link", [["label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["channel"] ?? null), "dataSource", [], "any", false, false, false, 11), "name", [], "any", false, false, false, 11), "title" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 12
($context["channel"] ?? null), "dataSource", [], "any", false, false, false, 12), "name", [], "any", false, false, false, 12), "path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_integration_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["channel"] ?? null), "dataSource", [], "any", false, false, false, 13), "id", [], "any", false, false, false, 13)])]], 10, $context, $this->getSourceContext());
            // line 15
            echo "                ";
            echo twig_call_macro($macros["UI"], "macro_renderHTMLProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.data_source.label"), ($context["inegrationLink"] ?? null)], 15, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 17
        echo "        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroChannel/Channel/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 17,  63 => 15,  61 => 13,  60 => 12,  59 => 11,  57 => 10,  55 => 9,  51 => 8,  47 => 7,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChannel/Channel/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ChannelBundle/Resources/views/Channel/widget/info.html.twig");
    }
}
