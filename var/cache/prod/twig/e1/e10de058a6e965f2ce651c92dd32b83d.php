<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Config/fieldUpdate.html.twig */
class __TwigTemplate_aeca5213e78e94c77edafff42426111a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => "@OroEntity/Form/fields.html.twig"], true);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 8)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 8), "N/A")) : ("N/A"))), "%fieldName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["field"] ?? null), "fieldName", [], "any", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "fieldName", [], "any", false, false, false, 9), "N/A")) : ("N/A")))]]);
        // line 12
        $context["audit_entity_class"] = twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "entity", [], "any", false, false, false, 12), "className", [], "any", false, false, false, 12), ["\\" => "_"]);
        // line 13
        $context["audit_title"] = (($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "fieldName", [], "any", true, true, false, 13)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "fieldName", [], "any", false, false, false, 13), "N/A")) : ("N/A"))) . " - ") . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 13)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 13), "N/A")) : ("N/A"))));
        // line 14
        $context["audit_path"] = "oro_entityconfig_audit_field";
        // line 15
        $context["audit_entity_id"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "id", [], "any", false, false, false, 15);
        // line 16
        $context["audit_show_change_history"] = true;
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEntityConfig/Config/fieldUpdate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 18
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/fieldUpdate.html.twig", 19)->unwrap();
        // line 20
        echo "
    ";
        // line 21
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_entityconfig_manage")) {
            // line 22
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "entity", [], "any", false, false, false, 22), "id", [], "any", false, false, false, 22)])], 22, $context, $this->getSourceContext());
            echo "
        ";
            // line 23
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_entityconfig_view", "params" => ["id" => "\$entity.id"]]], 23, $context, $this->getSourceContext());
            // line 27
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_entityextend_field_create")) {
                // line 28
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_entityextend_field_create", "params" => ["id" => "\$entity.id"]]], 28, $context, $this->getSourceContext()));
                // line 32
                echo "        ";
            }
            // line 33
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_entityconfig_field_update", "params" => ["id" => "\$id"]]], 33, $context, $this->getSourceContext()));
            // line 37
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 37, $context, $this->getSourceContext());
            echo "
    ";
        }
    }

    // line 41
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        if (( !array_key_exists("entityTitle", $context) &&  !array_key_exists("breadcrumbs", $context))) {
            // line 43
            echo "        ";
            $context["entityTitle"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "id", [], "any", false, false, false, 43)) ? (_twig_default_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 44
($context["field_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 44)), twig_capitalize_string_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["field"] ?? null), "fieldName", [], "any", false, false, false, 44)))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.info.new_field.label")));
            // line 47
            echo "
        ";
            // line 48
            $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.entity.plural_label"), "entityTitle" =>             // line 52
($context["entityTitle"] ?? null), "additional" => [0 => ["indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 55
($context["field"] ?? null), "entity", [], "any", false, false, false, 55), "id", [], "any", false, false, false, 55)]), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 56
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 56)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 56), "N/A")) : ("N/A")))]]];
            // line 60
            echo "    ";
        }
        // line 61
        echo "
    ";
        // line 62
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 65
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "    ";
        if ((array_key_exists("jsmodules", $context) && twig_length_filter($this->env, ($context["jsmodules"] ?? null)))) {
            // line 67
            echo "        <script>
            loadModules(";
            // line 68
            echo json_encode(($context["jsmodules"] ?? null));
            echo ")
        </script>
    ";
        }
        // line 71
        echo "
    ";
        // line 72
        $context["id"] = "configfield-update";
        // line 73
        echo "    ";
        $context["dataBlocks"] = $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormDataBlocks($this->env, $context, ($context["form"] ?? null));
        // line 74
        echo "    ";
        $context["data"] = ["formErrors" => ((        // line 75
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 76
($context["dataBlocks"] ?? null), "hiddenData" =>         // line 77
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest')];
        // line 79
        echo "
    ";
        // line 80
        if (((array_key_exists("non_extended_entities_classes", $context) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 81
($context["form"] ?? null), "extend", [], "any", false, true, false, 81), "relation", [], "any", true, true, false, 81)) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 82
($context["field"] ?? null), "type", [], "any", false, false, false, 82) != twig_constant("Oro\\Bundle\\EntityExtendBundle\\Extend\\RelationType::ONE_TO_MANY")))) {
            // line 84
            echo "        ";
            $context["options"] = ["nonExtendedEntitiesClassNames" =>             // line 85
($context["non_extended_entities_classes"] ?? null)];
            // line 87
            echo "        <div data-page-component-module=\"oroentityextend/js/bidirectional-only-for-extended-component\"
             data-page-component-options=\"";
            // line 88
            echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
            echo "\">
        </div>
    ";
        }
        // line 91
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Config/fieldUpdate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  185 => 91,  179 => 88,  176 => 87,  174 => 85,  172 => 84,  170 => 82,  169 => 81,  168 => 80,  165 => 79,  163 => 77,  162 => 76,  161 => 75,  159 => 74,  156 => 73,  154 => 72,  151 => 71,  145 => 68,  142 => 67,  139 => 66,  135 => 65,  129 => 62,  126 => 61,  123 => 60,  121 => 56,  120 => 55,  119 => 52,  118 => 48,  115 => 47,  113 => 44,  111 => 43,  108 => 42,  104 => 41,  96 => 37,  93 => 33,  90 => 32,  87 => 28,  84 => 27,  82 => 23,  77 => 22,  75 => 21,  72 => 20,  69 => 19,  65 => 18,  60 => 1,  58 => 16,  56 => 15,  54 => 14,  52 => 13,  50 => 12,  48 => 9,  47 => 8,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Config/fieldUpdate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Config/fieldUpdate.html.twig");
    }
}
