<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/SystemCalendarEvent/view.html.twig */
class __TwigTemplate_1445f678d86a4a057833624b9271617a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'stats' => [$this, 'block_stats'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 3)->unwrap();
        // line 4
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 4)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.title%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["entity"] ?? null), "title", [], "any", true, true, false, 7)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 7), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "%parent.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["entity"] ?? null), "systemCalendar", [], "any", false, false, false, 8), "name", [], "any", false, false, false, 8)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 12)->unwrap();
        // line 13
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 13)->unwrap();
        // line 14
        echo "
    ";
        // line 15
        if (($context["editable"] ?? null)) {
            // line 16
            echo "        ";
            // line 17
            echo "        ";
            echo twig_call_macro($macros["AC"], "macro_addContextButton", [($context["entity"] ?? null)], 17, $context, $this->getSourceContext());
            echo "
        ";
            // line 18
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_event_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 19
($context["entity"] ?? null), "id", [], "any", false, false, false, 19)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_label")]], 18, $context, $this->getSourceContext());
            // line 21
            echo "
    ";
        }
        // line 23
        echo "    ";
        if (($context["removable"] ?? null)) {
            // line 24
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 25
($context["entity"] ?? null), "id", [], "any", false, false, false, 25)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["entity"] ?? null), "systemCalendar", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26)]), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 28
($context["entity"] ?? null), "id", [], "any", false, false, false, 28), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_label")]], 24, $context, $this->getSourceContext());
            // line 30
            echo "
    ";
        }
    }

    // line 34
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 35)->unwrap();
        // line 36
        echo "
    ";
        // line 38
        echo "    <li class=\"context-data activity-context-activity-block\">
        ";
        // line 39
        echo twig_call_macro($macros["AC"], "macro_activity_contexts", [($context["entity"] ?? null)], 39, $context, $this->getSourceContext());
        echo "
    </li>
";
    }

    // line 43
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 45
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 48
($context["entity"] ?? null), "title", [], "any", true, true, false, 48)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 48), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "additional" => [0 => ["indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 50
($context["entity"] ?? null), "systemCalendar", [], "any", false, false, false, 50), "id", [], "any", false, false, false, 50)]), "indexLabel" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 51
($context["entity"] ?? null), "systemCalendar", [], "any", false, false, false, 51), "name", [], "any", false, false, false, 51)]]];
        // line 54
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 57
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/SystemCalendarEvent/view.html.twig", 58)->unwrap();
        // line 60
        ob_start(function () { return ''; });
        // line 61
        echo "<div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 63
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.title.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 63)], 63, $context, $this->getSourceContext());
        echo "
                ";
        // line 64
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 64)], 64, $context, $this->getSourceContext());
        echo "
                ";
        // line 65
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.start.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "start", [], "any", false, false, false, 65))], 65, $context, $this->getSourceContext());
        echo "
                ";
        // line 66
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.end.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "end", [], "any", false, false, false, 66))], 66, $context, $this->getSourceContext());
        echo "
                ";
        // line 67
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.all_day.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "allDay", [], "any", false, false, false, 67)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No")))], 67, $context, $this->getSourceContext());
        echo "
            </div>
            <div class=\"responsive-block\">
                ";
        // line 70
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 70, $context, $this->getSourceContext());
        echo "
            </div>
        </div>";
        $context["calendarEventInformation"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 75
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 80
($context["calendarEventInformation"] ?? null)]]]]];
        // line 84
        echo "
    ";
        // line 85
        $context["id"] = "calendarEventView";
        // line 86
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 87
        echo "
    ";
        // line 88
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/SystemCalendarEvent/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 88,  194 => 87,  191 => 86,  189 => 85,  186 => 84,  184 => 80,  183 => 75,  177 => 70,  171 => 67,  167 => 66,  163 => 65,  159 => 64,  155 => 63,  151 => 61,  149 => 60,  146 => 58,  142 => 57,  135 => 54,  133 => 51,  132 => 50,  131 => 48,  130 => 45,  128 => 44,  124 => 43,  117 => 39,  114 => 38,  111 => 36,  108 => 35,  104 => 34,  98 => 30,  96 => 28,  95 => 26,  94 => 25,  92 => 24,  89 => 23,  85 => 21,  83 => 19,  82 => 18,  77 => 17,  75 => 16,  73 => 15,  70 => 14,  67 => 13,  64 => 12,  60 => 11,  55 => 1,  53 => 8,  52 => 7,  49 => 4,  47 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/SystemCalendarEvent/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/SystemCalendarEvent/view.html.twig");
    }
}
