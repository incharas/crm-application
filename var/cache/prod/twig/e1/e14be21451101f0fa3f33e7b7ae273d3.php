<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Call/js/activityItemTemplate.html.twig */
class __TwigTemplate_af95fbd44f3825949c3a605629e83eab extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'activityDetails' => [$this, 'block_activityDetails'],
            'activityActions' => [$this, 'block_activityActions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCall/Call/js/activityItemTemplate.html.twig", 2)->unwrap();
        // line 4
        $context["entityClass"] = "Oro\\Bundle\\CallBundle\\Entity\\Call";
        // line 5
        $context["entityName"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["entityClass"] ?? null), "label"));
        // line 1
        $this->parent = $this->loadTemplate("@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", "@OroCall/Call/js/activityItemTemplate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_activityDetails($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, ($context["entityName"] ?? null), "html", null, true);
        echo "
    <% var template = (verb == 'create')
        ? ";
        // line 10
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.created_by"));
        echo "
        : ";
        // line 11
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.changed_by"));
        echo ";
    %>
    <%= _.template(template, { interpolate: /\\{\\{(.+?)\\}\\}/g })({
        user: owner_url ? '<a class=\"user\" href=\"' + owner_url + '\">' +  _.escape(owner) + '</a>' :  '<span class=\"user\">' + _.escape(owner) + '</span>',
        date: '<i class=\"date\">' + createdAt + '</i>',
        editor: editor_url ? '<a class=\"user\" href=\"' + editor_url + '\">' +  _.escape(editor) + '</a>' : _.escape(editor),
        editor_date: '<i class=\"date\">' + updatedAt + '</i>'
    }) %>
";
    }

    // line 21
    public function block_activityActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCall/Call/js/activityItemTemplate.html.twig", 22)->unwrap();
        // line 23
        echo "
    ";
        // line 24
        ob_start(function () { return ''; });
        // line 25
        echo "        <% if (editable) { %>
            ";
        // line 26
        echo twig_call_macro($macros["AC"], "macro_activity_context_link", [], 26, $context, $this->getSourceContext());
        echo "
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 29
        echo "    ";
        $context["actions"] = [0 => ($context["action"] ?? null)];
        // line 30
        echo "
    ";
        // line 31
        ob_start(function () { return ''; });
        // line 32
        echo "        <a href=\"<%- routing.generate('oro_call_view', {'id': relatedActivityId}) %>\"
           class=\"dropdown-item\"
           title=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.view_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\"><span
                class=\"fa-eye hide-text\" aria-hidden=\"true\">";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.view_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.view_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 39
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 40
        echo "
    ";
        // line 41
        ob_start(function () { return ''; });
        // line 42
        echo "        <% if (editable) { %>
        <a href=\"#\" class=\"dropdown-item action item-edit-button\"
           title=\"";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.update_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\"
           data-action-extra-options=\"";
        // line 45
        echo twig_escape_filter($this->env, json_encode(["dialogOptions" => ["width" => 1000]]), "html", null, true);
        echo "\">
            <span class=\"fa-pencil-square-o hide-text\">";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.update_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.update_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 51
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 52
        echo "
    ";
        // line 53
        ob_start(function () { return ''; });
        // line 54
        echo "        <% if (removable) { %>
        <a href=\"#\" class=\"dropdown-item action item-remove-button\"
           title=\"";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.delete_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\">
            <span class=\"fa-trash-o hide-text\" aria-hidden=\"true\">";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.delete_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.call.delete_call", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 62
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 63
        echo "
    ";
        // line 64
        $this->displayParentBlock("activityActions", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCall/Call/js/activityItemTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 64,  189 => 63,  186 => 62,  179 => 58,  175 => 57,  171 => 56,  167 => 54,  165 => 53,  162 => 52,  159 => 51,  152 => 47,  148 => 46,  144 => 45,  140 => 44,  136 => 42,  134 => 41,  131 => 40,  128 => 39,  122 => 36,  118 => 35,  114 => 34,  110 => 32,  108 => 31,  105 => 30,  102 => 29,  96 => 26,  93 => 25,  91 => 24,  88 => 23,  85 => 22,  81 => 21,  68 => 11,  64 => 10,  58 => 8,  54 => 7,  49 => 1,  47 => 5,  45 => 4,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Call/js/activityItemTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Call/js/activityItemTemplate.html.twig");
    }
}
