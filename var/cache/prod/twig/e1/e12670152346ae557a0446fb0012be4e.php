<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/page/breadcrumb-view.js */
class __TwigTemplate_95037fba96a9b068a7f7e3265235cf09 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const PageRegionView = require('./../base/page-region-view');

    const PageBreadcrumbView = PageRegionView.extend({
        listen: {
            'mainMenuUpdated mediator': 'onMenuUpdate'
        },
        pageItems: ['breadcrumb'],

        template: function(data) {
            return data.breadcrumb;
        },

        breadcrumbsTemplate: _.template('<ul class=\"breadcrumb\">' +
        '<% for (var i = 0; i < breadcrumbs.length; i++) { %>' +
        '<li class=\"breadcrumb-item<%= (i + 1 === breadcrumbs.length) ? \" active\": \"\" %>\"><%- breadcrumbs[i] %></li>' +
        '<% } %>' +
        '</ul>'),

        data: null,

        /**
         * @inheritdoc
         */
        constructor: function PageBreadcrumbView(options) {
            PageBreadcrumbView.__super__.constructor.call(this, options);
        },

        /**
         * Handles menu update event
         *  - prepares data for breadcrumbs rendering
         *  - renders view
         *  - dispose cached data
         *
         * @param {Object} menuView
         */
        onMenuUpdate: function(menuView) {
            const breadcrumbs = menuView.getActiveItems();
            if (breadcrumbs.length) {
                this.data = {
                    breadcrumb: this.breadcrumbsTemplate({breadcrumbs: breadcrumbs})
                };
                this.render();
                this.data = null;
            }
        },

        /**
         * Gets cached page data
         *
         * @returns {Object}
         * @override
         */
        getTemplateData: function() {
            return this.data;
        }
    });

    return PageBreadcrumbView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/page/breadcrumb-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/page/breadcrumb-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/page/breadcrumb-view.js");
    }
}
