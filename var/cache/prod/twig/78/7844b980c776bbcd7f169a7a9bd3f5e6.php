<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/actions.html.twig */
class __TwigTemplate_ea135e9271cd8a75194713f864eb23e5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 184
        echo "
";
    }

    // line 11
    public function macro_sendEmailLink($__email__ = null, $__entity__ = null, $__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "email" => $__email__,
            "entity" => $__entity__,
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 12
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
                // line 13
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/actions.html.twig", 13)->unwrap();
                // line 14
                echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_email_create", ["to" =>                 // line 17
($context["email"] ?? null), "entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(                // line 18
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 19
($context["entity"] ?? null), "id", [], "any", false, false, false, 19)]), "class" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 21
($context["parameters"] ?? null), "cssClass", [], "any", true, true, false, 21)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "cssClass", [], "any", false, false, false, 21)) : ("")), "dir" => "ltr", "iCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 23
($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 23)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 23)) : ("")), "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 24
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 24)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 24) . " no-hash")) : ("no-hash")), "label" =>                 // line 25
($context["email"] ?? null), "widget" => ["type" => "dialog", "multiple" => true, "reload-grid-name" => "activity-email-grid", "options" => ["alias" => "email-dialog", "method" => "POST", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 14, $context, $this->getSourceContext());
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 48
    public function macro_replyAllButton($__emailEntity__ = null, $__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailEntity" => $__emailEntity__,
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 49
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
                // line 50
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/actions.html.twig", 50)->unwrap();
                // line 51
                $context["routeParameters"] = ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 52
($context["emailEntity"] ?? null), "id", [], "any", false, false, false, 52)];
                // line 54
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "routeParameters", [], "any", true, true, false, 54)) {
                    // line 55
                    $context["routeParameters"] = twig_array_merge(($context["routeParameters"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "routeParameters", [], "any", false, false, false, 55));
                }
                // line 57
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_email_reply_all",                 // line 58
($context["routeParameters"] ?? null)), "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 59
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 59)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 59) . " no-hash")) : ("no-hash")), "iCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 60
($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 60)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 60) . " fa-reply-all")) : ("fa-reply-all")), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply_all"), "widget" => ["type" => "dialog", "multiple" => false, "refresh-widget-alias" => "activity-list-widget", "reload-widget-alias" => "thread-view", "options" => ["alias" => "reply-all-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply_all"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 57, $context, $this->getSourceContext());
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 84
    public function macro_replyButton($__emailEntity__ = null, $__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailEntity" => $__emailEntity__,
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 85
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
                // line 86
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/actions.html.twig", 86)->unwrap();
                // line 87
                $context["routeParameters"] = ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 88
($context["emailEntity"] ?? null), "id", [], "any", false, false, false, 88)];
                // line 90
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "routeParameters", [], "any", true, true, false, 90)) {
                    // line 91
                    $context["routeParameters"] = twig_array_merge(($context["routeParameters"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "routeParameters", [], "any", false, false, false, 91));
                }
                // line 93
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_email_reply",                 // line 94
($context["routeParameters"] ?? null)), "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 95
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 95)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 95) . " no-hash")) : ("no-hash")), "iCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 96
($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 96)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 96) . " fa-reply")) : ("fa-reply")), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "widget" => ["type" => "dialog", "multiple" => false, "refresh-widget-alias" => "activity-list-widget", "reload-widget-alias" => "thread-view", "options" => ["alias" => "reply-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 93, $context, $this->getSourceContext());
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 120
    public function macro_forwardButton($__emailEntity__ = null, $__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailEntity" => $__emailEntity__,
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 121
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
                // line 122
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/actions.html.twig", 122)->unwrap();
                // line 123
                $context["routeParameters"] = ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 124
($context["emailEntity"] ?? null), "id", [], "any", false, false, false, 124)];
                // line 126
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "routeParameters", [], "any", true, true, false, 126)) {
                    // line 127
                    $context["routeParameters"] = twig_array_merge(($context["routeParameters"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "routeParameters", [], "any", false, false, false, 127));
                }
                // line 129
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_email_forward",                 // line 130
($context["routeParameters"] ?? null)), "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 131
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 131)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 131) . " no-hash")) : ("no-hash")), "iCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 132
($context["parameters"] ?? null), "iCss", [], "any", true, true, false, 132)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "iCss", [], "any", false, false, false, 132) . " fa-mail-forward")) : ("fa-mail-forward")), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "widget" => ["type" => "dialog", "multiple" => false, "refresh-widget-alias" => "activity-list-widget", "reload-widget-alias" => "thread-view", "options" => ["alias" => "foward-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 129, $context, $this->getSourceContext());
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 156
    public function macro_createEmailButton($__parameters__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 157
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
                // line 158
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/actions.html.twig", 158)->unwrap();
                // line 159
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_email_create",                 // line 160
($context["parameters"] ?? null)), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.menu.compose"), "iCss" => "fa-envelope", "widget" => ["type" => "dialog", "multiple" => true, "reload-grid-name" => "user-email-grid", "options" => ["alias" => "email-dialog", "method" => "POST", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 159, $context, $this->getSourceContext());
                // line 181
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 185
    public function macro_addMarkUnreadButton($__emailEntity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailEntity" => $__emailEntity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 186
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_user_edit", ($context["emailEntity"] ?? null))) {
                // line 187
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/actions.html.twig", 187)->unwrap();
                // line 188
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_mark_seen", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 191
($context["emailEntity"] ?? null), "id", [], "any", false, false, false, 191), "status" => 0]), "dataAttributes" => ["page-component-module" => "oroemail/js/app/components/mark-unread-button", "page-component-options" => json_encode(["redirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_user_emails")])], "aCss" => "no-hash", "iCss" => "fa-minus hide-text", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 202
($context["emailEntity"] ?? null), "id", [], "any", false, false, false, 202), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.menu.mark_unread.label")]], 188, $context, $this->getSourceContext());
                // line 204
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/actions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 204,  253 => 202,  252 => 191,  250 => 188,  247 => 187,  245 => 186,  232 => 185,  221 => 181,  219 => 160,  217 => 159,  214 => 158,  212 => 157,  199 => 156,  189 => 132,  188 => 131,  187 => 130,  186 => 129,  183 => 127,  181 => 126,  179 => 124,  178 => 123,  175 => 122,  173 => 121,  159 => 120,  149 => 96,  148 => 95,  147 => 94,  146 => 93,  143 => 91,  141 => 90,  139 => 88,  138 => 87,  135 => 86,  133 => 85,  119 => 84,  109 => 60,  108 => 59,  107 => 58,  106 => 57,  103 => 55,  101 => 54,  99 => 52,  98 => 51,  95 => 50,  93 => 49,  79 => 48,  69 => 25,  68 => 24,  67 => 23,  66 => 21,  65 => 19,  64 => 18,  63 => 17,  62 => 14,  59 => 13,  57 => 12,  42 => 11,  37 => 184,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/actions.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/actions.html.twig");
    }
}
