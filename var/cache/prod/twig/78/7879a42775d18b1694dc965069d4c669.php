<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Widget/widget/button.html.twig */
class __TwigTemplate_6d5cea1ab1408c806fbd65addb671216 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["workflowMacros"] = $this->macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/Widget/widget/button.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["workflow"] ?? null), "transitionsData", [], "any", false, false, false, 3)) || ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["workflow"] ?? null), "resetAllowed", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["workflow"] ?? null), "resetAllowed", [], "any", false, false, false, 3), false)) : (false)))) {
            // line 4
            echo "    ";
            $context["blockId"] = ("entity-transitions-container-" . twig_random($this->env));
            // line 5
            echo "    <div class=\"transitions-btn-group\" id=\"";
            echo twig_escape_filter($this->env, ($context["blockId"] ?? null), "html", null, true);
            echo "\">
        ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["workflow"] ?? null), "transitionsData", [], "any", false, false, false, 6));
            foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
                // line 7
                echo "            ";
                // line 8
                echo "            ";
                $context["transitionLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, false, false, 8), "label", [], "any", false, false, false, 8), [], "workflows");
                // line 9
                echo "
            ";
                // line 10
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, true, false, 10), "frontendOptions", [], "any", false, true, false, 10), "message", [], "any", true, true, false, 10) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, true, false, 10), "frontendOptions", [], "any", false, true, false, 10), "message", [], "any", false, true, false, 10), "content", [], "any", true, true, false, 10))) {
                    // line 11
                    echo "                ";
                    $context["frontendMessage"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, false, false, 11), "frontendOptions", [], "any", false, false, false, 11), "message", [], "any", false, false, false, 11);
                    // line 12
                    echo "
                ";
                    // line 13
                    $context["transitionMessage"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "content", [], "any", false, false, false, 13), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "message_parameters", [], "any", true, true, false, 13)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "message_parameters", [], "any", false, false, false, 13), [])) : ([])), "workflows");
                    // line 14
                    echo "                ";
                    if ((($context["transitionMessage"] ?? null) && (($context["transitionMessage"] ?? null) != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "content", [], "any", false, false, false, 14)))) {
                        // line 15
                        echo "                    ";
                        // line 16
                        echo "                    ";
                        $context["message"] = ["content" => twig_nl2br(twig_escape_filter($this->env,                         // line 17
($context["transitionMessage"] ?? null), "html", null, true)), "title" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 18
($context["frontendMessage"] ?? null), "title", [], "any", true, true, false, 18)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "title", [], "any", false, false, false, 18), ($context["transitionLabel"] ?? null))) : (($context["transitionLabel"] ?? null)))];
                        // line 20
                        echo "                ";
                    }
                    // line 21
                    echo "            ";
                }
                // line 22
                echo "
            ";
                // line 23
                $context["transitionData"] = ["enabled" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 24
$context["data"], "isAllowed", [], "any", false, false, false, 24), "transition-label" =>                 // line 25
($context["transitionLabel"] ?? null), "message" => json_encode(((                // line 26
array_key_exists("message", $context)) ? (_twig_default_filter(($context["message"] ?? null), [])) : ([]))), "confirmation" => json_encode(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 27
$context["data"], "transition", [], "any", false, true, false, 27), "frontendOptions", [], "any", false, true, false, 27), "confirmation", [], "any", true, true, false, 27)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, true, false, 27), "frontendOptions", [], "any", false, true, false, 27), "confirmation", [], "any", false, false, false, 27), [])) : ([]))), "transition-condition-messages" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 28
$context["data"], "errors", [], "any", false, false, false, 28)];
                // line 30
                echo "
            ";
                // line 31
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, false, false, 31), "displayType", [], "any", false, false, false, 31) == "dialog")) {
                    // line 32
                    echo "                ";
                    if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "workflowItem", [], "any", true, true, false, 32)) {
                        // line 33
                        echo "                    ";
                        $context["data"] = twig_array_merge($context["data"], ["workflowItem" => null]);
                        // line 34
                        echo "                    ";
                        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, false, false, 34), "hasForm", [], "method", false, false, false, 34)) {
                            // line 35
                            echo "                        ";
                            $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["dialog-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_widget_start_transition_form", ["workflowName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 39
$context["data"], "workflow", [], "any", false, false, false, 39), "name", [], "any", false, false, false, 39), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 40
$context["data"], "transition", [], "any", false, false, false, 40), "name", [], "any", false, false, false, 40), "entityId" =>                             // line 41
($context["entity_id"] ?? null)]), "jsDialogWidget" => twig_constant("Oro\\Bundle\\WorkflowBundle\\Button\\AbstractTransitionButton::TRANSITION_JS_DIALOG_WIDGET")]);
                            // line 46
                            echo "                    ";
                        }
                        // line 47
                        echo "
                    ";
                        // line 49
                        echo "                    ";
                        $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_start", ["workflowName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 52
$context["data"], "workflow", [], "any", false, false, false, 52), "name", [], "any", false, false, false, 52), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 53
$context["data"], "transition", [], "any", false, false, false, 53), "name", [], "any", false, false, false, 53), "entityId" =>                         // line 54
($context["entity_id"] ?? null)])]);
                        // line 57
                        echo "                ";
                    }
                    // line 58
                    echo "            ";
                } else {
                    // line 59
                    echo "                ";
                    if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "workflowItem", [], "any", true, true, false, 59)) {
                        // line 60
                        echo "                    ";
                        $context["data"] = twig_array_merge($context["data"], ["workflowItem" => null]);
                        // line 61
                        echo "                    ";
                        $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_start_transition_form", ["workflowName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 64
$context["data"], "workflow", [], "any", false, false, false, 64), "name", [], "any", false, false, false, 64), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 65
$context["data"], "transition", [], "any", false, false, false, 65), "name", [], "any", false, false, false, 65), "entityId" =>                         // line 66
($context["entity_id"] ?? null), "originalUrl" =>                         // line 67
($context["originalUrl"] ?? null)])]);
                        // line 70
                        echo "                ";
                    } else {
                        // line 71
                        echo "                    ";
                        $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_transition_form", ["transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 74
$context["data"], "transition", [], "any", false, false, false, 74), "name", [], "any", false, false, false, 74), "workflowItemId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 75
$context["data"], "workflowItem", [], "any", false, false, false, 75), "id", [], "any", false, false, false, 75), "originalUrl" =>                         // line 76
($context["originalUrl"] ?? null)])]);
                        // line 79
                        echo "                ";
                    }
                    // line 80
                    echo "            ";
                }
                // line 81
                echo "
            ";
                // line 82
                echo twig_call_macro($macros["workflowMacros"], "macro_renderTransitionButton", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 83
$context["data"], "workflow", [], "any", false, false, false, 83), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 84
$context["data"], "transition", [], "any", false, false, false, 84), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 85
$context["data"], "workflowItem", [], "any", false, false, false, 85),                 // line 86
($context["transitionData"] ?? null)], 82, $context, $this->getSourceContext());
                // line 87
                echo "
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 89
            echo "    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/widget/button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 89,  175 => 87,  173 => 86,  172 => 85,  171 => 84,  170 => 83,  169 => 82,  166 => 81,  163 => 80,  160 => 79,  158 => 76,  157 => 75,  156 => 74,  154 => 71,  151 => 70,  149 => 67,  148 => 66,  147 => 65,  146 => 64,  144 => 61,  141 => 60,  138 => 59,  135 => 58,  132 => 57,  130 => 54,  129 => 53,  128 => 52,  126 => 49,  123 => 47,  120 => 46,  118 => 41,  117 => 40,  116 => 39,  114 => 35,  111 => 34,  108 => 33,  105 => 32,  103 => 31,  100 => 30,  98 => 28,  97 => 27,  96 => 26,  95 => 25,  94 => 24,  93 => 23,  90 => 22,  87 => 21,  84 => 20,  82 => 18,  81 => 17,  79 => 16,  77 => 15,  74 => 14,  72 => 13,  69 => 12,  66 => 11,  64 => 10,  61 => 9,  58 => 8,  56 => 7,  52 => 6,  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/widget/button.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/widget/button.html.twig");
    }
}
