<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/Call/updateActions.html.twig */
class __TwigTemplate_df12a77bec2a313e58662196f779fbbe extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["HangoutsCall"] = $this->macros["HangoutsCall"] = $this->loadTemplate("@OroHangoutsCall/macros.html.twig", "@OroHangoutsCall/Call/updateActions.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if ( !array_key_exists("hangoutOptions", $context)) {
            // line 4
            echo "    ";
            $context["hangoutOptions"] = [];
        }
        // line 6
        if ((array_key_exists("entity", $context) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phoneNumber", [], "any", false, false, false, 6)))) {
            // line 7
            echo "    ";
            // line 8
            echo "    ";
            if (twig_matches("{^.+@.+..+\$}", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phoneNumber", [], "any", false, false, false, 8))) {
                // line 9
                echo "        ";
                $context["hangoutOptions"] = twig_array_merge(($context["hangoutOptions"] ?? null), ["invites" => [0 => ["id" => twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 11
($context["entity"] ?? null), "phoneNumber", [], "any", false, false, false, 11), "html"), "invite_type" => "EMAIL"]]]);
                // line 15
                echo "    ";
            } else {
                // line 16
                echo "        ";
                $context["hangoutOptions"] = twig_array_merge(($context["hangoutOptions"] ?? null), ["invites" => [0 => ["id" => twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 18
($context["entity"] ?? null), "phoneNumber", [], "any", false, false, false, 18), "html"), "invite_type" => "PHONE"]]]);
                // line 22
                echo "    ";
            }
        }
        // line 24
        echo "
";
        // line 25
        echo twig_call_macro($macros["HangoutsCall"], "macro_renderStartButton", [["class" => "action btn-group", "componentModule" => "orohangoutscall/js/app/components/log-call-component", "componentName" => "log-hangout-call-component", "dataAttributes" => ["action-name" => "hangout-call"], "hangoutOptions" =>         // line 32
($context["hangoutOptions"] ?? null)]], 25, $context, $this->getSourceContext());
        // line 33
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/Call/updateActions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 33,  74 => 32,  73 => 25,  70 => 24,  66 => 22,  64 => 18,  62 => 16,  59 => 15,  57 => 11,  55 => 9,  52 => 8,  50 => 7,  48 => 6,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/Call/updateActions.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/Call/updateActions.html.twig");
    }
}
