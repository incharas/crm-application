<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroPlatform/Collector/collectors_toggle.html.twig */
class __TwigTemplate_c756f5c346efa8dce6b37ced27dcf5a1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script>
    /* eslint no-var: off */
    (function () {
        const cookieExpires = new Date(new Date().getTime()+1000*60*60*24*365).toGMTString();
        const trackContainerCheckbox = document.getElementById('sf-toolbar--track-container');
        trackContainerCheckbox.addEventListener('click', function (event) {
            let isActive = event.target.checked === true;
            document.cookie = 'sf_toolbar_track_container=' + isActive + ';expires=' + cookieExpires + ';path=/';
        });

        const checkboxes = document.getElementsByClassName('sf-toolbar--collector-checkbox');
        document.getElementById('sf-toolbar--all-collectors-enable').addEventListener('click', function (event) {
            let collectors = [];
            for (let i = 0; i < checkboxes.length; i++) {
                collectors.push(checkboxes[i].id);
                checkboxes[i].checked = true;
            }
            saveCollectorsToCookie(collectors);
        });
        document.getElementById('sf-toolbar--all-collectors-disable').addEventListener('click', function (event) {
            for (let i = 0; i < checkboxes.length; i++) {
                checkboxes[i].checked = false;
            }
            saveCollectorsToCookie([]);
        });

        for (let i = 0; i < checkboxes.length; i++) {
            let checkbox = checkboxes[i];
            checkbox.addEventListener('change', function (event) {
                let isActive = event.target.checked === true;
                let collectorsCookie = document.cookie.replace(/(?:(?:^|.*;\\s*)sf_toolbar_enabled_collectors\\s*\\=\\s*([^;]*).*\$)|^.*\$/, \"\$1\");
                let collectors = [];
                if (collectorsCookie) {
                    collectors = collectorsCookie.split('~');
                }
                let collector = event.target.id;
                if (isActive && collectors.indexOf(collector) === -1) {
                    collectors.push(collector);
                } else {
                    const index = collectors.indexOf(collector);
                    if (index > -1) {
                        collectors.splice(index, 1);
                    }
                }
                saveCollectorsToCookie(collectors);
            });
        }

        function saveCollectorsToCookie(collectors) {
            collectorsCookie = collectors.join('~');
            document.cookie = 'sf_toolbar_enabled_collectors=' + collectorsCookie + ';expires=' + cookieExpires + ';path=/';
        }
    })();
</script>
<style>
    .sf-toolbar-toggle-data-collectors b {
        vertical-align: top;
    }

    .sf-toolbar-toggle-data-collectors span {
        padding-right: 10px;
    }

    .sf-toolbar input[type=\"checkbox\"] {
        --checkbox-size: 13px;
    }
</style>
<div class=\"sf-toolbar-info-group sf-toolbar-toggle-data-collectors\">
    <div class=\"sf-toolbar-info-piece\">
        <b>Profiler <br/> Toolbars</b>
        ";
        // line 71
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_array_batch(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "collectors", [], "any", false, false, false, 71), (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "collectors", [], "any", false, false, false, 71)) / 2)));
        foreach ($context['_seq'] as $context["_key"] => $context["collectorNames"]) {
            // line 72
            echo "            <span>
                ";
            // line 73
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($context["collectorNames"]);
            foreach ($context['_seq'] as $context["_key"] => $context["collectorName"]) {
                // line 74
                echo "                    <div>
                    <input class=\"sf-toolbar--collector-checkbox\" id=\"";
                // line 75
                echo twig_escape_filter($this->env, $context["collectorName"], "html", null, true);
                echo "\"
                           type=\"checkbox\"
                           ";
                // line 77
                if (twig_in_filter($context["collectorName"], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "enabledCollectors", [], "any", false, false, false, 77))) {
                    echo "checked";
                }
                echo ">
                        <label for=\"";
                // line 78
                echo twig_escape_filter($this->env, $context["collectorName"], "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, $context["collectorName"], "html", null, true);
                echo "</label>
                    </div>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['collectorName'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 81
            echo "                </span>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['collectorNames'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 83
        echo "        <br><br>
        <span>
                <a href=\"#\" id=\"sf-toolbar--all-collectors-enable\">enable all</a>
                |
                <a href=\"#\" id=\"sf-toolbar--all-collectors-disable\">disable all</a>
            </span>
        <br><br>
        <span>
            <input id=\"sf-toolbar--track-container\" type=\"checkbox\"
                   ";
        // line 92
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["collector"] ?? null), "trackContainerChanges", [], "any", false, false, false, 92)) {
            echo "checked";
        }
        echo ">
            <label for=\"sf-toolbar--track-container\">track service container changes</label>
            </span>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroPlatform/Collector/collectors_toggle.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 92,  152 => 83,  145 => 81,  134 => 78,  128 => 77,  123 => 75,  120 => 74,  116 => 73,  113 => 72,  109 => 71,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroPlatform/Collector/collectors_toggle.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/PlatformBundle/Resources/views/Collector/collectors_toggle.html.twig");
    }
}
