<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/menuUpdate/pageHeader.html.twig */
class __TwigTemplate_85ad0808b135d4aa9f5d14232a61c38b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'breadcrumbMessage' => [$this, 'block_breadcrumbMessage'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"page-title__path\">
    <div class=\"top-row\">
        ";
        // line 3
        $this->displayBlock('breadcrumbs', $context, $blocks);
        // line 31
        echo "    </div>
</div>
";
    }

    // line 3
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "            <div class=\"page-title__entity-title-wrapper\">
                ";
        // line 5
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", true, true, false, 5)) {
            // line 6
            echo "                    <div class=\"sub-title\">";
            // line 7
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexPath", [], "any", true, true, false, 7)) {
                // line 8
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->addUrlQuery(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexPath", [], "any", false, false, false, 8)), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", false, false, false, 8), "html", null, true);
                echo "</a>";
            } else {
                // line 10
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", false, false, false, 10), "html", null, true);
            }
            // line 12
            echo "</div>
                    <span class=\"separator\">/</span>
                ";
        }
        // line 15
        echo "                ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "additional", [], "any", true, true, false, 15)) {
            // line 16
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "additional", [], "any", false, false, false, 16));
            foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                // line 17
                echo "                        <div class=\"sub-title\">";
                // line 18
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexPath", [], "any", true, true, false, 18)) {
                    // line 19
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexPath", [], "any", false, false, false, 19), "html", null, true);
                    echo "\">";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexLabel", [], "any", false, false, false, 19), "html", null, true);
                    echo "</a>";
                } else {
                    // line 21
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexLabel", [], "any", false, false, false, 21), "html", null, true);
                }
                // line 23
                echo "</div>
                        <span class=\"separator\">/</span>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "                ";
        }
        // line 27
        echo "                <h1 class=\"page-title__entity-title\">";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entityTitle", [], "any", false, false, false, 27), "html", null, true);
        echo "</h1>
            </div>
            ";
        // line 29
        $this->displayBlock('breadcrumbMessage', $context, $blocks);
        // line 30
        echo "        ";
    }

    // line 29
    public function block_breadcrumbMessage($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "@OroNavigation/menuUpdate/pageHeader.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  124 => 29,  120 => 30,  118 => 29,  112 => 27,  109 => 26,  101 => 23,  98 => 21,  91 => 19,  89 => 18,  87 => 17,  82 => 16,  79 => 15,  74 => 12,  71 => 10,  64 => 8,  62 => 7,  60 => 6,  58 => 5,  55 => 4,  51 => 3,  45 => 31,  43 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/menuUpdate/pageHeader.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/menuUpdate/pageHeader.html.twig");
    }
}
