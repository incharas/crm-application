<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSegment/Form/fields.html.twig */
class __TwigTemplate_26c06048b351e4a5e008fb77be9e4e0a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_segment_filter_builder_row' => [$this, 'block_oro_segment_filter_builder_row'],
            'oro_segment_filter_builder_widget' => [$this, 'block_oro_segment_filter_builder_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_segment_filter_builder_row', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('oro_segment_filter_builder_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_segment_filter_builder_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", true, true, false, 2) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 2), "rendered", [], "any", false, false, false, 2))) {
            // line 3
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 3), 'row', ["attr" => ["class" => "control-group-oro_segment_filter_builder_segment_name"]]);
            // line 5
            echo "
    ";
        }
        // line 7
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
    </div>
";
    }

    // line 12
    public function block_oro_segment_filter_builder_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $macros["segmentQD"] = $this->loadTemplate("@OroSegment/macros.html.twig", "@OroSegment/Form/fields.html.twig", 13)->unwrap();
        // line 14
        echo "    ";
        $macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroSegment/Form/fields.html.twig", 14)->unwrap();
        // line 15
        echo "
    ";
        // line 16
        $context["segment"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 16), "value", [], "any", false, false, false, 16);
        // line 17
        echo "    ";
        $context["id"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 17), "id", [], "any", false, false, false, 17);
        // line 18
        echo "    ";
        $context["coditionBuilderId"] = (($context["id"] ?? null) . "-condition-builder");
        // line 19
        echo "    ";
        $context["entityChoiceId"] = (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 19), "vars", [], "any", false, false, false, 19), "attr", [], "any", false, false, false, 19)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["data-ftid"] ?? null) : null);
        // line 20
        echo "    ";
        $context["definitionId"] = (($__internal_compile_1 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "definition", [], "any", false, false, false, 20), "vars", [], "any", false, false, false, 20), "attr", [], "any", false, false, false, 20)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["data-ftid"] ?? null) : null);
        // line 21
        echo "    ";
        $context["metadata"] = $this->extensions['Oro\Bundle\DashboardBundle\Twig\DashboardExtension']->getQueryFilterMetadata();
        // line 22
        echo "    ";
        $context["column_chain_template_id"] = ("column-chain-template-" . ($context["id"] ?? null));
        // line 23
        echo "
    ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 24), 'widget');
        echo "
    ";
        // line 25
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "definition", [], "any", false, false, false, 25), 'widget');
        echo "
    ";
        // line 26
        echo twig_call_macro($macros["QD"], "macro_query_designer_column_chain_template", [($context["column_chain_template_id"] ?? null)], 26, $context, $this->getSourceContext());
        echo "
    ";
        // line 27
        if ( !$this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 28
            echo "    ";
            echo twig_call_macro($macros["segmentQD"], "macro_query_designer_condition_builder", [["id" =>             // line 29
($context["coditionBuilderId"] ?? null), "currentSegmentId" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 30
($context["segment"] ?? null), "id", [], "any", true, true, false, 30)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["segment"] ?? null), "id", [], "any", false, false, false, 30), null)) : (null)), "page_limit" => twig_constant("\\Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager::PER_PAGE"), "conditionBuilderOptions" =>             // line 32
($context["condition_builder_options"] ?? null), "metadata" =>             // line 33
($context["metadata"] ?? null), "column_chain_template_selector" => ("#" .             // line 34
($context["column_chain_template_id"] ?? null)), "fieldConditionOptions" => ((            // line 35
array_key_exists("field_condition_options", $context)) ? (_twig_default_filter(($context["field_condition_options"] ?? null), [])) : ([]))]], 28, $context, $this->getSourceContext());
            // line 36
            echo "
    ";
        }
        // line 38
        echo "
    ";
        // line 39
        $context["widgetOptions"] = ["valueSource" => (("[data-ftid=" .         // line 40
($context["definitionId"] ?? null)) . "]"), "entityChoice" => (("[data-ftid=" .         // line 41
($context["entityChoiceId"] ?? null)) . "]"), "entityChangeConfirmMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.condition_builder.confirm_message"), "metadata" =>         // line 43
($context["metadata"] ?? null), "initEntityChangeEvents" => false, "select2FieldChoiceTemplate" => ("#" .         // line 45
($context["column_chain_template_id"] ?? null))];
        // line 47
        echo "
    ";
        // line 48
        $context["widgetOptions"] = $this->extensions['Oro\Bundle\SegmentBundle\Twig\SegmentExtension']->updateSegmentWidgetOptions(($context["widgetOptions"] ?? null), ($context["id"] ?? null));
        // line 49
        echo "    <div data-page-component-module=\"orosegment/js/app/components/segment-component\"
         data-page-component-options=\"";
        // line 50
        echo twig_escape_filter($this->env, json_encode(($context["widgetOptions"] ?? null)), "html", null, true);
        echo "\">
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroSegment/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  154 => 50,  151 => 49,  149 => 48,  146 => 47,  144 => 45,  143 => 43,  142 => 41,  141 => 40,  140 => 39,  137 => 38,  133 => 36,  131 => 35,  130 => 34,  129 => 33,  128 => 32,  127 => 30,  126 => 29,  124 => 28,  122 => 27,  118 => 26,  114 => 25,  110 => 24,  107 => 23,  104 => 22,  101 => 21,  98 => 20,  95 => 19,  92 => 18,  89 => 17,  87 => 16,  84 => 15,  81 => 14,  78 => 13,  74 => 12,  67 => 8,  62 => 7,  58 => 5,  55 => 3,  52 => 2,  48 => 1,  44 => 12,  41 => 11,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSegment/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SegmentBundle/Resources/views/Form/fields.html.twig");
    }
}
