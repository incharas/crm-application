<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityMerge/Form/fields.html.twig */
class __TwigTemplate_43f79f2089a0956518e6db51282cfb8f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_entity_merge_widget' => [$this, 'block_oro_entity_merge_widget'],
            'oro_entity_merge_javascript' => [$this, 'block_oro_entity_merge_javascript'],
            'oro_entity_merge_field_widget' => [$this, 'block_oro_entity_merge_field_widget'],
            'oro_entity_merge_choice_value_widget' => [$this, 'block_oro_entity_merge_choice_value_widget'],
            'oro_entity_merge_field_label' => [$this, 'block_oro_entity_merge_field_label'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_entity_merge_widget', $context, $blocks);
        // line 40
        echo "
";
        // line 41
        $this->displayBlock('oro_entity_merge_javascript', $context, $blocks);
        // line 51
        echo "
";
        // line 52
        $this->displayBlock('oro_entity_merge_field_widget', $context, $blocks);
        // line 81
        echo "
";
        // line 82
        $this->displayBlock('oro_entity_merge_choice_value_widget', $context, $blocks);
        // line 94
        echo "
";
        // line 95
        $this->displayBlock('oro_entity_merge_field_label', $context, $blocks);
    }

    // line 1
    public function block_oro_entity_merge_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderJavascript(($context["form"] ?? null));
        echo "
    <table class=\"table table-bordered entity-merge-table\">
        <thead>
        <tr class=\"default-field\">
            <td class=\"merge-first-column\" rowspan=\"2\">
                <div class=\"control-label wrap\">
                    ";
        // line 8
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "masterEntity", [], "any", false, false, false, 8), 'label');
        echo "
                </div>
            </td>
            ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "masterEntity", [], "any", false, false, false, 11));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 12
            echo "                <td class=\"entity-merge-column\">
                    <div class=\"entity-merge-fields-blocks-wrapper\">
                        <label class=\"entity-merge-uppercase entity-merge-inline\">
                            ";
            // line 15
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', ["attr" => twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 15), "attr", [], "any", false, false, false, 15), ["class" => "entity-merge-field-choice"])]);
            echo "

                            <strong data-entity-key=\"";
            // line 17
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 17), "name", [], "any", false, false, false, 17), "html", null, true);
            echo "\"
                                    data-entity-field-name=\"";
            // line 18
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 18), "full_name", [], "any", false, false, false, 18), "html", null, true);
            echo "\"
                                    class=\"merge-entity-representative\">
                                ";
            // line 20
            echo $this->extensions['Oro\Bundle\EntityMergeBundle\Twig\MergeExtension']->renderMergeEntityLabel(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20), (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 20) - 1));
            echo "
                            </strong>
                        </label>
                    </div>
                    <a class=\"entity-merge-select-all\" data-entity-key=\"";
            // line 24
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 24), "value", [], "any", false, false, false, 24), "html", null, true);
            echo "\"
                       href=\"#\">
                        ";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_merge.form.select_all"), "html", null, true);
            echo "
                    </a>
                </td>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 30
        echo "        </tr>
        </thead>
        <tbody>
        ";
        // line 33
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Oro\Bundle\EntityMergeBundle\Twig\MergeExtension']->sortMergeFields(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 33), "fields", [], "any", false, false, false, 33), "children", [], "any", false, false, false, 33)));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 34
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 36
        echo "        </tbody>
    </table>
    <p>* ";
        // line 38
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_merge.hint.other_related_entities"), "html", null, true);
        echo "</p>
";
    }

    // line 41
    public function block_oro_entity_merge_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityMerge/Form/fields.html.twig", 42)->unwrap();
        // line 43
        echo "
    <div ";
        // line 44
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroentitymerge/js/merge-view", "options" => ["_sourceElement" => (("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 47
($context["form"] ?? null), "vars", [], "any", false, false, false, 47), "id", [], "any", false, false, false, 47)) . " .form-horizontal")]]], 44, $context, $this->getSourceContext());
        // line 49
        echo "></div>
";
    }

    // line 52
    public function block_oro_entity_merge_field_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    <tr>
        <td class=\"merge-first-column\">
            ";
        // line 55
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
        echo "
            ";
        // line 56
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, true, false, 56), "mode", [], "any", false, true, false, 56), "vars", [], "any", false, true, false, 56), "choices", [], "any", true, true, false, 56)) {
            // line 57
            echo "                <div class=\"entity-merge-strategy-wrapper\">

                    <span class=\"entity-merge-inline entity-merge-strategy-label\">
                        <div class=\"control-label wrap\">
                            ";
            // line 61
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 61), "mode", [], "any", false, false, false, 61), 'label');
            echo "
                        </div>
                    </span>

                    <div class=\"entity-merge-inline\">
                        ";
            // line 66
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 66), "mode", [], "any", false, false, false, 66), 'widget', ["attr" => ["class" => "entity-merge-small-select"]]);
            echo "
                    </div>
                </div>
            ";
        } else {
            // line 70
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 70), "mode", [], "any", false, false, false, 70), 'widget');
            echo "
            ";
        }
        // line 72
        echo "
        </td>
        ";
        // line 74
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 74), "sourceEntity", [], "any", false, false, false, 74));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 75
            echo "            <td class=\"entity-merge-decision-container\">
                ";
            // line 76
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget', ["entityOffset" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 76) - 1)]);
            echo "
            </td>
        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 79
        echo "    </tr>
";
    }

    // line 82
    public function block_oro_entity_merge_choice_value_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 83
        echo "    <div class=\"entity-merge-fields-blocks-wrapper\">
        <div class=\"entity-merge-inline\">
            ";
        // line 85
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 85), "attr", [], "any", false, false, false, 85), ["class" => "entity-merge-field-choice"])]);
        echo "
        </div>
        <label data-entity-key=\"";
        // line 87
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 87), "name", [], "any", false, false, false, 87), "html", null, true);
        echo "\" data-entity-field-name=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 87), "full_name", [], "any", false, false, false, 87), "html", null, true);
        echo "\"
               class=\"entity-merge-inline-label merge-entity-representative\"
               for=\"";
        // line 89
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? null));
        foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
            echo " ";
            echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        echo ">
            ";
        // line 90
        echo $this->extensions['Oro\Bundle\EntityMergeBundle\Twig\MergeExtension']->renderMergeFieldValue(($context["merge_field_data"] ?? null), ($context["merge_entity_offset"] ?? null));
        echo "
        </label>
    </div>
";
    }

    // line 95
    public function block_oro_entity_merge_field_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "    ";
        if ( !(($context["label"] ?? null) === false)) {
            // line 97
            echo "        ";
            if (($context["required"] ?? null)) {
                // line 98
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 98)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 98), "")) : ("")) . " required"))]);
                // line 99
                echo "        ";
            }
            // line 100
            echo "        ";
            if (twig_test_empty(($context["label"] ?? null))) {
                // line 101
                echo "            ";
                $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
                // line 102
                echo "        ";
            }
            // line 103
            echo "        <strong ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            echo "</strong>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityMerge/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  355 => 103,  352 => 102,  349 => 101,  346 => 100,  343 => 99,  340 => 98,  337 => 97,  334 => 96,  330 => 95,  322 => 90,  305 => 89,  298 => 87,  293 => 85,  289 => 83,  285 => 82,  280 => 79,  263 => 76,  260 => 75,  243 => 74,  239 => 72,  233 => 70,  226 => 66,  218 => 61,  212 => 57,  210 => 56,  206 => 55,  202 => 53,  198 => 52,  193 => 49,  191 => 47,  190 => 44,  187 => 43,  184 => 42,  180 => 41,  174 => 38,  170 => 36,  161 => 34,  157 => 33,  152 => 30,  134 => 26,  129 => 24,  122 => 20,  117 => 18,  113 => 17,  108 => 15,  103 => 12,  86 => 11,  80 => 8,  70 => 2,  66 => 1,  62 => 95,  59 => 94,  57 => 82,  54 => 81,  52 => 52,  49 => 51,  47 => 41,  44 => 40,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityMerge/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityMergeBundle/Resources/views/Form/fields.html.twig");
    }
}
