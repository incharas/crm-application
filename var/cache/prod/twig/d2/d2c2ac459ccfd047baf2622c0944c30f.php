<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/zoomable-area.scss */
class __TwigTemplate_bc0c7c654f24cd4a6e01d9b109258061 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.zoomable-area {
    cursor: move;
    position: relative; /* required for valid controls positioning */

    .zoom-controls {
        position: absolute;
        z-index: 600;
        right: 10px;
        top: 10px;

        .zoom-level {
            width: 42px;
            display: inline-block;
            text-align: center;
            vertical-align: middle;
        }

        [class^='fa-'] {
            font-size: \$zoomable-area-zoom-controls-icon-font-size;
        }
    }

    .zoom-scroll-hint {
        display: flex;

        position: absolute;
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;

        background-color: \$zoomable-area-dimmed-background-color;

        pointer-events: none;
        align-items: center;

        > div {
            margin: 0 auto;
            padding: \$content-padding-small;

            font-size: \$base-font-size--m;
            background-color: \$zoomable-area-dimmed-background-color;
            text-align: center;
        }
    }
}

.fa-search-plus::before {
    content: \$fa-var-search-plus;
}

.fa-search-minus::before {
    content: \$fa-var-search-minus;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/zoomable-area.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/zoomable-area.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/zoomable-area.scss");
    }
}
