<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/page-center-title-view.js */
class __TwigTemplate_59761f85fd806988730a655ba544a2b0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');

    const PageCenterTitleView = BaseView.extend({
        leftBlock: null,

        rightBlock: null,

        currentClass: '',

        listen: {
            'layout:reposition mediator': 'onLayoutReposition'
        },

        /**
         * @inheritdoc
         */
        constructor: function PageCenterTitleView(options) {
            PageCenterTitleView.__super__.constructor.call(this, options);
        },

        initialize: function(options) {
            PageCenterTitleView.__super__.initialize.call(this, options);

            this.leftBlock = this.\$el.siblings('.pull-left-extra')[0];
            this.rightBlock = this.\$el.siblings('.title-buttons-container')[0];
            this.container = this.el.parentNode;
        },

        onLayoutReposition: function() {
            if (!this.leftBlock || !this.rightBlock) {
                return;
            }

            if (this.currentClass) {
                this.container.classList.remove(this.currentClass);
                this.currentClass = '';
            }

            if (this.el.style.display === 'none') {
                return;
            }

            if (this.inFewRows()) {
                const storedDisplay = this.el.style.display;
                this.el.style.display = 'none';
                this.currentClass = this.inFewRows() ? 'center-under-left' : 'center-under-both';
                this.container.classList.add(this.currentClass);
                this.el.style.display = storedDisplay;
            }
        },

        inFewRows: function() {
            const leftRect = this.leftBlock.getBoundingClientRect();
            const rightRect = this.rightBlock.getBoundingClientRect();

            return leftRect.bottom <= rightRect.top || rightRect.bottom <= leftRect.top;
        }
    });

    return PageCenterTitleView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/page-center-title-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/page-center-title-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/page-center-title-view.js");
    }
}
