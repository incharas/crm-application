<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/standart-confirmation.js */
class __TwigTemplate_0fac0373b10e19cb0b84152855fc465d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');
    const ModalView = require('oroui/js/modal');

    /**
     * Standart confirmation dialog
     *
     * @export  oroui/js/standart-confirmation
     * @class   oroui.StandartConfirmationView
     * @extends oroui.ModalView
     */
    const StandartConfirmationView = ModalView.extend({

        /** @property {String} */
        className: 'modal oro-modal-normal',

        defaultOptions: {
            title: __('Confirmation'),
            okText: __('Yes'),
            cancelText: __('Cancel')
        },

        /**
         * @inheritdoc
         */
        constructor: function StandartConfirmationView(options) {
            StandartConfirmationView.__super__.constructor.call(this, options);
        },

        /**
         * @param {Object} options
         */
        initialize: function(options) {
            options = _.defaults(options, this.defaultOptions);

            StandartConfirmationView.__super__.initialize.call(this, options);
        }
    });

    return StandartConfirmationView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/standart-confirmation.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/standart-confirmation.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/standart-confirmation.js");
    }
}
