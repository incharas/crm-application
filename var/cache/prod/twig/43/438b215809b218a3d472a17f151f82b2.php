<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/widget-picker.scss */
class __TwigTemplate_7cdc21ef262f731a82bd01a6a9c2f8aa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.widget-picker {
    &__modal {
        .modal-dialog {
            max-width: \$widget-picker-max-width;
        }

        .modal-body {
            text-align: left;
        }
    }

    &__container {
        max-height: 260px;
        width: 100%;

        text-align: left;

        overflow: auto;
    }

    &__filter {
        display: inline-flex;
    }

    @at-root input#{&}__filter-search {
        width: \$widget-picker-search-width;
    }

    &__title-text {
        font-weight: font-weight('bold');
        font-size: \$widget-picker-item-header-font-size;
        color: \$widget-picker-item-header-color;
        font-style: normal;
    }

    &__results {
        display: flex;
        flex-direction: column;
    }

    &__description-toggler {
        .fa-icon {
            @include fa-icon(\$widget-picker-description-expand-icon, before);
        }
    }

    &__description-cell {
        margin: \$widget-picker-description-cell-margin;
        line-height: \$widget-picker-description-cell-line-height;

        color: \$widget-picker-description-cell-color;

        transform: translateY(\$widget-picker-description-cell-vertical-offset);
    }

    &__toggler-column {
        width: \$widget-picker-toggler-column-width;
        text-align: center;
        flex-grow: 0;
        flex-shrink: 0;
        align-items: center;
    }

    &__icon-column {
        width: \$widget-picker-icon-column-width;
        flex-grow: 0;
        flex-shrink: 0;
    }

    &__info-column {
        flex-grow: 1;
    }

    &__actions-column {
        width: \$widget-picker-actions-column-width;
        padding-right: \$widget-picker-actions-column-padding-end;
        flex-grow: 0;
        flex-shrink: 0;
    }

    &__item {
        display: flex;
        flex-wrap: wrap;
        padding: \$widget-picker-item-cell-padding;
        border-bottom: \$widget-picker-item-separator-width solid \$widget-picker-item-separator-color;

        &[open] {
            .widget-picker__description-toggler {
                .fa-icon {
                    @include fa-icon(\$widget-picker-description-collapse-icon, before, true) {
                        color: \$widget-picker-description-collapse-icon-color;
                        font-size: \$widget-picker-description-collapse-font-size;
                    }
                }
            }
        }

        > * {
            vertical-align: top;
            display: flex;
            align-items: center;
        }

        &.loading {
            .widget-picker__add-action {
                display: none;
            }

            .widget-picker__actions-column {
                &::after {
                    @include loader(\$widget-picker-loading-size, \$widget-picker-loading-border-size);

                    content: '';
                    margin: (\$btn-line-height - \$widget-picker-loading-size) * .5  auto;

                    display: block;
                }
            }
        }
    }

    &__summary-row {
        list-style: none;
        flex-wrap: wrap;

        &::-webkit-details-marker {
            display: none;
        }

        &:focus {
            outline: none;
            outline-width: 0;
        }
    }

    &__title-cell {
        > * {
            display: inline;
        }
    }

    &__new-badge,
    &__added-badge {
        font-weight: font-weight('bold');
        margin-left: \$widget-picker-added-badge-space;
    }

    &__new-badge {
        color: \$widget-picker-new-badge-color;
    }

    &__added-badge {
        color: \$widget-picker-added-badge-color;

        > span {
            color: \$widget-picker-added-badge-count-color;
            font-weight: font-weight('light');
        }
    }

    &__img {
        filter: grayscale(100%);
        border-radius: 50%;
        border: 2px solid \$widget-picker-icon-bg-color;
        box-sizing: content-box;
    }

    &__icon {
        background-color: \$widget-picker-icon-bg-color;
        border-radius: 50%;
        color: \$widget-picker-icon-color;
        height: \$widget-picker-icon-height;
        font-size: 18px;
        margin: 0;
        width: \$widget-picker-icon-width;
        text-align: center;
        line-height: \$widget-picker-icon-height;
        display: inline-block;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/widget-picker.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/widget-picker.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/widget-picker.scss");
    }
}
