<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/form/base-input.scss */
class __TwigTemplate_1cf73f5775330a64aef6e8aa9f330a46 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

%base-input-disable-sate {
    border-color: \$input-disabled-border-color;
    background-color: \$input-disabled-background-color;
    color: \$input-disabled-color;
    resize: \$input-disabled-resize;
    // override for user agent's styles for disabled elements
    opacity: 1;
}

%base-input {
    border-radius: \$input-field-border-radius;
    background-color: \$input-background-color;
    border: \$input-border-width solid \$input-border-color;
    font-family: \$input-font-family;
    padding: \$input-inner-offset;
    box-shadow: \$input-box-shadow;
    color: \$input-color;
    margin-bottom: \$input-outer-offset-bottom;
    box-sizing: \$input-box-sizing;
    outline: \$input-outline;
    width: \$field-width;

    &:not(textarea) {
        height: \$field-size;
    }

    &:focus {
        box-shadow: \$input-focus-box-shadow;
        border-color: \$input-focus-border-color;
        z-index: 1;

        &:invalid {
            &:focus {
                box-shadow: \$input-invalid-focus-box-shadow;
                border-color: \$input-invalid-focus-border-color;
            }
        }
    }

    &:disabled {
        @extend %base-input-disable-sate;
    }

    &.error {
        border-color: \$input-error-border-color;
        box-shadow: \$input-error-box-shadow;
    }

    &.full-width {
        width: 100%;
        min-width: 100%;
    }

    // Disable platform styles of \"search\" input
    &::-webkit-search-decoration,
    &::-webkit-search-cancel-button,
    &::-webkit-search-results-button,
    &::-webkit-search-results-decoration,
    // Disable platform styles of \"time\" input
    &::-webkit-inner-spin-button {
        display: none;
    }

    @include placeholder {
        color: \$primary-700;
    }

    td & {
        margin-bottom: 0;
    }

    .control-group & {
        margin-bottom: \$input-control-group-outer-offset-bottom;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/form/base-input.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/form/base-input.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/form/base-input.scss");
    }
}
