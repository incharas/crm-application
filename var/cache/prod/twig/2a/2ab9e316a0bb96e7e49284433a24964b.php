<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivityList/ActivityList/js/activityItemTemplate.html.twig */
class __TwigTemplate_f6dba07cc76966ee983e25e94ea6356f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'activityIcon' => [$this, 'block_activityIcon'],
            'activityActions' => [$this, 'block_activityActions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroActivityList/ActivityList/js/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroActivityList/ActivityList/js/view.html.twig", "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_activityIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <span class=\"";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(((array_key_exists("entityClass", $context)) ? (_twig_default_filter(($context["entityClass"] ?? null))) : ("")), "icon"), "html_attr");
        echo "\" aria-hidden=\"true\"></span>
";
    }

    // line 7
    public function block_activityActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <% var togglerId = _.uniqueId('dropdown-') %>
    <div class=\"vertical-actions activity-actions\">
        <button id=\"<%- togglerId %>\" data-placement=\"left-start\" data-toggle=\"dropdown\" class=\"btn btn-icon btn-light-custom dropdown-toggle dropdown-toggle--no-caret activity-item\"
           aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.actions.label"), "html", null, true);
        echo "\">
           <span class=\"fa-ellipsis-h\"></span>
        </button>
        <ul class=\"dropdown-menu dropdown-toggle--no-caret activity-item\" role=\"menu\" aria-labelledby=\"<%- togglerId %>\">
            ";
        // line 15
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(((array_key_exists("actions", $context)) ? (_twig_default_filter(($context["actions"] ?? null), [])) : ([])));
        foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
            // line 16
            echo "                <li class=\"activity-action\">";
            echo $context["action"];
            echo "</li>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "            ";
        $this->loadTemplate("@OroActivityList/ActivityList/js/workflowTemplate.html.twig", "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", 18)->display($context);
        // line 19
        echo "        </ul>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 19,  87 => 18,  78 => 16,  74 => 15,  67 => 11,  62 => 8,  58 => 7,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActivityListBundle/Resources/views/ActivityList/js/activityItemTemplate.html.twig");
    }
}
