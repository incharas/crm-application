<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/label.scss */
class __TwigTemplate_baa0265a2589e80b0f3fabfb309ee706 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.label {
    display: inline-block;
    max-width: 100%;
    padding: 0 \$badge-padding-x;
    font-size: \$base-font-size--s;
    font-weight: font-weight('light');
    line-height: 1.2;
    border-radius: \$border-radius;

    &-large {
        font-size: \$base-font-size;
    }

    .page-title__path & {
        @extend %badge;
        @extend %badge-pill;

        border: none;
    }
}

@each \$color, \$values in \$label-theme-keys {
    .label-#{\$color} {
        @include label-custom-variant(\$values...);
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/label.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/label.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/label.scss");
    }
}
