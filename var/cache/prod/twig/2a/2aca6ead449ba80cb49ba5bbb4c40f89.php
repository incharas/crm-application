<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAccount/Account/widget/info.html.twig */
class __TwigTemplate_b49915132f8199e10554aaeff87bb702 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAccount/Account/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroAccount/Account/widget/info.html.twig", 2)->unwrap();
        // line 3
        echo "<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        ";
        // line 5
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["account"] ?? null), "name")) {
            // line 6
            echo "            <div class=\"responsive-block\">
                ";
            // line 7
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["account"] ?? null), "name", [], "any", false, false, false, 7)], 7, $context, $this->getSourceContext());
            echo "
                ";
            // line 8
            echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["account"] ?? null)], 8, $context, $this->getSourceContext());
            echo "
            </div>
        ";
        }
        // line 11
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroAccount/Account/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 11,  54 => 8,  50 => 7,  47 => 6,  45 => 5,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAccount/Account/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AccountBundle/Resources/views/Account/widget/info.html.twig");
    }
}
