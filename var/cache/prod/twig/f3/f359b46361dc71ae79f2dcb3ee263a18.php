<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Search/result.html.twig */
class __TwigTemplate_e4d0af0a5507f87d70c7cad32d3b31de extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'additional_info' => [$this, 'block_additional_info'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 6
        return "@OroSearch/Search/searchResultItem.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Search/result.html.twig", 7)->unwrap();
        // line 9
        $context["showImage"] = true;
        // line 10
        $context["iconType"] = "user";
        // line 11
        $context["image"] = ((($context["entity"] ?? null)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "avatar", [], "any", false, false, false, 11)) : (null));
        // line 13
        $context["recordUrl"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["indexer_item"] ?? null), "recordUrl", [], "any", false, false, false, 13);
        // line 29
        $context["title"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["indexer_item"] ?? null), "selectedData", [], "any", false, false, false, 29), "name", [], "any", false, false, false, 29);
        // line 30
        $context["entityType"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["indexer_item"] ?? null), "entityName", [], "any", false, false, false, 30), "label"));
        // line 32
        $context["entityInfo"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "value" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 33
($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 33)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 33))) : ("N/A"))], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "value" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 34)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 34))) : ("N/A"))], 2 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.last_login.label"), "value" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 35
($context["entity"] ?? null), "lastLogin", [], "any", false, false, false, 35)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lastLogin", [], "any", false, false, false, 35))) : ("N/A"))], 3 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.login_count.label"), "value" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 36
($context["entity"] ?? null), "loginCount", [], "any", true, true, false, 36)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "loginCount", [], "any", false, false, false, 36), 0)) : (0))]];
        // line 6
        $this->parent = $this->loadTemplate("@OroSearch/Search/searchResultItem.html.twig", "@OroUser/Search/result.html.twig", 6);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 15
    public function block_additional_info($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    ";
        if (($context["entity"] ?? null)) {
            // line 17
            echo "        <span class=\"page-title__status\">
            ";
            // line 18
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "enabled", [], "any", false, false, false, 18)) {
                // line 19
                echo "                ";
                echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Active"), "enabled"], 19, $context, $this->getSourceContext());
                echo "
            ";
            } else {
                // line 21
                echo "                ";
                echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Inactive"), "disabled"], 21, $context, $this->getSourceContext());
                echo "
            ";
            }
            // line 23
            echo "        </span>
    ";
        } else {
            // line 25
            echo "        ";
            $this->displayParentBlock("additional_info", $context, $blocks);
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroUser/Search/result.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 25,  91 => 23,  85 => 21,  79 => 19,  77 => 18,  74 => 17,  71 => 16,  67 => 15,  62 => 6,  60 => 36,  59 => 35,  58 => 34,  57 => 33,  56 => 32,  54 => 30,  52 => 29,  50 => 13,  48 => 11,  46 => 10,  44 => 9,  42 => 7,  35 => 6,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Search/result.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Search/result.html.twig");
    }
}
