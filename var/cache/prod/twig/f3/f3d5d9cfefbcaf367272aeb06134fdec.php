<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/skeleton/_ellipse.scss */
class __TwigTemplate_cd4b40bee6d78c88976c11e05f358db6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@use 'sass:map';

// @private: background-image generator
// Take a particle map and generate background-image value
// @param: Map \$particle
// @return: List of background-image value
@function skeleton-image-ellipse(\$particle) {
    \$color: map.get(\$particle, 'color');

    @return (
        radial-gradient(closest-side, \$color 100%, transparent 100%),
    );
}

// @private: background-size generator
// Take a particle map and generate background-size value
// @param: Map \$particle
// @return: List of background-size value
@function skeleton-size-ellipse(\$particle) {
    \$width: map.get(\$particle, 'width');
    \$height: map.get(\$particle, 'height');

    @return (
        \$width \$height,
    );
}

// @private: background-position generator
// Take a particle map and generate background-position value
// @param: Map \$particle
// @return: List of background-position value
@function skeleton-position-ellipse(\$particle) {
    \$x: map.get(\$particle, 'x');
    \$y: map.get(\$particle, 'y');

    @return (
        \$x \$y,
    );
}

// @public: function for get \$particle object for draw ellipse
// @param: \$color
// @param: \$width
// @param: \$height
// @param: \$x: 0
// @param: \$y: 0
// @return: Formated map of parameters
@function skeleton-ellipse(
    \$color,
    \$width,
    \$height,
    \$x: 0,
    \$y: 0
) {
    @return (
        'type': 'ellipse',
        'color': \$color,
        'width': \$width,
        'height': \$height,
        'x': \$x,
        'y': \$y
    );
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/skeleton/_ellipse.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/skeleton/_ellipse.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/skeleton/_ellipse.scss");
    }
}
