<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityExtend/ConfigFieldGrid/create.html.twig */
class __TwigTemplate_a335ca2a09ffe9fdfddd5c5aa5e2add2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), "@OroForm/Form/fields.html.twig", true);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 4), "N/A")) : ("N/A")))]]);
        // line 6
        if ( !array_key_exists("formAction", $context)) {
            // line 7
            $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityextend_field_create", ["id" => ($context["entity_id"] ?? null)]);
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEntityExtend/ConfigFieldGrid/create.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityExtend/ConfigFieldGrid/create.html.twig", 11)->unwrap();
        // line 12
        echo "
    ";
        // line 13
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_view", ["id" => ($context["entity_id"] ?? null)])], 13, $context, $this->getSourceContext());
        echo "
    ";
        // line 14
        echo twig_call_macro($macros["UI"], "macro_saveActionButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Continue"), "route" => "oro_entityextend_field_update", "params" => ["id" => "\$id"]]], 14, $context, $this->getSourceContext());
        // line 18
        echo "
";
    }

    // line 21
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        if ( !array_key_exists("breadcrumbs", $context)) {
            // line 23
            echo "        ";
            $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.config_grid.entities"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.config_grid.new_field"), "additional" => [0 => ["indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_view", ["id" =>             // line 30
($context["entity_id"] ?? null)]), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 31)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 31), "N/A")) : ("N/A")))]]];
            // line 35
            echo "    ";
        }
        // line 36
        echo "
    ";
        // line 37
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 40
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    ";
        $context["id"] = "configfield-create";
        // line 42
        echo "    ";
        $context["data"] = ["formErrors" => ((        // line 43
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" => $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormDataBlocks($this->env, $context,         // line 44
($context["form"] ?? null)), "hiddenData" =>         // line 45
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest')];
        // line 47
        echo "
    ";
        // line 48
        $context["options"] = ["typeId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 49
($context["form"] ?? null), "type", [], "any", false, false, false, 49), "vars", [], "any", false, false, false, 49), "id", [], "any", false, false, false, 49), "fieldId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 50
($context["form"] ?? null), "fieldName", [], "any", false, false, false, 50), "vars", [], "any", false, false, false, 50), "id", [], "any", false, false, false, 50)];
        // line 52
        echo "
    <div data-page-component-module=\"oroentityextend/js/extend-field-form-component\"
         data-page-component-options=\"";
        // line 54
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\">
        ";
        // line 55
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroEntityExtend/ConfigFieldGrid/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  132 => 55,  128 => 54,  124 => 52,  122 => 50,  121 => 49,  120 => 48,  117 => 47,  115 => 45,  114 => 44,  113 => 43,  111 => 42,  108 => 41,  104 => 40,  98 => 37,  95 => 36,  92 => 35,  90 => 31,  89 => 30,  87 => 23,  84 => 22,  80 => 21,  75 => 18,  73 => 14,  69 => 13,  66 => 12,  63 => 11,  59 => 10,  54 => 1,  51 => 7,  49 => 6,  47 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityExtend/ConfigFieldGrid/create.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityExtendBundle/Resources/views/ConfigFieldGrid/create.html.twig");
    }
}
