<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/font-awesome/_core.scss */
class __TwigTemplate_1d59afe4117266f600d198a61f8d9435 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import '~@oroinc/font-awesome/scss/bordered-pulled';
@import '~@oroinc/font-awesome/scss/icons';
@import 'icons-rtl';
@import '~@oroinc/font-awesome/scss/animated';
@import 'animated-rtl';

[class^='fa-'],
[class*=' fa-'] {
    display: inline-block;
    font-family: \$fa-font-family;
    font-size: inherit;
    font-weight: font-weight('normal');
    font-style: normal;
    line-height: 1;
    text-rendering: auto;
    -webkit-font-smoothing: antialiased;
    -moz-osx-font-smoothing: grayscale;

    &.hide-text {
        @include hide-text();

        color: inherit;

        &::after,
        &::before {
            padding: 0;
            text-indent: 0;
            font-size: \$icon-font-size;
            line-height: 1;
            font-family: \$fa-font-family;
        }
    }
}

.fa-arrows-v {
    // because resize-vertical icon is too thin
    padding: 0 3px;
}

.icon-empty::before {
    content: ' ';
}

.fa-offset-none {
    // stylelint-disable-next-line declaration-no-important
    margin: 0 !important;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/font-awesome/_core.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/font-awesome/_core.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/font-awesome/_core.scss");
    }
}
