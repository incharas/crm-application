<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChart/Form/fields.html.twig */
class __TwigTemplate_586c3c8299aeaef1765c640fa6a405fd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_chart_widget' => [$this, 'block_oro_chart_widget'],
            'oro_chart_setting_row' => [$this, 'block_oro_chart_setting_row'],
            'oro_chart_settings_collection_row' => [$this, 'block_oro_chart_settings_collection_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_chart_widget', $context, $blocks);
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('oro_chart_setting_row', $context, $blocks);
        // line 8
        echo "
";
        // line 9
        $this->displayBlock('oro_chart_settings_collection_row', $context, $blocks);
    }

    // line 1
    public function block_oro_chart_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form');
        echo "
";
    }

    // line 5
    public function block_oro_chart_setting_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    // line 9
    public function block_oro_chart_settings_collection_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroChart/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  80 => 10,  76 => 9,  69 => 6,  65 => 5,  58 => 2,  54 => 1,  50 => 9,  47 => 8,  45 => 5,  42 => 4,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChart/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ChartBundle/Resources/views/Form/fields.html.twig");
    }
}
