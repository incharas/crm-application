<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntity/Entities/update.html.twig */
class __TwigTemplate_888c1a4ae78925dfe97df775e4a16722 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), "@OroForm/Form/fields.html.twig", true);
        // line 4
        $context["entityName"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 4)) ? (_twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(        // line 5
($context["entity"] ?? null)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity.item", ["%id%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5)]))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 6))])));

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" =>         // line 8
($context["entityName"] ?? null)]]);
        // line 10
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entity_update", ["entityName" => ($context["entity_name"] ?? null), "id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 10)]);
        // line 12
        $context["audit_entity_class"] = twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "getId", [], "any", false, false, false, 12), "getClassName", [], "any", false, false, false, 12), ["\\" => "_"]);
        // line 13
        $context["audit_entity_id"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 13);
        // line 14
        $context["audit_title"] = _twig_default_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 14)), "N/A");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEntity/Entities/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 16
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntity/Entities/update.html.twig", 17)->unwrap();
        // line 18
        echo "
    ";
        // line 19
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 19) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)))) {
            // line 20
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entity_delete", ["entityName" =>             // line 21
($context["entity_class"] ?? null), "id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 21)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entity_index", ["entityName" =>             // line 22
($context["entity_class"] ?? null)]), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 24
($context["entity"] ?? null), "id", [], "any", false, false, false, 24), "id" => "btn-remove-account", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 26)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 26), "N/A")) : ("N/A")))]], 20, $context, $this->getSourceContext());
            // line 27
            echo "
        ";
            // line 28
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 28, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 30
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entity_index", ["entityName" => ($context["entity_name"] ?? null)])], 30, $context, $this->getSourceContext());
        echo "
    ";
        // line 31
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_entity_view", "params" => ["entityName" =>         // line 33
($context["entity_name"] ?? null), "id" => "\$id"]]], 31, $context, $this->getSourceContext());
        // line 35
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("CREATE", ("entity:" . ($context["entity_class"] ?? null)))) {
            // line 36
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_entity_update", "params" => ["entityName" =>             // line 38
($context["entity_name"] ?? null)]]], 36, $context, $this->getSourceContext()));
            // line 40
            echo "    ";
        }
        // line 41
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 41) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ("entity:" . ($context["entity_class"] ?? null))))) {
            // line 42
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_entity_update", "params" => ["entityName" =>             // line 44
($context["entity_name"] ?? null), "id" => "\$id"]]], 42, $context, $this->getSourceContext()));
            // line 46
            echo "    ";
        }
        // line 47
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 47, $context, $this->getSourceContext());
        echo "
";
    }

    // line 50
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    ";
        $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity.plural_label"), "entityTitle" =>         // line 55
($context["entityName"] ?? null), "additional" => [0 => ["indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entity_index", ["entityName" =>         // line 58
($context["entity_name"] ?? null)]), "indexLabel" => _twig_default_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 59
($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 59)), "N/A")]]];
        // line 63
        echo "
    ";
        // line 64
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 67
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 68
        echo "    ";
        $this->displayParentBlock("stats", $context, $blocks);
        echo "
";
    }

    // line 71
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 72
        echo "    ";
        $context["id"] = "custom_entity-update";
        // line 73
        echo "    ";
        $context["dataBlocks"] = $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormDataBlocks($this->env, $context, ($context["form"] ?? null));
        // line 74
        echo "    ";
        $context["data"] = ["formErrors" => ((        // line 75
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 76
($context["dataBlocks"] ?? null)];
        // line 78
        echo "
    ";
        // line 79
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntity/Entities/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 79,  174 => 78,  172 => 76,  171 => 75,  169 => 74,  166 => 73,  163 => 72,  159 => 71,  152 => 68,  148 => 67,  142 => 64,  139 => 63,  137 => 59,  136 => 58,  135 => 55,  133 => 51,  129 => 50,  122 => 47,  119 => 46,  117 => 44,  115 => 42,  112 => 41,  109 => 40,  107 => 38,  105 => 36,  102 => 35,  100 => 33,  99 => 31,  94 => 30,  89 => 28,  86 => 27,  84 => 26,  83 => 24,  82 => 22,  81 => 21,  79 => 20,  77 => 19,  74 => 18,  71 => 17,  67 => 16,  62 => 1,  60 => 14,  58 => 13,  56 => 12,  54 => 10,  52 => 8,  49 => 6,  48 => 5,  47 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntity/Entities/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Resources/views/Entities/update.html.twig");
    }
}
