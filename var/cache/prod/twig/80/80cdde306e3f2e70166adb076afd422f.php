<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/User/update.html.twig */
class __TwigTemplate_a6ff8814911c476bcba0bb42c72ad54e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'pageTitleIcon' => [$this, 'block_pageTitleIcon'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig"], true);
        // line 4
        $context["fullname"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "value", [], "any", false, false, false, 4)), "N/A");

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%username%" =>         // line 5
($context["fullname"] ?? null), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_label")]]);
        // line 7
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroUser/User/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/User/update.html.twig", 10)->unwrap();
        // line 11
        echo "
    ";
        // line 12
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "value", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "value", [], "any", false, false, false, 12))) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "value", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12) != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12)))) {
            // line 13
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_user", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["form"] ?? null), "vars", [], "any", false, false, false, 14), "value", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-user", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["form"] ?? null), "vars", [], "any", false, false, false, 18), "value", [], "any", false, false, false, 18), "id", [], "any", false, false, false, 18), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_label"), "disabled" =>  !            // line 20
($context["allow_delete"] ?? null)]], 13, $context, $this->getSourceContext());
            // line 21
            echo "
        ";
            // line 22
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 22, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 24
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_index")], 24, $context, $this->getSourceContext());
        echo "
    ";
        // line 25
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_user_view", "params" => ["id" => "\$id"]]], 25, $context, $this->getSourceContext());
        // line 29
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_create")) {
            // line 30
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_user_create"]], 30, $context, $this->getSourceContext()));
            // line 33
            echo "    ";
        }
        // line 34
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 34), "value", [], "any", false, false, false, 34), "id", [], "any", false, false, false, 34) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_user_update"))) {
            // line 35
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_user_update", "params" => ["id" => "\$id"]]], 35, $context, $this->getSourceContext()));
            // line 39
            echo "    ";
        }
        // line 40
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 40, $context, $this->getSourceContext());
        echo "
    ";
        // line 41
        $this->displayParentBlock("navButtons", $context, $blocks);
        echo "
";
    }

    // line 44
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 45), "value", [], "any", false, false, false, 45), "id", [], "any", false, false, false, 45)) {
            // line 46
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 47
($context["form"] ?? null), "vars", [], "any", false, false, false, 47), "value", [], "any", false, false, false, 47), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_plural_label"), "entityTitle" =>             // line 50
($context["fullname"] ?? null)];
            // line 53
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 55
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_label")]);
            // line 56
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroUser/User/update.html.twig", 56)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 57
            echo "    ";
        }
    }

    // line 60
    public function block_pageTitleIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 61
        echo "    <div class=\"page-title__icon\">";
        // line 62
        $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroUser/User/update.html.twig", 62)->display(twig_array_merge($context, ["file" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 63
($context["form"] ?? null), "vars", [], "any", false, false, false, 63), "value", [], "any", false, false, false, 63), "avatar", [], "any", false, false, false, 63), "filter" => "avatar_med", "img_attrs" => ["alt" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 65
($context["form"] ?? null), "vars", [], "any", false, false, false, 65), "value", [], "any", false, false, false, 65))]]));
        // line 67
        echo "</div>
";
    }

    // line 70
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 71), "value", [], "any", false, false, false, 71), "createdAt", [], "any", false, false, false, 71)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 71), "value", [], "any", false, false, false, 71), "createdAt", [], "any", false, false, false, 71)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 72
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 72), "value", [], "any", false, false, false, 72), "updatedAt", [], "any", false, false, false, 72)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 72), "value", [], "any", false, false, false, 72), "updatedAt", [], "any", false, false, false, 72)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 73
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.last_login.label"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 73), "value", [], "any", false, false, false, 73), "lastLogin", [], "any", false, false, false, 73)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 73), "value", [], "any", false, false, false, 73), "lastLogin", [], "any", false, false, false, 73)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 74
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.login_count.label"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 74), "value", [], "any", false, true, false, 74), "loginCount", [], "any", true, true, false, 74)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 74), "value", [], "any", false, true, false, 74), "loginCount", [], "any", false, false, false, 74), 0)) : (0)), "html", null, true);
        echo "</li>
";
    }

    // line 77
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 78
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/User/update.html.twig", 78)->unwrap();
        // line 79
        echo "
<div class=\"user-profile-cols\">
    ";
        // line 81
        $context["id"] = "user-profile";
        // line 82
        echo "
    ";
        // line 83
        $context["formRows"] = [0 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 84
($context["form"] ?? null), "enabled", [], "any", true, true, false, 84)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "enabled", [], "any", false, false, false, 84), 'row')) : ("")), 1 =>         // line 85
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "username", [], "any", false, false, false, 85), 'row'), 2 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 86
($context["form"] ?? null), "plainPassword", [], "any", true, true, false, 86)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "plainPassword", [], "any", false, false, false, 86), 'row')) : ("")), 3 =>         // line 87
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "namePrefix", [], "any", false, false, false, 87), 'row'), 4 =>         // line 88
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "firstName", [], "any", false, false, false, 88), 'row'), 5 =>         // line 89
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "middleName", [], "any", false, false, false, 89), 'row'), 6 =>         // line 90
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "lastName", [], "any", false, false, false, 90), 'row'), 7 =>         // line 91
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "nameSuffix", [], "any", false, false, false, 91), 'row'), 8 =>         // line 92
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "birthday", [], "any", false, false, false, 92), 'row'), 9 =>         // line 93
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "avatar", [], "any", false, false, false, 93), 'row'), 10 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 94
($context["form"] ?? null), "vars", [], "any", false, false, false, 94), "value", [], "any", false, false, false, 94), "id", [], "any", false, false, false, 94)) ? ("") : ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "inviteUser", [], "any", false, false, false, 94), 'row')))];
        // line 96
        echo "
    ";
        // line 97
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "passwordGenerate", [], "any", true, true, false, 97)) {
            // line 98
            echo "        ";
            $context["formRows"] = twig_array_merge(($context["formRows"] ?? null), [0 =>             // line 99
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "passwordGenerate", [], "any", false, false, false, 99), 'row')]);
            // line 101
            echo "        <span data-page-component-module=\"orouser/js/components/password-generate\"
              data-page-component-options=\"";
            // line 102
            echo twig_escape_filter($this->env, json_encode(["checkbox" => "[data-name=\"field__password-generate\"]", "passwordInput" => "[data-name=\"field__first\"],[data-name=\"field__second\"]"]), "html", null, true);
            // line 105
            echo "\"></span>
    ";
        }
        // line 107
        echo "
    ";
        // line 108
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" =>         // line 113
($context["formRows"] ?? null)], 1 => ["title" => "", "data" => [0 =>         // line 118
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "email", [], "any", false, false, false, 118), 'row'), 1 =>         // line 119
        $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emails", [], "any", false, false, false, 119), 'form_row_collection'), 2 =>         // line 120
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "phone", [], "any", false, false, false, 120), 'row')]]]]];
        // line 125
        echo "
    ";
        // line 126
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 127
        echo "
    ";
        // line 128
        $context["hasGroups"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "groups", [], "any", true, true, false, 128) && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isFeatureEnabled("manage_user_groups"));
        // line 129
        echo "    ";
        $context["hasRoles"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "userRoles", [], "any", true, true, false, 129);
        // line 130
        echo "    ";
        if ((($context["hasGroups"] ?? null) || ($context["hasRoles"] ?? null))) {
            // line 131
            echo "        ";
            ob_start(function () { return ''; });
            // line 132
            echo "            ";
            if (($context["hasGroups"] ?? null)) {
                // line 133
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "groups", [], "any", false, false, false, 133), 'row', ["attr" => ["class" => "horizontal"]]);
                echo "
            ";
            }
            // line 135
            echo "        ";
            $context["userGroups"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 136
            echo "
        ";
            // line 137
            ob_start(function () { return ''; });
            // line 138
            echo "            ";
            if (($context["hasRoles"] ?? null)) {
                // line 139
                echo "                <div class=\"form-horizontal\" id=\"roles-list\">
                    ";
                // line 140
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "userRoles", [], "any", false, false, false, 140), 'row', ["attr" => ["class" => "horizontal"]]);
                echo "
                </div>
            ";
            }
            // line 143
            echo "        ";
            $context["userRoles"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 144
            echo "
        ";
            // line 145
            $context["title"] = "";
            // line 146
            echo "        ";
            if ((($context["hasGroups"] ?? null) && ($context["hasRoles"] ?? null))) {
                // line 147
                echo "            ";
                $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("%groups% and %roles%", ["%groups%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.groups.label"), "%roles%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.roles.label")]);
                // line 151
                echo "        ";
            } elseif (($context["hasGroups"] ?? null)) {
                // line 152
                echo "            ";
                $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.groups.label");
                // line 153
                echo "        ";
            } elseif (($context["hasRoles"] ?? null)) {
                // line 154
                echo "            ";
                $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.roles.label");
                // line 155
                echo "        ";
            }
            // line 156
            echo "
        ";
            // line 157
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" =>             // line 158
($context["title"] ?? null), "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 162
($context["userGroups"] ?? null), 1 => ($context["userRoles"] ?? null)]]]]]);
            // line 166
            echo "    ";
        }
        // line 167
        echo "
    ";
        // line 168
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organizations", [], "any", true, true, false, 168)) {
            // line 169
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Access Settings"), "subblocks" => [0 => ["title" => "", "useSpan" => false, "data" => [0 =>             // line 174
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organizations", [], "any", false, false, false, 174), 'row')]]]]]);
            // line 177
            echo "    ";
        }
        // line 178
        echo "
    ";
        // line 179
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "change_password", [], "any", true, true, false, 179)) {
            // line 180
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.password.label"), "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 186
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "change_password", [], "any", false, false, false, 186), 'widget')]]]]]);
            // line 191
            echo "    ";
        }
        // line 192
        echo "
    ";
        // line 193
        $context["data"] = ["formErrors" => ((        // line 194
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 195
($context["dataBlocks"] ?? null)];
        // line 197
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/User/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  370 => 197,  368 => 195,  367 => 194,  366 => 193,  363 => 192,  360 => 191,  358 => 186,  356 => 180,  354 => 179,  351 => 178,  348 => 177,  346 => 174,  344 => 169,  342 => 168,  339 => 167,  336 => 166,  334 => 162,  333 => 158,  332 => 157,  329 => 156,  326 => 155,  323 => 154,  320 => 153,  317 => 152,  314 => 151,  311 => 147,  308 => 146,  306 => 145,  303 => 144,  300 => 143,  294 => 140,  291 => 139,  288 => 138,  286 => 137,  283 => 136,  280 => 135,  274 => 133,  271 => 132,  268 => 131,  265 => 130,  262 => 129,  260 => 128,  257 => 127,  255 => 126,  252 => 125,  250 => 120,  249 => 119,  248 => 118,  247 => 113,  246 => 108,  243 => 107,  239 => 105,  237 => 102,  234 => 101,  232 => 99,  230 => 98,  228 => 97,  225 => 96,  223 => 94,  222 => 93,  221 => 92,  220 => 91,  219 => 90,  218 => 89,  217 => 88,  216 => 87,  215 => 86,  214 => 85,  213 => 84,  212 => 83,  209 => 82,  207 => 81,  203 => 79,  201 => 78,  197 => 77,  189 => 74,  183 => 73,  177 => 72,  170 => 71,  166 => 70,  161 => 67,  159 => 65,  158 => 63,  157 => 62,  155 => 61,  151 => 60,  146 => 57,  143 => 56,  140 => 55,  134 => 53,  132 => 50,  131 => 47,  129 => 46,  126 => 45,  122 => 44,  116 => 41,  111 => 40,  108 => 39,  105 => 35,  102 => 34,  99 => 33,  96 => 30,  93 => 29,  91 => 25,  86 => 24,  81 => 22,  78 => 21,  76 => 20,  75 => 18,  74 => 14,  72 => 13,  70 => 12,  67 => 11,  64 => 10,  60 => 9,  55 => 1,  53 => 7,  51 => 5,  48 => 4,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/User/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/User/update.html.twig");
    }
}
