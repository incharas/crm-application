<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/wysiwig-editor/txt-html-transformer.js */
class __TwigTemplate_bd7e16984db675ba2e646b00da3fa922 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function() {
    'use strict';

    return {
        text2html: function(content) {
            // keep paragraphs at least
            return '<p>' + String(content).replace(/(\\n\\r?|\\r\\n?)/g, '</p><p>') + '</p>';
        },
        html2text: function(content) {
            return String(content)
                .replace(/<head>[^]*<\\/head>/, '')
                .replace(/<\\/?[^>]+>/g, '')
                .replace(/\\s*\\n{2,}/g, '\\n\\n')
                .trim();
        }
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/wysiwig-editor/txt-html-transformer.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/wysiwig-editor/txt-html-transformer.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/wysiwig-editor/txt-html-transformer.js");
    }
}
