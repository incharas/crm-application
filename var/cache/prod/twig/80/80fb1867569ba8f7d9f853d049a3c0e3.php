<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/tab/oro-tabs.scss */
class __TwigTemplate_0eb651735c88a66263129a62fc03f067 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.oro-tabs {
    .tab-pane {
        .fill-tab {
            border: \$oro-tabs-fill-tab-border;
            box-shadow: \$oro-tabs-fill-tab-box-shadow;
            box-sizing: \$oro-tabs-fill-tab-box-sizing;
            width: \$oro-tabs-fill-tab-width;
        }
    }

    &__vertical {
        display: \$oro-tabs-vertical-display;

        > .tabs-subtitle {
            padding: \$oro-tabs-subtitle-inner-offset;
            font-weight: \$oro-tabs-subtitle-font-weight;
        }

        > .oro-tabs__head {
            width: \$oro-tabs-head-width;
            flex-shrink: \$oro-tabs-head-flex-shrink;
            flex-grow: \$oro-tabs-head-flex-grow;
            margin-right: \$oro-tabs-head-outer-offset-right;
            position: \$oro-tabs-head-position;
        }

        > .oro-tabs__content {
            flex-grow: \$oro-tabs-content-flex-grow;
            border-left: \$oro-tabs-content-border-left;
            padding-left: \$horizontal-padding;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/tab/oro-tabs.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/tab/oro-tabs.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/tab/oro-tabs.scss");
    }
}
