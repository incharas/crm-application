<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTranslation/Language/Datagrid/translationCompleteness.html.twig */
class __TwigTemplate_bf4ef8777a748fbd7de2a58352813055 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["translationCompleteness"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "translationCompleteness"], "method", false, false, false, 1);
        // line 2
        echo "
";
        // line 3
        if ( !(null === ($context["translationCompleteness"] ?? null))) {
            // line 4
            echo "    <div class=\"translation-completeness\">
        <div class=\"progress\">
            <div class=\"progress-bar success\" style=\"width: ";
            // line 6
            echo twig_escape_filter($this->env, (($context["translationCompleteness"] ?? null) * 100), "html", null, true);
            echo "%;\"></div>
        </div>
        <b class=\"progress-label\">";
            // line 8
            echo twig_escape_filter($this->env, twig_round((($context["translationCompleteness"] ?? null) * 100), 0, "floor"), "html", null, true);
            echo "%</b>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroTranslation/Language/Datagrid/translationCompleteness.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 8,  48 => 6,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTranslation/Language/Datagrid/translationCompleteness.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/Resources/views/Language/Datagrid/translationCompleteness.html.twig");
    }
}
