<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/inter-window-mediator/inter-window-mediator.js */
class __TwigTemplate_8213bfca2cd736fcb2d94ec2649cd998 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/**
 * The mediator provides listening and triggering event through all browser windows/tabs
 * on the same URL domain.
 *
 * Based on `storage` event that fires on window when some property of localStorage was changed.
 */
define(function(require) {
    'use strict';

    const BaseClass = require('oroui/js/base-class');

    const InterWindowMediator = BaseClass.extend({
        /**
         * @inheritdoc
         */
        constructor: function InterWindowMediator() {
            this.onStorageChange = this.onStorageChange.bind(this);

            InterWindowMediator.__super__.constructor.call(this);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            InterWindowMediator.__super__.initialize.call(this, options);

            this.id = (Math.random().toString() + Date.now()).substr(2);
            window.addEventListener('storage', this.onStorageChange);
        },

        /**
         * @inheritdoc
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }

            window.removeEventListener('storage', this.onStorageChange);

            InterWindowMediator.__super__.dispose.call(this);
        },

        /**
         * Triggers the event in rest of browser tabs
         * tabs communication is implemented over storage event
         *
         * @param {string} eventName
         * @param {...(Object|Array|number|string|boolean|null}} - optional data that will be passed to a handler as arguments
         */
        trigger: function(eventName, args) {
            const eventData = {targetId: this.id, args};
            const storageKey = InterWindowMediator.NS + eventName;
            localStorage.setItem(storageKey, JSON.stringify(eventData));
            localStorage.removeItem(storageKey);
        },

        /**
         * Handles storage and triggers local event
         *
         * @param e
         */
        onStorageChange: function(e) {
            if (
                e.key.substring(0, InterWindowMediator.NS.length) === InterWindowMediator.NS &&
                e.newValue !== null && e.newValue !== ''
            ) {
                const eventName = e.key.substring(InterWindowMediator.NS.length);
                const eventData = JSON.parse(e.newValue);

                // Since IE11 triggers `storage` event on current window lets check and skip it
                if (eventData.targetId !== this.id) {
                    // triggers the event over original Backbone.Event.trigger method to execute all inner handlers
                    InterWindowMediator.__super__.trigger.apply(this, [eventName].concat(eventData.args));
                }
            }
        }
    }, {
        NS: 'inter-window-mediator:'
    });

    return InterWindowMediator;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/inter-window-mediator/inter-window-mediator.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/inter-window-mediator/inter-window-mediator.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/inter-window-mediator/inter-window-mediator.js");
    }
}
