<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/user_menu.html.twig */
class __TwigTemplate_51658c5159bf9885f53526c304caba24 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["togglerId"] = uniqid("dropdown-");
        // line 2
        echo "<button id=\"";
        echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
        echo "\"
        type=\"button\"
        class=\"dropdown-toggle";
        // line 4
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            echo " dropdown-toggle--no-caret";
        }
        echo "\"
        data-toggle=\"dropdown\"
        aria-label=\"";
        // line 6
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.menu.user_menu.label"), "html", null, true);
        echo "\" aria-haspopup=\"true\" aria-expanded=\"false\"
>
    ";
        // line 8
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 9
            echo "        <span class=\"fa-user-circle\" aria-hidden=\"true\"></span>
    ";
        } else {
            // line 11
            echo "        ";
            $context["user_name"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 11));
            // line 12
            echo "        ";
            echo twig_escape_filter($this->env, ($context["user_name"] ?? null), "html", null, true);
            echo "
        ";
            // line 13
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, true, false, 13), "avatar", [], "any", true, true, false, 13)) {
                // line 14
                echo "            ";
                $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroNavigation/Menu/user_menu.html.twig", 14)->display(twig_array_merge($context, ["file" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 15
($context["app"] ?? null), "user", [], "any", false, false, false, 15), "avatar", [], "any", false, false, false, 15), "filter" => "avatar_med", "img_attrs" => ["alt" =>                 // line 17
($context["user_name"] ?? null), "class" => "avatar"]]));
                // line 19
                echo "        ";
            }
            // line 20
            echo "    ";
        }
        // line 21
        echo "</button>
";
        // line 22
        $context["options"] = twig_array_merge(($context["options"] ?? null), ["togglerId" => ($context["togglerId"] ?? null)]);
        // line 23
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 24
            echo "    ";
            $this->loadTemplate("@OroNavigation/Menu/user_menu_mobile.html.twig", "@OroNavigation/Menu/user_menu.html.twig", 24)->display($context);
        } else {
            // line 26
            echo "    ";
            $this->loadTemplate("@OroNavigation/Menu/dropdown.html.twig", "@OroNavigation/Menu/user_menu.html.twig", 26)->display($context);
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/user_menu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 26,  91 => 24,  89 => 23,  87 => 22,  84 => 21,  81 => 20,  78 => 19,  76 => 17,  75 => 15,  73 => 14,  71 => 13,  66 => 12,  63 => 11,  59 => 9,  57 => 8,  52 => 6,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/user_menu.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/user_menu.html.twig");
    }
}
