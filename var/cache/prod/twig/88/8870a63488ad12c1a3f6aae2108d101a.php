<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/Datagrid/Property/recurrence.html.twig */
class __TwigTemplate_8728700a20b65a24063fa73b6467c92e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["originalEvent"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "parent"], "method", false, false, false, 1)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "parent"], "method", false, false, false, 1)) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getRootEntity", [], "method", false, false, false, 1)));
        // line 2
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["originalEvent"] ?? null), "recurringEvent", [], "any", false, false, false, 2)) {
            // line 3
            echo "    ";
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\CalendarBundle\Twig\RecurrenceExtension']->getRecurrenceTextValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["originalEvent"] ?? null), "recurringEvent", [], "any", false, false, false, 3), "recurrence", [], "any", false, false, false, 3)), "html", null, true);
            echo "
";
        } else {
            // line 5
            echo "    ";
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\CalendarBundle\Twig\RecurrenceExtension']->getRecurrenceTextValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["originalEvent"] ?? null), "recurrence", [], "any", false, false, false, 5)), "html", null, true);
            echo "
";
        }
        // line 7
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/Datagrid/Property/recurrence.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 7,  47 => 5,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/Datagrid/Property/recurrence.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/Datagrid/Property/recurrence.html.twig");
    }
}
