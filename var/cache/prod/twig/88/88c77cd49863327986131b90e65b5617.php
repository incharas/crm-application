<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntity/Entities/index.html.twig */
class __TwigTemplate_824ed55f7087c5bdb30a4126bd8d7853 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntity/Entities/index.html.twig", 2)->unwrap();
        // line 4
        $context["gridName"] = "custom-entity-grid";
        // line 6
        $context["params"] = ["class_name" => ($context["entity_class"] ?? null)];
        // line 8
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["plural_label"] ?? null));

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((        // line 10
array_key_exists("label", $context)) ? (_twig_default_filter(($context["label"] ?? null), "N/A")) : ("N/A")))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroEntity/Entities/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 12
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntity/Entities/index.html.twig", 13)->unwrap();
        // line 14
        echo "
    ";
        // line 15
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("CREATE", ("entity:" . ($context["entity_class"] ?? null)))) {
            // line 16
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entity_update", ["entityName" =>             // line 17
($context["entity_name"] ?? null)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(            // line 18
($context["label"] ?? null))]], 16, $context, $this->getSourceContext());
            // line 19
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntity/Entities/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 19,  73 => 18,  72 => 17,  70 => 16,  68 => 15,  65 => 14,  62 => 13,  58 => 12,  53 => 1,  51 => 10,  48 => 8,  46 => 6,  44 => 4,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntity/Entities/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Resources/views/Entities/index.html.twig");
    }
}
