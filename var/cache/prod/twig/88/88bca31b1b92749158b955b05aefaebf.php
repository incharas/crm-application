<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Default/error.html.twig */
class __TwigTemplate_560279c463c3587ac8cd67f0761a4313 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'bodyClass' => [$this, 'block_bodyClass'],
            'script' => [$this, 'block_script'],
            'content_side' => [$this, 'block_content_side'],
            'header' => [$this, 'block_header'],
            'right_panel' => [$this, 'block_right_panel'],
            'left_panel' => [$this, 'block_left_panel'],
            'before_content' => [$this, 'block_before_content'],
            'content' => [$this, 'block_content'],
            'footer' => [$this, 'block_footer'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/Default/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["titleTemplate" => "%code% - %status%", "params" => ["%code%" =>         // line 2
($context["status_code"] ?? null), "%status%" => ($context["status_text"] ?? null)], "force" => true]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/Default/index.html.twig", "@OroUI/Default/error.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <meta name=\"error\" content=\"";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? null), "html", null, true);
        echo "\">
    ";
        // line 5
        $this->displayParentBlock("head", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "error-page";
    }

    // line 9
    public function block_script($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 12
    public function block_content_side($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 15
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 18
    public function block_right_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 20
    public function block_left_panel($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 23
    public function block_before_content($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 26
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    <div class=\"error-page-wrapper\">
        <div class=\"error-page-main\">
            <div class=\"error-page-content\">
                ";
        // line 30
        if ($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeLogo()) {
            // line 31
            echo "                    <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->getDefaultPage(), "html", null, true);
            echo "\">
                        <img class=\"error-page-img\" alt=\"logo\" src=\"";
            // line 32
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeLogo()), "html", null, true);
            echo "\"/>
                    </a>
                ";
        }
        // line 35
        echo "                <h1 class=\"error-page-title\">";
        echo twig_escape_filter($this->env, ($context["status_code"] ?? null), "html", null, true);
        echo ". ";
        echo twig_escape_filter($this->env, ($context["status_text"] ?? null), "html", null, true);
        echo "</h1>
                <p class=\"error-page-description\">
                    ";
        // line 37
        if ((($context["status_code"] ?? null) == 404)) {
            // line 38
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("The page you requested could not be found. Please make sure the path you used is correct."), "html", null, true);
            echo "
                    ";
        } elseif ((        // line 39
($context["status_code"] ?? null) == 403)) {
            // line 40
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("You don't have permission to access this page."), "html", null, true);
            echo "
                    ";
        } elseif ((        // line 41
($context["status_code"] ?? null) == 410)) {
            // line 42
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("The link has expired."), "html", null, true);
            echo "
                    ";
        } else {
            // line 44
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("The System is currently under maintenance and should be available in a few minutes."), "html", null, true);
            echo "
                    ";
        }
        // line 46
        echo "                </p>
                ";
        // line 47
        if (((($context["status_code"] ?? null) == 404) || (($context["status_code"] ?? null) == 403))) {
            // line 48
            echo "                    <button class=\"btn btn-primary\" onclick=\"window.history.back(); return false;\">
                        ";
            // line 49
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Click to go back"), "html", null, true);
            echo "
                        <span class=\"fa-angle-right\"></span>
                    </button>
                ";
        }
        // line 53
        echo "            </div>
        </div>
        <footer class=\"error-page-footer\">
            <p>";
        // line 56
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.copyright", ["{{year}}" => twig_date_format_filter($this->env, "now", "Y")]), "html", null, true);
        echo "</p>
        </footer>
    </div>
    <script>
        document.getElementById('progressbar').style.display = 'none';
        document.getElementById('page').style.display = '';
        document.title = \"";
        // line 62
        echo twig_escape_filter($this->env, twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render(), "js"), "html", null, true);
        echo "\";
    </script>
";
    }

    // line 66
    public function block_footer($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    public function getTemplateName()
    {
        return "@OroUI/Default/error.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 66,  204 => 62,  195 => 56,  190 => 53,  183 => 49,  180 => 48,  178 => 47,  175 => 46,  169 => 44,  163 => 42,  161 => 41,  156 => 40,  154 => 39,  149 => 38,  147 => 37,  139 => 35,  133 => 32,  128 => 31,  126 => 30,  121 => 27,  117 => 26,  111 => 23,  105 => 20,  99 => 18,  93 => 15,  87 => 12,  81 => 9,  74 => 7,  68 => 5,  63 => 4,  59 => 3,  54 => 1,  52 => 2,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Default/error.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Default/error.html.twig");
    }
}
