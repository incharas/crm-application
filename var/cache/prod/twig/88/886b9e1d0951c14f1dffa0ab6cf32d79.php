<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCampaign/EmailCampaign/view.html.twig */
class __TwigTemplate_9f272ecff1eb294f66a406d5c1fd318e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroCampaign/EmailCampaign/view.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "N/A")) : ("N/A"))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroCampaign/EmailCampaign/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCampaign/EmailCampaign/view.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        if (($context["send_allowed"] ?? null)) {
            // line 10
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_campaign_send", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["entity"] ?? null), "id", [], "any", false, false, false, 11)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.send"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.send"), "iCss" => "fa-envelope"]], 10, $context, $this->getSourceContext());
            // line 15
            echo "
    ";
        }
        // line 17
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 18
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_campaign_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 19
($context["entity"] ?? null), "id", [], "any", false, false, false, 19)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.entity_label")]], 18, $context, $this->getSourceContext());
            // line 21
            echo "
    ";
        }
        // line 23
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_navButtons", $context)) ? (_twig_default_filter(($context["update_navButtons"] ?? null), "update_navButtons")) : ("update_navButtons")), ["entity" => ($context["entity"] ?? null)]);
    }

    // line 26
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 28
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_campaign_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 31
($context["entity"] ?? null), "name", [], "any", true, true, false, 31)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 31), "N/A")) : ("N/A"))];
        // line 33
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 36
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 37
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCampaign/EmailCampaign/view.html.twig", 37)->unwrap();
        // line 38
        echo "
    ";
        // line 39
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <span class=\"page-title__status\">
        ";
        // line 41
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "sent", [], "any", false, false, false, 41)) {
            // line 42
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.status.sent"), "enabled"], 42, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 44
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.status.not_sent"), "disabled"], 44, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 46
        echo "    </span>
";
    }

    // line 49
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 50
        echo "    ";
        ob_start(function () { return ''; });
        // line 51
        echo "        ";
        $this->loadTemplate("@OroCampaign/EmailCampaign/widget/view.html.twig", "@OroCampaign/EmailCampaign/view.html.twig", 51)->display($context);
        // line 52
        echo "    ";
        $context["campaignInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 54
        ob_start(function () { return ''; });
        // line 55
        if (($context["show_stats"] ?? null)) {
            // line 56
            echo "            ";
            $this->loadTemplate("@OroCampaign/EmailCampaign/widget/stats.html.twig", "@OroCampaign/EmailCampaign/view.html.twig", 56)->display($context);
            // line 57
            echo "        ";
        }
        $context["emailCampaignStatisticsData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 60
        ob_start(function () { return ''; });
        // line 61
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_email_campaign_status", $context)) ? (_twig_default_filter(($context["view_content_data_email_campaign_status"] ?? null), "view_content_data_email_campaign_status")) : ("view_content_data_email_campaign_status")), ["entity" => ($context["entity"] ?? null)]);
        $context["emailCampaignStatusData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 64
        ob_start(function () { return ''; });
        // line 65
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "marketingList", [], "any", false, false, false, 65))) {
            // line 66
            echo "            ";
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [(twig_constant("Oro\\Bundle\\MarketingListBundle\\Datagrid\\ConfigurationProvider::GRID_PREFIX") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 67
($context["entity"] ?? null), "marketingList", [], "any", false, false, false, 67), "id", [], "any", false, false, false, 67)), ["emailCampaign" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 69
($context["entity"] ?? null), "id", [], "any", false, false, false, 69)], ["cssClass" => "inner-grid"]], 66, $context, $this->getSourceContext());
            // line 74
            echo "
        ";
        }
        $context["listData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 78
        $context["generalSectionSubblocks"] = [0 => ["data" => [0 => ($context["campaignInformationWidget"] ?? null)]]];
        // line 79
        echo "    ";
        if ( !twig_test_empty(($context["emailCampaignStatisticsData"] ?? null))) {
            // line 80
            echo "        ";
            $context["generalSectionSubblocks"] = twig_array_merge(($context["generalSectionSubblocks"] ?? null), [0 => ["data" => [0 =>             // line 81
($context["emailCampaignStatisticsData"] ?? null)], "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaignstatistics.block.label")]]);
            // line 84
            echo "    ";
        }
        // line 85
        echo "
    ";
        // line 86
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.block.general"), "class" => "active", "subblocks" =>         // line 90
($context["generalSectionSubblocks"] ?? null)]];
        // line 93
        echo "    ";
        if ( !twig_test_empty(($context["emailCampaignStatusData"] ?? null))) {
            // line 94
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.block.status"), "subblocks" => [0 => ["data" => [0 =>             // line 97
($context["emailCampaignStatusData"] ?? null)]]]]]);
            // line 101
            echo "    ";
        }
        // line 102
        echo "    ";
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.campaign.emailcampaign.block.recipients"), "subblocks" => [0 => ["spanClass" => "empty", "data" => [0 =>         // line 107
($context["listData"] ?? null)]]]]]);
        // line 111
        echo "
    ";
        // line 112
        $context["id"] = "emailCampaignView";
        // line 113
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 114
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCampaign/EmailCampaign/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  224 => 114,  221 => 113,  219 => 112,  216 => 111,  214 => 107,  212 => 102,  209 => 101,  207 => 97,  205 => 94,  202 => 93,  200 => 90,  199 => 86,  196 => 85,  193 => 84,  191 => 81,  189 => 80,  186 => 79,  184 => 78,  179 => 74,  177 => 69,  176 => 67,  174 => 66,  172 => 65,  170 => 64,  167 => 61,  165 => 60,  161 => 57,  158 => 56,  156 => 55,  154 => 54,  151 => 52,  148 => 51,  145 => 50,  141 => 49,  136 => 46,  130 => 44,  124 => 42,  122 => 41,  117 => 39,  114 => 38,  111 => 37,  107 => 36,  100 => 33,  98 => 31,  97 => 28,  95 => 27,  91 => 26,  86 => 23,  82 => 21,  80 => 19,  78 => 18,  75 => 17,  71 => 15,  69 => 11,  67 => 10,  65 => 9,  62 => 8,  59 => 7,  55 => 6,  50 => 1,  48 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCampaign/EmailCampaign/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/CampaignBundle/Resources/views/EmailCampaign/view.html.twig");
    }
}
