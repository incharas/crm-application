<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAccount/Account/lifetimeValue.html.twig */
class __TwigTemplate_9cb40e75ef8440cabc030ab23d06e2bf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<li>
    <div class=\"label label-success orocrm-channel-lifetime-value-label\">
        ";
        // line 3
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.lifetime_value.label"), "html", null, true);
        echo ":
        <b>";
        // line 4
        echo $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatCurrency($this->extensions['Oro\Bundle\ChannelBundle\Twig\ChannelExtension']->getLifetimeValue(($context["entity"] ?? null)));
        echo "</b>
    </div>
</li>
";
    }

    public function getTemplateName()
    {
        return "@OroAccount/Account/lifetimeValue.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAccount/Account/lifetimeValue.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AccountBundle/Resources/views/Account/lifetimeValue.html.twig");
    }
}
