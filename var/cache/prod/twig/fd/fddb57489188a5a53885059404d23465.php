<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/loading-bar.scss */
class __TwigTemplate_f6f1f3f58f76128ed4189fe4768e4266 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:color';

\$loading-bar-display: none !default;
\$loading-bar-background: \$extra !default;
\$loading-bar-box-shadow: none !default;
\$loading-bar-border-radius: 3px !default;
\$loading-bar-position: absolute !default;
\$loading-bar-bottom: -2px !default;
\$loading-bar-left: 0 !default;
\$loading-bar-height: 2px !default;
\$loading-bar-transition: width 300ms ease-out, opacity 300ms linear !default;
\$loading-bar-animation-name: line-loader !default;
\$loading-bar-animation: \$loading-bar-animation-name 5s forwards !default;
\$loading-bar-overflow: hidden !default;

\$loading-bar-point-background: color.adjust(\$loading-bar-background, \$lightness: 15%) !default;
\$loading-bar-point-box-shadow: none !default;
\$loading-bar-point-width: 15% !default;
\$loading-bar-point-height: 100% !default;
\$loading-bar-point-position: absolute !default;
\$loading-bar-point-top: 0 !default;
\$loading-bar-point-animation-name: line-point !default;
\$loading-bar-point-animation: \$loading-bar-point-animation-name 800ms ease-in-out infinite !default;

\$loading-bar-block-process-position: absolute !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/loading-bar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/loading-bar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/loading-bar.scss");
    }
}
