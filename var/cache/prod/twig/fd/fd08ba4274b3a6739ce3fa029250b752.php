<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/widget/update.html.twig */
class __TwigTemplate_1227917f0785a21de62f42c5889cb4b5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/widget/update.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if ((array_key_exists("saved", $context) && ($context["saved"] ?? null))) {
            // line 4
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.controller.event.saved.message"), "triggerSuccess" => true, "remove" => true]];
            // line 11
            echo "
    ";
            // line 12
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
";
        } else {
            // line 14
            echo "    <div class=\"widget-content\">
        ";
            // line 15
            if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 15), "valid", [], "any", false, false, false, 15) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 15), "errors", [], "any", false, false, false, 15)))) {
                // line 16
                echo "            <div class=\"alert alert-error\" role=\"alert\">
                <div class=\"message\">
                    ";
                // line 18
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
                echo "
                </div>
            </div>
        ";
            }
            // line 22
            echo "
        ";
            // line 23
            $context["calendarEventDateRange"] = ["module" => "orocalendar/js/app/components/calendar-event-date-range-component", "name" => "calendar-event-date-range", "options" => ["nativeMode" => $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()]];
            // line 30
            echo "        <div class=\"form-container\">
            <form id=\"";
            // line 31
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 31), "id", [], "any", false, false, false, 31), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 31), "name", [], "any", false, false, false, 31), "html", null, true);
            echo "\" action=\"";
            echo twig_escape_filter($this->env, ($context["formAction"] ?? null), "html", null, true);
            echo "\" method=\"post\">
                <fieldset class=\"form form-horizontal\">
                    ";
            // line 33
            ob_start(function () { return ''; });
            // line 34
            echo "                        <div class=\"control-group-container\" ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["calendarEventDateRange"] ?? null)], 34, $context, $this->getSourceContext());
            echo ">
                            ";
            // line 35
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "start", [], "any", false, false, false, 35), 'row');
            echo "
                            ";
            // line 36
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "end", [], "any", false, false, false, 36), 'row');
            echo "
                            ";
            // line 37
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "allDay", [], "any", false, false, false, 37), 'row');
            echo "
                        </div>
                        ";
            // line 39
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recurrence", [], "any", true, true, false, 39)) {
                // line 40
                echo "                            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recurrence", [], "any", false, false, false, 40), 'row');
                echo "
                        ";
            }
            // line 42
            echo "                    ";
            $context["rightColumn"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 43
            echo "
                    ";
            // line 44
            ob_start(function () { return ''; });
            // line 45
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 45), 'row');
            echo "
                        ";
            // line 46
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 46), 'row', ["attr" => ["class" => "narrow-text-field"]]);
            echo "
                        ";
            // line 47
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "backgroundColor", [], "any", false, false, false, 47), 'row');
            echo "
                        ";
            // line 48
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendarUid", [], "any", true, true, false, 48)) {
                // line 49
                echo "                            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendarUid", [], "any", false, false, false, 49), 'row');
                echo "
                        ";
            }
            // line 51
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "attendees", [], "any", false, false, false, 51), 'row');
            echo "
                        ";
            // line 52
            if ((null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 52), "value", [], "any", false, false, false, 52), "recurrence", [], "any", false, false, false, 52))) {
                // line 53
                echo "                            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "reminders", [], "any", false, false, false, 53), 'row');
                echo "
                        ";
            } else {
                // line 55
                echo "                            ";
                Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "reminders", [], "any", false, false, false, 55), "setRendered", [], "any", false, false, false, 55);
                // line 56
                echo "                        ";
            }
            // line 57
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
                    ";
            $context["leftColumn"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 59
            echo "
                    <div class=\"span6 control-group-container\">
                        ";
            // line 61
            echo twig_escape_filter($this->env, ($context["leftColumn"] ?? null), "html", null, true);
            echo "
                    </div>
                    <div class=\"span6 control-group-container\">
                        ";
            // line 64
            echo twig_escape_filter($this->env, ($context["rightColumn"] ?? null), "html", null, true);
            echo "
                    </div>

                    <div class=\"widget-actions form-actions\">
                        <button class=\"btn\" type=\"reset\">";
            // line 68
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
            echo "</button>
                        <button class=\"btn btn-success\" type=\"submit\">";
            // line 69
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "html", null, true);
            echo "</button>
                    </div>
                </fieldset>
            </form>
            ";
            // line 73
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/widget/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 73,  191 => 69,  187 => 68,  180 => 64,  174 => 61,  170 => 59,  164 => 57,  161 => 56,  158 => 55,  152 => 53,  150 => 52,  145 => 51,  139 => 49,  137 => 48,  133 => 47,  129 => 46,  124 => 45,  122 => 44,  119 => 43,  116 => 42,  110 => 40,  108 => 39,  103 => 37,  99 => 36,  95 => 35,  90 => 34,  88 => 33,  79 => 31,  76 => 30,  74 => 23,  71 => 22,  64 => 18,  60 => 16,  58 => 15,  55 => 14,  50 => 12,  47 => 11,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/widget/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/widget/update.html.twig");
    }
}
