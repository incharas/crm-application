<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/ApplicationMenu/dotsMenu.html.twig */
class __TwigTemplate_276f7c279ecdb45c38c8797b65be7981 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo $this->extensions['Oro\Bundle\NavigationBundle\Twig\MenuExtension']->render("dots_menu");
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/ApplicationMenu/dotsMenu.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/ApplicationMenu/dotsMenu.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/ApplicationMenu/dotsMenu.html.twig");
    }
}
