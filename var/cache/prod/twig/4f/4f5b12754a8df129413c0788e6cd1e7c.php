<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Config/view.html.twig */
class __TwigTemplate_6d6cc1db8fcbed2d9c696a1d501117d1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/view.html.twig", 2)->unwrap();
        // line 3
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroEntityConfig/Config/view.html.twig", 3)->unwrap();
        // line 4
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroEntityConfig/Config/view.html.twig", 4)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 5)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 5), "N/A")) : ("N/A")))]]);
        // line 7
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_entityconfig_manage")) {
            // line 8
            $context["audit_entity_class"] = twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "classname", [], "any", false, false, false, 8), ["\\" => "_"]);
            // line 9
            $context["audit_title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 9), ($context["entity_name"] ?? null))) : (($context["entity_name"] ?? null))));
            // line 10
            $context["audit_path"] = "oro_entityconfig_audit";
            // line 11
            $context["audit_show_change_history"] = true;
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroEntityConfig/Config/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 14
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/view.html.twig", 15)->unwrap();
        // line 16
        echo "
    ";
        // line 17
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_entityconfig_manage")) {
            // line 18
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["extend_config"] ?? null), "is", [0 => "is_extend"], "method", false, false, false, 18)) {
                // line 19
                echo "            ";
                $this->loadTemplate("@OroImportExport/ImportExport/buttons_from_configuration.html.twig", "@OroEntityConfig/Config/view.html.twig", 19)->display(twig_array_merge($context, ["alias" => "oro_field_config_model", "options" => ["entity_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 21
($context["entity"] ?? null), "id", [], "any", false, false, false, 21)]]));
                // line 23
                echo "        ";
            }
            // line 24
            echo "
        ";
            // line 25
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 25)])]], 25, $context, $this->getSourceContext());
            echo "
        ";
            // line 26
            echo twig_call_macro($macros["entityConfig"], "macro_displayLayoutActions", [($context["button_config"] ?? null)], 26, $context, $this->getSourceContext());
            echo "
    ";
        }
    }

    // line 30
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 32
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.entity.plural_label"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 35
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 35)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 35), ($context["entity_name"] ?? null))) : (($context["entity_name"] ?? null))))];
        // line 37
        echo "
    ";
        // line 38
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 41
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Config/view.html.twig", 42)->unwrap();
        // line 43
        echo "
    <li>";
        // line 44
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "created", [], "any", false, false, false, 44)), "html", null, true);
        echo "</li>
    <li>";
        // line 45
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updated", [], "any", false, false, false, 45)), "html", null, true);
        echo "</li>
    ";
        // line 46
        if (($context["link"] ?? null)) {
            // line 47
            echo "    <li>
        ";
            // line 48
            echo twig_call_macro($macros["UI"], "macro_link", [["path" =>             // line 49
($context["link"] ?? null), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.info.records_count.label", ["%count%" => ((            // line 50
array_key_exists("entity_count", $context)) ? (_twig_default_filter(($context["entity_count"] ?? null), 0)) : (0))])]], 48, $context, $this->getSourceContext());
            // line 51
            echo "
    </li>
    ";
        } else {
            // line 54
            echo "    <li>
        <span>";
            // line 55
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.info.records_count.label", ["%count%" => ((array_key_exists("entity_count", $context)) ? (_twig_default_filter(($context["entity_count"] ?? null), 0)) : (0))]), "html", null, true);
            echo "</span>
    </li>
    ";
        }
    }

    // line 60
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 61
        echo "    ";
        if (twig_length_filter($this->env, ($context["jsmodules"] ?? null))) {
            // line 62
            echo "        <script>
            loadModules(";
            // line 63
            echo json_encode(($context["jsmodules"] ?? null));
            echo ")
        </script>
    ";
        }
        // line 66
        echo "
    ";
        // line 67
        ob_start(function () { return ''; });
        // line 68
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_widget_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 70
($context["entity"] ?? null), "id", [], "any", false, false, false, 70)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.entity.information.label")]);
        // line 72
        echo "
    ";
        $context["entityInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 74
        echo "
    ";
        // line 75
        ob_start(function () { return ''; });
        // line 76
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_widget_unique_keys", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 78
($context["entity"] ?? null), "id", [], "any", false, false, false, 78)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.entity.unique.label")]);
        // line 80
        echo "
    ";
        $context["uniqueKeysWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 82
        echo "
    ";
        // line 83
        ob_start(function () { return ''; });
        // line 84
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_widget_entity_fields", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 86
($context["entity"] ?? null), "id", [], "any", false, false, false, 86)])]);
        // line 87
        echo "
    ";
        $context["entityFieldsWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 89
        echo "
    ";
        // line 90
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.block_titles.general.label"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 95
($context["entityInformationWidget"] ?? null)]], 1 => ["data" => [0 =>         // line 96
($context["uniqueKeysWidget"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_config.block_titles.fields.label"), "subblocks" => [0 => ["data" => [0 =>         // line 102
($context["entityFieldsWidget"] ?? null)]]]]];
        // line 106
        echo "
    ";
        // line 107
        $context["id"] = "entityConfigView";
        // line 108
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 109
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Config/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  242 => 109,  239 => 108,  237 => 107,  234 => 106,  232 => 102,  231 => 96,  230 => 95,  229 => 90,  226 => 89,  222 => 87,  220 => 86,  218 => 84,  216 => 83,  213 => 82,  209 => 80,  207 => 78,  205 => 76,  203 => 75,  200 => 74,  196 => 72,  194 => 70,  192 => 68,  190 => 67,  187 => 66,  181 => 63,  178 => 62,  175 => 61,  171 => 60,  163 => 55,  160 => 54,  155 => 51,  153 => 50,  152 => 49,  151 => 48,  148 => 47,  146 => 46,  140 => 45,  134 => 44,  131 => 43,  128 => 42,  124 => 41,  118 => 38,  115 => 37,  113 => 35,  112 => 32,  110 => 31,  106 => 30,  99 => 26,  95 => 25,  92 => 24,  89 => 23,  87 => 21,  85 => 19,  82 => 18,  80 => 17,  77 => 16,  74 => 15,  70 => 14,  65 => 1,  62 => 11,  60 => 10,  58 => 9,  56 => 8,  54 => 7,  52 => 5,  49 => 4,  47 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Config/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Config/view.html.twig");
    }
}
