<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContact/Contact/widget/accountContacts.html.twig */
class __TwigTemplate_899b18307c35b3177d73a3ec269921ac extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["account_contacts"] = $this->macros["account_contacts"] = $this;
        // line 2
        echo "
<div class=\"widget-content\">
    <div class=\"contact-box-wrapper\">
        ";
        // line 5
        if ((($context["defaultContact"] ?? null) || twig_length_filter($this->env, ($context["contactsWithoutDefault"] ?? null)))) {
            // line 6
            echo "            ";
            if (($context["defaultContact"] ?? null)) {
                // line 7
                echo "                ";
                echo twig_call_macro($macros["account_contacts"], "macro_render_contact_box", [($context["entity"] ?? null), ($context["defaultContact"] ?? null), true], 7, $context, $this->getSourceContext());
                echo "
            ";
            }
            // line 9
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["contactsWithoutDefault"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["contact"]) {
                // line 10
                echo "                ";
                echo twig_call_macro($macros["account_contacts"], "macro_render_contact_box", [($context["entity"] ?? null), $context["contact"], false], 10, $context, $this->getSourceContext());
                echo "
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['contact'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "        ";
        } else {
            // line 13
            echo "            <div class=\"no-data\">
                ";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.account.no_contacts_exist"), "html", null, true);
            echo "
            </div>
        ";
        }
        // line 17
        echo "    </div>
</div>

";
    }

    // line 20
    public function macro_render_contact_box($__account__ = null, $__contact__ = null, $__isDefault__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "account" => $__account__,
            "contact" => $__contact__,
            "isDefault" => $__isDefault__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 21
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContact/Contact/widget/accountContacts.html.twig", 21)->unwrap();
            // line 22
            echo "    ";
            $macros["Email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroContact/Contact/widget/accountContacts.html.twig", 22)->unwrap();
            // line 23
            echo "    <div class=\"contact-box\">
        <div class=\"contact-box-title\">
            <a href=\"";
            // line 25
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_contact_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contact"] ?? null), "id", [], "any", false, false, false, 25)]), "html", null, true);
            echo "\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["contact"] ?? null)), "html", null, true);
            echo "\" class=\"contact-box-name-link contact-box-link\" >";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["contact"] ?? null)), "html", null, true);
            // line 27
            if (($context["isDefault"] ?? null)) {
                // line 28
                echo "                    <span class=\"label label-info\">";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.default_contact.label"), "html", null, true);
                echo "</span>
                ";
            }
            // line 30
            echo "            </a>
        </div>
        <div class=\"contact-box-row\">
            <span class=\"contact-element-label\">";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.phone.label"), "html", null, true);
            echo ":</span>
            ";
            // line 34
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contact"] ?? null), "primaryPhone", [], "any", false, false, false, 34)) {
                // line 35
                echo "                ";
                echo twig_call_macro($macros["UI"], "macro_renderPhoneWithActions", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contact"] ?? null), "primaryPhone", [], "any", false, false, false, 35), ($context["contact"] ?? null)], 35, $context, $this->getSourceContext());
                echo "
            ";
            } else {
                // line 37
                echo "                ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
                echo "
            ";
            }
            // line 39
            echo "        </div>
        <div class=\"contact-box-row\">
            <span class=\"contact-element-label\">";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.account.email.label"), "html", null, true);
            echo ":</span>
            ";
            // line 42
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contact"] ?? null), "primaryEmail", [], "any", false, false, false, 42)) {
                // line 43
                echo "                ";
                echo twig_call_macro($macros["Email"], "macro_renderEmailWithActions", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["contact"] ?? null), "primaryEmail", [], "any", false, false, false, 43), "email", [], "any", false, false, false, 43), ($context["contact"] ?? null)], 43, $context, $this->getSourceContext());
                echo "
            ";
            } else {
                // line 45
                echo "                ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
                echo "
            ";
            }
            // line 47
            echo "        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroContact/Contact/widget/accountContacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  173 => 47,  167 => 45,  161 => 43,  159 => 42,  155 => 41,  151 => 39,  145 => 37,  139 => 35,  137 => 34,  133 => 33,  128 => 30,  122 => 28,  120 => 27,  118 => 26,  113 => 25,  109 => 23,  106 => 22,  103 => 21,  88 => 20,  81 => 17,  75 => 14,  72 => 13,  69 => 12,  60 => 10,  55 => 9,  49 => 7,  46 => 6,  44 => 5,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContact/Contact/widget/accountContacts.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactBundle/Resources/views/Contact/widget/accountContacts.html.twig");
    }
}
