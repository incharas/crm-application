<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables/page-header.scss */
class __TwigTemplate_0892863c6600ae4b68c76a47da6a2789 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$mobile-page-title-icon-offset: 47px !default;
\$mobile-page-title-icon-size: 40px !default;

\$page-title-btn-more-actions-icon: \$fa-var-ellipsis-h !default;
\$page-title-btn-more-actions-submittable-icon: \$fa-var-sort-down !default;
\$page-title-btn-more-actions-submittable-color: \$primary-inverse !default;
\$page-title-btn-more-actions-submittable-bg-color: \$success !default;
\$page-title-btn-more-actions-submittable-bg-color-active: \$success-dark !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables/page-header.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables/page-header.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables/page-header.scss");
    }
}
