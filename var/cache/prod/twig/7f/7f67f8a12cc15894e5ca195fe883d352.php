<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/delete-confirmation.js */
class __TwigTemplate_05e6206c252f419a37ff1eb9b2db9e08 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');
    const ModalView = require('oroui/js/modal');
    let config = require('module-config').default(module.id);

    config = Object.assign({}, {
        className: 'modal oro-modal-danger',
        okText: __('Yes, Delete'),
        title: __('Delete Confirmation'),
        cancelText: __('Cancel'),
        okButtonClass: 'btn btn-danger'
    }, config);

    /**
     * Delete confirmation dialog
     *
     * @export  oroui/js/delete-confirmation
     * @class   oroui.DeleteConfirmationView
     * @extends oroui.ModalView
     */
    const DeleteConfirmationView = ModalView.extend({
        /** @property {String} */
        className: config.className,

        /** @property {String} */
        okText: config.okText,

        /** @property {String} */
        title: config.title,

        /** @property {String} */
        cancelText: config.cancelText,

        okButtonClass: config.okButtonClass,

        _attributes: {
            role: 'alertdialog'
        },

        /**
         * @inheritdoc
         */
        constructor: function DeleteConfirmationView(options) {
            DeleteConfirmationView.__super__.constructor.call(this, options);
        },

        /**
         * @param {Object} options
         */
        initialize: function(options) {
            const fields = ['title', 'okText', 'okButtonClass', 'cancelText'];

            _.defaults(options, _.pick(DeleteConfirmationView.prototype, fields));
            DeleteConfirmationView.__super__.initialize.call(this, options);
        }
    });

    return DeleteConfirmationView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/delete-confirmation.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/delete-confirmation.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/delete-confirmation.js");
    }
}
