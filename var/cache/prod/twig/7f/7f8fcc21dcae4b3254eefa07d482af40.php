<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAction/Widget/widget/buttons.html.twig */
class __TwigTemplate_4d9900e0181bc3c93e41439ae5d56361 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAction/Widget/widget/buttons.html.twig", 1)->unwrap();
        // line 2
        if (twig_length_filter($this->env, ($context["buttons"] ?? null))) {
            // line 3
            echo "    ";
            $context["renderedButtons"] = [];
            // line 4
            echo "    ";
            $context["groups"] = [];
            // line 5
            echo "
    ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["buttons"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
                // line 7
                echo "        ";
                $context["groupName"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "group", [], "any", true, true, false, 7)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "group", [], "any", false, false, false, 7), null)) : (null));
                // line 8
                echo "        ";
                if (($context["groupName"] ?? null)) {
                    // line 9
                    echo "            ";
                    ob_start(function () { return ''; });
                    // line 10
                    echo "                <li>
                    ";
                    // line 11
                    $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "template", [], "any", false, false, false, 11), "@OroAction/Widget/widget/buttons.html.twig", 11)->display(twig_array_merge($context, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "getTemplateData", [0 => ["onlyLink" => true]], "method", false, false, false, 11)));
                    // line 12
                    echo "                </li>
            ";
                    $context["link"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                    // line 14
                    echo "
            ";
                    // line 15
                    $context["groups"] = twig_array_merge(($context["groups"] ?? null), [($context["groupName"] ?? null) => twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["groups"] ?? null), ($context["groupName"] ?? null), [], "array", true, true, false, 15)) ? (_twig_default_filter((($__internal_compile_0 = ($context["groups"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[($context["groupName"] ?? null)] ?? null) : null), [])) : ([])), [0 => ($context["link"] ?? null)])]);
                    // line 16
                    echo "        ";
                } else {
                    // line 17
                    echo "            ";
                    ob_start(function () { return ''; });
                    // line 18
                    echo "                ";
                    $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "template", [], "any", false, false, false, 18), "@OroAction/Widget/widget/buttons.html.twig", 18)->display(twig_array_merge($context, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "getTemplateData", [0 => ["aClass" => "btn action-button"]], "method", false, false, false, 18)));
                    // line 19
                    echo "            ";
                    $context["renderedButton"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                    // line 20
                    echo "            ";
                    $context["renderedButtons"] = twig_array_merge(($context["renderedButtons"] ?? null), [0 => ($context["renderedButton"] ?? null)]);
                    // line 21
                    echo "        ";
                }
                // line 22
                echo "    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 23
            echo "
    <div class=\"widget-content\">
        <div>
            ";
            // line 26
            if (twig_length_filter($this->env, ($context["renderedButtons"] ?? null))) {
                // line 27
                echo "                ";
                echo twig_join_filter(($context["renderedButtons"] ?? null));
                echo "
            ";
            }
            // line 29
            echo "            ";
            if (twig_length_filter($this->env, ($context["groups"] ?? null))) {
                // line 30
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["groups"] ?? null));
                foreach ($context['_seq'] as $context["groupName"] => $context["groupButtons"]) {
                    // line 31
                    echo "                    ";
                    if ($context["groupButtons"]) {
                        // line 32
                        echo "                        ";
                        echo twig_call_macro($macros["UI"], "macro_dropdownButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(                        // line 33
$context["groupName"]), "html" => twig_join_filter(                        // line 34
$context["groupButtons"])]], 32, $context, $this->getSourceContext());
                        // line 35
                        echo "
                    ";
                    }
                    // line 37
                    echo "                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['groupName'], $context['groupButtons'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 38
                echo "            ";
            }
            // line 39
            echo "        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroAction/Widget/widget/buttons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  164 => 39,  161 => 38,  155 => 37,  151 => 35,  149 => 34,  148 => 33,  146 => 32,  143 => 31,  138 => 30,  135 => 29,  129 => 27,  127 => 26,  122 => 23,  108 => 22,  105 => 21,  102 => 20,  99 => 19,  96 => 18,  93 => 17,  90 => 16,  88 => 15,  85 => 14,  81 => 12,  79 => 11,  76 => 10,  73 => 9,  70 => 8,  67 => 7,  50 => 6,  47 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAction/Widget/widget/buttons.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActionBundle/Resources/views/Widget/widget/buttons.html.twig");
    }
}
