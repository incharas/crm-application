<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/widget-picker/widget-picker-model.js */
class __TwigTemplate_ef8385ed32d1613a23db5fc9c6194cf4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseModel = require('oroui/js/app/models/base/model');
    const _ = require('underscore');

    const WidgetPickerModel = BaseModel.extend({
        defaults: {
            dialogIcon: '',
            title: '',
            widgetName: '',
            description: '',
            isNew: false,
            added: 0
        },

        /**
         * @inheritdoc
         */
        constructor: function WidgetPickerModel(attrs, options) {
            WidgetPickerModel.__super__.constructor.call(this, attrs, options);
        },

        /**
         *
         * @returns {String}
         */
        getName: function() {
            return this.get('widgetName');
        },

        /**
         * @returns {Array}
         */
        getData: function() {
            const attributes = _.clone(this.getAttributes());
            delete attributes.added;
            return attributes;
        },

        increaseAddedCounter: function() {
            this.set('added', this.get('added') + 1);
        }
    });

    return WidgetPickerModel;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/widget-picker/widget-picker-model.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/widget-picker/widget-picker-model.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/widget-picker/widget-picker-model.js");
    }
}
