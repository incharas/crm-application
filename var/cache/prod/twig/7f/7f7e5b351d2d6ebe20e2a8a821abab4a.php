<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/CalendarEvent/viewActions.html.twig */
class __TwigTemplate_8c36ce557c743aa85466e31ce69dba0b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["invites"] = [];
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "childAttendees", [], "any", false, false, false, 8));
        foreach ($context['_seq'] as $context["_key"] => $context["attendee"]) {
            // line 9
            echo "    ";
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attendee"], "statusCode", [], "any", false, false, false, 9) != twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED")) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attendee"], "email", [], "any", false, false, false, 9)))) {
                // line 10
                echo "        ";
                $context["invites"] = twig_array_merge(($context["invites"] ?? null), [0 => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 11
$context["attendee"], "email", [], "any", false, false, false, 11), "invite_type" => "EMAIL"]]);
                // line 14
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attendee'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 16
        echo "<div class=\"pull-left btn-group\">
    ";
        // line 17
        $this->loadTemplate("@OroHangoutsCall/startHangoutButton.html.twig", "@OroHangoutsCall/CalendarEvent/viewActions.html.twig", 17)->display($context);
        // line 18
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/CalendarEvent/viewActions.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 18,  60 => 17,  57 => 16,  50 => 14,  48 => 11,  46 => 10,  43 => 9,  39 => 8,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/CalendarEvent/viewActions.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/CalendarEvent/viewActions.html.twig");
    }
}
