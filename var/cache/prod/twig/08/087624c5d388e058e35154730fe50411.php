<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/app-ready-load-modules.js */
class __TwigTemplate_9cb98a75d2d6a754334171d48ad2f3a3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "const loadModules = require( 'oroui/js/app/services/load-modules');
const appReadyPromise = require( 'oroui/js/app');

module.exports = function appReadyLoadModules(modules, callback, context) {
    return appReadyPromise.then(function() {
        return loadModules(modules, callback, context);
    });
};
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/app-ready-load-modules.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/app-ready-load-modules.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/app-ready-load-modules.js");
    }
}
