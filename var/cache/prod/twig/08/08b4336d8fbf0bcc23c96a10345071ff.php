<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/select2-view.js */
class __TwigTemplate_bbd06f5a7eaa61774064cbde460f39e8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const BaseView = require('oroui/js/app/views/base/view');
    require('jquery.select2');

    const Select2View = BaseView.extend({
        events: {
            'change': 'onChange',
            'select2-data-loaded': 'onDataLoaded'
        },

        /**
         * Use for jQuery select2 plugin initialization
         */
        select2Config: {},

        autoRender: true,

        /**
         * @inheritdoc
         */
        constructor: function Select2View(options) {
            Select2View.__super__.constructor.call(this, options);
        },

        /**
         * @constructor
         *
         * @param {Object} options
         */
        initialize: function(options) {
            this.select2Config = _.result(options, 'select2Config') || _.extend({}, this.select2Config);

            const \$emptyOption = this.\$el.find('option[value=\"\"]');
            // \"Required\" attribute is not allowed for '<input type=\"hidden\">
            // such input might be used as the initializing element for Select2
            const notRequired = this.\$el.is(':hidden') ? !this.\$el.attr('x-required') : !this.\$el[0].required;

            if (this.select2Config.allowClear === undefined && (notRequired || \$emptyOption.length)) {
                this.select2Config.allowClear = true;
            }
            if (this.select2Config.allowClear && !this.select2Config.placeholderOption && \$emptyOption.length) {
                this.select2Config.placeholderOption = function() {
                    return \$emptyOption;
                };
            }
        },

        render: function() {
            this.undelegateEvents();
            this.\$el.prop('readonly', this.\$el.is('[readonly]'));
            this.\$el.inputWidget('create', 'select2', {
                initializeOptions: this.select2Config
            });
            if (this.select2Config.onAfterInit) {
                this.select2Config.onAfterInit(this.\$el.data('select2'));
            }
            this.delegateEvents();
        },

        dispose: function() {
            if (this.disposed) {
                return;
            }
            delete this.select2Config;
            this.\$el.inputWidget('dispose');
            Select2View.__super__.dispose.call(this);
        },

        onChange: function(e) {
            if (this.select2Config.multiple) {
                // to update size of container, e.g. dialog
                this.\$el.trigger('content:changed');
            }
        },

        onDataLoaded: function(e) {
            if (this.select2Config.multiple) {
                // to update size of container, e.g. dialog
                this.\$el.trigger('content:changed');
            }
        },

        reset: function() {
            this.setValue('');
        },

        getValue: function() {
            return this.\$el.inputWidget('val');
        },

        setValue: function(value) {
            this.\$el.inputWidget('val', value, true);
        },

        getData: function() {
            return this.\$el.inputWidget('data');
        },

        setData: function(data) {
            this.\$el.inputWidget('data', data);
        },

        refresh: function() {
            return this.\$el.inputWidget('refresh');
        }
    });

    return Select2View;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/select2-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/select2-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/select2-view.js");
    }
}
