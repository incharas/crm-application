<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMarketingActivity/MarketingActivity/widget/marketingActivitySectionItemInfo.html.twig */
class __TwigTemplate_dda868ad138772a0b1e18a82b7ad4659 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroMarketingActivity/MarketingActivity/widget/marketingActivitySectionItemInfo.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content form-horizontal box-content\">
    ";
        // line 4
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_content_data_marketing_activities_summary", $context)) ? (_twig_default_filter(($context["view_content_data_marketing_activities_summary"] ?? null), "view_content_data_marketing_activities_summary")) : ("view_content_data_marketing_activities_summary")), ["campaignId" => ($context["campaignId"] ?? null), "entityClass" => ($context["entityClass"] ?? null), "entityId" => ($context["entityId"] ?? null)]);
        // line 5
        echo "</div>
<div class=\"marketing-activities-section-data-grid\">
    <h5>";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketingactivity.entity_plural_label"), "html", null, true);
        echo "</h5>
    ";
        // line 8
        $context["scope"] = twig_replace_filter("marketing-activity-campaign-%campaign%", ["%campaign%" => ($context["campaignId"] ?? null)]);
        // line 9
        echo "    ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [$this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName("marketing-activity-section-data-grid",         // line 10
($context["scope"] ?? null)), ["id" =>         // line 11
($context["campaignId"] ?? null), "entityClass" => ($context["entityClass"] ?? null), "entityId" => ($context["entityId"] ?? null)]], 9, $context, $this->getSourceContext());
        // line 12
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroMarketingActivity/MarketingActivity/widget/marketingActivitySectionItemInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 12,  58 => 11,  57 => 10,  55 => 9,  53 => 8,  49 => 7,  45 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMarketingActivity/MarketingActivity/widget/marketingActivitySectionItemInfo.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/MarketingActivityBundle/Resources/views/MarketingActivity/widget/marketingActivitySectionItemInfo.html.twig");
    }
}
