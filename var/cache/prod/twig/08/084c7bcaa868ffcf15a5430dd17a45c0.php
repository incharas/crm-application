<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCron/Form/fields.html.twig */
class __TwigTemplate_6b88cd62f64348af9f2ff9c2eb31471a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_cron_schedule_intervals_collection_widget' => [$this, 'block_oro_cron_schedule_intervals_collection_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "
";
        // line 29
        echo "
";
        // line 30
        $this->displayBlock('oro_cron_schedule_intervals_collection_widget', $context, $blocks);
    }

    public function block_oro_cron_schedule_intervals_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "    ";
        $macros["cronFormFields"] = $this;
        // line 32
        echo "
    ";
        // line 33
        ob_start(function () { return ''; });
        // line 34
        echo "        ";
        if (array_key_exists("prototype", $context)) {
            // line 35
            echo "            ";
            $context["prototype_html"] = twig_call_macro($macros["cronFormFields"], "macro_schedule_interval_prototype", [($context["form"] ?? null)], 35, $context, $this->getSourceContext());
            // line 36
            echo "        ";
        }
        // line 37
        echo "        ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 37)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 37) . " ")) : ("")) . "oro-item-collection oro-item-collection__row--align-middle collection-fields-list")]);
        // line 38
        echo "        ";
        $context["id"] = (($context["id"] ?? null) . "_collection");
        // line 39
        echo "        <div class=\"row-oro schedule-intervals\"
             data-page-component-module=\"oroui/js/app/components/view-component\"
             data-page-component-options=\"";
        // line 41
        echo twig_escape_filter($this->env, json_encode(["view" => "orocron/js/app/views/schedule-intervals-view"]), "html", null, true);
        // line 43
        echo "\">
            ";
        // line 44
        $context["prototype_name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 44), "prototype_name", [], "any", false, false, false, 44);
        // line 45
        echo "            <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
                <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\">
                    <thead>
                    <tr>
                        <th><span>";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cron.schedule_interval.active_at.label"), "html", null, true);
        echo "</span></th>
                        <th><span>";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cron.schedule_interval.deactivate_at.label"), "html", null, true);
        echo "</span></th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody data-last-index=\"";
        // line 54
        echo twig_escape_filter($this->env, twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 54)), "html", null, true);
        echo "\" data-row-count-add=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 54), "row_count_add", [], "any", false, false, false, 54), "html", null, true);
        echo "\" data-prototype-name=\"";
        echo twig_escape_filter($this->env, ($context["prototype_name"] ?? null), "html", null, true);
        echo "\"";
        if (array_key_exists("prototype_html", $context)) {
            echo " data-prototype=\"";
            echo twig_escape_filter($this->env, ($context["prototype_html"] ?? null));
            echo "\"";
        }
        echo ">
                    ";
        // line 55
        if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 55))) {
            // line 56
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 56));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 57
                echo "                            ";
                echo twig_call_macro($macros["cronFormFields"], "macro_schedule_interval_prototype", [$context["child"]], 57, $context, $this->getSourceContext());
                echo "
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 59
            echo "                    ";
        } elseif ((($context["show_form_when_empty"] ?? null) && array_key_exists("prototype_html", $context))) {
            // line 60
            echo "                        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 60), "row_count_initial", [], "any", false, false, false, 60) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 61
                echo "                            ";
                echo twig_replace_filter(($context["prototype_html"] ?? null), [($context["prototype_name"] ?? null) => $context["i"]]);
                echo "
                        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 63
            echo "                    ";
        }
        // line 64
        echo "                    </tbody>
                </table>
            </div>
            <div class=\"btn-container\">
                <button type=\"button\" class=\"btn add-list-item\" data-container=\".oro-item-collection tbody\"><i class=\"fa-plus\"></i>";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cron.schedule_interval.add"), "html", null, true);
        echo "</button>
            </div>
        </div>
        ";
        // line 71
        if ((($context["handle_primary"] ?? null) && ( !array_key_exists("prototype", $context) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["prototype"] ?? null), "primary", [], "any", true, true, false, 71)))) {
            // line 72
            echo "            ";
            echo twig_call_macro($macros["cronFormFields"], "macro_oro_collection_validate_primary_js", [$context], 72, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 74
        echo "    ";
        $___internal_parse_49_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 33
        echo twig_spaceless($___internal_parse_49_);
    }

    // line 3
    public function macro_schedule_interval_prototype($__widget__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 4
            echo "    ";
            if (twig_in_filter("collection", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 4), "block_prefixes", [], "any", false, false, false, 4))) {
                // line 5
                echo "        ";
                $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 5), "prototype", [], "any", false, false, false, 5);
                // line 6
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 6), "prototype", [], "any", false, false, false, 6), "vars", [], "any", false, false, false, 6), "name", [], "any", false, false, false, 6);
                // line 7
                echo "    ";
            } else {
                // line 8
                echo "        ";
                $context["form"] = ($context["widget"] ?? null);
                // line 9
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 9), "full_name", [], "any", false, false, false, 9);
                // line 10
                echo "    ";
            }
            // line 11
            echo "
    ";
            // line 12
            $context["hasErrors"] = (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "activeAt", [], "any", false, false, false, 12), "vars", [], "any", false, false, false, 12), "errors", [], "any", false, false, false, 12)) || twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "deactivateAt", [], "any", false, false, false, 12), "vars", [], "any", false, false, false, 12), "errors", [], "any", false, false, false, 12)));
            // line 13
            echo "
    <tr data-content=\"";
            // line 14
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\" data-validation-optional-group class=\"schedule-interval__row ";
            if (($context["hasErrors"] ?? null)) {
                echo "has-row-error";
            }
            echo "\" data-role=\"schedule-interval-row\">
        <td>";
            // line 15
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "activeAt", [], "any", false, false, false, 15), 'widget');
            echo "</td>
        <td>";
            // line 16
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "deactivateAt", [], "any", false, false, false, 16), 'widget');
            echo "</td>
        <td>
            <button type=\"button\" class=\"removeRow btn btn-icon btn-square-lighter\" aria-label=\"";
            // line 18
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Remove row"), "html", null, true);
            echo "\"><span class=\"fa-trash-o\" aria-hidden=\"true\"></span></button>
        </td>
    </tr>
    <tr ";
            // line 21
            if ( !($context["hasErrors"] ?? null)) {
                echo "style=\"display: none\"";
            }
            echo " class=\"schedule-interval__error-row\" data-role=\"schedule-interval-row-error\">
        <td colspan=\"3\">
            ";
            // line 23
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
            ";
            // line 24
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "activeAt", [], "any", false, false, false, 24), 'errors');
            echo "
            ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "deactivateAt", [], "any", false, false, false, 25), 'errors');
            echo "
        </td>
    </tr>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroCron/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  261 => 25,  257 => 24,  253 => 23,  246 => 21,  240 => 18,  235 => 16,  231 => 15,  223 => 14,  220 => 13,  218 => 12,  215 => 11,  212 => 10,  209 => 9,  206 => 8,  203 => 7,  200 => 6,  197 => 5,  194 => 4,  181 => 3,  177 => 33,  174 => 74,  168 => 72,  166 => 71,  160 => 68,  154 => 64,  151 => 63,  142 => 61,  137 => 60,  134 => 59,  125 => 57,  120 => 56,  118 => 55,  104 => 54,  97 => 50,  93 => 49,  85 => 45,  83 => 44,  80 => 43,  78 => 41,  74 => 39,  71 => 38,  68 => 37,  65 => 36,  62 => 35,  59 => 34,  57 => 33,  54 => 32,  51 => 31,  44 => 30,  41 => 29,  38 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCron/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CronBundle/Resources/views/Form/fields.html.twig");
    }
}
