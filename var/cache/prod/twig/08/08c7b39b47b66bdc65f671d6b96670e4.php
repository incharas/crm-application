<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/form-selectors.scss */
class __TwigTemplate_d9d03dcbd0ce2065776d83e6f7fd2b67 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$oro-form-selectors: (
    'inputs': (
        'textarea',
        'input[type=\"text\"]',
        'input[type=\"password\"]',
        'input[type=\"datetime\"]',
        'input[type=\"datetime-local\"]',
        'input[type=\"date\"]',
        'input[type=\"month\"]',
        'input[type=\"time\"]',
        'input[type=\"week\"]',
        'input[type=\"number\"]',
        'input[type=\"email\"]',
        'input[type=\"url\"]',
        'input[type=\"search\"]',
        'input[type=\"tel\"]',
        'input[type=\"color\"]',
        '.uneditable-input',
        '.selector'
    ),
    'select2-append': '.input-append .select2-container',
    'select2-prepend': '.input-prepend .select2-container',
    'select2-add-entity-enabled': '.entity-create-enabled .select2-container',
);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/form-selectors.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/form-selectors.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/form-selectors.scss");
    }
}
