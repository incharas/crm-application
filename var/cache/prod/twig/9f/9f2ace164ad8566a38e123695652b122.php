<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/functions/px2rem.scss */
class __TwigTemplate_9ca6a7084e53ddf05ff4379de1bd5307 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:math';

// Example:
// font-size: px2rem(14px);
@function px2rem(\$size) {
    // Default font size on html element is 100%, equivalent to 16px;
    @return math.div(\$size, 16px) * 1rem;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/functions/px2rem.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/functions/px2rem.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/functions/px2rem.scss");
    }
}
