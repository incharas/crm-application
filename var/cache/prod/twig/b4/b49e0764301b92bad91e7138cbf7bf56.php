<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/index.html.twig */
class __TwigTemplate_5efe6115ceaf63bd4bf4cf9637a7b221 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'content_datagrid' => [$this, 'block_content_datagrid'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/index.html.twig", 2)->unwrap();
        // line 3
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroOAuth2Server/Client/index.html.twig", 3)->unwrap();
        // line 5
        if (($context["isFrontend"] ?? null)) {
            // line 6
            $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.menu.frontend_oauth_application.label");
        } else {
            // line 8
            $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.menu.backoffice_oauth_application.label");
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroOAuth2Server/Client/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_oauth2_create")) {
            // line 13
            echo "    <div class=\"btn-group\">
        ";
            // line 14
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((            // line 15
($context["isFrontend"] ?? null)) ? ("oro_oauth2_frontend_create") : ("oro_oauth2_create"))), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.entity_label")]], 14, $context, $this->getSourceContext());
            // line 17
            echo "
    </div>
    ";
        }
    }

    // line 22
    public function block_content_datagrid($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "    <div data-bound-component=\"orodatagrid/js/app/components/datagrid-component\">
        ";
        // line 24
        if ( !($context["encryptionKeysExist"] ?? null)) {
            // line 25
            echo "            <div class=\"alert alert-warning\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.no_encryption_keys"), "html", null, true);
            echo "</div>
        ";
        }
        // line 27
        echo "        ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [((        // line 28
($context["isFrontend"] ?? null)) ? ("oauth-client-frontend-grid") : ("oauth-client-backend-grid")), ["frontend" =>         // line 29
($context["isFrontend"] ?? null), "_grid_view" => ["_disabled" => true]], ["cssClass" => "inner-grid"]], 27, $context, $this->getSourceContext());
        // line 31
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 31,  98 => 29,  97 => 28,  95 => 27,  89 => 25,  87 => 24,  84 => 23,  80 => 22,  73 => 17,  71 => 15,  70 => 14,  67 => 13,  64 => 12,  60 => 11,  55 => 1,  52 => 8,  49 => 6,  47 => 5,  45 => 3,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/index.html.twig");
    }
}
