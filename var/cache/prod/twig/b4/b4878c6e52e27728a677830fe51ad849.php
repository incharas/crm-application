<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/widget/info.html.twig */
class __TwigTemplate_19e2bf0c248fe2e5687fa259f4383455 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCalendar/CalendarEvent/widget/info.html.twig", 2)->unwrap();
        // line 3
        echo "
<div class=\"widget-content form-horizontal box-content row-fluid\">
    <div class=\"responsive-block\">
        ";
        // line 7
        echo "        ";
        if ((array_key_exists("renderContexts", $context) && ($context["renderContexts"] ?? null))) {
            // line 8
            echo "            <div class=\"activity-context-activity-list\">
                ";
            // line 9
            echo twig_call_macro($macros["AC"], "macro_activity_contexts", [($context["entity"] ?? null), ($context["target"] ?? null), true], 9, $context, $this->getSourceContext());
            echo "
            </div>
        ";
        }
        // line 12
        echo "        ";
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.title.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 12)], 12, $context, $this->getSourceContext());
        echo "
        ";
        // line 13
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 13)], 13, $context, $this->getSourceContext());
        echo "
        ";
        // line 14
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.start.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "start", [], "any", false, false, false, 14))], 14, $context, $this->getSourceContext());
        echo "
        ";
        // line 15
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.end.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "end", [], "any", false, false, false, 15))], 15, $context, $this->getSourceContext());
        echo "
        ";
        // line 16
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.all_day.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "allDay", [], "any", false, false, false, 16)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No")))], 16, $context, $this->getSourceContext());
        echo "
        ";
        // line 17
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurrence", [], "any", false, false, false, 17)) {
            // line 18
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.recurrence.label"), $this->extensions['Oro\Bundle\CalendarBundle\Twig\RecurrenceExtension']->getRecurrenceTextValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurrence", [], "any", false, false, false, 18))], 18, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 20
        echo "        ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurringEvent", [], "any", false, false, false, 20)) {
            // line 21
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.recurrence.exception.label"), $this->extensions['Oro\Bundle\CalendarBundle\Twig\RecurrenceExtension']->getRecurrenceTextValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurringEvent", [], "any", false, false, false, 21), "recurrence", [], "any", false, false, false, 21))], 21, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 23
        echo "        ";
        if (($context["canChangeInvitationStatus"] ?? null)) {
            // line 24
            echo "            ";
            $this->loadTemplate("@OroCalendar/CalendarEvent/invitationControl.html.twig", "@OroCalendar/CalendarEvent/widget/info.html.twig", 24)->display(twig_array_merge($context, ["entity" => ($context["entity"] ?? null), "triggerEventName" => "widget:doRefresh:activity-list-widget"]));
            // line 25
            echo "        ";
        }
        // line 26
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 26,  102 => 25,  99 => 24,  96 => 23,  90 => 21,  87 => 20,  81 => 18,  79 => 17,  75 => 16,  71 => 15,  67 => 14,  63 => 13,  58 => 12,  52 => 9,  49 => 8,  46 => 7,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/widget/info.html.twig");
    }
}
