<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/load-modules.js */
class __TwigTemplate_5e175a2394db1021312708f9b5c81436 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "const modules = require('dynamic-imports');

function pick(object, keys) {
    return keys.reduce((obj, key) => {
        if (object && object.hasOwnProperty(key)) {
            obj[key] = object[key];
        }
        return obj;
    }, {});
}

function loadModule(name, ...values) {
    if (!modules[name]) {
        throw new Error(`Module \"\${name}\" is not found in the list of modules used for dynamic-imports`);
    }
    return modules[name]().then(module =>
        values.length === 0 ? module.default : pick(module, values));
}

/**
 * Loads dynamic list of modules and execute callback function with passed modules
 *
 * @param {Object.<string, string>|Array.<string>|string} modules
 *  - Object: where keys are formal module names and values are actual
 *  - Array: module names,
 *  - string: single module name
 * @param {function(Object)=} callback
 * @param {Object=} context
 * @return {Promise}
 */
function loadModules(modules, callback, context) {
    let requirements;
    let processModules;
    const isModulesArray = Array.isArray(modules);

    if (isModulesArray) {
        requirements = modules;
        processModules = loadedModules => loadedModules;
    } else if (typeof modules === 'string') {
        requirements = [modules];
        processModules = loadedModules => loadedModules[0];
    } else {
        // if modules is an object of {formalName: moduleName}
        requirements = Object.values(modules);
        processModules = loadedModules => {
            // maps loaded modules into original object
            Object.keys(modules)
                .forEach((formalName, index) => modules[formalName] = loadedModules[index] || modules[formalName]);
            return modules;
        };
    }

    return Promise.all(requirements.map(moduleName => loadModule(moduleName)))
        .then(modules => {
            modules = processModules(modules);
            if (callback) {
                callback[isModulesArray ? 'apply' : 'call'](context || null, modules);
            }
            // promise can't be resolved a with multiple values
            return modules;
        });
}

/**
 * Loads module from object's property
 *
 * @param {Object} obj
 * @param {string} prop name with a module name
 * @return {Promise}
 */
loadModules.fromObjectProp = function(obj, prop) {
    if (typeof obj[prop] !== 'string') {
        return new Promise(resolve => resolve(obj[prop]));
    }
    return loadModules(obj[prop])
        .then(module => obj[prop] = module);
};

module.exports = loadModules;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/load-modules.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/load-modules.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/load-modules.js");
    }
}
