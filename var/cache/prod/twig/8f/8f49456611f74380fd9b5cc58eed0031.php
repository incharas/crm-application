<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/widget/buttons-widget.js */
class __TwigTemplate_3f225b99afd82cdfb76cb13c307f1c0b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(['underscore', 'backbone', 'oroui/js/widget/abstract-widget'
], function(_, Backbone, AbstractWidgetView) {
    'use strict';

    const \$ = Backbone.\$;

    /**
     * @export  oro/buttons-widget
     * @class   oro.ButtonsWidgetView
     * @extends oroui.widget.AbstractWidgetView
     */
    const ButtonsWidgetView = AbstractWidgetView.extend({
        options: _.extend(
            _.extend({}, AbstractWidgetView.prototype.options),
            {
                cssClass: 'pull-left icons-holder',
                type: 'buttons',
                loadingMaskEnabled: false
            }
        ),

        /**
         * @inheritdoc
         */
        constructor: function ButtonsWidgetView(options) {
            ButtonsWidgetView.__super__.constructor.call(this, options);
        },

        initialize: function(options) {
            options = options || {};
            this.options = _.defaults(options, this.options);

            this.widget = this.\$el;
            this.widget.addClass(this.options.cssClass);

            this.initializeWidget(options);
        },

        setTitle: function(title) {
            this.widget.attr('title', title);
        },

        getActionsElement: function() {
            return null;
        },

        show: function() {
            if (!this.\$el.data('wid')) {
                if (this.\$el.parent().length) {
                    this._showStatic();
                } else {
                    this._showRemote();
                }
            }
            AbstractWidgetView.prototype.show.call(this);
        },

        _showStatic: function() {
            const anchorId = '_widget_anchor-' + this.getWid();
            const anchorDiv = \$('<div id=\"' + anchorId + '\"/>');
            const parent = this.widget.parent();
            anchorDiv.insertAfter(parent);
            \$('#' + anchorId).replaceWith(\$(this.widget));
            parent.remove();
        },

        _showRemote: function() {
            this.widget.empty();
            this.widget.append(this.\$el);
            this.setElement(this.widget);
        }
    });

    return ButtonsWidgetView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/widget/buttons-widget.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/widget/buttons-widget.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/widget/buttons-widget.js");
    }
}
