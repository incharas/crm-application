<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroConfig/Configuration/system.html.twig */
class __TwigTemplate_cb5ebae0d6cbd37f92258db759ea789c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroConfig/configPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.config.menu.system_configuration.label");
        // line 4
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_config_configuration_system", ["activeGroup" =>         // line 6
($context["activeGroup"] ?? null), "activeSubGroup" => ($context["activeSubGroup"] ?? null)]);
        // line 9
        $context["routeName"] = "oro_config_configuration_system";
        // line 10
        $context["routeParameters"] = [];
        // line 1
        $this->parent = $this->loadTemplate("@OroConfig/configPage.html.twig", "@OroConfig/Configuration/system.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroConfig/Configuration/system.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 1,  48 => 10,  46 => 9,  44 => 6,  43 => 4,  41 => 3,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroConfig/Configuration/system.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Resources/views/Configuration/system.html.twig");
    }
}
