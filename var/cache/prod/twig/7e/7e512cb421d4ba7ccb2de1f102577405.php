<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityExtend/Form/fields.html.twig */
class __TwigTemplate_aaae2563846ffcbea5a17ea0e12e0760 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_entity_extend_enum_value_widget' => [$this, 'block_oro_entity_extend_enum_value_widget'],
            'oro_entity_extend_enum_value_collection_widget' => [$this, 'block_oro_entity_extend_enum_value_collection_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_entity_extend_enum_value_widget', $context, $blocks);
        // line 19
        echo "
";
        // line 20
        $this->displayBlock('oro_entity_extend_enum_value_collection_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_entity_extend_enum_value_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <div class=\"float-holder ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 2), "vars", [], "any", false, false, false, 2), "errors", [], "any", false, false, false, 2)) > 0)) {
            echo " validation-error";
        }
        echo "\">
        <div class=\"input-append input-append-sortable collection-element-primary\">
            ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 4), 'widget', ["disabled" => ($context["disabled"] ?? null)]);
        echo "
            <span class=\"add-on ui-sortable-handle";
        // line 5
        if (($context["disabled"] ?? null)) {
            echo " disabled";
        }
        echo "\"
                  data-name=\"sortable-handle\"
                  title=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.enum_options.priority.tooltip"), "html", null, true);
        echo "\">
                <i class=\"fa-arrows-v ";
        // line 8
        if (($context["disabled"] ?? null)) {
            echo " disabled";
        }
        echo "\"></i>
                ";
        // line 9
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "priority", [], "any", false, false, false, 9), 'widget', ["disabled" => ($context["disabled"] ?? null)]);
        echo "
            </span>
            <label class=\"add-on";
        // line 11
        if (($context["disabled"] ?? null)) {
            echo " disabled";
        }
        echo "\" title=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.enum_options.default.tooltip"), "html", null, true);
        echo "\">
                ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "is_default", [], "any", false, false, false, 12), 'widget', ["disabled" => ($context["disabled"] ?? null)]);
        echo "
            </label>
        </div>
        ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 15), 'errors');
        echo "
    </div>
    ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
";
    }

    // line 20
    public function block_oro_entity_extend_enum_value_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityExtend/Form/fields.html.twig", 21)->unwrap();
        // line 22
        echo "    ";
        if ( !($context["disabled"] ?? null)) {
            // line 23
            echo "        <div class=\"enum-value-collection\" ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["view" => "oroentityextend/js/app/views/enum-values-view", "multiple" =>             // line 27
($context["multiple"] ?? null), "autoRender" => true]]], 23, $context, $this->getSourceContext());
            // line 30
            echo ">
            ";
            // line 31
            $this->displayBlock("oro_collection_widget", $context, $blocks);
            echo "
        </div>
    ";
        } else {
            // line 34
            echo "        ";
            $this->displayBlock("oro_collection_widget", $context, $blocks);
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityExtend/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  134 => 34,  128 => 31,  125 => 30,  123 => 27,  121 => 23,  118 => 22,  115 => 21,  111 => 20,  105 => 17,  100 => 15,  94 => 12,  86 => 11,  81 => 9,  75 => 8,  71 => 7,  64 => 5,  60 => 4,  52 => 2,  48 => 1,  44 => 20,  41 => 19,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityExtend/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityExtendBundle/Resources/views/Form/fields.html.twig");
    }
}
