<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/variables/input-config.scss */
class __TwigTemplate_01c912be2f6929008d1764fa6522ed29 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@use 'sass:color';

\$use-base-style-for-input: true !default;

// Default
\$input-width-short: 64px !default;
\$input-padding: 9px \$input-padding-y 8px !default;
\$input-padding--m: 8px 9px 7px !default;
\$input-padding--s: 5px 9px 4px !default;
\$input-padding--x-s: 4px 9px 3px !default;
\$input-date-height: 40px !default;
\$input-date-size-s-height: 32px !default;
\$input-date-height: 35px !default;
\$input-number-box-shadow: none !default;
\$input-font-size: \$base-ui-element-font-size !default;
\$input-font-family: \$base-ui-element-font-family !default;
\$input-line-height: \$base-ui-element-line-height !default;
\$input-border: \$base-ui-element-border !default;
\$input-border-radius: \$base-ui-element-border-radius !default;
\$input-background-color: \$base-ui-element-bg-color !default;
\$input-color: \$base-ui-element-color !default;

\$input-time-placeholder-color: get-color('additional', 'middle') !default;

// Hover
\$input-border-color-hover-state: get-color('additional', 'dark') !default;
\$input-box-shadow-hover-state:
    inset 0 1px 1px color.scale(get-color('additional', 'ultra'), \$alpha: -75%),
    0 0 8px color.scale(get-color('ui', 'focus'), \$alpha: -60%);

// Focus
\$input-border-color-focus-state: \$base-ui-element-border-color-focus !default;
\$input-box-shadow-focus-state: \$base-ui-element-focus-style !default;

// Error
\$input-border-color-error-state: get-color('ui', 'error-dark') !default;
\$input-box-shadow-error-state: 0 1px 7px 0 color.scale(get-color('ui', 'error-dark'), \$alpha: -60%);

// Disabled
\$input-border-color-disabled-background: get-color('additional', 'base') !default;
\$input-border-color-disabled-hover-border-color: get-color('additional', 'light') !default;

// Error
\$input-border-color-error-state: get-color('ui', 'error-dark');
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/variables/input-config.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/variables/input-config.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/variables/input-config.scss");
    }
}
