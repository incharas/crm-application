<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/base/page-region-view.js */
class __TwigTemplate_8d749e6802b5d9683410cfff01c16c4a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'underscore',
    './view',
    'oroui/js/mediator'
], function(_, BaseView, mediator) {
    'use strict';

    const PageRegionView = BaseView.extend({
        listen: {
            'page:update mediator': 'onPageUpdate'
        },

        data: null,

        pageItems: [],

        /**
         * Defer object,
         * helps to notify environment that the view has updated its content
         */
        deferredRender: null,

        /**
         * @inheritdoc
         */
        constructor: function PageRegionView(options) {
            PageRegionView.__super__.constructor.call(this, options);
        },

        /**
         * Handles page load event
         *  - stores from page data corresponded page items
         *  - renders view
         *  - dispose cached data
         *
         * @param {Object} pageData
         * @param {Object} actionArgs arguments of controller's action point
         * @param {Object} jqXHR
         * @param {Array.<Object>} promises collection
         */
        onPageUpdate: function(pageData, actionArgs, jqXHR, promises) {
            this.data = _.pick(pageData, this.pageItems);
            this.actionArgs = actionArgs;
            this.disposePageComponents();
            this.render();
            this.data = null;
            this.actionArgs = null;
            if (this.deferredRender) {
                // collects initialization promises
                promises.push(this.getDeferredRenderPromise());
            }
        },

        /**
         * Renders the view
         *  - prevents rendering a view without page data
         *  - disposes old content before rendering
         *  - initializes page components
         *
         * @override
         */
        render: function() {
            const data = this.getTemplateData();

            if (!data) {
                // no data, it is initial auto render, skip rendering
                return this;
            } else if (!_.isEmpty(data)) {
                // data object is not empty, dispose old content and render new
                this.disposePageComponents();
                mediator.execute('layout:dispose', this.\$el);
                PageRegionView.__super__.render.call(this);
            }

            return this;
        },

        /**
         * Gets cached page data
         *
         * @returns {Object}
         * @override
         */
        getTemplateData: function() {
            return this.data;
        }
    });

    return PageRegionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/base/page-region-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/base/page-region-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/base/page-region-view.js");
    }
}
