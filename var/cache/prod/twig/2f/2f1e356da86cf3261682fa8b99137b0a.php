<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/print/page.scss */
class __TwigTemplate_9e2d049a6f0f3c7e3ad8708da025ab90 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

a,
a:visited {
    // stylelint-disable-next-line declaration-no-important
    text-decoration: underline !important;
}

a[href]::after {
    content: '';
}

a[class~='icons-holder-text'],
a[class~='icons-holder-text']:visited,
a[class~='accordion-toggle'],
a[class~='accordion-toggle']:visited {
    // stylelint-disable-next-line declaration-no-important
    text-decoration: none !important;
}

.widget-actions-container {
    white-space: nowrap;
}

.filter-box {
    padding-left: 20px;

    .filter-list {
        display: none;
    }

    .filter-item {
        a,
        a:visited {
            // stylelint-disable-next-line declaration-no-important
            text-decoration: none !important;
        }
    }
}

.scrollable-container {
    // stylelint-disable-next-line declaration-no-important
    height: auto !important;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/print/page.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/print/page.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/print/page.scss");
    }
}
