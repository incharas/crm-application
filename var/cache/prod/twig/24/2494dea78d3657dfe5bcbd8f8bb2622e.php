<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/entity.scss */
class __TwigTemplate_f75297000a725c189125314c990f775a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.entity-create-or-select-container {
    &.entity-create-multi-enabled {
        .dropdown-menu {
            li button {
                padding: 2px 12px;
                display: block;
                clear: both;
                font-weight: font-weight('light');
                text-align: left;
                white-space: nowrap;
                background-color: transparent;
                border: none;
                margin: 0;
                min-width: 100%;
                appearance: none;
            }
        }
    }

    .btn-group.entity-create-dropdown {
        display: inline-block;
        vertical-align: top;
        margin-left: \$content-padding-small * .5;
        border-radius: 0 4px 4px 0;

        > .btn {
            font-size: 14px;
        }
    }
}

.entity-field-path {
    > * {
        &::before {
            content: '/';
            display: inline;
            padding: 0 3px;
        }

        &:first-child {
            &::before {
                display: none;
            }
        }

        &:last-child {
            @include fa-icon(var(--fa-var-angle-right), before, true, true) {
                font-weight: font-weight('light');
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/entity.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/entity.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/entity.scss");
    }
}
