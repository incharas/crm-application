<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityExtend/ConfigEntityGrid/unique.html.twig */
class __TwigTemplate_d5712edf6c8ca026f861664ea2374412 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'unique_collection_widget' => [$this, 'block_unique_collection_widget'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), "@OroForm/Form/fields.html.twig", true);
        // line 3
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityextend_entity_unique_key", ["id" => ($context["entity_id"] ?? null)]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroEntityExtend/ConfigEntityGrid/unique.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityExtend/ConfigEntityGrid/unique.html.twig", 6)->unwrap();
        // line 7
        echo "
    ";
        // line 8
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_view", ["id" => ($context["entity_id"] ?? null)])], 8, $context, $this->getSourceContext());
        echo "
    ";
        // line 9
        echo twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "route" => "oro_entityconfig_view", "params" => ["id" =>         // line 12
($context["entity_id"] ?? null)]]], 9, $context, $this->getSourceContext());
        // line 13
        echo "
";
    }

    // line 16
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        $context["breadcrumbs"] = ["entity" => "entity", "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.config_grid.entities"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.config_grid.unique_keys"), "additional" => [0 => ["indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_entityconfig_view", ["id" =>         // line 24
($context["entity_id"] ?? null)]), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 25
($context["entity_config"] ?? null), "get", [0 => "label"], "method", true, true, false, 25)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity_config"] ?? null), "get", [0 => "label"], "method", false, false, false, 25), "N/A")) : ("N/A")))]]];
        // line 29
        echo "
    ";
        // line 30
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 33
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    ";
        $this->displayParentBlock("stats", $context, $blocks);
        echo "
";
    }

    // line 59
    public function block_unique_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "    ";
        $macros["configEntityGridUnique"] = $this;
        // line 61
        echo "
    ";
        // line 62
        ob_start(function () { return ''; });
        // line 63
        echo "        <div class=\"row-oro\">
            <div class=\"oro-item-collection collection-fields-list\" id=\"entity_extend_unique_key_collection\" data-prototype=\"";
        // line 64
        echo twig_escape_filter($this->env, twig_call_macro($macros["configEntityGridUnique"], "macro_unique_collection_prototype", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "keys", [], "any", false, false, false, 64)], 64, $context, $this->getSourceContext()));
        echo "\">
                ";
        // line 65
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "keys", [], "any", false, false, false, 65), "children", [], "any", false, false, false, 65));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 66
            echo "                    ";
            echo twig_call_macro($macros["configEntityGridUnique"], "macro_unique_collection_prototype", [$context["field"]], 66, $context, $this->getSourceContext());
            echo "
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "            </div>
            <div class=\"btn-container\">
                <a class=\"btn add-list-item pull-left\" href=\"#\">";
        // line 70
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add"), "html", null, true);
        echo "</a>
            </div>
        </div>
    ";
        $___internal_parse_55_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 62
        echo twig_spaceless($___internal_parse_55_);
    }

    // line 76
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 77
        echo "    ";
        $context["id"] = "configentity-unique";
        // line 78
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_extend.unique_keys"), "class" => "active", "subblocks" => [0 => ["title" => "", "useSpan" => false, "data" => [0 =>         $this->renderBlock("unique_collection_widget", $context, $blocks)]]]]];
        // line 92
        echo "
    ";
        // line 93
        $context["data"] = ["formErrors" => ((        // line 94
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 95
($context["dataBlocks"] ?? null), "hiddenData" =>         // line 96
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest')];
        // line 98
        echo "
    ";
        // line 99
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    // line 37
    public function macro_unique_collection_prototype($__widget__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 38
            echo "    ";
            if (twig_in_filter("prototype", twig_get_array_keys_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 38)))) {
                // line 39
                echo "        ";
                $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 39), "prototype", [], "any", false, false, false, 39);
                // line 40
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 40), "prototype", [], "any", false, false, false, 40), "vars", [], "any", false, false, false, 40), "name", [], "any", false, false, false, 40);
                // line 41
                echo "    ";
            } else {
                // line 42
                echo "        ";
                $context["form"] = ($context["widget"] ?? null);
                // line 43
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 43), "full_name", [], "any", false, false, false, 43);
                // line 44
                echo "    ";
            }
            // line 45
            echo "    <div data-content=\"";
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\" class=\"control-group-container\">
        <div class=\"row-oro oro-multiselect-holder\">
            <div class=\"float-holder\">
                ";
            // line 48
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                ";
            // line 49
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 49), 'row');
            echo "
                ";
            // line 50
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "key", [], "any", false, false, false, 50), 'row');
            echo "
            </div>
            <button class=\"removeRow btn btn-icon btn-square-light\" type=\"button\" data-related=\"";
            // line 52
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\">
                <span class=\"fa-close\"></span>
            </button>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityExtend/ConfigEntityGrid/unique.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  234 => 52,  229 => 50,  225 => 49,  221 => 48,  214 => 45,  211 => 44,  208 => 43,  205 => 42,  202 => 41,  199 => 40,  196 => 39,  193 => 38,  180 => 37,  174 => 99,  171 => 98,  169 => 96,  168 => 95,  167 => 94,  166 => 93,  163 => 92,  160 => 78,  157 => 77,  153 => 76,  149 => 62,  142 => 70,  138 => 68,  129 => 66,  125 => 65,  121 => 64,  118 => 63,  116 => 62,  113 => 61,  110 => 60,  106 => 59,  99 => 34,  95 => 33,  89 => 30,  86 => 29,  84 => 25,  83 => 24,  81 => 17,  77 => 16,  72 => 13,  70 => 12,  69 => 9,  65 => 8,  62 => 7,  59 => 6,  55 => 5,  50 => 1,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityExtend/ConfigEntityGrid/unique.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityExtendBundle/Resources/views/ConfigEntityGrid/unique.html.twig");
    }
}
