<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/clearable.html */
class __TwigTemplate_21ae1bcb5a924d64cea27548ca071af4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<span class=\"clearable-input__container\">
    <% if (placeholderIcon) { %>
        <span class=\"fa-icon <%- placeholderIcon %> clearable-input__placeholder-icon\" aria-hidden=\"true\"></span>
    <% } %>
    <button type=\"button\" class=\"btn btn-icon btn-square-light clearable-input__clear\">
        <span class=\"fa-icon\" aria-hidden=\"true\"></span>
    </button>
</span>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/clearable.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/clearable.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/clearable.html");
    }
}
