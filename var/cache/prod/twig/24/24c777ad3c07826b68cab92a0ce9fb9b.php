<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/flexible-arrow.scss */
class __TwigTemplate_e1f33cbf78261b9e9331064c16b81c13 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

// Use: @include flexible-arrow();
// Mixin can't wrap in RTLCSS directives
// Use RTL directives only for a specific line
@mixin flexible-arrow(
    \$arrow-height: 10px,
    \$arrow-width: 11px,
    \$arrow-offset: -5px,
    \$arrow-placeholder: 8px,
    \$arrow-box-shadow: 0 0 0 1px \$dropdown-border-color,
    \$arrow-transform: rotate(53deg) skewX(15deg),
    \$arrow-placement-end-transform: rotate(145deg) skewX(15deg),
    \$arrow-placement-start-transform: rotate(145deg) skewX(15deg),
) {
    &::before {
        position: absolute;

        height: \$arrow-placeholder;
        width: 100%;

        content: '';
    }

    &::after {
        position: absolute;
        top: 0;

        /* rtl:ignore */
        right: 0;
        bottom: 0;
        border: 0 none;

        /* rtl:ignore */
        left: 0;
        z-index: z('hidden');

        background: inherit;
        border-radius: inherit;

        content: '';
    }

    .arrow {
        position: absolute;
        z-index: z('hidden');

        width: \$arrow-width;
        height: \$arrow-height;

        border: 0 none;
        background: inherit;
        box-shadow: \$arrow-box-shadow;

        transform: \$arrow-transform;
    }

    &[x-placement^='top'] {
        margin-bottom: \$arrow-placeholder;

        &::before {
            top: 100%;
        }

        .arrow {
            bottom: \$arrow-offset;
        }
    }

    &[x-placement^='right'] {
        /* rtl:ignore */
        margin-left: \$arrow-placeholder;

        &::before {
            top: 0;

            /* rtl:ignore */
            right: 100%;

            height: 100%;
            width: \$arrow-placeholder;
        }

        .arrow {
            /* rtl:ignore */
            left: \$arrow-offset;

            transform: \$arrow-placement-end-transform;
        }
    }

    &[x-placement^='bottom'] {
        margin-top: \$arrow-placeholder;

        &::before {
            bottom: 100%;
        }

        .arrow {
            top: \$arrow-offset;
        }
    }

    &[x-placement^='left'] {
        /* rtl:ignore */
        margin-right: \$arrow-placeholder;

        &::before {
            top: 0;

            /* rtl:ignore */
            left: 100%;

            width: \$arrow-placeholder;
            height: 100%;
        }

        .arrow {
            /* rtl:ignore */
            right: \$arrow-offset;

            transform: \$arrow-placement-start-transform;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/flexible-arrow.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/flexible-arrow.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/flexible-arrow.scss");
    }
}
