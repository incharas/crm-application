<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/widgets-module.js */
class __TwigTemplate_1d054e461a3b1594874e5fc072d41b39 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import mediator from 'oroui/js/mediator';
import widgetManager from 'oroui/js/widget-manager';

/**
 * Init Widget Manager's handlers and listeners
 */
mediator.setHandler('widgets:getByIdAsync', widgetManager.getWidgetInstance, widgetManager);
mediator.setHandler('widgets:getByAliasAsync', widgetManager.getWidgetInstanceByAlias, widgetManager);
mediator.on('widget_initialize', widgetManager.addWidgetInstance, widgetManager);
mediator.on('widget_remove', widgetManager.removeWidget, widgetManager);
mediator.on('page:afterChange', widgetManager.resetWidgets, widgetManager);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/widgets-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/widgets-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/widgets-module.js");
    }
}
