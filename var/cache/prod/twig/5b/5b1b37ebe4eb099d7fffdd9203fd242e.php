<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/select.scss */
class __TwigTemplate_6f53278129ecf2a7c6dbf103fa671588 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

.select {
    background: \$select-bg;

    @include base-ui-element(
        \$use-base-style-for-select,
        \$select-padding,
        \$select-font-size,
        \$select-font-family,
        \$select-line-height,
        \$select-border,
        \$select-border-radius,
        \$select-background-color,
        \$select-color,
        \$select-disable-platform-style
    );

    @include placeholder {
        color: \$select-placeholder-color;
    }

    @include element-state('hover') {
        text-decoration: none;
        border-color: \$select-border-color-hover-state;
    }

    @include element-state('focus') {
        border-color: \$select-border-color-focus-state;
        box-shadow: \$select-box-shadow-focus-state;
    }

    &.focus-visible {
        border-color: \$select-border-color-focus-state;
    }

    @include element-state('error') {
        border-color: \$select-border-color-error-state;
    }

    @include element-state('error') {
        box-shadow: \$select-box-shadow-error-state;
    }

    @include element-state('disabled') {
        background-color: \$select-disabled-background-color;
        pointer-events: none;
    }

    &--size-m {
        height: \$base-ui-element-height-size-m;
        padding: \$select-padding--m;
    }

    &--size-s {
        height: \$select-height--s;
        padding: \$select-padding--s;
    }
}

@-moz-document url-prefix('') {
    .select {
        height: \$select-firefox-height;
        line-height: \$select-firefox-line-height;
        padding-top: \$select-firefox-inner-offset-top;
        padding-bottom: \$select-firefox-inner-offset-bottom;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/select.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/select.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/select.scss");
    }
}
