<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Call/action/logCallButton.html.twig */
class __TwigTemplate_b1c28c9aeda9439ee6b0b02896fdfce9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["cssClass"] = "btn icons-holder-text";
        // line 2
        $context["role"] = "button";
        // line 3
        $this->loadTemplate("@OroCall/Call/activityLink.html.twig", "@OroCall/Call/action/logCallButton.html.twig", 3)->display($context);
    }

    public function getTemplateName()
    {
        return "@OroCall/Call/action/logCallButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Call/action/logCallButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Call/action/logCallButton.html.twig");
    }
}
