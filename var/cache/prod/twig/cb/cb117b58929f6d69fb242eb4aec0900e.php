<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivity/macros.html.twig */
class __TwigTemplate_0ccaa3c039a936f840efe12a2a9ce892 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 64
        echo "
";
        // line 94
        echo "
";
    }

    // line 6
    public function macro_activity_contexts($__entity__ = null, $__target__ = null, $__checkTarget__ = null, $__component__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "target" => $__target__,
            "checkTarget" => $__checkTarget__,
            "component" => $__component__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 7
            echo "    <script type=\"text/template\" id=\"activity-context-activity-list\">
        <div class=\"context-item\" data-cid=\"<%- entity.cid %>\">
                <span data-id=\"<%- entity.get('targetId') %>\">
                    <span class=\"context-icon <%- entity.get('icon') %>\" aria-hidden=\"true\"></span>
                    <% if (entity.get('link')) { %>
                        <a href=\"<%- entity.get('link') %>\">
                            <span class=\"context-label\" title=\"<%- entity.get('title') %>\"><%- entity.get('title') %></span>
                        </a>
                    <% } else { %>
                        <span class=\"context-label\" title=\"<%- entity.get('title') %>\"><%- entity.get('title') %></span>
                    <% }  %>
                    ";
            // line 18
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
                // line 19
                echo "                        <span class=\"fa-close\" aria-hidden=\"true\" data-role=\"delete-item\"></span>
                    ";
            }
            // line 21
            echo "                </span>
        </div>
    </script>

    ";
            // line 25
            if (twig_test_empty(($context["checkTarget"] ?? null))) {
                // line 26
                echo "        ";
                $context["checkTarget"] = false;
                // line 27
                echo "    ";
            }
            // line 28
            echo "
    ";
            // line 29
            $context["targetClassNameEncoded"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["target"] ?? null), true);
            // line 30
            echo "    ";
            $context["activityClassAlias"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassAlias(($context["entity"] ?? null), true);
            // line 31
            echo "
    ";
            // line 32
            $context["options"] = ["entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 33
($context["entity"] ?? null), "id", [], "any", false, false, false, 33), "target" => false, "checkTarget" =>             // line 35
($context["checkTarget"] ?? null), "activityClassAlias" =>             // line 36
($context["activityClassAlias"] ?? null)];
            // line 38
            echo "
    ";
            // line 39
            if (($context["checkTarget"] ?? null)) {
                // line 40
                echo "        ";
                $context["targetEntity"] = ["target" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 42
($context["target"] ?? null), "id", [], "any", false, false, false, 42), "className" =>                 // line 43
($context["targetClassNameEncoded"] ?? null)]];
                // line 46
                echo "        ";
                $context["options"] = twig_array_merge(($context["options"] ?? null), ($context["targetEntity"] ?? null));
                // line 47
                echo "    ";
            }
            // line 48
            echo "
    ";
            // line 49
            $context["component"] = (((array_key_exists("component", $context) && ($context["component"] ?? null))) ? (            // line 50
($context["component"] ?? null)) : ("oroactivity/js/app/components/activity-context-activity-component"));
            // line 52
            echo "
    <div class=\"activity-context-activity\"
         data-page-component-module=\"";
            // line 54
            echo twig_escape_filter($this->env, ($context["component"] ?? null), "html", null, true);
            echo "\"
         data-page-component-options=\"";
            // line 55
            echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
            echo "\"
         data-layout=\"separate\">
        <div class=\"activity-context-activity-label\">
            ";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.label"), "html", null, true);
            echo "
        </div>
        <div class=\"activity-context-activity-items\">
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 65
    public function macro_activity_context_link($__hideText__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "hideText" => $__hideText__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 66
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroActivity/macros.html.twig", 66)->unwrap();
            // line 67
            echo "    <a href=\"#\" role=\"button\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.placeholder"), "html_attr");
            echo "\"
       class=\"dropdown-item\"
       data-url=\"<%- routing.generate('oro_activity_context', {'id': relatedActivityId, 'activity': relatedActivityClass }) %>\"
        ";
            // line 70
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "createOnEvent" => "click", "options" => ["alias" => "activity-context-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.add_context_entity.label"), "allowMaximize" => true, "allowMinimize" => true, "modal" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded", "height" => 600]]]], 70, $context, $this->getSourceContext());
            // line 89
            echo ">
        <span class=\"fa-link hide-text\" aria-hidden=\"true\">";
            // line 90
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.placeholder"), "html", null, true);
            echo "</span>
        ";
            // line 91
            ((($context["hideText"] ?? null)) ? (print ("")) : (print (twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.placeholder"), "html", null, true))));
            echo "
    </a>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 96
    public function macro_addContextButton($__entity__ = null, $__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 97
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
                // line 98
                echo "        ";
                $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroActivity/macros.html.twig", 98)->unwrap();
                // line 99
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_activity_context", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 102
($context["entity"] ?? null), "id", [], "any", false, false, false, 102), "activity" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(                // line 103
($context["entity"] ?? null), true)]), "aCss" => "no-hash", "iCss" => "fa-link hide-text", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 107
($context["entity"] ?? null), "id", [], "any", false, false, false, 107), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.add_context.label"), "widget" => ["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "options" => ["alias" => "activity-context-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.add_context_entity.label"), "allowMaximize" => true, "allowMinimize" => true, "modal" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded", "height" => 600]]]]], 99, $context, $this->getSourceContext());
                // line 128
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroActivity/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  230 => 128,  228 => 107,  227 => 103,  226 => 102,  224 => 99,  221 => 98,  219 => 97,  205 => 96,  193 => 91,  189 => 90,  186 => 89,  184 => 70,  177 => 67,  174 => 66,  161 => 65,  146 => 58,  140 => 55,  136 => 54,  132 => 52,  130 => 50,  129 => 49,  126 => 48,  123 => 47,  120 => 46,  118 => 43,  117 => 42,  115 => 40,  113 => 39,  110 => 38,  108 => 36,  107 => 35,  106 => 33,  105 => 32,  102 => 31,  99 => 30,  97 => 29,  94 => 28,  91 => 27,  88 => 26,  86 => 25,  80 => 21,  76 => 19,  74 => 18,  61 => 7,  45 => 6,  40 => 94,  37 => 64,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivity/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActivityBundle/Resources/views/macros.html.twig");
    }
}
