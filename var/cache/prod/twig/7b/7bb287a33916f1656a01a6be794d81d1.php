<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/Dotmailer/integrationConnection.html.twig */
class __TwigTemplate_e0c6a55c2dbc5edd35e34988d0aa4179 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'bodyClass' => [$this, 'block_bodyClass'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig"], true);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%title%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.integration.connection.label")]]);
        // line 6
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_integration_connection");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroDotmailer/Dotmailer/integrationConnection.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "dotmailer-page";
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 11
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.integration.connection.label");
        // line 13
        echo "    ";
        $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroDotmailer/Dotmailer/integrationConnection.html.twig", 13)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
    }

    // line 16
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/Dotmailer/integrationConnection.html.twig", 17)->unwrap();
        // line 18
        echo "
    ";
        // line 19
        ob_start(function () { return ''; });
        // line 20
        echo "        ";
        if (((($context["entity"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "transport", [], "any", false, false, false, 20), "clientId", [], "any", false, false, false, 20)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "transport", [], "any", false, false, false, 20), "clientKey", [], "any", false, false, false, 20))) {
            // line 21
            echo "            ";
            if (($context["loginUserUrl"] ?? null)) {
                // line 22
                echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
                // line 24
                echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_oauth_disconnect", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 25
($context["entity"] ?? null), "id", [], "any", false, false, false, 25)]), "aCss" => "btn btn-primary", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.integration.disconnect.label"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.integration.disconnect.label")]], 24, $context, $this->getSourceContext());
                // line 29
                echo "
                    </div>
                </div>
            ";
            } else {
                // line 33
                echo "                <div class=\"control-group\">
                    <div class=\"controls\">
                        ";
                // line 35
                echo twig_call_macro($macros["UI"], "macro_button", [["path" =>                 // line 36
($context["connectUrl"] ?? null), "aCss" => "btn btn-primary", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.integration.connect.label"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.integration.connect.label")]], 35, $context, $this->getSourceContext());
                // line 40
                echo "
                    </div>
                </div>
            ";
            }
            // line 44
            echo "        ";
        }
        // line 45
        echo "    ";
        $context["button"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 46
        echo "
    ";
        // line 47
        $context["id"] = "oro_dotmailer_integration_connection";
        // line 48
        echo "    ";
        $context["dataBlocks"] = [];
        // line 49
        echo "    ";
        if (($context["loginUserUrl"] ?? null)) {
            // line 50
            echo "        ";
            ob_start(function () { return ''; });
            // line 51
            echo "            <input type=\"hidden\" data-focusable=\"true\" /> ";
            // line 52
            echo "            <iframe src=\"";
            echo twig_escape_filter($this->env, ($context["loginUserUrl"] ?? null), "html", null, true);
            echo "\" width=\"100%\" height=\"1650\" frameborder=\"0\"></iframe>
        ";
            $context["dotmailerSection"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 54
            echo "
        ";
            // line 55
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.channel_type.label"), "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 60
($context["dotmailerSection"] ?? null)]]]]]);
            // line 64
            echo "    ";
        }
        // line 65
        echo "
    ";
        // line 66
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.integration.choose_connection.label"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 71
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channel", [], "any", false, false, false, 71), 'row'), 1 =>         // line 72
($context["button"] ?? null)]]]]]);
        // line 76
        echo "
    ";
        // line 77
        $context["data"] = ["formErrors" => ((        // line 78
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 79
($context["dataBlocks"] ?? null)];
        // line 81
        echo "
    ";
        // line 82
        $context["fieldsToSendOnChannelChange"] = [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channel", [], "any", false, false, false, 82), "vars", [], "any", false, false, false, 82), "full_name", [], "any", false, false, false, 82)];
        // line 83
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", true, true, false, 83)) {
            // line 84
            echo "        ";
            $context["fieldsToSendOnChannelChange"] = twig_array_merge(($context["fieldsToSendOnChannelChange"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", false, false, false, 84), "vars", [], "any", false, false, false, 84), "full_name", [], "any", false, false, false, 84)]);
            // line 85
            echo "    ";
        }
        // line 86
        echo "    <script type=\"text/javascript\">
        loadModules(['orodotmailer/js/integration-connection'], function (IntegrationConnection) {
            \"use strict\";

            \$(function () {
                var options = {
                    formSelector: '#";
        // line 92
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 92), "id", [], "any", false, false, false, 92), "html", null, true);
        echo "',
                    channelSelector: '#";
        // line 93
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channel", [], "any", false, false, false, 93), "vars", [], "any", false, false, false, 93), "id", [], "any", false, false, false, 93), "html", null, true);
        echo "',
                    fieldsSets: {
                        channel: ";
        // line 95
        echo json_encode(($context["fieldsToSendOnChannelChange"] ?? null));
        echo "
                    }
                };

                new IntegrationConnection(options);
            });
        });
    </script>

    ";
        // line 104
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/Dotmailer/integrationConnection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 104,  207 => 95,  202 => 93,  198 => 92,  190 => 86,  187 => 85,  184 => 84,  181 => 83,  179 => 82,  176 => 81,  174 => 79,  173 => 78,  172 => 77,  169 => 76,  167 => 72,  166 => 71,  165 => 66,  162 => 65,  159 => 64,  157 => 60,  156 => 55,  153 => 54,  147 => 52,  145 => 51,  142 => 50,  139 => 49,  136 => 48,  134 => 47,  131 => 46,  128 => 45,  125 => 44,  119 => 40,  117 => 36,  116 => 35,  112 => 33,  106 => 29,  104 => 25,  103 => 24,  99 => 22,  96 => 21,  93 => 20,  91 => 19,  88 => 18,  85 => 17,  81 => 16,  76 => 13,  73 => 12,  69 => 11,  63 => 9,  56 => 8,  51 => 1,  49 => 6,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/Dotmailer/integrationConnection.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/Dotmailer/integrationConnection.html.twig");
    }
}
