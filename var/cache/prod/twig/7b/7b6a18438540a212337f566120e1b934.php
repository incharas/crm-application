<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/inviteHangoutLink.html.twig */
class __TwigTemplate_eafe20cc3192a94900a542583a99e294 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'action_controll' => [$this, 'block_action_controll'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["options"] = ["class" => ((        // line 2
array_key_exists("cssClass", $context)) ? (($context["cssClass"] ?? null)) : ("")), "aCss" => "no-hash", "iCss" => "icon-hangouts", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.hangoutscall.call.hangouts_call"), "pageComponent" => ["module" => "orohangoutscall/js/app/components/invite-hangout-component", "options" => twig_array_merge(["modalOptions" => ["hangoutOptions" => ((        // line 10
array_key_exists("hangoutOptions", $context)) ? (($context["hangoutOptions"] ?? null)) : ([]))]], ((        // line 12
array_key_exists("extraComponentOptions", $context)) ? (($context["extraComponentOptions"] ?? null)) : ([])))]];
        // line 15
        echo "
";
        // line 16
        $this->displayBlock('action_controll', $context, $blocks);
    }

    public function block_action_controll($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroHangoutsCall/inviteHangoutLink.html.twig", 17)->unwrap();
        // line 18
        echo "
    ";
        // line 19
        echo twig_call_macro($macros["UI"], "macro_clientLink", [($context["options"] ?? null)], 19, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/inviteHangoutLink.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 19,  56 => 18,  53 => 17,  46 => 16,  43 => 15,  41 => 12,  40 => 10,  39 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/inviteHangoutLink.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/inviteHangoutLink.html.twig");
    }
}
