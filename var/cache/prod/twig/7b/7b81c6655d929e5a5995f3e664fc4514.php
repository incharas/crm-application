<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/multiple-entity/model.js */
class __TwigTemplate_f5ab8ef8cd88dcdcc2c0f9e49eea418c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'backbone'
], function(Backbone) {
    'use strict';

    /**
     * @export  oroform/js/multiple-entity/model
     * @class   oroform.MultipleEntity.Model
     * @extends Backbone.Model
     */
    const EntityModel = Backbone.Model.extend({
        defaults: {
            id: null,
            link: null,
            label: null,
            isDefault: false,
            extraData: []
        },

        /**
         * @inheritdoc
         */
        constructor: function EntityModel(attrs, options) {
            EntityModel.__super__.constructor.call(this, attrs, options);
        }
    });

    return EntityModel;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/multiple-entity/model.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/multiple-entity/model.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/multiple-entity/model.js");
    }
}
