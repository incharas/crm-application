<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/font-awesome/font-awesome.scss */
class __TwigTemplate_0fbbe1eddb8a492262331aee598529ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@import 'variables';
@import 'core';
@import '~@oroinc/font-awesome/scss/bordered-pulled';
@import '~@oroinc/font-awesome/scss/icons';
@import 'icons-rtl';
@import '~@oroinc/font-awesome/scss/animated';
@import 'animated-rtl';
@import 'placeholders';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/font-awesome/font-awesome.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/font-awesome/font-awesome.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/font-awesome/font-awesome.scss");
    }
}
