<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/variables/inline-editable-wrapper-variables.scss */
class __TwigTemplate_16c39a43713acce79a9f558619ec1fc0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$inline-editable-wrapper-loading-top: 4px !default;
\$inline-editable-wrapper-loading-end: 6px !default;
\$inline-editable-wrapper-loading-size: 12px !default;
\$inline-editable-wrapper-loading-width: 2px !default;

\$inline-actions-btn-offset: 10px 8px !default;

\$inline-actions-btn-s-offset: 4px !default;
\$inline-actions-btn-s-line-height: 1 !default;
\$inline-actions-btn-color: \$secondary-100 !default;
\$inline-actions-btn-color-hover: \$secondary !default;
\$inline-actions-btn-color-error: \$danger-darken !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/variables/inline-editable-wrapper-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/variables/inline-editable-wrapper-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/variables/inline-editable-wrapper-variables.scss");
    }
}
