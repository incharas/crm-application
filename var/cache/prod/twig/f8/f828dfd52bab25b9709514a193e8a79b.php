<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAction/Operation/button.html.twig */
class __TwigTemplate_f5d98c464dab5347ef73f0fd8bf6d04d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'button' => [$this, 'block_button'],
            'link' => [$this, 'block_link'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["linkTitle"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "frontendOptions", [], "any", false, true, false, 1), "title", [], "any", true, true, false, 1)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "frontendOptions", [], "any", false, false, false, 1), "title", [], "any", false, false, false, 1))) : (""));
        // line 2
        $context["linkLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "label", [], "any", false, false, false, 2));
        // line 3
        $context["linkAriaLabel"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "frontendOptions", [], "any", false, true, false, 3), "options", [], "any", false, true, false, 3), "ariaLabel", [], "any", true, true, false, 3)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "frontendOptions", [], "any", false, false, false, 3), "options", [], "any", false, false, false, 3), "ariaLabel", [], "any", false, false, false, 3))) : (""));
        // line 4
        $context["isAjax"] = false;
        // line 5
        echo "
";
        // line 6
        $this->displayBlock('button', $context, $blocks);
    }

    public function block_button($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $context["options"] = $this->extensions['Oro\Bundle\ActionBundle\Twig\OperationExtension']->getFrontendOptions(($context["button"] ?? null));
        // line 8
        echo "    ";
        $context["buttonOptions"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "options", [], "any", false, false, false, 8);
        // line 9
        echo "    ";
        if ( !((array_key_exists("onlyLink", $context)) ? (_twig_default_filter(($context["onlyLink"] ?? null), false)) : (false))) {
            echo "<div class=\"pull-left btn-group icons-holder\">";
        }
        // line 10
        echo "    ";
        $this->displayBlock('link', $context, $blocks);
        // line 45
        echo "    ";
        if ( !((array_key_exists("onlyLink", $context)) ? (_twig_default_filter(($context["onlyLink"] ?? null), false)) : (false))) {
            echo "</div>";
        }
    }

    // line 10
    public function block_link($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "        ";
        $context["classes"] = ((((("icons-holder-text operation-button " . twig_trim_filter(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 11), "class", [], "any", true, true, false, 11)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 11), "class", [], "any", false, false, false, 11), "")) : ("")))) . " ") . twig_trim_filter(((array_key_exists("aClass", $context)) ? (_twig_default_filter(($context["aClass"] ?? null), "")) : ("")))) . " ") . twig_trim_filter(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 11), "aCss", [], "any", true, true, false, 11)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 11), "aCss", [], "any", false, false, false, 11), "")) : (""))));
        // line 12
        echo "        ";
        $context["classes"] = twig_join_filter(array_unique(twig_split_filter($this->env, ($context["classes"] ?? null), " ")), " ");
        // line 13
        echo "        <a ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "id", [], "any", true, true, false, 13)) {
            echo "id=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "id", [], "any", false, false, false, 13), "html", null, true);
            echo "\"";
        }
        // line 14
        echo "           href=\"#\"
           class=\"";
        // line 15
        echo twig_escape_filter($this->env, ($context["classes"] ?? null), "html", null, true);
        echo "\"
           ";
        // line 16
        if ( !twig_test_empty(($context["linkTitle"] ?? null))) {
            // line 17
            echo "               title=\"";
            echo twig_escape_filter($this->env, ($context["linkTitle"] ?? null), "html", null, true);
            echo "\"
           ";
        }
        // line 19
        echo "           ";
        if ( !twig_test_empty(($context["linkAriaLabel"] ?? null))) {
            // line 20
            echo "               aria-label=\"";
            echo twig_escape_filter($this->env, ($context["linkAriaLabel"] ?? null), "html", null, true);
            echo "\"
           ";
        }
        // line 22
        echo "           ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data", [], "any", false, false, false, 22));
        foreach ($context['_seq'] as $context["name"] => $context["value"]) {
            // line 23
            echo "               data-";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, ((twig_test_iterable($context["value"])) ? (json_encode($context["value"])) : ($context["value"])), "html", null, true);
            echo "\"
           ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 25
        echo "           data-operation-url=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["buttonOptions"] ?? null), "url", [], "any", false, false, false, 25), "html", null, true);
        echo "\"
           data-options=\"";
        // line 26
        echo twig_escape_filter($this->env, json_encode(($context["buttonOptions"] ?? null)), "html", null, true);
        echo "\"
           ";
        // line 28
        echo "           ";
        if (!twig_in_filter("page-component-module", twig_get_array_keys_filter(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data", [], "any", true, true, false, 28)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data", [], "any", false, false, false, 28), [])) : ([]))))) {
            // line 29
            echo "               data-page-component-module=\"oroaction/js/app/components/button-component\"
           ";
        }
        // line 31
        echo "           role=\"";
        echo twig_escape_filter($this->env, ((array_key_exists("linkRole", $context)) ? (_twig_default_filter(($context["linkRole"] ?? null), "button")) : ("button")), "html", null, true);
        echo "\"
           ";
        // line 32
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["button"] ?? null), "buttonContext", [], "any", false, false, false, 32), "enabled", [], "any", false, false, false, 32)) {
            // line 33
            echo "               disabled=\"disabled\"
           ";
        }
        // line 35
        echo "            ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "name", [], "any", true, true, false, 35)) {
            // line 36
            echo "                data-action-name=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "name", [], "any", false, false, false, 36), "html", null, true);
            echo "\"
            ";
        }
        // line 38
        echo "        >
            ";
        // line 39
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 39), "icon", [], "any", true, true, false, 39) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 39), "iCss", [], "any", true, true, false, 39))) {
            // line 40
            echo "                <span class=\"";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 40), "icon", [], "any", true, true, false, 40)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 40), "icon", [], "any", false, false, false, 40), "")) : ("")), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 40), "iCss", [], "any", true, true, false, 40)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "buttonOptions", [], "any", false, true, false, 40), "iCss", [], "any", false, false, false, 40), "")) : ("")), "html", null, true);
            echo " hide-text\" aria-hidden=\"true\"></span>
            ";
        }
        // line 42
        echo "            ";
        echo twig_escape_filter($this->env, twig_trim_filter(($context["linkLabel"] ?? null)), "html", null, true);
        echo "
        </a>
    ";
    }

    public function getTemplateName()
    {
        return "@OroAction/Operation/button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  184 => 42,  176 => 40,  174 => 39,  171 => 38,  165 => 36,  162 => 35,  158 => 33,  156 => 32,  151 => 31,  147 => 29,  144 => 28,  140 => 26,  135 => 25,  124 => 23,  119 => 22,  113 => 20,  110 => 19,  104 => 17,  102 => 16,  98 => 15,  95 => 14,  88 => 13,  85 => 12,  82 => 11,  78 => 10,  71 => 45,  68 => 10,  63 => 9,  60 => 8,  57 => 7,  50 => 6,  47 => 5,  45 => 4,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAction/Operation/button.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActionBundle/Resources/views/Operation/button.html.twig");
    }
}
