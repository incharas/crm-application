<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Form/mergeValue.html.twig */
class __TwigTemplate_a15df2bce14b0f5c8e0f9c149139e7bc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        ob_start(function () { return ''; });
        // line 10
        echo "    <span class=\"empty\">-- ";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_merge.form.empty"), "html", null, true);
        echo " --</span>
";
        $context["empty_cell"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 12
        echo "
";
        // line 13
        if (twig_length_filter($this->env, ($context["value"] ?? null))) {
            // line 14
            echo "    <ul>
        ";
            // line 15
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["value"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attachment"]) {
                // line 16
                echo "            <li>
                ";
                // line 17
                $context["options"] = (($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getTypeIsImage(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "file", [], "any", false, false, false, 17), "mimeType", [], "any", false, false, false, 17))) ? (["galleryId" => "merge-attachments"]) : ([]));
                // line 18
                echo "                ";
                echo $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileView($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "file", [], "any", false, false, false, 18), ($context["options"] ?? null));
                echo "
            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attachment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "    </ul>
";
        } else {
            // line 23
            echo "    ";
            echo twig_escape_filter($this->env, ($context["empty_cell"] ?? null), "html", null, true);
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Form/mergeValue.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 23,  72 => 21,  62 => 18,  60 => 17,  57 => 16,  53 => 15,  50 => 14,  48 => 13,  45 => 12,  39 => 10,  37 => 9,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Form/mergeValue.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Form/mergeValue.html.twig");
    }
}
