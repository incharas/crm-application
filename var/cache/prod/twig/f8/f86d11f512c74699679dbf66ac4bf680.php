<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTracking/TrackingWebsite/script.js.twig */
class __TwigTemplate_817fd290626c99a34201b523fca516aa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["hostWithPath"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->pregReplace($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_ui.application_url"), "/^https?:\\/\\/?/", "");
        // line 2
        echo "<script type=\"text/javascript\">
    var _paq = _paq || [];
    _paq.push(['setUserId', [user_identifier] ]);
    _paq.push(['setConversionAttributionFirstReferrer', false]);
    _paq.push(['trackPageView']);
    // Uncomment following line to track custom events
    // _paq.push(['trackEvent', 'OroCRM', 'Tracking', [name], [value] ]);
    (function() {
        var u=((\"https:\" == document.location.protocol) ? \"https\" : \"http\") + \"://\" + ";
        // line 10
        echo ((($context["hostWithPath"] ?? null)) ? (json_encode(($context["hostWithPath"] ?? null))) : ("[host]"));
        echo " + \"/\";
        _paq.push(['setTrackerUrl', u+'tracking.php']);
        _paq.push(['setSiteId', '";
        // line 12
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "identifier", [], "any", false, false, false, 12), "js", null, true);
        echo "']);
        var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0]; g.type='text/javascript';
        g.defer=true; g.async=true; g.src=u+'bundles/orotracking/js/piwik.min.js'; s.parentNode.insertBefore(g,s);
    })();
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroTracking/TrackingWebsite/script.js.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 12,  49 => 10,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTracking/TrackingWebsite/script.js.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/TrackingBundle/Resources/views/TrackingWebsite/script.js.twig");
    }
}
