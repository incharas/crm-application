<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/bootstrap/bootstrap-tabs.js */
class __TwigTemplate_0cb5cd6f3c0f30c2feead054e8fddf6f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const DATA_KEY = 'bs.tab';
    const EVENT_KEY = '.' + DATA_KEY;
    const Event = {
        HIDDEN: 'hidden' + EVENT_KEY,
        SHOWN: 'shown' + EVENT_KEY
    };
    const ClassName = {
        ACTIVE: 'active',
        SHOW: 'show'
    };

    const mediator = require('oroui/js/mediator');
    const Util = require('bootstrap-util');

    \$(document)
        .on(Event.HIDDEN, function(event) {
            const prevEl = \$(event.relatedTarget);

            // Remove active state from element which placed outside of NAV_LIST_GROUP container
            if (prevEl.data('extra-toggle') === 'tab') {
                prevEl
                    .removeClass(ClassName.SHOW + ' ' + ClassName.ACTIVE)
                    .attr('aria-selected', false);
            }
            const selector = Util.getSelectorFromElement(event.target);
            mediator.trigger('content:hidden', \$(selector));
        })
        .on(Event.SHOWN, function(event) {
            const selector = Util.getSelectorFromElement(event.target);
            mediator.trigger('content:shown', \$(selector));
            mediator.trigger('layout:reposition');
        });
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-tabs.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-tabs.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/bootstrap/bootstrap-tabs.js");
    }
}
