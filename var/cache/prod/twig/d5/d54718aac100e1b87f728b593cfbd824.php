<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAddress/widget/addressBook.html.twig */
class __TwigTemplate_eebbce2975cf9ddc1203a986e48968e3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAddress/widget/addressBook.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        $context["addressBookOptions"] = ["module" => "oroaddress/js/app/components/address-book-widget-component", "options" => ["wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["app"] ?? null), "request", [], "any", false, false, false, 7), "get", [0 => "_wid"], "method", false, false, false, 7), "addresses" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment(        // line 8
($context["addressListUrl"] ?? null)), "addressListUrl" =>         // line 9
($context["addressListUrl"] ?? null), "addressCreateUrl" =>         // line 10
($context["addressCreateUrl"] ?? null), "addressUpdateRoute" => ["route" =>         // line 12
($context["addressUpdateRoute"] ?? null), "params" => [        // line 14
($context["ownerParam"] ?? null) => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 14)]], "addressDeleteRoute" => ["route" =>         // line 18
($context["addressDeleteRoute"] ?? null), "params" => [        // line 20
($context["ownerParam"] ?? null) => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 20)]], "isAddressHtmlFormatted" => true]];
        // line 26
        echo "
<div class=\"widget-content\" ";
        // line 27
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["addressBookOptions"] ?? null)], 27, $context, $this->getSourceContext());
        echo ">
    ";
        // line 29
        echo "    ";
        $this->loadTemplate("@OroAddress/Js/address.js.twig", "@OroAddress/widget/addressBook.html.twig", 29)->display($context);
        // line 30
        echo "
    ";
        // line 32
        echo "    <div class=\"widget-actions\">
        ";
        // line 33
        if (( !array_key_exists("address_edit_acl_resource", $context) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(($context["address_edit_acl_resource"] ?? null)))) {
            // line 34
            echo "        <button class=\"btn btn-primary\" type=\"button\" data-action-name=\"add_address\"><span class=\"fa-plus\" aria-hidden=\"true\"></span>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add Address"), "html", null, true);
            echo "</button>
        ";
        }
        // line 36
        echo "    </div>
    <div class=\"map-box\" id=\"address-book\"></div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroAddress/widget/addressBook.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 36,  70 => 34,  68 => 33,  65 => 32,  62 => 30,  59 => 29,  55 => 27,  52 => 26,  50 => 20,  49 => 18,  48 => 14,  47 => 12,  46 => 10,  45 => 9,  44 => 8,  43 => 7,  42 => 4,  39 => 3,  37 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAddress/widget/addressBook.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AddressBundle/Resources/views/widget/addressBook.html.twig");
    }
}
