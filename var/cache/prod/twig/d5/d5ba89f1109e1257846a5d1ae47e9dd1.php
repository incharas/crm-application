<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCase/Comment/widget/comments.html.twig */
class __TwigTemplate_b9171adaf138a7d6d89a5a469e318770 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'items_container' => [$this, 'block_items_container'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNote/Note/widget/notes.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCase/Comment/widget/comments.html.twig", 2)->unwrap();
        // line 4
        $context["containerExtraClass"] = "comments";
        // line 1
        $this->parent = $this->loadTemplate("@OroNote/Note/widget/notes.html.twig", "@OroCase/Comment/widget/comments.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_items_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $context["options"] = ["widgetId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["app"] ?? null), "request", [], "any", false, false, false, 8), "get", [0 => "_wid"], "method", false, false, false, 8), "notesOptions" => ["template" => "#template-comment-list", "itemTemplate" => "#template-comment-item", "itemAddEvent" => "comment:add", "itemViewIdPrefix" => "comment-", "itemView" => "orocase/js/app/views/comment-view", "itemModel" => "orocase/js/app/models/comment-model", "messages" => ["addDialogTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.dialog.add"), "editDialogTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.dialog.edit"), "itemSaved" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.message.saved"), "itemRemoved" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.message.removed"), "deleteConfirmation" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.message.delete_confirmation"), "loadItemsError" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.error.load"), "deleteItemError" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.error.delete")], "urls" => ["list" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_comment_list", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 26
($context["case"] ?? null), "id", [], "any", false, false, false, 26)]), "createItem" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_comment_create", ["caseId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 27
($context["case"] ?? null), "id", [], "any", false, false, false, 27)])], "routes" => ["update" => "oro_case_comment_update", "delete" => "oro_case_api_delete_comment"]], "notesData" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_comment_list", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["case"] ?? null), "id", [], "any", false, false, false, 34)]))];
        // line 36
        echo "    <div class=\"accordion\" id=\"comment-list\"
         data-page-component-module=\"oronote/js/app/components/notes-component\"
         data-page-component-options=\"";
        // line 38
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"></div>

    ";
        // line 40
        $this->loadTemplate("@OroCase/Comment/js/list.html.twig", "@OroCase/Comment/widget/comments.html.twig", 40)->display(twig_array_merge($context, ["id" => "template-comment-list"]));
        // line 41
        echo "    ";
        $this->loadTemplate("@OroCase/Comment/js/view.html.twig", "@OroCase/Comment/widget/comments.html.twig", 41)->display(twig_array_merge($context, ["id" => "template-comment-item"]));
    }

    public function getTemplateName()
    {
        return "@OroCase/Comment/widget/comments.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 41,  71 => 40,  66 => 38,  62 => 36,  60 => 34,  59 => 27,  58 => 26,  57 => 8,  55 => 7,  51 => 6,  46 => 1,  44 => 4,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCase/Comment/widget/comments.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/CaseBundle/Resources/views/Comment/widget/comments.html.twig");
    }
}
