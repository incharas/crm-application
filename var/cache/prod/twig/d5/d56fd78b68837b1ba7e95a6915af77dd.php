<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/validation.scss */
class __TwigTemplate_990abb7cd731b4fda26f2a19f906c99f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

.validation-failed,
.validation-error,
.validation-passed,
.validation-warning {
    @extend %base-notification;

    display: block;
    color: \$validation-failed-color;

    &--lite {
        @extend %base-notification-light;
    }
}

.validation-failed,
.validation-error {
    @extend %base-notification-error;

    &__icon {
        @include fa-icon(\$fa-var-warning, before, true) {
            display: inline-block;
            margin-right: 6px;
        }
    }

    &--flex {
        display: flex;
        align-items: baseline;
    }

    /* Following css rule covers case when a few inputs has the same place for errors (e.g. datetime picker).
     * `id` is used to cover only auto-generated jQuery.validate labels and leave posibility to show a few messages
     * when it rendered intentionally */
    & + &[id] {
        display: none;
    }
}

.validation-warning {
    @extend %base-notification-warning;

    &__icon {
        @include fa-icon(\$fa-var-warning, before, true) {
            display: inline-block;
            margin-right: 6px;
        }
    }
}

.validation-passed {
    @extend %base-notification-success;
}

.controls {
    &.validation-error {
        &::before {
            content: none;
        }
    }
}

.input-widget-select {
    &.error {
        border: \$input-widget-select-error-border;
        box-shadow: \$input-widget-select-error-box-shadow;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/validation.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/validation.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/validation.scss");
    }
}
