<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNotification/NotificationAlert/Property/externalId.html.twig */
class __TwigTemplate_145061a6bde8983cf100f659406d4ed5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["value"] ?? null)) {
            // line 2
            echo "    ";
            $context["collapseId"] = uniqid("collapse-");
            // line 3
            echo "    <div class=\"collapse-block\" style=\"width:260px\">
        <div id=\"";
            // line 4
            echo twig_escape_filter($this->env, ($context["collapseId"] ?? null), "html", null, true);
            echo "\" class=\"collapse-overflow collapse no-transition\"
             data-collapsed-text=\"";
            // line 5
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Show more"), "html", null, true);
            echo "\"
             data-expanded-text=\"";
            // line 6
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Show less"), "html", null, true);
            echo "\"
             data-check-overflow=\"true\"
             data-toggle=\"false\"
             data-state-id=\"";
            // line 9
            echo twig_escape_filter($this->env, ((("collapseBlock[" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "sourceType"], "method", false, false, false, 9)) . "]") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "id"], "method", false, false, false, 9)), "html", null, true);
            echo "\"
        >";
            // line 10
            echo twig_escape_filter($this->env, twig_join_filter(twig_split_filter($this->env, ($context["value"] ?? null), "", 31), " "), "html", null, true);
            echo "</div>
        <a href=\"#\"
           role=\"button\"
           class=\"collapse-toggle\"
           data-toggle=\"collapse\"
           data-target=\"";
            // line 15
            echo twig_escape_filter($this->env, ("#" . ($context["collapseId"] ?? null)), "html", null, true);
            echo "\"
           aria-expanded=\"false\"
           aria-controls=\"";
            // line 17
            echo twig_escape_filter($this->env, ($context["collapseId"] ?? null), "html", null, true);
            echo "\"><span data-text>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Show more"), "html", null, true);
            echo "</span></a>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroNotification/NotificationAlert/Property/externalId.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 17,  71 => 15,  63 => 10,  59 => 9,  53 => 6,  49 => 5,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNotification/NotificationAlert/Property/externalId.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NotificationBundle/Resources/views/NotificationAlert/Property/externalId.html.twig");
    }
}
