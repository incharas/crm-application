<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/bootstrap/bootstrap-popover.js */
class __TwigTemplate_ffa09faa8e3712ec2bee62a3820cc69b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const layout = require('oroui/js/layout');

    require('bootstrap-popover');
    require('./bootstrap-tooltip');

    const Tooltip = \$.fn.tooltip.Constructor;
    const Popover = \$.fn.popover.Constructor;

    _.extend(Popover.prototype, _.pick(Tooltip.prototype, 'show', 'hide', 'dispose'));

    Popover.prototype.getContent = function() {
        return \$('<div/>').append(this._getContent()).html();
    };

    Popover.prototype.applyPlacement = function(offset, placement) {
        const isOpen = this.isOpen();

        _.extend(this.config, {offset: offset, placement: placement});
        this.update();
        this.hide();

        if (isOpen) {
            this.show();
        }
    };

    Popover.prototype.getTipElement = function() {
        this.tip = this.tip || \$(this.config.template)[0];

        const addClass = \$(this.element).data('class');
        if (addClass) {
            \$(this.tip).addClass(addClass);
        }

        return this.tip;
    };

    Popover.prototype.updateContent = function(content) {
        this.element.setAttribute('data-content', content);
        this.config.content = content;
        if (this.isOpen()) {
            this.show();
        }
    };

    Popover.prototype.isOpen = function() {
        return \$(this.getTipElement()).is(':visible');
    };

    \$(document)
        .on('initLayout', function(e) {
            layout.initPopover(\$(e.target));
        })
        .on('disposeLayout', function(e) {
            \$(e.target).find('[data-toggle=\"popover\"]').each(function() {
                const \$el = \$(this);

                if (\$el.data(Popover.DATA_KEY)) {
                    \$el.popover('dispose');
                }
            });
        });

    return Popover;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-popover.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-popover.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/bootstrap/bootstrap-popover.js");
    }
}
