<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/select2/multiple-choice.html */
class __TwigTemplate_cf09c0679bc76404d4c195c99cd2551b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% var resultsId = _.uniqueId('select2-results-'); %>

<div class=\"select2-container select2-container-multi\">
    <ul class=\"select2-choices\"
        role=\"combobox\"
        aria-haspopup=\"true\"
        aria-expanded=\"false\"
        aria-owns=\"<%- resultsId %>\"
    >
        <li class=\"select2-search-field\" role=\"presentation\">
            <input type=\"text\"
                   autocomplete=\"off\"
                   autocorrect=\"off\"
                   autocapitilize=\"off\"
                   spellcheck=\"false\"
                   class=\"select2-input\"
                   role=\"searchbox\"
                   aria-haspopup=\"true\"
                   aria-autocomplete=\"list\"
                   aria-controls=\"<%- resultsId %>\"
            >
        </li>
    </ul>
    <div class=\"select2-drop select2-drop-multi select2-display-none\">
        <ul class=\"select2-results\"
            role=\"listbox\"
            aria-multiselectable=\"true\"
            id=\"<%- resultsId %>\"
            aria-expanded=\"false\"
            aria-hidden=\"true\"
            tabindex=\"-1\"
        ></ul>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/select2/multiple-choice.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/select2/multiple-choice.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/select2/multiple-choice.html");
    }
}
