<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMarketingActivity/MarketingActivity/marketingActivitiesSection.html.twig */
class __TwigTemplate_abb7c482da499ad5e41ad0887a18508d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMarketingActivity/MarketingActivity/marketingActivitiesSection.html.twig", 1)->unwrap();
        // line 2
        ob_start(function () { return ''; });
        // line 3
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_activity_widget_marketing_activities", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 6
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["entity"] ?? null), "id", [], "any", false, false, false, 7)]), "title" => "", "alias" => "marketing-activities-widget", "cssClass" => "list-widget activity-list-widget marketing-activities-list-widget standard-action-buttons"]);
        // line 12
        echo "
";
        $context["widgetContent"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 14
        echo twig_call_macro($macros["UI"], "macro_scrollSubblock", [null, [0 => ($context["widgetContent"] ?? null)]], 14, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroMarketingActivity/MarketingActivity/marketingActivitiesSection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 14,  46 => 12,  44 => 7,  43 => 6,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMarketingActivity/MarketingActivity/marketingActivitiesSection.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/MarketingActivityBundle/Resources/views/MarketingActivity/marketingActivitiesSection.html.twig");
    }
}
