<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/base/model.js */
class __TwigTemplate_140c7e0c2b7302856033d7b1a4e3a572 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const Chaplin = require('chaplin');

    /**
     * @class BaseModel
     * @extends Chaplin.Model
     */
    const BaseModel = Chaplin.Model.extend(/** @lends BaseModel.prototype */{
        constructor: function BaseModel(data, options) {
            BaseModel.__super__.constructor.call(this, data, options);
        }
    });

    return BaseModel;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/base/model.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/base/model.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/base/model.js");
    }
}
