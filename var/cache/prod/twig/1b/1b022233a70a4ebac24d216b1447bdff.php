<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroForm/layouts/default/layout.html.twig */
class __TwigTemplate_62475c764cbf499c4381a285cbfc21c3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'form_end_widget' => [$this, 'block_form_end_widget'],
            'input_widget' => [$this, 'block_input_widget'],
            'button_widget' => [$this, 'block_button_widget'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroLayout/Layout/div_layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroLayout/Layout/div_layout.html.twig", "@OroForm/layouts/default/layout.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_form_end_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $this->displayParentBlock("form_end_widget", $context, $blocks);
        echo "
    ";
        // line 5
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null), ((array_key_exists("js_validation_options", $context)) ? (_twig_default_filter(($context["js_validation_options"] ?? null), [])) : ([])));
        echo "
";
    }

    // line 8
    public function block_input_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        if ((($context["type"] ?? null) == "checkbox")) {
            // line 10
            echo "        <label class=\"checkbox-label\">
            ";
            // line 11
            $this->displayParentBlock("input_widget", $context, $blocks);
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->processText(($context["label"] ?? null), ($context["translation_domain"] ?? null)), "html", null, true);
            // line 13
            echo "</label>
    ";
        } else {
            // line 15
            echo "        ";
            $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " input"]);
            // line 18
            echo "
        ";
            // line 19
            $this->displayParentBlock("input_widget", $context, $blocks);
            echo "
    ";
        }
    }

    // line 23
    public function block_button_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    ";
        if ((((array_key_exists("style", $context)) ? (_twig_default_filter(($context["style"] ?? null), "")) : ("")) == "auto")) {
            // line 25
            echo "        ";
            if ((($context["action"] ?? null) == "submit")) {
                // line 26
                echo "            ";
                $context["style"] = "btn--info";
                // line 27
                echo "        ";
            } else {
                // line 28
                echo "            ";
                $context["style"] = (((($context["action"] ?? null) == "reset")) ? ("btn--action") : (""));
                // line 29
                echo "        ";
            }
            // line 30
            echo "    ";
        }
        // line 31
        echo "    ";
        if ( !array_key_exists("style", $context)) {
            // line 32
            echo "        ";
            $context["add_class"] = "";
            // line 33
            echo "    ";
        } else {
            // line 34
            echo "        ";
            $context["add_class"] = (" btn " . ($context["style"] ?? null));
            // line 35
            echo "    ";
        }
        // line 36
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 37
($context["attr"] ?? null), "class", [], "any", true, true, false, 37)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 37), "")) : ("")) . ($context["add_class"] ?? null))]);
        // line 39
        echo "    ";
        $this->displayParentBlock("button_widget", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroForm/layouts/default/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  138 => 39,  136 => 37,  134 => 36,  131 => 35,  128 => 34,  125 => 33,  122 => 32,  119 => 31,  116 => 30,  113 => 29,  110 => 28,  107 => 27,  104 => 26,  101 => 25,  98 => 24,  94 => 23,  87 => 19,  84 => 18,  81 => 15,  77 => 13,  75 => 12,  73 => 11,  70 => 10,  67 => 9,  63 => 8,  57 => 5,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroForm/layouts/default/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/views/layouts/default/layout.html.twig");
    }
}
