<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/User/widget/apiKeyGen.html.twig */
class __TwigTemplate_1c01ee81566842ae041590e681df3172 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/User/widget/apiKeyGen.html.twig", 1)->unwrap();
        // line 2
        $context["userId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "id", [], "any", false, false, false, 2);
        // line 3
        $context["formId"] = ("user-apikey-gen-form-" . ($context["userId"] ?? null));
        // line 4
        $context["apiKeyElementId"] = ("user-apikey-gen-elem-" . ($context["userId"] ?? null));
        // line 5
        $context["options"] = ["view" => "orouser/js/views/user-apikey-gen-view", "apiKeyElementSelector" => ("#" .         // line 7
($context["apiKeyElementId"] ?? null)), "formSelector" => ("#" .         // line 8
($context["formId"] ?? null)), "responseMessage" => "oro.user.apikey_gen.new_key.success"];
        // line 11
        $context["apiKeyHTML"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(        // line 12
($context["form"] ?? null), 'row', ["method" => "POST", "action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_apigen", ["id" =>         // line 15
($context["userId"] ?? null)]), "attr" => ["id" =>         // line 16
($context["formId"] ?? null)], "apiKeyElementId" =>         // line 17
($context["apiKeyElementId"] ?? null)]);
        // line 20
        $context["attributeOptions"] = ["rootClass" => "api-block", "tooltipHTML" => twig_call_macro($macros["ui"], "macro_tooltip", ["oro.user.api.tooltip_text", [], "bottom"], 22, $context, $this->getSourceContext())];
        // line 24
        echo "<div data-nohash=\"true\"
     data-page-component-module=\"oroui/js/app/components/view-component\"
     data-page-component-options=\"";
        // line 26
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
     data-layout=\"separate\">
    ";
        // line 28
        echo twig_call_macro($macros["ui"], "macro_renderAttribute", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.api.label"), ($context["apiKeyHTML"] ?? null), ($context["attributeOptions"] ?? null)], 28, $context, $this->getSourceContext());
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/User/widget/apiKeyGen.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 28,  61 => 26,  57 => 24,  55 => 20,  53 => 17,  52 => 16,  51 => 15,  50 => 12,  49 => 11,  47 => 8,  46 => 7,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/User/widget/apiKeyGen.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/User/widget/apiKeyGen.html.twig");
    }
}
