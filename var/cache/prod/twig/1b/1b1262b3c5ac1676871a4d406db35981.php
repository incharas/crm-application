<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/main-menu/main-menu.scss */
class __TwigTemplate_2c983601b8059d4c012a137c3e9230dd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.main-menu {
    margin: 0;
    padding: 0;
    font-size: \$menu-font-size;

    // reset global styles
    list-style: none;
    background-color: \$menu-background-color;

    &__header {
        min-height: \$menu-header-height;
        background-color: \$menu-header-background-color;

        .app-logo {
            width: 100%;
            margin: 9px 0;
            text-align: center;
            height: 30px;
        }
    }

    .dropdown-menu-wrapper {
        z-index: \$zindex-dropdown;

        &.hidden {
            display: none;
        }

        &__scrollable {
            overflow-x: hidden;
            overflow-y: auto;
        }
    }

    .dropdown-menu {
        z-index: \$zindex-fixed;
    }

    .divider {
        height: 1px;
        margin: 0;

        border: 1px solid \$menu-item-divider-color;
        border-width: 1px 0 0;

        .title {
            // stylelint-disable-next-line declaration-no-important
            display: none !important;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/main-menu/main-menu.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/main-menu/main-menu.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/main-menu/main-menu.scss");
    }
}
