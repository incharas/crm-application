<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/tools/frontend-type-map.js */
class __TwigTemplate_f9d46f40c46d32d2bbb2ebf0821b5a11 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';
    const frontendTypeMap = {
        tags: {
            viewer: require('orotag/js/app/views/viewer/tags-view'),
            editor: require('orotag/js/app/views/editor/tags-editor-view')
        }
    };
    return frontendTypeMap;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/tools/frontend-type-map.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/tools/frontend-type-map.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/tools/frontend-type-map.js");
    }
}
