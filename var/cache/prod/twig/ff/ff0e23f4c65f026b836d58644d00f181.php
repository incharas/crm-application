<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Workflow/transitionForm.html.twig */
class __TwigTemplate_e3d66adafc87c9a13dfedcf4b5b42511 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pin_button' => [$this, 'block_pin_button'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'content' => [$this, 'block_content'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["entity"] = null;
        // line 3
        $macros["macros"] = $this->macros["macros"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/Workflow/transitionForm.html.twig", 3)->unwrap();
        // line 5
        $context["pageParams"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 5), "page", [], "any", true, true, false, 5)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, false, false, 5), "page", [], "any", false, false, false, 5)) : (null));
        // line 6
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "title", [], "any", true, true, false, 6)) {
            // line 7
            $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "title", [], "any", false, false, false, 7));
        } else {
            // line 9
            $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "buttonLabel", [], "any", false, false, false, 9), [], "workflows");
            // line 10
            if ((twig_test_empty(($context["pageTitle"] ?? null)) || (($context["pageTitle"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "buttonLabel", [], "any", false, false, false, 10)))) {
                // line 11
                $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "label", [], "any", false, false, false, 11), [], "workflows");
            }
        }
        // line 15
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "parent_label", [], "any", true, true, false, 15)) {
            // line 16
            $context["indexLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "parent_label", [], "any", false, false, false, 16));
        } else {
            // line 18
            $context["indexLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["workflow"] ?? null), "label", [], "any", false, false, false, 18), [], "workflows");
        }
        // line 21
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "parent_route", [], "any", true, true, false, 21)) {
            // line 22
            $context["indexPath"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "parent_route", [], "any", false, false, false, 22), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "parent_route_parameters", [], "any", true, true, false, 22)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageParams"] ?? null), "parent_route_parameters", [], "any", false, false, false, 22)) : ([])));
        } else {
            // line 24
            $context["indexPath"] = urldecode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 24), "query", [], "any", false, false, false, 24), "get", [0 => "originalUrl"], "method", false, false, false, 24));
        }

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%workflow_title%" => ((        // line 27
($context["pageTitle"] ?? null) . " - ") . ($context["indexLabel"] ?? null))]]);
        // line 32
        $context["saveAndTransitButtonId"] = "save-and-transit";
        // line 2
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroWorkflow/Workflow/transitionForm.html.twig", 2);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 29
    public function block_pin_button($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 34
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/Workflow/transitionForm.html.twig", 35)->unwrap();
        // line 36
        echo "
    ";
        // line 37
        if (($context["indexPath"] ?? null)) {
            // line 38
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_cancelButton", [urldecode(($context["indexPath"] ?? null))], 38, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 40
        echo "
    <div class=\"btn-group\">
        <button
            type=\"button\"
            class=\"btn btn-success\"
            id=\"";
        // line 45
        echo twig_escape_filter($this->env, ($context["saveAndTransitButtonId"] ?? null), "html", null, true);
        echo "\"
            data-transition-url=\"";
        // line 46
        echo twig_escape_filter($this->env, ($context["transitionUrl"] ?? null), "html", null, true);
        echo "\"
        >";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Submit"), "html", null, true);
        echo "</button>
    </div>
";
    }

    // line 51
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "    ";
        $context["breadcrumbs"] = ["indexPath" => urldecode(        // line 53
($context["indexPath"] ?? null)), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 54
($context["indexLabel"] ?? null)), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 55
($context["pageTitle"] ?? null))];
        // line 57
        echo "
    ";
        // line 58
        $this->displayBlock('stats', $context, $blocks);
        // line 59
        echo "
    ";
        // line 60
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 58
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 63
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 64
        echo "    <ul class=\"breadcrumb\">
        <li>";
        // line 65
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["indexLabel"] ?? null)), "html", null, true);
        echo "</li>
    </ul>
";
    }

    // line 69
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 70
        echo "    ";
        $context["content_attr"] = twig_array_merge(((array_key_exists("content_attr", $context)) ? (_twig_default_filter(($context["content_attr"] ?? null), [])) : ([])), ["class" => "workflow-container"]);
        // line 73
        echo "
    ";
        // line 74
        $context["content_data_attr"] = twig_array_merge(((array_key_exists("content_data_attr", $context)) ? (_twig_default_filter(($context["content_data_attr"] ?? null), [])) : ([])), ["class" => ""]);
        // line 77
        echo "
    ";
        // line 78
        $this->displayParentBlock("content", $context, $blocks);
        echo "
";
    }

    // line 81
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 82
        echo "    ";
        ob_start(function () { return ''; });
        // line 83
        echo "        ";
        $context["widgetAlias"] = "transition-form";
        // line 84
        echo "        ";
        $context["workflowFormWidgetViewOptions"] = ["view" => ["view" => "oroworkflow/js/app/views/workflow-form-widget-view", "widgetAlias" =>         // line 87
($context["widgetAlias"] ?? null), "saveAndTransitButtonSelector" => ("#" .         // line 88
($context["saveAndTransitButtonId"] ?? null))]];
        // line 91
        echo "        <div class=\"form-container\" ";
        echo twig_call_macro($macros["macros"], "macro_renderPageComponentAttributes", [($context["workflowFormWidgetViewOptions"] ?? null)], 91, $context, $this->getSourceContext());
        echo ">
            ";
        // line 92
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" =>         // line 94
($context["transitionFormUrl"] ?? null), "alias" =>         // line 95
($context["widgetAlias"] ?? null), "loadingMaskEnabled" => false]);
        // line 97
        echo "
        </div>
    ";
        $context["transitionFormWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 100
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "hasFormConfiguration", [], "method", false, false, false, 100)) {
            // line 101
            echo "        ";
            echo twig_escape_filter($this->env, ($context["transitionFormWidget"] ?? null), "html", null, true);
            echo "
    ";
        } else {
            // line 103
            echo "        ";
            $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "subblocks" => [0 => ["data" => [0 =>             // line 106
($context["transitionFormWidget"] ?? null)]]]]];
            // line 109
            echo "
        ";
            // line 110
            $context["id"] = "transitionPage";
            // line 111
            echo "        ";
            $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
            // line 112
            echo "        ";
            $this->displayParentBlock("content_data", $context, $blocks);
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Workflow/transitionForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  254 => 112,  251 => 111,  249 => 110,  246 => 109,  244 => 106,  242 => 103,  236 => 101,  233 => 100,  228 => 97,  226 => 95,  225 => 94,  224 => 92,  219 => 91,  217 => 88,  216 => 87,  214 => 84,  211 => 83,  208 => 82,  204 => 81,  198 => 78,  195 => 77,  193 => 74,  190 => 73,  187 => 70,  183 => 69,  176 => 65,  173 => 64,  169 => 63,  163 => 58,  157 => 60,  154 => 59,  152 => 58,  149 => 57,  147 => 55,  146 => 54,  145 => 53,  143 => 52,  139 => 51,  132 => 47,  128 => 46,  124 => 45,  117 => 40,  111 => 38,  109 => 37,  106 => 36,  103 => 35,  99 => 34,  93 => 29,  88 => 2,  86 => 32,  84 => 27,  80 => 24,  77 => 22,  75 => 21,  72 => 18,  69 => 16,  67 => 15,  63 => 11,  61 => 10,  59 => 9,  56 => 7,  54 => 6,  52 => 5,  50 => 3,  48 => 1,  41 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Workflow/transitionForm.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Workflow/transitionForm.html.twig");
    }
}
