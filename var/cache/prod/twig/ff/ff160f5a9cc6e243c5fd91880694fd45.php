<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/widget-picker/widget-picker-collection-view.js */
class __TwigTemplate_45eac7c21d8093a73418595caad27c3f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseCollectionView = require('oroui/js/app/views/base/collection-view');
    const WidgetPickerItemView = require('oroui/js/app/views/widget-picker/widget-picker-item-view');
    const _ = require('underscore');

    const WidgetPickerCollectionView = BaseCollectionView.extend({
        itemView: WidgetPickerItemView,
        isWidgetLoadingInProgress: false,

        /**
         * @inheritdoc
         */
        constructor: function WidgetPickerCollectionView(options) {
            WidgetPickerCollectionView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            if (!options.loadWidget) {
                throw new Error('Missing required \"loadWidget\" option');
            }
            if (!options.filterModel) {
                throw new Error('Missing required \"filterModel\" option');
            }
            _.extend(this, _.pick(options, ['filterModel', 'loadWidget']));
            options.filterer = this.filterModel.filterer.bind(this.filterModel);
            WidgetPickerCollectionView.__super__.initialize.call(this, options);
        },

        /**
         * @inheritdoc
         */
        delegateListeners: function() {
            this.listenTo(this.filterModel, 'change', this.filter);
            return WidgetPickerCollectionView.__super__.delegateListeners.call(this);
        },

        /**
         * @inheritdoc
         */
        initItemView: function(model) {
            const view = WidgetPickerCollectionView.__super__.initItemView.call(this, model);
            this.listenTo(view, 'widget_add', this.processWidgetAdd);
            view.setFilterModel(this.filterModel);
            return view;
        },

        /**
         *
         * @param {WidgetPickerModel} widgetModel
         * @param {WidgetPickerItemView} widgetPickerItemView
         */
        processWidgetAdd: function(widgetModel, widgetPickerItemView) {
            if (!this.isWidgetLoadingInProgress) {
                this.isWidgetLoadingInProgress = true;
                this.loadWidget(widgetModel, this._startLoading());
                widgetPickerItemView.trigger('start_loading');
                _.each(this.getItemViews(), function(itemView) {
                    if (itemView.cid !== widgetPickerItemView.cid) {
                        itemView.trigger('block_add_btn');
                    }
                });
            }
        },

        /**
         *
         * @returns {Function}
         * @protected
         */
        _startLoading: function() {
            return () => {
                if (this.disposed) {
                    return;
                }
                this.isWidgetLoadingInProgress = false;
                _.each(this.getItemViews(), function(itemView) {
                    itemView.trigger('unblock_add_btn');
                });
            };
        }
    });

    return WidgetPickerCollectionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/widget-picker/widget-picker-collection-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/widget-picker/widget-picker-collection-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/widget-picker/widget-picker-collection-view.js");
    }
}
