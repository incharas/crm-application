<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/clearfix.scss */
class __TwigTemplate_e8ffa82ef21665b8a224a6ec30061abe extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@at-root html#{&} {
    overflow-x: hidden;
    overflow-y: auto;

    &.disable-touch-scrolling,
    &.disable-touch-scrolling body {
        overflow: hidden;
        height: 100%;
    }
}

// stylelint-disable-next-line scss/selector-no-redundant-nesting-selector
& {
    min-width: inherit;
    font-size: \$base-font-size;
}

.app-page.hidden-page {
    display: none;
}

.container,
.navbar-static-top .container,
.navbar-fixed-top .container,
.navbar-fixed-bottom .container,
.popup-box-errors {
    width: auto;
}

.dot-menu,
#bookmark-buttons,
#breadcrumb {
    // stylelint-disable-next-line declaration-no-important
    display: none !important;
}

.scrollspy-nav,
.scrollspy-nav-target {
    display: none;
}

&.error-page {
    #container {
        position: inherit;

        .popup-frame {
            position: inherit;
        }

        .popup-holder {
            position: inherit;
            margin: 0;
        }
    }
}

#footer {
    display: none;
}

fieldset {
    min-width: 100%;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/clearfix.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/clearfix.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/clearfix.scss");
    }
}
