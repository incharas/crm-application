<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Twig/dynamicField.html.twig */
class __TwigTemplate_5772ad305128b1ebf7d50816df48c9b3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAttachment/Twig/dynamicField.html.twig", 1)->unwrap();
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroAttachment/Twig/dynamicField.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        $context["type"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "fieldConfigId", [], "any", false, false, false, 4), "fieldType", [], "any", false, false, false, 4);
        // line 5
        $context["fieldName"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "fieldConfigId", [], "any", false, false, false, 5), "fieldName", [], "any", false, false, false, 5);
        // line 6
        $context["className"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "fieldConfigId", [], "any", false, false, false, 6), "className", [], "any", false, false, false, 6);
        // line 7
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "entity", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7);
        // line 8
        $context["value"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "value", [], "any", true, true, false, 8)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "value", [], "any", false, false, false, 8), null)) : (null));
        // line 9
        echo "
";
        // line 10
        if ((($context["type"] ?? null) == "file")) {
            // line 11
            echo "    ";
            ob_start(function () { return ''; });
            // line 12
            echo $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileView($this->env, ($context["value"] ?? null));
            $context["value"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        } elseif ((        // line 14
($context["type"] ?? null) == "image")) {
            // line 15
            echo "    ";
            ob_start(function () { return ''; });
            // line 16
            echo $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getImageView($this->env, ($context["value"] ?? null));
            $context["value"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        }
        // line 19
        echo "
";
        // line 20
        if (((($context["type"] ?? null) == "file") || (($context["type"] ?? null) == "image"))) {
            // line 21
            echo "    ";
            echo twig_escape_filter($this->env, ((array_key_exists("value", $context)) ? (_twig_default_filter(($context["value"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "html", null, true);
            echo "
";
        } else {
            // line 23
            echo "    ";
            $context["gridName"] = (((($context["type"] ?? null) == "multiFile")) ? ("attachment-files-grid") : ("attachment-images-grid"));
            // line 24
            echo "    ";
            $context["gridFullName"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName(($context["gridName"] ?? null), ($context["fieldName"] ?? null));
            // line 25
            echo "    <div class=\"widget-content\">
        ";
            // line 26
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridFullName"] ?? null), ["entityId" => ($context["entityId"] ?? null), "entityField" => ($context["fieldName"] ?? null), "entityTable" => ($context["className"] ?? null)]], 26, $context, $this->getSourceContext());
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Twig/dynamicField.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 26,  91 => 25,  88 => 24,  85 => 23,  79 => 21,  77 => 20,  74 => 19,  70 => 16,  67 => 15,  65 => 14,  62 => 12,  59 => 11,  57 => 10,  54 => 9,  52 => 8,  50 => 7,  48 => 6,  46 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Twig/dynamicField.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Twig/dynamicField.html.twig");
    }
}
