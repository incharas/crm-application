<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Widget/widget/transitionComplete.html.twig */
class __TwigTemplate_6031e787fe88e637652a6e8903ff9386 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["transitionSuccess"] ?? null)) {
            // line 2
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["trigger" => [0 => ["eventBroker" => "widget", "name" => "transitionSuccess", "args" => [0 =>             // line 7
($context["response"] ?? null)]]]]];
        } else {
            // line 12
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["trigger" => [0 => ["eventBroker" => "widget", "name" => "transitionFailure", "args" => [0 => ["status" =>             // line 18
($context["responseCode"] ?? null), "message" =>             // line 19
($context["responseMessage"] ?? null)]]]]]];
        }
        // line 25
        echo "
";
        // line 26
        echo json_encode(($context["widgetResponse"] ?? null));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/widget/transitionComplete.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 26,  50 => 25,  47 => 19,  46 => 18,  44 => 12,  41 => 7,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/widget/transitionComplete.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/widget/transitionComplete.html.twig");
    }
}
