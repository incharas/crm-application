<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Index/default.html.twig */
class __TwigTemplate_05de0943c870ea1299615790cfc79f21 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'content' => [$this, 'block_content'],
            'title' => [$this, 'block_title'],
            'navButtons' => [$this, 'block_navButtons'],
            'titleNavButtons' => [$this, 'block_titleNavButtons'],
            'widgets_content' => [$this, 'block_widgets_content'],
            'widgets' => [$this, 'block_widgets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroDashboard/Index/default.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Index/default.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["dashboard"] ?? null), "getLabel", [], "method", false, false, false, 3)]]);
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    ";
        $context["breadcrumbs"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.menu.dashboards_tab.label")], 1 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["dashboard"] ?? null), "getLabel", [], "method", false, false, false, 7))]];
        // line 9
        echo "    ";
        $this->loadTemplate("@OroNavigation/Menu/breadcrumbs.html.twig", "@OroDashboard/Index/default.html.twig", 9)->display($context);
    }

    // line 11
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        $context["widgetIdPrefix"] = (("dashboard-widget-" . twig_random($this->env)) . "-");
        // line 13
        $context["allowEdit"] = $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dashboard"] ?? null), "entity", [], "any", false, false, false, 13));
        // line 14
        echo "
";
        // line 15
        $context["availableWidgets"] = [];
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["widgets"] ?? null));
        foreach ($context['_seq'] as $context["widgetName"] => $context["widget"]) {
            // line 17
            echo "    ";
            if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "acl", [], "any", true, true, false, 17) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "acl", [], "any", false, false, false, 17)))) {
                // line 18
                echo "        ";
                $context["icon"] = $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "icon", [], "any", true, true, false, 18)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "icon", [], "any", false, false, false, 18), "bundles/orodashboard/img/no_icon.png")) : ("bundles/orodashboard/img/no_icon.png")));
                // line 19
                echo "        ";
                $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "label", [], "any", false, false, false, 19));
                // line 20
                echo "        ";
                $context["description"] = "";
                // line 21
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "description", [], "any", true, true, false, 21)) {
                    // line 22
                    echo "            ";
                    $context["description"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "description", [], "any", false, false, false, 22));
                    // line 23
                    echo "        ";
                }
                // line 24
                echo "        ";
                $context["availableWidgets"] = twig_array_merge(($context["availableWidgets"] ?? null), [0 => ["dialogIcon" =>                 // line 25
($context["icon"] ?? null), "iconClass" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 26
$context["widget"], "icon_class", [], "any", true, true, false, 26)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "icon_class", [], "any", false, false, false, 26), null)) : (null)), "title" =>                 // line 27
($context["title"] ?? null), "widgetName" =>                 // line 28
$context["widgetName"], "description" =>                 // line 29
($context["description"] ?? null), "isNew" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 30
$context["widget"], "isNew", [], "any", false, false, false, 30), "configurationDialogOptions" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 31
$context["widget"], "configuration_dialog_options", [], "any", false, false, false, 31)]]);
                // line 33
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['widgetName'], $context['widget'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 35
        echo "
";
        // line 36
        $context["widgetIds"] = [];
        // line 37
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dashboard"] ?? null), "widgets", [], "any", false, false, false, 37));
        foreach ($context['_seq'] as $context["_key"] => $context["widget"]) {
            // line 38
            echo "    ";
            if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "config", [], "any", false, true, false, 38), "acl", [], "any", true, true, false, 38) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "config", [], "any", false, false, false, 38), "acl", [], "any", false, false, false, 38)))) {
                // line 39
                echo "        ";
                $context["widgetIds"] = twig_array_merge(($context["widgetIds"] ?? null), [0 => (($context["widgetIdPrefix"] ?? null) . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "id", [], "any", false, false, false, 39))]);
                // line 40
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['widget'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 42
        echo "
";
        // line 43
        $context["dashboardContainerOptions"] = ["widgetIds" =>         // line 44
($context["widgetIds"] ?? null), "dashboardId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 45
($context["dashboard"] ?? null), "id", [], "any", false, false, false, 45), "columnsSelector" => ".dashboard-column", "allowEdit" => ((        // line 47
($context["allowEdit"] ?? null)) ? ("true") : ("false")), "availableWidgets" =>         // line 48
($context["availableWidgets"] ?? null)];
        // line 50
        echo "
";
        // line 51
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Index/default.html.twig", 51)->unwrap();
        // line 52
        echo "
<div class=\"layout-content dashboard-container-wrapper\" ";
        // line 53
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orodashboard/js/app/views/dashboard-container-view", "options" =>         // line 55
($context["dashboardContainerOptions"] ?? null)]], 53, $context, $this->getSourceContext());
        // line 56
        echo ">
    <div class=\"container-fluid page-title\">
        <div class=\"navigation navbar-extra navbar-extra-right\">
            <div class=\"row\">
                ";
        // line 60
        $this->displayBlock('title', $context, $blocks);
        // line 69
        echo "
                ";
        // line 70
        $this->displayBlock('navButtons', $context, $blocks);
        // line 89
        echo "
                ";
        // line 90
        $this->displayBlock('titleNavButtons', $context, $blocks);
        // line 148
        echo "            </div>
        </div>
    </div>
    ";
        // line 151
        $this->displayBlock('widgets_content', $context, $blocks);
        // line 181
        echo "</div>
";
    }

    // line 60
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 61
        echo "                <div class=\"pull-left pull-left-extra\">
                    <div class=\"pull-left\">
                        <h1 class=\"oro-subtitle\">
                            ";
        // line 64
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dashboard"] ?? null), "getLabel", [], "method", false, false, false, 64)), "html", null, true);
        echo "
                        </h1>
                    </div>
                </div>
                ";
    }

    // line 70
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "                    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Index/default.html.twig", 71)->unwrap();
        // line 72
        echo "
                    ";
        // line 73
        if ((twig_length_filter($this->env, ($context["dashboards"] ?? null)) > 1)) {
            // line 74
            echo "                        <div class=\"dashboard-selector-container pull-right\">
                            <label for=\"dashboard_selector\">";
            // line 75
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.entity_plural_label"), "html", null, true);
            echo ":</label>
                            <select id=\"dashboard_selector\" ";
            // line 76
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orodashboard/js/app/views/dashboard-change-view"]], 76, $context, $this->getSourceContext());
            // line 78
            echo ">
                                ";
            // line 79
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["dashboards"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["dashboardModel"]) {
                // line 80
                echo "                                    <option value=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["dashboardModel"], "id", [], "any", false, false, false, 80), "html", null, true);
                echo "\"";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["dashboardModel"], "id", [], "any", false, false, false, 80) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dashboard"] ?? null), "id", [], "any", false, false, false, 80))) {
                    echo " selected=\"selected\"";
                }
                echo ">
                                        ";
                // line 81
                $context["translatedLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["dashboardModel"], "getLabel", [], "method", false, false, false, 81));
                // line 82
                echo "                                        ";
                echo twig_escape_filter($this->env, (((twig_length_filter($this->env, ($context["translatedLabel"] ?? null)) <= 50)) ? (($context["translatedLabel"] ?? null)) : ((twig_trim_filter(twig_slice($this->env, ($context["translatedLabel"] ?? null), 0, 50), null, "right") . "..."))), "html", null, true);
                echo "
                                    </option>
                                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dashboardModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 85
            echo "                            </select>
                        </div>
                    ";
        }
        // line 88
        echo "                ";
    }

    // line 90
    public function block_titleNavButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 91
        echo "
                    ";
        // line 92
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("dashboard_navButtons_before", $context)) ? (_twig_default_filter(($context["dashboard_navButtons_before"] ?? null), "dashboard_navButtons_before")) : ("dashboard_navButtons_before")), array());
        // line 93
        echo "
                    <div class=\"pull-right title-buttons-container\">
                        ";
        // line 95
        if (($context["allowEdit"] ?? null)) {
            // line 96
            echo "                            <a href=\"#\" class=\"dashboard-widgets-add btn main-group\">
                                <span class=\"fa-plus\"></span>";
            // line 98
            echo twig_escape_filter($this->env, twig_trim_filter($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.add_dashboard_widgets.add_widget")), "html", null, true);
            // line 99
            echo "</a>
                        ";
        }
        // line 101
        echo "
                        ";
        // line 102
        ob_start(function () { return ''; });
        // line 103
        echo "                            ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dashboard"] ?? null), "entity", [], "any", false, false, false, 103)) && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\DashboardBundle\\Entity\\Dashboard", "entities"))) {
            // line 104
            echo "                                ";
            echo twig_call_macro($macros["UI"], "macro_dropdownItem", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 105
($context["dashboard"] ?? null), "id", [], "any", false, false, false, 105)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.edit_dashboard_link.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.edit_dashboard_link.text"), "aCss" => "dropdown-item", "iCss" => "fa-pencil-square-o"]], 104, $context, $this->getSourceContext());
            // line 110
            echo "
                            ";
        }
        // line 112
        echo "                            ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_dashboard_create") && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\DashboardBundle\\Entity\\Dashboard", "entities"))) {
            // line 113
            echo "                                ";
            echo twig_call_macro($macros["UI"], "macro_dropdownItem", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_create"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.create_dashboard_link.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.create_dashboard_link.text"), "aCss" => "dropdown-item", "iCss" => "fa-plus"]], 113, $context, $this->getSourceContext());
            // line 119
            echo "
                            ";
        }
        // line 121
        echo "                            ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dashboard"] ?? null), "entity", [], "any", false, false, false, 121)) && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\DashboardBundle\\Entity\\Dashboard", "entities"))) {
            // line 122
            echo "                                <li>
                                    ";
            // line 123
            echo twig_call_macro($macros["UI"], "macro_deleteLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_dashboard", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 124
($context["dashboard"] ?? null), "id", [], "any", false, false, false, 124)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_index"), "aCss" => "no-hash remove-button dropdown-item", "id" => "btn-remove-user", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 128
($context["dashboard"] ?? null), "id", [], "any", false, false, false, 128), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.entity_label")]], 123, $context, $this->getSourceContext());
            // line 130
            echo "
                                </li>
                            ";
        }
        // line 133
        echo "                        ";
        $context["html"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 134
        echo "
                        ";
        // line 135
        if ( !twig_test_empty(twig_trim_filter(($context["html"] ?? null)))) {
            // line 136
            echo "                            ";
            echo twig_call_macro($macros["UI"], "macro_dropdownButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.tools_dropdown.label"), "iCss" => "fa-cog", "aCss" => "pull-right", "html" =>             // line 140
($context["html"] ?? null)]], 136, $context, $this->getSourceContext());
            // line 141
            echo "
                        ";
        }
        // line 143
        echo "                    </div>

                    ";
        // line 145
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("dashboard_navButtons_after", $context)) ? (_twig_default_filter(($context["dashboard_navButtons_after"] ?? null), "dashboard_navButtons_after")) : ("dashboard_navButtons_after")), array());
        // line 146
        echo "
                ";
    }

    // line 151
    public function block_widgets_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 152
        echo "        ";
        $context["contentClass"] = ((array_key_exists("contentClass", $context)) ? (_twig_default_filter(($context["contentClass"] ?? null), "dashboard-container")) : ("dashboard-container"));
        // line 153
        echo "        <div class=\"scrollable-container\">
            <div class=\"responsive-section ";
        // line 154
        echo twig_escape_filter($this->env, ($context["contentClass"] ?? null), "html", null, true);
        echo "\">
                <div class=\"clearfix\">
                ";
        // line 156
        $this->displayBlock('widgets', $context, $blocks);
        // line 177
        echo "                </div>
            </div>
        </div>
    ";
    }

    // line 156
    public function block_widgets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 157
        echo "                    ";
        $macros["dashboardMacros"] = $this;
        // line 158
        echo "                    ";
        echo twig_call_macro($macros["dashboardMacros"], "macro_renderWidgetsColumn", [["widgets" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 160
($context["dashboard"] ?? null), "getOrderedColumnWidgets", [0 => 0, 1 => false, 2 => true], "method", false, false, false, 160), "columnElementId" => "dashboard-column-0", "columnClass" => "dashboard-column", "widgetIdPrefix" =>         // line 163
($context["widgetIdPrefix"] ?? null), "allowEdit" =>         // line 164
($context["allowEdit"] ?? null)]], 159, $context, $this->getSourceContext());
        // line 166
        echo "
                    ";
        // line 167
        echo twig_call_macro($macros["dashboardMacros"], "macro_renderWidgetsColumn", [["widgets" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 169
($context["dashboard"] ?? null), "getOrderedColumnWidgets", [0 => 1, 1 => true, 2 => false], "method", false, false, false, 169), "columnElementId" => "dashboard-column-1", "columnClass" => "dashboard-column", "widgetIdPrefix" =>         // line 172
($context["widgetIdPrefix"] ?? null), "allowEdit" =>         // line 173
($context["allowEdit"] ?? null)]], 168, $context, $this->getSourceContext());
        // line 175
        echo "
                ";
    }

    // line 184
    public function macro_renderWidgetsColumn($__options__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "options" => $__options__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 185
            echo "    <div id=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "columnElementId", [], "any", false, false, false, 185), "html", null, true);
            echo "\" class=\"responsive-cell dashboard-column\">
        ";
            // line 186
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "widgets", [], "any", false, false, false, 186));
            foreach ($context['_seq'] as $context["_key"] => $context["widget"]) {
                // line 187
                echo "            ";
                if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "config", [], "any", false, true, false, 187), "acl", [], "any", true, true, false, 187) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "config", [], "any", false, false, false, 187), "acl", [], "any", false, false, false, 187)))) {
                    // line 188
                    echo "                ";
                    echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "dashboard-item", "wid" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 191
($context["options"] ?? null), "widgetIdPrefix", [], "any", false, false, false, 191) . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "id", [], "any", false, false, false, 191)), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 192
$context["widget"], "config", [], "any", false, false, false, 192), "route", [], "any", false, false, false, 192), twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "config", [], "any", false, false, false, 192), "route_parameters", [], "any", false, false, false, 192), ["_widgetId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "id", [], "any", false, false, false, 192)])), "state" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 194
$context["widget"], "id", [], "any", false, false, false, 194), "expanded" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 195
$context["widget"], "expanded", [], "any", false, false, false, 195), "layoutPosition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 196
$context["widget"], "layoutPosition", [], "any", false, false, false, 196)], "allowEdit" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 198
($context["options"] ?? null), "allowEdit", [], "any", false, false, false, 198), "showConfig" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 199
($context["options"] ?? null), "allowEdit", [], "any", false, false, false, 199) && (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["widget"], "config", [], "any", false, false, false, 199), "configuration", [], "any", false, false, false, 199)) > 0)), "widgetName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 200
$context["widget"], "entity", [], "any", false, false, false, 200), "name", [], "any", false, false, false, 200), "configurationDialogOptions" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 201
$context["widget"], "config", [], "any", false, false, false, 201), "configuration_dialog_options", [], "any", false, false, false, 201)]);
                    // line 203
                    echo "
            ";
                }
                // line 205
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['widget'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 206
            echo "        <div class=\"empty-text";
            if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "widgets", [], "any", false, false, false, 206)) > 0)) {
                echo " hidden-empty-text";
            }
            echo "\">
            <div class=\"widget-placeholder\">
                ";
            // line 208
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "allowEdit", [], "any", false, false, false, 208)) {
                // line 209
                echo "                    ";
                echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.empty_column_message.allowed");
                echo "
                ";
            } else {
                // line 211
                echo "                    ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.empty_column_message.denied"), "html", null, true);
                echo "
                ";
            }
            // line 213
            echo "            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Index/default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  484 => 213,  478 => 211,  472 => 209,  470 => 208,  462 => 206,  456 => 205,  452 => 203,  450 => 201,  449 => 200,  448 => 199,  447 => 198,  446 => 196,  445 => 195,  444 => 194,  443 => 192,  442 => 191,  440 => 188,  437 => 187,  433 => 186,  428 => 185,  415 => 184,  410 => 175,  408 => 173,  407 => 172,  406 => 169,  405 => 167,  402 => 166,  400 => 164,  399 => 163,  398 => 160,  396 => 158,  393 => 157,  389 => 156,  382 => 177,  380 => 156,  375 => 154,  372 => 153,  369 => 152,  365 => 151,  360 => 146,  358 => 145,  354 => 143,  350 => 141,  348 => 140,  346 => 136,  344 => 135,  341 => 134,  338 => 133,  333 => 130,  331 => 128,  330 => 124,  329 => 123,  326 => 122,  323 => 121,  319 => 119,  316 => 113,  313 => 112,  309 => 110,  307 => 105,  305 => 104,  302 => 103,  300 => 102,  297 => 101,  293 => 99,  291 => 98,  288 => 96,  286 => 95,  282 => 93,  280 => 92,  277 => 91,  273 => 90,  269 => 88,  264 => 85,  254 => 82,  252 => 81,  243 => 80,  239 => 79,  236 => 78,  234 => 76,  230 => 75,  227 => 74,  225 => 73,  222 => 72,  219 => 71,  215 => 70,  206 => 64,  201 => 61,  197 => 60,  192 => 181,  190 => 151,  185 => 148,  183 => 90,  180 => 89,  178 => 70,  175 => 69,  173 => 60,  167 => 56,  165 => 55,  164 => 53,  161 => 52,  159 => 51,  156 => 50,  154 => 48,  153 => 47,  152 => 45,  151 => 44,  150 => 43,  147 => 42,  140 => 40,  137 => 39,  134 => 38,  130 => 37,  128 => 36,  125 => 35,  118 => 33,  116 => 31,  115 => 30,  114 => 29,  113 => 28,  112 => 27,  111 => 26,  110 => 25,  108 => 24,  105 => 23,  102 => 22,  99 => 21,  96 => 20,  93 => 19,  90 => 18,  87 => 17,  83 => 16,  81 => 15,  78 => 14,  76 => 13,  74 => 12,  70 => 11,  65 => 9,  63 => 7,  61 => 5,  57 => 4,  53 => 1,  51 => 3,  48 => 2,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Index/default.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Index/default.html.twig");
    }
}
