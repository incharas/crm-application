<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/attribute-item.scss */
class __TwigTemplate_2ab470233e78ea333f18214bfa0acbd2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$attribute-item-display: flex !default;
\$attribute-item-offset: 0 0 10px !default;

\$attribute-item-term-width: 45% !default;
\$attribute-item-term-max-width: 200px !default;
\$attribute-item-term-color: \$primary-550 !default;
\$attribute-item-term-text-align: right !default;

\$attribute-item-description-offset-start: \$content-padding * .5 !default;
\$attribute-item-description-text-align: left !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/attribute-item.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/attribute-item.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/attribute-item.scss");
    }
}
