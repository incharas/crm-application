<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/macros.html.twig */
class __TwigTemplate_a912413bbb1a44c40b6fbdfb335f38ad extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_renderDateWidgeView($__form__ = null, $__valueType__ = null, $__className__ = null, $__viewType__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "valueType" => $__valueType__,
            "className" => $__className__,
            "viewType" => $__viewType__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    <div class=\"datetime-range-filter filter-criteria ";
            echo twig_escape_filter($this->env, ($context["className"] ?? null), "html", null, true);
            echo "-range-filter-";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 2), "name", [], "any", false, false, false, 2), "html", null, true);
            echo "\">
        <!-- datetime range filter placeholder -->
        <input type=\"hidden\" name=\"";
            // line 4
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "full_name", [], "any", false, false, false, 4), "html", null, true);
            echo "[part]\" value=\"value\"/>
    </div>

    ";
            // line 7
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/macros.html.twig", 7)->unwrap();
            // line 8
            echo "
    ";
            // line 9
            $context["options"] = ["_sourceElement" => "div.widget-configuration", "metadata" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["form"] ?? null), "vars", [], "any", false, false, false, 11), "datetime_range_metadata", [], "any", false, false, false, 11), "formName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 12
($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "name", [], "any", false, false, false, 12), "formFullName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "full_name", [], "any", false, false, false, 13), "valueType" =>             // line 14
($context["valueType"] ?? null)];
            // line 16
            echo "
    ";
            // line 17
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 17), "value", [], "any", false, false, false, 17), "value", [], "any", false, false, false, 17) != null)) {
                // line 18
                echo "        ";
                $context["options"] = twig_array_merge(($context["options"] ?? null), ["valueConfig" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 19
($context["form"] ?? null), "vars", [], "any", false, false, false, 19), "value", [], "any", false, false, false, 19), "value", [], "any", false, false, false, 19)]);
                // line 21
                echo "    ";
            }
            // line 22
            echo "
    <div ";
            // line 23
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" =>             // line 24
($context["viewType"] ?? null), "options" =>             // line 25
($context["options"] ?? null)]], 23, $context, $this->getSourceContext());
            // line 26
            echo "></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroDashboard/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 26,  97 => 25,  96 => 24,  95 => 23,  92 => 22,  89 => 21,  87 => 19,  85 => 18,  83 => 17,  80 => 16,  78 => 14,  77 => 13,  76 => 12,  75 => 11,  74 => 9,  71 => 8,  69 => 7,  63 => 4,  55 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/macros.html.twig");
    }
}
