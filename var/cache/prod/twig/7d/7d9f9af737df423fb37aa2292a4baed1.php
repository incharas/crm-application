<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/jstree-wrapper.scss */
class __TwigTemplate_44a7dabc0c6c9a1b9c1f643b5e390b58 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$jstree-wrapper-position: relative !default;
\$jstree-wrapper-min-height: 200px !default;
\$jstree-wrapper-inner-offset: 10px 0 !default;
\$jstree-wrapper-width: 100% !default;

\$jstree-wrapper-inner-height: 100% !default;
\$jstree-wrapper-inner-display: flex !default;
\$jstree-wrapper-inner-flex-direction: column !default;

\$jstree-wrapper-content-min-height: 0 !default;
\$jstree-wrapper-content-display: flex !default;
\$jstree-wrapper-content-flex-direction: column !default;

\$jstree-wrapper-title-display: flex !default;
\$jstree-wrapper-title-align-items: center !default;
\$jstree-wrapper-title-justify-content: space-between !default;

\$jstree-wrapper-label-display: flex !default;
\$jstree-wrapper-label-offset-top: 6px !default;
\$jstree-wrapper-label-offset-bottom: 8px !default;

\$jstree-wrapper-label-expanded-jstree-actions-display: inline-block !default;

\$jstree-wrapper-text-font-size: 18px !default;
\$jstree-wrapper-text-font-weight: font-weight('bold') !default;
\$jstree-wrapper-text-cursor: pointer !default;
\$jstree-wrapper-text-color: \$primary-200 !default;

\$jstree-wrapper-checkbox-display: none !default;

\$jstree-wrapper-jstree-offset: 8px 0 !default;
\$jstree-wrapper-jstree-outline: none !default;

\$content-with-sidebar-jstree-wrapper-height: 100% !default;
\$content-with-sidebar-jstree-wrapper-inner-offset: 0 \$content-padding !default;
\$sidebar-container-content-with-sidebar-jstree-wrapper-min-height: 0 !default;
\$sidebar-container-content-with-sidebar-jstree-wrapper-overflow: auto !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/jstree-wrapper.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/jstree-wrapper.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/jstree-wrapper.scss");
    }
}
