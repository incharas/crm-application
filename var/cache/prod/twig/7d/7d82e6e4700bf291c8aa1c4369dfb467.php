<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/SystemCalendar/view.html.twig */
class __TwigTemplate_04e22b188b3570e16439dd712be60e49 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/SystemCalendar/view.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroCalendar/SystemCalendar/view.html.twig", 3)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.title%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["entity"] ?? null), "name", [], "any", false, false, false, 5)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroCalendar/SystemCalendar/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/SystemCalendar/view.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        if (($context["canAddEvent"] ?? null)) {
            // line 11
            echo "        <div class=\"btn-group\">
            ";
            // line 12
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_event_create", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["entity"] ?? null), "id", [], "any", false, false, false, 13)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_label")]], 12, $context, $this->getSourceContext());
            // line 15
            echo "
        </div>
    ";
        }
        // line 18
        echo "    ";
        if (($context["editable"] ?? null)) {
            // line 19
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["entity"] ?? null), "id", [], "any", false, false, false, 20)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.entity_label")]], 19, $context, $this->getSourceContext());
            // line 22
            echo "
    ";
        }
        // line 24
        echo "    ";
        if (($context["removable"] ?? null)) {
            // line 25
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_systemcalendar", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["entity"] ?? null), "id", [], "any", false, false, false, 26)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_index"), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 29
($context["entity"] ?? null), "id", [], "any", false, false, false, 29), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.entity_label")]], 25, $context, $this->getSourceContext());
            // line 31
            echo "
    ";
        }
    }

    // line 35
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 37
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 40
($context["entity"] ?? null), "name", [], "any", false, false, false, 40)];
        // line 42
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 45
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/SystemCalendar/view.html.twig", 46)->unwrap();
        // line 47
        echo "
    ";
        // line 48
        ob_start(function () { return ''; });
        // line 49
        echo "        <div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 51
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 51)], 51, $context, $this->getSourceContext());
        echo "
                ";
        // line 52
        echo twig_call_macro($macros["UI"], "macro_renderColorProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.background_color.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 54
($context["entity"] ?? null), "backgroundColor", [], "any", false, false, false, 54), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.no_color")], 52, $context, $this->getSourceContext());
        // line 55
        echo "
                ";
        // line 56
        if (($context["showScope"] ?? null)) {
            // line 57
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.public.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "public", [], "any", false, false, false, 57)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.scope.system")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.scope.organization")))], 57, $context, $this->getSourceContext());
            // line 59
            echo "
                ";
        }
        // line 61
        echo "            </div>
            <div class=\"responsive-block\">
                ";
        // line 63
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 63, $context, $this->getSourceContext());
        echo "
            </div>
        </div>
    ";
        $context["systemCalendarInformation"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 67
        echo "
    ";
        // line 68
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 72
($context["systemCalendarInformation"] ?? null)]]]]];
        // line 75
        echo "
    ";
        // line 76
        ob_start(function () { return ''; });
        // line 77
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_widget_events", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 79
($context["entity"] ?? null), "id", [], "any", false, false, false, 79)])]);
        // line 80
        echo "
    ";
        $context["systemCalendarEventsWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 82
        echo "
    ";
        // line 83
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_plural_label"), "subblocks" => [0 => ["data" => [0 =>         // line 86
($context["systemCalendarEventsWidget"] ?? null)]]]]]);
        // line 89
        echo "
    ";
        // line 90
        $context["id"] = "systemCalendarView";
        // line 91
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 92
        echo "
    ";
        // line 93
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/SystemCalendar/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  202 => 93,  199 => 92,  196 => 91,  194 => 90,  191 => 89,  189 => 86,  188 => 83,  185 => 82,  181 => 80,  179 => 79,  177 => 77,  175 => 76,  172 => 75,  170 => 72,  169 => 68,  166 => 67,  159 => 63,  155 => 61,  151 => 59,  148 => 57,  146 => 56,  143 => 55,  141 => 54,  140 => 52,  136 => 51,  132 => 49,  130 => 48,  127 => 47,  124 => 46,  120 => 45,  113 => 42,  111 => 40,  110 => 37,  108 => 36,  104 => 35,  98 => 31,  96 => 29,  95 => 26,  93 => 25,  90 => 24,  86 => 22,  84 => 20,  82 => 19,  79 => 18,  74 => 15,  72 => 13,  71 => 12,  68 => 11,  66 => 10,  63 => 9,  60 => 8,  56 => 7,  51 => 1,  49 => 5,  46 => 3,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/SystemCalendar/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/SystemCalendar/view.html.twig");
    }
}
