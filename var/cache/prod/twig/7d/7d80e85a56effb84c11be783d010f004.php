<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Reset/reset.html.twig */
class __TwigTemplate_a4f55138a5168a437a8c19e8eee8e9a0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'bodyClass' => [$this, 'block_bodyClass'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), "@OroUI/Form/login.html.twig", true);
        // line 1
        $this->parent = $this->loadTemplate("@OroUser/layout.html.twig", "@OroUser/Reset/reset.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "login-page";
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    <div class=\"container\">
        <div class=\"form-wrapper\">
            <div class=\"form-wrapper__inner\">
                ";
        // line 10
        $context["resetLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Reset");
        // line 11
        echo "                ";
        $context["cancelLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel");
        // line 12
        echo "                ";
        $context["showLabels"] = ((twig_length_filter($this->env, ($context["resetLabel"] ?? null)) <= 9) && (twig_length_filter($this->env, ($context["cancelLabel"] ?? null)) <= 9));
        // line 13
        echo "                ";
        $context["layoutName"] = ((($context["showLabels"] ?? null)) ? ("form-row-layout") : ("form-column-layout"));
        // line 14
        echo "                ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_reset_reset", ["token" =>         // line 15
($context["token"] ?? null)]), "attr" => ["class" => ("form-vertical form-signin form-signin--reset " .         // line 17
($context["layoutName"] ?? null))]]);
        // line 19
        echo "
                    <div class=\"title-box\">
                        <h2 class=\"title\">";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Password Reset"), "html", null, true);
        echo "</h2>
                    </div>
                    <fieldset class=\"field-set form-signin__fieldset\">
                        ";
        // line 24
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 24), "errors", [], "any", false, false, false, 24)) > 0)) {
            // line 25
            echo "                            <div class=\"alert alert-error\" role=\"alert\">
                                ";
            // line 26
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                            </div>
                        ";
        }
        // line 29
        echo "                        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
                        <div class=\"form-row form-signin__footer\">
                            <button class=\"btn extra-submit btn-uppercase btn-primary\" type=\"submit\">";
        // line 31
        echo twig_escape_filter($this->env, ($context["resetLabel"] ?? null), "html", null, true);
        echo "</button>
                            <a href=\"";
        // line 32
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_security_login");
        echo "\" class=\"btn\">";
        echo twig_escape_filter($this->env, ($context["cancelLabel"] ?? null), "html", null, true);
        echo "</a>
                        </div>
                    </fieldset>
                ";
        // line 35
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
            </div>
            <div class=\"login-copyright\">";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.copyright", ["{{year}}" => twig_date_format_filter($this->env, "now", "Y")]), "html", null, true);
        echo "</div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Reset/reset.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 37,  121 => 35,  113 => 32,  109 => 31,  103 => 29,  97 => 26,  94 => 25,  92 => 24,  86 => 21,  82 => 19,  80 => 17,  79 => 15,  77 => 14,  74 => 13,  71 => 12,  68 => 11,  66 => 10,  61 => 7,  57 => 6,  50 => 4,  45 => 1,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Reset/reset.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Reset/reset.html.twig");
    }
}
