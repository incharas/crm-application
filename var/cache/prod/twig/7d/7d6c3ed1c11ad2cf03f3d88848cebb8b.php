<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/clients.html.twig */
class __TwigTemplate_3d397821abd2601bfd5e3dde1c867e71 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/clients.html.twig", 1)->unwrap();
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroOAuth2Server/Client/clients.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        if ( !($context["encryptionKeysExist"] ?? null)) {
            // line 5
            echo "    <div class=\"alert alert-warning\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.no_encryption_keys"), "html", null, true);
            echo "</div>
";
        }
        // line 7
        echo "<div class=\"oauth-clients-container\">
    ";
        // line 8
        if (($context["creationGranted"] ?? null)) {
            // line 9
            echo "    <div class=\"user-fieldset-block-actions clearfix\">
        <div class=\"pull-right\">
            ";
            // line 11
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_oauth2_server_client_create", ["entityClass" =>             // line 12
($context["entityClass"] ?? null), "entityId" => ($context["entityId"] ?? null)]), "aCss" => "no-hash btn-medium", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.action.add"), "widget" => ["type" => "dialog", "multiple" => false, "reload-grid-name" => "oauth-client-with-owner-grid", "options" => ["alias" => "oauth-client-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.dialog.create_title"), "modal" => true, "width" => 500]]]]], 11, $context, $this->getSourceContext());
            // line 28
            echo "
        </div>
    </div>
    ";
        }
        // line 32
        echo "
    ";
        // line 33
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["oauth-client-with-owner-grid", ["ownerEntityClass" =>         // line 35
($context["entityClass"] ?? null), "ownerEntityId" => ($context["entityId"] ?? null), "_grid_view" => ["_disabled" => true]], ["cssClass" => "inner-grid"]], 33, $context, $this->getSourceContext());
        // line 37
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/clients.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  76 => 37,  74 => 35,  73 => 33,  70 => 32,  64 => 28,  62 => 12,  61 => 11,  57 => 9,  55 => 8,  52 => 7,  46 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/clients.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/clients.html.twig");
    }
}
