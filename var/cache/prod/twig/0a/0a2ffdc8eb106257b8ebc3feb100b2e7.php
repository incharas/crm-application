<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Index/quickLaunchpad.html.twig */
class __TwigTemplate_40bd136b689d03251ca6c649b238308d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'title' => [$this, 'block_title'],
            'titleNavButtons' => [$this, 'block_titleNavButtons'],
            'navButtons' => [$this, 'block_navButtons'],
            'widgets_content' => [$this, 'block_widgets_content'],
            'widgets' => [$this, 'block_widgets'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroDashboard/Index/quickLaunchpad.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"layout-content dashboard-container-wrapper\">
        <div class=\"container-fluid page-title\">
            <div class=\"navigation navbar-extra navbar-extra-right\">
                <div class=\"row\">
                    ";
        // line 8
        $this->displayBlock('title', $context, $blocks);
        // line 15
        echo "                    ";
        $this->displayBlock('titleNavButtons', $context, $blocks);
        // line 43
        echo "                </div>
            </div>
        </div>
        ";
        // line 46
        $this->displayBlock('widgets_content', $context, $blocks);
        // line 60
        echo "    </div>
";
    }

    // line 8
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "                        <div class=\"pull-left pull-left-extra\">
                            <div class=\"pull-left\">
                                <h1 class=\"oro-subtitle\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.title.quick_launchpad"), "html", null, true);
        echo "</h1>
                            </div>
                        </div>
                    ";
    }

    // line 15
    public function block_titleNavButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "                        <div class=\"pull-right title-buttons-container\">
                            ";
        // line 17
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("dashboard_navButtons_before", $context)) ? (_twig_default_filter(($context["dashboard_navButtons_before"] ?? null), "dashboard_navButtons_before")) : ("dashboard_navButtons_before")), array());
        // line 18
        echo "
                            ";
        // line 19
        $this->displayBlock('navButtons', $context, $blocks);
        // line 40
        echo "                            ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("dashboard_navButtons_after", $context)) ? (_twig_default_filter(($context["dashboard_navButtons_after"] ?? null), "dashboard_navButtons_after")) : ("dashboard_navButtons_after")), array());
        // line 41
        echo "                        </div>
                    ";
    }

    // line 19
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "                                ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Index/quickLaunchpad.html.twig", 20)->unwrap();
        // line 21
        echo "
                                ";
        // line 22
        if ((twig_length_filter($this->env, ($context["dashboards"] ?? null)) > 1)) {
            // line 23
            echo "                                    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Index/quickLaunchpad.html.twig", 23)->unwrap();
            // line 24
            echo "
                                    <div class=\"dashboard-selector-container pull-left\">
                                        <label for=\"dashboard_selector\">";
            // line 26
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.entity_plural_label"), "html", null, true);
            echo ":</label>
                                        <select id=\"dashboard_selector\" ";
            // line 27
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orodashboard/js/app/views/dashboard-change-view"]], 27, $context, $this->getSourceContext());
            // line 29
            echo ">
                                            <option>";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.title.chose_dashboard"), "html", null, true);
            echo "</option>
                                            ";
            // line 31
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["dashboards"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["dashboardModel"]) {
                // line 32
                echo "                                                <option value=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["dashboardModel"], "id", [], "any", false, false, false, 32), "html", null, true);
                echo "\">
                                                    ";
                // line 33
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["dashboardModel"], "getLabel", [], "method", false, false, false, 33)), "html", null, true);
                echo "
                                                </option>
                                            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['dashboardModel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "                                        </select>
                                    </div>
                                ";
        }
        // line 39
        echo "                            ";
    }

    // line 46
    public function block_widgets_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "            <div class=\"container-fluid scrollable-container\">
                <div class=\"row launchpad-container\">
                ";
        // line 49
        $this->displayBlock('widgets', $context, $blocks);
        // line 57
        echo "                </div>
            </div>
        ";
    }

    // line 49
    public function block_widgets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 50
        echo "                    <div class=\"launchpad-first-column\">
                        ";
        // line 51
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("quick_launchpad_left_column", $context)) ? (_twig_default_filter(($context["quick_launchpad_left_column"] ?? null), "quick_launchpad_left_column")) : ("quick_launchpad_left_column")), array());
        // line 52
        echo "                    </div>
                    <div class=\"launchpad-second-column\">
                        ";
        // line 54
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("quick_launchpad_right_column", $context)) ? (_twig_default_filter(($context["quick_launchpad_right_column"] ?? null), "quick_launchpad_right_column")) : ("quick_launchpad_right_column")), array());
        // line 55
        echo "                    </div>
                ";
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Index/quickLaunchpad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  205 => 55,  203 => 54,  199 => 52,  197 => 51,  194 => 50,  190 => 49,  184 => 57,  182 => 49,  178 => 47,  174 => 46,  170 => 39,  165 => 36,  156 => 33,  151 => 32,  147 => 31,  143 => 30,  140 => 29,  138 => 27,  134 => 26,  130 => 24,  127 => 23,  125 => 22,  122 => 21,  119 => 20,  115 => 19,  110 => 41,  107 => 40,  105 => 19,  102 => 18,  100 => 17,  97 => 16,  93 => 15,  85 => 11,  81 => 9,  77 => 8,  72 => 60,  70 => 46,  65 => 43,  62 => 15,  60 => 8,  54 => 4,  50 => 3,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Index/quickLaunchpad.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Index/quickLaunchpad.html.twig");
    }
}
