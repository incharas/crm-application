<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/mobile/form.scss */
class __TwigTemplate_4866f4a40a5c2c94375685e5486ef361 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.section-content {
    margin-top: \$section-content-mobile-offset-top;
    padding: \$section-content-mobile-inner-offset;

    .row-fluid {
        margin: \$row-fluid-mobile-inner-offset;
    }
}

fieldset .span6 {
    margin-left: 0;
    margin-right: 10px;
}

.responsive-form-inner {
    .input-append {
        display: flex;
        flex-grow: 1;
    }

    .float-holder {
        width: calc(100% - 44px);
        margin-right: \$content-padding-small;
    }

    input[type='text']:not(.precision, .conversionRate),
    input[type='email'],
    input[type='password'] {
        width: 100%;
    }

    .control-group-checkbox {
        .controls {
            right: auto;
            left: 0;
        }
    }

    .control-label.wrap {
        + .controls {
            /* stylelint-disable selector-max-compound-selectors, declaration-no-important */
            > .input-widget-select,
            > .input-widget-select span {
                // Override js width setting
                width: 100% !important;
            }
            /* stylelint-enable selector-max-compound-selectors, declaration-no-important */
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/mobile/form.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/mobile/form.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/mobile/form.scss");
    }
}
