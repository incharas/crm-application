<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImportExport/ImportExport/widget/configurableExport.html.twig */
class __TwigTemplate_8982d9df18f1dd6f4c6ef52faf8133ab extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroImportExport/ImportExport/widget/configurableExport.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["exportConfigurableWidgetViewOptions"] = ["view" => ["view" => "oroimportexport/js/app/views/export-configurable-widget-view", "wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["app"] ?? null), "request", [], "any", false, false, false, 6), "get", [0 => "_wid"], "method", false, false, false, 6), "errorMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Errors occured during file export.")]];
        // line 10
        echo "
<div class=\"widget-content import-widget-content\">
    ";
        // line 12
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("configurable_export_messages", $context)) ? (_twig_default_filter(($context["configurable_export_messages"] ?? null), "configurable_export_messages")) : ("configurable_export_messages")), ["entityClass" => ($context["entityName"] ?? null), "options" => ($context["options"] ?? null), "exportJob" => ($context["exportJob"] ?? null)]);
        // line 13
        echo "
    <div class=\"form-container\" ";
        // line 14
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["exportConfigurableWidgetViewOptions"] ?? null)], 14, $context, $this->getSourceContext());
        echo ">
        ";
        // line 15
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_importexport_export_config", ["entity" =>         // line 16
($context["entityName"] ?? null), "options" => ($context["options"] ?? null), "exportJob" => ($context["exportJob"] ?? null)]), "attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 18
($context["form"] ?? null), "vars", [], "any", false, false, false, 18), "id", [], "any", false, false, false, 18), "data-nohash" => "true", "class" => "form-horizontal"]]);
        // line 22
        echo "

            <fieldset class=\"form\">
                <div>
                    ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "processorAlias", [], "any", false, false, false, 26), 'row');
        echo "
                </div>
                ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
            </fieldset>

            <div class=\"widget-actions\">
                <button class=\"btn\" type=\"reset\">";
        // line 32
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
                <button class=\"btn btn-primary\" type=\"submit\">
                    ";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export.popup.button.label"), "html", null, true);
        echo "
                </button>
            </div>
        ";
        // line 37
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
        ";
        // line 38
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroImportExport/ImportExport/widget/configurableExport.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  95 => 38,  91 => 37,  85 => 34,  80 => 32,  73 => 28,  68 => 26,  62 => 22,  60 => 18,  59 => 16,  58 => 15,  54 => 14,  51 => 13,  49 => 12,  45 => 10,  43 => 6,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImportExport/ImportExport/widget/configurableExport.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Resources/views/ImportExport/widget/configurableExport.html.twig");
    }
}
