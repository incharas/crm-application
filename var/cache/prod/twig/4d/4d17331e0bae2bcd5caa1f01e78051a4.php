<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContact/Dashboard/contactsLaunchpad.html.twig */
class __TwigTemplate_2e1399158801f830402a143e20f67844 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/launchpad.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["widgetName"] = "contacts_launchpad";
        // line 4
        $context["widgetLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.entity_plural_label");
        // line 5
        $context["widgetIcon"] = "users";
        // line 6
        $context["widgetAcl"] = "oro_contact_view";
        // line 8
        $context["items"] = ["index" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.list"), "route" => "oro_contact_index", "acl" => "oro_contact_view"], "create" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.entity_label")]), "route" => "oro_contact_create", "acl" => "oro_contact_create"], "group" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.group.manage"), "route" => "oro_contact_group_index", "acl" => "oro_contact_group_view"]];
        // line 1
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/launchpad.html.twig", "@OroContact/Dashboard/contactsLaunchpad.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroContact/Dashboard/contactsLaunchpad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 1,  49 => 8,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContact/Dashboard/contactsLaunchpad.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactBundle/Resources/views/Dashboard/contactsLaunchpad.html.twig");
    }
}
