<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/jstree-actions-module.js */
class __TwigTemplate_734554586b04906ac685b833ef83c2e2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import ActionManager from 'oroui/js/jstree-action-manager';
import ExpandActionView from 'oroui/js/app/views/jstree/expand-action-view';
import CollapseActionView from 'oroui/js/app/views/jstree/collapse-action-view';
import MoveActionView from 'oroui/js/app/views/jstree/move-action-view';

/**
* Register actions for all jstree in application
* @example
* You can give ActionManager a few hook types, like a string
* ActionManager.addAction('action name', {
*    view: SomeActionView,
*    isAvailable: function(options) {} - should return true/false
});
**/

ActionManager.addAction('expand', {
    view: ExpandActionView
});

ActionManager.addAction('collapse', {
    view: CollapseActionView
});

ActionManager.addAction('move', {
    view: MoveActionView,
    isAvailable: function(options) {
        const move = options.actions.move || {};
        return options.\$tree.data('treeView').checkboxEnabled && move.routeName;
    }
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/jstree-actions-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/jstree-actions-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/jstree-actions-module.js");
    }
}
