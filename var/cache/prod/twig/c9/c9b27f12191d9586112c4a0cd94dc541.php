<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImportExport/ImportExport/import-button.html.twig */
class __TwigTemplate_59b9975b49f03a0f70bc8a6127ee6fec extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["showImportButton"] = false;
        // line 2
        echo "
";
        // line 3
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Oro\Bundle\ImportExportBundle\Twig\GetImportExportConfigurationExtension']->getConfiguration(($context["alias"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["configuration"]) {
            // line 4
            echo "    ";
            if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "importProcessorAlias", [], "any", true, true, false, 4) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 5
$context["configuration"], "importProcessorAlias", [], "any", false, false, false, 5))) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_importexport_import")) && ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(("CREATE;entity:" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 7
$context["configuration"], "entityClass", [], "any", false, false, false, 7))) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(("EDIT;entity:" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "entityClass", [], "any", false, false, false, 7)))))) {
                // line 9
                echo "        ";
                $context["showImportButton"] = true;
                // line 10
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['configuration'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "
";
        // line 13
        if (($context["showImportButton"] ?? null)) {
            // line 14
            echo "    <div class=\"btn-group\">
        <a href=\"#\" role=\"button\"
            class=\"btn import-btn icons-holder-text no-hash\"
            data-page-component-module=\"oroui/js/app/components/view-component\"
            data-page-component-options=\"";
            // line 18
            echo twig_escape_filter($this->env, json_encode(["view" => "oroimportexport/js/app/views/import-button-view", "alias" =>             // line 20
($context["alias"] ?? null), "routeOptions" => ((            // line 21
array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([]))]), "html", null, true);
            // line 22
            echo "\"
        >
            <span class=\"fa-download hide-text\">";
            // line 24
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.label"), "html", null, true);
            echo "</span>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.label"), "html", null, true);
            echo "
        </a>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroImportExport/ImportExport/import-button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  80 => 24,  76 => 22,  74 => 21,  73 => 20,  72 => 18,  66 => 14,  64 => 13,  61 => 12,  54 => 10,  51 => 9,  49 => 7,  48 => 5,  46 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImportExport/ImportExport/import-button.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Resources/views/ImportExport/import-button.html.twig");
    }
}
