<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroForm/Layout/widgetForm.html.twig */
class __TwigTemplate_ae29d1e46cc7924dd8eb173e5b5712b4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget-content\">
    ";
        // line 2
        $this->displayBlock('content', $context, $blocks);
        // line 4
        echo "</div>
";
    }

    // line 2
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@OroForm/Layout/widgetForm.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  52 => 3,  48 => 2,  43 => 4,  41 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroForm/Layout/widgetForm.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/views/Layout/widgetForm.html.twig");
    }
}
