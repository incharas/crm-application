<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/User/Autocomplete/Widget/selection.html.twig */
class __TwigTemplate_4937a445d26a97b6c78ede87797d9e6d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<span class=\"select2-result-label-icon\">
    <picture>
        <% if (typeof avatar != 'undefined' && avatar) { %>
             <% _.each(avatar.sources, function(source) { %>
            <source srcset=\"<%- source.srcset %>\" type=\"<%- source.type %>\">
            <% }); %>
            <img src=\"<% if (avatar.src) { %><%- avatar.src %><% } else { %>";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroui/img/avatar-xsmall.png"), "html_attr");
        echo "<% } %>\" width=\"16\" height=\"16\">
        <% } else { %>
            <img src=\"";
        // line 9
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroui/img/avatar-xsmall.png"), "html_attr");
        echo "\" width=\"16\" height=\"16\">
        <% } %>
    </picture>
</span><span class=\"select2-result-label-title\"><% if (id == 'current_user') { %><i><% } %><%- fullName %><% if (id == 'current_user') { %></i><% } %></span>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/User/Autocomplete/Widget/selection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 9,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/User/Autocomplete/Widget/selection.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/User/Autocomplete/Widget/selection.html.twig");
    }
}
