<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCache/Action/invalidate.html.twig */
class __TwigTemplate_565e2302d1007e15c2e8e00f46879022 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'form' => [$this, 'block_form'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroAction/Operation/form.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroAction/Operation/form.html.twig", "@OroCache/Action/invalidate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_form($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        $context["buttonOptions"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["operation"] ?? null), "definition", [], "any", false, false, false, 3), "buttonOptions", [], "any", false, false, false, 3);
        // line 4
        echo "    ";
        $context["pageComponentOptions"] = [];
        // line 5
        echo "    <div class=\"invalidate-cache-content\" data-page-component-module=\"orocache/js/app/components/invalidate-cache-component\">
        ";
        // line 6
        $context["attr"] = ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 6), "id", [], "any", false, false, false, 6), "data-collect" => "true", "class" => "form-dialog invalidate-cache-form"];
        // line 7
        echo "        ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["buttonOptions"] ?? null), "page_component_module", [], "any", true, true, false, 7)) {
            // line 8
            echo "            ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["data-page-component-module" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["buttonOptions"] ?? null), "page_component_module", [], "any", false, false, false, 8)]);
            // line 9
            echo "        ";
        }
        // line 10
        echo "        ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 10), "uri", [], "any", false, false, false, 10), "attr" => ($context["attr"] ?? null)]);
        echo "
            <table>
                <tbody>
                <tr class=\"invalidate-cache-tr\">
                    <td class=\"invalidate-cache-td-labels invalidate-cache-label\">";
        // line 14
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cache.invalidate.label"), "html", null, true);
        echo ":&nbsp;</td>
                    <td class=\"invalidate-cache-td-fields invalidate-cache-field\">";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "invalidateType", [], "any", false, false, false, 15), 'widget', ["attr" => ["class" => "cache-invalidate-type"]]);
        echo "</td>
                    <td class=\"invalidate-cache-td-labels invalidate-cache-at\">&nbsp;";
        // line 16
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cache.invalidate.at.label"), "html", null, true);
        echo "&nbsp;</td>
                    <td class=\"invalidate-cache-td-fields invalidate-cache-date\">";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "invalidateCacheAt", [], "any", false, false, false, 17), 'widget');
        echo "</td>
                </tr>
                </tbody>
            </table>
            <div class=\"hidden\">
                ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
            </div>
            <div class=\"widget-actions invalidate-cache-buttons\">
                <button type=\"reset\" class=\"btn\">";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cache.invalidate.action.cancel"), "html", null, true);
        echo "</button>
                <button type=\"button\" class=\"btn btn-danger\" id=\"remove_scheduled_cache_invalidation_button\">";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cache.invalidate.action.remove"), "html", null, true);
        echo "</button>
                <button type=\"submit\" class=\"btn btn-success\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.cache.invalidate.action.submit"), "html", null, true);
        echo "</button>
            </div>
        ";
        // line 29
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        echo "
        ";
        // line 30
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroCache/Action/invalidate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  121 => 30,  117 => 29,  112 => 27,  108 => 26,  104 => 25,  98 => 22,  90 => 17,  86 => 16,  82 => 15,  78 => 14,  70 => 10,  67 => 9,  64 => 8,  61 => 7,  59 => 6,  56 => 5,  53 => 4,  50 => 3,  46 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCache/Action/invalidate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CacheBundle/Resources/views/Action/invalidate.html.twig");
    }
}
