<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSync/Include/contentTags.html.twig */
class __TwigTemplate_42fb36a46bd0c3c8f87f241d4a462b17 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 1
    public function macro_syncContentTags($__data__ = null, $__includeCollectionTag__ = false, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "data" => $__data__,
            "includeCollectionTag" => $__includeCollectionTag__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSync/Include/contentTags.html.twig", 2)->unwrap();
            // line 3
            echo "
    <div ";
            // line 4
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orosync/js/app/components/tag-content", "options" => ["tags" => $this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->generate(            // line 7
($context["data"] ?? null), ($context["includeCollectionTag"] ?? null))]]], 4, $context, $this->getSourceContext());
            // line 9
            echo " ></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroSync/Include/contentTags.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 9,  60 => 7,  59 => 4,  56 => 3,  53 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSync/Include/contentTags.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SyncBundle/Resources/views/Include/contentTags.html.twig");
    }
}
