<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChannel/Channel/update.html.twig */
class __TwigTemplate_e6a0f91b5cc4d0be6406d24c9131a71c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtons' => [$this, 'block_navButtons'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["channelMacro"] = $this->macros["channelMacro"] = $this->loadTemplate("@OroChannel/macros.html.twig", "@OroChannel/Channel/update.html.twig", 2)->unwrap();
        // line 4
        $context["entity"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "value", [], "any", false, false, false, 4);
        // line 5
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_create")));
        // line 7
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 7)) {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%channel.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 8
($context["entity"] ?? null), "name", [], "any", false, false, false, 8)]]);
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroChannel/Channel/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 12)) {
            // line 13
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 14
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["entity"] ?? null), "name", [], "any", true, true, false, 17)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 17), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))];
            // line 19
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 21
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_label")]);
            // line 22
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroChannel/Channel/update.html.twig", 22)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 23
            echo "    ";
        }
    }

    // line 26
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/Channel/update.html.twig", 27)->unwrap();
        // line 28
        echo "
    ";
        // line 29
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 29) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)))) {
            // line 30
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_channel", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["entity"] ?? null), "id", [], "any", false, false, false, 31)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-channel", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 35
($context["entity"] ?? null), "id", [], "any", false, false, false, 35), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.channel.entity_label")]], 30, $context, $this->getSourceContext());
            // line 37
            echo "
        ";
            // line 38
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 38, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 40
        echo "
    ";
        // line 41
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_channel_index")], 41, $context, $this->getSourceContext());
        echo "

    ";
        // line 43
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_channel_view", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 43, $context, $this->getSourceContext());
        // line 47
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_channel_create")) {
            // line 48
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_channel_create"]], 48, $context, $this->getSourceContext()));
            // line 51
            echo "    ";
        }
        // line 52
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 52), "value", [], "any", false, false, false, 52), "id", [], "any", false, false, false, 52) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_channel_update"))) {
            // line 53
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_channel_update", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 53, $context, $this->getSourceContext()));
            // line 57
            echo "    ";
        }
        // line 58
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 58, $context, $this->getSourceContext());
        echo "
";
    }

    // line 61
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 62
        echo "    ";
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
";
    }

    // line 65
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "    ";
        $context["id"] = "channel-update";
        // line 67
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 73
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 73), 'row'), 1 =>         // line 74
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channelType", [], "any", false, false, false, 74), 'row'), 2 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 75
($context["form"] ?? null), "dataSource", [], "any", true, true, false, 75)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "dataSource", [], "any", false, false, false, 75), 'row')) : (""))]]]]];
        // line 80
        echo "
    ";
        // line 81
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 81), "value", [], "any", false, false, false, 81), "entities", [], "any", false, false, false, 81)) > 0)) {
            // line 82
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Entities"), "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 86
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entities", [], "any", false, false, false, 86), 'row')]]]]]);
            // line 89
            echo "    ";
        }
        // line 90
        echo "
    ";
        // line 91
        ob_start(function () { return ''; });
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("oro_channel_additional_block", $context)) ? (_twig_default_filter(($context["oro_channel_additional_block"] ?? null), "oro_channel_additional_block")) : ("oro_channel_additional_block")), ["entity" => ($context["entity"] ?? null), "form" => ($context["form"] ?? null)]);
        $context["additional"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 92
        echo "    ";
        if (twig_trim_filter(($context["additional"] ?? null))) {
            // line 93
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional"), "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 97
($context["additional"] ?? null)]]]]]);
            // line 100
            echo "    ";
        }
        // line 101
        echo "
    ";
        // line 102
        $context["data"] = ["formErrors" => ((        // line 103
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 104
($context["dataBlocks"] ?? null)];
        // line 106
        echo "
    ";
        // line 107
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "

    ";
        // line 109
        echo twig_call_macro($macros["channelMacro"], "macro_initializeChannelForm", [($context["form"] ?? null), $this->extensions['Oro\Bundle\ChannelBundle\Twig\ChannelExtension']->getEntitiesMetadata(), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "customerIdentity", [], "any", false, false, false, 109)], 109, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroChannel/Channel/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  219 => 109,  214 => 107,  211 => 106,  209 => 104,  208 => 103,  207 => 102,  204 => 101,  201 => 100,  199 => 97,  197 => 93,  194 => 92,  190 => 91,  187 => 90,  184 => 89,  182 => 86,  180 => 82,  178 => 81,  175 => 80,  173 => 75,  172 => 74,  171 => 73,  169 => 67,  166 => 66,  162 => 65,  155 => 62,  151 => 61,  144 => 58,  141 => 57,  138 => 53,  135 => 52,  132 => 51,  129 => 48,  126 => 47,  124 => 43,  119 => 41,  116 => 40,  111 => 38,  108 => 37,  106 => 35,  105 => 31,  103 => 30,  101 => 29,  98 => 28,  95 => 27,  91 => 26,  86 => 23,  83 => 22,  80 => 21,  74 => 19,  72 => 17,  71 => 14,  69 => 13,  66 => 12,  62 => 11,  57 => 1,  54 => 8,  51 => 7,  49 => 5,  47 => 4,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChannel/Channel/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ChannelBundle/Resources/views/Channel/update.html.twig");
    }
}
