<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroPlatform/Collector/platform.html.twig */
class __TwigTemplate_bc8146f7ca0fed6315a7c8886100fe4b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'toolbar' => [$this, 'block_toolbar'],
            'text' => [$this, 'block_text'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@OroPlatform/Collector/platform.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_toolbar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        ob_start(function () { return ''; });
        // line 5
        echo "        ";
        echo twig_include($this->env, $context, "@OroPlatform/Icon/white_logo.svg");
        echo "
        <style>#oro-platform-icon {top: 8px;}</style>
    ";
        $context["icon"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 8
        echo "    ";
        ob_start(function () { return ''; });
        // line 9
        echo "        ";
        $this->displayBlock('text', $context, $blocks);
        // line 34
        echo "    ";
        $context["text"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 35
        echo "    ";
        $this->loadTemplate("@WebProfiler/Profiler/toolbar_item.html.twig", "@OroPlatform/Collector/platform.html.twig", 35)->display(twig_array_merge($context, ["link" => false, "additional_classes" => "sf-toolbar-block-right"]));
    }

    // line 9
    public function block_text($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "            <div class=\"sf-toolbar-info-group\">
                <div class=\"sf-toolbar-info-piece\">
                    <b>API Sandbox</b>
                    <span>
                    <a href=\"";
        // line 14
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("nelmio_api_doc_index");
        echo "\">API</a>
                </span>
                </div>
            </div>
            <div class=\"sf-toolbar-info-group\">
                <div class=\"sf-toolbar-info-piece\">
                    <b>Resources</b>
                    <span>
                    <a href=\"https://doc.oroinc.com/\" rel=\"help\">OroPlatform Documentation</a>
                </span>
                </div>
                <div class=\"sf-toolbar-info-piece\">
                    <b>Help</b>
                    <span>
                    <a href=\"https://doc.oroinc.com/community/#contact-community\" rel=\"help\">Oro Support Channels</a>
                </span>
                </div>
            </div>
            ";
        // line 32
        echo twig_include($this->env, $context, "@OroPlatform/Collector/collectors_toggle.html.twig");
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "@OroPlatform/Collector/platform.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 32,  85 => 14,  79 => 10,  75 => 9,  70 => 35,  67 => 34,  64 => 9,  61 => 8,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroPlatform/Collector/platform.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/PlatformBundle/Resources/views/Collector/platform.html.twig");
    }
}
