<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/form-section-loading-view.js */
class __TwigTemplate_2a9dea1b0e44f56c29956c0e7c692abf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');
    const LoadingMaskView = require('oroui/js/app/views/loading-mask-view');

    const FormLoadingView = BaseView.extend({
        autoRender: true,

        /**
         * @inheritdoc
         */
        constructor: function FormLoadingView(options) {
            FormLoadingView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this.subview('loadingMaskView', new LoadingMaskView({
                container: this.\$('.section-content')
            }));

            FormLoadingView.__super__.initialize.call(this, options);
        },

        render: function() {
            FormLoadingView.__super__.render.call(this);

            this.\$el.attr({'data-layout': 'separate', 'data-skip-input-widgets': true});

            return this;
        },

        startLoading: function() {
            const loadingMaskView = this.subview('loadingMaskView');

            loadingMaskView.show();
            this.\$el.removeAttr('data-skip-input-widgets');

            return this.initLayout().then(function() {
                loadingMaskView.hide();
            });
        }
    });

    return FormLoadingView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/form-section-loading-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/form-section-loading-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/form-section-loading-view.js");
    }
}
