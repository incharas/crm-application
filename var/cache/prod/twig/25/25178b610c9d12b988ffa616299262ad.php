<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Attachment/Datagrid/Property/attachmentLink.html.twig */
class __TwigTemplate_740d74835120446c7f498d6306dab6eb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["attachment"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "att"], "method", false, false, false, 1);
        // line 2
        $context["additional"] = (($this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getTypeIsImage(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "file", [], "any", false, false, false, 2), "mimeType", [], "any", false, false, false, 2))) ? (["galleryId" => "grid-attachments"]) : ([]));
        // line 3
        echo $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileView($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attachment"] ?? null), "file", [], "any", false, false, false, 3), ($context["additional"] ?? null));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Attachment/Datagrid/Property/attachmentLink.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Attachment/Datagrid/Property/attachmentLink.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Attachment/Datagrid/Property/attachmentLink.html.twig");
    }
}
