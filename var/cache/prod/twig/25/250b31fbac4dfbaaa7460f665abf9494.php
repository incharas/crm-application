<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/variables/button-group.scss */
class __TwigTemplate_0a301d215a8e4ed14d6f57c611044b04 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:map';

\$btn-group-divider-width: 1px;
\$btn-group-divider-color: \$primary-inverse;

\$btn-group-divider-keys: () !default;
\$btn-group-divider-keys: map.merge(
    (
        'light': transparent,
        'lighter': transparent,
        'primary': rgba(\$primary-inverse, .4),
        'info': rgba(\$primary-inverse, .4),
        'success': #a5c387,
        'warning': rgba(\$primary-inverse, .4),
        'danger': rgba(\$primary-inverse, .4),
        'dark': rgba(\$primary-inverse, .4),
        'inverse': rgba(\$primary-inverse, .4)
    ),
    \$btn-group-divider-keys
);

\$btn-group-square-divider-width: \$btn-square-border-width;

\$btn-group-square-divider-keys: () !default;
\$btn-group-square-divider-keys: map.merge(
    (
        'default': \$primary-750,
        'light': transparent,
        'lighter': transparent
    ),
    \$btn-group-square-divider-keys
);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/variables/button-group.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/variables/button-group.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/variables/button-group.scss");
    }
}
