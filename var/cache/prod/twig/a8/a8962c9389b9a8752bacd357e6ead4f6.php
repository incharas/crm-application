<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/activity.html.twig */
class __TwigTemplate_4193e1d949b9b0cc1ce992fdee60c2d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroCalendar/CalendarEvent/activity.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    ";
        // line 4
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["activity-calendar-event-grid", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 5
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "id", [], "any", false, false, false, 6)]], 4, $context, $this->getSourceContext());
        // line 7
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/activity.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 7,  45 => 6,  44 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/activity.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/activity.html.twig");
    }
}
