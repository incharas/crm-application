<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOrganization/Form/fields.html.twig */
class __TwigTemplate_6000812c16c9dbb7888fa7f764397687 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_business_unit_tree_widget' => [$this, 'block_oro_business_unit_tree_widget'],
            'oro_organizations_select_widget' => [$this, 'block_oro_organizations_select_widget'],
            'oro_type_choice_ownership_type_widget' => [$this, 'block_oro_type_choice_ownership_type_widget'],
            'oro_business_unit_tree_select_widget' => [$this, 'block_oro_business_unit_tree_select_widget'],
            'choice_bu_widget_collapsed' => [$this, 'block_choice_bu_widget_collapsed'],
            'choice_bu_widget_options' => [$this, 'block_choice_bu_widget_options'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_business_unit_tree_widget', $context, $blocks);
        // line 17
        echo "
";
        // line 18
        $this->displayBlock('oro_organizations_select_widget', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('oro_type_choice_ownership_type_widget', $context, $blocks);
        // line 40
        echo "
";
        // line 41
        $this->displayBlock('oro_business_unit_tree_select_widget', $context, $blocks);
        // line 54
        echo "
";
        // line 55
        $this->displayBlock('choice_bu_widget_collapsed', $context, $blocks);
        // line 73
        echo "
";
        // line 74
        $this->displayBlock('choice_bu_widget_options', $context, $blocks);
    }

    // line 1
    public function block_oro_business_unit_tree_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        if (($context["expanded"] ?? null)) {
            // line 3
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 3)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 3) . " horizontal")) : ("horizontal"))]);
            // line 4
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 4) . " validate-group")]);
            // line 5
            echo "        <div ";
            $this->displayBlock("widget_container_attributes", $context, $blocks);
            echo ">
            ";
            // line 6
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["id"] => $context["child"]) {
                // line 7
                echo "                <div class=\"oro-clearfix\">
                    ";
                // line 8
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
                echo "
                    ";
                // line 9
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'label', ["raw_label" => true]);
                echo "
                </div>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['id'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 12
            echo "        </div>
    ";
        } else {
            // line 14
            echo "        ";
            $this->displayBlock("choice_widget_collapsed", $context, $blocks);
            echo "
    ";
        }
    }

    // line 18
    public function block_oro_organizations_select_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        ob_start(function () { return ''; });
        // line 20
        echo "        <input type=\"hidden\" name=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organizations", [], "any", false, false, false, 20), "vars", [], "any", false, false, false, 20), "full_name", [], "any", false, false, false, 20), "html", null, true);
        echo "\"
               value=\"";
        // line 21
        echo twig_escape_filter($this->env, json_encode(["organizations" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 21), "selected_organizations", [], "any", false, false, false, 21)]), "html", null, true);
        echo "\">
        ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "businessUnits", [], "any", false, false, false, 22), 'widget');
        echo "
    ";
        $___internal_parse_34_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 19
        echo twig_spaceless($___internal_parse_34_);
    }

    // line 26
    public function block_oro_type_choice_ownership_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "    ";
        ob_start(function () { return ''; });
        // line 28
        echo "        ";
        if ((($context["value"] ?? null) || ($context["disabled"] ?? null))) {
            // line 29
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["choices"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["choice"]) {
                // line 30
                echo "                ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "value", [], "any", false, false, false, 30) == ((array_key_exists("value", $context)) ? (_twig_default_filter(($context["value"] ?? null), "NONE")) : ("NONE")))) {
                    // line 31
                    echo "                    <div class=\"control-label\">";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 31), "html", null, true);
                    echo "</div>
                ";
                }
                // line 33
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['choice'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "            ";
            $this->displayBlock("hidden_widget", $context, $blocks);
            echo "
        ";
        } else {
            // line 36
            echo "            ";
            $this->displayBlock("choice_widget", $context, $blocks);
            echo "
        ";
        }
        // line 38
        echo "    ";
        $___internal_parse_35_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 27
        echo twig_spaceless($___internal_parse_35_);
    }

    // line 41
    public function block_oro_business_unit_tree_select_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        $context["entityId"] = false;
        // line 43
        echo "    ";
        if (("oro_business_unit_form" == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 43), "vars", [], "any", false, false, false, 43), "name", [], "any", false, false, false, 43))) {
            // line 44
            echo "        ";
            $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, false, false, 44), "vars", [], "any", false, false, false, 44), "value", [], "any", false, false, false, 44), "id", [], "any", false, false, false, 44);
            // line 45
            echo "    ";
        }
        // line 46
        echo "    ";
        if (($context["expanded"] ?? null)) {
            // line 47
            echo "        ";
            // line 48
            echo "        ";
            $context["raw_label"] = true;
            // line 49
            echo "        ";
            $this->displayBlock("choice_widget_expanded", $context, $blocks);
            echo "
    ";
        } else {
            // line 51
            echo "        ";
            $this->displayBlock("choice_bu_widget_collapsed", $context, $blocks);
            echo "
    ";
        }
    }

    // line 55
    public function block_choice_bu_widget_collapsed($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "    ";
        ob_start(function () { return ''; });
        // line 57
        echo "        <select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (($context["multiple"] ?? null)) {
            echo " multiple=\"multiple\"";
        }
        echo ">
            ";
        // line 58
        if ( !(null === ($context["empty_value"] ?? null))) {
            // line 59
            echo "                <option value=\"\"";
            if ((($context["required"] ?? null) && twig_test_empty(($context["value"] ?? null)))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["empty_value"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            echo "</option>
            ";
        }
        // line 61
        echo "            ";
        if ((twig_length_filter($this->env, ($context["preferred_choices"] ?? null)) > 0)) {
            // line 62
            echo "                ";
            $context["options"] = ($context["preferred_choices"] ?? null);
            // line 63
            echo "                ";
            $this->displayBlock("choice_widget_options", $context, $blocks);
            echo "
                ";
            // line 64
            if (((twig_length_filter($this->env, ($context["choices"] ?? null)) > 0) &&  !(null === ($context["separator"] ?? null)))) {
                // line 65
                echo "                    <option disabled=\"disabled\">";
                echo twig_escape_filter($this->env, ($context["separator"] ?? null), "html", null, true);
                echo "</option>
                ";
            }
            // line 67
            echo "            ";
        }
        // line 68
        echo "            ";
        $context["options"] = ($context["choices"] ?? null);
        // line 69
        echo "            ";
        $this->displayBlock("choice_bu_widget_options", $context, $blocks);
        echo "
        </select>
    ";
        $___internal_parse_36_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 56
        echo twig_spaceless($___internal_parse_36_);
    }

    // line 74
    public function block_choice_bu_widget_options($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 75
        echo "    ";
        ob_start(function () { return ''; });
        // line 76
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 77
            echo "            ";
            if (twig_test_iterable($context["choice"])) {
                // line 78
                echo "                <optgroup label=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["group_label"], [], ($context["translation_domain"] ?? null)), "html", null, true);
                echo "\">
                    ";
                // line 79
                $context["options"] = $context["choice"];
                // line 80
                echo "                    ";
                $this->displayBlock("choice_widget_options", $context, $blocks);
                echo "
                </optgroup>
            ";
            } else {
                // line 83
                echo "                ";
                $context["disable"] = "";
                // line 84
                echo "                ";
                if ((array_key_exists("forbidden_business_unit_ids", $context) && twig_length_filter($this->env, ($context["forbidden_business_unit_ids"] ?? null)))) {
                    // line 85
                    echo "                    ";
                    if (twig_in_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "value", [], "any", false, false, false, 85), ($context["forbidden_business_unit_ids"] ?? null))) {
                        // line 86
                        echo "                        ";
                        $context["disable"] = "disabled=\"disabled\"";
                        // line 87
                        echo "                    ";
                    }
                    // line 88
                    echo "                ";
                } else {
                    // line 89
                    echo "                    ";
                    if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["business_unit_ids"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "value", [], "any", false, false, false, 89), [], "array", true, true, false, 89) || (($context["entityId"] ?? null) && (($context["entityId"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "value", [], "any", false, false, false, 89))))) {
                        // line 90
                        echo "                        ";
                        $context["disable"] = "disabled=\"disabled\"";
                        // line 91
                        echo "                    ";
                    }
                    // line 92
                    echo "                ";
                }
                // line 93
                echo "                <option ";
                echo twig_escape_filter($this->env, ($context["disable"] ?? null), "html", null, true);
                echo " value=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "value", [], "any", false, false, false, 93), "html", null, true);
                echo "\"
                ";
                // line 94
                if (Symfony\Bridge\Twig\Extension\twig_is_selected_choice($context["choice"], ($context["value"] ?? null))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                // line 95
                ob_start(function () { return ''; });
                // line 96
                $context["label"] = (((array_key_exists("translatable_options", $context) &&  !($context["translatable_options"] ?? null))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 96)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 96), [], ($context["translation_domain"] ?? null))));
                // line 97
                echo "                        ";
                // line 98
                echo "                        ";
                echo ($context["label"] ?? null);
                $___internal_parse_38_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 95
                echo twig_spaceless($___internal_parse_38_);
                // line 100
                echo "</option>
            ";
            }
            // line 102
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 103
        echo "    ";
        $___internal_parse_37_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 75
        echo twig_spaceless($___internal_parse_37_);
    }

    public function getTemplateName()
    {
        return "@OroOrganization/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  414 => 75,  411 => 103,  397 => 102,  393 => 100,  391 => 95,  387 => 98,  385 => 97,  383 => 96,  381 => 95,  376 => 94,  369 => 93,  366 => 92,  363 => 91,  360 => 90,  357 => 89,  354 => 88,  351 => 87,  348 => 86,  345 => 85,  342 => 84,  339 => 83,  332 => 80,  330 => 79,  325 => 78,  322 => 77,  304 => 76,  301 => 75,  297 => 74,  293 => 56,  286 => 69,  283 => 68,  280 => 67,  274 => 65,  272 => 64,  267 => 63,  264 => 62,  261 => 61,  251 => 59,  249 => 58,  241 => 57,  238 => 56,  234 => 55,  226 => 51,  220 => 49,  217 => 48,  215 => 47,  212 => 46,  209 => 45,  206 => 44,  203 => 43,  200 => 42,  196 => 41,  192 => 27,  189 => 38,  183 => 36,  177 => 34,  171 => 33,  165 => 31,  162 => 30,  157 => 29,  154 => 28,  151 => 27,  147 => 26,  143 => 19,  138 => 22,  134 => 21,  129 => 20,  126 => 19,  122 => 18,  114 => 14,  110 => 12,  101 => 9,  97 => 8,  94 => 7,  90 => 6,  85 => 5,  82 => 4,  79 => 3,  76 => 2,  72 => 1,  68 => 74,  65 => 73,  63 => 55,  60 => 54,  58 => 41,  55 => 40,  53 => 26,  50 => 25,  48 => 18,  45 => 17,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOrganization/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/OrganizationBundle/Resources/views/Form/fields.html.twig");
    }
}
