<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/form-description.scss */
class __TwigTemplate_4f7ab9cd6a4017bf1a8b87112c1a6980 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$form-description-offset-bottom: 50px !default;
\$form-description-signin-help-offset-bottom: 30px !default;

\$form-description-logo-offset-bottom: 40px !default;
\$form-description-logo-img-display: block !default;
\$form-description-logo-img-offset: 0 auto !default;
\$form-description-logo-img-max-height: 40px !default;

\$form-description-main-font-size: 22px !default;
\$form-description-main-font-weight: font-weight('light') !default;
\$form-description-main-line-height: 1.2 !default;
\$form-description-main-text-align: center !default;
\$form-description-main-color: \$primary-200 !default;
\$form-description-main-offset-bottom: 16px !default;

\$form-description-text-font-size: 14px !default;
\$form-description-text-align: center !default;
\$form-description-color: \$primary-550 !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/form-description.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/form-description.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/form-description.scss");
    }
}
