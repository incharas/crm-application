<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/WorkflowDefinition/widget/activateForm.html.twig */
class __TwigTemplate_ff21fd1debe226650698c991f355e9d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget-content\"
     data-page-component-module=\"oroworkflow/js/app/components/activate-form-widget-component\"
     data-page-component-options=\"";
        // line 3
        echo twig_escape_filter($this->env, json_encode(["_wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["app"] ?? null), "request", [], "any", false, false, false, 4), "get", [0 => "_wid"], "method", false, false, false, 4), "success" =>         // line 5
array_key_exists("savedId", $context), "deactivated" => ((        // line 6
array_key_exists("deactivated", $context)) ? (twig_join_filter(($context["deactivated"] ?? null), ", ")) : (null)), "selectors" => ["form" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7))], "error" => ((        // line 8
array_key_exists("error", $context)) ? (($context["error"] ?? null)) : (null))]), "html", null, true);
        // line 9
        echo "\">

     ";
        // line 11
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/WorkflowDefinition/widget/activateForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  52 => 11,  48 => 9,  46 => 8,  45 => 7,  44 => 6,  43 => 5,  42 => 4,  41 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/WorkflowDefinition/widget/activateForm.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/WorkflowDefinition/widget/activateForm.html.twig");
    }
}
