<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Notification/button.html.twig */
class __TwigTemplate_1aa3a0d9fe29fa1b5d92002195d0c26c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Notification/button.html.twig", 1)->unwrap();
        // line 2
        if ($this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isFeatureEnabled("email")) {
            // line 3
            echo "<script type=\"text/template\" id=\"email-notification-item-template\">
    <div class=\"info\" data-id=\"<%- id %>\">
        <div class=\"body\">
            <% if (subject) { %>
                <div class=\"title nowrap-ellipsis\"><%- subject %></div>
            <% } else { %>
                <div class=\"empty-subject nowrap-ellipsis\">(";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.recent_emails_widget.no_subject"), "html", null, true);
            echo ")</div>
            <% } %>
            <div class=\"description nowrap-ellipsis\"><%= bodyContent %></div>
        </div>
        <% if (seen) {  %>
        <i class=\"mail-icon\" title=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.datagrid.emails.action.mark_as_unread"), "html", null, true);
            echo "\" data-role=\"toggle-read-status\"></i>
        <% } else { %>
        <i class=\"mail-icon highlight\" title=\"";
            // line 16
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.datagrid.emails.action.mark_as_read"), "html", null, true);
            echo "\" data-role=\"toggle-read-status\"></i>
        <% } %>
    </div>
    <div class=\"footer\">
        <span class=\"from-name\">
            <% if (linkFromName) { %>
                <a href=\"<%- linkFromName %>\" dir=\"ltr\"><%- fromName %></a>
            <% } else { %>
                <%- fromName %>
            <% } %>
        </span>
        <span class=\"forward-action-wrapper\">
            <span class=\"forward-action\">";
            // line 29
            echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrlRaw" => "<%- forwardRoute %>", "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 31)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 31) . " no-hash")) : ("no-hash")), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "widget" => ["type" => "dialog", "multiple" => false, "options" => ["alias" => "reply-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 29, $context, $this->getSourceContext());
            // line 50
            echo "</span>
            <span class=\"reply-action\">";
            // line 52
            echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrlRaw" => "<%- replyRoute %>", "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 54
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 54)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 54) . " no-hash")) : ("no-hash")), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "widget" => ["type" => "dialog", "multiple" => false, "options" => ["alias" => "reply-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply_all"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 52, $context, $this->getSourceContext());
            // line 73
            echo "</span>
            <span class=\"reply-all-action\">";
            // line 75
            echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrlRaw" => "<%- replyAllRoute %>", "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 77
($context["parameters"] ?? null), "aCss", [], "any", true, true, false, 77)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "aCss", [], "any", false, false, false, 77) . " no-hash")) : ("no-hash")), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply_all"), "widget" => ["type" => "dialog", "multiple" => false, "options" => ["alias" => "reply-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 75, $context, $this->getSourceContext());
            // line 96
            echo "</span>
        </span>
    </div>
</script>

";
            // line 101
            if ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_email.show_recent_emails_in_user_bar")) {
                // line 102
                echo "<li class=\"email-notification-menu dropdown\" title=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.notification.menu_item.hint"), "html", null, true);
                echo "\"
    ";
                // line 103
                echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroemail/js/app/components/user-menu-email-notification-component", "options" => ["listSelector" => ".dropdown-menu", "iconSelector" => ".email-notification-icon", "emails" =>                 // line 108
($context["emails"] ?? null), "count" =>                 // line 109
($context["count"] ?? null), "hasMarkAllButton" => true, "wsChannel" => $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailWSChannel()]]], 103, $context, $this->getSourceContext());
                // line 113
                echo ">
    ";
                // line 114
                if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
                    // line 115
                    echo "    ";
                    $context["togglerId"] = uniqid("dropdown-");
                    // line 116
                    echo "    <a href=\"#\" role=\"button\" id=\"";
                    echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                    echo "\" data-toggle=\"dropdown\"
       class=\"dropdown-toggle dropdown-toggle--no-caret email-notification-icon\"
       title=\"";
                    // line 118
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.menu.user_emails"), "html", null, true);
                    echo "\"
       aria-label=\"";
                    // line 119
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.menu.user_emails"), "html", null, true);
                    echo "\" aria-haspopup=\"true\" aria-expanded=\"false\"></a>
    <div class=\"dropdown-menu dropdown-menu-right\" aria-labelledby=\"";
                    // line 120
                    echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                    echo "\" tabindex=\"0\"></div>
    <div class=\"new-email-notification\" ";
                    // line 121
                    echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroemail/js/app/components/new-email-message-component"]], 121, $context, $this->getSourceContext());
                    // line 123
                    echo "> ";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.notification.new_email"), "html", null, true);
                    echo "</div>
    ";
                }
                // line 125
                echo "</li>
";
            } else {
                // line 127
                echo "    <li class=\"hide\" ";
                echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroemail/js/app/components/new-email-flash-message-component"]], 127, $context, $this->getSourceContext());
                // line 129
                echo "></li>
";
            }
            // line 131
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/Notification/button.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  155 => 131,  151 => 129,  148 => 127,  144 => 125,  138 => 123,  136 => 121,  132 => 120,  128 => 119,  124 => 118,  118 => 116,  115 => 115,  113 => 114,  110 => 113,  108 => 109,  107 => 108,  106 => 103,  101 => 102,  99 => 101,  92 => 96,  90 => 77,  89 => 75,  86 => 73,  84 => 54,  83 => 52,  80 => 50,  78 => 31,  77 => 29,  62 => 16,  57 => 14,  49 => 9,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Notification/button.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Notification/button.html.twig");
    }
}
