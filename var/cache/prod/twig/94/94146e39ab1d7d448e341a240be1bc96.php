<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/actions/index.html.twig */
class __TwigTemplate_48ee53a39a80500e9ad3dfb1c9e8b881 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'before_content_addition' => [$this, 'block_before_content_addition'],
            'content' => [$this, 'block_content'],
            'pageActions' => [$this, 'block_pageActions'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_datagrid' => [$this, 'block_content_datagrid'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroUI/actions/index.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/actions/index.html.twig", 2)->unwrap();
        // line 3
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroUI/actions/index.html.twig", 3)->unwrap();
        // line 4
        $context["buttonsPlaceholderData"] = [];
        // line 5
        if (array_key_exists("entity_class", $context)) {
            // line 6
            $context["buttonsPlaceholderData"] = ["entity_class" => ($context["entity_class"] ?? null)];
        } elseif (        // line 7
array_key_exists("entity", $context)) {
            // line 8
            $context["buttonsPlaceholderData"] = ["entity_class" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null))];
        }
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_before_content_addition($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("index_before_content_addition", $context)) ? (_twig_default_filter(($context["index_before_content_addition"] ?? null), "index_before_content_addition")) : ("index_before_content_addition")), array());
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    <div class=\"container-fluid page-title\">
        ";
        // line 17
        ob_start(function () { return ''; });
        // line 18
        $this->displayBlock('pageActions', $context, $blocks);
        $context["pageActionsBlock"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 22
        echo "
        <div class=\"navigation navbar-extra navbar-extra-right\">
            <div class=\"row\">
                <div class=\"pull-left pull-left-extra\">
                    <div class=\"pull-left\">
                        <h1 class=\"oro-subtitle\">";
        // line 27
        ((array_key_exists("pageTitle", $context)) ? (print (twig_escape_filter($this->env, ($context["pageTitle"] ?? null), "html", null, true))) : (print ("")));
        echo "</h1>
                    </div>
                </div>
                <div class=\"pull-right title-buttons-container invisible\"
                         data-page-component-module=\"oroui/js/app/components/view-component\"
                         data-page-component-options=\"";
        // line 32
        echo twig_escape_filter($this->env, json_encode(["view" => "oroui/js/app/views/hidden-initialization-view"]), "html", null, true);
        echo "\"
                         data-layout=\"separate\">
                    ";
        // line 34
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("index_navButtons_before", $context)) ? (_twig_default_filter(($context["index_navButtons_before"] ?? null), "index_navButtons_before")) : ("index_navButtons_before")), ($context["buttonsPlaceholderData"] ?? null));
        // line 35
        echo "                    ";
        $this->displayBlock('navButtons', $context, $blocks);
        // line 36
        echo "                    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("index_navButtons_after", $context)) ? (_twig_default_filter(($context["index_navButtons_after"] ?? null), "index_navButtons_after")) : ("index_navButtons_after")), ($context["buttonsPlaceholderData"] ?? null));
        // line 37
        echo "                </div>
                <div class=\"page-title-center\"
                     data-role=\"filters-state-view-container\"
                     data-page-component-module=\"oroui/js/app/components/view-component\"
                     data-page-component-options=\"";
        // line 41
        echo twig_escape_filter($this->env, json_encode(["view" => "oroui/js/app/views/page-center-title-view"]), "html", null, true);
        echo "\">
                </div>
            </div>
            ";
        // line 44
        if (twig_trim_filter(($context["pageActionsBlock"] ?? null))) {
            // line 45
            echo "            <div class=\"row inline-info\">
                <div class=\"pull-right\">
                    <div class=\"inline-decorate-container\">
                        <ul class=\"inline-decorate\">
                            ";
            // line 49
            echo twig_escape_filter($this->env, ($context["pageActionsBlock"] ?? null), "html", null, true);
            echo "
                        </ul>
                    </div>
                </div>
            </div>
            ";
        }
        // line 55
        echo "        </div>
    </div>
    ";
        // line 57
        $this->displayBlock('content_datagrid', $context, $blocks);
    }

    // line 18
    public function block_pageActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("index_pageActions", $context)) ? (_twig_default_filter(($context["index_pageActions"] ?? null), "index_pageActions")) : ("index_pageActions")), ($context["buttonsPlaceholderData"] ?? null));
    }

    // line 35
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 57
    public function block_content_datagrid($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "        ";
        if (array_key_exists("gridName", $context)) {
            // line 59
            echo "            ";
            if (array_key_exists("gridScope", $context)) {
                // line 60
                echo "                ";
                $context["gridName"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName(($context["gridName"] ?? null), ($context["gridScope"] ?? null));
                // line 61
                echo "            ";
            }
            // line 62
            echo "            ";
            $context["renderParams"] = twig_array_merge(["enableFullScreenLayout" => true, "enableViews" => true, "showViewsInNavbar" => true, "filtersStateElement" => "[data-role=\"filters-state-view-container\"]"], ((            // line 68
array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])));
            // line 69
            echo "            ";
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), ($context["renderParams"] ?? null)], 69, $context, $this->getSourceContext());
            echo "

            ";
            // line 72
            echo "            ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/actions/index.html.twig", 72)->unwrap();
            // line 73
            echo "
            <div ";
            // line 74
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orodatagrid/js/app/components/datagrid-allow-tracking-component", "options" => ["gridName" =>             // line 77
($context["gridName"] ?? null)]]], 74, $context, $this->getSourceContext());
            // line 79
            echo "></div>
        ";
        }
        // line 81
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@OroUI/actions/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 81,  200 => 79,  198 => 77,  197 => 74,  194 => 73,  191 => 72,  185 => 69,  183 => 68,  181 => 62,  178 => 61,  175 => 60,  172 => 59,  169 => 58,  165 => 57,  159 => 35,  155 => 19,  151 => 18,  147 => 57,  143 => 55,  134 => 49,  128 => 45,  126 => 44,  120 => 41,  114 => 37,  111 => 36,  108 => 35,  106 => 34,  101 => 32,  93 => 27,  86 => 22,  83 => 18,  81 => 17,  78 => 16,  74 => 15,  69 => 12,  65 => 11,  61 => 1,  58 => 8,  56 => 7,  54 => 6,  52 => 5,  50 => 4,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/actions/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/actions/index.html.twig");
    }
}
