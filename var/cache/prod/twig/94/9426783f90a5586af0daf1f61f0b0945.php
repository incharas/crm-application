<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/templates/error-template.html */
class __TwigTemplate_a323c90b97f4c9ffdf79623a50ab9ddc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% var showIcon = obj.showIcon !== void 0 ? showIcon : true %>
<span role=\"alert\">
    <%if (showIcon) { %><span class=\"validation-failed__icon\" aria-hidden=\"true\"></span><% } %>
    <span><%= message %></span>
</span>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/templates/error-template.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/templates/error-template.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/templates/error-template.html");
    }
}
