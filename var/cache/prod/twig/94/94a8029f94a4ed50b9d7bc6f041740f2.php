<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Datagrid/Property/attributes.html.twig */
class __TwigTemplate_f33c65056473dcd19c410c8e67a3237c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Datagrid/Property/attributes.html.twig", 1)->unwrap();
        // line 2
        $context["attributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "attributes"], "method", false, false, false, 2);
        // line 3
        echo "
";
        // line 4
        echo twig_call_macro($macros["UI"], "macro_renderList", [($context["attributes"] ?? null)], 4, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Datagrid/Property/attributes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Datagrid/Property/attributes.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Datagrid/Property/attributes.html.twig");
    }
}
