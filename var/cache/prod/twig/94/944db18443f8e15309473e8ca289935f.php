<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/attribute-item.scss */
class __TwigTemplate_6d662abca4f40779d2f010365d11144e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.attribute-item {
    display: \$attribute-item-display;
    margin: \$attribute-item-offset;

    &__term {
        width: \$attribute-item-term-width;
        max-width: \$attribute-item-term-max-width;
        color: \$attribute-item-term-color;
        text-align: \$attribute-item-term-text-align;
        flex-shrink: 0;
    }

    &__description {
        margin-left: \$attribute-item-description-offset-start;
        text-align: \$attribute-item-description-text-align;
        flex-grow: 1;
        width: 100%;

        .list-inline {
            margin-bottom: 0;
        }

        img {
            max-width: 100%;
            height: auto;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/attribute-item.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/attribute-item.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/attribute-item.scss");
    }
}
