<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroIntegration/Integration/forceSyncButton.html.twig */
class __TwigTemplate_c72e4c7c056d7c42c16b5dad80bc3594 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroIntegration/Integration/forceSyncButton.html.twig", 1)->unwrap();
        // line 2
        echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_integration_schedule", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "id", [], "any", false, false, false, 3), "force" => true]), "aCss" => "no-hash schedule-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.integration.button.force_sync"), "dataAttributes" => ["force" => true]]], 2, $context, $this->getSourceContext());
        // line 9
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroIntegration/Integration/forceSyncButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 9,  40 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroIntegration/Integration/forceSyncButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Resources/views/Integration/forceSyncButton.html.twig");
    }
}
