<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/aspect-ratio.scss */
class __TwigTemplate_ddb07ce89b7fc53e0ffb2bc5d8cfd99b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@use 'sass:math';

// Maintain an elements aspect ratio, even as it scales
// \$width and \$height arguments for aspect ratio
// \$content selector for inner content block
// \$position [static|relative|absolute|fixed]
@mixin aspect-ratio(\$width: 1, \$height: 1, \$content: null, \$position: relative) {
    position: \$position;

    &::before {
        content: '';

        display: block;
        width: 100%;
        padding-top: math.div(\$height, \$width) * 100%;
    }

    @if \$content {
        > #{\$content} {
            position: absolute;
            top: 0;
            left: 0;
            right: 0;
            bottom: 0;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/aspect-ratio.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/aspect-ratio.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/aspect-ratio.scss");
    }
}
