<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/show-more/inline-show-more-view.js */
class __TwigTemplate_78b1b525b3f3f1ed4c24354a98a4e63a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import _ from 'underscore';
import AbstractShowMoreView from 'oroui/js/app/views/show-more/abstract-show-more-view';

const InlineShowMoreView = AbstractShowMoreView.extend({
    itemsContainerSelector: '[data-role=\"items-container\"]',

    itemSelector: '> *',

    listen: {
        'layout:reposition mediator': 'render'
    },

    /**
     * @inheritdoc
     */
    constructor: function InlineShowMoreView(options) {
        InlineShowMoreView.__super__.constructor.call(this, options);
    },

    numItemsToHide() {
        if (this.items.length < 2) {
            return 0;
        }

        const threshold = this.items[0].getBoundingClientRect().bottom;
        const firstIndexToHide = _.findIndex(_.rest(this.items),
            item => item.getBoundingClientRect().top > threshold);

        return firstIndexToHide !== -1 ? this.items.length - firstIndexToHide - 1 : 0;
    }
});

export default InlineShowMoreView;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/show-more/inline-show-more-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/show-more/inline-show-more-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/show-more/inline-show-more-view.js");
    }
}
