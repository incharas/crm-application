<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/uniform.scss */
class __TwigTemplate_c6f82552e1683093c7a7c4532240d4b7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

div.uploader,
div.selector {
    position: relative;

    display: inline-block;
    vertical-align: middle;
    padding: 0;
    margin: 0;

    overflow: hidden;
    cursor: default;

    input,
    select {
        position: absolute;
        top: -\$input-border-width;
        right: -\$input-border-width;
        bottom: -\$input-border-width;
        left: -\$input-border-width;

        width: calc(100% + #{\$input-border-width * 2});
        max-width: calc(100% + #{\$input-border-width * 2});
        min-width: calc(100% + #{\$input-border-width * 2});
        height: calc(100% + #{\$input-border-width * 2});

        border: none;
        cursor: default;
        opacity: 0;
    }

    &.uniform-empty-value span {
        color: \$primary-700;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/uniform.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/uniform.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/uniform.scss");
    }
}
