<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/favorites.html.twig */
class __TwigTemplate_6fb9b57ceb40f5afad787e4c592603dd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'list' => [$this, 'block_list'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/favorites.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 13
    public function block_list($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        $context["favorites"] = [];
        // line 15
        echo "    ";
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 15) &&  !(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "depth", [], "any", false, false, false, 15) === 0)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 15))) {
            // line 16
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "children", [], "any", false, false, false, 16));
            foreach ($context['_seq'] as $context["_key"] => $context["favoritesItem"]) {
                // line 17
                echo "            ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["favoritesItem"], "extras", [], "any", false, false, false, 17), "isAllowed", [], "any", false, false, false, 17)) {
                    // line 18
                    echo "                ";
                    $context["favorites"] = twig_array_merge(($context["favorites"] ?? null), [0 => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["favoritesItem"], "extras", [], "any", false, false, false, 18), "id", [], "any", false, false, false, 18), "title" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["favoritesItem"], "label", [], "any", false, false, false, 18), "title_rendered" => $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["favoritesItem"], "label", [], "any", false, false, false, 18)), "url" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["favoritesItem"], "uri", [], "any", false, false, false, 18), "type" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["favoritesItem"], "extras", [], "any", false, false, false, 18), "type", [], "any", false, false, false, 18)]]);
                    // line 19
                    echo "            ";
                }
                // line 20
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['favoritesItem'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "    ";
        }
        // line 22
        echo "    ";
        $macros["navigation"] = $this;
        // line 23
        echo "    ";
        $context["frontendOptions"] = [0 => "el", 1 => "tabTitle", 2 => "tabIcon"];
        // line 24
        echo "    <div data-data=\"";
        echo twig_escape_filter($this->env, json_encode(($context["favorites"] ?? null)), "html", null, true);
        echo "\" data-options=\"";
        echo twig_call_macro($macros["navigation"], "macro_get_options", [($context["frontendOptions"] ?? null), ($context["options"] ?? null)], 24, $context, $this->getSourceContext());
        echo "\"></div>
    <ul class=\"extra-list\"></ul>
    <div class=\"no-data\">";
        // line 26
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.messages.no_items"), "html", null, true);
        echo "</div>
";
    }

    // line 3
    public function macro_get_options($__attributes__ = null, $__data__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attributes" => $__attributes__,
            "data" => $__data__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 4
            $context["options"] = [];
            // line 5
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attributes"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attribute"]) {
                // line 6
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), $context["attribute"], [], "array", true, true, false, 6)) {
                    // line 7
                    $context["options"] = twig_array_merge(($context["options"] ?? null), [$context["attribute"] => (($__internal_compile_0 = ($context["data"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[$context["attribute"]] ?? null) : null)]);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attribute'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/favorites.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  128 => 10,  121 => 7,  119 => 6,  115 => 5,  113 => 4,  99 => 3,  93 => 26,  85 => 24,  82 => 23,  79 => 22,  76 => 21,  70 => 20,  67 => 19,  64 => 18,  61 => 17,  56 => 16,  53 => 15,  50 => 14,  46 => 13,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/favorites.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/favorites.html.twig");
    }
}
