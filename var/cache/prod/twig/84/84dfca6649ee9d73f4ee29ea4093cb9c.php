<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/tab_panel.html.twig */
class __TwigTemplate_923aa848ba224ac470d1a1e8b75e030b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/tab_panel.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["options"] = twig_array_merge(["useDropdown" => true, "verticalTabs" => false, "subtitle" => false, "subTabs" => false], ((        // line 8
array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([])));
        // line 9
        echo "
";
        // line 10
        $context["containerAlias"] = "tab";
        // line 11
        $context["activeTabAlias"] = null;
        // line 12
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 13
            echo "    ";
            $context["containerAlias"] = ((($context["containerAlias"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "alias", [], "any", false, false, false, 13));
            // line 14
            echo "    ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "activeTabAlias", [], "any", true, true, false, 14) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "activeTabAlias", [], "any", false, false, false, 14) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "alias", [], "any", false, false, false, 14)))) {
                // line 15
                echo "        ";
                $context["activeTabAlias"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "activeTabAlias", [], "any", false, false, false, 15);
                // line 16
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 18
        echo "
<div class=\"oro-tabs";
        // line 19
        echo ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "verticalTabs", [], "any", false, false, false, 19)) ? (" oro-tabs__vertical") : (""));
        echo "\"
     data-page-component-responsive-tabs=\"";
        // line 20
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
>
    <div class=\"oro-tabs__head\">
        ";
        // line 23
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "subtitle", [], "any", false, false, false, 23)) {
            // line 24
            echo "            <div class=\"tabs-subtitle\">";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "subtitle", [], "any", false, false, false, 24), "html", null, true);
            echo "</div>
        ";
        }
        // line 26
        echo "
        ";
        // line 27
        $context["extraClasses"] = "";
        // line 28
        echo "
        ";
        // line 29
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "verticalTabs", [], "any", false, false, false, 29)) {
            // line 30
            echo "            ";
            $context["extraClasses"] = (($context["extraClasses"] ?? null) . " flex-column");
            // line 31
            echo "        ";
        }
        // line 32
        echo "
        ";
        // line 33
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "useDropdown", [], "any", false, false, false, 33)) {
            // line 34
            echo "            ";
            $context["extraClasses"] = (($context["extraClasses"] ?? null) . " nav-tabs-dropdown");
            // line 35
            echo "        ";
        }
        // line 36
        echo "
        ";
        // line 37
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "subTabs", [], "any", false, false, false, 37)) {
            // line 38
            echo "            ";
            $context["extraClasses"] = (($context["extraClasses"] ?? null) . " sub-tabs");
            // line 39
            echo "        ";
        }
        // line 40
        echo "
        <ul class=\"nav nav-tabs ";
        // line 41
        echo twig_escape_filter($this->env, ($context["extraClasses"] ?? null), "html", null, true);
        echo "\" role=\"tablist\">
            ";
        // line 42
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 43
            echo "                ";
            $context["isActiveTab"] = ((($context["activeTabAlias"] ?? null)) ? ((($context["activeTabAlias"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "alias", [], "any", false, false, false, 43))) : ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 43) == 1)));
            // line 44
            echo "                <li class=\"nav-item\">
                    ";
            // line 45
            $context["widgetOptions"] = ["type" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 46
$context["tab"], "widgetType", [], "any", true, true, false, 46)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "widgetType", [], "any", false, false, false, 46), "block")) : ("block")), "event" => "shown.bs.tab", "multiple" => false, "options" => ["container" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 50
$context["tab"], "alias", [], "any", false, false, false, 50)), "loadingElement" => ("#" .             // line 51
($context["containerAlias"] ?? null)), "alias" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 52
$context["tab"], "alias", [], "any", false, false, false, 52)]];
            // line 55
            echo "
                    ";
            // line 56
            $context["dataAttributes"] = ["target" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 57
$context["tab"], "alias", [], "any", false, false, false, 57)), "toggle" => "tab", "url" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 59
$context["tab"], "url", [], "any", false, false, false, 59)];
            // line 61
            echo "
                    ";
            // line 62
            if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "content", [], "any", true, true, false, 62)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "content", [], "any", false, false, false, 62), "")) : ("")) || ($context["isActiveTab"] ?? null))) {
                // line 63
                echo "                        ";
                $context["widgetOptions"] = twig_array_merge(($context["widgetOptions"] ?? null), ["initialized" => true]);
                // line 64
                echo "                    ";
            }
            // line 65
            echo "
                    ";
            // line 66
            if (($context["isActiveTab"] ?? null)) {
                // line 67
                echo "                        ";
                $context["className"] = "nav-link active";
                // line 68
                echo "                        ";
                $context["ariaSelectted"] = "true";
                // line 69
                echo "                    ";
            } else {
                // line 70
                echo "                        ";
                $context["className"] = "nav-link";
                // line 71
                echo "                        ";
                $context["ariaSelectted"] = "false";
                // line 72
                echo "                    ";
            }
            // line 73
            echo "
                    ";
            // line 74
            $context["tabOptions"] = twig_array_merge($context["tab"], ["id" => (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 75
$context["tab"], "alias", [], "any", false, false, false, 75) . "-tab"), "widget" =>             // line 76
($context["widgetOptions"] ?? null), "dataAttributes" =>             // line 77
($context["dataAttributes"] ?? null), "label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 78
$context["tab"], "label", [], "any", false, false, false, 78), "class" =>             // line 79
($context["className"] ?? null), "role" => "tab", "ariaSelected" =>             // line 81
($context["ariaSelectted"] ?? null), "ariaControls" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 82
$context["tab"], "alias", [], "any", false, false, false, 82)]);
            // line 84
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_clientLink", [($context["tabOptions"] ?? null)], 84, $context, $this->getSourceContext());
            echo "
                </li>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 87
        echo "        </ul>
    </div>
    <div class=\"oro-tabs__content\">
        <div class=\"tab-content\" id=\"";
        // line 90
        echo twig_escape_filter($this->env, ($context["containerAlias"] ?? null), "html", null, true);
        echo "\">
            ";
        // line 91
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 92
            echo "                ";
            $context["isActiveTab"] = ((($context["activeTabAlias"] ?? null)) ? ((($context["activeTabAlias"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "alias", [], "any", false, false, false, 92))) : ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 92) == 1)));
            // line 93
            echo "                <div class=\"tab-pane";
            if (($context["isActiveTab"] ?? null)) {
                echo " active";
            }
            echo "\" id=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "alias", [], "any", false, false, false, 93), "html", null, true);
            echo "\" role=\"tabpanel\" aria-labelledby=\"";
            echo twig_escape_filter($this->env, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "alias", [], "any", false, false, false, 93) . "-tab"), "html", null, true);
            echo "\">
                    ";
            // line 94
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "content", [], "any", true, true, false, 94)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "content", [], "any", false, false, false, 94), "")) : (""))) {
                // line 95
                echo "                        ";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "content", [], "any", false, false, false, 95);
                echo "
                    ";
            } elseif (            // line 96
($context["isActiveTab"] ?? null)) {
                // line 97
                echo "                        ";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, $context["tab"]);
                echo "
                    ";
            }
            // line 99
            echo "                </div>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 101
        echo "        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/tab_panel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  308 => 101,  293 => 99,  287 => 97,  285 => 96,  280 => 95,  278 => 94,  267 => 93,  264 => 92,  247 => 91,  243 => 90,  238 => 87,  220 => 84,  218 => 82,  217 => 81,  216 => 79,  215 => 78,  214 => 77,  213 => 76,  212 => 75,  211 => 74,  208 => 73,  205 => 72,  202 => 71,  199 => 70,  196 => 69,  193 => 68,  190 => 67,  188 => 66,  185 => 65,  182 => 64,  179 => 63,  177 => 62,  174 => 61,  172 => 59,  171 => 57,  170 => 56,  167 => 55,  165 => 52,  164 => 51,  163 => 50,  162 => 46,  161 => 45,  158 => 44,  155 => 43,  138 => 42,  134 => 41,  131 => 40,  128 => 39,  125 => 38,  123 => 37,  120 => 36,  117 => 35,  114 => 34,  112 => 33,  109 => 32,  106 => 31,  103 => 30,  101 => 29,  98 => 28,  96 => 27,  93 => 26,  87 => 24,  85 => 23,  79 => 20,  75 => 19,  72 => 18,  65 => 16,  62 => 15,  59 => 14,  56 => 13,  52 => 12,  50 => 11,  48 => 10,  45 => 9,  43 => 8,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/tab_panel.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/tab_panel.html.twig");
    }
}
