<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Security/login.html.twig */
class __TwigTemplate_d07c4e1aabf4bbf6fd8522b75cd4b7a4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'bodyClass' => [$this, 'block_bodyClass'],
            'messages' => [$this, 'block_messages'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUser/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUser/layout.html.twig", "@OroUser/Security/login.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        $this->displayParentBlock("head", $context, $blocks);
        echo "
    ";
        // line 5
        echo "    <style type=\"text/css\">
        #login-page-loader {
            position: fixed;
            top: 0;
            left: 0;
            height: 100%;
            width: 100%;
            z-index: 800;
            background: white url('data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMzgiIGhlaWdodD0iMzgiIHZpZXdCb3g9IjAgMCAzOCAzOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KICAgIDxkZWZzPgogICAgICAgIDxsaW5lYXJHcmFkaWVudCB4MT0iOC4wNDIlIiB5MT0iMCUiIHgyPSI2NS42ODIlIiB5Mj0iMjMuODY1JSIgaWQ9ImEiPgogICAgICAgICAgICA8c3RvcCBzdG9wLWNvbG9yPSIjODg4IiBzdG9wLW9wYWNpdHk9IjAiIG9mZnNldD0iMCUiLz4KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzg4OCIgc3RvcC1vcGFjaXR5PSIuNjMxIiBvZmZzZXQ9IjYzLjE0NiUiLz4KICAgICAgICAgICAgPHN0b3Agc3RvcC1jb2xvcj0iIzg4OCIgb2Zmc2V0PSIxMDAlIi8+CiAgICAgICAgPC9saW5lYXJHcmFkaWVudD4KICAgIDwvZGVmcz4KICAgIDxnIGZpbGw9Im5vbmUiIGZpbGwtcnVsZT0iZXZlbm9kZCI+CiAgICAgICAgPGcgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoMSAxKSI+CiAgICAgICAgICAgIDxwYXRoIGQ9Ik0zNiAxOGMwLTkuOTQtOC4wNi0xOC0xOC0xOCIgaWQ9Ik92YWwtMiIgc3Ryb2tlPSJ1cmwoI2EpIiBzdHJva2Utd2lkdGg9IjIiPgogICAgICAgICAgICAgICAgPGFuaW1hdGVUcmFuc2Zvcm0KICAgICAgICAgICAgICAgICAgICBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iCiAgICAgICAgICAgICAgICAgICAgdHlwZT0icm90YXRlIgogICAgICAgICAgICAgICAgICAgIGZyb209IjAgMTggMTgiCiAgICAgICAgICAgICAgICAgICAgdG89IjM2MCAxOCAxOCIKICAgICAgICAgICAgICAgICAgICBkdXI9IjAuOXMiCiAgICAgICAgICAgICAgICAgICAgcmVwZWF0Q291bnQ9ImluZGVmaW5pdGUiIC8+CiAgICAgICAgICAgIDwvcGF0aD4KICAgICAgICAgICAgPGNpcmNsZSBmaWxsPSIjZmZmIiBjeD0iMzYiIGN5PSIxOCIgcj0iMSI+CiAgICAgICAgICAgICAgICA8YW5pbWF0ZVRyYW5zZm9ybQogICAgICAgICAgICAgICAgICAgIGF0dHJpYnV0ZU5hbWU9InRyYW5zZm9ybSIKICAgICAgICAgICAgICAgICAgICB0eXBlPSJyb3RhdGUiCiAgICAgICAgICAgICAgICAgICAgZnJvbT0iMCAxOCAxOCIKICAgICAgICAgICAgICAgICAgICB0bz0iMzYwIDE4IDE4IgogICAgICAgICAgICAgICAgICAgIGR1cj0iMC45cyIKICAgICAgICAgICAgICAgICAgICByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSIgLz4KICAgICAgICAgICAgPC9jaXJjbGU+CiAgICAgICAgPC9nPgogICAgPC9nPgo8L3N2Zz4K') center center no-repeat;
        }
    </style>
";
    }

    // line 17
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "login-page";
    }

    // line 18
    public function block_messages($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 20
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "<div id=\"login-page-loader\"></div>
<div class=\"container\">
    <div class=\"form-wrapper\">
        <div class=\"form-wrapper__inner\">
            ";
        // line 25
        $context["usernameLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Username");
        // line 26
        echo "            ";
        $context["passwordLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Password");
        // line 27
        echo "            ";
        $context["showLabels"] = ((twig_length_filter($this->env, ($context["usernameLabel"] ?? null)) <= 9) && (twig_length_filter($this->env, ($context["passwordLabel"] ?? null)) <= 9));
        // line 28
        echo "            ";
        $context["layoutName"] = ((($context["showLabels"] ?? null)) ? ("form-row-layout") : ("form-column-layout"));
        // line 29
        echo "            <form id=\"login-form\" action=\"";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_security_check");
        echo "\" method=\"post\" class=\"form-signin form-signin--login ";
        echo twig_escape_filter($this->env, ($context["layoutName"] ?? null), "html", null, true);
        echo "\">
                <div class=\"form-description\">
                    ";
        // line 31
        if ($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeLogo()) {
            // line 32
            echo "                        <div class=\"form-description__logo\">
                            <img src=\"";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeLogo()), "html", null, true);
            echo "\"
                                 alt=\"";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.auth.description.logo"), "html", null, true);
            echo "\"
                                 class=\"form-description__logo-img\">
                        </div>
                    ";
        }
        // line 38
        echo "                    <div class=\"form-description__main\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.auth.description.main"), "html", null, true);
        echo "</div>
                    <div class=\"form-description__text\">";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.auth.description.text"), "html", null, true);
        echo "</div>
                </div>
                <div class=\"title-box\">
                    <h2 class=\"title\">";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Login"), "html", null, true);
        echo "</h2>
                </div>
                <fieldset class=\"field-set\">
                    ";
        // line 45
        if (($context["error"] ?? null)) {
            // line 46
            echo "                        <div class=\"alert alert-error\" role=\"alert\">
                            <div>";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["error"] ?? null), "messageKey", [], "any", false, false, false, 47), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["error"] ?? null), "messageData", [], "any", false, false, false, 47), "security"), "html", null, true);
            echo "</div>
                        </div>
                    ";
        }
        // line 50
        echo "                    ";
        echo twig_escape_filter($this->env, ($context["messagesContent"] ?? null), "html", null, true);
        echo "
                    <div class=\"input-field-group\">
                        <div class=\"input-prepend\">
                            <label for=\"prependedInput\" class=\"add-on\">";
        // line 53
        echo twig_escape_filter($this->env, ($context["usernameLabel"] ?? null), "html", null, true);
        echo "</label>
                            <input type=\"text\" id=\"prependedInput\" class=\"input\" autocomplete=\"username\" name=\"_username\" value=\"";
        // line 54
        echo twig_escape_filter($this->env, ($context["last_username"] ?? null), "html", null, true);
        echo "\" required=\"required\" placeholder=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Username or Email"), "html", null, true);
        echo "\" autofocus>
                        </div>
                        <div class=\"input-prepend input-prepend--last\">
                            <label for=\"prependedInput2\" class=\"add-on\">";
        // line 57
        echo twig_escape_filter($this->env, ($context["passwordLabel"] ?? null), "html", null, true);
        echo "</label>
                            <input type=\"password\" id=\"prependedInput2\" class=\"input\" autocomplete=\"off\" name=\"_password\" required=\"required\" placeholder=\"";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Password"), "html", null, true);
        echo "\">
                        </div>
                    </div>
                    <div class=\"oro-remember-me\">
                        <label class=\"checkbox-label\" for=\"remember_me\">
                            <input type=\"checkbox\"
                                    id=\"remember_me\"
                                    name=\"_remember_me\"
                                    value=\"on\">";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Remember me on this computer"), "html", null, true);
        // line 68
        echo "</label>
                    </div>
                    <div class=\"form-signin__footer\">
                        <button type=\"submit\" class=\"btn extra-submit btn-uppercase btn-primary\" id=\"_submit\" name=\"_submit\">";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Log in"), "html", null, true);
        echo "</button>
                        <a href=\"";
        // line 72
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_reset_request");
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Forgot your password?"), "html", null, true);
        echo "</a>
                    </div>
                    <div class=\"form-signin__oauth\">
                        ";
        // line 75
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("oro_user_login_form", $context)) ? (_twig_default_filter(($context["oro_user_login_form"] ?? null), "oro_user_login_form")) : ("oro_user_login_form")), array());
        // line 76
        echo "                    </div>
                </fieldset>
                <input type=\"hidden\" name=\"_target_path\" value=\"\">
                <input type=\"hidden\" name=\"_csrf_token\" value=\"";
        // line 79
        echo twig_escape_filter($this->env, ($context["csrf_token"] ?? null), "html", null, true);
        echo "\">
            </form>
        </div>
        <div class=\"login-copyright\">";
        // line 82
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.copyright", ["{{year}}" => twig_date_format_filter($this->env, "now", "Y")]), "html", null, true);
        echo "</div>
    </div>
    <script>
        document.getElementById('login-form').addEventListener('submit', function (event) {
            var buttons = event.target.getElementsByTagName('button');
            for (var i = 0; i < buttons.length; i++) {
                buttons[i].setAttribute('disabled', 'disabled');
            }
        });
        window.addEventListener('load', function() {
            var loader = document.getElementById('login-page-loader');
            if (loader) {
                loader.parentNode.removeChild(loader);
            }
        });
    </script>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Security/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  225 => 82,  219 => 79,  214 => 76,  212 => 75,  204 => 72,  200 => 71,  195 => 68,  193 => 67,  182 => 58,  178 => 57,  170 => 54,  166 => 53,  159 => 50,  153 => 47,  150 => 46,  148 => 45,  142 => 42,  136 => 39,  131 => 38,  124 => 34,  120 => 33,  117 => 32,  115 => 31,  107 => 29,  104 => 28,  101 => 27,  98 => 26,  96 => 25,  90 => 21,  86 => 20,  80 => 18,  73 => 17,  58 => 5,  53 => 3,  49 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Security/login.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Security/login.html.twig");
    }
}
