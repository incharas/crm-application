<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWindows/js_modules_config.html.twig */
class __TwigTemplate_8bb4f3719bc6a036d474a74093e62cdf extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroWindows/js_modules_config.html.twig", 1)->unwrap();
        // line 2
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["orowindows/js/dialog/state/model" => ["urlRoot" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_windows")]]], 2, $context, $this->getSourceContext());
        // line 6
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroWindows/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWindows/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WindowsBundle/Resources/views/js_modules_config.html.twig");
    }
}
