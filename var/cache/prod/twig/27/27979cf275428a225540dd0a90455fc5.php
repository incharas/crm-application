<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Datagrid/Property/from.html.twig */
class __TwigTemplate_cc6c5be09b3c38fbcb3ed10d82c86430 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/Datagrid/Property/from.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["isNew"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "is_new"], "method", false, false, false, 3);
        // line 4
        $context["fromEmailAddressOwnerClass"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "fromEmailAddressOwnerClass"], "method", false, false, false, 4);
        // line 5
        $context["fromEmailAddressOwnerId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "fromEmailAddressOwnerId"], "method", false, false, false, 5);
        // line 6
        echo "
";
        // line 7
        if (($context["isNew"] ?? null)) {
            echo "<strong>";
        }
        // line 8
        if (($context["fromEmailAddressOwnerId"] ?? null)) {
            // line 9
            echo twig_call_macro($macros["EA"], "macro_renderEmailAddressLink", [($context["value"] ?? null), ($context["fromEmailAddressOwnerClass"] ?? null), ($context["fromEmailAddressOwnerId"] ?? null)], 9, $context, $this->getSourceContext());
        } else {
            // line 11
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
        }
        // line 13
        if (($context["isNew"] ?? null)) {
            echo "</strong>";
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Datagrid/Property/from.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 13,  60 => 11,  57 => 9,  55 => 8,  51 => 7,  48 => 6,  46 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Datagrid/Property/from.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Datagrid/Property/from.html.twig");
    }
}
