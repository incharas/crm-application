<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/menuUpdate/update.html.twig */
class __TwigTemplate_fb340d2a636b29cd4384a893ae61062b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
            'content_column' => [$this, 'block_content_column'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 2)->unwrap();
        // line 3
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig"], true);
        // line 4
        $context["formAction"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 4), "uri", [], "any", false, false, false, 4);
        // line 5
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5);
        // line 6
        ob_start(function () { return ''; });
        // line 7
        echo "<a href=\"#\" onclick=\"window.location.reload(false);return false;\">";
        // line 8
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.reload_link.label"), "html", null, true);
        // line 9
        echo "</a>";
        $context["reloadLink"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 12
        if ( !((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["menuItem"] ?? null), "isRoot", [], "any", true, true, false, 12)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["menuItem"] ?? null), "isRoot", [], "any", false, false, false, 12), false)) : (false))) {
            // line 13
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isDivider", [], "any", false, false, false, 13)) {
                // line 14
                $context["breadcrumbs"] = twig_array_merge(($context["breadcrumbs"] ?? null), ["entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.divider")]);
            } elseif (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["entity"] ?? null), "titles", [], "any", false, false, false, 15))) {
                // line 16
                $context["breadcrumbs"] = twig_array_merge(($context["breadcrumbs"] ?? null), ["entityTitle" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocalizationExtension']->getLocalizedValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "titles", [], "any", false, false, false, 16))]);
            } else {
                // line 18
                $context["breadcrumbs"] = twig_array_merge(($context["breadcrumbs"] ?? null), ["entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.entity_label")])]);
            }
        } else {
            // line 21
            $context["breadcrumbs"] = twig_array_merge(($context["breadcrumbs"] ?? null), ["entityTitle" => ""]);
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 24
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 25)->unwrap();
        // line 26
        echo "
    ";
        // line 27
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isDivider", [], "any", false, false, false, 27) == false)) {
            // line 28
            echo "        ";
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => (            // line 29
($context["routePrefix"] ?? null) . "update"), "params" => ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 30
($context["entity"] ?? null), "menu", [], "any", false, false, false, 30), "key" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 30) != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["menu"] ?? null), "name", [], "any", false, false, false, 30))) ? ("\$key") : ("")), "context" => ($context["context"] ?? null)]]], 28, $context, $this->getSourceContext());
            // line 32
            echo "
        ";
            // line 33
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => (            // line 34
($context["routePrefix"] ?? null) . "create"), "params" => ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 35
($context["entity"] ?? null), "menu", [], "any", false, false, false, 35), "parentKey" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 35) != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["menu"] ?? null), "name", [], "any", false, false, false, 35))) ? ("\$key") : ("")), "context" => ($context["context"] ?? null)]]], 33, $context, $this->getSourceContext()));
            // line 37
            echo "
        ";
            // line 38
            echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 38, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 40
        echo "
    ";
        // line 41
        if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isCustom", [], "any", false, false, false, 41) ||  !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 41)))) {
            // line 42
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 42)) {
                // line 43
                echo "            ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isCustom", [], "any", false, false, false, 43) &&  !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 43)))) {
                    // line 44
                    echo "                ";
                    echo twig_call_macro($macros["UI"], "macro_deleteButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.delete"), "successMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((                    // line 46
array_key_exists("deletedMessage", $context)) ? (_twig_default_filter(($context["deletedMessage"] ?? null), "oro.navigation.menuupdate.deleted_message")) : ("oro.navigation.menuupdate.deleted_message")), ["%reload_link%" => ($context["reloadLink"] ?? null)]), "dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((                    // line 47
($context["routePrefix"] ?? null) . "ajax_delete"), ["context" => ($context["context"] ?? null), "menuName" => ($context["menuName"] ?? null), "key" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 47)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((                    // line 48
($context["routePrefix"] ?? null) . "view"), ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "menu", [], "any", false, false, false, 48), "context" => ($context["context"] ?? null)]), "aCss" => "no-hash remove-button", "id" => "btn-remove-attribute", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.entity_label")]], 44, $context, $this->getSourceContext());
                    // line 52
                    echo "
            ";
                } else {
                    // line 54
                    echo "                ";
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "active", [], "any", false, false, false, 54)) {
                        // line 55
                        echo "                    ";
                        // line 56
                        echo "                    <div class=\"pull-right btn-group icons-holder\">
                        ";
                        // line 57
                        echo twig_call_macro($macros["UI"], "macro_ajaxButton", [["aCss" => "no-hash", "iCss" => "fa-eye-slash", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.hide"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.hide"), "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 62
($context["entity"] ?? null), "id", [], "any", false, false, false, 62), "dataMethod" => "PUT", "dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((                        // line 64
($context["routePrefix"] ?? null) . "ajax_hide"), ["context" => ($context["context"] ?? null), "menuName" => ($context["menuName"] ?? null), "key" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 64)]), "dataRedirect" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 65
($context["app"] ?? null), "request", [], "any", false, false, false, 65), "uri", [], "any", false, false, false, 65), "successMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((                        // line 66
array_key_exists("hiddenMessage", $context)) ? (_twig_default_filter(($context["hiddenMessage"] ?? null), "oro.navigation.menuupdate.hide_success_message")) : ("oro.navigation.menuupdate.hide_success_message")), ["%reload_link%" => ($context["reloadLink"] ?? null)])]], 57, $context, $this->getSourceContext());
                        // line 67
                        echo "
                    </div>
                ";
                    } else {
                        // line 70
                        echo "                    ";
                        // line 71
                        echo "                    <div class=\"pull-right btn-group icons-holder\">
                        ";
                        // line 72
                        echo twig_call_macro($macros["UI"], "macro_ajaxButton", [["aCss" => "no-hash", "iCss" => "fa-eye", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.show"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.show"), "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 77
($context["entity"] ?? null), "id", [], "any", false, false, false, 77), "dataMethod" => "PUT", "dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((                        // line 79
($context["routePrefix"] ?? null) . "ajax_show"), ["context" => ($context["context"] ?? null), "menuName" => ($context["menuName"] ?? null), "key" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 79)]), "dataRedirect" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 80
($context["app"] ?? null), "request", [], "any", false, false, false, 80), "uri", [], "any", false, false, false, 80), "successMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((                        // line 81
array_key_exists("showedMessage", $context)) ? (_twig_default_filter(($context["showedMessage"] ?? null), "oro.navigation.menuupdate.show_success_message")) : ("oro.navigation.menuupdate.show_success_message")), ["%reload_link%" => ($context["reloadLink"] ?? null)])]], 72, $context, $this->getSourceContext());
                        // line 82
                        echo "
                    </div>
                ";
                    }
                    // line 85
                    echo "            ";
                }
                // line 86
                echo "        ";
            }
            // line 87
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isDivider", [], "any", false, false, false, 87) == false)) {
                // line 88
                echo "            ";
                // line 89
                echo "            ";
                $context["html"] = twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((                // line 90
($context["routePrefix"] ?? null) . "create"), ["menuName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "menu", [], "any", false, false, false, 90), "parentKey" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 90), "context" => ($context["context"] ?? null)]), "aCss" => "btn-primary", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.entity_label")]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.entity_label")])]], 89, $context, $this->getSourceContext());
                // line 95
                echo "            ";
                // line 96
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_ajaxButton", [["aCss" => "btn-primary no-hash menu-divider-create-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.divider")]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.divider")]), "dataMethod" => "POST", "dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath((                // line 101
($context["routePrefix"] ?? null) . "ajax_create"), ["context" => ($context["context"] ?? null), "menuName" => ($context["menuName"] ?? null), "parentKey" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 101), "isDivider" => true]), "dataRedirect" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 102
($context["app"] ?? null), "request", [], "any", false, false, false, 102), "uri", [], "any", false, false, false, 102), "successMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((                // line 103
array_key_exists("dividerCreatedMessage", $context)) ? (_twig_default_filter(($context["dividerCreatedMessage"] ?? null), "oro.navigation.menuupdate.divider_created")) : ("oro.navigation.menuupdate.divider_created")), ["%reload_link%" => ($context["reloadLink"] ?? null)])]], 96, $context, $this->getSourceContext()));
                // line 105
                echo "
            ";
                // line 106
                $context["parameters"] = ["html" =>                 // line 107
($context["html"] ?? null), "groupKey" => "createButtons", "options" => ["moreButtonAttrs" => ["class" => "btn-primary"]]];
                // line 115
                echo "
            ";
                // line 116
                echo twig_call_macro($macros["UI"], "macro_pinnedDropdownButton", [($context["parameters"] ?? null)], 116, $context, $this->getSourceContext());
                echo "

            ";
                // line 118
                echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 118, $context, $this->getSourceContext());
                echo "
        ";
            }
            // line 120
            echo "    ";
        }
    }

    // line 123
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 124
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 124)->unwrap();
        // line 125
        echo "
    ";
        // line 126
        $context["id"] = "menu-update-edit";
        // line 127
        echo "
    ";
        // line 128
        ob_start(function () { return ''; });
        // line 129
        echo "        ";
        $this->displayBlock('content_column', $context, $blocks);
        // line 159
        echo "    ";
        $context["content"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 160
        echo "
    ";
        // line 161
        $context["treeOptions"] = ["view" => "oronavigation/js/app/views/tree-manage-view", "data" =>         // line 163
($context["tree"] ?? null), "nodeId" => ((        // line 164
($context["menuItem"] ?? null)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "key", [], "any", false, false, false, 164)) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "parentKey", [], "any", false, false, false, 164))), "menu" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 165
($context["entity"] ?? null), "menu", [], "any", false, false, false, 165), "context" =>         // line 166
($context["context"] ?? null), "updateAllowed" => true, "onRootSelectRoute" => (        // line 168
($context["routePrefix"] ?? null) . "view"), "onSelectRoute" => (        // line 169
($context["routePrefix"] ?? null) . "update"), "onMoveRoute" => (        // line 170
($context["routePrefix"] ?? null) . "ajax_move"), "successMessage" => ((        // line 171
array_key_exists("movedMessage", $context)) ? (_twig_default_filter(($context["movedMessage"] ?? null), "oro.navigation.menuupdate.moved_success_message")) : ("oro.navigation.menuupdate.moved_success_message")), "maxDepth" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 172
($context["menu"] ?? null), "extras", [], "any", false, true, false, 172), "max_nesting_level", [], "any", true, true, false, 172)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["menu"] ?? null), "extras", [], "any", false, true, false, 172), "max_nesting_level", [], "any", false, false, false, 172), 6)) : (6))];
        // line 174
        echo "
    <div class=\"sidebar-container\">
        ";
        // line 176
        $this->loadTemplate("@OroNavigation/menuUpdate/update.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 176, "1218921550")->display(twig_array_merge($context, ["options" => ["fixSidebarHeight" => false]]));
        // line 204
        echo "    </div>
";
    }

    // line 129
    public function block_content_column($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 130
        echo "            ";
        $context["dataBlocks"] = [];
        // line 131
        echo "            ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isDivider", [], "any", false, false, false, 131) == false)) {
            // line 132
            echo "                ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 139
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "titles", [], "any", false, false, false, 139), 'row'), 1 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 140
($context["form"] ?? null), "uri", [], "any", true, true, false, 140)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "uri", [], "any", false, false, false, 140), 'row')) : (null)), 2 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 141
($context["form"] ?? null), "aclResourceId", [], "any", true, true, false, 141)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "aclResourceId", [], "any", false, false, false, 141), 'row')) : (null)), 3 =>             // line 142
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "icon", [], "any", false, false, false, 142), 'row'), 4 =>             // line 143
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "descriptions", [], "any", false, false, false, 143), 'row')]]]]]);
            // line 148
            echo "            ";
        }
        // line 149
        echo "
            ";
        // line 150
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 151
        echo "
            ";
        // line 152
        $context["data"] = ["formErrors" => ((        // line 153
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 154
($context["dataBlocks"] ?? null)];
        // line 156
        echo "
            ";
        // line 157
        $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 157)->displayBlock("content_data", $context);
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/menuUpdate/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  303 => 157,  300 => 156,  298 => 154,  297 => 153,  296 => 152,  293 => 151,  291 => 150,  288 => 149,  285 => 148,  283 => 143,  282 => 142,  281 => 141,  280 => 140,  279 => 139,  277 => 132,  274 => 131,  271 => 130,  267 => 129,  262 => 204,  260 => 176,  256 => 174,  254 => 172,  253 => 171,  252 => 170,  251 => 169,  250 => 168,  249 => 166,  248 => 165,  247 => 164,  246 => 163,  245 => 161,  242 => 160,  239 => 159,  236 => 129,  234 => 128,  231 => 127,  229 => 126,  226 => 125,  223 => 124,  219 => 123,  214 => 120,  209 => 118,  204 => 116,  201 => 115,  199 => 107,  198 => 106,  195 => 105,  193 => 103,  192 => 102,  191 => 101,  189 => 96,  187 => 95,  185 => 90,  183 => 89,  181 => 88,  178 => 87,  175 => 86,  172 => 85,  167 => 82,  165 => 81,  164 => 80,  163 => 79,  162 => 77,  161 => 72,  158 => 71,  156 => 70,  151 => 67,  149 => 66,  148 => 65,  147 => 64,  146 => 62,  145 => 57,  142 => 56,  140 => 55,  137 => 54,  133 => 52,  131 => 48,  130 => 47,  129 => 46,  127 => 44,  124 => 43,  121 => 42,  119 => 41,  116 => 40,  111 => 38,  108 => 37,  106 => 35,  105 => 34,  104 => 33,  101 => 32,  99 => 30,  98 => 29,  96 => 28,  94 => 27,  91 => 26,  88 => 25,  84 => 24,  79 => 1,  76 => 21,  72 => 18,  69 => 16,  67 => 15,  65 => 14,  63 => 13,  61 => 12,  58 => 9,  56 => 8,  54 => 7,  52 => 6,  50 => 5,  48 => 4,  46 => 3,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/menuUpdate/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/menuUpdate/update.html.twig");
    }
}


/* @OroNavigation/menuUpdate/update.html.twig */
class __TwigTemplate_fb340d2a636b29cd4384a893ae61062b___1218921550 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'sidebar' => [$this, 'block_sidebar'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 176
        return "@OroUI/content_sidebar.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUI/content_sidebar.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 176);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 179
    public function block_sidebar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 180
        echo "                ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/update.html.twig", 180)->unwrap();
        // line 181
        echo "                ";
        echo twig_call_macro($macros["UI"], "macro_renderJsTree", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menu.menu_list_default.label"), "treeOptions" =>         // line 184
($context["treeOptions"] ?? null)], ["move" => ["view" => "oroui/js/app/views/jstree/move-action-view", "routeName" => (        // line 189
($context["routePrefix"] ?? null) . "move"), "routeParams" => ["menuName" =>         // line 190
($context["menuName"] ?? null), "context" => ($context["context"] ?? null)]]]], 181, $context, $this->getSourceContext());
        // line 193
        echo "
            ";
    }

    // line 196
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 197
        echo "                ";
        // line 198
        echo "                    <div class=\"category-data\">
                        ";
        // line 199
        echo ($context["content"] ?? null);
        echo "
                    </div>
                ";
        // line 202
        echo "            ";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/menuUpdate/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  394 => 202,  389 => 199,  386 => 198,  384 => 197,  380 => 196,  375 => 193,  373 => 190,  372 => 189,  371 => 184,  369 => 181,  366 => 180,  362 => 179,  351 => 176,  303 => 157,  300 => 156,  298 => 154,  297 => 153,  296 => 152,  293 => 151,  291 => 150,  288 => 149,  285 => 148,  283 => 143,  282 => 142,  281 => 141,  280 => 140,  279 => 139,  277 => 132,  274 => 131,  271 => 130,  267 => 129,  262 => 204,  260 => 176,  256 => 174,  254 => 172,  253 => 171,  252 => 170,  251 => 169,  250 => 168,  249 => 166,  248 => 165,  247 => 164,  246 => 163,  245 => 161,  242 => 160,  239 => 159,  236 => 129,  234 => 128,  231 => 127,  229 => 126,  226 => 125,  223 => 124,  219 => 123,  214 => 120,  209 => 118,  204 => 116,  201 => 115,  199 => 107,  198 => 106,  195 => 105,  193 => 103,  192 => 102,  191 => 101,  189 => 96,  187 => 95,  185 => 90,  183 => 89,  181 => 88,  178 => 87,  175 => 86,  172 => 85,  167 => 82,  165 => 81,  164 => 80,  163 => 79,  162 => 77,  161 => 72,  158 => 71,  156 => 70,  151 => 67,  149 => 66,  148 => 65,  147 => 64,  146 => 62,  145 => 57,  142 => 56,  140 => 55,  137 => 54,  133 => 52,  131 => 48,  130 => 47,  129 => 46,  127 => 44,  124 => 43,  121 => 42,  119 => 41,  116 => 40,  111 => 38,  108 => 37,  106 => 35,  105 => 34,  104 => 33,  101 => 32,  99 => 30,  98 => 29,  96 => 28,  94 => 27,  91 => 26,  88 => 25,  84 => 24,  79 => 1,  76 => 21,  72 => 18,  69 => 16,  67 => 15,  65 => 14,  63 => 13,  61 => 12,  58 => 9,  56 => 8,  54 => 7,  52 => 6,  50 => 5,  48 => 4,  46 => 3,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/menuUpdate/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/menuUpdate/update.html.twig");
    }
}
