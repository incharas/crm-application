<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTask/TaskCrud/dialog/update.html.twig */
class __TwigTemplate_034ba3b2c3366a3c874227ef99f8d221 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content_data' => [$this, 'block_content_data'],
            'widget_context' => [$this, 'block_widget_context'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroTask/TaskCrud/dialog/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "valid", [], "any", false, false, false, 4) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "errors", [], "any", false, false, false, 4)))) {
            // line 5
            echo "        <div class=\"alert alert-error\" role=\"alert\">
            <div class=\"message\">
                ";
            // line 7
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
            </div>
        </div>
    ";
        }
        // line 11
        echo "    <fieldset class=\"form form-horizontal\">
        <div class=\"span6\">
            ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "subject", [], "any", false, false, false, 13), 'row');
        echo "
            ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 14), 'row', ["attr" => ["class" => "narrow-text-field"]]);
        echo "
        </div>
        <div class=\"span6\">
            ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "dueDate", [], "any", false, false, false, 17), 'row');
        echo "
            ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "taskPriority", [], "any", false, false, false, 18), 'row');
        echo "
            ";
        // line 19
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", true, true, false, 19)) {
            // line 20
            echo "                ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 20), 'row');
            echo "
            ";
        }
        // line 22
        echo "        </div>
        <div class=\"span6\">
            ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
        </div>
        <div class=\"widget-actions form-actions\">
            <button class=\"btn\" type=\"reset\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
            ";
        // line 28
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 28), "value", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28)) {
            // line 29
            echo "            <button class=\"btn btn-success\" type=\"submit\">
                ";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.update_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.entity_label")]), "html", null, true);
            echo "
            </button>
            ";
        } else {
            // line 33
            echo "            <button class=\"btn btn-success\" type=\"submit\">
                ";
            // line 34
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.entity_label")]), "html", null, true);
            echo "
            </button>
            ";
        }
        // line 37
        echo "        </div>
    </fieldset>
";
    }

    // line 42
    public function block_widget_context($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/TaskCrud/dialog/update.html.twig", 43)->unwrap();
        // line 44
        echo "
    <div ";
        // line 45
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/widget-form-component", "options" => ["_wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 48
($context["app"] ?? null), "request", [], "any", false, false, false, 48), "get", [0 => "_wid"], "method", false, false, false, 48), "data" => ((        // line 49
array_key_exists("savedId", $context)) ? (_twig_default_filter(($context["savedId"] ?? null), null)) : (null)), "message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.controller.task.saved.message")]]], 45, $context, $this->getSourceContext());
        // line 52
        echo "></div>
";
    }

    public function getTemplateName()
    {
        return "@OroTask/TaskCrud/dialog/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  149 => 52,  147 => 49,  146 => 48,  145 => 45,  142 => 44,  139 => 43,  135 => 42,  129 => 37,  123 => 34,  120 => 33,  114 => 30,  111 => 29,  109 => 28,  105 => 27,  99 => 24,  95 => 22,  89 => 20,  87 => 19,  83 => 18,  79 => 17,  73 => 14,  69 => 13,  65 => 11,  58 => 7,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTask/TaskCrud/dialog/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-task-bundle/Resources/views/TaskCrud/dialog/update.html.twig");
    }
}
