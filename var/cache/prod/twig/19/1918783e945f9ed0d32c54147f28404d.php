<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/font-awesome/_icons-rtl.scss */
class __TwigTemplate_0a20cf1802be2632677dfdd1cecc8b32 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* rtl:raw:
    .fa-angle-left:before {
        content: var(--fa-var-angle-left);
    }

    .fa-angle-right:before {
        content: var(--fa-var-angle-right);
    }

    .fa-angle-double-left:before {
        content: var(--fa-var-angle-double-left);
    }

    .fa-angle-double-right:before {
        content: var(--fa-var-angle-double-right);
    }

    .fa-arrow-circle-left:before {
        content: var(--fa-var-arrow-circle-left);
    }

    .fa-arrow-circle-right:before {
        content: var(--fa-var-arrow-circle-right);
    }

    .fa-arrow-circle-o-left:before {
        content: var(--fa-var-arrow-circle-o-left);
    }

    .fa-arrow-circle-o-right:before {
        content: var(--fa-var-arrow-circle-o-right);
    }

    .fa-arrow-left:before {
        content: var(--fa-var-arrow-left);
    }

    .fa-arrow-right:before {
        content: var(--fa-var-arrow-right);
    }

    .fa-caret-left:before {
        content: var(--fa-var-caret-left);
    }

    .fa-caret-right:before {
        content: var(--fa-var-caret-right);
    }

    .fa-toggle-left:before,
    .fa-caret-square-o-left:before {
        content: var(--fa-var-toggle-left);
    }

    .fa-toggle-right:before,
    .fa-caret-square-o-right:before {
        content: var(--fa-var-toggle-right);
    }

    .fa-chevron-circle-left:before {
        content: var(--fa-var-chevron-circle-left);
    }

    .fa-chevron-circle-right:before {
        content: var(--fa-var-chevron-circle-right);
    }

    .fa-chevron-left:before {
        content: var(--fa-var-chevron-left);
    }

    .fa-chevron-right:before {
        content: var(--fa-var-chevron-right);
    }

    .fa-chevron-down:before {
        content: var(--fa-var-chevron-down);
    }

    .fa-hand-o-left:before {
        content: var(--fa-var-hand-o-left);
    }

    .fa-hand-o-right:before {
        content: var(--fa-var-hand-o-right);
    }

    .fa-long-arrow-left:before {
        content: var(--fa-var-long-arrow-left);
    }

    .fa-long-arrow-right:before {
        content: var(--fa-var-long-arrow-right);
    }
*/
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/font-awesome/_icons-rtl.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/font-awesome/_icons-rtl.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/font-awesome/_icons-rtl.scss");
    }
}
