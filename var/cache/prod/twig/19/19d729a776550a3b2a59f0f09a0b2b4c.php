<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/settings/mixins/absolute-line-height.scss */
class __TwigTemplate_6d21c3f698d12c4915c0166a01d3a5e5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:math';

// Remove the unit of a length
// @return number
// Use: \$value: strip-units(10px); -> 10
@function strip-units(\$value) {
    @return math.div(\$value, \$value * 0 + 1);
}

// Returns absolute value of line-height in font-size units, if it's given in relative units
// @return number
// Use: \$value: absolute-line-height(10px, 1.5); -> 15px
//      \$value: absolute-line-height(10px, 1.5em); -> 15px
//      \$value: absolute-line-height(10px, 150%); -> 15px
//      \$value: absolute-line-height(10px, 15px); -> 15px
@function absolute-line-height(\$font-size, \$line-height: 1.2) {
    \$line-unit: math.unit(\$line-height);
    \$line-value: strip-units(\$line-height);

    @if \$line-unit == '' or \$line-unit == 'em' or \$line-unit == 'rem' {
        @return \$line-value * \$font-size;
    } @else if \$line-unit == '%' {
        @return (\$line-value * .01) * \$font-size;
    } @else {
        @return \$line-height;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/settings/mixins/absolute-line-height.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/settings/mixins/absolute-line-height.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/settings/mixins/absolute-line-height.scss");
    }
}
