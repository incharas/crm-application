<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/layouts/default/page/layout.html.twig */
class __TwigTemplate_8023c941174db801483a9833655c1aa9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_layout_js_modules_config_widget' => [$this, 'block__layout_js_modules_config_widget'],
            '_layout_js_build_scripts_widget' => [$this, 'block__layout_js_build_scripts_widget'],
            'body_widget' => [$this, 'block_body_widget'],
            '_wrapper_widget' => [$this, 'block__wrapper_widget'],
            '_page_container_widget' => [$this, 'block__page_container_widget'],
            '_page_main_widget' => [$this, 'block__page_main_widget'],
            '_page_main_content_widget' => [$this, 'block__page_main_content_widget'],
            '_page_main_header_widget' => [$this, 'block__page_main_header_widget'],
            '_page_content_widget' => [$this, 'block__page_content_widget'],
            '_page_sidebar_widget' => [$this, 'block__page_sidebar_widget'],
            '_skip_to_content_widget' => [$this, 'block__skip_to_content_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_layout_js_modules_config_widget', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('_layout_js_build_scripts_widget', $context, $blocks);
        // line 17
        echo "
";
        // line 18
        $this->displayBlock('body_widget', $context, $blocks);
        // line 37
        echo "
";
        // line 38
        $this->displayBlock('_wrapper_widget', $context, $blocks);
        // line 47
        echo "
";
        // line 48
        $this->displayBlock('_page_container_widget', $context, $blocks);
        // line 57
        echo "
";
        // line 58
        $this->displayBlock('_page_main_widget', $context, $blocks);
        // line 69
        echo "
";
        // line 70
        $this->displayBlock('_page_main_content_widget', $context, $blocks);
        // line 79
        echo "
";
        // line 80
        $this->displayBlock('_page_main_header_widget', $context, $blocks);
        // line 90
        echo "
";
        // line 91
        $this->displayBlock('_page_content_widget', $context, $blocks);
        // line 121
        echo "
";
        // line 122
        $this->displayBlock('_page_sidebar_widget', $context, $blocks);
        // line 140
        echo "
";
        // line 141
        $this->displayBlock('_skip_to_content_widget', $context, $blocks);
    }

    // line 1
    public function block__layout_js_modules_config_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    ";
        // line 3
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("js_modules_config", $context)) ? (_twig_default_filter(($context["js_modules_config"] ?? null), "js_modules_config")) : ("js_modules_config")), array());
        // line 4
        echo "    ";
        $macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroUI/layouts/default/page/layout.html.twig", 4)->unwrap();
        // line 5
        echo "    ";
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["oroui/js/app" => ["publicPath" => (($__internal_compile_0 = twig_split_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(        // line 7
($context["publicPath"] ?? null)), "?", 2)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null)]]], 5, $context, $this->getSourceContext());
        // line 9
        echo "
";
    }

    // line 12
    public function block__layout_js_build_scripts_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroUI/layouts/default/page/layout.html.twig", 13)->unwrap();
        // line 14
        echo "    ";
        echo twig_call_macro($macros["Asset"], "macro_js", [($context["src"] ?? null)], 14, $context, $this->getSourceContext());
        echo "
    ";
        // line 15
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
";
    }

    // line 18
    public function block_body_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/layouts/default/page/layout.html.twig", 19)->unwrap();
        // line 20
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => ((" body " . (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) ? ("mobile") : ("desktop"))) . "-version")]);
        // line 23
        echo "
    <body ";
        // line 24
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        <div ";
        // line 25
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/app-loading-mask-component"]], 25, $context, $this->getSourceContext());
        // line 27
        echo "></div>
        <div ";
        // line 28
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/app-loading-bar-component"]], 28, $context, $this->getSourceContext());
        // line 30
        echo "></div>
        <div id=\"container\" data-layout=\"separate\"
             ";
        // line 32
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/content-view"]], 32, $context, $this->getSourceContext());
        echo ">
            ";
        // line 33
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
        </div>
    </body>
";
    }

    // line 38
    public function block__wrapper_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " wrapper"]);
        // line 42
        echo "
    <div ";
        // line 43
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 44
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 48
    public function block__page_container_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-container"]);
        // line 52
        echo "
    <div ";
        // line 53
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 54
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 58
    public function block__page_main_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-main", "data-role" => "page-main-container", "data-skip-focus-decoration" => ""]);
        // line 64
        echo "
    <main";
        // line 65
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 66
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </main>
";
    }

    // line 70
    public function block__page_main_content_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 71
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-main__content"]);
        // line 74
        echo "
    <div";
        // line 75
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 76
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 80
    public function block__page_main_header_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 81
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "count", [], "any", false, false, false, 81) > 0)) {
            // line 82
            echo "        ";
            $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-main__header"]);
            // line 85
            echo "        <div";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo ">
            ";
            // line 86
            echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
            echo "
        </div>
    ";
        }
    }

    // line 91
    public function block__page_content_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 92
        echo "    ";
        $context["visibleSidebarChildren"] = [];
        // line 93
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["blocks"] ?? null), "page_sidebar", [], "any", true, true, false, 93)) {
            // line 94
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["blocks"] ?? null), "page_sidebar", [], "any", false, false, false, 94), "children", [], "any", false, false, false, 94));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 95
                echo "            ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 95), "visible", [], "any", false, false, false, 95)) {
                    // line 96
                    echo "                ";
                    $context["visibleSidebarChildren"] = twig_array_merge(($context["visibleSidebarChildren"] ?? null), [0 => $context["child"]]);
                    // line 97
                    echo "            ";
                }
                // line 98
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 99
            echo "    ";
        }
        // line 100
        echo "
    ";
        // line 101
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-content", "data-role" => "page-content"]);
        // line 105
        echo "
    ";
        // line 106
        $context["sidebarIsVisible"] = (twig_length_filter($this->env, ($context["visibleSidebarChildren"] ?? null)) > 0);
        // line 107
        echo "    ";
        if ((array_key_exists("sidebarExpanded", $context) && (($context["sidebarExpanded"] ?? null) == false))) {
            // line 108
            echo "        ";
            $context["sidebarIsVisible"] = false;
            // line 109
            echo "    ";
        }
        // line 110
        echo "
    ";
        // line 111
        if (($context["sidebarIsVisible"] ?? null)) {
            // line 112
            echo "        ";
            $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-content--has-sidebar"]);
            // line 115
            echo "    ";
        }
        // line 116
        echo "
    <section ";
        // line 117
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 118
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </section>
";
    }

    // line 122
    public function block__page_sidebar_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 123
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["block"] ?? null), "count", [], "any", false, false, false, 123) > 0)) {
            // line 124
            echo "        ";
            $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " page-sidebar", "data-role" => "page-sidebar"]);
            // line 128
            echo "
        ";
            // line 129
            if ((array_key_exists("sidebarExpanded", $context) && (($context["sidebarExpanded"] ?? null) == false))) {
                // line 130
                echo "            ";
                $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " hidden"]);
                // line 133
                echo "        ";
            }
            // line 134
            echo "
        <aside ";
            // line 135
            $this->displayBlock("block_attributes", $context, $blocks);
            echo ">
            ";
            // line 136
            echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
            echo "
        </aside>
    ";
        }
    }

    // line 141
    public function block__skip_to_content_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 142
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => "btn btn--info skip-to-content__btn", "data-page-component-proxy-focus" => "[data-role=\"page-main-container\"]", "type" => "button"]);
        // line 147
        echo "
    <div class=\"skip-to-content\">
        <button ";
        // line 149
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.button.skip_to_content"), "html", null, true);
        echo "</button>
        <div class=\"skip-to-content__decorator\" aria-hidden=\"true\"></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/layouts/default/page/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  414 => 149,  410 => 147,  407 => 142,  403 => 141,  395 => 136,  391 => 135,  388 => 134,  385 => 133,  382 => 130,  380 => 129,  377 => 128,  374 => 124,  371 => 123,  367 => 122,  360 => 118,  356 => 117,  353 => 116,  350 => 115,  347 => 112,  345 => 111,  342 => 110,  339 => 109,  336 => 108,  333 => 107,  331 => 106,  328 => 105,  326 => 101,  323 => 100,  320 => 99,  314 => 98,  311 => 97,  308 => 96,  305 => 95,  300 => 94,  297 => 93,  294 => 92,  290 => 91,  282 => 86,  277 => 85,  274 => 82,  271 => 81,  267 => 80,  260 => 76,  256 => 75,  253 => 74,  250 => 71,  246 => 70,  239 => 66,  235 => 65,  232 => 64,  229 => 59,  225 => 58,  218 => 54,  214 => 53,  211 => 52,  208 => 49,  204 => 48,  197 => 44,  193 => 43,  190 => 42,  187 => 39,  183 => 38,  175 => 33,  171 => 32,  167 => 30,  165 => 28,  162 => 27,  160 => 25,  156 => 24,  153 => 23,  150 => 20,  147 => 19,  143 => 18,  137 => 15,  132 => 14,  129 => 13,  125 => 12,  120 => 9,  118 => 7,  116 => 5,  113 => 4,  111 => 3,  106 => 2,  102 => 1,  98 => 141,  95 => 140,  93 => 122,  90 => 121,  88 => 91,  85 => 90,  83 => 80,  80 => 79,  78 => 70,  75 => 69,  73 => 58,  70 => 57,  68 => 48,  65 => 47,  63 => 38,  60 => 37,  58 => 18,  55 => 17,  53 => 12,  50 => 11,  48 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/layouts/default/page/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/layout.html.twig");
    }
}
