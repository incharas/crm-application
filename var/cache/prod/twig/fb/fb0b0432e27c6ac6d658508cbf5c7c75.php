<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Mail/invite.html.twig */
class __TwigTemplate_63d28345d220e880030e5388e9a1cb4d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUser/Mail/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUser/Mail/layout.html.twig", "@OroUser/Mail/invite.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <style>
        @media (max-width: 480pt) {
            .wrapper{
                width: 100% !important;
            }
        }
        a:hover {
            text-decoration: underline;
        }
    </style>

    <table class=\"wrapper\" style=\"border-collapse: collapse; width: 640px; width: 480pt; max-width: 100%; box-sizing: border-box\">
        <tr>
            <td style=\"background-color: #ffffff; padding: 4pt\">
                <span style=\"font-family: Arial, Helvetica, sans-serif; font-size: 14pt; line-height: 14pt; color: #444444\">Hello, ";
        // line 18
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["user"] ?? null)), "html", null, true);
        echo "!</span>
                <div style=\"height: 10pt; line-height: 10pt\">&nbsp;</div>
                <p style=\"color: #444444; font-size: 10pt; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 5pt 0 5pt 0;\">
                    A new user has been created for you at <a href=\"";
        // line 21
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getAbsoluteUrl("oro_user_security_login"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_ui.application_url"), "html", null, true);
        echo "</a>
                </p>
                <p style=\"color: #444444; font-size: 10pt; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 5pt 0 10pt 0;\">
                    Please use the following credentials to log in:
                </p>
                <p style=\"color: #444444; font-size: 10pt; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 5pt 0 10pt 0;\">
                    <strong>Login:</strong> ";
        // line 27
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "username", [], "any", false, false, false, 27), "html", null, true);
        echo "
                </p>

                ";
        // line 30
        if ( !twig_test_empty(($context["password"] ?? null))) {
            // line 31
            echo "                    <p style=\"color: #444444; font-size: 10pt; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 5pt 0 10pt 0;\">
                        <strong>Password:</strong> ";
            // line 32
            echo twig_escape_filter($this->env, ($context["password"] ?? null), "html", null, true);
            echo "
                    </p>
                    <p></p>
                    <p style=\"color: #444444; font-size: 10pt; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 5pt 0 10pt 0;\">
                        <strong>We strongly recommend that you change your password after logging in.</strong>
                    </p>
                ";
        } else {
            // line 39
            echo "                    <p style=\"color: #444444; font-size: 10pt; font-family: Arial, Helvetica, sans-serif; font-weight: normal; margin: 5pt 0 10pt 0;\">
                        <strong>Proceed to creating your own password by clicking the Reset Password button.</strong>
                    </p>
                    <table style=\"width: 100%;\">
                        <tbody>
                        <tr>
                            <td>&nbsp;</td>
                            <td style=\"padding: 5pt 0; background: #2ea63a; width: 130pt; text-align: center\">
                                <a style=\"color: #ffffff; font-size: 11pt; text-transform: uppercase; text-decoration: none; line-height: 1em; font-family: Arial, Helvetica, sans-serif;\" href=\"";
            // line 47
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getAbsoluteUrl("oro_user_reset_reset", ["token" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["user"] ?? null), "confirmationToken", [], "any", false, false, false, 47)]), "html", null, true);
            echo "\" target=\"_blank\">
                                    RESET&nbsp;PASSWORD
                                </a>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        </tbody>
                    </table>
                ";
        }
        // line 56
        echo "            </td>
        </tr>
    </table>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Mail/invite.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 56,  114 => 47,  104 => 39,  94 => 32,  91 => 31,  89 => 30,  83 => 27,  72 => 21,  66 => 18,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Mail/invite.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Mail/invite.html.twig");
    }
}
