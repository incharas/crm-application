<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/select2/default-template.html */
class __TwigTemplate_a7da766437eb78de7a0da0d9d1600fe7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<%
    var data = \$(element).data(),
        background;
    if (data.icon) {
        background = 'style=\"background: url(' + data.icon + ') no-repeat;\"';
    } else {
        background = 'style=\"display: none;\"';
    }
%>
<span class=\"aware-icon-block aware-icon-block-text\" <%= background %> ></span>
<span class=\"aware-icon-block-text\"><%= highlight(_.escape(text)) %></span>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/select2/default-template.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/select2/default-template.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/select2/default-template.html");
    }
}
