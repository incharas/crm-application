<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/bootstrap/bootstrap-modal.js */
class __TwigTemplate_186a4eb03044e9d6e71b1595004cd3d7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    require('bootstrap-modal');

    const NAME = 'modal';
    const EVENT_KEY = '.bs.modal';
    const Event = {
        FOCUSIN: 'focusin' + EVENT_KEY
    };

    const Modal = \$.fn[NAME].Constructor;
    const original = _.pick(Modal.prototype, 'dispose');

    Modal.prototype.dispose = function() {
        this._removeBackdrop();
        original.dispose.call(this);
    };

    /**
     * fix endless loop
     * Based on https://github.com/Khan/bootstrap/commit/378ab557e24b861579d2ec4ce6f04b9ea995ab74
     * Updated to support two modals on page
     */
    Modal.prototype._enforceFocus = function() {
        const that = this;

        \$(document).off(Event.FOCUSIN) // Guard against infinite focus loop
            .on(Event.FOCUSIN, function safeSetFocus(event) {
                if (document !== event.target &&
                    that._element !== event.target &&
                    \$(that._element).has(event.target).length
                ) {
                    \$(document).off(Event.FOCUSIN);
                    that._element.focus();
                    \$(document).on(Event.FOCUSIN, safeSetFocus);
                }
            });
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-modal.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-modal.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/bootstrap/bootstrap-modal.js");
    }
}
