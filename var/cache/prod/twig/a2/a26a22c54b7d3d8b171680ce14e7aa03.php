<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/macros/phone.html */
class __TwigTemplate_a8d665207e4383b4f4df36c734346b50 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (obj.phone) {
    var phoneNumber;
    if (_.isObject(phone)) {
        if (!_.isEmpty(phone.phone)) {
            phoneNumber = phone.phone;
        }
    } else {
        phoneNumber = phone;
    }
    var label = obj.title || phoneNumber;
    %>
    <% if (phoneNumber) { %>
    <a href=\"tel:<%- phoneNumber %>\" title=\"<%- label %>\" class=\"phone nowrap\"><bdo dir=\"ltr\"><%- label %></bdo></a>
    <% } %>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/macros/phone.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/macros/phone.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/macros/phone.html");
    }
}
