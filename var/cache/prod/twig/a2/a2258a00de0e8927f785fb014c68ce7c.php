<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/skeleton/_functions.scss */
class __TwigTemplate_ea7f0112eca1a016ad4b7f4821608532 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */
@use 'sass:list';
@use 'sass:map';

@import './rect';
@import './ellipse';

// Function for normalize arguments of \"@mixin skeleton\"
// Make sure all items with any type (list or map) merged into flat list
// @param: List \$particles
// @return: Flat list of \$particles
@function skeleton-normalize(\$particles...) {
    \$result: ();

    @each \$particle in \$particles {
        @if type-of(\$particle) == 'list' {
            \$result: list.join(\$result, \$particle, comma);
        }

        @if type-of(\$particle) == 'map' {
            \$result: list.append(\$result, \$particle, comma);
        }
    }

    @return \$result;
}

// background-image generator
// Take list of skeleton items and call generator by type
// @param: List \$particles
// @return: List of background-image value
@function skeleton-image(\$particles...) {
    \$result: null;

    @if length(\$particles) == 0 {
        @return \$result;
    }

    \$result: ();

    @each \$particle in \$particles {
        @if map.get(\$particle, 'type') == 'rect' {
            \$result: list.append(\$result, skeleton-image-rect(\$particle), comma);
        }

        @if map.get(\$particle, 'type') == 'ellipse' {
            \$result: list.append(\$result, skeleton-image-ellipse(\$particle), comma);
        }
    }

    @return \$result;
}

// background-size generator
// Take list of skeleton items and call generator by type
// @param: List \$particles
// @return: List of background-size value
@function skeleton-size(\$particles...) {
    \$result: null;

    @if length(\$particles) == 0 {
        @return \$result;
    }

    \$result: ();

    @each \$particle in \$particles {
        @if map.get(\$particle, 'type') == 'rect' {
            \$result: list.append(\$result, skeleton-size-rect(\$particle), comma);
        }

        @if map.get(\$particle, 'type') == 'ellipse' {
            \$result: list.append(\$result, skeleton-size-ellipse(\$particle), comma);
        }
    }

    @return \$result;
}

// background-position generator
// Take list of skeleton items and call generator by type
// @param: List \$particles
// @return: List of background-position value
@function skeleton-position(\$particles...) {
    \$result: null;

    @if length(\$particles) == 0 {
        @return \$result;
    }

    \$result: ();

    @each \$particle in \$particles {
        @if map.get(\$particle, 'type') == 'rect' {
            \$result: list.append(\$result, skeleton-position-rect(\$particle), comma);
        }

        @if map.get(\$particle, 'type') == 'ellipse' {
            \$result: list.append(\$result, skeleton-position-ellipse(\$particle), comma);
        }
    }

    @return \$result;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/skeleton/_functions.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/skeleton/_functions.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/skeleton/_functions.scss");
    }
}
