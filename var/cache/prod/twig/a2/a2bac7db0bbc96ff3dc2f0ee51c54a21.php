<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/no-data.scss */
class __TwigTemplate_0574e4d63f1187def07a4106a41e9697 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$no-data-offset: 4px 0 !default;
\$no-data-color: \$primary-550 !default;
\$no-data-font-size: 13px !default;
\$no-data-line-height: 1.35 !default;

\$no-data-icon: \$fa-var-search !default;
\$no-data-icon-offset: 6px !default;

\$no-data-title-color: \$primary-200 !default;
\$no-data-title-font-size: 18px !default;
\$no-data-title-font-weight: font-weight('bold') !default;
\$no-data-title-offset: 10px !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/no-data.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/no-data.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/no-data.scss");
    }
}
