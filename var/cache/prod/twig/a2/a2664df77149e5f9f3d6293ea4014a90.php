<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/_dots_tabs-content.html.twig */
class __TwigTemplate_b14a0815a5ea673174611e5ce48bbe32 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'root' => [$this, 'block_root'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/_dots_tabs-content.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/_dots_tabs-content.html.twig", 4)->unwrap();
        // line 5
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/Menu/_dots_tabs-content.html.twig", 5)->unwrap();
        // line 6
        echo "
    <div class=\"tab-content\">
        ";
        // line 8
        $context["items"] = ($context["item"] ?? null);
        // line 9
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["items"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 10
            echo "            ";
            $context["showNonAuthorized"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, true, false, 10), "show_non_authorized", [], "any", true, true, false, 10) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, false, false, 10), "show_non_authorized", [], "any", false, false, false, 10));
            // line 11
            echo "            ";
            $context["displayable"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, false, false, 11), "isAllowed", [], "any", false, false, false, 11) || ($context["showNonAuthorized"] ?? null));
            // line 12
            echo "            ";
            $context["componentAttrs"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, true, false, 12), "component_options", [], "any", true, true, false, 12)) ? (twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, false, false, 12), "component_options", [], "any", false, false, false, 12)], 12, $context, $this->getSourceContext())) : (""));
            // line 13
            echo "            ";
            if (($context["displayable"] ?? null)) {
                // line 14
                echo "                <div class=\"tab-pane";
                if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, true, false, 14), "active_if_first_is_empty", [], "any", true, true, false, 14)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, true, false, 14), "active_if_first_is_empty", [], "any", false, false, false, 14), false)) : (false))) {
                    echo " active";
                }
                echo "\"
                     id=\"";
                // line 15
                echo twig_escape_filter($this->env, twig_trim_filter(twig_lower_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 15), [" " => "_", "#" => "_"]))), "html", null, true);
                echo "-content\" ";
                echo twig_escape_filter($this->env, ($context["componentAttrs"] ?? null), "html", null, true);
                echo "
                     role=\"tabpanel\"
                     aria-labelledby=\"";
                // line 17
                echo twig_escape_filter($this->env, twig_trim_filter(twig_lower_filter($this->env, twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 17), [" " => "_", "#" => "_"]))), "html", null, true);
                echo "-tab\"
                     aria-orientation=\"vertical\"
                >
                    ";
                // line 20
                $context["options"] = ["tabTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "label", [], "any", false, false, false, 20)];
                // line 21
                echo "                    ";
                $context["options"] = ["defaultUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_default")];
                // line 22
                echo "                    ";
                $context["options"] = twig_array_merge(($context["options"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "extras", [], "any", false, false, false, 22));
                // line 23
                echo "                    ";
                echo $this->extensions['Oro\Bundle\NavigationBundle\Twig\MenuExtension']->render(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "name", [], "any", false, false, false, 23), ($context["options"] ?? null));
                echo "
                </div>
            ";
            }
            // line 26
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "        ";
        $context["item"] = ($context["items"] ?? null);
        // line 28
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/_dots_tabs-content.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  123 => 28,  120 => 27,  114 => 26,  107 => 23,  104 => 22,  101 => 21,  99 => 20,  93 => 17,  86 => 15,  79 => 14,  76 => 13,  73 => 12,  70 => 11,  67 => 10,  62 => 9,  60 => 8,  56 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/_dots_tabs-content.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/_dots_tabs-content.html.twig");
    }
}
