<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/element-value-copy-to-clipboard-view.js */
class __TwigTemplate_96af0e377d2d34b7db0d21dcd9159930 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');
    const tools = require('oroui/js/tools');
    const error = require('oroui/js/error');
    const BaseView = require('oroui/js/app/views/base/view');
    const messenger = require('oroui/js/messenger');

    const ElementValueCopyToClipboardView = BaseView.extend({
        options: {
            elementSelector: null,
            messages: {
                copy_not_supported: 'oro.ui.element_value.messages.copy_not_supported',
                copied: 'oro.ui.element_value.messages.copied',
                copy_not_successful: 'oro.ui.element_value.messages.copy_not_successful'
            }
        },

        events: {
            click: 'clickHandler'
        },

        /**
         * @inheritdoc
         */
        constructor: function ElementValueCopyToClipboardView(options) {
            ElementValueCopyToClipboardView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this.options = _.defaults(options || {}, this.options);

            ElementValueCopyToClipboardView.__super__.initialize.call(this, options);
        },

        clickHandler: function() {
            const \$source = this.options.elementSelector !== null ? \$(this.options.elementSelector) : this.\$el;
            const textToCopy = \$source.text();
            const \$textarea = this.createUtilityTextarea(textToCopy);

            \$source.closest('.ui-dialog, body').append(\$textarea);

            if (tools.isIOS()) {
                const selection = window.getSelection();
                const range = document.createRange();

                range.selectNodeContents(\$textarea[0]);
                selection.removeAllRanges();
                selection.addRange(range);
                \$textarea[0].setSelectionRange(0, textToCopy.length);
            } else {
                \$textarea.select();
            }

            try {
                if (document.execCommand('copy')) {
                    this.onCopySuccess();
                } else {
                    this.onCopyFailed();
                }
            } catch (err) {
                error.showErrorInConsole(err);
                this.onCopyNotSupported();
            }

            \$textarea.remove();
        },

        onCopySuccess: function() {
            messenger.notificationFlashMessage('success', __(this.options.messages.copied));
        },

        onCopyFailed: function() {
            messenger.notificationFlashMessage('warning', __(this.options.messages.copy_not_successful));
        },

        onCopyNotSupported: function() {
            messenger.notificationFlashMessage('warning', __(this.options.messages.copy_not_supported));
        },

        /**
         * Creates jQuery object with textarea stylized to be placed outside screen and containing text to copy
         *
         * @param {string} value
         * @return {jQuery}
         */
        createUtilityTextarea: function(value) {
            const \$textarea = \$('<textarea/>', {'contenteditable': 'true', 'aria-hidden': 'true'});

            \$textarea.css({position: 'fixed', top: '-1000px'}).val(value);

            return \$textarea;
        }
    });

    return ElementValueCopyToClipboardView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/element-value-copy-to-clipboard-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/element-value-copy-to-clipboard-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/element-value-copy-to-clipboard-view.js");
    }
}
