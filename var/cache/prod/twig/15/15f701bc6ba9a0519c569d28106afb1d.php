<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImap/Connection/checkMailbox.html.twig */
class __TwigTemplate_c46f994a80ef1254a40a284bd4ed0177 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "origin", [], "any", false, true, false, 1), "folders", [], "any", true, true, false, 1)) {
            // line 2
            echo "    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "origin", [], "any", false, false, false, 2), "folders", [], "any", false, false, false, 2), 'row');
            echo "
";
        }
        // line 4
        echo "
";
        // line 5
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, true, false, 5), "userEmailOrigin", [], "any", false, true, false, 5), "folders", [], "any", true, true, false, 5)) {
            // line 6
            echo "    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "imapAccountType", [], "any", false, false, false, 6), "userEmailOrigin", [], "any", false, false, false, 6), "folders", [], "any", false, false, false, 6), 'row');
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroImap/Connection/checkMailbox.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 6,  48 => 5,  45 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImap/Connection/checkMailbox.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImapBundle/Resources/views/Connection/checkMailbox.html.twig");
    }
}
