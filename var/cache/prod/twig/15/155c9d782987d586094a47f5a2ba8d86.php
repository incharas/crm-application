<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/main.scss */
class __TwigTemplate_1ae229a0d9bf29c149e68e5c3d8d958f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.mobile-version {
    @import 'variables';
    @import 'app-header';
    @import 'attribute-item';
    @import 'clearfix';
    @import 'content-sidebar';
    @import 'dialog';
    @import 'dropdown';
    @import 'form';
    @import 'layout';
    @import 'form-description';
    @import 'login';
    @import 'main-menu';
    @import 'modal';
    @import 'nav';
    @import 'page-header';
    @import 'accordion';
    @import 'flash-messages';
    @import 'inline-actions';
    @import 'select2';
    @import 'jstree';
    @import 'tables';
    @import 'scrollspy';
    @import 'popover';
    @import 'widget-picker';
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/main.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/main.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/main.scss");
    }
}
