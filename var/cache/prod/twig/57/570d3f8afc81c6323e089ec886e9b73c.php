<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/tab-item-view.js */
class __TwigTemplate_8ea383e72bc0d2e399492c50de3d32f9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require, exports, module) {
    'use strict';

    const _ = require('underscore');
    const \$ = require('jquery');
    const BaseView = require('oroui/js/app/views/base/view');
    let config = require('module-config').default(module.id);

    config = _.extend({
        className: 'nav-item',
        templateClassName: 'nav-link'
    }, config);

    const TabItemView = BaseView.extend({
        tagName: 'li',

        className: config.className,

        template: require('tpl-loader!oroui/templates/tab-collection-item.html'),

        listen: {
            'change:active model': 'updateStates',
            'change:changed model': 'updateStates'
        },

        events: {
            'shown.bs.tab': 'onTabShown',
            'click': 'onTabClick'
        },

        /**
         * @inheritdoc
         */
        constructor: function TabItemView(options) {
            TabItemView.__super__.constructor.call(this, options);
        },

        initialize: function(options) {
            TabItemView.__super__.initialize.call(this, options);

            this.updateStates();
        },

        updateStates: function() {
            this.\$el.toggleClass('changed', !!this.model.get('changed'));
            const \$tab = this.\$('[role=\"tab\"]');
            if (\$tab.attr('aria-selected') !== String(this.model.get('active'))) {
                \$tab.attr('aria-selected', this.model.get('active'));
            }
            if (this.model.get('active')) {
                const tabPanel = this.model.get('controlTabPanel') || this.model.get('id');
                \$('#' + tabPanel).attr('aria-labelledby', this.model.get('uniqueId'));
            }
        },

        onTabShown: function(e) {
            this.model.set('active', true);
            this.model.trigger('select', this.model);
        },

        onTabClick: function(e) {
            this.model.trigger('click', this.model);
        }
    });

    return TabItemView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/tab-item-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/tab-item-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/tab-item-view.js");
    }
}
