<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMessageQueue/ConsumerHeartbeat/queue_consumer_heartbeat_js.html.twig */
class __TwigTemplate_7434dd815b6abe7533248730879bbefc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->checkWsConnected()) {
            // line 2
            echo "<script>
    loadModules(['orosync/js/sync', 'oroui/js/messenger'],
        function (sync, messenger) {
            sync.subscribe('oro/message_queue_heartbeat', function (response) {
                messenger.notificationMessage(
                    'error',
                    ";
            // line 9
            echo "
                    '";
            // line 10
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.message_queue_job.no_alive_consumers"), "html", null, true);
            echo "'
                );
            });
        });
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroMessageQueue/ConsumerHeartbeat/queue_consumer_heartbeat_js.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 10,  47 => 9,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMessageQueue/ConsumerHeartbeat/queue_consumer_heartbeat_js.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/MessageQueueBundle/Resources/views/ConsumerHeartbeat/queue_consumer_heartbeat_js.html.twig");
    }
}
