<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/history-state-collection.js */
class __TwigTemplate_c25d9c51d9093620cfa83d276f5b84f9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const HistoryStateModel = require('./history-state-model');
    const BaseCollection = require('./base/collection');

    const HistoryStateCollection = BaseCollection.extend({
        model: HistoryStateModel,

        /**
         * @inheritdoc
         */
        constructor: function HistoryStateCollection(...args) {
            HistoryStateCollection.__super__.constructor.apply(this, args);
        }
    });

    return HistoryStateCollection;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/history-state-collection.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/history-state-collection.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/history-state-collection.js");
    }
}
