<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Widget/widget/buttons.html.twig */
class __TwigTemplate_1367b52294e7a8ae30787132be008eb0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["workflowMacros"] = $this->macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/Widget/widget/buttons.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["workflowsData"] ?? null));
        foreach ($context['_seq'] as $context["workflowName"] => $context["workflow"]) {
            // line 5
            echo "        ";
            if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "transitionsData", [], "any", false, false, false, 5)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "resetAllowed", [], "any", false, false, false, 5))) {
                // line 6
                echo "            ";
                $context["blockId"] = ("entity-transitions-container-" . twig_random($this->env));
                // line 7
                echo "            <div class=\"btn-group\" id=\"";
                echo twig_escape_filter($this->env, ($context["blockId"] ?? null), "html", null, true);
                echo "\">
                ";
                // line 8
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["workflow"], "transitionsData", [], "any", false, false, false, 8));
                foreach ($context['_seq'] as $context["_key"] => $context["data"]) {
                    // line 9
                    echo "                    ";
                    // line 10
                    echo "                    ";
                    // line 11
                    echo "                    ";
                    $context["transitionData"] = ["enabled" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 12
$context["data"], "isAllowed", [], "any", false, false, false, 12), "message" => twig_nl2br(twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 13
$context["data"], "transition", [], "any", false, false, false, 13), "message", [], "any", false, false, false, 13), [], "workflows"), "html", null, true)), "transition-condition-messages" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 14
$context["data"], "errors", [], "any", false, false, false, 14)];
                    // line 16
                    echo "
                    ";
                    // line 17
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, false, false, 17), "displayType", [], "any", false, false, false, 17) == "dialog")) {
                        // line 18
                        echo "                        ";
                        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "workflowItem", [], "any", true, true, false, 18)) {
                            // line 19
                            echo "                            ";
                            $context["data"] = twig_array_merge($context["data"], ["workflowItem" => null]);
                            // line 20
                            echo "                            ";
                            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "transition", [], "any", false, false, false, 20), "hasForm", [], "method", false, false, false, 20)) {
                                // line 21
                                echo "                                ";
                                $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["dialog-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_widget_start_transition_form", ["workflowName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                                 // line 24
$context["data"], "workflow", [], "any", false, false, false, 24), "name", [], "any", false, false, false, 24), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                                 // line 25
$context["data"], "transition", [], "any", false, false, false, 25), "name", [], "any", false, false, false, 25), "entityId" =>                                 // line 26
($context["entity_id"] ?? null)])]);
                                // line 29
                                echo "                            ";
                            }
                            // line 30
                            echo "
                            ";
                            // line 32
                            echo "                            ";
                            $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_start", ["workflowName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 35
$context["data"], "workflow", [], "any", false, false, false, 35), "name", [], "any", false, false, false, 35), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 36
$context["data"], "transition", [], "any", false, false, false, 36), "name", [], "any", false, false, false, 36), "entityId" =>                             // line 37
($context["entity_id"] ?? null)])]);
                            // line 40
                            echo "                        ";
                        }
                        // line 41
                        echo "                    ";
                    } else {
                        // line 42
                        echo "                        ";
                        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["data"], "workflowItem", [], "any", true, true, false, 42)) {
                            // line 43
                            echo "                            ";
                            $context["data"] = twig_array_merge($context["data"], ["workflowItem" => null]);
                            // line 44
                            echo "                            ";
                            $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_start_transition_form", ["workflowName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 47
$context["data"], "workflow", [], "any", false, false, false, 47), "name", [], "any", false, false, false, 47), "transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 48
$context["data"], "transition", [], "any", false, false, false, 48), "name", [], "any", false, false, false, 48), "entityId" =>                             // line 49
($context["entity_id"] ?? null)])]);
                            // line 52
                            echo "                        ";
                        } else {
                            // line 53
                            echo "                            ";
                            $context["transitionData"] = twig_array_merge(($context["transitionData"] ?? null), ["transition-url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_transition_form", ["transitionName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 56
$context["data"], "transition", [], "any", false, false, false, 56), "name", [], "any", false, false, false, 56), "workflowItemId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 57
$context["data"], "workflowItem", [], "any", false, false, false, 57), "id", [], "any", false, false, false, 57)])]);
                            // line 60
                            echo "                        ";
                        }
                        // line 61
                        echo "                    ";
                    }
                    // line 62
                    echo "
                    ";
                    // line 63
                    echo twig_call_macro($macros["workflowMacros"], "macro_renderTransitionButton", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 64
$context["data"], "workflow", [], "any", false, false, false, 64), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 65
$context["data"], "transition", [], "any", false, false, false, 65), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 66
$context["data"], "workflowItem", [], "any", false, false, false, 66),                     // line 67
($context["transitionData"] ?? null)], 63, $context, $this->getSourceContext());
                    // line 68
                    echo "
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['data'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 70
                echo "            </div>
        ";
            }
            // line 72
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['workflowName'], $context['workflow'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 73
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/widget/buttons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 73,  156 => 72,  152 => 70,  145 => 68,  143 => 67,  142 => 66,  141 => 65,  140 => 64,  139 => 63,  136 => 62,  133 => 61,  130 => 60,  128 => 57,  127 => 56,  125 => 53,  122 => 52,  120 => 49,  119 => 48,  118 => 47,  116 => 44,  113 => 43,  110 => 42,  107 => 41,  104 => 40,  102 => 37,  101 => 36,  100 => 35,  98 => 32,  95 => 30,  92 => 29,  90 => 26,  89 => 25,  88 => 24,  86 => 21,  83 => 20,  80 => 19,  77 => 18,  75 => 17,  72 => 16,  70 => 14,  69 => 13,  68 => 12,  66 => 11,  64 => 10,  62 => 9,  58 => 8,  53 => 7,  50 => 6,  47 => 5,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/widget/buttons.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/widget/buttons.html.twig");
    }
}
