<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMarketingList/MarketingList/view.html.twig */
class __TwigTemplate_539bfc75a770c4e249d672aad30c0a76 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMarketingList/MarketingList/view.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroMarketingList/MarketingList/view.html.twig", 3)->unwrap();
        // line 4
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroMarketingList/MarketingList/view.html.twig", 4)->unwrap();
        // line 5
        $macros["segmentQD"] = $this->macros["segmentQD"] = $this->loadTemplate("@OroSegment/macros.html.twig", "@OroMarketingList/MarketingList/view.html.twig", 5)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["entity"] ?? null), "name", [], "any", true, true, false, 7)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 7), "N/A")) : ("N/A"))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroMarketingList/MarketingList/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMarketingList/MarketingList/view.html.twig", 10)->unwrap();
        // line 11
        echo "
    ";
        // line 12
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("marketing_list_nav_buttons", $context)) ? (_twig_default_filter(($context["marketing_list_nav_buttons"] ?? null), "marketing_list_nav_buttons")) : ("marketing_list_nav_buttons")), ["entity" => ($context["entity"] ?? null)]);
        // line 13
        echo "    ";
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "type", [], "any", false, false, false, 13), "name", [], "any", false, false, false, 13) == twig_constant("Oro\\Bundle\\MarketingListBundle\\Entity\\MarketingListType::TYPE_STATIC")) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)))) {
            // line 14
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_post_marketinglist_segment_run", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["entity"] ?? null), "segment", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15)]), "aCss" => "no-hash run-button btn", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.refresh_list"), "iCss" => "fa-refresh", "dataAttributes" => ["page-component-module" => "orosegment/js/app/components/refresh-button", "page-component-options" => json_encode(["successMessage" => "oro.marketinglist.refresh_dialog.success", "content" => "oro.marketinglist.refresh_dialog.content", "reloadRequired" => true])]]], 14, $context, $this->getSourceContext());
            // line 27
            echo "
    ";
        }
        // line 29
        echo "    ";
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)))) {
            // line 30
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["entity"] ?? null), "id", [], "any", false, false, false, 31)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.entity_label")]], 30, $context, $this->getSourceContext());
            // line 33
            echo "
    ";
        }
        // line 35
        echo "    ";
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)))) {
            // line 36
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_marketinglist", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 37
($context["entity"] ?? null), "id", [], "any", false, false, false, 37)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-user", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 41
($context["entity"] ?? null), "id", [], "any", false, false, false, 41), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.entity_label")]], 36, $context, $this->getSourceContext());
            // line 43
            echo "
    ";
        }
    }

    // line 47
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 49
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 52
($context["entity"] ?? null), "name", [], "any", false, false, false, 52)];
        // line 54
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 57
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMarketingList/MarketingList/view.html.twig", 58)->unwrap();
        // line 60
        ob_start(function () { return ''; });
        // line 61
        echo "<div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 63
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 63)], 63, $context, $this->getSourceContext());
        echo "
                ";
        // line 64
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.union.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "union", [], "any", false, false, false, 64)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No")))], 64, $context, $this->getSourceContext());
        echo "
                ";
        // line 65
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.type.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "type", [], "any", false, false, false, 65), "label", [], "any", false, false, false, 65))], 65, $context, $this->getSourceContext());
        echo "
                ";
        // line 66
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 66)], 66, $context, $this->getSourceContext());
        // line 68
        ob_start(function () { return ''; });
        // line 69
        echo "<i class=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "icon", [], "any", false, false, false, 69), "html", null, true);
        echo " hide-text\"></i>&nbsp;";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "label", [], "any", false, false, false, 69), "html", null, true);
        $context["entityData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 72
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.entity.label"), ($context["entityData"] ?? null)], 72, $context, $this->getSourceContext());
        echo "

                ";
        // line 74
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "segment", [], "any", false, false, false, 74))) {
            // line 75
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.entity_label"), twig_call_macro($macros["UI"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 77
($context["entity"] ?? null), "segment", [], "any", false, false, false, 77), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "segment", [], "any", false, false, false, 77), "name", [], "any", false, false, false, 77), "oro_segment_view"], 77, $context, $this->getSourceContext())], 75, $context, $this->getSourceContext());
            // line 78
            echo "
                ";
        }
        // line 80
        echo "
                ";
        // line 81
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.owner.label"), twig_call_macro($macros["UI"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 83
($context["entity"] ?? null), "owner", [], "any", false, false, false, 83), $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 83)), "oro_user_view"], 83, $context, $this->getSourceContext())], 81, $context, $this->getSourceContext());
        // line 84
        echo "
            </div>
            <div class=\"responsive-block\">
                ";
        // line 87
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 87, $context, $this->getSourceContext());
        echo "
            </div>
        </div>";
        $context["listInformation"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 92
        ob_start(function () { return ''; });
        // line 93
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("marketing_list_sync_info", $context)) ? (_twig_default_filter(($context["marketing_list_sync_info"] ?? null), "marketing_list_sync_info")) : ("marketing_list_sync_info")), ["marketingList" => ($context["entity"] ?? null)]);
        $context["syncStatusData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 96
        $context["itemsGridName"] = (($context["gridName"] ?? null) . "_items");
        // line 97
        echo "    ";
        $context["removedItemsGridName"] = (($context["gridName"] ?? null) . "_removed_items");
        // line 98
        echo "    ";
        $context["itemsMixin"] = "oro-marketing-list-items-mixin";
        // line 99
        echo "    ";
        $context["removedItemsMixin"] = "oro-marketing-list-removed-items-mixin";
        // line 100
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "manual", [], "any", false, false, false, 100)) {
            // line 101
            echo "        ";
            $context["itemsMixin"] = "oro-marketing-list-manual-items-mixin";
            // line 102
            echo "        ";
            $context["removedItemsMixin"] = "oro-marketing-list-manual-removed-items-mixin";
            // line 103
            echo "    ";
        }
        // line 104
        ob_start(function () { return ''; });
        // line 105
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["itemsGridName"] ?? null), ["grid-mixin" => ($context["itemsMixin"] ?? null)], ["cssClass" => "inner-grid"]], 105, $context, $this->getSourceContext());
        $context["listData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 107
        ob_start(function () { return ''; });
        // line 108
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["removedItemsGridName"] ?? null), ["grid-mixin" => ($context["removedItemsMixin"] ?? null)], ["cssClass" => "inner-grid"]], 108, $context, $this->getSourceContext());
        $context["removedItemsGrid"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 111
        $context["generalSectionBlocks"] = [0 => ["data" => [0 => ($context["listInformation"] ?? null)]]];
        // line 112
        echo "    ";
        if ( !twig_test_empty(($context["syncStatusData"] ?? null))) {
            // line 113
            echo "        ";
            $context["generalSectionBlocks"] = twig_array_merge(($context["generalSectionBlocks"] ?? null), [0 => ["data" => [0 =>             // line 114
($context["syncStatusData"] ?? null)]]]);
            // line 116
            echo "    ";
        }
        // line 117
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.block.general"), "class" => "active", "subblocks" =>         // line 121
($context["generalSectionBlocks"] ?? null)], 1 => ["title" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 124
($context["config"] ?? null), "plural_label", [], "any", false, false, false, 124), "subblocks" => [0 => ["data" => [0 =>         // line 126
($context["listData"] ?? null)]]]], 2 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.block.removed"), "subblocks" => [0 => ["data" => [0 =>         // line 132
($context["removedItemsGrid"] ?? null)]]]]];
        // line 136
        echo "
    <div data-page-component-module=\"oromarketinglist/js/app/components/grid-linker\"
         data-page-component-options=\"";
        // line 138
        echo twig_escape_filter($this->env, json_encode([0 => ["main" =>         // line 139
($context["itemsGridName"] ?? null), "secondary" => ($context["removedItemsGridName"] ?? null)], 1 => ["main" =>         // line 140
($context["removedItemsGridName"] ?? null), "secondary" => ($context["itemsGridName"] ?? null)]]), "html", null, true);
        // line 141
        echo "\"></div>

    ";
        // line 143
        $context["id"] = "marketingListView";
        // line 144
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 145
        echo "
    ";
        // line 146
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroMarketingList/MarketingList/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 146,  265 => 145,  262 => 144,  260 => 143,  256 => 141,  254 => 140,  253 => 139,  252 => 138,  248 => 136,  246 => 132,  245 => 126,  244 => 124,  243 => 121,  241 => 117,  238 => 116,  236 => 114,  234 => 113,  231 => 112,  229 => 111,  226 => 108,  224 => 107,  221 => 105,  219 => 104,  216 => 103,  213 => 102,  210 => 101,  207 => 100,  204 => 99,  201 => 98,  198 => 97,  196 => 96,  193 => 93,  191 => 92,  185 => 87,  180 => 84,  178 => 83,  177 => 81,  174 => 80,  170 => 78,  168 => 77,  166 => 75,  164 => 74,  159 => 72,  153 => 69,  151 => 68,  149 => 66,  145 => 65,  141 => 64,  137 => 63,  133 => 61,  131 => 60,  128 => 58,  124 => 57,  117 => 54,  115 => 52,  114 => 49,  112 => 48,  108 => 47,  102 => 43,  100 => 41,  99 => 37,  97 => 36,  94 => 35,  90 => 33,  88 => 31,  86 => 30,  83 => 29,  79 => 27,  77 => 15,  75 => 14,  72 => 13,  70 => 12,  67 => 11,  64 => 10,  60 => 9,  55 => 1,  53 => 7,  50 => 5,  48 => 4,  46 => 3,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMarketingList/MarketingList/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/MarketingListBundle/Resources/views/MarketingList/view.html.twig");
    }
}
