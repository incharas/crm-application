<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/all.css */
class __TwigTemplate_c9aa7bbd97784d3c0eabe75bb45f1009 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.footer,
footer,
#footer {
    font-size: 13px;
    display: block;
    width: 100%;
}

#footer-frame {
    display: table-row;
    height: 1%;
}

/* other style */
.checkbox {
    margin: 0;
}

.form-signin .input-prepend {
    margin-bottom: 13px;
}

.form-signin .input-prepend--last {
    margin-bottom: 0;
}

.scroll-holder {
    overflow: auto;
    max-height: 575px;
}

#oro_user_group_form_roles {
    height: 67px;
}

.version-container {
    display: inline-block;
    width: 100%;
    text-align: center;
    font-size: 14px;
    line-height: 1em;
}

.dropdown-toggle:focus {
    outline: 0;
}

.user-status-list {
    list-style: none;
    display: table;
    table-layout: fixed;
    margin: 0 0 10px;
}

.user-status-list li {
    height: 25px;
}

.user-status-list .list-group-item {
    width: 100%;
    display: table-row;
}

.user-status-list .list-group-item-text {
    display: table-cell;
}

.oro-multiselect-holder .float-holder span.validation-failed {
    padding-top: 4px;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/all.css";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/all.css", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/all.css");
    }
}
