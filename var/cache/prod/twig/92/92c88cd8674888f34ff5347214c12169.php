<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/settings/_functions.scss */
class __TwigTemplate_e6ed4fb6c9381ee6a315ce6d5e3c7da1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:meta';
@use 'sass:list';
@use 'sass:map';

// @return the value in a \$font-weights map associated with a given key;
// Use: font-weight: font-weight('bold') => font-weight: 700;

@function font-weight(\$key) {
    \$font-weight-key: map.get(\$font-weights, \$key);

    @if (\$font-weight-key) {
        @return \$font-weight-key;
    } @else {
        @warn 'Font weight value not found:' \$key;
    }
}

// @return modified selector
// Use: modify-selector(\$selector: 'a', \$part-before: '~ ') =>  '~ a';
@function modify-selector(\$selector, \$part-before: null, \$part-after: null) {
    @if (\$part-before) {
        \$selector: \$part-before + \$selector;
    }

    @if (\$part-after) {
        \$selector: \$part-after + \$selector;
    }

    @return \$selector;
}

// @return modified list of selectors
// Use: modify-selector(selectors: 'a , button', \$part-before: '~ ') => '~ a, ~ button';
@function modify-selectors(\$selectors, \$part-before: null, \$part-after: null) {
    @if (meta.type-of(\$selectors) != 'list') {
        @warn '\$selectors: #{\$selectors} must be a list type';
    }

    \$modified-selectors: ();
    \$list-separator: list.separator(\$selectors);

    @each \$selector in \$selectors {
        \$modified-selectors: list.append(
            \$modified-selectors,
            modify-selector(\$selector, \$part-before, \$part-after),
            \$separator: \$list-separator
        );
    }

    @return \$modified-selectors;
}

// @return the list of CSS selectors for HTML elements that can receive focus
// Use: font-get-focusable-selector() => 'a[href]:not([tabindex=\"-1\"]), input:not([disabled]):not([tabindex=\"-1\"])'
@function get-focusable-selectors(\$whole-list: false, \$part-before: null, \$part-after: null) {
    \$base: (
        'a[href]',
        'input:not([disabled])',
        'select:not([disabled])',
        'textarea:not([disabled])',
        'button:not([disabled])',
        '.dropdown-menu',
        '[tabindex]'
    );
    \$more: (
        'area[href]',
        'iframe',
        '[contentEditable=true]'
    );

    @if (\$whole-list) {
        @return modify-selectors(\$base, \$part-before, \$part-after) + modify-selectors(\$more, \$part-before, \$part-after);
    } @else {
        @return modify-selectors(\$base, \$part-before, \$part-after);
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/settings/_functions.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/settings/_functions.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/settings/_functions.scss");
    }
}
