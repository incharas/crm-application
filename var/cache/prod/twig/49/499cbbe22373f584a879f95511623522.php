<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/page-header.scss */
class __TwigTemplate_d8ab9232061974781b119cb700ec48a9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.page-title {
    &__path {
        .top-row {
            .pull-left {
                float: none;
                display: inline-block;
                vertical-align: middle;
            }
        }
    }
}

.container-fluid.page-title {
    padding-right: 0;
    padding-left: \$content-padding;

    .navbar-extra {
        margin-bottom: 10px;
    }

    .navbar-extra > .row {
        .title-buttons-container {
            max-width: 100%;

            // All children should have a own font-size
            font-size: 0;
            text-align: right;

            /* stylelint-disable selector-max-compound-selectors */
            > .pull-left,
            > .btn-group {
                text-align: left;
                display: inline-block;
                vertical-align: top;

                .btn {
                    margin-bottom: 4px;
                }
            }

            > .pinned-dropdown > .btn-group {
                text-align: left;
            }

            .separator-btn {
                margin: 0 0 0 \$content-padding-small;
            }
            /* stylelint-enable selector-max-compound-selectors */
        }
    }

    .page-title-center {
        position: relative;
        min-width: 150px;
        flex-grow: 100;

        &:empty {
            display: none;
        }
    }

    .center-under-both {
        .page-title-center {
            flex-basis: 100%;
            order: 3;
            height: 31px;
        }
    }

    .center-under-left {
        .pull-left-extra {
            flex-basis: 100%;
        }
    }

    .navigation {
        .row {
            display: flex;
            flex-flow: row wrap;
            justify-content: flex-end;

            > div {
                margin-bottom: 6px;
            }

            &.inline-info {
                flex-wrap: wrap-reverse;

                > div {
                    margin-bottom: 0;
                }
            }

            > .pull-left-extra {
                flex-grow: 1;
                margin-left: 0;
                text-align: left;
            }

            > .pull-right {
                flex-shrink: 0;
                order: 2;
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/page-header.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/page-header.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/page-header.scss");
    }
}
