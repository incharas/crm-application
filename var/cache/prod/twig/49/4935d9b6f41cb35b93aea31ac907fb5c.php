<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/Grid/dialog/widget.html.twig */
class __TwigTemplate_1b617ba9168c6ec377a3aa7e293f6a13 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'widget_content' => [$this, 'block_widget_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDataGrid/Grid/widget/widget.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroDataGrid/Grid/widget/widget.html.twig", "@OroDataGrid/Grid/dialog/widget.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_widget_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["renderParams"] = twig_array_merge(($context["renderParams"] ?? null), ["routerEnabled" => false, "jsmodules" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["renderParams"] ?? null), "jsmodules", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "jsmodules", [], "any", false, false, false, 6), [0 => "orodatagrid/js/row-select-for-widget/builder"])) : ([0 => "orodatagrid/js/row-select-for-widget/builder"])), "gridBuildersOptions" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["renderParams"] ?? null), "gridBuildersOptions", [], "any", true, true, false, 7)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["renderParams"] ?? null), "gridBuildersOptions", [], "any", false, false, false, 7), ["rowSelectForWidget" => ["wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["app"] ?? null), "request", [], "any", false, false, false, 9), "get", [0 => "_wid"], "method", false, false, false, 9), "multiSelect" =>         // line 10
($context["multiselect"] ?? null)]])) : (["rowSelectForWidget" => ["wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["app"] ?? null), "request", [], "any", false, false, false, 9), "get", [0 => "_wid"], "method", false, false, false, 9), "multiSelect" =>         // line 10
($context["multiselect"] ?? null)]]))]);
        // line 14
        echo "
    ";
        // line 15
        $this->displayParentBlock("widget_content", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/Grid/dialog/widget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 15,  59 => 14,  57 => 10,  56 => 9,  55 => 10,  54 => 9,  53 => 7,  52 => 6,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/Grid/dialog/widget.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/Grid/dialog/widget.html.twig");
    }
}
