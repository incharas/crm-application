<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/WorkflowDefinition/update.html.twig */
class __TwigTemplate_5273338f810ca010a488fbcec278622f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head_script' => [$this, 'block_head_script'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["QD"] = $this->macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroWorkflow/WorkflowDefinition/update.html.twig", 2)->unwrap();
        // line 3
        $macros["workflowMacros"] = $this->macros["workflowMacros"] = $this->loadTemplate("@OroWorkflow/macros.html.twig", "@OroWorkflow/WorkflowDefinition/update.html.twig", 3)->unwrap();
        // line 5
        $context["pageComponent"] = ["module" => "oroworkflow/js/app/components/workflow-editor-component", "options" => ["entity" => ["configuration" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["entity"] ?? null), "configuration", [], "any", false, false, false, 9), "translateLinks" => ((        // line 10
array_key_exists("translateLinks", $context)) ? (_twig_default_filter(($context["translateLinks"] ?? null), [])) : ([])), "name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["entity"] ?? null), "name", [], "any", false, false, false, 11), "label" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["entity"] ?? null), "label", [], "any", false, false, false, 12), "entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["entity"] ?? null), "relatedEntity", [], "any", false, false, false, 13), "entity_attribute" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 14
($context["entity"] ?? null), "entityAttributeName", [], "any", true, true, false, 14)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "entityAttributeName", [], "any", false, false, false, 14), "entity")) : ("entity")), "startStep" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["entity"] ?? null), "startStep", [], "any", false, true, false, 15), "name", [], "any", true, true, false, 15)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startStep", [], "any", false, true, false, 15), "name", [], "any", false, false, false, 15), null)) : (null)), "stepsDisplayOrdered" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 16
($context["entity"] ?? null), "stepsDisplayOrdered", [], "any", false, false, false, 16), "priority" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 17
($context["entity"] ?? null), "priority", [], "any", false, false, false, 17), "exclusive_active_groups" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 18
($context["entity"] ?? null), "exclusiveActiveGroups", [], "any", false, false, false, 18), "exclusive_record_groups" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 19
($context["entity"] ?? null), "exclusiveRecordGroups", [], "any", false, false, false, 19), "applications" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 20
($context["entity"] ?? null), "applications", [], "any", false, false, false, 20)], "availableDestinations" =>         // line 22
($context["availableDestinations"] ?? null), "chartOptions" => [], "connectionOptions" => ["detachable" => true]]];

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%workflow_definition.label%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 42
($context["entity"] ?? null), "label", [], "any", false, false, false, 42), [], "workflows")]]);
        // line 44
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 44)) {
            // line 45
            $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_definition_post", ["workflowDefinition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 45)]);
        } else {
            // line 47
            $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_definition_post");
        }
        // line 58
        $context["gridUrl"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_index");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroWorkflow/WorkflowDefinition/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 50
    public function block_head_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 51
        echo "    ";
        $this->displayParentBlock("head_script", $context, $blocks);
        echo "

    ";
        // line 53
        $this->displayBlock('stylesheets', $context, $blocks);
    }

    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'stylesheet');
        echo "
    ";
    }

    // line 60
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 61
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/update.html.twig", 61)->unwrap();
        // line 62
        echo "
    ";
        // line 63
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 63) && ($context["delete_allowed"] ?? null)) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)))) {
            // line 64
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_workflow_definition_delete", ["workflowDefinition" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 65
($context["entity"] ?? null), "name", [], "any", false, false, false, 65)]), "dataRedirect" =>             // line 66
($context["gridUrl"] ?? null), "aCss" => "no-hash remove-button", "id" => "btn-remove-workflow", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 69
($context["entity"] ?? null), "name", [], "any", false, false, false, 69), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.entity_label")]], 64, $context, $this->getSourceContext());
            // line 71
            echo "
        ";
            // line 72
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 72, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 74
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [($context["gridUrl"] ?? null)], 74, $context, $this->getSourceContext());
        echo "

    ";
        // line 76
        $context["html"] = (twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [], 76, $context, $this->getSourceContext()) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [], 76, $context, $this->getSourceContext()));
        // line 77
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 77, $context, $this->getSourceContext());
        echo "
";
    }

    // line 80
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 81
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 81)) {
            // line 82
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 83
($context["entity"] ?? null), "indexPath" =>             // line 84
($context["gridUrl"] ?? null), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 86
($context["entity"] ?? null), "label", [], "any", false, false, false, 86)];
            // line 88
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 90
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.entity_label")]);
            // line 91
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroWorkflow/WorkflowDefinition/update.html.twig", 91)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 92
            echo "    ";
        }
    }

    // line 95
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 96)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 96)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 97
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 97)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 97)), "html", null, true))) : (print ("N/A")));
        echo "</li>
";
    }

    // line 100
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 101
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/update.html.twig", 101)->unwrap();
        // line 102
        echo "    ";
        $macros["workflowDefinitionUpdate"] = $this;
        // line 103
        echo "
    ";
        // line 104
        echo twig_call_macro($macros["QD"], "macro_query_designer_column_chain_template", ["entity-column-chain-template"], 104, $context, $this->getSourceContext());
        echo "

    ";
        // line 106
        $context["requiredConstraint"] = ["NotBlank" => null];
        // line 109
        echo "
    <script type=\"text/template\" id=\"workflow-translate-link-template\">
        <% if (translateLink) { %>";
        // line 111
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLink %>", true], 111, $context, $this->getSourceContext());
        echo "<% } %>
    </script>

    <script type=\"text/template\" id=\"transition-form-template\">
        <div class=\"oro-tabs\">
            <div class=\"oro-tabs__head\">
                <ul class=\"nav nav-tabs\">
                    <li class=\"nav-item\"><a href=\"#transition-form\" class=\"nav-link active\" data-toggle=\"tab\">";
        // line 118
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Info"), "html", null, true);
        echo "</a></li>
                    <li class=\"nav-item\"><a href=\"#transition-attributes\" class=\"nav-link\" data-toggle=\"tab\">";
        // line 119
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Attributes"), "html", null, true);
        echo "</a></li>
                </ul>
            </div>
            <div class=\"oro-tabs__content\">
                <div class=\"tab-content\">
                    <div class=\"tab-pane active\" id=\"transition-form\">
                        <div class=\"tab-data\">
                            <div class=\"form-container\">
                                <form action=\"#\" class=\"form-horizontal\">
                                    <div class=\"control-group\">
                                        ";
        // line 129
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.name.label"), true, "oro.workflow.workflowdefinition.transition.name.tooltip"], 129, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <input type=\"text\" name=\"label\" value=\"<%- label %>\" data-validation=\"";
        // line 131
        echo twig_escape_filter($this->env, json_encode(($context["requiredConstraint"] ?? null)), "html", null, true);
        echo "\">
                                            <% if (name && !_is_clone && typeof translateLinks !== 'undefined' && translateLinks.label) { %>
                                                ";
        // line 133
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.label %>", true], 133, $context, $this->getSourceContext());
        echo "
                                            <% } %>
                                        </div>
                                    </div>

                                    <% if(!stepFrom || stepFrom.get('name')) {
                                    var stepFromName = stepFrom ? stepFrom.get('name') : '';
                                    %>
                                    <div class=\"control-group\">
                                        ";
        // line 142
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.from_step.label"), true, "oro.workflow.workflowdefinition.transition.step_from.tooltip"], 142, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <select name=\"step_from\" data-validation=\"";
        // line 144
        echo twig_escape_filter($this->env, json_encode(($context["requiredConstraint"] ?? null)), "html", null, true);
        echo "\"
                                            <% if (name && !_is_clone) { %>disabled=\"disabled\"<% } %>
                                            >
                                            <option value=\"\"></option>
                                            <% _.each(allowedStepsFrom, function (step) { %>
                                            <option
                                                value=\"<%- step.get('name') %>\"
                                                <% if (step.get('name') == stepFromName) { %>selected=\"selected\"<% } %>
                                            >
                                                <%- step.get('label') %>
                                            </option>
                                            <% }); %>
                                            </select>
                                        </div>
                                    </div>
                                    <% } %>
                                    <div class=\"control-group\">
                                        ";
        // line 161
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.to_step.label"), true, "oro.workflow.workflowdefinition.transition.step_to.tooltip"], 161, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <select name=\"step_to\" data-validation=\"";
        // line 163
        echo twig_escape_filter($this->env, json_encode(($context["requiredConstraint"] ?? null)), "html", null, true);
        echo "\">
                                                <option value=\"\"></option>
                                                    <% _.each(allowedStepsTo, function (step) { %>
                                                <option
                                                    value=\"<%- step.get('name') %>\"
                                                    <% if (step.get('name') == step_to) { %>selected=\"selected\"<% } %>
                                                >
                                                    <%- step.get('label') %>
                                                </option>
                                                <% }); %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class=\"control-group\">
                                        ";
        // line 178
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.view_form.label"), true, "oro.workflow.workflowdefinition.transition.display_type.tooltip"], 178, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <select name=\"display_type\" data-validation=\"";
        // line 180
        echo twig_escape_filter($this->env, json_encode(($context["requiredConstraint"] ?? null)), "html", null, true);
        echo "\">
                                                <option value=\"dialog\" <% if (display_type == 'dialog') { %>selected=\"selected\"<% } %>>";
        // line 181
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Popup window"), "html", null, true);
        echo "</value>
                                                <option value=\"page\" <% if (display_type == 'page') { %>selected=\"selected\"<% } %>>";
        // line 182
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Separate page"), "html", null, true);
        echo "</value>
                                            </select>
                                        </div>
                                    </div>

                                    <div class=\"control-group destination-page-controls\">
                                        ";
        // line 188
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.destination_page.label"), false], 188, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <select name=\"destination_page\">
                                                ";
        // line 191
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["availableDestinations"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["destination"]) {
            // line 192
            echo "                                                    <option value=\"";
            echo twig_escape_filter($this->env, $context["destination"], "html", null, true);
            echo "\" <% if (destination_page == '";
            echo twig_escape_filter($this->env, $context["destination"], "html", null, true);
            echo "') { %>selected=\"selected\"<% } %>>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans((("oro.workflow.workflowdefinition.transition.destination_page." . ((array_key_exists("destination", $context)) ? (_twig_default_filter($context["destination"], "default")) : ("default"))) . ".label")), "html", null, true);
            echo "</value>
                                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['destination'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 194
        echo "                                            </select>
                                        </div>
                                    </div>

                                    <div class=\"control-group\">
                                        ";
        // line 199
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.warning_message.label"), false, "oro.workflow.workflowdefinition.transition.message.tooltip"], 199, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <textarea name=\"message\"><%= message %></textarea>
                                            <% if (name && !_is_clone && typeof translateLinks !== 'undefined' && translateLinks.message) { %>
                                                ";
        // line 203
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.message %>", true], 203, $context, $this->getSourceContext());
        echo "
                                            <% } %>
                                        </div>
                                    </div>

                                    <div class=\"control-group\">
                                        ";
        // line 209
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.button_label.label"), false, "oro.workflow.workflowdefinition.transition.button_label.tooltip"], 209, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <input type=\"text\" name=\"button_label\" value=\"<%- button_label %>\">
                                            <% if (name && !_is_clone && typeof translateLinks !== 'undefined' && translateLinks.button_label) { %>
                                                ";
        // line 213
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.button_label %>", true], 213, $context, $this->getSourceContext());
        echo "
                                            <% } %>
                                        </div>
                                    </div>
                                    <div class=\"control-group\">
                                        ";
        // line 218
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.button_title.label"), false, "oro.workflow.workflowdefinition.transition.button_title.tooltip"], 218, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <input type=\"text\" name=\"button_title\" value=\"<%- button_title %>\">
                                            <% if (name && !_is_clone && typeof translateLinks !== 'undefined' && translateLinks.button_title) { %>
                                                ";
        // line 222
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.button_title %>", true], 222, $context, $this->getSourceContext());
        echo "
                                            <% } %>
                                        </div>
                                    </div>

                                    ";
        // line 227
        echo twig_call_macro($macros["UI"], "macro_getApplicableForUnderscore", [$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "transition_prototype_icon", [], "any", false, false, false, 227), 'row')], 227, $context, $this->getSourceContext());
        echo "
                                    <% print('<sc' + 'ript>'); %>
                                    loadModules(['jquery'], function(\$) {
                                    \$('#";
        // line 230
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "transition_prototype_icon", [], "any", false, false, false, 230), "vars", [], "any", false, false, false, 230), "id", [], "any", false, false, false, 230), "html", null, true);
        echo "').inputWidget('val', '<%- buttonIcon %>');
                                    });
                                    <% print('</sc' + 'ript>'); %>

                                    <div class=\"control-group\">
                                        ";
        // line 235
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.button_style.label"), false, "oro.workflow.workflowdefinition.transition.button_color.tooltip"], 235, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <select name=\"button_color\">
                                                <% _.each(allowedButtonStyles, function (style) { %>
                                                <option value=\"<%- style.name %>\"
                                                <% if (buttonStyle == style.name) { %>selected=\"selected\"<% } %>
                                                ><%- style.label %></option>
                                                <% }); %>
                                            </select>
                                        </div>
                                    </div>

                                    <div class=\"control-group transition-example-container\">
                                        ";
        // line 248
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.button_preview.label"), false, "oro.workflow.workflowdefinition.transition.button_preview.tooltip"], 248, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <div class=\"transition-btn-example\"></div>
                                        </div>
                                    </div>

                                    <div class=\"widget-actions\">
                                        <button type=\"reset\" class=\"btn\">";
        // line 255
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
                                        <button type=\"submit\" class=\"btn btn-success\">";
        // line 256
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Apply"), "html", null, true);
        echo "</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class=\"tab-pane\" id=\"transition-attributes\">
                        <div class=\"tab-data\">
                            <div class=\"transition-attributes-form-container form-container\"></div>
                            <div class=\"transition-attributes-list-container\" style=\"margin-top: 10px\"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type=\"text/template\" id=\"step-form-template\">
        ";
        // line 275
        $context["orderConstraints"] = twig_array_merge(($context["requiredConstraint"] ?? null), ["Range" => ["minMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("This value should be {{ limit }} or more."), "maxMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("This value should be {{ limit }} or less."), "invalidMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("This value should be a valid number."), "min" => 0, "max" => null]]);
        // line 284
        echo "        <div class=\"form-container\">
            <div class=\"oro-tabs\">
                <div class=\"oro-tabs__head\">
                    <ul class=\"nav nav-tabs\">
                        <li class=\"nav-item\"><a href=\"#step-form\" class=\"nav-link active\" data-toggle=\"tab\">";
        // line 288
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Info"), "html", null, true);
        echo "</a></li>
                        <% if (transitionsAllowed) { %>
                        <li class=\"nav-item\"><a href=\"#step-transitions\" class=\"nav-link\" data-toggle=\"tab\">";
        // line 290
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Transitions"), "html", null, true);
        echo "</a></li>
                        <% } %>
                    </ul>
                </div>
                <div class=\"oro-tabs__content\">
                    <div class=\"tab-content\">
                        <div class=\"tab-pane active\" id=\"step-form\">
                            <div class=\"tab-data\">
                                <form action=\"#\" class=\"form-horizontal\">
                                    <div class=\"control-group\">
                                        ";
        // line 300
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.name.label"), true, "oro.workflow.workflowdefinition.step.name.tooltip"], 300, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <input type=\"text\" name=\"label\" value=\"<%- label %>\" data-validation=\"";
        // line 302
        echo twig_escape_filter($this->env, json_encode(($context["requiredConstraint"] ?? null)), "html", null, true);
        echo "\">
                                            <% if (name && !_is_clone && typeof translateLinks !== 'undefined' && translateLinks.label) { %>
                                                ";
        // line 304
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.label %>", true], 304, $context, $this->getSourceContext());
        echo "
                                            <% } %>
                                        </div>
                                    </div>

                                    <div class=\"control-group\">
                                        ";
        // line 310
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.position.label"), false, "oro.workflow.workflowdefinition.step.order.tooltip"], 310, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <input type=\"text\" name=\"order\" value=\"<%- order %>\" data-validation=\"";
        // line 312
        echo twig_escape_filter($this->env, json_encode(($context["orderConstraints"] ?? null)), "html", null, true);
        echo "\">
                                        </div>
                                    </div>

                                    <div class=\"control-group control-group-checkbox\">
                                        ";
        // line 317
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.final.label"), false, "oro.workflow.workflowdefinition.step.is_final.tooltip"], 317, $context, $this->getSourceContext());
        echo "
                                        <div class=\"controls\">
                                            <input type=\"checkbox\" name=\"is_final\" <% if (is_final) { %>checked=\"checked\"<% } %>>
                                        </div>
                                    </div>

                                    <div class=\"widget-actions\">
                                        <button type=\"reset\" class=\"btn\">";
        // line 324
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
                                        <button type=\"submit\" class=\"btn btn-success\">";
        // line 325
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Apply"), "html", null, true);
        echo "</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <% if (transitionsAllowed) { %>
                        <div class=\"tab-pane\" id=\"step-transitions\">
                            <div class=\"tab-data\">
                                <div class=\"transitions-list-container\"></div>
                            </div>
                        </div>
                        <% } %>
                    </div>
                </div>
            </div>
        </div>
    </script>

    <script type=\"text/template\" id=\"step-list-template\">
        <div class=\"grid-container steps-list\">
            <input name=\"oro_workflow_definition_form[steps]\" type=\"hidden\" value=''>
            <input name=\"oro_workflow_definition_form[transitions]\" type=\"hidden\" value=''>
            <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\" style=\"margin-bottom: 10px\">
                <thead>
                    <tr>
                        <th class=\"label-column\"><span>";
        // line 351
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Step"), "html", null, true);
        echo "</span></th>
                        <th><span>";
        // line 352
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Transitions"), "html", null, true);
        echo "</span></th>
                        <th><span title=\"";
        // line 353
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.step.order.tooltip"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Position"), "html", null, true);
        echo "</span></th>
                        <th class=\"action-column\"><span>";
        // line 354
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Actions"), "html", null, true);
        echo "</span></th>
                    </tr>
                </thead>
                <tbody class=\"item-container\"></tbody>
            </table>
        </div>
    </script>

    <script type=\"text/template\" id=\"step-row-template\">
        <td class=\"step-name workflow-translatable-label\">
            <% if (_is_start) { %>
            <%- label %>
            <% } else { %>
            <a href=\"#\" class=\"edit-step\" title=\"";
        // line 367
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Update this step"), "html", null, true);
        echo "\"><%- label %></a>
            <% if (is_final) { %>&nbsp;<strong title=\"";
        // line 368
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.step.is_final.tooltip"), "html", null, true);
        echo "\">(";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Final"), "html", null, true);
        echo ")</strong><% } %>
                <% if (name && !_is_clone && typeof translateLinks !== 'undefined' && translateLinks.label) { %>
                    ";
        // line 370
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.label %>"], 370, $context, $this->getSourceContext());
        echo "
                <% } %>
            <% } %>
        </td>
        <td class=\"step-transitions\"></td>
        <td><span title=\"";
        // line 375
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.step.order.tooltip"), "html", null, true);
        echo "\"><%- order %></span></td>
        <td class=\"step-actions\">
            <div class=\"btn-group pull-right\">
                <a href=\"#\" class=\"btn btn-icon btn-lighter add-step-transition action\" title=\"";
        // line 378
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add transition to this step"), "html", null, true);
        echo "\"><i class=\"fa-plus-circle hide-text\"></i></a>
                <% if (!_is_start) { %>
                <a href=\"#\" class=\"btn btn-icon btn-lighter edit-step action\" title=\"";
        // line 380
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Update this step"), "html", null, true);
        echo "\"><i class=\"fa-pencil-square-o hide-text\"></i></a>
                <a href=\"#\" class=\"btn btn-icon btn-lighter clone-step action\" title=\"";
        // line 381
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Clone this step"), "html", null, true);
        echo "\"><i class=\"fa-files-o hide-text\"></i></a>
                <a href=\"#\" class=\"btn btn-icon btn-lighter delete-step action\" title=\"";
        // line 382
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Delete this step"), "html", null, true);
        echo "\"><i class=\"fa-trash-o hide-text\"></i></a>
                <% } %>
            </div>
        </td>
    </script>

    <script type=\"text/template\" id=\"transition-row-short-template\">
        <a href=\"#\" class=\"edit-transition\" title=\"";
        // line 389
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Update this transition"), "html", null, true);
        echo "\"><%- label %></a>
            <% if (name && !_is_clone && typeof translateLinks !== 'undefined' && translateLinks.label) { %>
                ";
        // line 391
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.label %>"], 391, $context, $this->getSourceContext());
        echo "
            <% } %>
        <a href=\"#\" class=\"clone-transition action\" title=\"";
        // line 393
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Clone this transition"), "html", null, true);
        echo "\"><i class=\"fa-files-o hide-text\"></i></a>
        <a href=\"#\" class=\"delete-transition action\" title=\"";
        // line 394
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Delete this transition"), "html", null, true);
        echo "\"><i class=\"fa-trash-o hide-text\"></i></a>
        <i class=\"fa-long-arrow-right\"></i>
        <span title=\"";
        // line 396
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.transition.step_to.tooltip"), "html", null, true);
        echo "\"><%- stepToLabel %></span>
    </script>

    <script type=\"text/template\" id=\"attribute-form-option-edit-template\">
        <form action=\"#\">
            <div class=\"form-horizontal\" style=\"width: 436px;\">
                <input type=\"hidden\" name=\"itemId\" value=\"\">
                <div class=\"control-group\">
                    ";
        // line 404
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.entity_field.label"), true, "oro.workflow.workflowdefinition.attribute.property_path.tooltip"], 404, $context, $this->getSourceContext());
        echo "
                    <div class=\"controls\">
                        <input type=\"hidden\" name=\"property_path\" data-validation=\"";
        // line 406
        echo twig_escape_filter($this->env, json_encode(($context["requiredConstraint"] ?? null)), "html", null, true);
        echo "\"/>
                    </div>
                </div>

                <div class=\"control-group\">
                    ";
        // line 411
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.label.label"), false, "oro.workflow.workflowdefinition.attribute.label.tooltip"], 411, $context, $this->getSourceContext());
        echo "
                    <div class=\"controls\">
                        <input type=\"text\" name=\"label\" value=\"<%- label %>\" placeholder=\"";
        // line 413
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Leave empty for system value"), "html", null, true);
        echo "\"/>
                    </div>
                </div>

                <div class=\"control-group control-group-checkbox\">
                    ";
        // line 418
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.form.required.label"), false, "oro.workflow.workflowdefinition.attribute.required.tooltip"], 418, $context, $this->getSourceContext());
        echo "
                    <div class=\"controls\">
                        <input type=\"checkbox\" name=\"required\" <% if (required) { %>checked=\"checked\"<% } %>/>
                    </div>
                </div>

                <div class=\"clearfix\">
                    <div class=\"pull-right\">
                        <button type=\"reset\" class=\"btn hide\"><i class=\"fa-undo\"></i> ";
        // line 426
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Reset"), "html", null, true);
        echo "</button>
                        <button type=\"submit\" class=\"btn btn-success\"><i class=\"fa-plus\"></i> ";
        // line 427
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add"), "html", null, true);
        echo "</button>
                    </div>
                </div>
            </div>
        </form>
    </script>

    <script type=\"text/template\" id=\"attribute-form-option-list-template\">
        <div class=\"grid-container form-options-list\">
            <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\" style=\"margin-bottom: 10px\">
                <thead>
                    <tr>
                        <th><span>";
        // line 439
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Entity field"), "html", null, true);
        echo "</span></th>
                        <th><span>";
        // line 440
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Label"), "html", null, true);
        echo "</span></th>
                        <th><span>";
        // line 441
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Required"), "html", null, true);
        echo "</span></th>
                        <th class=\"action-column\"><span>";
        // line 442
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Actions"), "html", null, true);
        echo "</span></th>
                    </tr>
                </thead>
                <tbody class=\"item-container\"></tbody>
            </table>
        </div>
    </script>

    <script type=\"text/template\" id=\"attribute-form-option-row-template\">
        <td>
            <%= entityField %>
            <% if (!is_entity_attribute) { %>
            <span class=\"muted\">(";
        // line 454
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("System"), "html", null, true);
        echo ")</span>
            <% } %>
        </td>
        <td>
            <%- label %>
            <% if (isSystemLabel) { %><span class=\"muted\">(";
        // line 459
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("System"), "html", null, true);
        echo ")</span><% } %>
            <% if (typeof translateLinks !== 'undefined' && translateLinks.label) { %>
                ";
        // line 461
        echo twig_call_macro($macros["workflowMacros"], "macro_renderGoToTranslationsIconByLink", ["<%- translateLinks.label %>"], 461, $context, $this->getSourceContext());
        echo "
            <% } %>
        </td>
        <td>
            <% if (required) { %>
                ";
        // line 466
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes"), "html", null, true);
        echo "
            <% } else { %>
                ";
        // line 468
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No"), "html", null, true);
        echo "
            <% } %>
        </td>
        <td class=\"step-actions\">
            <div class=\"pull-right\">
                <% if (is_entity_attribute) { %>
                <a href=\"#\" class=\"edit-form-option action\" title=\"";
        // line 474
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Update field"), "html", null, true);
        echo "\"><i class=\"fa-pencil-square-o hide-text\"></i></a>
                <% } %>
                <a href=\"#\" class=\"delete-form-option action\" title=\"";
        // line 476
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Delete field"), "html", null, true);
        echo "\"><i class=\"fa-trash-o hide-text\"></i></a>
            </div>
        </td>
    </script>

    <script type=\"text/template\" id=\"transition-list-template\">
        <div class=\"grid-container transition-list\">
            <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\" style=\"margin-bottom: 10px\">
                <thead>
                    <tr>
                        <th><span title=\"";
        // line 486
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.transition.name.tooltip"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Transition name"), "html", null, true);
        echo "</span></th>
                        <th><span title=\"";
        // line 487
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.transition.step_to.tooltip"), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("To step"), "html", null, true);
        echo "</span></th>
                        <th class=\"action-column\"><span>";
        // line 488
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Actions"), "html", null, true);
        echo "</span></th>
                    </tr>
                </thead>
                <tbody class=\"item-container\">
                <tr class=\"no-rows-message\">
                    <td colspan=\"3\">
                        <div style=\"padding: 10px 0;text-align: center\">
                            ";
        // line 495
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("There are no transitions yet."), "html", null, true);
        echo "
                        </div>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
    </script>

    <script type=\"text/template\" id=\"transition-row-template\">
        <td class=\"transition-name\">
            <span title=\"";
        // line 506
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.transition.name.tooltip"), "html", null, true);
        echo "\"><%- label %></span>
        </td>
        <td><span title=\"";
        // line 508
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.transition.step_to.tooltip"), "html", null, true);
        echo "\"><%- stepToLabel %></span></td>
        <td class=\"transition-actions\">
            <div class=\"pull-right\">
                <a href=\"#\" class=\"delete-transition action\" title=\"";
        // line 511
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Delete this transition"), "html", null, true);
        echo "\"><i class=\"fa-trash-o hide-text\"></i></a>
            </div>
        </td>
    </script>

    ";
        // line 516
        ob_start(function () { return ''; });
        // line 517
        echo "        ";
        $context["startStep"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startStep", [], "any", false, false, false, 517)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "startStep", [], "any", false, false, false, 517), "name", [], "any", false, false, false, 517)) : (""));
        // line 518
        echo "        <div class=\"control-group\">
            ";
        // line 519
        echo twig_call_macro($macros["workflowDefinitionUpdate"], "macro_render_label", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.start_step.label"), false, "oro.workflow.workflowdefinition.start_step.tooltip"], 519, $context, $this->getSourceContext());
        echo "
            <div class=\"controls\">
                <input type=\"hidden\" name=\"start_step\" value=\"";
        // line 521
        echo twig_escape_filter($this->env, ($context["startStep"] ?? null), "html", null, true);
        echo "\"/>
            </div>
        </div>
    ";
        $context["startStepSelector"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 525
        echo "
    ";
        // line 526
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["data" => [0 =>         // line 531
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 531), 'row'), 1 =>         // line 532
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "related_entity", [], "any", false, false, false, 532), 'row'), 2 =>         // line 533
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "exclusive_active_groups", [], "any", false, false, false, 533), 'row'), 3 =>         // line 534
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "exclusive_record_groups", [], "any", false, false, false, 534), 'row'), 4 =>         // line 535
($context["startStepSelector"] ?? null), 5 =>         // line 536
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "steps_display_ordered", [], "any", false, false, false, 536), 'row')]]]]];
        // line 541
        echo "
    ";
        // line 542
        ob_start(function () { return ''; });
        // line 543
        echo "    <div class=\"workflow-definition-buttons\">
        <div class=\"pull-right\">
            <button type=\"button\" class=\"btn btn-primary add-transition-btn\">";
        // line 545
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add transition"), "html", null, true);
        echo "</button>
            <button type=\"button\" class=\"btn btn-primary add-step-btn\">";
        // line 546
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add step"), "html", null, true);
        echo "</button>
        </div>
    </div>
    <div class=\"row-fluid clearfix\">
        <div class=\"workflow-table-container\">
            <div class=\"workflow-definition-steps-list-container clearfix\"></div>
        </div>
    </div>
    ";
        $context["stepsListWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 555
        echo "
    ";
        // line 556
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.title.steps_and_transitions"), "subblocks" => [0 => ["data" => [0 =>         // line 561
($context["stepsListWidget"] ?? null)]]]]]);
        // line 566
        echo "
    ";
        // line 567
        if ( !$this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            // line 568
            echo "        ";
            ob_start(function () { return ''; });
            // line 569
            echo "            <div class=\"workflow-definition-buttons\">
                <div class=\"workflow-history-container pull-left\"></div>
                <div class=\"pull-left\">
                    <button type=\"button\" class=\"btn btn-secondary refresh-btn\">
                        <span class=\"fa-refresh\" aria-hidden=\"true\"></span>
                        ";
            // line 574
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Auto sort"), "html", null, true);
            echo "
                    </button>
                </div>
                <div class=\"pull-right\">
                    <button type=\"button\" class=\"btn btn-primary add-transition-btn\">";
            // line 578
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add transition"), "html", null, true);
            echo "</button>
                    <button type=\"button\" class=\"btn btn-primary add-step-btn\">";
            // line 579
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add step"), "html", null, true);
            echo "</button>
                </div>
            </div>
            <div class=\"workflow-step-editor\" ";
            // line 582
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroworkflow/js/app/views/flowchart/flowchart-container-view", "name" => "flowchart-container"]], 582, $context, $this->getSourceContext());
            // line 585
            echo "></div>
        ";
            $context["workflowDiagram"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 587
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.block.title.workflow_diagrams"), "subblocks" => [0 => ["data" => [0 =>             // line 592
($context["workflowDiagram"] ?? null)]]]]]);
            // line 597
            echo "    ";
        }
        // line 598
        echo "
    ";
        // line 599
        $context["id"] = "workflow-designer";
        // line 600
        echo "    ";
        $context["data"] = ["dataBlocks" =>         // line 601
($context["dataBlocks"] ?? null)];
        // line 603
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    // line 31
    public function macro_render_label($__label__ = null, $__required__ = null, $__tooltip__ = null, $__tooltip_placement__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "label" => $__label__,
            "required" => $__required__,
            "tooltip" => $__tooltip__,
            "tooltip_placement" => $__tooltip_placement__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 32
            echo "    <label class=\"control-label ";
            if (($context["required"] ?? null)) {
                echo "required";
            }
            echo "\">
        ";
            // line 33
            if ((array_key_exists("tooltip", $context) && ($context["tooltip"] ?? null))) {
                // line 34
                echo "            ";
                $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/update.html.twig", 34)->unwrap();
                // line 35
                echo "            ";
                echo twig_call_macro($macros["ui"], "macro_tooltip", [($context["tooltip"] ?? null), [], ($context["tooltip_placement"] ?? null)], 35, $context, $this->getSourceContext());
                echo "
        ";
            }
            // line 37
            echo "
        ";
            // line 38
            echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
            echo " ";
            if (($context["required"] ?? null)) {
                echo "<em>*</em>";
            }
            // line 39
            echo "    </label>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/WorkflowDefinition/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1049 => 39,  1043 => 38,  1040 => 37,  1034 => 35,  1031 => 34,  1029 => 33,  1022 => 32,  1006 => 31,  999 => 603,  997 => 601,  995 => 600,  993 => 599,  990 => 598,  987 => 597,  985 => 592,  983 => 587,  979 => 585,  977 => 582,  971 => 579,  967 => 578,  960 => 574,  953 => 569,  950 => 568,  948 => 567,  945 => 566,  943 => 561,  942 => 556,  939 => 555,  927 => 546,  923 => 545,  919 => 543,  917 => 542,  914 => 541,  912 => 536,  911 => 535,  910 => 534,  909 => 533,  908 => 532,  907 => 531,  906 => 526,  903 => 525,  896 => 521,  891 => 519,  888 => 518,  885 => 517,  883 => 516,  875 => 511,  869 => 508,  864 => 506,  850 => 495,  840 => 488,  834 => 487,  828 => 486,  815 => 476,  810 => 474,  801 => 468,  796 => 466,  788 => 461,  783 => 459,  775 => 454,  760 => 442,  756 => 441,  752 => 440,  748 => 439,  733 => 427,  729 => 426,  718 => 418,  710 => 413,  705 => 411,  697 => 406,  692 => 404,  681 => 396,  676 => 394,  672 => 393,  667 => 391,  662 => 389,  652 => 382,  648 => 381,  644 => 380,  639 => 378,  633 => 375,  625 => 370,  618 => 368,  614 => 367,  598 => 354,  592 => 353,  588 => 352,  584 => 351,  555 => 325,  551 => 324,  541 => 317,  533 => 312,  528 => 310,  519 => 304,  514 => 302,  509 => 300,  496 => 290,  491 => 288,  485 => 284,  483 => 275,  461 => 256,  457 => 255,  447 => 248,  431 => 235,  423 => 230,  417 => 227,  409 => 222,  402 => 218,  394 => 213,  387 => 209,  378 => 203,  371 => 199,  364 => 194,  351 => 192,  347 => 191,  341 => 188,  332 => 182,  328 => 181,  324 => 180,  319 => 178,  301 => 163,  296 => 161,  276 => 144,  271 => 142,  259 => 133,  254 => 131,  249 => 129,  236 => 119,  232 => 118,  222 => 111,  218 => 109,  216 => 106,  211 => 104,  208 => 103,  205 => 102,  202 => 101,  198 => 100,  190 => 97,  183 => 96,  179 => 95,  174 => 92,  171 => 91,  168 => 90,  162 => 88,  160 => 86,  159 => 84,  158 => 83,  156 => 82,  153 => 81,  149 => 80,  142 => 77,  140 => 76,  134 => 74,  129 => 72,  126 => 71,  124 => 69,  123 => 66,  122 => 65,  120 => 64,  118 => 63,  115 => 62,  112 => 61,  108 => 60,  101 => 54,  94 => 53,  88 => 51,  84 => 50,  79 => 1,  77 => 58,  74 => 47,  71 => 45,  69 => 44,  67 => 42,  64 => 22,  63 => 20,  62 => 19,  61 => 18,  60 => 17,  59 => 16,  58 => 15,  57 => 14,  56 => 13,  55 => 12,  54 => 11,  53 => 10,  52 => 9,  51 => 5,  49 => 3,  47 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/WorkflowDefinition/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/WorkflowDefinition/update.html.twig");
    }
}
