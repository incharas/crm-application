<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables/select2-variables.scss */
class __TwigTemplate_94b462e9902a7ce2d9d8684853bf3104 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$mobile-select2-container-font-size: 16px !default;
\$mobile-select2-container-height: 40px !default;

\$mobile-select2-choice-height: 36px !default;

\$mobile-select2-chosen-height: 36px !default;
\$mobile-select2-chosen-end-offset: 26px !default;
\$mobile-select2-chosen-inner-offset: 11px \$mobile-select2-chosen-end-offset 7px 10px !default;

\$mobile-select2-arrow-font-size: 18px !default;
\$mobile-select2-arrow-top: 10px !default;

\$mobile-select2-append-height: 40px !default;

\$mobile-select2-allowclear-right: 48px !default;

\$mobile-select2-search-choice-close-height: 36px !default;
\$mobile-select2-search-choice-close-width: 38px !default;
\$mobile-select2-search-choice-close-top: 0 !default;
\$mobile-select2-search-choice-close-right: 0 !default;
\$mobile-select2-search-choice-close-color: \$primary-200 !default;
\$mobile-select2-search-choice-close-text-align: center !default;

\$mobile-select2-search-choice-close-icon-content: \$fa-var-times !default;
\$mobile-select2-search-choice-close-icon-size: 16px !default;
\$mobile-select2-search-choice-close-icon-top: 50% !default;
\$mobile-select2-search-choice-close-icon-right: 50% !default;
\$mobile-select2-search-choice-close-icon-position: absolute !default;
\$mobile-select2-search-choice-close-icon-transform: translate(50%, -50%) !default;

\$mobile-select2-container-multi-top-min-height: 40px !default;
\$mobile-select2-container-multi-top-height: auto !default;

\$mobile-select2-search-multi-choices-inner-offset: 4px 0 0 4px !default;
\$mobile-select2-search-multi-placeholder-height: 28px !default;
\$mobile-select2-search-choice-select2-locked-inner-offset: null !default;

\$mobile-select2-container-multi-choice-offset-right: 5px !default;

\$mobile-select2-container-multi-select2-search-field-height: 36px !default;
\$mobile-select2-container-multi-select2-search-field-top-offset: -4px !default;

\$mobile-select2-results-after-search-inner-offset: 0 8px 8px !default;
\$mobile-select2-results-inner-offset: 8px !default;

\$mobile-select2-result-label-font-size: \$mobile-select2-container-font-size !default;
\$mobile-select2-result-label-line-height: 1.5 !default;
\$mobile-select2-result-label-inner-offset: 4px 8px 5px !default;

\$mobile-select2-drop-max-width: calc(100% - #{2 * \$content-padding}) !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables/select2-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables/select2-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables/select2-variables.scss");
    }
}
