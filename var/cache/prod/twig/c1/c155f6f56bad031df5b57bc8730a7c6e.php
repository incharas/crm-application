<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/jstree.html.twig */
class __TwigTemplate_6005b2c634aee2ff4b7bfd6136b6b803 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'header' => [$this, 'block_header'],
            'label' => [$this, 'block_label'],
            'actions' => [$this, 'block_actions'],
            'content' => [$this, 'block_content'],
            'search' => [$this, 'block_search'],
            'tree' => [$this, 'block_tree'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["data"] = twig_array_merge(($context["data"] ?? null), ["disableActions" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 2
($context["data"] ?? null), "disableActions", [], "any", true, true, false, 2)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "disableActions", [], "any", false, false, false, 2), false)) : (false)), "disableSearch" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["data"] ?? null), "disableSearch", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "disableSearch", [], "any", false, false, false, 3), false)) : (false)), "treeOptions" => twig_array_merge(["view" => "oroui/js/app/views/jstree/base-tree-view"], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["data"] ?? null), "treeOptions", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "treeOptions", [], "any", false, false, false, 6), [])) : ([]))), "actionsOptions" => twig_array_merge(["view" => "oroui/js/app/views/jstree/action-manager-view", "actions" => ((        // line 9
array_key_exists("actions", $context)) ? (_twig_default_filter(($context["actions"] ?? null), [])) : ([]))], ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 10
($context["data"] ?? null), "actionsOptions", [], "any", true, true, false, 10)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "actionsOptions", [], "any", false, false, false, 10), [])) : ([])))]);
        // line 12
        echo "
<div class=\"jstree-wrapper\" data-role=\"jstree-wrapper\">
    <div class=\"jstree-wrapper__inner\" data-page-component-view=\"";
        // line 14
        echo twig_escape_filter($this->env, json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "treeOptions", [], "any", false, false, false, 14)), "html", null, true);
        echo "\">
        <div class=\"jstree-wrapper__title\">
            ";
        // line 16
        $this->displayBlock('header', $context, $blocks);
        // line 35
        echo "        </div>
        <div class=\"jstree-wrapper__content\">
            ";
        // line 37
        $this->displayBlock('content', $context, $blocks);
        // line 55
        echo "        </div>
    </div>
</div>
";
    }

    // line 16
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "                ";
        $this->displayBlock('label', $context, $blocks);
        // line 27
        echo "                ";
        $this->displayBlock('actions', $context, $blocks);
        // line 34
        echo "            ";
    }

    // line 17
    public function block_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "                    ";
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "label", [], "any", true, true, false, 18)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "label", [], "any", false, false, false, 18), "")) : (""))) {
            // line 19
            echo "                        <span class=\"jstree-wrapper__label\">
                            <span class=\"jstree-wrapper__text\"
                                data-collapse-trigger>
                                ";
            // line 22
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "label", [], "any", false, false, false, 22), "html", null, true);
            echo "
                            </span>
                        </span>
                    ";
        }
        // line 26
        echo "                ";
    }

    // line 27
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "                    ";
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "disableActions", [], "any", false, false, false, 28)) {
            // line 29
            echo "                    <div class=\"jstree-actions dropdown btn-group\"
                        data-page-component-view=\"";
            // line 30
            echo twig_escape_filter($this->env, json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "actionsOptions", [], "any", false, false, false, 30)), "html", null, true);
            echo "\">
                    </div>
                    ";
        }
        // line 33
        echo "                ";
    }

    // line 37
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "                ";
        $this->displayBlock('search', $context, $blocks);
        // line 51
        echo "                ";
        $this->displayBlock('tree', $context, $blocks);
        // line 54
        echo "            ";
    }

    // line 38
    public function block_search($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "                    ";
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "disableSearch", [], "any", false, false, false, 39)) {
            // line 40
            echo "                    <div class=\"jstree-search-component\" data-name=\"jstree-search-component\">
                        <input type=\"search\" placeholder=\"";
            // line 41
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.jstree.quick_search"), "html", null, true);
            echo "\" class=\"jstree-search-component__input\" data-name=\"search\"/>
                        <span data-name=\"clear-search\" class=\"jstree-search-component__clear-icon\">
                            <i class=\"fa fa-close\"></i>
                        </span>
                        <span class=\"jstree-search-component__search-icon\">
                            <i class=\"fa fa-search\"></i>
                        </span>
                    </div>
                    ";
        }
        // line 50
        echo "                ";
    }

    // line 51
    public function block_tree($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "                    <div data-role=\"jstree-container\" class=\"jstree-container\"></div>
                ";
    }

    public function getTemplateName()
    {
        return "@OroUI/jstree.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  175 => 52,  171 => 51,  167 => 50,  155 => 41,  152 => 40,  149 => 39,  145 => 38,  141 => 54,  138 => 51,  135 => 38,  131 => 37,  127 => 33,  121 => 30,  118 => 29,  115 => 28,  111 => 27,  107 => 26,  100 => 22,  95 => 19,  92 => 18,  88 => 17,  84 => 34,  81 => 27,  78 => 17,  74 => 16,  67 => 55,  65 => 37,  61 => 35,  59 => 16,  54 => 14,  50 => 12,  48 => 10,  47 => 9,  46 => 6,  45 => 3,  44 => 2,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/jstree.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/jstree.html.twig");
    }
}
