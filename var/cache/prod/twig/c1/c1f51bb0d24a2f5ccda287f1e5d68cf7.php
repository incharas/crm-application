<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/select-create-inline-type-async-view.js */
class __TwigTemplate_4beebed2ae1da469181c0f6fddde73c6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const SelectCreateInlineTypeView = require('oroform/js/app/views/select-create-inline-type-view');

    const SelectCreateInlineTypeAsyncView = SelectCreateInlineTypeView.extend({
        events: {
            'select2-data-request .select2': 'onSelect2Request',
            'select2-data-loaded .select2': 'onSelect2Loaded'
        },

        /**
         * @inheritdoc
         */
        constructor: function SelectCreateInlineTypeAsyncView(options) {
            SelectCreateInlineTypeAsyncView.__super__.constructor.call(this, options);
        },

        onGridRowSelect: function(data) {
            SelectCreateInlineTypeAsyncView.__super__.onGridRowSelect.call(this, data);
            this.dialogWidget.hide();
        },

        onCreate: function(e) {
            SelectCreateInlineTypeAsyncView.__super__.onCreate.call(this, e);
            this.dialogWidget.once('beforeContentLoad', () => {
                this.dialogWidget.hide();
                this.\$el.addClass('loading');
            });
        },

        onSelect2Request: function() {
            if (this.dialogWidget) {
                this.\$el.addClass('loading');
            }
        },

        onSelect2Loaded: function() {
            this.\$el.removeClass('loading');
        }
    });

    return SelectCreateInlineTypeAsyncView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/select-create-inline-type-async-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/select-create-inline-type-async-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/select-create-inline-type-async-view.js");
    }
}
