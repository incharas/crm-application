<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Call/activityButton.html.twig */
class __TwigTemplate_3906529fa37f009b15c2ab38cdcbf6f0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'action_controll' => [$this, 'block_action_controll'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroCall/Call/activityLink.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroCall/Call/activityLink.html.twig", "@OroCall/Call/activityButton.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_action_controll($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCall/Call/activityButton.html.twig", 4)->unwrap();
        // line 5
        echo "
    ";
        // line 6
        $context["options"] = twig_array_merge(($context["options"] ?? null), ["aCss" => "no-hash"]);
        // line 9
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_clientButton", [($context["options"] ?? null)], 9, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCall/Call/activityButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  58 => 9,  56 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Call/activityButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Call/activityButton.html.twig");
    }
}
