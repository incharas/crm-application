<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Reply/parentBody.html.twig */
class __TwigTemplate_c69e962fe516656570c2661d5449f649 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<body> ";
        // line 2
        echo "    <br><br>
    <div class=\"quote\">
        <p>
            ";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.parent_message_header", ["%date%" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["email"] ?? null), "sentAt", [], "any", false, false, false, 6)), "%user%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["email"] ?? null), "fromName", [], "any", false, false, false, 7)]), "html", null, true);
        // line 8
        echo "
        </p>
        <div class=\"email-prev-body\">
            ";
        // line 11
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 11)) {
            // line 12
            echo "                ";
            echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "emailBody", [], "any", false, false, false, 12), "bodyContent", [], "any", false, false, false, 12);
            echo "
            ";
        }
        // line 14
        echo "        </div>
    </div>
</body>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Reply/parentBody.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 14,  55 => 12,  53 => 11,  48 => 8,  46 => 7,  45 => 6,  44 => 5,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Reply/parentBody.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Reply/parentBody.html.twig");
    }
}
