<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/content-processor/pinned-dropdown-button.js */
class __TwigTemplate_e0991ed530617ff311845f4cc13e4589 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(['./dropdown-button', 'oroui/js/mediator', 'oroui/js/persistent-storage'
], function(\$, mediator, persistentStorage) {
    'use strict';

    \$.widget('oroui.pinnedDropdownButtonProcessor', \$.oroui.dropdownButtonProcessor, {
        options: {
            mainButtons: '',
            minItemQuantity: 0,
            groupKey: ''
        },

        keyPreffix: 'pinned-dropdown-button-processor-',

        _create: function() {
            this._super();
            this._on({
                'click [data-button-index]': this._onButtonClick
            });
        },

        /**
         * Fetches buttons and creates index for them
         *
         * @param {jQuery|null} \$element
         * @returns {*}
         * @private
         */
        _collectButtons: function(\$element) {
            const \$buttons = this._super(\$element);
            \$buttons.filter(':not(.divider)').each(function(i) {
                \$(this).attr('data-button-index', '').data('button-index', i);
            });
            return \$buttons;
        },

        /**
         * Defines main buttons
         *
         * @param {jQuery} \$buttons
         * @returns {jQuery}
         * @private
         */
        _mainButtons: function(\$buttons) {
            const key = this._getStorageKey();
            const index = key ? persistentStorage.getItem(key) || 0 : 0;
            const result = \$buttons.get(index);

            return result ? \$(result) : this._super(\$buttons);
        },

        /**
         * Stores index of used button
         *
         * @param e
         * @private
         */
        _onButtonClick: function(e) {
            const key = this._getStorageKey();
            if (key) {
                persistentStorage.setItem(key, \$(e.target).data('button-index') || 0);
            }
            mediator.trigger('dropdown-button:click');
        },

        /**
         * Defines storage key
         *
         * @returns {string}
         * @private
         */
        _getStorageKey: function() {
            return this.options.groupKey ? this.keyPreffix + this.options.groupKey : '';
        }
    });

    return \$;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/content-processor/pinned-dropdown-button.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/content-processor/pinned-dropdown-button.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/content-processor/pinned-dropdown-button.js");
    }
}
