<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/templates/editor/multi-checkbox-editor.html */
class __TwigTemplate_e3d442e5e5f4ae1d580a29cd206d349c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"inline-editor__inner\">
    <div class=\"inline-editor__fields\">
        <select multiple>
            <% _.each(options, function (option) { %>
            <option value=\"<%- option.id %>\" title=\"<%- option.text %>\"
            <% if (_.indexOf(value, option.id) !== -1) { %> selected=\"selected\"<% } %>>
            <%- option.text %>
            </option>
            <% }); %>
        </select>
    </div>
    <div class=\"inline-editor__actions inline-editor__actions--over-multiselect\">
        <button class=\"btn btn-icon btn-square-light inline-editor__action-item\" type=\"submit\" data-role=\"apply\"
                title=\"<%- _.__('oro.form.inlineEditing.action.save') %>\">
            <span class=\"fa-check\" aria-hidden=\"true\"></span>
        </button>
        <button class=\"btn btn-icon btn-square-light inline-editor__action-item\" data-action=\"cancel\"
                title=\"<%- _.__('oro.form.inlineEditing.action.cancel') %>\">
            <span class=\"fa-ban\" aria-hidden=\"true\"></span>
        </button>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/templates/editor/multi-checkbox-editor.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/templates/editor/multi-checkbox-editor.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/templates/editor/multi-checkbox-editor.html");
    }
}
