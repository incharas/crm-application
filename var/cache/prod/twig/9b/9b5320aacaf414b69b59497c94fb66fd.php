<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/jstree-action-manager.js */
class __TwigTemplate_34f2c460655823050c2f884d2082aeba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const error = require('oroui/js/error');

    /**
     * Actions manager, can stored action views, and share for all jstree
     *
     * @type {Object}
     */
    const ActionManager = {
        /**
         * Actions stack
         *
         * @property {Object}
         */
        actions: {},

        /**
         * Register actions
         *
         * @param {String} name
         * @param {Object} action
         */
        addAction: function(name, action) {
            if (!_.isObject(action) || !action.hasOwnProperty('view')) {
                return error.showErrorInConsole('\"view\" and \"name\" property is required, please define');
            }

            if (!_.isFunction(action.view)) {
                return error.showErrorInConsole('\"view\" should be constructor function');
            }

            _.defaults(action, {
                name: name,
                isAvailable: function() {
                    return true;
                }
            });

            this.actions[action.name] = action;
        },

        /**
         *
         * Get actions for current jstree
         *
         * @param {Object} options
         * @return {Array}
         */
        getActions: function(options) {
            return _.filter(this.actions, function(action) {
                return action.isAvailable(options);
            }, this);
        }
    };

    return ActionManager;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/jstree-action-manager.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/jstree-action-manager.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/jstree-action-manager.js");
    }
}
