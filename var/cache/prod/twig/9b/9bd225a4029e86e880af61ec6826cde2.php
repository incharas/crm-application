<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Twig/image.html.twig */
class __TwigTemplate_fed751baf3afc9a348493d26372b1931 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["title"] = $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileTitle(($context["file"] ?? null));
        // line 2
        echo "<a href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileUrl(($context["file"] ?? null)), "html", null, true);
        echo "\" class=\"no-hash\" title=\"";
        echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
        echo "\">
    ";
        // line 3
        $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroAttachment/Twig/image.html.twig", 3)->display(twig_array_merge($context, ["sources" => $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getResizedPictureSources(        // line 4
($context["file"] ?? null), ($context["width"] ?? null), ($context["height"] ?? null)), "img_attrs" => ["alt" =>         // line 5
($context["title"] ?? null), "width" => ($context["width"] ?? null), "height" => ($context["height"] ?? null)]]));
        // line 7
        echo "</a>
";
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Twig/image.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 7,  48 => 5,  47 => 4,  46 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Twig/image.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Twig/image.html.twig");
    }
}
