<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Shortcut/shortcuts.html.twig */
class __TwigTemplate_aea0fcdac5b68c51d47d1ea163092727 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo $this->extensions['Oro\Bundle\NavigationBundle\Twig\MenuExtension']->render("shortcuts", ["source" => "oro_api_get_shortcuts", "details" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_shortcut_actionslist")]);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Shortcut/shortcuts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Shortcut/shortcuts.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Shortcut/shortcuts.html.twig");
    }
}
