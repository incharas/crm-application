<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroScope/Form/fields.html.twig */
class __TwigTemplate_5ba2dac8bead1d2e747862e8c3675dd5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_scope_widget' => [$this, 'block_oro_scope_widget'],
            'oro_scope_collection_widget' => [$this, 'block_oro_scope_collection_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_scope_widget', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('oro_scope_collection_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_scope_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <div class=\"form-horizontal\">
        <div class=\"control-group-container\">
            ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 4));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 5
            echo "                <div class=\"control-group\">
                    ";
            // line 6
            if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 6), "label", [], "any", false, false, false, 6))) {
                // line 7
                echo "                        <div class=\"control-label wrap\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'label');
                echo "</div>
                    ";
            }
            // line 9
            echo "                    <div class=\"controls\">";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'widget');
            echo "</div>
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "        </div>
    </div>
    <div>
        ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
    </div>
";
    }

    // line 19
    public function block_oro_scope_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "    <div class=\"scope-collection\">
        ";
        // line 21
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 21)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 21) . " ")) : ("")) . "oro-options-collection")]);
        // line 22
        echo "        ";
        $this->displayBlock("oro_collection_widget", $context, $blocks);
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroScope/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  102 => 22,  100 => 21,  97 => 20,  93 => 19,  86 => 15,  81 => 12,  71 => 9,  65 => 7,  63 => 6,  60 => 5,  56 => 4,  52 => 2,  48 => 1,  44 => 19,  41 => 18,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroScope/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ScopeBundle/Resources/views/Form/fields.html.twig");
    }
}
