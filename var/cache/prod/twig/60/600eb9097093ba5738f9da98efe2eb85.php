<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroLocale/Form/fields.html.twig */
class __TwigTemplate_e998be40ce157c69b0208d451c456655 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_quarter_widget' => [$this, 'block_oro_quarter_widget'],
            'oro_locale_fallback_value_widget' => [$this, 'block_oro_locale_fallback_value_widget'],
            'oro_locale_fallback_value_tabs_widget' => [$this, 'block_oro_locale_fallback_value_tabs_widget'],
            'oro_locale_localized_property_widget' => [$this, 'block_oro_locale_localized_property_widget'],
            'oro_locale_localized_property_tabs_widget' => [$this, 'block_oro_locale_localized_property_tabs_widget'],
            'oro_locale_localized_fallback_value_collection_widget' => [$this, 'block_oro_locale_localized_fallback_value_collection_widget'],
            'oro_locale_localized_fallback_value_collection_tabs_widget' => [$this, 'block_oro_locale_localized_fallback_value_collection_tabs_widget'],
            '_localization_oro_locale___default_localization_widget' => [$this, 'block__localization_oro_locale___default_localization_widget'],
            '_localization_oro_locale___enabled_localizations_widget' => [$this, 'block__localization_oro_locale___enabled_localizations_widget'],
            '_localization_oro_locale___enabled_localizations_use_parent_scope_value_row' => [$this, 'block__localization_oro_locale___enabled_localizations_use_parent_scope_value_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_quarter_widget', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('oro_locale_fallback_value_widget', $context, $blocks);
        // line 43
        echo "
";
        // line 44
        $this->displayBlock('oro_locale_fallback_value_tabs_widget', $context, $blocks);
        // line 56
        echo "
";
        // line 57
        $this->displayBlock('oro_locale_localized_property_widget', $context, $blocks);
        // line 88
        echo "
";
        // line 89
        $this->displayBlock('oro_locale_localized_property_tabs_widget', $context, $blocks);
        // line 125
        echo "
";
        // line 126
        $this->displayBlock('oro_locale_localized_fallback_value_collection_widget', $context, $blocks);
        // line 134
        echo "
";
        // line 135
        $this->displayBlock('oro_locale_localized_fallback_value_collection_tabs_widget', $context, $blocks);
        // line 141
        echo "
";
        // line 142
        $this->displayBlock('_localization_oro_locale___default_localization_widget', $context, $blocks);
        // line 153
        echo "
";
        // line 154
        $this->displayBlock('_localization_oro_locale___enabled_localizations_widget', $context, $blocks);
        // line 165
        echo "
";
        // line 166
        $this->displayBlock('_localization_oro_locale___enabled_localizations_use_parent_scope_value_row', $context, $blocks);
        // line 171
        echo "
";
        // line 189
        echo "
";
    }

    // line 1
    public function block_oro_quarter_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 2)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 2) . " ")) : ("")) . "oro-quarter")]);
        // line 3
        echo "    ";
        $context["options"] = ["disabled" => ($context["disabled"] ?? null)];
        // line 4
        echo "    <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo ">
        ";
        // line 5
        echo twig_replace_filter(($context["date_pattern"] ?? null), ["{{ year }}" => "", "{{ month }}" =>         // line 7
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "month", [], "any", false, false, false, 7), 'widget', ($context["options"] ?? null)), "{{ day }}" =>         // line 8
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "day", [], "any", false, false, false, 8), 'widget', ($context["options"] ?? null))]);
        // line 9
        echo "
    </div>";
    }

    // line 13
    public function block_oro_locale_fallback_value_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 14), "group_fallback_fields", [], "any", false, false, false, 14)) {
            // line 15
            echo "        <div class=\"fallback-item-fallback-line\">
            <div nowrap=\"true\" class=\"fallback-item-use-fallback\">
                ";
            // line 17
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_fallback", [], "any", false, false, false, 17), 'widget');
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_fallback", [], "any", false, false, false, 17), "vars", [], "any", false, false, false, 17), "label", [], "any", false, false, false, 17)), "html", null, true);
            echo "
            </div>
            <div class=\"fallback-item-fallback\">
                ";
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "fallback", [], "any", false, false, false, 20), 'widget', ["attr" => ["class" => "fallback"]]);
            echo "
            </div>
        </div>
    ";
        }
        // line 24
        echo "    <div>
        <div class=\"fallback-item-value";
        // line 25
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 25), "group_fallback_fields", [], "any", false, false, false, 25)) {
            echo " fallback-item-value-top";
        }
        echo "\">
            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 26), 'widget', ["attr" => ["class" => "fallback-item-value-input"]]);
        echo "
        </div>
        ";
        // line 28
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 28), "group_fallback_fields", [], "any", false, false, false, 28)) {
            // line 29
            echo "            <div class=\"fallback-item-fallback-line\">
                <div nowrap=\"true\" class=\"fallback-item-use-fallback\">
                    ";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_fallback", [], "any", false, false, false, 31), 'widget');
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_fallback", [], "any", false, false, false, 31), "vars", [], "any", false, false, false, 31), "label", [], "any", false, false, false, 31)), "html", null, true);
            echo "
                </div>
                <div class=\"fallback-item-fallback ";
            // line 33
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 33), "exclude_parent_localization", [], "any", false, false, false, 33)) {
                echo "hide";
            }
            echo "\">
                    ";
            // line 34
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "fallback", [], "any", false, false, false, 34), 'widget', ["attr" => ["class" => "fallback"]]);
            echo "
                </div>
            </div>
        ";
        }
        // line 38
        echo "    </div>

    ";
        // line 40
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 40), 'errors');
        echo "
    ";
        // line 41
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "fallback", [], "any", false, false, false, 41), 'errors');
        echo "
";
    }

    // line 44
    public function block_oro_locale_fallback_value_tabs_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    <div class=\"fallback-item-fallback\">
        ";
        // line 46
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "fallback", [], "any", false, false, false, 46), 'row', ["attr" => ["class" => "fallback"]]);
        echo "
    </div>
    <div class=\"fallback-item-value\">
        ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 49), 'widget', ["attr" => ["class" => "fallback-item-value-input"]]);
        echo "
    </div>
    <div class=\"fallback-item-use-fallback hide\">
        ";
        // line 52
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "use_fallback", [], "any", false, false, false, 52), 'widget');
        echo "
    </div>
    ";
        // line 54
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 54), 'errors');
        echo "
";
    }

    // line 57
    public function block_oro_locale_localized_property_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "    <div class=\"fallback-container\"
            data-page-component-module=\"oroui/js/app/components/view-component\"
            data-page-component-options=\"";
        // line 60
        echo twig_escape_filter($this->env, json_encode(["view" => "orolocale/js/app/views/fallback-view"]), "html", null, true);
        echo "\"
            data-layout=\"separate\"
            >
        <div class=\"fallback-item\">
            <div class=\"fallback-item-value fallback-item-value--first\">
                <div class=\"input-append\">
                    <div class=\"input-group\">
                        ";
        // line 67
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "default", [], "any", false, false, false, 67), 'widget');
        echo "
                        ";
        // line 68
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "default", [], "any", false, false, false, 68), 'errors');
        echo "
                    </div>
                    <div class=\"btn-group\">
                        <button type=\"button\" class=\"btn btn-icon btn-square-default fallback-status\"></button>
                    </div>
                </div>
            </div>
            <div class=\"fallback-item-label\">";
        // line 75
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "default", [], "any", false, false, false, 75), "vars", [], "any", false, false, false, 75), "label", [], "any", false, false, false, 75)), "html", null, true);
        echo "</div>
        </div>
        ";
        // line 77
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "localizations", [], "any", false, false, false, 77));
        foreach ($context['_seq'] as $context["_key"] => $context["localization"]) {
            // line 78
            echo "            <div class=\"fallback-item\" style=\"display: none;\">
                <div class=\"fallback-item-label ";
            // line 79
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["localization"], "vars", [], "any", false, false, false, 79), "group_fallback_fields", [], "any", false, false, false, 79)) {
                echo "fallback-item-top";
            }
            echo "\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["localization"], "vars", [], "any", false, false, false, 79), "label", [], "any", false, false, false, 79)), "html", null, true);
            echo "</div>
                <div ";
            // line 80
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["localization"], "vars", [], "any", false, false, false, 80), "group_fallback_fields", [], "any", false, false, false, 80)) {
                echo "class=\"fallback-item-top\"";
            }
            echo ">
                    ";
            // line 81
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["localization"], 'widget');
            echo "
                    ";
            // line 82
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["localization"], 'errors');
            echo "
                </div>
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['localization'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 86
        echo "    </div>
";
    }

    // line 89
    public function block_oro_locale_localized_property_tabs_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 90
        echo "    ";
        $macros["fields"] = $this;
        // line 91
        echo "
    ";
        // line 92
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroLocale/Form/fields.html.twig", 92)->unwrap();
        // line 93
        echo "    <div class=\"fallback-container oro-tabs tabbable\"
         data-layout=\"separate\"
         ";
        // line 95
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orolocale/js/app/views/localizable-collection-tabs-view"]], 95, $context, $this->getSourceContext());
        // line 97
        echo "
    >
        ";
        // line 99
        $context["tabId"] = uniqid("fallback-container-");
        // line 100
        echo "
        <div class=\"oro-tabs__head\" ";
        // line 101
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/tabs-component"]], 101, $context, $this->getSourceContext());
        // line 103
        echo ">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                ";
        // line 105
        ob_start(function () { return ''; });
        // line 106
        echo "                    ";
        echo twig_call_macro($macros["fields"], "macro_renderTabNavItem", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "default", [], "any", false, false, false, 106), true, ((($context["tabId"] ?? null) . "-") . 0)], 106, $context, $this->getSourceContext());
        echo "
                    ";
        // line 107
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "localizations", [], "any", false, false, false, 107));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["localization"]) {
            // line 108
            echo "                        ";
            echo twig_call_macro($macros["fields"], "macro_renderTabNavItem", [$context["localization"], false, ((($context["tabId"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 108))], 108, $context, $this->getSourceContext());
            echo "
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['localization'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 110
        echo "                ";
        $___internal_parse_53_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 105
        echo twig_spaceless($___internal_parse_53_);
        // line 111
        echo "            </ul>
        </div>
        <div class=\"oro-tabs__content\">
            <div class=\"tab-content\">
                ";
        // line 115
        ob_start(function () { return ''; });
        // line 116
        echo "                    ";
        echo twig_call_macro($macros["fields"], "macro_renderTab", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "default", [], "any", false, false, false, 116), true, ((($context["tabId"] ?? null) . "-") . 0)], 116, $context, $this->getSourceContext());
        echo "
                    ";
        // line 117
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "localizations", [], "any", false, false, false, 117));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["localization"]) {
            // line 118
            echo "                        ";
            echo twig_call_macro($macros["fields"], "macro_renderTab", [$context["localization"], false, ((($context["tabId"] ?? null) . "-") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 118))], 118, $context, $this->getSourceContext());
            echo "
                    ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['localization'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 120
        echo "                ";
        $___internal_parse_54_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 115
        echo twig_spaceless($___internal_parse_54_);
        // line 121
        echo "            </div>
        </div>
    </div>
";
    }

    // line 126
    public function block_oro_locale_localized_fallback_value_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 127
        echo "    <div class=\"control-group\">
        ";
        // line 128
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "values", [], "any", false, false, false, 128), 'widget');
        echo "
        ";
        // line 129
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "values", [], "any", false, false, false, 129), 'errors');
        echo "
        ";
        // line 130
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "ids", [], "any", false, false, false, 130), 'widget');
        echo "
        ";
        // line 131
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "ids", [], "any", false, false, false, 131), 'errors');
        echo "
    </div>
";
    }

    // line 135
    public function block_oro_locale_localized_fallback_value_collection_tabs_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 136
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "values", [], "any", false, false, false, 136), 'widget');
        echo "
    ";
        // line 137
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "values", [], "any", false, false, false, 137), 'errors');
        echo "
    ";
        // line 138
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "ids", [], "any", false, false, false, 138), 'widget');
        echo "
    ";
        // line 139
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "ids", [], "any", false, false, false, 139), 'errors');
        echo "
";
    }

    // line 142
    public function block__localization_oro_locale___default_localization_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 143
        echo "    <span data-page-component-module=\"oroui/js/app/components/view-component\"
          data-page-component-options=\"";
        // line 144
        echo twig_escape_filter($this->env, json_encode(["view" => "orolocale/js/app/views/localization-select-view", "selectSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 146
($context["form"] ?? null), "value", [], "any", false, false, false, 146), "vars", [], "any", false, false, false, 146), "id", [], "any", false, false, false, 146)), "useParentSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 147
($context["form"] ?? null), "use_parent_scope_value", [], "any", false, false, false, 147), "vars", [], "any", false, false, false, 147), "id", [], "any", false, false, false, 147))]), "html", null, true);
        // line 148
        echo "\"
    >
        ";
        // line 150
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    </span>
";
    }

    // line 154
    public function block__localization_oro_locale___enabled_localizations_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 155
        echo "    <span data-page-component-module=\"oroui/js/app/components/view-component\"
          data-page-component-options=\"";
        // line 156
        echo twig_escape_filter($this->env, json_encode(["view" => "orolocale/js/app/views/localizations-select-view", "selectSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 158
($context["form"] ?? null), "value", [], "any", false, false, false, 158), "vars", [], "any", false, false, false, 158), "id", [], "any", false, false, false, 158)), "useParentSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 159
($context["form"] ?? null), "use_parent_scope_value", [], "any", false, false, false, 159), "vars", [], "any", false, false, false, 159), "id", [], "any", false, false, false, 159))]), "html", null, true);
        // line 160
        echo "\"
    >
        ";
        // line 162
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    </span>
";
    }

    // line 166
    public function block__localization_oro_locale___enabled_localizations_use_parent_scope_value_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 167
        echo "    <span class=\"hide\">
        ";
        // line 168
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row', ["attr" => ($context["attr"] ?? null)]);
        echo "
    </span>
";
    }

    // line 172
    public function macro_renderTabNavItem($__form__ = null, $__isDefault__ = null, $__uniqid__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "isDefault" => $__isDefault__,
            "uniqid" => $__uniqid__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 173
            echo "    <li class=\"nav-item\">
        <a id=\"";
            // line 174
            echo twig_escape_filter($this->env, (($context["uniqid"] ?? null) . "-tab"), "html", null, true);
            echo "\"
           class=\"nav-link ";
            // line 175
            if (($context["isDefault"] ?? null)) {
                echo "active";
            }
            echo "\"
           role=\"tab\"
           href=\"#\"
           aria-controls=\"";
            // line 178
            echo twig_escape_filter($this->env, ($context["uniqid"] ?? null), "html", null, true);
            echo "\"
           aria-selected=\"";
            // line 179
            if (($context["isDefault"] ?? null)) {
                echo "true";
            } else {
                echo "false";
            }
            echo "\"
           data-role=\"change-localization\"
           data-target=\".localization-fields-";
            // line 181
            echo twig_escape_filter($this->env, ($context["uniqid"] ?? null), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 181), "name", [], "any", false, false, false, 181), "html", null, true);
            echo "\"
           data-toggle=\"tab\"
           data-related=\"";
            // line 183
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 183), "name", [], "any", false, false, false, 183), "html", null, true);
            echo "\"
        >
            ";
            // line 185
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 185), "label", [], "any", false, false, false, 185)), "html", null, true);
            echo "
        </a>
    </li>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 190
    public function macro_renderTab($__form__ = null, $__isDefault__ = null, $__uniqid__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "isDefault" => $__isDefault__,
            "uniqid" => $__uniqid__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 191
            echo "    <div id=\"";
            echo twig_escape_filter($this->env, ($context["uniqid"] ?? null), "html", null, true);
            echo "\"
         class=\"fallback-item localization-fields-";
            // line 192
            echo twig_escape_filter($this->env, ($context["uniqid"] ?? null), "html", null, true);
            echo "-";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 192), "name", [], "any", false, false, false, 192), "html", null, true);
            echo " tab-pane";
            if (($context["isDefault"] ?? null)) {
                echo " active";
            }
            echo "\"
         role=\"tabpanel\"
         aria-labelledby=\"";
            // line 194
            echo twig_escape_filter($this->env, (($context["uniqid"] ?? null) . "-tab"), "html", null, true);
            echo "\"
    >
        ";
            // line 196
            if (($context["isDefault"] ?? null)) {
                echo "<div class=\"fallback-item-value\">";
            }
            // line 197
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
            echo "
        ";
            // line 198
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
        ";
            // line 199
            if (($context["isDefault"] ?? null)) {
                echo "</div>";
            }
            // line 200
            echo "    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroLocale/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  676 => 200,  672 => 199,  668 => 198,  663 => 197,  659 => 196,  654 => 194,  643 => 192,  638 => 191,  623 => 190,  610 => 185,  605 => 183,  598 => 181,  589 => 179,  585 => 178,  577 => 175,  573 => 174,  570 => 173,  555 => 172,  548 => 168,  545 => 167,  541 => 166,  534 => 162,  530 => 160,  528 => 159,  527 => 158,  526 => 156,  523 => 155,  519 => 154,  512 => 150,  508 => 148,  506 => 147,  505 => 146,  504 => 144,  501 => 143,  497 => 142,  491 => 139,  487 => 138,  483 => 137,  478 => 136,  474 => 135,  467 => 131,  463 => 130,  459 => 129,  455 => 128,  452 => 127,  448 => 126,  441 => 121,  439 => 115,  436 => 120,  419 => 118,  402 => 117,  397 => 116,  395 => 115,  389 => 111,  387 => 105,  384 => 110,  367 => 108,  350 => 107,  345 => 106,  343 => 105,  339 => 103,  337 => 101,  334 => 100,  332 => 99,  328 => 97,  326 => 95,  322 => 93,  320 => 92,  317 => 91,  314 => 90,  310 => 89,  305 => 86,  295 => 82,  291 => 81,  285 => 80,  277 => 79,  274 => 78,  270 => 77,  265 => 75,  255 => 68,  251 => 67,  241 => 60,  237 => 58,  233 => 57,  227 => 54,  222 => 52,  216 => 49,  210 => 46,  207 => 45,  203 => 44,  197 => 41,  193 => 40,  189 => 38,  182 => 34,  176 => 33,  170 => 31,  166 => 29,  164 => 28,  159 => 26,  153 => 25,  150 => 24,  143 => 20,  136 => 17,  132 => 15,  129 => 14,  125 => 13,  120 => 9,  118 => 8,  117 => 7,  116 => 5,  111 => 4,  108 => 3,  106 => 2,  102 => 1,  97 => 189,  94 => 171,  92 => 166,  89 => 165,  87 => 154,  84 => 153,  82 => 142,  79 => 141,  77 => 135,  74 => 134,  72 => 126,  69 => 125,  67 => 89,  64 => 88,  62 => 57,  59 => 56,  57 => 44,  54 => 43,  52 => 13,  49 => 12,  47 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroLocale/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/LocaleBundle/Resources/views/Form/fields.html.twig");
    }
}
