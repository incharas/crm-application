<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReport/Report/viewFromGrid.html.twig */
class __TwigTemplate_ac1b52346f9d03c0508e1296321e9560 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroReport/Report/viewFromGrid.html.twig", 2)->unwrap();
        // line 3
        $context["gridName"] = ($context["gridName"] ?? null);
        // line 4
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["pageTitle"] ?? null));

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["titleTemplate" => "%title%", "params" => ["%title%" => twig_capitalize_string_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(        // line 9
($context["pageTitle"] ?? null)))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroReport/Report/viewFromGrid.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroReport/Report/viewFromGrid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  50 => 1,  48 => 9,  45 => 4,  43 => 3,  41 => 2,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReport/Report/viewFromGrid.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ReportBundle/Resources/views/Report/viewFromGrid.html.twig");
    }
}
