<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/fa-icon.scss */
class __TwigTemplate_45c74bd76338050d6a7eb318c5ceb338 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

// Mixin for include font-awesome icons to custom elements
// List of icons https://github.com/FortAwesome/Font-Awesome/blob/master/scss/_variables.scss
// @param \$icon-name (Font Awesome icon)
// @param \$state {CSS pseudo-element}
// Use: @include fa-icon(\$view-product-gallery-icon, before, true) {
//  extra rules
// }
@mixin fa-icon(\$icon: null, \$state: before, \$extra-rules: false, \$rawIcon: false) {
    @if (\$icon) {
        \$content: \$icon;

        @if (\$rawIcon) {
            \$content: \$icon;
        } @else {
            \$content: '#{\$icon}';
        }

        &::#{\$state} {
            content: \$content;
            font-family: \$icon-font;

            @if (\$extra-rules) {
                @content;
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/fa-icon.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/fa-icon.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/fa-icon.scss");
    }
}
