<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/registry/registry-entry.js */
class __TwigTemplate_582cee410521e76e2dcc33b4596a7ef8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const Backbone = require('backbone');

    /**
     *
     * @param {Object} instance
     * @param {string} instance.globalId
     * @constructor
     * @mixes {Backbone.Events}
     */
    function RegistryEntry(instance) {
        this.instance = instance;
        this.id = instance.globalId;
        this.applicants = [];
    }

    RegistryEntry.prototype = _.extend(Object.create(Backbone.Events), /** @lends RegistryEntry.prototype */ {
        disposed: false,

        dispose: function() {
            if (this.disposed) {
                return;
            }
            this.trigger('dispose', this);
            this.stopListening();
            delete this.instance;
            this.applicants = [];
            this.disposed = true;
            return typeof Object.freeze === 'function' ? Object.freeze(this) : void 0;
        },

        addApplicant: function(applicant) {
            if (!applicant || !_.isFunction(applicant.trigger)) {
                throw new TypeError('applicant object should implement Backbone.Events');
            }
            if (this.applicants.indexOf(applicant) === -1) {
                this.applicants.push(applicant);
                this.listenToOnce(applicant, 'dispose', this.removeApplicant);
            }
        },

        removeApplicant: function(applicant) {
            const index = this.applicants.indexOf(applicant);
            if (index !== -1) {
                this.applicants.splice(index, 1);
                this.trigger('removeApplicant', this, applicant);
            }
        }
    });

    return RegistryEntry;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/registry/registry-entry.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/registry/registry-entry.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/registry/registry-entry.js");
    }
}
