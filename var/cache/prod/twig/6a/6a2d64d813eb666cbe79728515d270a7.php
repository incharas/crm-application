<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroApi/ApiDoc/resource.html.twig */
class __TwigTemplate_3ea8a791affa47498394b65d25b85d97 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->loadTemplate("@NelmioApiDoc/method.html.twig", "@OroApi/ApiDoc/resource.html.twig", 1)->display($context);
    }

    public function getTemplateName()
    {
        return "@OroApi/ApiDoc/resource.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroApi/ApiDoc/resource.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ApiBundle/Resources/views/ApiDoc/resource.html.twig");
    }
}
