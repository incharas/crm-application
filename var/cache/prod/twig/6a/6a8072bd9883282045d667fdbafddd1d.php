<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAction/Widget/_widget.html.twig */
class __TwigTemplate_792ecd1d15448640bfabc8431adeef36 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Oro\Bundle\ActionBundle\Twig\OperationExtension']->hasButtons(($context["params"] ?? null))) {
            // line 2
            ob_start(function () { return ''; });
            // line 3
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath($this->extensions['Oro\Bundle\ActionBundle\Twig\OperationExtension']->getWidgetRoute(),             // line 5
($context["params"] ?? null)), "alias" => "action_buttons_widget"]);
            $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 10
            if (twig_length_filter($this->env, twig_trim_filter(($context["action"] ?? null)))) {
                // line 11
                echo "<div class=\"pull-left\">";
                // line 12
                echo twig_escape_filter($this->env, ($context["action"] ?? null), "html", null, true);
                // line 13
                echo "</div>";
            }
        }
    }

    public function getTemplateName()
    {
        return "@OroAction/Widget/_widget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 13,  49 => 12,  47 => 11,  45 => 10,  42 => 5,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAction/Widget/_widget.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActionBundle/Resources/views/Widget/_widget.html.twig");
    }
}
