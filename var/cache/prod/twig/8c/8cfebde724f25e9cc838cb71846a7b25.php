<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChannel/ChannelIntegration/widget/update.html.twig */
class __TwigTemplate_d7212db69ff57cbe8852dc24d55c2a45 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 3
        return "@OroIntegration/Integration/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["isWidgetContext"] = true;
        // line 2
        $context["formAction"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 2), "uri", [], "any", false, false, false, 2);
        // line 4
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/ChannelIntegration/widget/update.html.twig", 4)->unwrap();
        // line 3
        $this->parent = $this->loadTemplate("@OroIntegration/Integration/update.html.twig", "@OroChannel/ChannelIntegration/widget/update.html.twig", 3);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/ChannelIntegration/widget/update.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
    <div class=\"widget-actions\">
        <button type=\"reset\" class=\"btn\">";
        // line 11
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
        <button type=\"submit\" class=\"btn btn-success\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Done"), "html", null, true);
        echo "</button>
    </div>

    ";
        // line 15
        $context["wid"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 15), "get", [0 => "_wid"], "method", false, false, false, 15);
        // line 16
        echo "
    <div ";
        // line 17
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orochannel/js/app/views/configure-integration-view", "options" => ["_sourceElement" => (("[data-wid=\"" .         // line 20
($context["wid"] ?? null)) . "\"]"), "wid" =>         // line 21
($context["wid"] ?? null), "dataFieldSelector" => "[data-ftid=oro_channel_form_dataSource_data]", "apiKeyFieldSelector" => "[data-ftid=oro_integration_channel_form_transport_apiKey]"]]], 17, $context, $this->getSourceContext());
        // line 25
        echo "></div>
";
    }

    public function getTemplateName()
    {
        return "@OroChannel/ChannelIntegration/widget/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  87 => 25,  85 => 21,  84 => 20,  83 => 17,  80 => 16,  78 => 15,  72 => 12,  68 => 11,  63 => 9,  60 => 8,  57 => 7,  53 => 6,  48 => 3,  46 => 4,  44 => 2,  42 => 1,  35 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChannel/ChannelIntegration/widget/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ChannelBundle/Resources/views/ChannelIntegration/widget/update.html.twig");
    }
}
