<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/DataFieldMapping/update.html.twig */
class __TwigTemplate_d5f34cb3cab01caf2b5c6c55203258a1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'bodyClass' => [$this, 'block_bodyClass'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["QD"] = $this->macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroDotmailer/DataFieldMapping/update.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["entity"] ?? null), "entity", [], "any", false, false, false, 5), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmapping.entity_label")]]);
        // line 8
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 8);
        // line 10
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "value", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_mapping_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "value", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_mapping_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroDotmailer/DataFieldMapping/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 11
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "dotmailer-page";
    }

    // line 12
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/DataFieldMapping/update.html.twig", 13)->unwrap();
        // line 14
        echo "
    ";
        // line 15
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 15), "value", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 15), "value", [], "any", false, false, false, 15)))) {
            // line 16
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_dotmailer_datafield_mapping", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["form"] ?? null), "vars", [], "any", false, false, false, 17), "value", [], "any", false, false, false, 17), "id", [], "any", false, false, false, 17)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_mapping_index"), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20), "id", [], "any", false, false, false, 20), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmapping.entity_label")]], 16, $context, $this->getSourceContext());
            // line 22
            echo "

        ";
            // line 24
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 24, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 26
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_mapping_index")], 26, $context, $this->getSourceContext());
        echo "
    ";
        // line 27
        $context["html"] = "";
        // line 28
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_dotmailer_datafield_mapping_create")) {
            // line 29
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_dotmailer_datafield_mapping_create"]], 29, $context, $this->getSourceContext()));
            // line 32
            echo "    ";
        }
        // line 33
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 33), "value", [], "any", false, false, false, 33), "id", [], "any", false, false, false, 33) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_dotmailer_datafield_mapping_update"))) {
            // line 34
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_dotmailer_datafield_mapping_update", "params" => ["id" => "\$id"]]], 34, $context, $this->getSourceContext()));
            // line 38
            echo "    ";
        }
        // line 39
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 39, $context, $this->getSourceContext());
        echo "
";
    }

    // line 42
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 44
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 45
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_mapping_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmapping.entity_plural_label"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmapping.entity_label")];
            // line 50
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 52
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmapping.entity_label")]);
            // line 53
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroDotmailer/DataFieldMapping/update.html.twig", 53)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 54
            echo "    ";
        }
    }

    // line 57
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/DataFieldMapping/update.html.twig", 58)->unwrap();
        // line 59
        echo "    ";
        $macros["dataFieldMappingUpdate"] = $this;
        // line 60
        echo "
    ";
        // line 61
        $context["id"] = "dotmailer-datafield-mapping-form";
        // line 62
        echo "    ";
        $context["type"] = "oro_dotmailer_datafield_mapping";
        // line 63
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.block.general"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 68
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channel", [], "any", false, false, false, 68), 'row'), 1 =>         // line 69
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 69), 'row'), 2 =>         // line 70
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "syncPriority", [], "any", false, false, false, 70), 'row')]]]]];
        // line 74
        echo "    ";
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.block.mapping"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 79
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "config_source", [], "any", false, false, false, 79), 'row'), 1 => twig_call_macro($macros["UI"], "macro_scrollSubblock", ["", [0 => twig_call_macro($macros["dataFieldMappingUpdate"], "macro_mapping_form", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 84
($context["form"] ?? null), "config", [], "any", false, false, false, 84), ["id" => (        // line 85
($context["type"] ?? null) . "-mapping-form")]], 83, $context, $this->getSourceContext()), 1 => twig_call_macro($macros["dataFieldMappingUpdate"], "macro_mapping_list", [["id" => (        // line 89
($context["type"] ?? null) . "-mapping-list"), "rowId" => (        // line 90
($context["type"] ?? null) . "-mapping-row")]], 87, $context, $this->getSourceContext())], "", "", (        // line 96
($context["type"] ?? null) . "-mappings")], 80, $context, $this->getSourceContext())]]]]]);
        // line 101
        echo "
    ";
        // line 102
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.block.additional")));
        // line 103
        echo "
    ";
        // line 104
        $context["data"] = ["formErrors" => ((        // line 105
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 106
($context["dataBlocks"] ?? null)];
        // line 108
        echo "
    ";
        // line 109
        echo twig_call_macro($macros["QD"], "macro_query_designer_column_chain_template", ["column-chain-template"], 109, $context, $this->getSourceContext());
        echo "
    ";
        // line 110
        echo twig_call_macro($macros["dataFieldMappingUpdate"], "macro_initJsWidgets", [($context["type"] ?? null), ($context["form"] ?? null), ($context["entities"] ?? null)], 110, $context, $this->getSourceContext());
        echo "

    ";
        // line 112
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    // line 114
    public function macro_mapping_form($__form__ = null, $__attr__ = null, $__params__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "attr" => $__attr__,
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 115
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/DataFieldMapping/update.html.twig", 115)->unwrap();
            // line 116
            echo "    ";
            $context["attr"] = twig_array_merge(((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 117
($context["attr"] ?? null), "class", [], "any", true, true, false, 117)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 117), "")) : ("")) . " dotmailer-mapping-form clearfix"))]);
            // line 119
            echo "    <div";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr"] ?? null)], 119, $context, $this->getSourceContext());
            echo ">
        ";
            // line 120
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entityFields", [], "any", false, false, false, 120), 'row', ["label" => "oro.dotmailer.datafieldmappingconfig.entity_fields.label", "attr" => ["data-purpose" => "multiple-entityfield-selector", "data-validation" => json_encode(["NotBlank" => ["message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.validation.entity_field_required")]])]]);
            // line 126
            echo "
        ";
            // line 127
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "dataField", [], "any", false, false, false, 127), 'row', ["label" => "oro.dotmailer.datafieldmappingconfig.data_field.label", "attr" => ["data-purpose" => "datafield-selector", "data-validation" => json_encode(["NotBlank" => null])]]);
            // line 133
            echo "
        ";
            // line 134
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "isTwoWaySync", [], "any", false, false, false, 134), 'row', ["label" => "oro.dotmailer.datafieldmappingconfig.is_two_way_sync.label", "attr" => ["class" => "two-way-sync-selector", "data-purpose" => "two-way-sync-selector"]]);
            // line 140
            echo "
        <div class=\"pull-right\">
            <div class=\"btn-group\">
                ";
            // line 143
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["aCss" => "no-hash cancel-button column-form-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.cancel")]], 143, $context, $this->getSourceContext());
            // line 146
            echo "
            </div>
            <div class=\"btn-group\">
                ";
            // line 149
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["visible" => false, "aCss" => "no-hash btn-success save-button column-form-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.save")]], 149, $context, $this->getSourceContext());
            // line 153
            echo "
            </div>
            <div class=\"btn-group\">
                ";
            // line 156
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["visible" => false, "aCss" => "no-hash btn-primary add-button column-form-button", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.form.action.add")]], 156, $context, $this->getSourceContext());
            // line 160
            echo "
            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 165
    public function macro_mapping_list($__attr__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attr" => $__attr__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 166
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/DataFieldMapping/update.html.twig", 166)->unwrap();
            // line 167
            echo "    ";
            $macros["dataFieldMappingUpdate"] = $this;
            // line 168
            echo "
    ";
            // line 169
            $context["attr"] = twig_array_merge(((array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 169)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 169), "")) : ("")) . " grid-container query-designer-grid-container"))]);
            // line 170
            echo "    <div";
            echo twig_call_macro($macros["UI"], "macro_attributes", [($context["attr"] ?? null)], 170, $context, $this->getSourceContext());
            echo ">
        <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\">
            <thead>
            <tr>
                <th class=\"entityfield-column\"><span>";
            // line 174
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.entity_fields.label"), "html", null, true);
            echo "</span></th>
                <th class=\"datafield-column\"><span>";
            // line 175
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.data_field.label"), "html", null, true);
            echo "</span></th>
                <th class=\"twowaysync-column\"><span>";
            // line 176
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.is_two_way_sync.label"), "html", null, true);
            echo "</span></th>
                <th class=\"action-column\"><span>";
            // line 177
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.query_designer.datagrid.column.actions"), "html", null, true);
            echo "</span></th>
            </tr>
            </thead>
            <tbody class=\"mapping-container\">
            </tbody>
        </table>
    </div>
    ";
            // line 184
            echo twig_call_macro($macros["dataFieldMappingUpdate"], "macro_mapping_template", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "rowId", [], "any", false, false, false, 184)], 184, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 186
    public function macro_mapping_template($__id__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "id" => $__id__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 187
            echo "    <script type=\"text/html\" id=\"";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "\">
        <tr data-cid=\"<%- cid %>\">
            <td class=\"field-cell<% if (obj.deleted) { %> deleted-field<% } %>\"><%= entityFields %></td>
            <td class=\"datafield-cell\"><%- dataField %></td>
            <td class=\"twowaysync-cell\"><%= isTwoWaySync %></td>
            <td class=\"action-cell\">
                ";
            // line 193
            ob_start(function () { return ''; });
            // line 194
            echo "                <a href=\"#\" class=\"btn btn-icon btn-lighter action no-hash edit-button\"
                        title=\"";
            // line 195
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.update_mapping"), "html", null, true);
            echo "\"
                        role=\"button\"
                        data-collection-action=\"edit\">
                    <span class=\"fa-edit\" aria-hidden=\"true\"></span>
                </a>
                <a href=\"#\" class=\"btn btn-icon btn-lighter action no-hash delete-button\"
                        title=\"";
            // line 201
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.delete_mapping"), "html", null, true);
            echo "\"
                        role=\"button\"
                        data-collection-action=\"delete\"
                        data-message=\"";
            // line 204
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.delete_mapping_confirmation"), "html", null, true);
            echo "\">
                    <span class=\"fa-trash\" aria-hidden=\"true\"></span>
                </a>
                ";
            $___internal_parse_98_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 193
            echo twig_spaceless($___internal_parse_98_);
            // line 208
            echo "            </td>
        </tr>
    </script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 212
    public function macro_initJsWidgets($__type__ = null, $__form__ = null, $__entities__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "type" => $__type__,
            "form" => $__form__,
            "entities" => $__entities__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 213
            echo "    ";
            $context["widgetOptions"] = ["dataProviderFilterPreset" => "dotmailer", "valueSource" => (("[data-ftid=" .             // line 215
($context["type"] ?? null)) . "_form_config_source]"), "entityChoice" => (("[data-ftid=" .             // line 216
($context["type"] ?? null)) . "_form_entity]"), "entityChangeConfirmMessage" => "oro.dotmailer.datafieldmapping.change_entity_confirmation", "mapping" => ["editor" => ["namePattern" => (("^" .             // line 219
($context["type"] ?? null)) . "_form\\[config\\]\\[([\\w\\W]*)\\]\$")], "form" => (("#" .             // line 220
($context["type"] ?? null)) . "-mapping-form"), "itemContainer" => (("#" .             // line 221
($context["type"] ?? null)) . "-mapping-list .mapping-container"), "itemTemplate" => (("#" .             // line 222
($context["type"] ?? null)) . "-mapping-row")], "channel" => ["channelChoice" => (("[data-ftid=" .             // line 225
($context["type"] ?? null)) . "_form_channel]"), "changeChannelConfirmMessage" => "oro.dotmailer.datafieldmapping.change_channel_confirmation"], "select2FieldChoiceTemplate" => "#column-chain-template", "select2FieldChoicePlaceholoder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafieldmappingconfig.entityField.placeholder")];
            // line 231
            echo "
    <div data-page-component-module=\"orodotmailer/js/app/components/mapping-component\"
         data-page-component-options=\"";
            // line 233
            echo twig_escape_filter($this->env, json_encode(($context["widgetOptions"] ?? null)), "html", null, true);
            echo "\"></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/DataFieldMapping/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  446 => 233,  442 => 231,  440 => 225,  439 => 222,  438 => 221,  437 => 220,  436 => 219,  435 => 216,  434 => 215,  432 => 213,  417 => 212,  405 => 208,  403 => 193,  396 => 204,  390 => 201,  381 => 195,  378 => 194,  376 => 193,  366 => 187,  353 => 186,  342 => 184,  332 => 177,  328 => 176,  324 => 175,  320 => 174,  312 => 170,  310 => 169,  307 => 168,  304 => 167,  301 => 166,  288 => 165,  275 => 160,  273 => 156,  268 => 153,  266 => 149,  261 => 146,  259 => 143,  254 => 140,  252 => 134,  249 => 133,  247 => 127,  244 => 126,  242 => 120,  237 => 119,  235 => 117,  233 => 116,  230 => 115,  215 => 114,  209 => 112,  204 => 110,  200 => 109,  197 => 108,  195 => 106,  194 => 105,  193 => 104,  190 => 103,  188 => 102,  185 => 101,  183 => 96,  182 => 90,  181 => 89,  180 => 85,  179 => 84,  178 => 79,  176 => 74,  174 => 70,  173 => 69,  172 => 68,  170 => 63,  167 => 62,  165 => 61,  162 => 60,  159 => 59,  156 => 58,  152 => 57,  147 => 54,  144 => 53,  141 => 52,  135 => 50,  133 => 45,  131 => 44,  128 => 43,  124 => 42,  117 => 39,  114 => 38,  111 => 34,  108 => 33,  105 => 32,  102 => 29,  99 => 28,  97 => 27,  92 => 26,  87 => 24,  83 => 22,  81 => 20,  80 => 17,  78 => 16,  76 => 15,  73 => 14,  70 => 13,  66 => 12,  59 => 11,  54 => 1,  52 => 10,  50 => 8,  48 => 5,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/DataFieldMapping/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/DataFieldMapping/update.html.twig");
    }
}
