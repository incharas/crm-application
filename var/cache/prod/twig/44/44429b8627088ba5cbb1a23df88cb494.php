<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/content_sidebar.html.twig */
class __TwigTemplate_97820168ebb70591140e777abaf3ecf0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'header_wrapper' => [$this, 'block_header_wrapper'],
            'header' => [$this, 'block_header'],
            'controls_wrapper' => [$this, 'block_controls_wrapper'],
            'sidebar_wrapper' => [$this, 'block_sidebar_wrapper'],
            'sidebar' => [$this, 'block_sidebar'],
            'content_wrapper' => [$this, 'block_content_wrapper'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["options"] = twig_array_merge(((array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([])), ["view" => "oroui/js/app/views/content-sidebar-view"]);
        // line 4
        $context["class"] = "content-with-sidebar--";
        // line 5
        echo "<div data-page-component-view=\"";
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
     class=\"";
        // line 6
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "container\">
    <div data-role=\"sidebar\" class=\"";
        // line 7
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "sidebar\">
        <div class=\"";
        // line 8
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "header\">
            ";
        // line 9
        $this->displayBlock('header_wrapper', $context, $blocks);
        // line 25
        echo "        </div>
        ";
        // line 26
        $this->displayBlock('sidebar_wrapper', $context, $blocks);
        // line 32
        echo "    </div>
    ";
        // line 33
        $this->displayBlock('content_wrapper', $context, $blocks);
        // line 39
        echo "</div>
";
    }

    // line 9
    public function block_header_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "                <div class=\"";
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "header-content\">
                    ";
        // line 11
        $this->displayBlock('header', $context, $blocks);
        // line 13
        echo "                </div>
                ";
        // line 14
        $this->displayBlock('controls_wrapper', $context, $blocks);
        // line 24
        echo "            ";
    }

    // line 11
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "                    ";
    }

    // line 14
    public function block_controls_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "                    <div class=\"";
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "controls\" data-role=\"sidebar-controls\">
                        <div data-role=\"sidebar-maximize\" class=\"";
        // line 16
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "control ";
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "maximize\">
                            <i class=\"fa-arrow-right \"></i>
                        </div>
                        <div data-role=\"sidebar-minimize\" class=\"";
        // line 19
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "control ";
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "minimize\">
                            <i class=\"fa-arrow-left \"></i>
                        </div>
                    </div>
                ";
    }

    // line 26
    public function block_sidebar_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        echo "        <div data-role=\"sidebar-content\" class=\"";
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "sidebar-content\">
            ";
        // line 28
        $this->displayBlock('sidebar', $context, $blocks);
        // line 30
        echo "        </div>
        ";
    }

    // line 28
    public function block_sidebar($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "            ";
    }

    // line 33
    public function block_content_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    <div data-role=\"content\" class=\"";
        echo twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true);
        echo "content\">
        ";
        // line 35
        $this->displayBlock('content', $context, $blocks);
        // line 37
        echo "    </div>
    ";
    }

    // line 35
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "        ";
    }

    public function getTemplateName()
    {
        return "@OroUI/content_sidebar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 36,  178 => 35,  173 => 37,  171 => 35,  166 => 34,  162 => 33,  158 => 29,  154 => 28,  149 => 30,  147 => 28,  142 => 27,  138 => 26,  127 => 19,  119 => 16,  114 => 15,  110 => 14,  106 => 12,  102 => 11,  98 => 24,  96 => 14,  93 => 13,  91 => 11,  86 => 10,  82 => 9,  77 => 39,  75 => 33,  72 => 32,  70 => 26,  67 => 25,  65 => 9,  61 => 8,  57 => 7,  53 => 6,  48 => 5,  46 => 4,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/content_sidebar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/content_sidebar.html.twig");
    }
}
