<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Calendar/Menu/changeCalendarColor.html.twig */
class __TwigTemplate_fb67987c6880823fce3d0b101bc71820 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["oro_menu"] = $this->macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroCalendar/Calendar/Menu/changeCalendarColor.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        ob_start(function () { return ''; });
        // line 4
        echo "<li ";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["itemAttributes"] ?? null)], 4, $context, $this->getSourceContext());
        echo ">
    <div class=\"color-picker\"></div>
    <div class=\"dropdown-divider context-menu-divider\"></div>";
        // line 7
        $context["linkAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "linkAttributes", [], "any", false, false, false, 7);
        // line 8
        echo "        <a href=\"#\" role=\"button\" ";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 8, $context, $this->getSourceContext());
        echo ">
            <span class=\"custom-color-wrapper\">
                <span class=\"custom-color\"></span>
            </span>
            <span class=\"custom-color-name\">";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.context.edit_color"), "html", null, true);
        echo "</span>
        </a>
</li>
";
        $___internal_parse_86_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 3
        echo twig_spaceless($___internal_parse_86_);
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Calendar/Menu/changeCalendarColor.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  67 => 3,  60 => 12,  52 => 8,  50 => 7,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Calendar/Menu/changeCalendarColor.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Calendar/Menu/changeCalendarColor.html.twig");
    }
}
