<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImportExport/ImportExport/export-buttons.html.twig */
class __TwigTemplate_3627b962e7c1d10c67e2125ce1ac0900 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroImportExport/ImportExport/export-buttons.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["exportButtonsHtml"] = "";
        // line 4
        $context["showExportButtons"] = false;
        // line 5
        echo "
";
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->extensions['Oro\Bundle\ImportExportBundle\Twig\GetImportExportConfigurationExtension']->getConfiguration(($context["alias"] ?? null)));
        foreach ($context['_seq'] as $context["_key"] => $context["configuration"]) {
            // line 7
            echo "    ";
            $context["hasExportProcessor"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportProcessorAlias", [], "any", true, true, false, 7) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportProcessorAlias", [], "any", false, false, false, 7)));
            // line 8
            echo "    ";
            $context["exportAllowed"] = ((($context["hasExportProcessor"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_importexport_export")) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW;entity:OroImportExportBundle:ImportExportResult"));
            // line 9
            echo "    ";
            $context["exportProcessor"] = ((twig_test_iterable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportProcessorAlias", [], "any", false, false, false, 9))) ? (twig_first($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportProcessorAlias", [], "any", false, false, false, 9))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportProcessorAlias", [], "any", false, false, false, 9)));
            // line 10
            echo "    ";
            $context["isExportPopupRequired"] = twig_test_iterable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportProcessorAlias", [], "any", false, false, false, 10));
            // line 11
            echo "    ";
            $context["exportLabel"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportButtonLabel", [], "any", true, true, false, 11)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportButtonLabel", [], "any", false, false, false, 11), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export.label"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export.label")));
            // line 12
            echo "
    ";
            // line 13
            if (($context["exportAllowed"] ?? null)) {
                // line 14
                echo "        ";
                $context["showExportButtons"] = true;
                // line 15
                echo "
        ";
                // line 16
                ob_start(function () { return ''; });
                // line 17
                echo "            <a href=\"#\"
                role=\"button\"
                class=\"btn export-btn icons-holder-text no-hash\"
                data-page-component-module=\"oroui/js/app/components/view-component\"
                data-page-component-options=\"";
                // line 21
                echo twig_escape_filter($this->env, json_encode(["view" => "oroimportexport/js/app/views/export-button-view", "entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 23
$context["configuration"], "entityClass", [], "any", false, false, false, 23), "routeOptions" => twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 24
$context["configuration"], "routeOptions", [], "any", true, true, false, 24)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "routeOptions", [], "any", false, false, false, 24), [])) : ([])), ((array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([]))), "exportTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 25
$context["configuration"], "exportPopupTitle", [], "any", true, true, false, 25)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportPopupTitle", [], "any", false, false, false, 25), ($context["exportLabel"] ?? null))) : (($context["exportLabel"] ?? null))), "exportProcessor" => ((                // line 26
array_key_exists("exportProcessor", $context)) ? (_twig_default_filter(($context["exportProcessor"] ?? null), null)) : (null)), "exportJob" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 27
$context["configuration"], "exportJobName", [], "any", true, true, false, 27)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "exportJobName", [], "any", false, false, false, 27), null)) : (null)), "isExportPopupRequired" => ((                // line 28
array_key_exists("isExportPopupRequired", $context)) ? (_twig_default_filter(($context["isExportPopupRequired"] ?? null), false)) : (false)), "filePrefix" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 29
$context["configuration"], "filePrefix", [], "any", true, true, false, 29)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configuration"], "filePrefix", [], "any", false, false, false, 29), null)) : (null))]), "html", null, true);
                // line 30
                echo "\"
            >
                <span class=\"fa-upload hide-text\">";
                // line 32
                echo twig_escape_filter($this->env, ($context["exportLabel"] ?? null), "html", null, true);
                echo "</span>";
                echo twig_escape_filter($this->env, ($context["exportLabel"] ?? null), "html", null, true);
                echo "
            </a>
        ";
                $context["html"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 35
                echo "
        ";
                // line 36
                $context["exportButtonsHtml"] = (($context["exportButtonsHtml"] ?? null) . ($context["html"] ?? null));
                // line 37
                echo "    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['configuration'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 39
        echo "
";
        // line 40
        if (($context["showExportButtons"] ?? null)) {
            // line 41
            echo "    <div class=\"btn-group\">
        ";
            // line 42
            echo twig_call_macro($macros["UI"], "macro_pinnedDropdownButton", [["html" =>             // line 43
($context["exportButtonsHtml"] ?? null)]], 42, $context, $this->getSourceContext());
            // line 44
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroImportExport/ImportExport/export-buttons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 44,  129 => 43,  128 => 42,  125 => 41,  123 => 40,  120 => 39,  113 => 37,  111 => 36,  108 => 35,  100 => 32,  96 => 30,  94 => 29,  93 => 28,  92 => 27,  91 => 26,  90 => 25,  89 => 24,  88 => 23,  87 => 21,  81 => 17,  79 => 16,  76 => 15,  73 => 14,  71 => 13,  68 => 12,  65 => 11,  62 => 10,  59 => 9,  56 => 8,  53 => 7,  49 => 6,  46 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImportExport/ImportExport/export-buttons.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Resources/views/ImportExport/export-buttons.html.twig");
    }
}
