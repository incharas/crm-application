<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/settings/mixins/gradient-pattern.scss */
class __TwigTemplate_f4cfa90f3c9d19c565b9ae662a048d9c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@mixin pattern-checkerboard(\$pattern-size: 20px, \$pattern-color: #e8e8e8, \$pattern-background: #fff) {
    background: \$pattern-background;
    background-image:
        linear-gradient(45deg, \$pattern-color 25%, transparent 25%),
        linear-gradient(-45deg, \$pattern-color 25%, transparent 25%),
        linear-gradient(45deg, transparent 75%, \$pattern-color 75%),
        linear-gradient(-45deg, transparent 75%, \$pattern-color 75%);
    background-size: \$pattern-size \$pattern-size;
    background-position: 0 0, 0 #{\$pattern-size * .5},
                         #{\$pattern-size * .5} -#{\$pattern-size * .5},
                         -#{\$pattern-size * .5} 0;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/settings/mixins/gradient-pattern.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/settings/mixins/gradient-pattern.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/settings/mixins/gradient-pattern.scss");
    }
}
