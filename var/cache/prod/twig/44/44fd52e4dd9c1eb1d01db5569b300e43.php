<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/templates/inline-editable-wrapper-view.html */
class __TwigTemplate_81c80e571295e6e73df9c1887e76c135 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<span class=\"inline-actions-element\">
    <span class=\"inline-actions-element_wrapper\" data-role=\"container\"></span>
    <span class=\"inline-actions-element_actions\">
        <button data-role=\"start-editing\" class=\"inline-actions-btn inline-actions-btn--size-s\"
            title=\"<%- _.__('Edit') %>\"><span class=\"fa-pencil\" aria-hidden=\"true\"></span></button>
    </span>
</span>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/templates/inline-editable-wrapper-view.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/templates/inline-editable-wrapper-view.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/templates/inline-editable-wrapper-view.html");
    }
}
