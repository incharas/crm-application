<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/load-more.scss */
class __TwigTemplate_8b527ab0eac78011d324f258455eba0d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$load-more-offset: 8px 0 !default;
\$load-more-label-offset: 0 8px !default;
\$load-more-label-color: \$primary-550 !default;
\$load-more-label-background-color: \$primary-inverse !default;
\$load-more-label-color-hover: \$primary-300 !default;
\$load-more-align: center !default;
\$load-more-align: center !default;
\$load-more-border-width: 1px !default;
\$load-more-decor-size: 8px !default;
\$load-more-decor-border: \$load-more-border-width solid \$primary-860 !default;
\$load-more-loader-icon-size: 14px !default;
\$load-more-loader-icon-width: 2px !default;

\$load-more-in-thread-offset: 0 !default;
\$load-more-in-thread-background-color: \$primary-900 !default;
\$load-more-in-thread-border-color: \$primary-830 !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/load-more.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/load-more.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/load-more.scss");
    }
}
