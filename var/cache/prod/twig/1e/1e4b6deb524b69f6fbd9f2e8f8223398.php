<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOrganization/Organization/update.html.twig */
class __TwigTemplate_3065395405827abdde04e0c2aba4576d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%organization.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "name", [], "any", false, false, false, 3), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.organization.entity_label")]]);
        // line 5
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 5);
        // line 6
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_organization_update_current");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroOrganization/Organization/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOrganization/Organization/update.html.twig", 9)->unwrap();
        // line 10
        echo "
    ";
        // line 11
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_organization_update")) {
            // line 12
            echo "        ";
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_organization_update_current"]], 12, $context, $this->getSourceContext());
            // line 13
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 13, $context, $this->getSourceContext());
            echo "
    ";
        }
    }

    // line 17
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 19
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 20
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_organization_update_current"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.organization.entity_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 23
($context["entity"] ?? null), "name", [], "any", false, false, false, 23)];
            // line 25
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 27
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.organization.entity_label")]);
            // line 28
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroOrganization/Organization/update.html.twig", 28)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 29
            echo "    ";
        }
    }

    // line 32
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "    ";
        $context["id"] = "organization-form";
        // line 34
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 41
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "enabled", [], "any", false, false, false, 41), 'row'), 1 =>         // line 42
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 42), 'row'), 2 =>         // line 43
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 43), 'row'), 3 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 44
($context["form"] ?? null), "owner", [], "any", true, true, false, 44)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 44), 'row')) : (""))]]]]];
        // line 49
        echo "
    ";
        // line 50
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 51
        echo "
    ";
        // line 52
        $context["data"] = ["formErrors" => ((        // line 53
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 54
($context["dataBlocks"] ?? null)];
        // line 56
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroOrganization/Organization/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 56,  132 => 54,  131 => 53,  130 => 52,  127 => 51,  125 => 50,  122 => 49,  120 => 44,  119 => 43,  118 => 42,  117 => 41,  115 => 34,  112 => 33,  108 => 32,  103 => 29,  100 => 28,  97 => 27,  91 => 25,  89 => 23,  88 => 20,  86 => 19,  83 => 18,  79 => 17,  71 => 13,  68 => 12,  66 => 11,  63 => 10,  60 => 9,  56 => 8,  51 => 1,  49 => 6,  47 => 5,  45 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOrganization/Organization/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/OrganizationBundle/Resources/views/Organization/update.html.twig");
    }
}
