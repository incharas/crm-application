<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroApi/ApiDoc/input.html.twig */
class __TwigTemplate_4373c9f5a308168794d4de49192cd964 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<h4>Input</h4>
<table class='fullwidth input'>
    <thead>
    <tr>
        <th>Name</th>
        <th>Type</th>
        <th>Description</th>
    </tr>
    </thead>
        <tbody>
        ";
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
        foreach ($context['_seq'] as $context["name"] => $context["infos"]) {
            // line 12
            echo "            ";
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "readonly", [], "any", false, false, false, 12)) {
                // line 13
                echo "                <tr>
                    <td>";
                // line 14
                echo twig_escape_filter($this->env, $context["name"], "html", null, true);
                echo "</td>
                    <td>";
                // line 15
                ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "dataType", [], "any", true, true, false, 15)) ? (print (twig_escape_filter($this->env, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "dataType", [], "any", false, false, false, 15) . (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "subType", [], "any", true, true, false, 15) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "subType", [], "any", false, false, false, 15))) ? (((" (" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "subType", [], "any", false, false, false, 15)) . ")")) : (""))), "html", null, true))) : (print ("")));
                echo "</td>
                    <td>";
                // line 16
                echo ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "description", [], "any", true, true, false, 16)) ? ($this->extensions['Oro\Bundle\ApiBundle\Twig\MarkdownExtension']->markdown(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["infos"], "description", [], "any", false, false, false, 16))) : (""));
                echo "</td>
                </tr>
            ";
            }
            // line 19
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['infos'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        </tbody>
</table>
";
    }

    public function getTemplateName()
    {
        return "@OroApi/ApiDoc/input.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  79 => 20,  73 => 19,  67 => 16,  63 => 15,  59 => 14,  56 => 13,  53 => 12,  49 => 11,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroApi/ApiDoc/input.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ApiBundle/Resources/views/ApiDoc/input.html.twig");
    }
}
