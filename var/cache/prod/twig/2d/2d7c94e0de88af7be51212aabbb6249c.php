<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroIntegration/Integration/update.html.twig */
class __TwigTemplate_591eb6890ae08ca85d33cab5958e7ae9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroIntegration/Integration/update.html.twig", 2)->unwrap();
        // line 4
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), $this->extensions['Oro\Bundle\IntegrationBundle\Twig\IntegrationExtension']->getThemes(($context["form"] ?? null)), true);
        // line 6
        $context["entity"] = ((array_key_exists("entity", $context)) ? (($context["entity"] ?? null)) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 6), "value", [], "any", false, false, false, 6)));
        // line 7
        $context["formAction"] = ((array_key_exists("formAction", $context)) ? (        // line 8
($context["formAction"] ?? null)) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["entity"] ?? null), "id", [], "any", false, false, false, 9)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_integration_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 9)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_integration_create")))));
        // line 12
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 12)) {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%integration.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["entity"] ?? null), "name", [], "any", false, false, false, 13)]]);
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroIntegration/Integration/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 16
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroIntegration/Integration/update.html.twig", 17)->unwrap();
        // line 18
        echo "
    ";
        // line 19
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <span class=\"page-title__status\">
        ";
        // line 21
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "enabled", [], "any", false, false, false, 21)) {
            // line 22
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.integration.integration.enabled.active.label"), "enabled"], 22, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 24
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.integration.integration.enabled.inactive.label"), "disabled"], 24, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 26
        echo "    </span>
";
    }

    // line 29
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroIntegration/Integration/update.html.twig", 30)->unwrap();
        // line 31
        echo "
    ";
        // line 32
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_integration_index")], 32, $context, $this->getSourceContext());
        echo "

    ";
        // line 34
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 34)) {
            // line 35
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "enabled", [], "any", false, false, false, 35)) {
                // line 36
                echo "        <div class=\"btn-group\" ";
                echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orointegration/js/app/views/integration-sync-view", "options" => ["integrationName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 39
($context["entity"] ?? null), "name", [], "any", false, false, false, 39)]]], 36, $context, $this->getSourceContext());
                // line 41
                echo ">
            ";
                // line 42
                ob_start(function () { return ''; });
                // line 43
                echo "                ";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("oro_integration_sync_button", $context)) ? (_twig_default_filter(($context["oro_integration_sync_button"] ?? null), "oro_integration_sync_button")) : ("oro_integration_sync_button")), ["entity" => ($context["entity"] ?? null)]);
                // line 44
                echo "                ";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("oro_integration_force_sync_button", $context)) ? (_twig_default_filter(($context["oro_integration_force_sync_button"] ?? null), "oro_integration_force_sync_button")) : ("oro_integration_force_sync_button")), ["entity" => ($context["entity"] ?? null)]);
                // line 45
                echo "            ";
                $context["buttonsHtml"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 46
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_pinnedDropdownButton", [["html" => ($context["buttonsHtml"] ?? null)]], 46, $context, $this->getSourceContext());
                echo "
        </div>
        ";
            }
            // line 49
            echo "    ";
        }
        // line 50
        echo "
    ";
        // line 51
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 51), "vars", [], "any", false, false, false, 51), "choices", [], "any", false, false, false, 51))) {
            // line 52
            echo "        ";
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_integration_index", "params" => ["_enableContentProviders" => "mainMenu"]]], 52, $context, $this->getSourceContext());
            // line 56
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_integration_create")) {
                // line 57
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_integration_create"]], 57, $context, $this->getSourceContext()));
                // line 60
                echo "        ";
            }
            // line 61
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 61), "value", [], "any", false, false, false, 61), "id", [], "any", false, false, false, 61) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_integration_update"))) {
                // line 62
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_integration_update", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 62, $context, $this->getSourceContext()));
                // line 66
                echo "        ";
            }
            // line 67
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 67, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 69
        echo "
";
    }

    // line 72
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 73)) {
            // line 74
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 75
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_integration_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.integration.integration.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 78
($context["entity"] ?? null), "name", [], "any", true, true, false, 78)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 78), "N/A")) : ("N/A"))];
            // line 80
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 82
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.integration.integration.entity_label")]);
            // line 83
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroIntegration/Integration/update.html.twig", 83)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 84
            echo "    ";
        }
    }

    // line 87
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 89
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 90
        echo "    ";
        $context["id"] = "channel-update";
        // line 91
        echo "    ";
        $context["dataBlocks"] = [];
        // line 92
        echo "
    ";
        // line 93
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "synchronizationSettings", [], "any", true, true, false, 93) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "synchronizationSettings", [], "any", false, false, false, 93), "count", [], "any", false, false, false, 93))) {
            // line 94
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Synchronization Settings"), "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 98
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "synchronizationSettings", [], "any", false, false, false, 98), 'widget')]]]]]);
            // line 101
            echo "    ";
        }
        // line 102
        echo "
    ";
        // line 103
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mappingSettings", [], "any", true, true, false, 103) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mappingSettings", [], "any", false, false, false, 103), "count", [], "any", false, false, false, 103))) {
            // line 104
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Mapping Settings"), "subblocks" => [0 => ["title" => "", "data" => [0 =>             // line 108
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mappingSettings", [], "any", false, false, false, 108), 'widget')]]]]]);
            // line 111
            echo "    ";
        }
        // line 112
        echo "
    ";
        // line 113
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 113) && (((array_key_exists("isWidgetContext", $context)) ? (_twig_default_filter(($context["isWidgetContext"] ?? null), false)) : (false)) === false))) {
            // line 114
            echo "        ";
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Statuses"), "subblocks" => [0 => ["title" => "", "data" => [0 => twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["oro-integration-status-grid", ["integrationId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 120
($context["entity"] ?? null), "id", [], "any", false, false, false, 120), "integrationType" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 121
($context["entity"] ?? null), "type", [], "any", false, false, false, 121)]], 119, $context, $this->getSourceContext())]]]]]);
            // line 126
            echo "    ";
        }
        // line 127
        echo "
    ";
        // line 128
        $context["dataBlocks"] = twig_array_merge([0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Basic Information"), "data" => [0 =>         // line 133
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget')]]]]],         // line 135
($context["dataBlocks"] ?? null));
        // line 136
        echo "
    ";
        // line 137
        $context["data"] = ["formErrors" => ((        // line 138
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 139
($context["dataBlocks"] ?? null)];
        // line 141
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroIntegration/Integration/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  282 => 141,  280 => 139,  279 => 138,  278 => 137,  275 => 136,  273 => 135,  272 => 133,  271 => 128,  268 => 127,  265 => 126,  263 => 121,  262 => 120,  260 => 114,  258 => 113,  255 => 112,  252 => 111,  250 => 108,  248 => 104,  246 => 103,  243 => 102,  240 => 101,  238 => 98,  236 => 94,  234 => 93,  231 => 92,  228 => 91,  225 => 90,  221 => 89,  215 => 87,  210 => 84,  207 => 83,  204 => 82,  198 => 80,  196 => 78,  195 => 75,  193 => 74,  190 => 73,  186 => 72,  181 => 69,  175 => 67,  172 => 66,  169 => 62,  166 => 61,  163 => 60,  160 => 57,  157 => 56,  154 => 52,  152 => 51,  149 => 50,  146 => 49,  139 => 46,  136 => 45,  133 => 44,  130 => 43,  128 => 42,  125 => 41,  123 => 39,  121 => 36,  118 => 35,  116 => 34,  111 => 32,  108 => 31,  105 => 30,  101 => 29,  96 => 26,  90 => 24,  84 => 22,  82 => 21,  77 => 19,  74 => 18,  71 => 17,  67 => 16,  62 => 1,  59 => 13,  56 => 12,  54 => 9,  53 => 8,  52 => 7,  50 => 6,  48 => 4,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroIntegration/Integration/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Resources/views/Integration/update.html.twig");
    }
}
