<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/safe-area-offset.scss */
class __TwigTemplate_238704a657a8353ff4844a789c915e59 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */
@use 'sass:list';

// Set inner offset from the edges to include the safe-area
// iPhone X
// Use like native padding
// Ex. @include safe-area-offset(padding, 10px 15px);
// Ex. @include safe-area-offset(margin, 10px 15px 20px);
@mixin safe-area-offset(\$property, \$values) {
    @if (\$property != null and \$values != null and length(\$values) <= 4) {
        \$safe-area-offset-left: 0;
        \$safe-area-offset-right: 0;

        #{\$property}: #{\$values};

        @if (\$enable-safe-area) {
            @if (length(\$values) == 1) {
                \$safe-area-offset-left: list.nth(\$values, 1);
                \$safe-area-offset-right: list.nth(\$values, 1);
            } @else if (length(\$values) == 2 or length(\$values) == 3) {
                \$safe-area-offset-left: list.nth(\$values, 2);
                \$safe-area-offset-right: list.nth(\$values, 2);
            } @else if (length(\$values) == 4) {
                \$safe-area-offset-left: list.nth(\$values, 4);
                \$safe-area-offset-right: list.nth(\$values, 2);
            }

            @include safe-area-property-left(#{\$property}-left, \$safe-area-offset-left, false);
            @include safe-area-property-right(#{\$property}-right, \$safe-area-offset-right, false);
        }
    } @else if (length(\$values) > 4) {
        @warn 'Incorrect arguments of mixin';
    }
}

// Set any property with left safe-area zone
@mixin safe-area-property-left(\$property, \$value: 0, \$default: true) {
    @if (\$property or \$value) {
        @if (\$default) {
            #{\$property}: #{\$value};
        }

        @if (\$enable-safe-area) {
            @if (strip-units(\$value) == 0) {
                /* iOS 11 */
                #{\$property}: constant(safe-area-inset-left);

                /* iOS 11.2+ */
                #{\$property}: env(safe-area-inset-left);
            } @else {
                /* iOS 11 */
                #{\$property}: calc(constant(safe-area-inset-left) + #{\$value});

                /* iOS 11.2+ */
                #{\$property}: calc(env(safe-area-inset-left) + #{\$value});
            }
        }
    }
}

// Set any property with right safe-area zone
@mixin safe-area-property-right(\$property, \$value: 0, \$default: true) {
    @if (\$property or \$value) {
        @if (\$default) {
            #{\$property}: #{\$value};
        }

        @if (\$enable-safe-area) {
            @if (strip-units(\$value) == 0) {
                /* iOS 11 */
                #{\$property}: constant(safe-area-inset-right);

                /* iOS 11.2+ */
                #{\$property}: env(safe-area-inset-right);
            } @else {
                /* iOS 11 */
                #{\$property}: calc(constant(safe-area-inset-right) + #{\$value});

                /* iOS 11.2+ */
                #{\$property}: calc(env(safe-area-inset-right) + #{\$value});
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/safe-area-offset.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/safe-area-offset.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/safe-area-offset.scss");
    }
}
