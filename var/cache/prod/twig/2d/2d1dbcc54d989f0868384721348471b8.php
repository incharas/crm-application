<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/widget/thread.html.twig */
class __TwigTemplate_a7fc528c2327b1e7f81cedd7b838ecf3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget-content\">
    ";
        // line 2
        if (( !array_key_exists("shortEmailThread", $context) && (twig_length_filter($this->env, ($context["thread"] ?? null)) >= 7))) {
            // line 3
            echo "        ";
            // line 4
            echo "        ";
            $context["shortEmailThread"] = true;
            // line 5
            echo "        ";
            $context["skippedEmails"] = [];
            // line 6
            echo "    ";
        }
        // line 7
        echo "    ";
        $context["threadViewOptions"] = ["view" => "oroemail/js/app/views/email-thread-view", "actionPanelSelector" => ".email-thread-action-panel"];
        // line 11
        echo "    ";
        $context["threadViewOptions"] = twig_array_merge(($context["threadViewOptions"] ?? null), ["isBaseView" =>  !($context["renderContexts"] ?? null)]);
        // line 12
        echo "    <div class=\"thread-view\"
         data-page-component-module=\"oroui/js/app/components/view-component\"
         data-page-component-options=\"";
        // line 14
        echo twig_escape_filter($this->env, json_encode(($context["threadViewOptions"] ?? null)), "html", null, true);
        echo "\"
         data-page-component-name=\"email-thread\"
         data-layout=\"separate\">
        ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(twig_reverse_filter($this->env, ($context["thread"] ?? null)));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["email"]) {
            // line 18
            echo "            ";
            if ((((array_key_exists("shortEmailThread", $context) && ($context["shortEmailThread"] ?? null)) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 18) > 2)) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 18) < (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "length", [], "any", false, false, false, 18) - 1)))) {
                // line 19
                echo "                ";
                $context["skippedEmails"] = twig_array_merge(($context["skippedEmails"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["email"], "id", [], "any", false, false, false, 19)]);
                // line 20
                echo "                ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 20) == (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "length", [], "any", false, false, false, 20) - 2))) {
                    // line 21
                    echo "                    <div class=\"load-more in-thread\" data-role=\"email-load-more\" data-emails-items=\"";
                    echo twig_escape_filter($this->env, json_encode(($context["skippedEmails"] ?? null)), "html", null, true);
                    echo "\">
                        <span class=\"load-more__label\">";
                    // line 22
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.load_more_emails", ["%quantity%" => twig_length_filter($this->env, ($context["skippedEmails"] ?? null))]), "html", null, true);
                    echo "</span>
                    </div>
                ";
                }
                // line 25
                echo "            ";
            } else {
                // line 26
                echo "                ";
                // line 27
                echo "                ";
                $context["emailCollapsed"] =  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 27);
                // line 28
                echo "                ";
                $this->loadTemplate("@OroEmail/Email/Thread/emailItem.html.twig", "@OroEmail/Email/widget/thread.html.twig", 28)->display($context);
                // line 29
                echo "            ";
            }
            // line 30
            echo "        ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['email'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 31
        echo "    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/widget/thread.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 31,  120 => 30,  117 => 29,  114 => 28,  111 => 27,  109 => 26,  106 => 25,  100 => 22,  95 => 21,  92 => 20,  89 => 19,  86 => 18,  69 => 17,  63 => 14,  59 => 12,  56 => 11,  53 => 7,  50 => 6,  47 => 5,  44 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/widget/thread.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/widget/thread.html.twig");
    }
}
