<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivityList/Segment/activityCondition.html.twig */
class __TwigTemplate_55cc134c0788a7641afae0ee3f528f42 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["activityConditionOptions"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(["filters" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 2
($context["params"] ?? null), "metadata", [], "any", false, true, false, 2), "filters", [], "any", true, true, false, 2)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "metadata", [], "any", false, true, false, 2), "filters", [], "any", false, false, false, 2), [])) : ([])), "fieldChoice" => ["select2" => ["formatSelectionTemplateSelector" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["params"] ?? null), "column_chain_template_selector", [], "any", true, true, false, 5)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "column_chain_template_selector", [], "any", false, false, false, 5), null)) : (null))]]], ((        // line 8
array_key_exists("activityConditionOptions", $context)) ? (_twig_default_filter(($context["activityConditionOptions"] ?? null), [])) : ([])));
        // line 9
        echo "
<li class=\"option\" data-criteria=\"condition-activity\"
    data-module=\"oroactivitylist/js/app/views/activity-condition-view\"
    data-options=\"";
        // line 12
        echo twig_escape_filter($this->env, json_encode(($context["activityConditionOptions"] ?? null)), "html", null, true);
        echo "\">
    ";
        // line 13
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.condition_builder.criteria.activity"), "html", null, true);
        echo "
</li>
";
    }

    public function getTemplateName()
    {
        return "@OroActivityList/Segment/activityCondition.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 13,  47 => 12,  42 => 9,  40 => 8,  39 => 5,  38 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivityList/Segment/activityCondition.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActivityListBundle/Resources/views/Segment/activityCondition.html.twig");
    }
}
