<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/widget/map-widget-module-name.js */
class __TwigTemplate_72d166e0a5a3e306579055da66903b68 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function() {
    'use strict';

    const moduleNameTemplate = 'oro/";
        // line 4
        echo twig_escape_filter($this->env, ($context["type"] ?? null), "js", null, true);
        echo "-widget';

    return function(type) {
        return moduleNameTemplate.replace('";
        // line 7
        echo twig_escape_filter($this->env, ($context["type"] ?? null), "js", null, true);
        echo "', type);
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/widget/map-widget-module-name.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  48 => 7,  42 => 4,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/widget/map-widget-module-name.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/widget/map-widget-module-name.js");
    }
}
