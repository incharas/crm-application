<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/layouts/default/imports/oro_workflow_transition_form/layout.html.twig */
class __TwigTemplate_336fd8d4a4d683bba4be4413669b32e4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_transition_form_wrapper_widget' => [$this, 'block__transition_form_wrapper_widget'],
            '_form_warning_message_widget' => [$this, 'block__form_warning_message_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_transition_form_wrapper_widget', $context, $blocks);
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('_form_warning_message_widget', $context, $blocks);
    }

    // line 1
    public function block__transition_form_wrapper_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["attr"] ?? null), "class", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 3), "")) : ("")) . " transition-form-widget")]);
        // line 5
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 6
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 10
    public function block__form_warning_message_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["attr"] ?? null), "class", [], "any", true, true, false, 12)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 12), "")) : ("")) . " transition-message alert")]);
        // line 14
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 15
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/layouts/default/imports/oro_workflow_transition_form/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  81 => 15,  76 => 14,  74 => 12,  72 => 11,  68 => 10,  61 => 6,  56 => 5,  54 => 3,  52 => 2,  48 => 1,  44 => 10,  41 => 9,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/layouts/default/imports/oro_workflow_transition_form/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/layouts/default/imports/oro_workflow_transition_form/layout.html.twig");
    }
}
