<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/reboot.scss */
class __TwigTemplate_50075ea66bdf84fe0b9c1b04e1b03cb3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import '~@oroinc/bootstrap/scss/reboot';

html {
    // 100% does not work, font is scaled by mobile browser anyway
    text-size-adjust: none;
}

body {
    text-align: left;
}

a {
    &:focus {
        outline: \$input-focus-border-color auto 1px;
    }
}

// Bad practices, should be removed in future
ul,
ol {
    padding: 0;
}

input {
    &::-ms-clear {
        display: none;
    }

    &[type='date'] {
        min-height: 32px;

        &::-webkit-inner-spin-button {
            display: none;
        }

        // Fix height Shadow DOM elements
        &::-webkit-date-and-time-value,
        &::-webkit-datetime-edit {
            line-height: 1;
        }
    }

    &[type='time'] {
        // Fix height Shadow DOM elements
        &::-webkit-date-and-time-value,
        &::-webkit-datetime-edit {
            line-height: 1;
        }
    }

    &[type='number'] {
        // fix FireFox platform styles
        box-shadow: none;
    }
}

select {
    /* Hide expand button of select in IE11
     * Remove when stop supporting IE11
     */
    &::-ms-expand {
        display: none;
    }
}

[type='search'] {
    appearance: textfield;
    outline-offset: -2px;
}

button {
    // default padding from Chrome
    padding: 1px 6px;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/reboot.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/reboot.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/reboot.scss");
    }
}
