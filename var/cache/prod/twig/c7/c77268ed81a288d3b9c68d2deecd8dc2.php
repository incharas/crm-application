<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Dashboard/tabbedWidget.html.twig */
class __TwigTemplate_b5ad40e48dec970217b8c990c92655c0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'tab_content' => [$this, 'block_tab_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/widget.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/widget.html.twig", "@OroDashboard/Dashboard/tabbedWidget.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Dashboard/tabbedWidget.html.twig", 4)->unwrap();
        // line 5
        echo "    <div class=\"tab-container\"
        ";
        // line 6
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/tabs-component"]], 6, $context, $this->getSourceContext());
        // line 8
        echo "
    >
        <ul class=\"nav nav-tabs\" role=\"tablist\">
            ";
        // line 11
        $context["tabTabId"] = uniqid("tab-");
        // line 12
        echo "            ";
        $context["tabContentId"] = uniqid("tab-content-");
        // line 13
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabs"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 14
            echo "                <li class=\"nav-item tab\">
                    <a id=\"";
            // line 15
            echo twig_escape_filter($this->env, (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 15) == 0)) ? (($context["tabTabId"] ?? null)) : ((($context["tabTabId"] ?? null) . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 15)))), "html", null, true);
            echo "\"
                       href=\"#";
            // line 16
            echo twig_escape_filter($this->env, ($context["tabContentId"] ?? null), "html", null, true);
            echo "\"
                       role=\"tab\"
                       class=\"no-hash tab-button nav-link ";
            // line 18
            if ((($context["activeTab"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "name", [], "any", false, false, false, 18))) {
                echo " active";
            }
            echo "\"
                       data-name=\"";
            // line 19
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "name", [], "any", false, false, false, 19), "html", null, true);
            echo "\"
                       data-url=\"";
            // line 20
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "url", [], "any", false, false, false, 20), "html", null, true);
            echo "\"
                       data-toggle=\"tab\"
                       aria-controls=\"";
            // line 22
            echo twig_escape_filter($this->env, ($context["tabContentId"] ?? null), "html", null, true);
            echo "\"
                       aria-selected=\"";
            // line 23
            if ((($context["activeTab"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "name", [], "any", false, false, false, 23))) {
                echo "true";
            } else {
                echo "false";
            }
            echo "\"
                    >
                        ";
            // line 25
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "label", [], "any", false, false, false, 25), "html", null, true);
            echo "
                        ";
            // line 26
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "afterHtml", [], "any", true, true, false, 26)) {
                // line 27
                echo "                            ";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["tab"], "afterHtml", [], "any", false, false, false, 27);
                echo "
                        ";
            }
            // line 29
            echo "                    </a>
                </li>
            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 32
        echo "        </ul>
        <div class=\"tab-content\" role=\"tabpanel\" aria-labelledby=\"";
        // line 33
        echo twig_escape_filter($this->env, ($context["tabTabId"] ?? null), "html", null, true);
        echo "\">
            <div class=\"content\">
                ";
        // line 35
        $this->displayBlock('tab_content', $context, $blocks);
        // line 38
        echo "            </div>
        </div>
    </div>

    ";
        // line 42
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Dashboard/tabbedWidget.html.twig", 42)->unwrap();
        // line 43
        echo "
    <div ";
        // line 44
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orodashboard/js/app/views/widget-tabs-view", "options" => ["_sourceElement" => ("#" .         // line 47
($context["widgetContentId"] ?? null))]]], 44, $context, $this->getSourceContext());
        // line 49
        echo "></div>

    ";
        // line 51
        $this->displayParentBlock("content", $context, $blocks);
        echo "
";
    }

    // line 35
    public function block_tab_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "                    ";
        echo ($context["activeTabContent"] ?? null);
        echo "
                ";
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Dashboard/tabbedWidget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  193 => 36,  189 => 35,  183 => 51,  179 => 49,  177 => 47,  176 => 44,  173 => 43,  171 => 42,  165 => 38,  163 => 35,  158 => 33,  155 => 32,  139 => 29,  133 => 27,  131 => 26,  127 => 25,  118 => 23,  114 => 22,  109 => 20,  105 => 19,  99 => 18,  94 => 16,  90 => 15,  87 => 14,  69 => 13,  66 => 12,  64 => 11,  59 => 8,  57 => 6,  54 => 5,  51 => 4,  47 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Dashboard/tabbedWidget.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Dashboard/tabbedWidget.html.twig");
    }
}
