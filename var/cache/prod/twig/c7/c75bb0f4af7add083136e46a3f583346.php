<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCron/Schedule/Datagrid/command.html.twig */
class __TwigTemplate_9fa524562ce6d23bacca1dc17fc9a9e5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<code>
    ";
        // line 2
        $context["argumentsStr"] = twig_join_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "arguments"], "method", false, false, false, 2), " ");
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "command"], "method", false, false, false, 3), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (((twig_length_filter($this->env, ($context["argumentsStr"] ?? null)) <= 30)) ? (($context["argumentsStr"] ?? null)) : ((twig_trim_filter(twig_slice($this->env, ($context["argumentsStr"] ?? null), 0, 30), null, "right") . "..."))), "html", null, true);
        echo "
</code>
";
    }

    public function getTemplateName()
    {
        return "@OroCron/Schedule/Datagrid/command.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCron/Schedule/Datagrid/command.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CronBundle/Resources/views/Schedule/Datagrid/command.html.twig");
    }
}
