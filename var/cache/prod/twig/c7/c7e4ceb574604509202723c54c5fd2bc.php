<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDigitalAsset/DigitalAsset/widget/choose.html.twig */
class __TwigTemplate_a4a610d425b123cd047eba3df155031e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'widget_content' => [$this, 'block_widget_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((array_key_exists("saved", $context) && ($context["saved"] ?? null))) {
            // line 2
            $context["widgetResponse"] = ["widget" => ["message" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.dialog.uploaded.message"), "messageOptions" => ["container" => (("[data-wid=" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 6
($context["app"] ?? null), "request", [], "any", false, false, false, 6), "get", [0 => "_wid"], "method", false, false, false, 6)) . "] .flash-messages")], "messageAfterPageChange" => false, "triggerSuccess" => false, "newDigitalAssetId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["digital_asset"] ?? null), "id", [], "any", false, false, false, 10), "gridName" =>             // line 11
($context["grid_name"] ?? null), "trigger" => [0 => ["eventBroker" => "mediator", "name" => ("datagrid:doInitialRefresh:" .             // line 15
($context["grid_name"] ?? null)), "args" => []], 1 => ["eventBroker" => "widget", "name" => "formReset", "args" => []]], "remove" => false]];
            // line 27
            echo json_encode(($context["widgetResponse"] ?? null));
        } else {
            // line 29
            $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroDigitalAsset/DigitalAsset/widget/choose.html.twig", 29)->unwrap();
            // line 30
            echo "
    ";
            // line 31
            $context["grid_render_params"] = ["enableViews" => true, "showViewsInNavbar" => false, "filtersStateElement" => "[data-role=\"filters-state-view-container\"]", "routerEnabled" => false, "jsmodules" => [0 => "orodatagrid/js/row-select-for-widget/builder"], "gridBuildersOptions" => ["rowSelectForWidget" => ["wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 39
($context["app"] ?? null), "request", [], "any", false, false, false, 39), "get", [0 => "_wid"], "method", false, false, false, 39), "multiSelect" => false]], "cssClass" => ("inner-grid " . ((            // line 43
($context["is_image_type"] ?? null)) ? ("digital-asset-image-grid") : ("digital-asset-file-grid")))];
            // line 45
            echo "
    <div class=\"widget-content\">";
            // line 47
            $this->displayBlock('widget_content', $context, $blocks);
            // line 109
            echo "    </div>
";
        }
    }

    // line 47
    public function block_widget_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "<div class=\"flash-messages\">
                <div class=\"flash-messages-frame\">
                    <div class=\"flash-messages-holder\"></div>
                </div>
            </div>";
        // line 54
        if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 54), "valid", [], "any", false, false, false, 54) && $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors'))) {
            // line 55
            echo "<div class=\"alert alert-error\" role=\"alert\">
                    <div class=\"message\">
                        ";
            // line 57
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                    </div>
                </div>";
        }
        // line 62
        $context["form_options"] = ["attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 64
($context["form"] ?? null), "vars", [], "any", false, false, false, 64), "id", [], "any", false, false, false, 64)], "action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 67
($context["app"] ?? null), "request", [], "any", false, false, false, 67), "attributes", [], "any", false, false, false, 67), "get", [0 => "_route"], "method", false, false, false, 67), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 68
($context["app"] ?? null), "request", [], "any", false, false, false, 68), "attributes", [], "any", false, false, false, 68), "get", [0 => "_route_params"], "method", false, false, false, 68))];
        // line 71
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ($context["form_options"] ?? null));
        // line 73
        echo "<div class=\"form-flex\">";
        // line 74
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "sourceFile", [], "any", false, false, false, 74), 'row', ["attr" => ["class" => "form-source-file widget-title-container"]]);
        // line 79
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "titles", [], "any", false, false, false, 79), 'row', ["attr" => ["class" => "form-titles widget-title-container"]]);
        // line 84
        echo "<div class=\"widget-actions form-buttons\">
                    <button class=\"btn\" type=\"reset\">";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.dialog.clear.label"), "html", null, true);
        echo "</button>
                    <button class=\"btn btn-primary\" type=\"submit\">";
        // line 86
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.dialog.upload.label"), "html", null, true);
        echo "</button>
                </div>
            </div>

            <div class=\"hide\">";
        // line 91
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        // line 92
        echo "</div>";
        // line 94
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
        // line 95
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        // line 97
        echo "<h4 class=\"scrollspy-title datagrid-title\">";
        // line 98
        if (($context["is_image_type"] ?? null)) {
            // line 99
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.dialog.datagrid_title.images"), "html", null, true);
        } else {
            // line 101
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.dialog.datagrid_title.files"), "html", null, true);
        }
        // line 103
        echo "</h4>
            <div class=\"scrollspy-nav-target\" aria-hidden=\"true\"></div>
            <div class=\"section-content\">
                ";
        // line 106
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["grid_name"] ?? null), ((array_key_exists("grid_params", $context)) ? (_twig_default_filter(($context["grid_params"] ?? null), [])) : ([])), ($context["grid_render_params"] ?? null)], 106, $context, $this->getSourceContext());
        echo "
            </div>
        ";
    }

    public function getTemplateName()
    {
        return "@OroDigitalAsset/DigitalAsset/widget/choose.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  141 => 106,  136 => 103,  133 => 101,  130 => 99,  128 => 98,  126 => 97,  124 => 95,  122 => 94,  120 => 92,  118 => 91,  111 => 86,  107 => 85,  104 => 84,  102 => 79,  100 => 74,  98 => 73,  96 => 71,  94 => 68,  93 => 67,  92 => 64,  91 => 62,  85 => 57,  81 => 55,  79 => 54,  73 => 48,  69 => 47,  63 => 109,  61 => 47,  58 => 45,  56 => 43,  55 => 39,  54 => 31,  51 => 30,  49 => 29,  46 => 27,  44 => 15,  43 => 11,  42 => 10,  41 => 6,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDigitalAsset/DigitalAsset/widget/choose.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DigitalAssetBundle/Resources/views/DigitalAsset/widget/choose.html.twig");
    }
}
