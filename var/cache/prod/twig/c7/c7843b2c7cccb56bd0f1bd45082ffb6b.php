<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/Datagrid/Column/attributeFamilies.html.twig */
class __TwigTemplate_cbb816a1999482c7e5ed1e47265a9c26 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/Datagrid/Column/attributeFamilies.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 4
        $context["attributeFamilies"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "attributeFamiliesViewData"], "method", false, false, false, 4);
        // line 5
        if ( !twig_test_empty(($context["attributeFamilies"] ?? null))) {
            // line 6
            echo "<ul class=\"extra-list\">
";
            // line 7
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attributeFamilies"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attributeFamily"]) {
                // line 8
                echo "    <li class=\"extra-list-element\">
    ";
                // line 9
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attributeFamily"], "viewLink", [], "any", false, false, false, 9)) {
                    // line 10
                    echo "        ";
                    echo twig_call_macro($macros["UI"], "macro_renderUrl", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attributeFamily"], "viewLink", [], "any", false, false, false, 10), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attributeFamily"], "label", [], "any", false, false, false, 10)], 10, $context, $this->getSourceContext());
                    echo "
    ";
                } else {
                    // line 12
                    echo "        ";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attributeFamily"], "label", [], "any", false, false, false, 12), "html", null, true);
                    echo "
    ";
                }
                // line 14
                echo "    </li>
";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attributeFamily'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 16
            echo "</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/Datagrid/Column/attributeFamilies.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  77 => 16,  70 => 14,  64 => 12,  58 => 10,  56 => 9,  53 => 8,  49 => 7,  46 => 6,  44 => 5,  42 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/Datagrid/Column/attributeFamilies.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/Datagrid/Column/attributeFamilies.html.twig");
    }
}
