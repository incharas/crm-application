<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAnalytics/Form/form.html.twig */
class __TwigTemplate_3d0e294738dc83ea831b2cb4e1c4e35e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((((array_key_exists("form", $context) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recency", [], "any", true, true, false, 1)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "frequency", [], "any", true, true, false, 1)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "monetary", [], "any", true, true, false, 1))) {
            // line 2
            echo "    ";
            $context["options"] = ["rfm_enable_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "rfm_enabled", [], "any", false, false, false, 2), "vars", [], "any", false, false, false, 2), "id", [], "any", false, false, false, 2)];
            // line 3
            echo "    ";
            $context["rfmEnabled"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 3), "value", [], "any", false, true, false, 3), "data", [], "any", false, true, false, 3), "rfm_enabled", [], "any", true, true, false, 3) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 3), "value", [], "any", false, false, false, 3), "data", [], "any", false, false, false, 3), "rfm_enabled", [], "any", false, false, false, 3));
            // line 4
            echo "    <div class=\"rfm-settings ";
            if (($context["rfmEnabled"] ?? null)) {
                echo "rfm-enabled";
            }
            echo "\"
        data-page-component-module=\"oroanalytics/js/app/components/rfm-settings\"
        data-page-component-options=\"";
            // line 6
            echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
            echo "\">

        <h4>";
            // line 8
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.rfm_setting.title"), "html", null, true);
            echo "</h4>

        ";
            // line 10
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "rfm_enabled", [], "any", false, false, false, 10), 'row');
            echo "

        <div class=\"control-group rfm-settings-data\">
            <div class=\"controls\">
                <div class=\"hide\">
                    ";
            // line 15
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recency", [], "any", true, true, false, 15)) {
                // line 16
                echo "                        ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recency", [], "any", false, false, false, 16), 'widget');
                echo "
                    ";
            }
            // line 18
            echo "                    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "frequency", [], "any", true, true, false, 18)) {
                // line 19
                echo "                        ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "frequency", [], "any", false, false, false, 19), 'widget');
                echo "
                    ";
            }
            // line 21
            echo "                    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "monetary", [], "any", true, true, false, 21)) {
                // line 22
                echo "                        ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "monetary", [], "any", false, false, false, 22), 'widget');
                echo "
                    ";
            }
            // line 24
            echo "                </div>

                ";
            // line 26
            if ((((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recency", [], "any", false, false, false, 26), "vars", [], "any", false, false, false, 26), "errors", [], "any", false, false, false, 26)) > 0) || (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "frequency", [], "any", false, false, false, 26), "vars", [], "any", false, false, false, 26), "errors", [], "any", false, false, false, 26)) > 0)) || (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "monetary", [], "any", false, false, false, 26), "vars", [], "any", false, false, false, 26), "errors", [], "any", false, false, false, 26)) > 0))) {
                // line 27
                echo "                    <div class=\"alert alert-error\" role=\"alert\">
                        <ul>
                            ";
                // line 29
                if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recency", [], "any", false, false, false, 29), "vars", [], "any", false, false, false, 29), "errors", [], "any", false, false, false, 29)) > 0)) {
                    // line 30
                    echo "                                <li>
                                    <strong>";
                    // line 31
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.recency.label"), "html", null, true);
                    echo "</strong>
                                    ";
                    // line 32
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recency", [], "any", false, false, false, 32), 'errors');
                    echo "
                                </li>
                            ";
                }
                // line 35
                echo "
                            ";
                // line 36
                if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "frequency", [], "any", false, false, false, 36), "vars", [], "any", false, false, false, 36), "errors", [], "any", false, false, false, 36)) > 0)) {
                    // line 37
                    echo "                                <li>
                                    <strong>";
                    // line 38
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.frequency.label"), "html", null, true);
                    echo "</strong>
                                    ";
                    // line 39
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "frequency", [], "any", false, false, false, 39), 'errors');
                    echo "
                                </li>
                            ";
                }
                // line 42
                echo "
                            ";
                // line 43
                if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "monetary", [], "any", false, false, false, 43), "vars", [], "any", false, false, false, 43), "errors", [], "any", false, false, false, 43)) > 0)) {
                    // line 44
                    echo "                                <li>
                                    <strong>";
                    // line 45
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.monetary.label"), "html", null, true);
                    echo "</strong>
                                    ";
                    // line 46
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "monetary", [], "any", false, false, false, 46), 'errors');
                    echo "
                                </li>
                            ";
                }
                // line 49
                echo "                        </ul>
                    </div>
                ";
            }
            // line 52
            echo "
                <table class=\"grid grid-main-container table-hover table table-bordered table-condensed\">
                    <thead>
                    <tr>
                        <th><label>";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.score.title"), "html", null, true);
            echo "</label></th>
                        <th>
                            ";
            // line 58
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recency", [], "any", false, false, false, 58), 'label');
            echo "
                            ";
            // line 59
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.hint.recency"), "html", null, true);
            echo "
                        </th>
                        <th>
                            ";
            // line 62
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "frequency", [], "any", false, false, false, 62), 'label');
            echo "
                            ";
            // line 63
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.hint.frequency"), "html", null, true);
            echo "
                        </th>
                        <th>
                            ";
            // line 66
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "monetary", [], "any", false, false, false, 66), 'label');
            echo "
                            ";
            // line 67
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.hint.monetary"), "html", null, true);
            echo "
                        </th>
                        <th class=\"action-column\"><label>";
            // line 69
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.form.actions.title"), "html", null, true);
            echo "</label></th>
                    </tr>
                    </thead>
                    <tbody></tbody>
                </table>

                <button type=\"button\" class=\"btn action-add\">
                    ";
            // line 76
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.title.add_row"), "html", null, true);
            echo "
                </button>
            </div>
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroAnalytics/Form/form.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 76,  203 => 69,  198 => 67,  194 => 66,  188 => 63,  184 => 62,  178 => 59,  174 => 58,  169 => 56,  163 => 52,  158 => 49,  152 => 46,  148 => 45,  145 => 44,  143 => 43,  140 => 42,  134 => 39,  130 => 38,  127 => 37,  125 => 36,  122 => 35,  116 => 32,  112 => 31,  109 => 30,  107 => 29,  103 => 27,  101 => 26,  97 => 24,  91 => 22,  88 => 21,  82 => 19,  79 => 18,  73 => 16,  71 => 15,  63 => 10,  58 => 8,  53 => 6,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAnalytics/Form/form.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AnalyticsBundle/Resources/views/Form/form.html.twig");
    }
}
