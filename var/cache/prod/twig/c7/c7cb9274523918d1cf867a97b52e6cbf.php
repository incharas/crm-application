<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Customer/opportunitiesGrid.html.twig */
class __TwigTemplate_e46882773909553ee829eaabc4c1cbe7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroSales/Customer/opportunitiesGrid.html.twig", 1)->unwrap();
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Customer/opportunitiesGrid.html.twig", 2)->unwrap();
        // line 3
        echo "
<div class=\"widget-content\">

    ";
        // line 6
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_sales_opportunity_create")) {
            // line 7
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_renderButtonsRow", [twig_call_macro($macros["UI"], "macro_quickAccessAddButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_customer_aware_create", ["targetClass" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["gridParams"] ?? null), "customer_class", [], "any", false, false, false, 9), "targetId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["gridParams"] ?? null), "customer_id", [], "any", false, false, false, 10)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.customer.quick_access_button.new_opportunity.label"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.customer.quick_access_button.new_opportunity.label"), "widget" => ["reload-grid-name" => "sales-customers-opportunities-grid", "options" => ["alias" => "oro_sales_opportunity_customer_aware_create_dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.customer.quick_access_button.create_opportunity.widget.title")]]]]], 7, $context, $this->getSourceContext())], 7, $context, $this->getSourceContext());
            // line 23
            echo "
    ";
        }
        // line 25
        echo "
    ";
        // line 26
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["sales-customers-opportunities-grid", ($context["gridParams"] ?? null), ["cssClass" => "inner-grid"]], 26, $context, $this->getSourceContext());
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Customer/opportunitiesGrid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 26,  57 => 25,  53 => 23,  51 => 10,  50 => 9,  48 => 7,  46 => 6,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Customer/opportunitiesGrid.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Customer/opportunitiesGrid.html.twig");
    }
}
