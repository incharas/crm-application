<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/repeated.js */
class __TwigTemplate_0f8d099d130582b9f5256a9ad0334a39 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const __ = require('orotranslation/js/translator');
    const defaultParam = {
        invalid_message: 'This value is not valid.',
        invalid_message_parameters: {}
    };

    /**
     * @export oroform/js/validator/repeated
     */
    return [
        'Repeated',
        function(value, element, params) {
            // validator should be added to repeated field (second one)
            const attr = element.hasAttribute('data-ftid') ? 'data-ftid' : 'id';
            const id = element.getAttribute(attr);
            const secondName = params.second_name || '';
            const firstId = id.slice(0, -secondName.length) + (params.first_name || '');
            const firstElement = document.querySelector('[' + attr + '=\"' + firstId + '\"]');
            return firstElement && (this.optional(firstElement) || value === this.elementValue(firstElement));
        },
        function(param) {
            param = Object.assign({}, defaultParam, param);
            return __(param.invalid_message, param.invalid_message_parameters);
        }
    ];
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/repeated.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/repeated.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/repeated.js");
    }
}
