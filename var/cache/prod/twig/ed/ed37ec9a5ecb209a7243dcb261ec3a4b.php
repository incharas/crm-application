<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/widget-picker/widget-picker-item-view.html */
class __TwigTemplate_a11e6a32433b97db48871debe811ade4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<summary class=\"widget-picker__summary-row\">
    <span class=\"widget-picker__toggler-column\">
        <% if (description) { %>
            <button class=\"btn btn-icon btn-light-custom widget-picker__description-toggler\"
                data-role=\"description-toggler\">
                <span class=\"fa-icon collapse-action-icon\" aria-hidden=\"true\"></span>
            </button>
        <% } %>
    </span>
    <span class=\"widget-picker__icon-column\">
        <% if (iconClass) { %>
            <i class=\"<%- iconClass %> widget-picker__icon\" aria-hidden=\"true\"></i>
            <% } else { %>
                <img src=\"<%- dialogIcon %>\" class=\"widget-picker__img\" alt=\"<%- title %>\" title=\"<%- title %>\" />
            <% } %>
    </span>
    <span class=\"widget-picker__info-column\">
        <span class=\"widget-picker__title-cell\">
            <em class=\"widget-picker__title-text\"><%- title %></em>
            <% if (isNew) { %>
            <span class=\"widget-picker__new-badge\"><%- _.__('oro.ui.widget_picker.new') %></span>
            <% } %>
            <span class=\"widget-picker__added-badge<% if (!added) { %> hide <% } %>\" 
                data-role=\"added-badge\"><%- _.__('oro.ui.widget_picker.added') %>
                    <span data-role=\"added-count\">(<%- added %>)</span>
            </span>
        </span>
    </span>
    <span class=\"widget-picker__actions-column\">
        <button class=\"btn btn-primary widget-picker__add-action\" data-role=\"add-action\">
            <%- _.__('oro.ui.widget_picker.add') %>
        </button>
    </span>
</summary>
<% if (description) { %>
    <p class=\"widget-picker__description-cell\" data-role=\"description\">
        <%- description %>
    </p>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/widget-picker/widget-picker-item-view.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/widget-picker/widget-picker-item-view.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/widget-picker/widget-picker-item-view.html");
    }
}
