<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/dialog/update.html.twig */
class __TwigTemplate_6b5cdd7cd64a1695216907f8fd2c48d8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroOAuth2Server/Client/dialog/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "valid", [], "any", false, false, false, 4) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "errors", [], "any", false, false, false, 4)))) {
            // line 5
            echo "        <div class=\"alert alert-error\" role=\"alert\">
            <div class=\"message\">
                ";
            // line 7
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
            </div>
        </div>
    ";
        }
        // line 11
        echo "    <fieldset class=\"form form-horizontal\">
        ";
        // line 12
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "identifier", [], "any", false, false, false, 12), 'row');
        echo "
        ";
        // line 13
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 13), 'row');
        echo "
        ";
        // line 14
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "active", [], "any", false, false, false, 14), 'row');
        echo "
        ";
        // line 15
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", true, true, false, 15)) {
            // line 16
            echo "            ";
            if (twig_in_filter("hidden", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 16), "vars", [], "any", false, false, false, 16), "block_prefixes", [], "any", false, false, false, 16))) {
                // line 17
                echo "                <div data-validation-ignore=\"true\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 17), 'row');
                echo "</div>
            ";
            } else {
                // line 19
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 19), 'row', ["group_attr" => ["class" => "client-grants"]]);
                echo "
            ";
            }
            // line 21
            echo "        ";
        }
        // line 22
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
        <div class=\"widget-actions form-actions\">
            <button class=\"btn\" type=\"reset\">";
        // line 24
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
            <button class=\"btn btn-primary\" type=\"submit\">";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "html", null, true);
        echo "</button>
        </div>
    </fieldset>
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/dialog/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 25,  105 => 24,  99 => 22,  96 => 21,  90 => 19,  84 => 17,  81 => 16,  79 => 15,  75 => 14,  71 => 13,  67 => 12,  64 => 11,  57 => 7,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/dialog/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/dialog/update.html.twig");
    }
}
