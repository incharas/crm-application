<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDraft/Draft/draftConfirmation.html.twig */
class __TwigTemplate_4af51e18f6fa077377dccf8864211b9a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((twig_length_filter($this->env, ($context["draftableFields"] ?? null)) > 0)) {
            // line 2
            echo "    ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.draft.operations.create.confirmation.body_title"), "html", null, true);
            echo "
    <ul>";
            // line 4
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["draftableFields"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
                // line 5
                echo "<li>";
                echo twig_escape_filter($this->env, $context["field"], "html", null, true);
                echo "</li>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 7
            echo "</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroDraft/Draft/draftConfirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 7,  48 => 5,  44 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDraft/Draft/draftConfirmation.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DraftBundle/Resources/views/Draft/draftConfirmation.html.twig");
    }
}
