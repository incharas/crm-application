<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/popover.scss */
class __TwigTemplate_532cdca886a02b2a2effb0f3d89904b5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

/* rtl:begin:ignore */

@import '~@oroinc/bootstrap/scss/popover';

.popover {
    box-shadow: \$popover-box-shadow;

    .arrow {
        z-index: \$popover-arrow-z-index;

        &::before {
            content: none;
        }

        &::after {
            background-color: \$popover-arrow-color;
            width: \$popover-arrow-size;
            height: \$popover-arrow-size;
            display: block;
            transform: rotate(45deg);
            border: none;
        }
    }

    &.bs-popover-top {
        .arrow {
            &::after {
                bottom: \$popover-arrow-offset;
                box-shadow: \$popover-arrow-top-shadow;
            }
        }
    }

    &.bs-popover-bottom {
        .arrow {
            &::after {
                top: \$popover-arrow-offset;
                box-shadow: \$popover-arrow-bottom-shadow;
            }
        }
    }

    &.bs-popover-left {
        .arrow {
            &::after {
                right: \$popover-arrow-offset;
                box-shadow: \$popover-arrow-left-shadow;
            }
        }
    }

    &.bs-popover-right {
        .arrow {
            &::after {
                left: \$popover-arrow-offset;
                box-shadow: \$popover-arrow-right-shadow;
            }
        }
    }

    .oro-popover-content {
        overflow-y: auto;
        height: 100%;
    }

    &-header {
        padding: 8px 14px;
        margin: 0;
        font-size: 14px;
        font-weight: font-weight('light');
        line-height: 18px;
        background-color: \$primary-900;
        border-bottom: 1px solid \$additional-ultra-light;
        border-radius: 5px 5px 0 0;
    }
}

/* rtl:end:ignore */

.popover-body {
    background-color: \$popover-body-bg;
    padding-right: \$popover-body-offset-right;

    &.popover-no-close-button {
        padding-right: \$popover-body-offset-right-no-close;
    }
}

.popover-close {
    position: \$popover-close-position;
    top: \$popover-close-position-y;
    right: \$popover-close-position-x;
    cursor: \$popover-close-cursor;
    color: \$popover-close-color;

    &::before {
        font-size: \$popover-close-before-font-size;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/popover.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/popover.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/popover.scss");
    }
}
