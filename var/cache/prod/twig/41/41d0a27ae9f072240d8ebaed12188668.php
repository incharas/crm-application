<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/type.js */
class __TwigTemplate_67faef5384182af59e8a7a784f715009 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const __ = require('orotranslation/js/translator');
    const numberFormatter = require('orolocale/js/formatter/number');
    const defaultParam = {
        message: 'This value should be of type ";
        // line 7
        echo twig_escape_filter($this->env, ($context["type"] ?? null), "js", null, true);
        echo ".'
    };

    /**
     * @export oroform/js/validator/type
     */
    return [
        'Type',
        function(value, element, param) {
            let result;
            switch (param.type) {
                case 'integer':
                    const valueNumber = Number(value).toFixed();
                    result = String(valueNumber) === value;
                    break;
                case 'float':
                case 'numeric':
                    result = !isNaN(numberFormatter.unformatStrict(value));
                    break;
                default:
                    result = true;
            }
            return this.optional(element) || result;
        },
        function(param) {
            param = Object.assign({}, defaultParam, param);
            return __(param.message, {type: param.type});
        }
    ];
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/type.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/type.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/type.js");
    }
}
