<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReminder/Reminder/showScript.html.twig */
class __TwigTemplate_f9ca3ab2bcee53dacfc1496efb62448b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1)) {
            // line 2
            echo "    ";
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroReminder/Reminder/showScript.html.twig", 2)->unwrap();
            // line 3
            echo "
    <div ";
            // line 4
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "ororeminder/js/app/components/reminder-messages-publisher", "options" => ["reminders" => $this->extensions['Oro\Bundle\ReminderBundle\Twig\ReminderExtension']->getRequestedRemindersData()]]], 4, $context, $this->getSourceContext());
            // line 9
            echo "></div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroReminder/Reminder/showScript.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 9,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReminder/Reminder/showScript.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ReminderBundle/Resources/views/Reminder/showScript.html.twig");
    }
}
