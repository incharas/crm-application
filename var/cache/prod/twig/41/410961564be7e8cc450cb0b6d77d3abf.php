<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/api-accessor-unload-messages-group.js */
class __TwigTemplate_a0ec9a6a7f6a2d319b9a3df4d65e165c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';
    const UnloadMessagesGroup = require('./unload-messages-group');
    const apiAccessorUnloadMessagesGroup = new UnloadMessagesGroup({});
    return apiAccessorUnloadMessagesGroup;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/api-accessor-unload-messages-group.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/api-accessor-unload-messages-group.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/api-accessor-unload-messages-group.js");
    }
}
