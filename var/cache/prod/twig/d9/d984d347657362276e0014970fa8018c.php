<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/Form/fields.html.twig */
class __TwigTemplate_c4f933af703c20ce1426283729da6af0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_tag_select_row' => [$this, 'block_oro_tag_select_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_tag_select_row', $context, $blocks);
    }

    public function block_oro_tag_select_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_tag_view")) {
            // line 3
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_tag_assign_unassign")) {
                // line 4
                echo "            <div class=\"control-group";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 4)) {
                    echo " ";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 4), "html", null, true);
                }
                echo "\">
                <div class=\"control-label wrap\">
                    ";
                // line 6
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["label_attr" => ($context["label_attr"] ?? null)]);
                echo "
                </div>
                <div class=\"controls tags-form-select-editor\">
                    ";
                // line 9
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
                echo "
                    ";
                // line 10
                echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderJavascript(($context["form"] ?? null));
                echo "
                </div>
            </div>
        ";
            }
            // line 14
            echo "    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroTag/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  77 => 14,  70 => 10,  66 => 9,  60 => 6,  51 => 4,  48 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/Form/fields.html.twig");
    }
}
