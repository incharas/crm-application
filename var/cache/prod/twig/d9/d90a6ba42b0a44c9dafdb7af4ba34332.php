<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/jquery-widget-component.js */
class __TwigTemplate_b55dc4580d3912a0203391d924c70b98 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const \$ = require('jquery');
    const loadModules = require('oroui/js/app/services/load-modules');
    const BaseComponent = require('oroui/js/app/components/base/component');

    /**
     * Initializes jquery widget on _sourceElement
     */
    const JqueryWidgetComponent = BaseComponent.extend({
        \$el: null,

        widgetName: null,

        /**
         * @inheritdoc
         */
        constructor: function JqueryWidgetComponent(options) {
            JqueryWidgetComponent.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this.\$el = options._sourceElement;
            const subPromises = _.values(options._subPromises);
            const widgetOptions = _.omit(options, ['_sourceElement', '_subPromises', 'widgetModule', 'widgetName']);
            const initializeJqueryWidget = widgetName => {
                widgetName = _.isString(widgetName) ? widgetName : '';
                this.widgetName = widgetName || options.widgetName;
                this.\$el[this.widgetName](widgetOptions);
                this._resolveDeferredInit();
            };

            this._deferredInit();
            if (subPromises.length) {
                // ensure that all nested components are already initialized
                \$.when(...subPromises).then(function() {
                    loadModules(options.widgetModule, initializeJqueryWidget);
                });
            } else {
                loadModules(options.widgetModule, initializeJqueryWidget);
            }
        },

        /**
         * @inheritdoc
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }
            if (this.\$el[this.widgetName]('instance')) {
                this.\$el[this.widgetName]('destroy');
            }
            return JqueryWidgetComponent.__super__.dispose.call(this);
        }
    });

    return JqueryWidgetComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/jquery-widget-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/jquery-widget-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/jquery-widget-component.js");
    }
}
