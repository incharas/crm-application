<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/page/content-view.js */
class __TwigTemplate_eee06566e91ca2c65ae728ca6407ecbc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const tools = require('oroui/js/tools');
    const PageRegionView = require('./../base/page-region-view');

    /**
     * Finds first container that has active scrollbar and sets focus on it for ability of scrolling it by keyboard
     */
    function focusScrollElement() {
        const scrollable = [
            '.scrollable-container',
            '.other-scroll',
            '.layout-content .scrollable-container',
            '.system-configuration-container .scrollable-container',
            '.scrollspy'
        ];

        const target = _.find(scrollable, function(item) {
            const \$el = \$(item).first();
            const overflow = \$el.css('overflow-y');
            return \$el.length && /auto|scroll/.test(overflow) && \$el[0].scrollHeight > \$el[0].clientHeight;
        });

        if (!_.isUndefined(target)) {
            \$(target).attr({
                'tabindex': 0,
                'data-scroll-focus': ''
            }).one('blur', function() {
                \$(this).removeAttr('data-scroll-focus tabindex');
            }).focus();
        }
    }

    const PageContentView = PageRegionView.extend({
        /**
         * @inheritdoc
         */
        constructor: function PageContentView(options) {
            PageContentView.__super__.constructor.call(this, options);
        },

        template: function(data) {
            return data.content;
        },
        pageItems: ['content', 'scripts'],

        listen: {
            'page:afterChange mediator': 'onPageAfterChange'
        },

        render: function() {
            PageContentView.__super__.render.call(this);

            const data = this.getTemplateData();

            if (data && data.scripts) {
                this.\$el.append(data.scripts);
            }

            if (data) {
                this.initLayout();
            }

            return this;
        },

        /**
         * Handles page:afterChange event
         */
        onPageAfterChange: function() {
            // should not be applied before layouting (see init-layout.js)
            // that will give issues on extra small screens
            _.defer(this.initFocus.bind(this));

            // force to redraw page header to avoid wrong width
            this.\$('.page-title:first').hide().show(0);
        },

        /**
         * Sets focus on first form field in case active element
         * is not active on purpose (autofocus attribute)
         */
        initFocus: function() {
            const activeElement = document.activeElement;

            if (!\$(activeElement).is('body') || tools.isTouchDevice() || \$(activeElement).is('[autofocus]')) {
                return;
            }

            const \$form = this.\$('form:first');

            if (\$form.length) {
                \$form.focusFirstInput();
            } else {
                _.delay(focusScrollElement, 200);
            }
        }
    });

    return PageContentView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/page/content-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/page/content-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/page/content-view.js");
    }
}
