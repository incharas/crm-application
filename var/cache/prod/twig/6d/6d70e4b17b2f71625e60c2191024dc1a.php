<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/create.html.twig */
class __TwigTemplate_2b6e4fd55666a766c3e67e97fe0dbfc7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/create.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.entity_label")]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroOAuth2Server/Client/create.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $this->displayParentBlock("navButtons", $context, $blocks);
        echo "

    ";
        // line 13
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "data", [], "any", false, false, false, 13), "frontend", [], "any", false, false, false, 13)) ? ("oro_oauth2_frontend_index") : ("oro_oauth2_index")))], 13, $context, $this->getSourceContext());
        echo "

    ";
        // line 15
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 16
($context["form"] ?? null), "vars", [], "any", false, false, false, 16), "data", [], "any", false, false, false, 16), "frontend", [], "any", false, false, false, 16)) ? ("oro_oauth2_frontend_view") : ("oro_oauth2_view")), "params" => ["id" => "\$id"]]], 15, $context, $this->getSourceContext());
        // line 19
        echo "
    ";
        // line 20
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 20, $context, $this->getSourceContext());
        echo "
";
    }

    // line 23
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    ";
        $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 25
($context["form"] ?? null), "vars", [], "any", false, false, false, 25), "data", [], "any", false, false, false, 25), "frontend", [], "any", false, false, false, 25)) ? ("oro.oauth2server.client.entity_frontend_label") : ("oro.oauth2server.client.entity_label")))]);
        // line 30
        echo "    ";
        $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroOAuth2Server/Client/create.html.twig", 30)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
    }

    // line 33
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    ";
        $context["id"] = "oauth-application";
        // line 35
        echo "
    ";
        // line 36
        ob_start(function () { return ''; });
        // line 37
        echo "        ";
        if (twig_in_filter("hidden", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 37), "vars", [], "any", false, false, false, 37), "block_prefixes", [], "any", false, false, false, 37))) {
            // line 38
            echo "            <div data-validation-ignore=\"true\">";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 38), 'row');
            echo "</div>
        ";
        } else {
            // line 40
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 40), 'row', ["group_attr" => ["class" => "client-grants"]]);
            echo "
        ";
        }
        // line 42
        echo "    ";
        $context["grantsRow"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 43
        echo "
    ";
        // line 44
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" => [0 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 50
($context["form"] ?? null), "organization", [], "any", true, true, false, 50)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", false, false, false, 50), 'row')) : ("")), 1 =>         // line 51
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 51), 'row'), 2 =>         // line 52
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "active", [], "any", false, false, false, 52), 'row'), 3 =>         // line 53
($context["grantsRow"] ?? null), 4 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 54
($context["form"] ?? null), "redirectUris", [], "any", true, true, false, 54)) ? (        $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "redirectUris", [], "any", false, false, false, 54), 'form_row_collection', ["group_attr" => ["id" => "client-redirect-uris", "class" => "hide"]])) : ("")), 5 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 55
($context["form"] ?? null), "owner", [], "any", true, true, false, 55)) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "owner", [], "any", false, false, false, 55), 'row', ["group_attr" => ["id" => "client-owner", "class" => "hide"]])) : (""))]]]]];
        // line 60
        echo "
    ";
        // line 61
        $context["data"] = ["formErrors" => ((        // line 62
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 63
($context["dataBlocks"] ?? null)];
        // line 65
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "

    ";
        // line 67
        $context["options"] = ["ownerField" => "#client-owner", "redirectUrisField" => "#client-redirect-uris", "_sourceElement" => ".client-grants input:radio"];
        // line 72
        echo "    <div ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orooauth2server/js/views/client-grant-view", "options" =>         // line 74
($context["options"] ?? null)]], 72, $context, $this->getSourceContext());
        // line 75
        echo "></div>
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  153 => 75,  151 => 74,  149 => 72,  147 => 67,  141 => 65,  139 => 63,  138 => 62,  137 => 61,  134 => 60,  132 => 55,  131 => 54,  130 => 53,  129 => 52,  128 => 51,  127 => 50,  126 => 44,  123 => 43,  120 => 42,  114 => 40,  108 => 38,  105 => 37,  103 => 36,  100 => 35,  97 => 34,  93 => 33,  88 => 30,  86 => 25,  84 => 24,  80 => 23,  74 => 20,  71 => 19,  69 => 16,  68 => 15,  63 => 13,  57 => 11,  53 => 10,  48 => 1,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/create.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/create.html.twig");
    }
}
