<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/Datagrid/Property/contacts.html.twig */
class __TwigTemplate_0c57c7fad25ca360394cd1c9c5844a0b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroEmail/Email/Datagrid/Property/contacts.html.twig", 1)->unwrap();
        // line 2
        $macros["emailContacts"] = $this->macros["emailContacts"] = $this;
        // line 3
        echo "
";
        // line 4
        $context["isNew"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "is_new"], "method", false, false, false, 4);
        // line 5
        echo "
<span class=\"nowrap\">
    <span class=\"icon grid\">
        <i class=\"";
        // line 8
        echo ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "incoming"], "method", false, false, false, 8)) ? ("fa-sign-in") : ("fa-sign-out"));
        echo "\"></i>
    </span>
    ";
        // line 10
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "incoming"], "method", false, false, false, 10)) {
            // line 11
            echo "        ";
            echo twig_call_macro($macros["emailContacts"], "macro_renderEmailAddressCell", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "fromName"], "method", false, false, false, 11), ($context["isNew"] ?? null), 22], 11, $context, $this->getSourceContext());
            echo "
    ";
        } else {
            // line 13
            echo "        ";
            $context["recipients"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "recipients"], "method", false, false, false, 13);
            // line 14
            echo "        ";
            if ((twig_length_filter($this->env, ($context["recipients"] ?? null)) > 0)) {
                // line 15
                echo "            ";
                if ((twig_length_filter($this->env, ($context["recipients"] ?? null)) < 3)) {
                    // line 16
                    echo "                ";
                    echo twig_call_macro($macros["emailContacts"], "macro_renderEmailAddressCell", [twig_call_macro($macros["EA"], "macro_email_participants_name", [($context["recipients"] ?? null), true, false], 16, $context, $this->getSourceContext()), ($context["isNew"] ?? null)], 16, $context, $this->getSourceContext());
                    echo "
            ";
                } else {
                    // line 18
                    echo "                ";
                    $context["firstEmail"] = twig_first($this->env, ($context["recipients"] ?? null));
                    // line 19
                    echo "                ";
                    $context["lastEmail"] = twig_last($this->env, ($context["recipients"] ?? null));
                    // line 20
                    echo "                ";
                    $context["firstLastRecipients"] = ((twig_call_macro($macros["EA"], "macro_email_participant_name_or_me", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 21
($context["firstEmail"] ?? null), "emailAddress", [], "any", false, false, false, 21), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["firstEmail"] ?? null), "name", [], "any", false, false, false, 21), true, false], 21, $context, $this->getSourceContext()) . " .. ") . twig_call_macro($macros["EA"], "macro_email_participant_name_or_me", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 23
($context["lastEmail"] ?? null), "emailAddress", [], "any", false, false, false, 23), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["lastEmail"] ?? null), "name", [], "any", false, false, false, 23), true, false], 23, $context, $this->getSourceContext()));
                    // line 25
                    echo "                ";
                    echo twig_call_macro($macros["emailContacts"], "macro_renderEmailAddressCell", [($context["firstLastRecipients"] ?? null), ($context["isNew"] ?? null)], 25, $context, $this->getSourceContext());
                    echo "
            ";
                }
                // line 27
                echo "        ";
            }
            // line 28
            echo "    ";
        }
        // line 29
        echo "    ";
        if ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_email.threads_grouping")) {
            // line 30
            echo "        ";
            $context["threadEmailCount"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "thread_email_count"], "method", false, false, false, 30);
            // line 31
            echo "        ";
            if ((($context["threadEmailCount"] ?? null) > 1)) {
                // line 32
                echo "            ";
                echo twig_call_macro($macros["emailContacts"], "macro_renderEmailAddressCell", [(("(" . ($context["threadEmailCount"] ?? null)) . ")"), ($context["isNew"] ?? null)], 32, $context, $this->getSourceContext());
                echo "
        ";
            }
            // line 34
            echo "    ";
        }
        // line 35
        echo "</span>

";
    }

    // line 37
    public function macro_renderEmailAddressCell($__label__ = null, $__isNew__ = null, $__maxLength__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "label" => $__label__,
            "isNew" => $__isNew__,
            "maxLength" => $__maxLength__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 38
            echo "    ";
            if ((($context["maxLength"] ?? null) && (twig_length_filter($this->env, ($context["label"] ?? null)) > ($context["maxLength"] ?? null)))) {
                // line 39
                echo "        ";
                list($context["title"], $context["label"]) =                 [($context["label"] ?? null), (twig_trim_filter(twig_slice($this->env, ($context["label"] ?? null), 0, ($context["maxLength"] ?? null)), null, "right") . "...")];
                // line 40
                echo "    ";
            }
            // line 41
            if (($context["isNew"] ?? null)) {
                // line 42
                echo "<strong";
                if (array_key_exists("title", $context)) {
                    echo " title=\"";
                    echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
                    echo "\"";
                }
                echo ">";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["label"] ?? null));
                echo "</strong>";
            } elseif (            // line 43
array_key_exists("title", $context)) {
                // line 44
                echo "<span title=\"";
                echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
                echo "\">";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["label"] ?? null));
                echo "</span>";
            } else {
                // line 46
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["label"] ?? null));
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/Datagrid/Property/contacts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 46,  164 => 44,  162 => 43,  152 => 42,  150 => 41,  147 => 40,  144 => 39,  141 => 38,  126 => 37,  120 => 35,  117 => 34,  111 => 32,  108 => 31,  105 => 30,  102 => 29,  99 => 28,  96 => 27,  90 => 25,  88 => 23,  87 => 21,  85 => 20,  82 => 19,  79 => 18,  73 => 16,  70 => 15,  67 => 14,  64 => 13,  58 => 11,  56 => 10,  51 => 8,  46 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/Datagrid/Property/contacts.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/Datagrid/Property/contacts.html.twig");
    }
}
