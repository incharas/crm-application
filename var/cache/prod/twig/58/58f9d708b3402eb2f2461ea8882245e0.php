<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTranslation/Operation/loadLanguage.html.twig */
class __TwigTemplate_66259dbf2261310bb01cf57d4580a0ea extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'form_widget' => [$this, 'block_form_widget'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroAction/Operation/form.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroAction/Operation/form.html.twig", "@OroTranslation/Operation/loadLanguage.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_form_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["translationCompleteness"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "stat", [], "any", false, true, false, 4), "translationStatus", [], "any", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "stat", [], "any", false, true, false, 4), "translationStatus", [], "any", false, false, false, 4), null)) : (null));
        // line 5
        echo "
    <div>
        <div class=\"pull-left\">
            <span class=\"fa-info-circle tooltip-icon\"
               data-toggle=\"popover\"
               data-content=\"<div class='oro-popover-content'>";
        // line 10
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.translation.form.tooltip.translation_completeness"), "html", null, true);
        echo "</div>\"
            ></span>
            ";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.translation.action.translation_completeness.label"), "html", null, true);
        echo "&nbsp;</div>
        <div class=\"pull-right\">
            ";
        // line 14
        if ( !(null === ($context["translationCompleteness"] ?? null))) {
            // line 15
            echo "                <div class=\"oro-translation-languages\">
                    <div class=\"translation-completeness\">
                        <div class=\"progress\">
                            <div class=\"progress-bar success\" style=\"width: ";
            // line 18
            echo twig_escape_filter($this->env, ($context["translationCompleteness"] ?? null), "html", null, true);
            echo "%;\"></div>
                        </div>
                        <b class=\"progress-label\">";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent((($context["translationCompleteness"] ?? null) / 100)), "html", null, true);
            echo "</b>
                    </div>
                </div>
            ";
        } else {
            // line 24
            echo "                <span class=\"badge badge-disabled\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "</span>
            ";
        }
        // line 26
        echo "        </div>
    </div>
    ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTranslation/Operation/loadLanguage.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  99 => 28,  95 => 26,  89 => 24,  82 => 20,  77 => 18,  72 => 15,  70 => 14,  65 => 12,  60 => 10,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTranslation/Operation/loadLanguage.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/Resources/views/Operation/loadLanguage.html.twig");
    }
}
