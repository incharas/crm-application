<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/jstree/abstract-action-view.js */
class __TwigTemplate_3543aef6a69b1e188f7a51f5f6b94fd0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const BaseView = require('oroui/js/app/views/base/view');

    const AbstractActionView = BaseView.extend({
        /**
         * @property {Object}
         */
        options: {
            \$tree: '',
            action: '',
            template: require('tpl-loader!oroui/templates/jstree-action.html'),
            icon: '',
            label: ''
        },

        /**
         * @inheritdoc
         */
        events: {
            'click [data-role=\"jstree-action\"]': 'onClick'
        },

        /**
         * @inheritdoc
         */
        constructor: function AbstractActionView(options) {
            AbstractActionView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            this.options = \$.extend(true, {}, this.options, options);
            AbstractActionView.__super__.initialize.call(this, options);
        },

        /**
         * @inheritdoc
         */
        render: function() {
            const \$el = \$(this.options.template(this.options));
            if (this.\$el) {
                this.\$el.replaceWith(\$el);
            }
            this.setElement(\$el);
            return this;
        },

        onClick: function() {},

        /**
         * @inheritdoc
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }

            delete this.options;
            AbstractActionView.__super__.dispose.call(this);
        }
    });

    return AbstractActionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/jstree/abstract-action-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/jstree/abstract-action-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/jstree/abstract-action-view.js");
    }
}
