<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroZendesk/Case/syncButton.html.twig */
class __TwigTemplate_3cc6f3c58700cadf6e7f369a59d36a31 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 2
            echo "    ";
            $context["channels"] = $this->extensions['Oro\Bundle\ZendeskBundle\Twig\ZendeskExtension']->getEnabledTwoWaySyncChannels();
            // line 3
            echo "    ";
            $context["firstChannel"] = twig_first($this->env, ($context["channels"] ?? null));
            // line 4
            echo "    ";
            $context["togglerId"] = uniqid("dropdown-");
            // line 5
            echo "    <div class=\"btn-group zendesk-integration-btn-group\">
        <a class=\"zendesk-integration-btn btn icons-holder-text no-hash\"
           href=\"#\"
           role=\"button\"
           data-channel-id=\"";
            // line 9
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["firstChannel"] ?? null), "id", [], "any", false, false, false, 9), "html", null, true);
            echo "\"
        >
           <span class=\"fa-upload  hide-text\" aria-hidden=\"true\"></span>";
            // line 11
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.form.sync_to_zendesk.label"), "html", null, true);
            echo "
        </a>
        <a href=\"#\" role=\"button\" id=\"";
            // line 13
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\" class=\"btn dropdown-toggle\" data-toggle=\"dropdown\"
           aria-haspopup=\"true\" aria-expanded=\"false\" aria-label=\"";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.form.sync_to_zendesk.label"), "html", null, true);
            echo "\">
           <span class=\"sr-only\">";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.zendesk.form.sync_to_zendesk.label"), "html", null, true);
            echo "</span>
       </a>
        <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"";
            // line 17
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\">
            ";
            // line 18
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["channels"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["channel"]) {
                // line 19
                echo "                <li>
                    <a href=\"#\"
                       role=\"menuitem\"
                       class=\"zendesk-integration-btn icons-holder-text no-hash\"
                       data-channel-id=\"";
                // line 23
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["channel"], "id", [], "any", false, false, false, 23), "html", null, true);
                echo "\">
                        ";
                // line 24
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["channel"], "name", [], "any", false, false, false, 24), "html", null, true);
                echo "
                    </a>
                </li>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['channel'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "        </ul>
    </div>
    <script type=\"text/javascript\">
        loadModules(['jquery', 'routing', 'oroui/js/mediator', 'oroui/js/messenger', 'orotranslation/js/translator'],
            function (\$, routing, mediator, messenger, __) {
                \$('.zendesk-integration-btn').bind('click', function(event) {
                    event.preventDefault();

                    var id = \$(this).data('channel-id');
                    var url = routing.generate('oro_api_post_ticket_sync_case', {id: '";
            // line 37
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 37), "html", null, true);
            echo "', channelId: id});

                    mediator.execute('showLoading');
                    \$.post({
                        url: url,
                        errorHandlerMessage: __('oro.zendesk.reverse_sync.fail_message')
                    }).done(function(res, status, jqXHR) {
                        mediator.once('page:afterChange', function(){
                            messenger.notificationFlashMessage('success', __('oro.zendesk.reverse_sync.success_message'));
                        });
                        mediator.execute('refreshPage', {restore: true});
                    }).always(function () {
                        mediator.execute('hideLoading');
                    });

                    return false;
                });
            });
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroZendesk/Case/syncButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 37,  105 => 28,  95 => 24,  91 => 23,  85 => 19,  81 => 18,  77 => 17,  72 => 15,  68 => 14,  64 => 13,  59 => 11,  54 => 9,  48 => 5,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroZendesk/Case/syncButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-zendesk/Resources/views/Case/syncButton.html.twig");
    }
}
