<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/hidden-initialization-view.js */
class __TwigTemplate_93302e9829849f4e31a05cd129cfe694 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/** @exports HiddenInitializationView */
define(function(require) {
    'use strict';

    const BaseView = require('oroui/js/app/views/base/view');

    /**
     * View allows hide part of DOM tree till all page components will be initialized
     *
     * Usage sample:
     *
     * > Please note that all div's attributes are required for valid work.
     *
     * ```html
     * <div class=\"invisible\"
     *         data-page-component-module=\"oroui/js/app/components/view-component\"
     *         data-page-component-options=\"{'view': 'oroui/js/app/views/hidden-initialization-view'}\"
     *         data-layout=\"separate\">
     *     <!-- write anything here -->
     * </div>
     * ```
     *
     * @class HiddenInitializationView
     * @augments BaseView
     */
    const HiddenInitializationView = BaseView.extend(/** @lends HiddenInitializationView.prototype */{
        autoRender: true,

        /**
         * @inheritdoc
         */
        constructor: function HiddenInitializationView(options) {
            HiddenInitializationView.__super__.constructor.call(this, options);
        },

        render: function() {
            this.\$el.addClass('invisible');
            this.initLayout().done(() => {
                this.\$el.removeClass('invisible');
            });
        }
    });

    return HiddenInitializationView;
});

";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/hidden-initialization-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/hidden-initialization-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/hidden-initialization-view.js");
    }
}
