<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Assets/oro_css.html.twig */
class __TwigTemplate_72d72917d8edf284c4c12e7a11928478 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroUI/Assets/oro_css.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["extension"] = (($this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->isRtlMode()) ? ("rtl.css") : ("css"));
        // line 4
        echo "
";
        // line 5
        echo twig_call_macro($macros["Asset"], "macro_css", [("build/admin/css/oro." . ($context["extension"] ?? null)), "media=\"all\""], 5, $context, $this->getSourceContext());
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroUI/Assets/oro_css.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Assets/oro_css.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Assets/oro_css.html.twig");
    }
}
