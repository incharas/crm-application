<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroLocale/Twig/address.html.twig */
class __TwigTemplate_9818a06687e18caa80f751352c07f0fe extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'address' => [$this, 'block_address'],
            'address_part' => [$this, 'block_address_part'],
            'address_part_postal_code' => [$this, 'block_address_part_postal_code'],
            'address_part_phone' => [$this, 'block_address_part_phone'],
            '_address_part_ltr' => [$this, 'block__address_part_ltr'],
            '_address_part_attributes' => [$this, 'block__address_part_attributes'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('address', $context, $blocks);
        // line 4
        echo "
";
        // line 5
        $this->displayBlock('address_part', $context, $blocks);
        // line 10
        echo "
";
        // line 11
        $this->displayBlock('address_part_postal_code', $context, $blocks);
        // line 14
        echo "
";
        // line 15
        $this->displayBlock('address_part_phone', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('_address_part_ltr', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('_address_part_attributes', $context, $blocks);
    }

    // line 1
    public function block_address($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo ($context["formatted"] ?? null);
    }

    // line 5
    public function block_address_part($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        if ((($context["part_value"] ?? null) != "")) {
            // line 7
            echo "<span";
            $this->displayBlock("_address_part_attributes", $context, $blocks);
            echo ">";
            echo twig_escape_filter($this->env, ($context["part_value"] ?? null), "html", null, true);
            echo "</span>";
        }
    }

    // line 11
    public function block_address_part_postal_code($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        $this->displayBlock("_address_part_ltr", $context, $blocks);
    }

    // line 15
    public function block_address_part_phone($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        $this->displayBlock("_address_part_ltr", $context, $blocks);
    }

    // line 19
    public function block__address_part_ltr($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        if ((($context["part_value"] ?? null) != "")) {
            // line 21
            $context["attr"] = ["dir" => "ltr"];
            // line 22
            echo "<bdo";
            $this->displayBlock("_address_part_attributes", $context, $blocks);
            echo ">";
            echo twig_escape_filter($this->env, ($context["part_value"] ?? null), "html", null, true);
            echo "</bdo>";
        }
    }

    // line 26
    public function block__address_part_attributes($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        $context["attr"] = twig_array_merge(["data-part" =>         // line 28
($context["part_name"] ?? null), "class" => ("address-part-" . twig_replace_filter(        // line 29
($context["part_name"] ?? null), ["_" => "-"]))], ((        // line 30
array_key_exists("attr", $context)) ? (_twig_default_filter(($context["attr"] ?? null), [])) : ([])));
        // line 32
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["attr"] ?? null));
        foreach ($context['_seq'] as $context["name"] => $context["value"]) {
            // line 33
            echo twig_escape_filter($this->env, (" " . $context["name"]), "html", null, true);
            echo "=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "@OroLocale/Twig/address.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  141 => 33,  137 => 32,  135 => 30,  134 => 29,  133 => 28,  132 => 27,  128 => 26,  119 => 22,  117 => 21,  115 => 20,  111 => 19,  107 => 16,  103 => 15,  99 => 12,  95 => 11,  86 => 7,  84 => 6,  80 => 5,  76 => 2,  72 => 1,  68 => 26,  65 => 25,  63 => 19,  60 => 18,  58 => 15,  55 => 14,  53 => 11,  50 => 10,  48 => 5,  45 => 4,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroLocale/Twig/address.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/LocaleBundle/Resources/views/Twig/address.html.twig");
    }
}
