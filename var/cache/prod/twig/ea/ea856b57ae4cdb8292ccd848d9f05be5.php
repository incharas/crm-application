<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAction/Operation/page.html.twig */
class __TwigTemplate_e0ddae5dd294717005011ee964cde875 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["formAction"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 3), "uri", [], "any", false, false, false, 3);
        // line 4
        $context["entity"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "data", [], "any", false, false, false, 4);
        // line 6
        if (($context["entity"] ?? null)) {
            // line 7
            $context["className"] = $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "data", [], "any", false, false, false, 7));
            // line 8
            $context["entityLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["className"] ?? null), "label"));
            // line 9
            $context["indexUrl"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassRoute(($context["className"] ?? null), "name"));

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["titleTemplate" => "%entity% - %operationName% - %label% - %pluralLabel%", "params" => ["%entity%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["entity"] ?? null), "__toString", [], "any", false, false, false, 14), "%operationName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["operation"] ?? null), "definition", [], "any", false, false, false, 15), "label", [], "any", false, false, false, 15)), "%label%" =>             // line 16
($context["entityLabel"] ?? null), "%pluralLabel%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(            // line 17
($context["className"] ?? null), "plural_label"))]]);
        } else {
            // line 21
            $context["indexUrl"] = "";

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["titleTemplate" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["operation"] ?? null), "definition", [], "any", false, false, false, 22), "label", [], "any", false, false, false, 22))]);
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroAction/Operation/page.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 25
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    ";
        if (($context["entity"] ?? null)) {
            // line 27
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 28
($context["entity"] ?? null), "indexPath" =>             // line 29
($context["indexUrl"] ?? null), "indexLabel" =>             // line 30
($context["entityLabel"] ?? null), "entityTitle" => ((            // line 31
array_key_exists("entity", $context)) ? (_twig_default_filter(($context["entity"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))];
            // line 33
            echo "
        ";
            // line 34
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 36
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroAction/Operation/page.html.twig", 36)->display(twig_array_merge($context, ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["operation"] ?? null), "definition", [], "any", false, false, false, 36), "label", [], "any", false, false, false, 36))]));
            // line 37
            echo "    ";
        }
    }

    // line 40
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAction/Operation/page.html.twig", 41)->unwrap();
        // line 42
        echo "
    ";
        // line 43
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [((array_key_exists("fromUrl", $context)) ? (_twig_default_filter(($context["fromUrl"] ?? null), ($context["indexUrl"] ?? null))) : (($context["indexUrl"] ?? null)))], 43, $context, $this->getSourceContext());
        echo "

    ";
        // line 45
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => twig_call_macro($macros["UI"], "macro_saveAndStayButton", ["Submit"], 45, $context, $this->getSourceContext())]], 45, $context, $this->getSourceContext());
        echo "
";
    }

    // line 48
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 49
        echo "    ";
        if ( !array_key_exists("id", $context)) {
            // line 50
            echo "        ";
            $context["id"] = ("action-page-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["operation"] ?? null), "name", [], "any", false, false, false, 50));
            // line 51
            echo "    ";
        }
        // line 52
        echo "
    ";
        // line 53
        if ( !array_key_exists("data", $context)) {
            // line 54
            echo "        ";
            $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 55
($context["operation"] ?? null), "definition", [], "any", false, false, false, 55), "label", [], "any", false, false, false, 55)), "class" => "active", "subblocks" => [0 => ["data" => [0 =>             // line 59
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget')]]]]];
            // line 63
            echo "
        ";
            // line 64
            $context["data"] = ["formErrors" => ((            // line 65
array_key_exists("formErrors", $context)) ? (_twig_default_filter(($context["formErrors"] ?? null), "")) : ("")), "dataBlocks" =>             // line 66
($context["dataBlocks"] ?? null)];
            // line 68
            echo "    ";
        }
        // line 69
        echo "
    ";
        // line 70
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroAction/Operation/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  161 => 70,  158 => 69,  155 => 68,  153 => 66,  152 => 65,  151 => 64,  148 => 63,  146 => 59,  145 => 55,  143 => 54,  141 => 53,  138 => 52,  135 => 51,  132 => 50,  129 => 49,  125 => 48,  119 => 45,  114 => 43,  111 => 42,  108 => 41,  104 => 40,  99 => 37,  96 => 36,  91 => 34,  88 => 33,  86 => 31,  85 => 30,  84 => 29,  83 => 28,  81 => 27,  78 => 26,  74 => 25,  69 => 1,  66 => 22,  63 => 21,  60 => 17,  59 => 16,  58 => 15,  57 => 14,  54 => 9,  52 => 8,  50 => 7,  48 => 6,  46 => 4,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAction/Operation/page.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActionBundle/Resources/views/Operation/page.html.twig");
    }
}
