<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/Form/fields.html.twig */
class __TwigTemplate_976d8f1e853d09bd01cdb1dcc950af4d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_dotmailer_transport_check_button_widget' => [$this, 'block_oro_dotmailer_transport_check_button_widget'],
            'oro_dotmailer_transport_check_button_row' => [$this, 'block_oro_dotmailer_transport_check_button_row'],
            '_oro_dotmailer_datafield_mapping_form_config_entityFields_row' => [$this, 'block__oro_dotmailer_datafield_mapping_form_config_entityFields_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_dotmailer_transport_check_button_widget', $context, $blocks);
        // line 17
        echo "
";
        // line 18
        $this->displayBlock('oro_dotmailer_transport_check_button_row', $context, $blocks);
        // line 21
        echo "
";
        // line 22
        $this->displayBlock('_oro_dotmailer_datafield_mapping_form_config_entityFields_row', $context, $blocks);
    }

    // line 1
    public function block_oro_dotmailer_transport_check_button_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["options"] = ["pingUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_ping")];
        // line 5
        echo "    <div data-page-component-module=\"orodotmailer/js/app/components/dm-resource-component\"
         data-page-component-options=\"";
        // line 6
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\">
        <div class=\"control-group\">
            <div class=\"controls\">
                <div style=\"display: none;\" class=\"ping-holder\">
                    <button type=\"";
        // line 10
        echo twig_escape_filter($this->env, ((array_key_exists("type", $context)) ? (_twig_default_filter(($context["type"] ?? null), "button")) : ("button")), "html", null, true);
        echo "\" ";
        $this->displayBlock("button_attributes", $context, $blocks);
        echo ">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
        echo "</button>
                    <div class=\"connection-status alert\" role=\"alert\" style=\"display: none\"></div>
                </div>
            </div>
        </div>
    </div>
";
    }

    // line 18
    public function block_oro_dotmailer_transport_check_button_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $this->displayBlock("button_row", $context, $blocks);
        echo "
";
    }

    // line 22
    public function block__oro_dotmailer_datafield_mapping_form_config_entityFields_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "    ";
        ob_start(function () { return ''; });
        // line 24
        echo "        <div class=\"control-group entity-field-control\">
            ";
        // line 25
        if ( !(($context["label"] ?? null) === false)) {
            // line 26
            echo "                <div class=\"control-label wrap\">
                    ";
            // line 27
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["label_attr" => ($context["label_attr"] ?? null)]);
            echo "
                </div>
            ";
        }
        // line 30
        echo "            <div class=\"controls";
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            echo " validation-error";
        }
        echo "\">
                <div class=\"fields-container\" data-role=\"fields-container\"></div>
                ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
                ";
        // line 33
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
                <a class=\"btn add-field\" href=\"#\"><i class=\"icon-plus\"></i>";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Add"), "html", null, true);
        echo "</a>
            </div>
        </div>
    ";
        $___internal_parse_97_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 23
        echo twig_spaceless($___internal_parse_97_);
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  141 => 23,  134 => 34,  130 => 33,  126 => 32,  118 => 30,  112 => 27,  109 => 26,  107 => 25,  104 => 24,  101 => 23,  97 => 22,  90 => 19,  86 => 18,  71 => 10,  64 => 6,  61 => 5,  58 => 2,  54 => 1,  50 => 22,  47 => 21,  45 => 18,  42 => 17,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/Form/fields.html.twig");
    }
}
