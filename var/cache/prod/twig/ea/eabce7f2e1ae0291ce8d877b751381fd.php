<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmbeddedForm/layouts/embedded_default/layout.html.twig */
class __TwigTemplate_4abee134fcdb908273e2b26e6afc4b40 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'embed_form_start_widget' => [$this, 'block_embed_form_start_widget'],
            'embed_form_fields_widget' => [$this, 'block_embed_form_fields_widget'],
            'embed_form_end_widget' => [$this, 'block_embed_form_end_widget'],
            'embed_form_field_widget' => [$this, 'block_embed_form_field_widget'],
            'embed_form_errors_widget' => [$this, 'block_embed_form_errors_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('embed_form_start_widget', $context, $blocks);
        // line 12
        echo "
";
        // line 13
        $this->displayBlock('embed_form_fields_widget', $context, $blocks);
        // line 19
        echo "
";
        // line 20
        $this->displayBlock('embed_form_end_widget', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('embed_form_field_widget', $context, $blocks);
        // line 27
        echo "
";
        // line 28
        $this->displayBlock('embed_form_errors_widget', $context, $blocks);
    }

    // line 1
    public function block_embed_form_start_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["attr"] = twig_array_merge(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 2), "attr", [], "any", false, false, false, 2), ($context["attr"] ?? null));
        // line 3
        echo "    ";
        $context["action"] = ((array_key_exists("action_path", $context)) ? (($context["action_path"] ?? null)) : (((array_key_exists("action_route_name", $context)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["action_route_name"] ?? null), ($context["action_route_parameters"] ?? null))) : (null))));
        // line 4
        if ((array_key_exists("method", $context) && !twig_in_filter(($context["method"] ?? null), [0 => "GET", 1 => "POST"]))) {
            // line 5
            $context["form_method"] = "POST";
        }
        // line 7
        echo "<form";
        $this->displayBlock("block_attributes", $context, $blocks);
        if ( !(($context["action"] ?? null) === null)) {
            echo " action=\"";
            echo twig_escape_filter($this->env, ($context["action"] ?? null), "html", null, true);
            echo "\"";
        }
        if (array_key_exists("method", $context)) {
            echo " method=\"";
            echo twig_escape_filter($this->env, twig_lower_filter($this->env, ((array_key_exists("form_method", $context)) ? (_twig_default_filter(($context["form_method"] ?? null), ($context["method"] ?? null))) : (($context["method"] ?? null)))), "html", null, true);
            echo "\"";
        }
        if (array_key_exists("enctype", $context)) {
            echo " enctype=\"";
            echo twig_escape_filter($this->env, ($context["enctype"] ?? null), "html", null, true);
            echo "\"";
        }
        echo ">";
        // line 8
        if (array_key_exists("form_method", $context)) {
            // line 9
            echo "<input type=\"hidden\" name=\"_method\" value=\"";
            echo twig_escape_filter($this->env, ($context["method"] ?? null), "html", null, true);
            echo "\" />";
        }
    }

    // line 13
    public function block_embed_form_fields_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    ";
        // line 15
        if (($context["render_rest"] ?? null)) {
            // line 16
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
    ";
        }
    }

    // line 20
    public function block_embed_form_end_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end', ["render_rest" => ($context["render_rest"] ?? null)]);
        echo "
";
    }

    // line 24
    public function block_embed_form_field_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
";
    }

    // line 28
    public function block_embed_form_errors_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 29), "errors", [], "any", false, false, false, 29)) > 0)) {
            // line 30
            echo "        <div";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo ">
            ";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
        </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroEmbeddedForm/layouts/embedded_default/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  162 => 31,  157 => 30,  154 => 29,  150 => 28,  143 => 25,  139 => 24,  132 => 21,  128 => 20,  120 => 16,  118 => 15,  113 => 14,  109 => 13,  102 => 9,  100 => 8,  81 => 7,  78 => 5,  76 => 4,  73 => 3,  70 => 2,  66 => 1,  62 => 28,  59 => 27,  57 => 24,  54 => 23,  52 => 20,  49 => 19,  47 => 13,  44 => 12,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmbeddedForm/layouts/embedded_default/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/layout.html.twig");
    }
}
