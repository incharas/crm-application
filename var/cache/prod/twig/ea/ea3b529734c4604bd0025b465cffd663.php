<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/footer.scss */
class __TwigTemplate_40658bdf6d1e254b7b2c08d81319810a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

footer.footer {
    .alert {
        margin: 0;
        border-radius: 0;
        border: solid \$primary;
        border-width: 1px 0 0;
        background: \$primary-100;
        text-shadow: none;
        text-align: left;
        padding: 10px 12px;
        color: \$primary-inverse;
        font-weight: font-weight('bold');
        font-size: 11px;
        line-height: 12px;

        .actions {
            float: right;

            .btn-close {
                float: left;
                color: \$primary-inverse;
                cursor: pointer;
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/footer.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/footer.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/footer.scss");
    }
}
