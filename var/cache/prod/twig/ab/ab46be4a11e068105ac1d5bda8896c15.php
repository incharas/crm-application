<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/Grid/dialog/multi.html.twig */
class __TwigTemplate_870f5db56626c98d5a585511f522622a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'page_container' => [$this, 'block_page_container'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDataGrid/Grid/dialog/multi.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 20
        $this->displayBlock('page_container', $context, $blocks);
    }

    public function block_page_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    <div class=\"widget-content\">

        ";
        // line 23
        $context["itemsArray"] = [];
        // line 24
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["entityTargets"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 25
            echo "            ";
            $context["itemArray"] = ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
$context["item"], "label", [], "any", false, false, false, 26)), "className" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 27
$context["item"], "className", [], "any", false, false, false, 27), "gridName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 28
$context["item"], "gridName", [], "any", false, false, false, 28)];
            // line 31
            echo "            ";
            $context["itemsArray"] = twig_array_merge(($context["itemsArray"] ?? null), [0 => ($context["itemArray"] ?? null)]);
            // line 32
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 33
        echo "
        ";
        // line 34
        $context["firstItem"] = twig_first($this->env, ($context["itemsArray"] ?? null));
        // line 35
        echo "        ";
        $context["options"] = twig_array_merge(((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), ["items" =>         // line 36
($context["itemsArray"] ?? null), "params" => ((        // line 37
array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), "gridWidgetName" =>         // line 38
($context["gridWidgetName"] ?? null), "dialogWidgetName" =>         // line 39
($context["dialogWidgetName"] ?? null), "sourceEntityId" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 40
($context["sourceEntity"] ?? null), "id", [], "any", true, true, false, 40)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["sourceEntity"] ?? null), "id", [], "any", false, false, false, 40), null)) : (null)), "sourceEntityClassAlias" =>         // line 41
($context["sourceEntityClassAlias"] ?? null)]);
        // line 44
        echo "        ";
        $context["togglerId"] = uniqid("dropdown-");
        // line 45
        echo "
        <script type=\"text/template\" id=\"multi-grid-item\">
            <li id=\"<%- entity.get('entityAlias') %>\" class=\"dropdown-item\" data-cid=\"<%- entity.cid %>\">
                <%- entity.get('label') %>
            </li>
        </script>

        <div data-page-component-module=\"";
        // line 52
        echo twig_escape_filter($this->env, ((array_key_exists("multiGridComponent", $context)) ? (_twig_default_filter(($context["multiGridComponent"] ?? null), "orodatagrid/js/app/components/multi-grid-component")) : ("orodatagrid/js/app/components/multi-grid-component")), "html", null, true);
        echo "\"
             data-page-component-options=\"";
        // line 53
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\" class=\"dropdown\">
            <div class=\"activity-context-current-block dropdown-toggle\" id=\"";
        // line 54
        echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
        echo "\"
                    data-toggle=\"dropdown\" aria-haspopup=\"true\" aria-expanded=\"false\">
                <span class=\"activity-context-current-item\"></span>
            </div>
            <ul class=\"context-items-dropdown dropdown-menu\" aria-labelledby=\"";
        // line 58
        echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
        echo "\"></ul>
        </div>

        ";
        // line 61
        if (($context["itemsArray"] ?? null)) {
            // line 62
            echo "            ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "widgetTemplate" => "dialog", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_datagrid_widget", twig_array_merge(twig_array_merge(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 67
($context["params"] ?? null), "grid_query", [], "any", false, true, false, 67), "params", [], "any", true, true, false, 67)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "grid_query", [], "any", false, true, false, 67), "params", [], "any", false, false, false, 67), [])) : ([])), ["gridName" => (($__internal_compile_0 = twig_first($this->env,             // line 68
($context["itemsArray"] ?? null))) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["gridName"] ?? null) : null), "params" => ["class_name" => (($__internal_compile_1 = twig_first($this->env,             // line 69
($context["itemsArray"] ?? null))) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["className"] ?? null) : null)]]), ((            // line 70
array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])))), "alias" =>             // line 72
($context["gridWidgetName"] ?? null)]);
            // line 73
            echo "
        ";
        }
        // line 75
        echo "    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/Grid/dialog/multi.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  137 => 75,  133 => 73,  131 => 72,  130 => 70,  129 => 69,  128 => 68,  127 => 67,  125 => 62,  123 => 61,  117 => 58,  110 => 54,  106 => 53,  102 => 52,  93 => 45,  90 => 44,  88 => 41,  87 => 40,  86 => 39,  85 => 38,  84 => 37,  83 => 36,  81 => 35,  79 => 34,  76 => 33,  70 => 32,  67 => 31,  65 => 28,  64 => 27,  63 => 26,  61 => 25,  56 => 24,  54 => 23,  50 => 21,  43 => 20,  40 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/Grid/dialog/multi.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/Grid/dialog/multi.html.twig");
    }
}
