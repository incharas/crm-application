<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/mediator.js */
class __TwigTemplate_667ecd18c931090562ae788339ef3c72 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'underscore',
    'backbone',
    'chaplin'
], function(_, Backbone, Chaplin) {
    'use strict';

    const mediator = Backbone.mediator = Chaplin.mediator;

    _.extend(mediator, Backbone.Events);
    /**
     * Listen Id should be defined before Chaplin.mediator get sealed
     * on application start
     */
    if (!mediator._listenId) {
        mediator._listenId = _.uniqueId('l');
        mediator._listeners = {};
    }

    /**
     * @export oroui/js/mediator
     * @name   oro.mediator
     */
    return mediator;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/mediator.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/mediator.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/mediator.js");
    }
}
