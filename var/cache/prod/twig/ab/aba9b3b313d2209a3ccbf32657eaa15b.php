<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/settings/global-settings.scss */
class __TwigTemplate_3980941d21003a11bec06d23f1b74ca9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$global-url: '~bundles' !default;

@import 'functions';
@import 'colors';
@import 'global-variables';
@import 'mixins/main';
@import '../font-awesome/config';
@import '../bootstrap/functions';
@import '../bootstrap/variables';
@import '../bootstrap/mixins';

// Oro variables
@import '../oro/settings/main';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/settings/global-settings.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/settings/global-settings.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/settings/global-settings.scss");
    }
}
