<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/components/select-create-inline-type-component.js */
class __TwigTemplate_ad93bc624b512b709feacbd25915aea4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseComponent = require('oroui/js/app/components/base/component');
    const SelectCreateInlineTypeView = require('oroform/js/app/views/select-create-inline-type-view');
    const _ = require('underscore');
    require('jquery.select2');

    const SelectCreateInlineTypeComponent = BaseComponent.extend({
        ViewConstructor: SelectCreateInlineTypeView,

        /**
         * @inheritdoc
         */
        constructor: function SelectCreateInlineTypeComponent(options) {
            SelectCreateInlineTypeComponent.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            SelectCreateInlineTypeComponent.__super__.initialize.call(this, options);
            this.view = new this.ViewConstructor(_.extend({
                el: options._sourceElement
            }, _.pick(options, 'urlParts', 'entityLabel', 'existingEntityGridId', 'inputSelector')));
        },
        getUrlParts: function() {
            return this.view.getUrlParts();
        },
        setUrlParts: function(newParts) {
            this.view.setUrlParts(newParts);
        },
        setSelection: function(value) {
            this.view.setSelection(value);
        },
        getSelection: function() {
            return this.view.getSelection();
        }
    });

    return SelectCreateInlineTypeComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/components/select-create-inline-type-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/components/select-create-inline-type-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/components/select-create-inline-type-component.js");
    }
}
