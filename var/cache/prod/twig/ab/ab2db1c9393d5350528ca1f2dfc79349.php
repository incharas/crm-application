<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/view.html.twig */
class __TwigTemplate_5bd96322bef8d9951b1ca1e9b6b200cb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/view.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%application.name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "N/A")) : ("N/A"))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroOAuth2Server/Client/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 8
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_oauth2_server_client_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["entity"] ?? null), "id", [], "any", false, false, false, 9)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 10)) ? ("oro_oauth2_frontend_index") : ("oro_oauth2_index"))), "aCss" => "no-hash remove-button", "id" => "btn-remove-business_unit", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["entity"] ?? null), "id", [], "any", false, false, false, 13), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.entity_label")]], 8, $context, $this->getSourceContext());
            // line 15
            echo "
    ";
        }
        // line 17
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 18
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 20)) ? ("oro_oauth2_frontend_update") : ("oro_oauth2_update")), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["entity"] ?? null), "id", [], "any", false, false, false, 21)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 23
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 23)) ? ("oro.oauth2server.menu.frontend_oauth_application.label") : ("oro.oauth2server.menu.backoffice_oauth_application.label")))]], 18, $context, $this->getSourceContext());
            // line 27
            echo "
    ";
        }
    }

    // line 31
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 33
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 34)) ? ("oro_oauth2_frontend_index") : ("oro_oauth2_index"))), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 35
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 35)) ? ("oro.oauth2server.menu.frontend_oauth_application.label") : ("oro.oauth2server.menu.backoffice_oauth_application.label"))), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 39
($context["entity"] ?? null), "name", [], "any", true, true, false, 39)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 39), "N/A")) : ("N/A"))];
        // line 41
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 44
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "    ";
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <span class=\"page-title__status\">
        ";
        // line 47
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "active", [], "any", false, false, false, 47)) {
            // line 48
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.active.statuses.active"), "enabled"], 48, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 50
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.active.statuses.inactive"), "disabled"], 50, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 52
        echo "    </span>
";
    }

    // line 55
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 58
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 59
        echo "    ";
        if ( !($context["encryptionKeysExist"] ?? null)) {
            // line 60
            echo "        <div class=\"alert alert-warning\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.no_encryption_keys"), "html", null, true);
            echo "</div>
    ";
        }
        // line 62
        echo "    ";
        ob_start(function () { return ''; });
        // line 63
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "grants", [], "any", false, false, false, 63));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["grant"]) {
            // line 64
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "first", [], "any", false, false, false, 64)) {
                echo ", ";
            }
            // line 65
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(("oro.oauth2server.grant_types." . $context["grant"])), "html", null, true);
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['grant'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        $context["grants"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 68
        echo "    ";
        ob_start(function () { return ''; });
        // line 69
        echo "        <div class=\"widget-content\">
            <div class=\"row-fluid form-horizontal\">
                <div class=\"responsive-block\">
                    ";
        // line 72
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.identifier.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "identifier", [], "any", false, false, false, 72)], 72, $context, $this->getSourceContext());
        echo "
                    ";
        // line 73
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 73)], 73, $context, $this->getSourceContext());
        echo "
                    ";
        // line 74
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.grants.label"), ($context["grants"] ?? null)], 74, $context, $this->getSourceContext());
        echo "
                    ";
        // line 75
        if (twig_in_filter("authorization_code", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "grants", [], "any", false, false, false, 75))) {
            // line 76
            echo "                        ";
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.redirect_uris.label"), twig_nl2br(twig_escape_filter($this->env, twig_join_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "redirectUris", [], "any", false, false, false, 76), "
"), "html", null, true))], 76, $context, $this->getSourceContext());
            echo "
                    ";
        }
        // line 78
        if ( !(null === ($context["user"] ?? null))) {
            // line 79
            echo "                        ";
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 80
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 80)) ? ("oro.customer.customeruser.entity_label") : ("oro.user.entity_label"))), twig_call_macro($macros["UI"], "macro_entityViewLink", [            // line 85
($context["user"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 86
($context["user"] ?? null), "fullName", [], "any", false, false, false, 86), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 87
($context["entity"] ?? null), "frontend", [], "any", false, false, false, 87)) ? ("oro_customer_customer_user_view") : ("oro_user_view")), "VIEW"], 84, $context, $this->getSourceContext())], 79, $context, $this->getSourceContext());
            // line 90
            echo "
                    ";
        }
        // line 92
        echo "                </div>
            </div>
        </div>
    ";
        $context["data"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 96
        echo "
    ";
        // line 97
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 102
($context["data"] ?? null)]]]]];
        // line 106
        echo "
    ";
        // line 107
        $context["id"] = "oauthApplicationsView";
        // line 108
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 109
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  255 => 109,  252 => 108,  250 => 107,  247 => 106,  245 => 102,  244 => 97,  241 => 96,  235 => 92,  231 => 90,  229 => 87,  228 => 86,  227 => 85,  226 => 80,  224 => 79,  222 => 78,  215 => 76,  213 => 75,  209 => 74,  205 => 73,  201 => 72,  196 => 69,  193 => 68,  178 => 65,  174 => 64,  157 => 63,  154 => 62,  148 => 60,  145 => 59,  141 => 58,  135 => 55,  130 => 52,  124 => 50,  118 => 48,  116 => 47,  110 => 45,  106 => 44,  99 => 41,  97 => 39,  96 => 35,  95 => 34,  94 => 33,  92 => 32,  88 => 31,  82 => 27,  80 => 23,  79 => 21,  78 => 20,  76 => 18,  73 => 17,  69 => 15,  67 => 13,  66 => 10,  65 => 9,  63 => 8,  60 => 7,  56 => 6,  51 => 1,  49 => 4,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/view.html.twig");
    }
}
