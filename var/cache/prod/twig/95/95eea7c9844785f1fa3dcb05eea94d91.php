<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/tagField.html.twig */
class __TwigTemplate_b780d24bd5f7a0943a3979cb332ea3b7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTag/tagField.html.twig", 1)->unwrap();
        // line 2
        $macros["Tag"] = $this->macros["Tag"] = $this->loadTemplate("@OroTag/macros.html.twig", "@OroTag/tagField.html.twig", 2)->unwrap();
        // line 3
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_tag_view") && $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isFeatureEnabled("manage_tags"))) {
            // line 4
            echo "    <div class=\"row-fluid\">
        <div class=\"responsive-block\">
    ";
            // line 6
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.entity_plural_label"), twig_call_macro($macros["Tag"], "macro_renderView", [($context["entity"] ?? null)], 6, $context, $this->getSourceContext())], 6, $context, $this->getSourceContext());
            echo "
        </div>
        <div class=\"responsive-block\"></div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroTag/tagField.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 6,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/tagField.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/tagField.html.twig");
    }
}
