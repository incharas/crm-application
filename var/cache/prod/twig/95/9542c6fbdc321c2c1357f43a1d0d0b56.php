<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImportExport/ImportExport/widget/importValidateExportTemplate.html.twig */
class __TwigTemplate_c9448c23de91a0c57f0260b279074f42 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroImportExport/ImportExport/widget/importValidateExportTemplate.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["importProcessorAliasesToConfirmMessage"] = [];
        // line 4
        echo "
";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["configsWithForm"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["configWithForm"]) {
            // line 6
            echo "    ";
            $context["config"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configWithForm"], "configuration", [], "any", false, false, false, 6);
            // line 7
            echo "    ";
            $context["importProcessorAliasesToConfirmMessage"] = twig_array_merge(($context["importProcessorAliasesToConfirmMessage"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "importProcessorsToConfirmationMessage", [], "any", false, false, false, 7));
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['configWithForm'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 9
        echo "
";
        // line 10
        $context["ImportValidateViewOptions"] = ["view" => "oroimportexport/js/app/views/import-validate-view", "options" => ["importProcessorAliasesToConfirmMessages" =>         // line 13
($context["importProcessorAliasesToConfirmMessage"] ?? null), "wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 14
($context["app"] ?? null), "request", [], "any", false, false, false, 14), "get", [0 => "_wid"], "method", false, false, false, 14)]];
        // line 18
        echo "
<div class=\"widget-content import-widget-content\" ";
        // line 19
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["ImportValidateViewOptions"] ?? null)], 19, $context, $this->getSourceContext());
        echo ">
    ";
        // line 20
        $context["tabId"] = uniqid("import-tablist-");
        // line 21
        echo "    ";
        if ((twig_length_filter($this->env, ($context["configsWithForm"] ?? null)) > 1)) {
            // line 22
            echo "        <div  ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/tabs-component"]], 22, $context, $this->getSourceContext());
            // line 24
            echo ">
            <ul class=\"nav nav-tabs\" role=\"tablist\">
                ";
            // line 26
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["configsWithForm"] ?? null));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["configWithForm"]) {
                // line 27
                echo "                    ";
                $context["config"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configWithForm"], "configuration", [], "any", false, false, false, 27);
                // line 28
                echo "                    ";
                $context["shortEntityName"] = $this->extensions['Oro\Bundle\ImportExportBundle\Twig\BasenameTwigExtension']->basenameFilter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "entityClass", [], "any", false, false, false, 28));
                // line 29
                echo "
                    ";
                // line 30
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "importEntityLabel", [], "any", true, true, false, 30) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "importEntityLabel", [], "any", false, false, false, 30)))) {
                    // line 31
                    echo "                        ";
                    $context["entityLabel"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "importEntityLabel", [], "any", false, false, false, 31);
                    // line 32
                    echo "                    ";
                } else {
                    // line 33
                    echo "                        ";
                    $context["entityLabel"] = ($context["shortEntityName"] ?? null);
                    // line 34
                    echo "                    ";
                }
                // line 35
                echo "
                    ";
                // line 36
                $context["isActiveEntity"] = ((($context["chosenEntityName"] ?? null) && (($context["chosenEntityName"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "entityClass", [], "any", false, false, false, 36))) || ( !($context["chosenEntityName"] ?? null) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "index", [], "any", false, false, false, 36) == 1)));
                // line 37
                echo "
                    ";
                // line 38
                if ( !twig_test_empty((($__internal_compile_0 = ($context["entityVisibility"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "entityClass", [], "any", false, false, false, 38)] ?? null) : null))) {
                    // line 39
                    echo "                    <li class=\"nav-item\">
                        <a id=\"importExport";
                    // line 40
                    echo twig_escape_filter($this->env, (($context["shortEntityName"] ?? null) . ($context["tabId"] ?? null)), "html", null, true);
                    echo "-tab\"
                           href=\"#importExport";
                    // line 41
                    echo twig_escape_filter($this->env, (($context["shortEntityName"] ?? null) . ($context["tabId"] ?? null)), "html", null, true);
                    echo "\"
                           class=\"nav-link";
                    // line 42
                    if (($context["isActiveEntity"] ?? null)) {
                        echo " active";
                    }
                    echo "\"
                           data-toggle=\"tab\"
                           aria-selected=\"";
                    // line 44
                    if (($context["isActiveEntity"] ?? null)) {
                        echo "true";
                    } else {
                        echo "false";
                    }
                    echo "\"
                           aria-controls=\"importExport";
                    // line 45
                    echo twig_escape_filter($this->env, (($context["shortEntityName"] ?? null) . ($context["tabId"] ?? null)), "html", null, true);
                    echo "\"
                           role=\"tab\"
                        >";
                    // line 47
                    echo twig_escape_filter($this->env, ($context["entityLabel"] ?? null), "html", null, true);
                    echo "</a>
                    </li>
                    ";
                }
                // line 50
                echo "                ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['configWithForm'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "            </ul>
        </div>
    ";
        }
        // line 54
        echo "
    ";
        // line 55
        if ((twig_length_filter($this->env, ($context["configsWithForm"] ?? null)) > 1)) {
            // line 56
            echo "    <div class=\"tab-content\">
    ";
        }
        // line 58
        echo "
        ";
        // line 59
        $context["isShown"] = false;
        // line 60
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["configsWithForm"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["configWithForm"]) {
            // line 61
            echo "            ";
            $context["config"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configWithForm"], "configuration", [], "any", false, false, false, 61);
            // line 62
            echo "            ";
            $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["configWithForm"], "form", [], "any", false, false, false, 62), "createView", [], "method", false, false, false, 62);
            // line 63
            echo "            ";
            $context["shortEntityName"] = $this->extensions['Oro\Bundle\ImportExportBundle\Twig\BasenameTwigExtension']->basenameFilter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "entityClass", [], "any", false, false, false, 63));
            // line 64
            echo "            ";
            $context["isActiveEntity"] = ( !twig_test_empty((($__internal_compile_1 = ($context["entityVisibility"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "entityClass", [], "any", false, false, false, 64)] ?? null) : null)) && ((($context["chosenEntityName"] ?? null) && (($context["chosenEntityName"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "entityClass", [], "any", false, false, false, 64))) || ( !($context["chosenEntityName"] ?? null) && (($context["isShown"] ?? null) == false))));
            // line 65
            echo "            ";
            if (($context["isActiveEntity"] ?? null)) {
                // line 66
                echo "                ";
                $context["isShown"] = true;
                // line 67
                echo "            ";
            }
            // line 68
            echo "            ";
            $context["strategyTooltip"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "importStrategyTooltip", [], "any", false, false, false, 68)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "importStrategyTooltip", [], "any", false, false, false, 68)) : ("oro.importexport.import.strategy.tooltip"));
            // line 69
            echo "
            ";
            // line 70
            if ( !twig_test_empty((($__internal_compile_2 = ($context["entityVisibility"] ?? null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "entityClass", [], "any", false, false, false, 70)] ?? null) : null))) {
                // line 71
                echo "            <div id=\"importExport";
                echo twig_escape_filter($this->env, (($context["shortEntityName"] ?? null) . ($context["tabId"] ?? null)), "html", null, true);
                echo "\"
                 class=\"tab-pane";
                // line 72
                if (($context["isActiveEntity"] ?? null)) {
                    echo " active";
                }
                echo "\"
                 role=\"tabpanel\"
                 aria-labelledby=\"importExport";
                // line 74
                echo twig_escape_filter($this->env, (($context["shortEntityName"] ?? null) . ($context["tabId"] ?? null)), "html", null, true);
                echo "-tab\"
            >
                <div class=\"alert alert-info import-notice\" role=\"alert\">
                    <b>";
                // line 77
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.importance"), "html", null, true);
                echo "</b>:
                    ";
                // line 78
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.columns_notice"), "html", null, true);
                echo "
                </div>

                ";
                // line 81
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("import_validate_export_messages", $context)) ? (_twig_default_filter(($context["import_validate_export_messages"] ?? null), "import_validate_export_messages")) : ("import_validate_export_messages")), ["alias" => ($context["alias"] ?? null), "config" => ($context["config"] ?? null), "options" => ($context["options"] ?? null)]);
                // line 82
                echo "
                <div class=\"form-container\">
                    ";
                // line 84
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_importexport_import_validate_export_template_form", ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 86
($context["config"] ?? null), "entityClass", [], "any", false, false, false, 86), "importJob" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 87
($context["config"] ?? null), "importJobName", [], "any", false, false, false, 87), "importValidateJob" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 88
($context["config"] ?? null), "importValidationJobName", [], "any", false, false, false, 88), "alias" =>                 // line 89
($context["alias"] ?? null)]), "attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 92
($context["form"] ?? null), "vars", [], "any", false, false, false, 92), "id", [], "any", false, false, false, 92), "data-nohash" => "true", "class" => "form-horizontal"]]);
                // line 96
                echo "

                        ";
                // line 98
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
                foreach ($context['_seq'] as $context["key"] => $context["option"]) {
                    // line 99
                    echo "                            <input type=\"hidden\" name=\"options[";
                    echo twig_escape_filter($this->env, $context["key"], "html", null, true);
                    echo "]\" value=\"";
                    echo twig_escape_filter($this->env, $context["option"], "html", null, true);
                    echo "\" />
                        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['option'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 101
                echo "
                        <input type=\"hidden\" name=\"isValidateJob\" value=\"false\" />
                        <input type=\"hidden\" name=\"importProcessorTopicName\" value=\"";
                // line 103
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "importProcessorTopicName", [], "any", false, false, false, 103), "html", null, true);
                echo "\"/>

                        <fieldset class=\"form\">
                            <div class=\"control-group-container import-file\">
                                ";
                // line 107
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 107), 'row', ["label" => "oro.importexport.import.file", "attr" => ["data-validation-ignore-onblur" => true]]);
                // line 110
                echo "
                            </div>
                            <div class=\"control-group-container\" ";
                // line 112
                if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "processorAlias", [], "any", false, false, false, 112), "vars", [], "any", false, false, false, 112), "choices", [], "any", false, false, false, 112)) <= 1)) {
                    echo "style=\"display: none;\"";
                }
                echo ">
                                ";
                // line 113
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "processorAlias", [], "any", false, false, false, 113), 'row', ["label" => "oro.importexport.import.strategy", "tooltip" =>                 // line 115
($context["strategyTooltip"] ?? null)]);
                // line 116
                echo "
                            </div>
                        ";
                // line 118
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
                echo "

                        ";
                // line 120
                $context["exportTemplateProcessors"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "exportTemplateProcessorAlias", [], "any", false, false, false, 120);
                // line 121
                echo "                        ";
                $context["hasExportTemplateProcessor"] =  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "exportTemplateProcessorAlias", [], "any", false, false, false, 121));
                // line 122
                echo "                        ";
                $context["isMultipleProcessorsRequired"] = twig_test_iterable(($context["exportTemplateProcessors"] ?? null));
                // line 123
                echo "                            <div class=\"control-group control-group-file\">
                                <div class=\"control-label wrap\"></div>
                                <div class=\"controls\">
                                    ";
                // line 126
                if (($context["hasExportTemplateProcessor"] ?? null)) {
                    // line 127
                    echo "                                        ";
                    if ((($context["isMultipleProcessorsRequired"] ?? null) && (twig_length_filter($this->env, ($context["exportTemplateProcessors"] ?? null)) > 1))) {
                        // line 128
                        echo "                                            ";
                        $context["exportTemplateButtons"] = [];
                        // line 129
                        echo "
                                            ";
                        // line 130
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(($context["exportTemplateProcessors"] ?? null));
                        foreach ($context['_seq'] as $context["alias"] => $context["label"]) {
                            // line 131
                            echo "                                                ";
                            $context["exportTemplateButtons"] = twig_array_merge(($context["exportTemplateButtons"] ?? null), [0 => ["path" => "#", "aCss" => "icons-holder-text no-hash export-tmpl-btn", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(                            // line 134
$context["label"]), "iCss" => "fa-file-o hide-text", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(                            // line 136
$context["label"]), "data" => ["page-component-module" => "oroui/js/app/components/view-component", "page-component-options" => json_encode(["view" => "oroimportexport/js/app/views/export-template-button-view", "exportTemplateProcessor" =>                             // line 141
$context["alias"], "exportTemplateJob" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                             // line 142
($context["config"] ?? null), "exportTemplateJob", [], "any", true, true, false, 142)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "exportTemplateJob", [], "any", false, false, false, 142), null)) : (null)), "routeOptions" => ((                            // line 143
array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([]))])]]]);
                            // line 148
                            echo "                                            ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['alias'], $context['label'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 149
                        echo "
                                            ";
                        // line 150
                        echo twig_call_macro($macros["UI"], "macro_dropdownButton", [["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export_template.label"), "placement" => "bottom-start", "elements" =>                         // line 153
($context["exportTemplateButtons"] ?? null)]], 150, $context, $this->getSourceContext());
                        // line 154
                        echo "
                                        ";
                    } else {
                        // line 156
                        echo "                                            ";
                        echo twig_call_macro($macros["UI"], "macro_button", [["path" => "#", "aCss" => "icons-holder-text no-hash export-tmpl-btn", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export_template.label"), "iCss" => "fa-file-o hide-text", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export_template.label"), "data" => ["page-component-module" => "oroui/js/app/components/view-component", "page-component-options" => json_encode(["view" => "oroimportexport/js/app/views/export-template-button-view", "exportTemplateProcessor" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 166
($context["config"] ?? null), "exportTemplateProcessorAlias", [], "any", false, false, false, 166), "exportTemplateJob" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 167
($context["config"] ?? null), "exportTemplateJob", [], "any", true, true, false, 167)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "exportTemplateJob", [], "any", false, false, false, 167), null)) : (null)), "routeOptions" => ((                        // line 168
array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([]))])]]], 156, $context, $this->getSourceContext());
                        // line 171
                        echo "
                                        ";
                    }
                    // line 173
                    echo "                                    ";
                }
                // line 174
                echo "                                </div>
                            </div>
                        </fieldset>
                    ";
                // line 177
                echo                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
                echo "
                    ";
                // line 178
                echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
                echo "
                </div>
            </div>
            ";
            }
            // line 182
            echo "        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['configWithForm'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 183
        echo "
    ";
        // line 184
        if ((twig_length_filter($this->env, ($context["configsWithForm"] ?? null)) > 1)) {
            // line 185
            echo "    </div>
    ";
        }
        // line 187
        echo "
    <div class=\"widget-actions\">
        <button class=\"btn\" type=\"reset\">";
        // line 189
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
        <button class=\"btn btn-primary\" type=\"button\" data-action-name=\"validate_import\" id=\"validate_import_button\">";
        // line 190
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.validation_label"), "html", null, true);
        echo "</button>
        <button class=\"btn btn-success\" type=\"button\" data-action-name=\"import\" id=\"import_button\">";
        // line 191
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.label"), "html", null, true);
        echo "</button>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroImportExport/ImportExport/widget/importValidateExportTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  444 => 191,  440 => 190,  436 => 189,  432 => 187,  428 => 185,  426 => 184,  423 => 183,  417 => 182,  410 => 178,  406 => 177,  401 => 174,  398 => 173,  394 => 171,  392 => 168,  391 => 167,  390 => 166,  388 => 156,  384 => 154,  382 => 153,  381 => 150,  378 => 149,  372 => 148,  370 => 143,  369 => 142,  368 => 141,  367 => 136,  366 => 134,  364 => 131,  360 => 130,  357 => 129,  354 => 128,  351 => 127,  349 => 126,  344 => 123,  341 => 122,  338 => 121,  336 => 120,  331 => 118,  327 => 116,  325 => 115,  324 => 113,  318 => 112,  314 => 110,  312 => 107,  305 => 103,  301 => 101,  290 => 99,  286 => 98,  282 => 96,  280 => 92,  279 => 89,  278 => 88,  277 => 87,  276 => 86,  275 => 84,  271 => 82,  269 => 81,  263 => 78,  259 => 77,  253 => 74,  246 => 72,  241 => 71,  239 => 70,  236 => 69,  233 => 68,  230 => 67,  227 => 66,  224 => 65,  221 => 64,  218 => 63,  215 => 62,  212 => 61,  207 => 60,  205 => 59,  202 => 58,  198 => 56,  196 => 55,  193 => 54,  188 => 51,  174 => 50,  168 => 47,  163 => 45,  155 => 44,  148 => 42,  144 => 41,  140 => 40,  137 => 39,  135 => 38,  132 => 37,  130 => 36,  127 => 35,  124 => 34,  121 => 33,  118 => 32,  115 => 31,  113 => 30,  110 => 29,  107 => 28,  104 => 27,  87 => 26,  83 => 24,  80 => 22,  77 => 21,  75 => 20,  71 => 19,  68 => 18,  66 => 14,  65 => 13,  64 => 10,  61 => 9,  54 => 7,  51 => 6,  47 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImportExport/ImportExport/widget/importValidateExportTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Resources/views/ImportExport/widget/importValidateExportTemplate.html.twig");
    }
}
