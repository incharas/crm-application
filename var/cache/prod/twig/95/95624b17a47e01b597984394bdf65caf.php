<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/widget-picker/widget-picker-filter-view.html */
class __TwigTemplate_996e528baa86ba4df5c67e52647aeb4b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<input type=\"text\" class=\"widget-picker__filter-search\" data-role=\"filter-search\" value=\"<%- search %>\"
    data-clearable data-placeholder-icon=\"fa-search\" placeholder=\"<%- _.__('oro.ui.widget_picker.filter.search_placeholder') %>\" />
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/widget-picker/widget-picker-filter-view.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/widget-picker/widget-picker-filter-view.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/widget-picker/widget-picker-filter-view.html");
    }
}
