<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/layouts/default/layout.html.twig */
class __TwigTemplate_db44b894e8a7abe98b07ef4d1b0b6396 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'attribute_group_rest_widget' => [$this, 'block_attribute_group_rest_widget'],
            'attribute_group_rest_attribute_group_widget' => [$this, 'block_attribute_group_rest_attribute_group_widget'],
            'attribute_label_widget' => [$this, 'block_attribute_label_widget'],
            'attribute_text_widget' => [$this, 'block_attribute_text_widget'],
            'attribute_decimal_widget' => [$this, 'block_attribute_decimal_widget'],
            'attribute_boolean_widget' => [$this, 'block_attribute_boolean_widget'],
            'attribute_currency_widget' => [$this, 'block_attribute_currency_widget'],
            'attribute_percent_widget' => [$this, 'block_attribute_percent_widget'],
            'attribute_date_widget' => [$this, 'block_attribute_date_widget'],
            'attribute_datetime_widget' => [$this, 'block_attribute_datetime_widget'],
            'attribute_multiselect_widget' => [$this, 'block_attribute_multiselect_widget'],
            'attribute_image_widget' => [$this, 'block_attribute_image_widget'],
            'attribute_file_widget' => [$this, 'block_attribute_file_widget'],
            'attribute_multiimages_widget' => [$this, 'block_attribute_multiimages_widget'],
            'attribute_multifiles_widget' => [$this, 'block_attribute_multifiles_widget'],
            'attribute_localized_fallback_widget' => [$this, 'block_attribute_localized_fallback_widget'],
            'attribute_group_rest_attribute_widget' => [$this, 'block_attribute_group_rest_attribute_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('attribute_group_rest_widget', $context, $blocks);
        // line 32
        $this->displayBlock('attribute_group_rest_attribute_group_widget', $context, $blocks);
        // line 44
        echo "
";
        // line 45
        $this->displayBlock('attribute_label_widget', $context, $blocks);
        // line 50
        echo "
";
        // line 51
        $this->displayBlock('attribute_text_widget', $context, $blocks);
        // line 58
        echo "
";
        // line 59
        $this->displayBlock('attribute_decimal_widget', $context, $blocks);
        // line 62
        echo "
";
        // line 63
        $this->displayBlock('attribute_boolean_widget', $context, $blocks);
        // line 66
        echo "
";
        // line 67
        $this->displayBlock('attribute_currency_widget', $context, $blocks);
        // line 70
        echo "
";
        // line 71
        $this->displayBlock('attribute_percent_widget', $context, $blocks);
        // line 74
        echo "
";
        // line 75
        $this->displayBlock('attribute_date_widget', $context, $blocks);
        // line 78
        echo "
";
        // line 79
        $this->displayBlock('attribute_datetime_widget', $context, $blocks);
        // line 82
        echo "
";
        // line 83
        $this->displayBlock('attribute_multiselect_widget', $context, $blocks);
        // line 93
        echo "
";
        // line 94
        $this->displayBlock('attribute_image_widget', $context, $blocks);
        // line 105
        echo "
";
        // line 106
        $this->displayBlock('attribute_file_widget', $context, $blocks);
        // line 109
        echo "
";
        // line 110
        $this->displayBlock('attribute_multiimages_widget', $context, $blocks);
        // line 118
        echo "
";
        // line 119
        $this->displayBlock('attribute_multifiles_widget', $context, $blocks);
        // line 144
        echo "
";
        // line 145
        $this->displayBlock('attribute_localized_fallback_widget', $context, $blocks);
        // line 148
        echo "
";
        // line 149
        $this->displayBlock('attribute_group_rest_attribute_widget', $context, $blocks);
    }

    // line 1
    public function block_attribute_group_rest_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/layouts/default/layout.html.twig", 2)->unwrap();
        // line 3
        $context["content"] = "";
        // line 4
        echo "    ";
        $context["visibleTabsOptions"] = [];
        // line 5
        echo "    ";
        $context["tabsOptionsById"] = [];
        // line 6
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["tabsOptions"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["tab"]) {
            // line 7
            $context["tabsOptionsById"] = twig_array_merge(($context["tabsOptionsById"] ?? null), [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 8
$context["tab"], "id", [], "any", false, false, false, 8) => $context["tab"]]);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['tab'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["block"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 12
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 12), "visible", [], "any", false, false, false, 12)) {
                // line 13
                $context["childContent"] = $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock($context["child"], 'widget');
                // line 14
                if ((twig_length_filter($this->env, twig_trim_filter(($context["childContent"] ?? null))) > 0)) {
                    // line 15
                    $context["content"] = (($context["content"] ?? null) . ($context["childContent"] ?? null));
                    // line 16
                    echo "                ";
                    $context["visibleTabsOptions"] = twig_array_merge(($context["visibleTabsOptions"] ?? null), [0 => (($__internal_compile_0 = ($context["tabsOptionsById"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 16), "group", [], "any", false, false, false, 16)] ?? null) : null)]);
                }
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        if ( !twig_test_empty(($context["visibleTabsOptions"] ?? null))) {
            // line 21
            echo "<div ";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo ">
            <div ";
            // line 22
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroentityconfig/js/attribute-group-tabs-component", "name" => "attribute-group-tabs-component", "options" => ["data" =>             // line 25
($context["visibleTabsOptions"] ?? null)]]], 22, $context, $this->getSourceContext());
            // line 26
            echo "></div>";
            // line 27
            echo ($context["content"] ?? null);
            // line 28
            echo "</div>";
        }
    }

    // line 32
    public function block_attribute_group_rest_attribute_group_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "    ";
        $context["content"] = $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        // line 34
        echo "    ";
        if ((twig_length_filter($this->env, twig_trim_filter(($context["content"] ?? null))) > 0)) {
            // line 35
            echo "    ";
            $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["data-page-component-module" => "oroentityconfig/js/attribute-group-tab-content-component", "~data-page-component-options" => json_encode(["id" =>             // line 37
($context["group"] ?? null)])]);
            // line 39
            echo "    <div ";
            $this->displayBlock("block_attributes", $context, $blocks);
            echo " style=\"display: none;\" class=\"tab-content\">
        ";
            // line 40
            echo ($context["content"] ?? null);
            echo "
    </div>
    ";
        }
    }

    // line 45
    public function block_attribute_label_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "    ";
        if ((twig_first($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null))) != "_")) {
            // line 47
            echo "        <label class=\"entity-label\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
            echo ":</label>
    ";
        }
    }

    // line 51
    public function block_attribute_text_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "    ";
        if ( !twig_test_iterable(($context["value"] ?? null))) {
            // line 53
            echo "        ";
            echo twig_nl2br(twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true));
            echo "
    ";
        } else {
            // line 55
            echo "        ";
            echo twig_nl2br(twig_escape_filter($this->env, twig_join_filter(($context["value"] ?? null), "
"), "html", null, true));
            echo "
    ";
        }
    }

    // line 59
    public function block_attribute_decimal_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 60
        echo "    ";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(($context["value"] ?? null)), "html", null, true);
        echo "
";
    }

    // line 63
    public function block_attribute_boolean_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 64
        echo "    ";
        echo twig_escape_filter($this->env, ((($context["value"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No"))), "html", null, true);
        echo "
";
    }

    // line 67
    public function block_attribute_currency_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 68
        echo "    ";
        echo ((($context["value"] ?? null)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatCurrency(($context["value"] ?? null))) : (null));
        echo "
";
    }

    // line 71
    public function block_attribute_percent_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 72
        echo "    ";
        ((($context["value"] ?? null)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(($context["value"] ?? null)), "html", null, true))) : (print (null)));
        echo "
";
    }

    // line 75
    public function block_attribute_date_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 76
        echo "    ";
        ((($context["value"] ?? null)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(($context["value"] ?? null)), "html", null, true))) : (print (null)));
        echo "
";
    }

    // line 79
    public function block_attribute_datetime_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 80
        echo "    ";
        ((($context["value"] ?? null)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(($context["value"] ?? null)), "html", null, true))) : (print (null)));
        echo "
";
    }

    // line 83
    public function block_attribute_multiselect_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 84
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["value"] ?? null));
        $context['_iterated'] = false;
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 85
            echo twig_escape_filter($this->env, $context["item"], "html", null, true);
            // line 86
            if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 86)) {
                // line 87
                echo ",&nbsp;";
            }
            $context['_iterated'] = true;
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        if (!$context['_iterated']) {
            // line 90
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 94
    public function block_attribute_image_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 95
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["alt" =>         // line 96
($context["label"] ?? null), "width" =>         // line 97
($context["width"] ?? null), "height" =>         // line 98
($context["height"] ?? null)]);
        // line 100
        echo "    ";
        $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroEntityConfig/layouts/default/layout.html.twig", 100)->display(twig_array_merge($context, ["sources" => $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getResizedPictureSources(        // line 101
($context["value"] ?? null), ($context["width"] ?? null), ($context["height"] ?? null)), "img_attrs" =>         // line 102
($context["attr"] ?? null)]));
    }

    // line 106
    public function block_attribute_file_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 107
        echo "    ";
        echo $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileView($this->env, ($context["value"] ?? null));
        echo "
";
    }

    // line 110
    public function block_attribute_multiimages_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 111
        echo "    ";
        $context["collection"] = ($context["value"] ?? null);
        // line 112
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["collection"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 113
            $context["value"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "file", [], "any", false, false, false, 113);
            // line 114
            echo "        ";
            $context["label"] = "";
            // line 115
            echo "        ";
            $this->displayBlock("attribute_image_widget", $context, $blocks);
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 119
    public function block_attribute_multifiles_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 120
        echo "    ";
        $context["collection"] = ($context["value"] ?? null);
        // line 121
        echo "    <div class=\"table-responsive\">
        <table class=\"grid grid-main-container table\">
            <thead class=\"grid-header\">
            <tr class=\"grid-header-row\">
                <th class=\"grid-cell\">";
        // line 125
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.attachment.frontend.file.file.label"), "html", null, true);
        echo "</th>
                <th class=\"grid-cell\">";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.attachment.frontend.file.mime_type.label"), "html", null, true);
        echo "</th>
                <th class=\"grid-cell\">";
        // line 127
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.attachment.frontend.file.file_size.label"), "html", null, true);
        echo "</th>
                <th class=\"grid-cell\">";
        // line 128
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.attachment.frontend.file.created_at.label"), "html", null, true);
        echo "</th>
            </tr>
            </thead>
            <tbody class=\"grid-body\">";
        // line 132
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["collection"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 133
            echo "<tr class=\"grid-row\">
                    <td class=\"grid-cell\">";
            // line 134
            echo $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileView($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "file", [], "any", false, false, false, 134));
            echo "</td>
                    <td class=\"grid-cell\">";
            // line 135
            echo twig_escape_filter($this->env, twig_upper_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "file", [], "any", false, false, false, 135), "extension", [], "any", false, false, false, 135)), "html", null, true);
            echo "</td>
                    <td class=\"grid-cell\">";
            // line 136
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileSize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "file", [], "any", false, false, false, 136), "fileSize", [], "any", false, false, false, 136)), "html", null, true);
            echo "</td>
                    <td class=\"grid-cell\">";
            // line 137
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "file", [], "any", false, false, false, 137), "createdAt", [], "any", false, false, false, 137)), "html", null, true);
            echo "</td>
                </tr>";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 140
        echo "</tbody>
        </table>
    </div>
";
    }

    // line 145
    public function block_attribute_localized_fallback_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 146
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(($context["translated_value"] ?? null));
        echo "
";
    }

    // line 149
    public function block_attribute_group_rest_attribute_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 150
        echo "    <div class=\"tab-content__wrapper\">";
        $this->displayBlock("attribute_label_widget", $context, $blocks);
        echo " ";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/layouts/default/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  532 => 150,  528 => 149,  521 => 146,  517 => 145,  510 => 140,  502 => 137,  498 => 136,  494 => 135,  490 => 134,  487 => 133,  483 => 132,  477 => 128,  473 => 127,  469 => 126,  465 => 125,  459 => 121,  456 => 120,  452 => 119,  435 => 115,  432 => 114,  430 => 113,  413 => 112,  410 => 111,  406 => 110,  399 => 107,  395 => 106,  391 => 102,  390 => 101,  388 => 100,  386 => 98,  385 => 97,  384 => 96,  382 => 95,  378 => 94,  370 => 90,  356 => 87,  354 => 86,  352 => 85,  334 => 84,  330 => 83,  323 => 80,  319 => 79,  312 => 76,  308 => 75,  301 => 72,  297 => 71,  290 => 68,  286 => 67,  279 => 64,  275 => 63,  268 => 60,  264 => 59,  255 => 55,  249 => 53,  246 => 52,  242 => 51,  234 => 47,  231 => 46,  227 => 45,  219 => 40,  214 => 39,  212 => 37,  210 => 35,  207 => 34,  204 => 33,  200 => 32,  195 => 28,  193 => 27,  191 => 26,  189 => 25,  188 => 22,  183 => 21,  181 => 20,  172 => 16,  170 => 15,  168 => 14,  166 => 13,  164 => 12,  160 => 11,  154 => 8,  153 => 7,  149 => 6,  146 => 5,  143 => 4,  141 => 3,  139 => 2,  135 => 1,  131 => 149,  128 => 148,  126 => 145,  123 => 144,  121 => 119,  118 => 118,  116 => 110,  113 => 109,  111 => 106,  108 => 105,  106 => 94,  103 => 93,  101 => 83,  98 => 82,  96 => 79,  93 => 78,  91 => 75,  88 => 74,  86 => 71,  83 => 70,  81 => 67,  78 => 66,  76 => 63,  73 => 62,  71 => 59,  68 => 58,  66 => 51,  63 => 50,  61 => 45,  58 => 44,  56 => 32,  54 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/layouts/default/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/layouts/default/layout.html.twig");
    }
}
