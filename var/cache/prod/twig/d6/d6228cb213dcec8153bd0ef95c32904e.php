<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNote/Note/js/view.html.twig */
class __TwigTemplate_f3bdbaf20c8101f0f7609bafb2d8d234 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script type=\"text/html\" id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html_attr");
        echo "\">
    <div class=\"accordion-group\">
        <div class=\"accordion-heading collapse<% if (!collapsed) { %> show<% } %>\">
            <div class=\"title-item\">
                <a href=\"#accordion-item<%- id %>\" class=\"no-hash accordion-toggle<% if (collapsed) { %> collapsed<% } %>\"></a>
                <span class=\"visual\">
                    <% if (createdBy_avatarPicture and createdBy_avatarPicture.src) { %>
                        <span class=\"avatar\">
                            <picture>
                                <% _.each(typeof createdBy_avatarPicture.sources !== 'undefined' && createdBy_avatarPicture.sources ? createdBy_avatarPicture.sources : [], function(source) { %>
                                    <source srcset=\"<%- source.srcset %>\" type=\"<%- source.type %>\">
                                <% }); %>
                                <img src=\"<%- createdBy_avatarPicture.src %>\"/>
                            </picture>
                        </span>
                    <% } else { %>
                        <span class=\"avatar-placeholder fa-user-circle\" aria-hidden=\"true\"></span>
                    <% } %>

                    <% if (createdBy_url) { %>
                        <a class=\"user\" href=\"<%- createdBy_url %>\"><%- createdBy %></a>
                    <% } else if (createdBy) { %>
                        <span class=\"user\"><%- createdBy %></span>
                    <% } %>
                </span>
                <span class=\"details\">
                    <i class=\"date\"><%- createdAt %></i>
                </span>
            </div>
            <div class=\"message-item\">
                <div class=\"message\"><%- brief_message %></div>
            </div>
            <div class=\"actions\">
                <button class=\"btn btn-icon btn-lighter item-edit-button<% if (!editable) { %> disabled<% } %>\"
                    <% if (!editable) { %> disabled=\"disabled\"<% } %> title=\"";
        // line 35
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.action.edit"), "html_attr");
        echo "\">
                        <span class=\"fa-pencil-square-o\" aria-label=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.action.edit"), "html");
        echo "\"></span>
                ";
        // line 38
        echo "                </button><button
                    class=\"btn btn-icon btn-lighter item-remove-button<% if (!removable) { %> disabled<% } %>\"
                    <% if (!removable) { %> disabled=\"disabled\"<% } %> title=\"";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.action.remove"), "html_attr");
        echo "\">
                        <span class=\"fa-trash-o\" aria-label=\"";
        // line 41
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.action.remove"), "html");
        echo "\"></span>
                </button>
            </div>
        </div>
        <div class=\"accordion-body collapse<% if (!collapsed) { %> in<% } %>\" id=\"accordion-item<%- id %>\">
            <div class=\"message\">
                <%= message %>
            </div>
            <% if (hasUpdate) { %>
            <div class=\"details\">
                <div>
                    <%= _.template(
                        ";
        // line 53
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.widget_updated_by"));
        echo ",
                        { interpolate: /\\{\\{(.+?)\\}\\}/g }
                    )({
                        user: updatedBy_url
                            ? '<a class=\"user\" href=\"' + updatedBy_url + '\">' +  _.escape(updatedBy) + '</a>'
                            : '<i class=\"user\">' +  _.escape(updatedBy) + '</i>',
                        date: '<i class=\"date\">' + updatedAt + '</i>'
                    }) %>
                </div>
            </div>
            <% } %>
        </div>
    </div>
</script>
";
    }

    public function getTemplateName()
    {
        return "@OroNote/Note/js/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 53,  91 => 41,  87 => 40,  83 => 38,  79 => 36,  75 => 35,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNote/Note/js/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NoteBundle/Resources/views/Note/js/view.html.twig");
    }
}
