<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/variables.scss */
class __TwigTemplate_3283c6de0e651a1c9d06ff7497683970 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import 'variables/variables';
@import 'variables/accordion-variables';
@import 'variables/attribute-item';
@import 'variables/flash-messages-variables';
@import 'variables/app-header';
@import 'variables/content-sidebar';
@import 'variables/dialog-variables';
@import 'variables/form-description';
@import 'variables/form-variables';
@import 'variables/layout';
@import 'variables/main-menu-variables';
@import 'variables/nav';
@import 'variables/page-header';
@import 'variables/select2-variables';
@import 'variables/scrollspy-variables';
@import 'variables/popover';
@import 'variables/jstree';
@import 'variables/widget-picker-variables';
@import 'variables/login-variables'
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/variables.scss");
    }
}
