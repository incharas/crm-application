<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/body-scroll-locker.js */
class __TwigTemplate_eb4f68f321a7f9783037a72bbd6d2185 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "const lockers = {};
let isScrollLocked = false;

const scrollUpdate = () => {
    if (isScrollLocked === Object.keys(lockers).length > 0) {
        // not all lockers are released
        return;
    }

    isScrollLocked = !isScrollLocked;
    if (isScrollLocked) {
        const scrollY = window.scrollY;
        document.body.style.position = 'fixed';
        document.body.style.top = `-\${scrollY}px`;
    } else {
        const scrollY = document.body.style.top;
        document.body.style.position = '';
        document.body.style.top = '';
        window.scrollTo(0, parseInt(scrollY || '0') * -1);
    }
};

export default {
    addLocker(cid) {
        lockers[cid] = true;
        scrollUpdate();
    },

    removeLocker(cid) {
        delete lockers[cid];
        scrollUpdate();
    }
};
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/body-scroll-locker.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/body-scroll-locker.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/body-scroll-locker.js");
    }
}
