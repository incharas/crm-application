<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroMarketingList/MarketingList/update.html.twig */
class __TwigTemplate_8eb1515abab2bfa65a9095107070e75b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head_script' => [$this, 'block_head_script'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["QD"] = $this->macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroMarketingList/MarketingList/update.html.twig", 2)->unwrap();
        // line 3
        $macros["segmentQD"] = $this->macros["segmentQD"] = $this->loadTemplate("@OroSegment/macros.html.twig", "@OroMarketingList/MarketingList/update.html.twig", 3)->unwrap();
        // line 4
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroMarketingList/MarketingList/update.html.twig", 4)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entityName%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "name", [], "any", false, false, false, 6)]]);
        // line 7
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 7), "value", [], "any", false, false, false, 7), "id", [], "any", false, false, false, 7)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroMarketingList/MarketingList/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_head_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $this->displayParentBlock("head_script", $context, $blocks);
        echo "

    ";
        // line 12
        $this->displayBlock('stylesheets', $context, $blocks);
    }

    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'stylesheet');
        echo "
    ";
    }

    // line 17
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMarketingList/MarketingList/update.html.twig", 18)->unwrap();
        // line 19
        echo "
    ";
        // line 20
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20), "id", [], "any", false, false, false, 20) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20)))) {
            // line 21
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_marketinglist", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["form"] ?? null), "vars", [], "any", false, false, false, 22), "value", [], "any", false, false, false, 22), "id", [], "any", false, false, false, 22)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-marketing-list", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "value", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.entity_label")]], 21, $context, $this->getSourceContext());
            // line 28
            echo "

        ";
            // line 30
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 30, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 32
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_index")], 32, $context, $this->getSourceContext());
        echo "
    ";
        // line 33
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_marketing_list_view", "params" => ["id" => "\$id"]]], 33, $context, $this->getSourceContext());
        // line 37
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_contactus_request_create")) {
            // line 38
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_marketing_list_create"]], 38, $context, $this->getSourceContext()));
            // line 41
            echo "    ";
        }
        // line 42
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 42), "value", [], "any", false, false, false, 42), "id", [], "any", false, false, false, 42) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_marketing_list_update"))) {
            // line 43
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_marketing_list_update", "params" => ["id" => "\$id"]]], 43, $context, $this->getSourceContext()));
            // line 47
            echo "    ";
        }
        // line 48
        echo "    ";
        // line 49
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 49, $context, $this->getSourceContext());
        echo "
";
    }

    // line 52
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 53), "value", [], "any", false, false, false, 53), "id", [], "any", false, false, false, 53)) {
            // line 54
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 55
($context["form"] ?? null), "vars", [], "any", false, false, false, 55), "value", [], "any", false, false, false, 55), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_marketing_list_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 58
($context["entity"] ?? null), "name", [], "any", false, false, false, 58)];
            // line 60
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 62
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.entity_label")]);
            // line 63
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroMarketingList/MarketingList/update.html.twig", 63)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 64
            echo "    ";
        }
    }

    // line 67
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 68
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroMarketingList/MarketingList/update.html.twig", 68)->unwrap();
        // line 69
        echo "
    ";
        // line 70
        $context["id"] = "marketing-list-profile";
        // line 71
        echo "    ";
        $context["ownerDataBlock"] = ["dataBlocks" => [0 => ["subblocks" => [0 => ["data" => []]]]]];
        // line 78
        echo "
    ";
        // line 79
        $context["ownerDataBlock"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->processForm($this->env, ($context["ownerDataBlock"] ?? null), ($context["form"] ?? null));
        // line 80
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 87
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 87), 'row', ["label" => "oro.marketinglist.name.label"]), 1 =>         // line 88
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "union", [], "any", false, false, false, 88), 'row', ["label" => "oro.marketinglist.union.label", "tooltip" => "oro.marketinglist.union.tooltip"]), 2 =>         // line 92
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 92), 'row', ["label" => "oro.marketinglist.description.label", "attr" => ["class" => "segment-descr"]])]], 1 => ["title" => "", "data" => twig_array_merge([0 =>         // line 103
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 103), 'row', ["label" => "oro.marketinglist.entity.label"]), 1 =>         // line 104
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 104), 'row', ["label" => "oro.marketinglist.type.label"])], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_1 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 105
($context["ownerDataBlock"] ?? null), "dataBlocks", [], "any", false, false, false, 105)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[0] ?? null) : null), "subblocks", [], "any", false, false, false, 105)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null), "data", [], "any", false, false, false, 105))]]]];
        // line 110
        echo "
    ";
        // line 112
        echo "    ";
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 112), "value", [], "any", false, false, false, 112), "manual", [], "any", false, false, false, 112)) {
            // line 113
            echo "    ";
            $context["hasEntity"] = (( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 113), "value", [], "any", false, false, false, 113)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 113), "value", [], "any", false, true, false, 113), "entity", [], "any", true, true, false, 113)) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 113), "value", [], "any", false, false, false, 113), "entity", [], "any", false, false, false, 113)));
            // line 114
            echo "    ";
            $context["columnsComponentOptions"] = ["formSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 115
($context["form"] ?? null), "vars", [], "any", false, false, false, 115), "id", [], "any", false, false, false, 115)), "entityChoiceSelector" => "[data-ftid=oro_marketing_list_form_entity]", "fieldsChoiceSelector" => "#contact-information-fields-list", "contactInformationFields" => ((            // line 118
($context["hasEntity"] ?? null)) ? ($this->extensions['Oro\Bundle\MarketingListBundle\Twig\ContactInformationFieldsExtension']->getContactInformationFieldsInfo(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 118), "value", [], "any", false, false, false, 118), "entity", [], "any", false, false, false, 118))) : ([]))];
            // line 120
            echo "
    ";
            // line 121
            $context["type"] = "oro_marketing_list";
            // line 122
            echo "    ";
            ob_start(function () { return ''; });
            // line 123
            echo "    <div data-page-component-module=\"oromarketinglist/js/app/components/columns-component\"
         data-page-component-options=\"";
            // line 124
            echo twig_escape_filter($this->env, json_encode(($context["columnsComponentOptions"] ?? null)), "html", null, true);
            echo "\"
         class=\"marketing-list-qd-columns\"
    >
        <div id=\"column-information-notice\" class=\"alert alert-info\" role=\"alert\">
            <strong>";
            // line 128
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.form.importance"), "html", null, true);
            echo "</strong>:
            ";
            // line 129
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.form.columns_notice"), "html", null, true);
            echo "
            <div class=\"column-information-fields-notice\">";
            // line 130
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.marketinglist.form.available_fields_notice"), "html", null, true);
            echo ":</div>
            <div id=\"contact-information-fields-list\"></div>
        </div>

        ";
            // line 134
            echo twig_call_macro($macros["QD"], "macro_query_designer_column_form", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 135
($context["form"] ?? null), "column", [], "any", false, false, false, 135), ["id" => (            // line 136
($context["type"] ?? null) . "-column-form")], [], [0 => "column", 1 => "label", 2 => "sorting", 3 => "action"]], 134, $context, $this->getSourceContext());
            // line 139
            echo "

        ";
            // line 141
            echo twig_call_macro($macros["QD"], "macro_query_designer_column_list", [["id" => (            // line 142
($context["type"] ?? null) . "-column-list"), "rowId" => (($context["type"] ?? null) . "-column-row")], [0 => "column", 1 => "label", 2 => "sorting", 3 => "action"]], 141, $context, $this->getSourceContext());
            // line 144
            echo "
    </div>
    ";
            $context["columnsDesigner"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 147
            echo "
    ";
            // line 148
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.form.designer"), "content_attr" => ["id" => (            // line 150
($context["type"] ?? null) . "-designer")], "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.form.columns"), "spanClass" => (            // line 154
($context["type"] ?? null) . "-columns responsive-cell"), "data" => [0 =>             // line 156
($context["columnsDesigner"] ?? null)]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.segment.form.filters"), "spanClass" => (            // line 161
($context["type"] ?? null) . "-filters responsive-cell"), "data" => [0 => twig_call_macro($macros["segmentQD"], "macro_query_designer_condition_builder", [["id" => (            // line 164
($context["type"] ?? null) . "-condition-builder"), "currentSegmentId" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 165
($context["entity"] ?? null), "id", [], "any", true, true, false, 165)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 165), null)) : (null)), "page_limit" => twig_constant("\\Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager::PER_PAGE"), "metadata" =>             // line 167
($context["metadata"] ?? null)]], 163, $context, $this->getSourceContext())]]]]]);
            // line 173
            echo "    ";
        }
        // line 174
        echo "
    ";
        // line 175
        $context["data"] = ["formErrors" => ((        // line 176
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 177
($context["dataBlocks"] ?? null), "hiddenData" =>         // line 178
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "definition", [], "any", false, false, false, 178), 'widget')];
        // line 180
        echo "
    ";
        // line 181
        echo twig_call_macro($macros["UI"], "macro_scrollData", [($context["id"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null), ($context["form"] ?? null)], 181, $context, $this->getSourceContext());
        echo "

    ";
        // line 184
        echo "    ";
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 184), "value", [], "any", false, false, false, 184), "manual", [], "any", false, false, false, 184)) {
            // line 185
            echo "        ";
            echo twig_call_macro($macros["QD"], "macro_query_designer_column_chain_template", ["column-chain-template"], 185, $context, $this->getSourceContext());
            echo "
        ";
            // line 186
            echo twig_call_macro($macros["segmentQD"], "macro_initJsWidgets", [($context["type"] ?? null), ($context["form"] ?? null), ($context["entities"] ?? null), ($context["metadata"] ?? null)], 186, $context, $this->getSourceContext());
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroMarketingList/MarketingList/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  306 => 186,  301 => 185,  298 => 184,  293 => 181,  290 => 180,  288 => 178,  287 => 177,  286 => 176,  285 => 175,  282 => 174,  279 => 173,  277 => 167,  276 => 165,  275 => 164,  274 => 161,  273 => 156,  272 => 154,  271 => 150,  270 => 148,  267 => 147,  262 => 144,  260 => 142,  259 => 141,  255 => 139,  253 => 136,  252 => 135,  251 => 134,  244 => 130,  240 => 129,  236 => 128,  229 => 124,  226 => 123,  223 => 122,  221 => 121,  218 => 120,  216 => 118,  215 => 115,  213 => 114,  210 => 113,  207 => 112,  204 => 110,  202 => 105,  201 => 104,  200 => 103,  199 => 92,  198 => 88,  197 => 87,  195 => 80,  193 => 79,  190 => 78,  187 => 71,  185 => 70,  182 => 69,  179 => 68,  175 => 67,  170 => 64,  167 => 63,  164 => 62,  158 => 60,  156 => 58,  155 => 55,  153 => 54,  150 => 53,  146 => 52,  139 => 49,  137 => 48,  134 => 47,  131 => 43,  128 => 42,  125 => 41,  122 => 38,  119 => 37,  117 => 33,  112 => 32,  107 => 30,  103 => 28,  101 => 26,  100 => 22,  98 => 21,  96 => 20,  93 => 19,  90 => 18,  86 => 17,  79 => 13,  72 => 12,  66 => 10,  62 => 9,  57 => 1,  55 => 7,  53 => 6,  50 => 4,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroMarketingList/MarketingList/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/MarketingListBundle/Resources/views/MarketingList/update.html.twig");
    }
}
