<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Widget/entityWorkflows.html.twig */
class __TwigTemplate_b53aa51b5880f87542f18ce05d0b4411 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["entity_class"] = ((array_key_exists("entity", $context)) ? ($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null))) : (null));
        // line 2
        if (( !twig_test_empty(($context["entity_class"] ?? null)) && $this->extensions['Oro\Bundle\WorkflowBundle\Twig\WorkflowExtension']->hasApplicableWorkflows(($context["entity"] ?? null)))) {
            // line 3
            echo "    ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_widget_entity_workflows", ["entityClass" =>             // line 7
($context["entity_class"] ?? null), "entityId" => ((            // line 8
array_key_exists("entity", $context)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 8)) : (0))]), "alias" => "workflows"]);
            // line 12
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/entityWorkflows.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 12,  44 => 8,  43 => 7,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/entityWorkflows.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/entityWorkflows.html.twig");
    }
}
