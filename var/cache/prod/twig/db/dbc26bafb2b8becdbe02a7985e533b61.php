<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChannel/macros.html.twig */
class __TwigTemplate_acb815cfe1320a68e0e59bece9913f65 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "
";
        // line 47
        echo "
";
    }

    // line 7
    public function macro_initializeChannelForm($__form__ = null, $__entitiesMetadata__ = null, $__customerIdentity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "entitiesMetadata" => $__entitiesMetadata__,
            "customerIdentity" => $__customerIdentity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 8
            echo "    ";
            $context["options"] = ["channelTypeEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["form"] ?? null), "channelType", [], "any", false, false, false, 9), "vars", [], "any", false, false, false, 9), "id", [], "any", false, false, false, 9)), "channelEntitiesEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["form"] ?? null), "entities", [], "any", false, false, false, 10), "vars", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)), "customerIdentity" =>             // line 11
($context["customerIdentity"] ?? null), "entitiesMetadata" =>             // line 12
($context["entitiesMetadata"] ?? null), "fields" => ["name" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["form"] ?? null), "name", [], "any", false, false, false, 14), "vars", [], "any", false, false, false, 14), "id", [], "any", false, false, false, 14)), "channelType" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["form"] ?? null), "channelType", [], "any", false, false, false, 15), "vars", [], "any", false, false, false, 15), "id", [], "any", false, false, false, 15)), "tokenEl" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 16
($context["form"] ?? null), "_token", [], "any", false, false, false, 16), "vars", [], "any", false, false, false, 16), "id", [], "any", false, false, false, 16))]];
            // line 19
            echo "
    <div data-page-component-module=\"orochannel/js/app/components/channel\"
         data-page-component-options=\"";
            // line 21
            echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
            echo "\"></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 29
    public function macro_inializeEntitiesViewComponent($__channel__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "channel" => $__channel__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 30
            echo "    ";
            $context["containerIdentifier"] = "entities-list-view";
            // line 31
            echo "
    <div id=\"";
            // line 32
            echo twig_escape_filter($this->env, ($context["containerIdentifier"] ?? null), "html", null, true);
            echo "\"></div>
    <script>
        loadModules(['jquery', 'orochannel/js/entity-management/entity-component-view'],
        function (\$, EntityComponentView) {
            var entityComponentView = new EntityComponentView({
                data:     ";
            // line 37
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["channel"] ?? null), "entities", [], "any", false, false, false, 37));
            echo ",
                mode:     EntityComponentView.prototype.MODES.VIEW_MODE,
                metadata: ";
            // line 39
            echo json_encode($this->extensions['Oro\Bundle\ChannelBundle\Twig\ChannelExtension']->getEntitiesMetadata());
            echo "
            });

            entityComponentView.render();
            \$(";
            // line 43
            echo json_encode(("#" . ($context["containerIdentifier"] ?? null)));
            echo ").append(entityComponentView.\$el);
        });
    </script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 55
    public function macro_renderChannelProperty($__entity__ = null, $__params__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "params" => $__params__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 56
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroChannel/macros.html.twig", 56)->unwrap();
            // line 57
            echo "
    ";
            // line 58
            $context["label"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["params"] ?? null), "label", [], "any", true, true, false, 58)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 59
($context["params"] ?? null), "label", [], "any", false, false, false, 59)) : ($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue("Oro\\Bundle\\ChannelBundle\\Entity\\Channel", "label")));
            // line 62
            echo "
    ";
            // line 63
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), twig_call_macro($macros["UI"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dataChannel", [], "any", false, false, false, 63), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dataChannel", [], "any", false, false, false, 63)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dataChannel", [], "any", false, false, false, 63), "name", [], "any", false, false, false, 63)) : (null)), "oro_channel_view"], 63, $context, $this->getSourceContext())], 63, $context, $this->getSourceContext());
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroChannel/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  163 => 63,  160 => 62,  158 => 59,  157 => 58,  154 => 57,  151 => 56,  137 => 55,  124 => 43,  117 => 39,  112 => 37,  104 => 32,  101 => 31,  98 => 30,  85 => 29,  74 => 21,  70 => 19,  68 => 16,  67 => 15,  66 => 14,  65 => 12,  64 => 11,  63 => 10,  62 => 9,  60 => 8,  45 => 7,  40 => 47,  37 => 23,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChannel/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ChannelBundle/Resources/views/macros.html.twig");
    }
}
