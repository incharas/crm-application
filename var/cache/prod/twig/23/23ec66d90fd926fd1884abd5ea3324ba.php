<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/input-widgets.js */
class __TwigTemplate_9f0f69ac14f908c9b76cd47cfa62e838 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import InputWidgetManager from 'oroui/js/input-widget-manager';
import UniformSelectInputWidget from 'oroui/js/app/views/input-widget/uniform-select';
import UniformFileInputWidget from 'oroui/js/app/views/input-widget/uniform-file';
import Select2InputWidget from 'oroui/js/app/views/input-widget/select2';
import NumberInputWidget from 'oroui/js/app/views/input-widget/number';
import ClearableInputWidget from 'oroui/js/app/views/input-widget/clearable';

InputWidgetManager.addWidget('uniform-select', {
    selector: 'select:not(.no-uniform):not([multiple])',
    Widget: UniformSelectInputWidget
});

InputWidgetManager.addWidget('uniform-file', {
    selector: 'input:file:not(.no-uniform)',
    Widget: UniformFileInputWidget
});

InputWidgetManager.addWidget('select2', {
    selector: 'select,input',
    disableAutoCreate: true,
    Widget: Select2InputWidget
});

InputWidgetManager.addWidget('number', {
    selector: 'input[type=\"number\"]',
    Widget: NumberInputWidget
});

InputWidgetManager.addWidget('clearable', {
    selector: 'input[data-clearable]',
    Widget: ClearableInputWidget
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/input-widgets.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/input-widgets.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/input-widgets.js");
    }
}
