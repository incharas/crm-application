<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAttachment/Attachment/Datagrid/Property/fileSize.html.twig */
class __TwigTemplate_f25530bff05b09c3cb6652748b22418d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileSize(($context["value"] ?? null)), "html", null, true);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroAttachment/Attachment/Datagrid/Property/fileSize.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAttachment/Attachment/Datagrid/Property/fileSize.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/AttachmentBundle/Resources/views/Attachment/Datagrid/Property/fileSize.html.twig");
    }
}
