<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroForm/Form/fields.html.twig */
class __TwigTemplate_c782cbeb59f24d97212057793d0ca5e9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'checkbox_widget' => [$this, 'block_checkbox_widget'],
            'radio_widget' => [$this, 'block_radio_widget'],
            'form_widget_simple' => [$this, 'block_form_widget_simple'],
            'oro_select2_widget' => [$this, 'block_oro_select2_widget'],
            'oro_select2_javascript' => [$this, 'block_oro_select2_javascript'],
            'oro_select2_javascript_prototype' => [$this, 'block_oro_select2_javascript_prototype'],
            'oro_select2_hidden_row' => [$this, 'block_oro_select2_hidden_row'],
            'form_javascript' => [$this, 'block_form_javascript'],
            'form_stylesheet' => [$this, 'block_form_stylesheet'],
            'form_row_collection' => [$this, 'block_form_row_collection'],
            'oro_ticker_symbol_widget' => [$this, 'block_oro_ticker_symbol_widget'],
            'oro_multiple_entity_widget' => [$this, 'block_oro_multiple_entity_widget'],
            'form_label' => [$this, 'block_form_label'],
            'oro_money_row' => [$this, 'block_oro_money_row'],
            'oro_money_widget' => [$this, 'block_oro_money_widget'],
            'percent_row' => [$this, 'block_percent_row'],
            'percent_widget' => [$this, 'block_percent_widget'],
            'oro_date_widget' => [$this, 'block_oro_date_widget'],
            'oro_datetime_widget' => [$this, 'block_oro_datetime_widget'],
            'oro_collection_widget' => [$this, 'block_oro_collection_widget'],
            'oro_form_js_validation' => [$this, 'block_oro_form_js_validation'],
            'oro_entity_create_or_select_row' => [$this, 'block_oro_entity_create_or_select_row'],
            'oro_autocomplete_widget' => [$this, 'block_oro_autocomplete_widget'],
            'oro_entity_create_or_select_widget' => [$this, 'block_oro_entity_create_or_select_widget'],
            'oro_entity_create_or_select_choice_widget' => [$this, 'block_oro_entity_create_or_select_choice_widget'],
            'oro_entity_create_or_select_inline_widget' => [$this, 'block_oro_entity_create_or_select_inline_widget'],
            'oro_link_type_widget' => [$this, 'block_oro_link_type_widget'],
            'oro_download_links_type_widget' => [$this, 'block_oro_download_links_type_widget'],
            'oro_simple_color_picker_row' => [$this, 'block_oro_simple_color_picker_row'],
            'oro_simple_color_picker_widget' => [$this, 'block_oro_simple_color_picker_widget'],
            'oro_simple_color_choice_widget' => [$this, 'block_oro_simple_color_choice_widget'],
            'oro_simple_color_choice_widget_options' => [$this, 'block_oro_simple_color_choice_widget_options'],
            'oro_color_table_row' => [$this, 'block_oro_color_table_row'],
            'oro_color_table_widget' => [$this, 'block_oro_color_table_widget'],
            'oro_resizeable_rich_text_widget' => [$this, 'block_oro_resizeable_rich_text_widget'],
            'oro_entity_tree_select_row' => [$this, 'block_oro_entity_tree_select_row'],
            'oro_entity_tree_select_widget' => [$this, 'block_oro_entity_tree_select_widget'],
            'button_row' => [$this, 'block_button_row'],
            'oro_checkbox_widget' => [$this, 'block_oro_checkbox_widget'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/Form/fields.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroUI/Form/fields.html.twig", "@OroForm/Form/fields.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_checkbox_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "<input type=\"checkbox\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "\"";
        }
        if (($context["checked"] ?? null)) {
            echo " checked=\"checked\"";
        }
        echo ">";
    }

    // line 7
    public function block_radio_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "<input type=\"radio\" ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (array_key_exists("value", $context)) {
            echo " value=\"";
            echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            echo "\"";
        }
        if (($context["checked"] ?? null)) {
            echo " checked=\"checked\"";
        }
        echo ">";
    }

    // line 11
    public function block_form_widget_simple($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter(($context["type"] ?? null), "text")) : ("text"));
        // line 13
        echo "    ";
        // line 14
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "placeholder", [], "any", true, true, false, 14) && !twig_in_filter(($context["type"] ?? null), [0 => "email", 1 => "number", 2 => "password", 3 => "search", 4 => "tel", 5 => "text", 6 => "url"]))) {
            // line 15
            echo "        ";
            $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["data-placeholder" => (((            // line 16
($context["translation_domain"] ?? null) === false)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "placeholder", [], "any", false, false, false, 16)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "placeholder", [], "any", false, false, false, 16), ($context["attr_translation_parameters"] ?? null), ($context["translation_domain"] ?? null))))]);
            // line 18
            echo "        ";
            $context["attr"] = twig_array_filter($this->env, ($context["attr"] ?? null), function ($__v__, $__k__) use ($context, $macros) { $context["v"] = $__v__; $context["k"] = $__k__; return (($context["k"] ?? null) != "placeholder"); });
            // line 19
            echo "    ";
        }
        // line 20
        echo "    ";
        // line 21
        echo "    ";
        if ((array_key_exists("required", $context) && !twig_in_filter(($context["type"] ?? null), [0 => "text", 1 => "search", 2 => "url", 3 => "tel", 4 => "email", 5 => "password", 6 => "date", 7 => "month", 8 => "week", 9 => "time", 10 => "datetime-local", 11 => "number", 12 => "checkbox", 13 => "radio", 14 => "file"]))) {
            // line 22
            echo "        ";
            if (($context["required"] ?? null)) {
                // line 23
                echo "            ";
                $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["x-required" => "required"]);
                // line 26
                echo "        ";
            }
            // line 27
            echo "        ";
            $context["required"] = null;
            // line 28
            echo "    ";
        }
        // line 29
        echo "    ";
        $this->displayParentBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 32
    public function block_oro_select2_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "grid", [], "any", true, true, false, 33) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "grid", [], "any", false, true, false, 33), "name", [], "any", true, true, false, 33))) {
            // line 34
            echo "        ";
            $context["url"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_datagrid_index", ["gridName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "grid", [], "any", false, false, false, 34), "name", [], "any", false, false, false, 34)]);
            // line 35
            echo "    ";
        } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "route_name", [], "any", true, true, false, 35) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "route_name", [], "any", false, false, false, 35))) {
            // line 36
            echo "        ";
            $context["url"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "route_name", [], "any", false, false, false, 36), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "route_parameters", [], "any", true, true, false, 36)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "route_parameters", [], "any", false, false, false, 36), [])) : ([])));
            // line 37
            echo "    ";
        } else {
            // line 38
            echo "        ";
            $context["url"] = "";
            // line 39
            echo "    ";
        }
        // line 40
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "placeholder", [], "any", true, true, false, 40)) {
            // line 41
            echo "        ";
            $context["configs"] = twig_array_merge(($context["configs"] ?? null), ["placeholder" => twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "placeholder", [], "any", false, false, false, 41)))]);
            // line 42
            echo "    ";
        }
        // line 43
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "result_template_twig", [], "any", true, true, false, 43) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "result_template_twig", [], "any", false, false, false, 43))) {
            // line 44
            echo "        ";
            $context["configs"] = twig_array_merge(($context["configs"] ?? null), ["result_template" => twig_include($this->env, $context, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "result_template_twig", [], "any", false, false, false, 44))]);
            // line 45
            echo "    ";
        }
        // line 46
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "selection_template_twig", [], "any", true, true, false, 46) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "selection_template_twig", [], "any", false, false, false, 46))) {
            // line 47
            echo "        ";
            $context["configs"] = twig_array_merge(($context["configs"] ?? null), ["selection_template" => twig_include($this->env, $context, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "selection_template_twig", [], "any", false, false, false, 47))]);
            // line 48
            echo "    ";
        }
        // line 49
        echo "    ";
        $context["configs"] = twig_array_merge(["containerCssClass" => "oro-select2", "dropdownCssClass" => "oro-select2__dropdown"],         // line 52
($context["configs"] ?? null));
        // line 53
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "component", [], "any", true, true, false, 53)) {
            // line 54
            echo "        ";
            $context["component"] = (("oro/select2-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "component", [], "any", false, false, false, 54)) . "-component");
            // line 55
            echo "    ";
        } else {
            // line 56
            echo "        ";
            $context["component"] = "oro/select2-component";
            // line 57
            echo "    ";
        }
        // line 58
        echo "    ";
        if ( !array_key_exists("component_options", $context)) {
            // line 59
            echo "        ";
            $context["component_options"] = [];
            // line 60
            echo "    ";
        }
        // line 61
        echo "    ";
        $context["component_options"] = twig_array_merge(($context["component_options"] ?? null), ["configs" => ($context["configs"] ?? null), "url" => ($context["url"] ?? null)]);
        // line 62
        echo "    ";
        if (array_key_exists("excluded", $context)) {
            // line 63
            echo "        ";
            $context["component_options"] = twig_array_merge(($context["component_options"] ?? null), ["excluded" => ($context["excluded"] ?? null)]);
            // line 64
            echo "    ";
        }
        // line 65
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ["class" => "select2", "data-page-component-module" =>         // line 67
($context["component"] ?? null), "data-page-component-options" => json_encode(        // line 68
($context["component_options"] ?? null))]]);
        // line 69
        echo "
";
    }

    // line 72
    public function block_oro_select2_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 73
        echo "    ";
        $this->displayBlock('oro_select2_javascript_prototype', $context, $blocks);
    }

    public function block_oro_select2_javascript_prototype($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 76
    public function block_oro_select2_hidden_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 77
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        echo "
";
    }

    // line 80
    public function block_form_javascript($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 81
        echo "    ";
        ob_start(function () { return ''; });
        // line 82
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 83
            echo "            ";
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderJavascript($context["child"]);
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 85
        echo "    ";
        $___internal_parse_59_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 81
        echo twig_spaceless($___internal_parse_59_);
    }

    // line 88
    public function block_form_stylesheet($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "    ";
        ob_start(function () { return ''; });
        // line 90
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 91
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'stylesheet');
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "    ";
        $___internal_parse_60_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 89
        echo twig_spaceless($___internal_parse_60_);
    }

    // line 96
    public function block_form_row_collection($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 97
        echo "    ";
        ob_start(function () { return ''; });
        // line 98
        echo "        ";
        $macros["__internal_parse_62"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroForm/Form/fields.html.twig", 98)->unwrap();
        // line 99
        echo "        <div class=\"control-group";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 99)) {
            echo " ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 99), "html", null, true);
        }
        if (array_key_exists("block_prefixes", $context)) {
            echo " control-group-";
            echo twig_escape_filter($this->env, (($__internal_compile_0 = ($context["block_prefixes"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[1] ?? null) : null), "html", null, true);
        }
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["group_attr"] ?? null), "class", [], "any", true, true, false, 99)) {
            echo " ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["group_attr"] ?? null), "class", [], "any", false, false, false, 99), "html", null, true);
        }
        echo "\"";
        if (array_key_exists("group_attr", $context)) {
            $this->displayBlock("group_attributes", $context, $blocks);
        }
        echo ">
            ";
        // line 100
        if ((((array_key_exists("hint", $context)) ? (_twig_default_filter(($context["hint"] ?? null))) : ("")) && (((array_key_exists("hint_position", $context)) ? (_twig_default_filter(($context["hint_position"] ?? null))) : ("")) == "above"))) {
            // line 101
            echo "                <div";
            $this->displayBlock("hint_attributes", $context, $blocks);
            echo ">";
            echo ($context["hint"] ?? null);
            echo "</div>
            ";
        }
        // line 103
        echo "            ";
        if ( !(($context["label"] ?? null) === false)) {
            // line 104
            echo "                <div class=\"control-label wrap\">
                    ";
            // line 105
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["label_attr" => ($context["label_attr"] ?? null)]);
            echo "
                </div>
            ";
        }
        // line 108
        echo "            <div class=\"controls";
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            echo " validation-error";
        }
        echo "\">
                <div class=\"row-oro\">
                    <div class=\"oro-item-collection collection-fields-list\" data-prototype=\"";
        // line 110
        echo twig_escape_filter($this->env, twig_call_macro($macros["__internal_parse_62"], "macro_collection_prototype", [($context["form"] ?? null)], 110, $context, $this->getSourceContext()));
        echo "\">
                        ";
        // line 111
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 111));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            echo twig_call_macro($macros["__internal_parse_62"], "macro_collection_prototype", [$context["child"]], 111, $context, $this->getSourceContext());
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 112
        echo "                    </div>
                    ";
        // line 113
        if (($context["allow_add"] ?? null)) {
            echo "<a class=\"btn add-list-item\" href=\"#\" role=\"button\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["add_label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            echo "</a>";
        }
        // line 114
        echo "                </div>
                ";
        // line 115
        if ((((array_key_exists("hint", $context)) ? (_twig_default_filter(($context["hint"] ?? null))) : ("")) && (((array_key_exists("hint_position", $context)) ? (_twig_default_filter(($context["hint_position"] ?? null))) : ("")) == "after_input"))) {
            // line 116
            echo "                    <div";
            $this->displayBlock("hint_attributes", $context, $blocks);
            echo ">";
            echo ($context["hint"] ?? null);
            echo "</div>
                ";
        }
        // line 118
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        // line 119
        echo "</div>
            ";
        // line 120
        if ((((array_key_exists("hint", $context)) ? (_twig_default_filter(($context["hint"] ?? null))) : ("")) && (((array_key_exists("hint_position", $context)) ? (_twig_default_filter(($context["hint_position"] ?? null))) : ("")) == "below"))) {
            // line 121
            echo "                <div";
            $this->displayBlock("hint_attributes", $context, $blocks);
            echo ">";
            echo ($context["hint"] ?? null);
            echo "</div>
            ";
        }
        // line 123
        echo "        </div>
    ";
        $___internal_parse_61_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 97
        echo twig_spaceless($___internal_parse_61_);
    }

    // line 127
    public function block_oro_ticker_symbol_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 128
        echo "    <script>
        loadModules(['jquery', 'bootstrap'],
        function(\$){
            \$(function() {
                var cache = {};
                \$(\"#";
        // line 133
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\").typeahead({
                    source: function (request, process) {
                        YAHOO = {
                            Finance: {
                                SymbolSuggest: {
                                    ssCallback: function (data) {
                                        var result = \$.map(data.ResultSet.Result, function (item) {
                                            return item.name + \" (\" + item.symbol + \")\";
                                        });
                                        \$.each(data.ResultSet.Result, function (itemKey, item) {
                                            cache[item.name + \" (\" + item.symbol + \")\"] = item.symbol;
                                        });
                                        process(result)
                                    }
                                }
                            }
                        };
                        \$.ajax({
                            type: \"GET\",
                            dataType: \"jsonp\",
                            jsonp: \"callback\",
                            jsonpCallback: \"YAHOO.Finance.SymbolSuggest.ssCallback\",
                            data: {
                                query: request
                            },
                            cache: true,
                            url: \"http://autoc.finance.yahoo.com/autoc\"
                        });
                    },
                    updater: function(item) {
                        if (typeof cache[item] != 'undefined') {
                            return cache[item];
                        } else {
                            return item;
                        }
                    }
                });
            });
        });
    </script>

    ";
        // line 174
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    // line 177
    public function block_oro_multiple_entity_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 178
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "

    <div id=\"";
        // line 180
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "-container\"></div>

    <script>
        loadModules(['jquery',
            'oroform/js/multiple-entity', 'oroform/js/multiple-entity/collection', 'oroform/js/multiple-entity/model'
        ], function(\$, MultipleEntity, MultipleEntityCollection, MultipleEntityModel) {
            ";
        // line 186
        $context["selectionUrl"] = null;
        // line 187
        echo "            ";
        $context["originalFieldId"] = (($__internal_compile_1 = ($context["attr"] ?? null)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1["data-ftid"] ?? null) : null);
        // line 188
        echo "            ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 188), "grid_url", [], "any", true, true, false, 188) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 188), "grid_url", [], "any", false, false, false, 188))) {
            // line 189
            echo "                ";
            $context["selectionUrl"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 189), "grid_url", [], "any", false, false, false, 189);
            // line 190
            echo "            ";
        } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 190), "selection_url", [], "any", true, true, false, 190) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 190), "selection_url", [], "any", false, false, false, 190))) {
            // line 191
            echo "                ";
            $context["selectionUrl"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 191), "selection_url", [], "any", false, false, false, 191);
            // line 192
            echo "            ";
        }
        // line 193
        echo "            ";
        $context["selectionUrlMethod"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 193), "selection_url_method", [], "any", true, true, false, 193)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 193), "selection_url_method", [], "any", false, false, false, 193), null)) : (null));
        // line 194
        echo "            ";
        $context["selectionRouteName"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 194), "selection_route", [], "any", true, true, false, 194)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 194), "selection_route", [], "any", false, false, false, 194), null)) : (null));
        // line 195
        echo "            ";
        $context["selectionRouteParameters"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 195), "selection_route_parameters", [], "any", true, true, false, 195)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 195), "selection_route_parameters", [], "any", false, false, false, 195), [])) : ([]));
        // line 196
        echo "
            var widget = new MultipleEntity({
                el: '#";
        // line 198
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "-container',
                addedElement: 'input[data-ftid=\"";
        // line 199
        echo twig_escape_filter($this->env, ($context["originalFieldId"] ?? null), "html", null, true);
        echo "_added\"]',
                removedElement: 'input[data-ftid=\"";
        // line 200
        echo twig_escape_filter($this->env, ($context["originalFieldId"] ?? null), "html", null, true);
        echo "_removed\"]',
                name: ";
        // line 201
        echo json_encode(($context["id"] ?? null));
        echo ",
                defaultElement: ";
        // line 202
        echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 202), "default_element", [], "any", false, false, false, 202));
        echo ",
                selectionUrl: ";
        // line 203
        echo json_encode(($context["selectionUrl"] ?? null));
        echo ",
                selectionUrlMethod: ";
        // line 204
        echo json_encode(($context["selectionUrlMethod"] ?? null));
        echo ",
                selectionRouteName: ";
        // line 205
        echo json_encode(($context["selectionRouteName"] ?? null));
        echo ",
                selectionRouteParams: ";
        // line 206
        echo json_encode(($context["selectionRouteParameters"] ?? null), twig_constant("JSON_FORCE_OBJECT"));
        echo ",
                allowAction: ";
        // line 207
        echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 207), "allow_action", [], "any", false, false, false, 207));
        echo ",
                collection: new MultipleEntityCollection(),
                selectorWindowTitle: ";
        // line 209
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 209), "selector_window_title", [], "any", false, false, false, 209)));
        echo "
            });
            var data = [];
            ";
        // line 212
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["initial_elements"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
            // line 213
            echo "                data.push(new MultipleEntityModel(";
            echo json_encode($context["element"]);
            echo "));
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 215
        echo "            widget.getCollection().reset(data);

            ";
        // line 217
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 217), "extra_config", [], "any", true, true, false, 217) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 217), "extra_config", [], "any", false, false, false, 217))) {
            // line 218
            echo "                ";
            $this->displayBlock(("oro_multiple_entity_js_" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 218), "extra_config", [], "any", false, false, false, 218)), $context, $blocks);
            echo "
            ";
        }
        // line 220
        echo "        });
    </script>
";
    }

    // line 224
    public function block_form_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 225
        echo "    ";
        ob_start(function () { return ''; });
        // line 226
        echo "    ";
        if ( !(($context["label"] ?? null) === false)) {
            // line 227
            echo "        ";
            if ( !($context["compound"] ?? null)) {
                // line 228
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["for" => ($context["id"] ?? null)]);
                // line 229
                echo "        ";
            }
            // line 230
            echo "        ";
            if (($context["required"] ?? null)) {
                // line 231
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 231)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 231), "")) : ("")) . " required"))]);
                // line 232
                echo "        ";
            }
            // line 233
            echo "        ";
            if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
                // line 234
                echo "            ";
                $context["label_attr"] = twig_array_merge(($context["label_attr"] ?? null), ["class" => twig_trim_filter((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", true, true, false, 234)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["label_attr"] ?? null), "class", [], "any", false, false, false, 234), "")) : ("")) . " validation-error"))]);
                // line 235
                echo "        ";
            }
            // line 236
            echo "        ";
            if (twig_test_empty(($context["label"] ?? null))) {
                // line 237
                echo "            ";
                if ( !twig_test_empty(($context["label_format"] ?? null))) {
                    // line 238
                    $context["label"] = twig_replace_filter(($context["label_format"] ?? null), ["%name%" => ($context["name"] ?? null), "%id%" => ($context["id"] ?? null)]);
                    // line 239
                    echo "            ";
                } else {
                    // line 240
                    echo "                ";
                    $context["label"] = $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->humanize(($context["name"] ?? null));
                    // line 241
                    echo "            ";
                }
                // line 242
                echo "        ";
            }
            // line 243
            echo "        ";
            $context["isRadioLabel"] = (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 243), "vars", [], "any", false, true, false, 243), "expanded", [], "any", true, true, false, 243)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "parent", [], "any", false, true, false, 243), "vars", [], "any", false, true, false, 243), "expanded", [], "any", false, false, false, 243), false)) : (false)) && array_key_exists("checked", $context));
            // line 244
            echo "
        <label";
            // line 245
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["label_attr"] ?? null));
            foreach ($context['_seq'] as $context["attrname"] => $context["attrvalue"]) {
                echo " ";
                echo twig_escape_filter($this->env, $context["attrname"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["attrvalue"], "html", null, true);
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['attrname'], $context['attrvalue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo ">";
            // line 246
            if ((array_key_exists("tooltip", $context) && ($context["tooltip"] ?? null))) {
                // line 247
                $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroForm/Form/fields.html.twig", 247)->unwrap();
                // line 248
                echo "                ";
                echo twig_call_macro($macros["ui"], "macro_tooltip", [                // line 249
($context["tooltip"] ?? null), ((                // line 250
array_key_exists("tooltip_parameters", $context)) ? (_twig_default_filter(($context["tooltip_parameters"] ?? null), [])) : ([])), ((                // line 251
array_key_exists("tooltip_placement", $context)) ? (_twig_default_filter(($context["tooltip_placement"] ?? null), null)) : (null)), ((                // line 252
array_key_exists("tooltip_details_enabled", $context)) ? (_twig_default_filter(($context["tooltip_details_enabled"] ?? null), false)) : (false)), ((                // line 253
array_key_exists("tooltip_details_link", $context)) ? (_twig_default_filter(($context["tooltip_details_link"] ?? null), null)) : (null)), ((                // line 254
array_key_exists("tooltip_details_anchor", $context)) ? (_twig_default_filter(($context["tooltip_details_anchor"] ?? null), null)) : (null))], 248, $context, $this->getSourceContext());
            }
            // line 257
            if ((array_key_exists("translatable_label", $context) &&  !($context["translatable_label"] ?? null))) {
                // line 258
                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
            } elseif ((            // line 259
array_key_exists("raw_label", $context) && ($context["raw_label"] ?? null))) {
                // line 260
                echo ($context["label"] ?? null);
            } else {
                // line 262
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            }
            // line 264
            echo "<em>";
            if ((($context["required"] ?? null) &&  !($context["isRadioLabel"] ?? null))) {
                echo "*";
            } else {
                echo "&nbsp;";
            }
            echo "</em>
        </label>";
        }
        $___internal_parse_63_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 225
        echo twig_spaceless($___internal_parse_63_);
    }

    // line 270
    public function block_oro_money_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 271
        echo "    ";
        $context["label"] = ((($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)) . " (") . ($context["currency_symbol"] ?? null)) . ")");
        // line 272
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row', ["type" => "text", "label" => ($context["label"] ?? null), "translatable_label" => false]);
        echo "
";
    }

    // line 275
    public function block_oro_money_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 276
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter(($context["type"] ?? null), "text")) : ("text"));
        // line 277
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 280
    public function block_percent_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 281
        echo "    ";
        $context["label"] = ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null)) . " (%)");
        // line 282
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row', ["type" => "text", "label" => ($context["label"] ?? null), "translatable_label" => false]);
        echo "
";
    }

    // line 285
    public function block_percent_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 286
        echo "    ";
        $context["type"] = ((array_key_exists("type", $context)) ? (_twig_default_filter(($context["type"] ?? null), "text")) : ("text"));
        // line 287
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 290
    public function block_oro_date_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 291
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "data-validation", [], "any", true, true, false, 291)) {
            // line 292
            echo "        ";
            $context["dateValidation"] = (($__internal_compile_2 = ($context["attr"] ?? null)) && is_array($__internal_compile_2) || $__internal_compile_2 instanceof ArrayAccess ? ($__internal_compile_2["data-validation"] ?? null) : null);
            // line 293
            echo "    ";
        } else {
            // line 294
            echo "        ";
            $context["dateValidation"] = ["Date" => []];
            // line 295
            echo "
        ";
            // line 296
            if (($context["required"] ?? null)) {
                // line 297
                echo "            ";
                $context["dateValidation"] = twig_array_merge(($context["dateValidation"] ?? null), ["NotBlank" => ["message" => "This value should not be blank."]]);
                // line 298
                echo "        ";
            }
            // line 299
            echo "
        ";
            // line 300
            $context["dateValidation"] = json_encode(($context["dateValidation"] ?? null), twig_constant("JSON_FORCE_OBJECT"));
            // line 301
            echo "    ";
        }
        // line 302
        echo "
    ";
        // line 303
        $context["options"] = ["view" => "oroui/js/app/views/datepicker/datepicker-view", "nativeMode" => $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile(), "dateInputAttrs" => ["placeholder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.choose_date"), "id" =>         // line 308
($context["id"] ?? null), "name" =>         // line 309
($context["id"] ?? null), "data-validation" =>         // line 310
($context["dateValidation"] ?? null), "class" => ("datepicker-input " . ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 311
($context["attr"] ?? null), "class", [], "any", true, true, false, 311)) ? ((($__internal_compile_3 = ($context["attr"] ?? null)) && is_array($__internal_compile_3) || $__internal_compile_3 instanceof ArrayAccess ? ($__internal_compile_3["class"] ?? null) : null)) : (""))), "aria-live" => "assertive", "autocomplete" => "off", "autocorrect" => "off", "autocapitalize" => "off"], "datePickerOptions" => ["altFormat" => "yy-mm-dd", "changeMonth" => true, "changeYear" => true, "yearRange" => ((        // line 321
array_key_exists("years", $context)) ? (_twig_default_filter(($context["years"] ?? null), "-80:+1")) : ("-80:+1")), "minDate" =>         // line 322
($context["minDate"] ?? null), "maxDate" =>         // line 323
($context["maxDate"] ?? null), "showButtonPanel" => true]];
        // line 327
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["data-page-component-module" => "oroui/js/app/components/view-component", "data-page-component-options" => json_encode(        // line 329
($context["options"] ?? null), twig_constant("JSON_FORCE_OBJECT"))]);
        // line 331
        echo "
    ";
        // line 332
        $this->displayBlock("date_widget", $context, $blocks);
        echo "
";
    }

    // line 335
    public function block_oro_datetime_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 336
        echo "    ";
        $context["dateValidation"] = ["Date" => []];
        // line 337
        echo "    ";
        $context["timeValidation"] = ["Time" => []];
        // line 338
        echo "
    ";
        // line 339
        if (($context["required"] ?? null)) {
            // line 340
            echo "        ";
            $context["dateValidation"] = twig_array_merge(($context["dateValidation"] ?? null), ["NotBlank" => []]);
            // line 341
            echo "        ";
            $context["timeValidation"] = twig_array_merge(($context["timeValidation"] ?? null), ["NotBlank" => []]);
            // line 342
            echo "    ";
        }
        // line 343
        echo "
    ";
        // line 344
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 344)) {
            // line 345
            echo "        ";
            $context["attrClass"] = (($__internal_compile_4 = ($context["attr"] ?? null)) && is_array($__internal_compile_4) || $__internal_compile_4 instanceof ArrayAccess ? ($__internal_compile_4["class"] ?? null) : null);
            // line 346
            echo "    ";
        } else {
            // line 347
            echo "        ";
            $context["attrClass"] = "";
            // line 348
            echo "    ";
        }
        // line 349
        echo "
    ";
        // line 350
        $context["options"] = ["view" => "oroui/js/app/views/datepicker/datetimepicker-view", "nativeMode" => $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile(), "dateInputAttrs" => ["placeholder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.choose_date"), "id" =>         // line 355
($context["id"] ?? null), "name" =>         // line 356
($context["id"] ?? null), "class" => ("input-small datepicker-input " .         // line 357
($context["attrClass"] ?? null)), "data-validation" => json_encode(        // line 358
($context["dateValidation"] ?? null), twig_constant("JSON_FORCE_OBJECT")), "aria-live" => "assertive", "autocomplete" => "off", "autocorrect" => "off", "autocapitalize" => "off"], "datePickerOptions" => ["altFormat" => "yy-mm-dd", "changeMonth" => true, "changeYear" => true, "yearRange" => ((        // line 368
array_key_exists("years", $context)) ? (_twig_default_filter(($context["years"] ?? null), "-80:+1")) : ("-80:+1")), "showButtonPanel" => true], "timeInputAttrs" => ["placeholder" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.choose_time"), "id" => ("time_selector_" .         // line 373
($context["id"] ?? null)), "name" => ("time_selector_" .         // line 374
($context["id"] ?? null)), "class" => ("input-small timepicker-input " .         // line 375
($context["attrClass"] ?? null)), "data-validation" => json_encode(        // line 376
($context["timeValidation"] ?? null), twig_constant("JSON_FORCE_OBJECT"))], "timePickerOptions" => []];
        // line 381
        echo "    ";
        $context["id"] = ("hidden_" . ($context["id"] ?? null));
        // line 382
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["data-page-component-module" => "oroui/js/app/components/view-component", "data-page-component-options" => json_encode(        // line 384
($context["options"] ?? null), twig_constant("JSON_FORCE_OBJECT"))]);
        // line 386
        echo "
    ";
        // line 387
        $this->displayBlock("datetime_widget", $context, $blocks);
        echo "
";
    }

    // line 443
    public function block_oro_collection_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 444
        echo "    ";
        $macros["formFields"] = $this;
        // line 445
        echo "    ";
        ob_start(function () { return ''; });
        // line 446
        echo "        ";
        if (array_key_exists("prototype", $context)) {
            // line 447
            echo "            ";
            $context["prototype_html"] = twig_call_macro($macros["formFields"], "macro_oro_collection_item_prototype", [($context["form"] ?? null)], 447, $context, $this->getSourceContext());
            // line 448
            echo "        ";
        }
        // line 449
        echo "        ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 449)) ? ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 449) . " ")) : ("")) . "oro-item-collection collection-fields-list")]);
        // line 450
        echo "        ";
        $context["id"] = (($context["id"] ?? null) . "_collection");
        // line 451
        echo "        <div class=\"row-oro\"
            ";
        // line 452
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 452), "validation_ignore_if_not_changed", [], "any", true, true, false, 452) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 452), "validation_ignore_if_not_changed", [], "any", false, false, false, 452))) {
            // line 453
            echo "                data-page-component-view=\"oroform/js/app/views/lazy-validation-collection-view\"
            ";
        }
        // line 455
        echo "        >
            ";
        // line 456
        $context["prototype_name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 456), "prototype_name", [], "any", false, false, false, 456);
        // line 457
        echo "            <div ";
        $this->displayBlock("widget_container_attributes", $context, $blocks);
        echo " data-last-index=\"";
        echo twig_escape_filter($this->env, twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 457)), "html", null, true);
        echo "\" data-row-count-add=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 457), "row_count_add", [], "any", false, false, false, 457), "html", null, true);
        echo "\" data-prototype-name=\"";
        echo twig_escape_filter($this->env, ($context["prototype_name"] ?? null), "html", null, true);
        echo "\"";
        if (array_key_exists("prototype_html", $context)) {
            echo " data-prototype=\"";
            echo twig_escape_filter($this->env, ($context["prototype_html"] ?? null));
            echo "\"";
        }
        echo ">
                <input type=\"hidden\" name=\"validate_";
        // line 458
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\" data-collection-name=\"";
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\" data-name=\"collection-validation\" disabled data-validate-element>
                ";
        // line 459
        if (twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 459))) {
            // line 460
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 460));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 461
                echo "                        ";
                echo twig_call_macro($macros["formFields"], "macro_oro_collection_item_prototype", [$context["child"]], 461, $context, $this->getSourceContext());
                echo "
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 463
            echo "                ";
        } elseif ((($context["show_form_when_empty"] ?? null) && array_key_exists("prototype_html", $context))) {
            // line 464
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(range(0, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 464), "row_count_initial", [], "any", false, false, false, 464) - 1)));
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                // line 465
                echo "                        ";
                echo twig_replace_filter(($context["prototype_html"] ?? null), [($context["prototype_name"] ?? null) => $context["i"]]);
                echo "
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 467
            echo "                ";
        }
        // line 468
        echo "            </div>
            ";
        // line 469
        if (($context["allow_add"] ?? null)) {
            // line 470
            echo "            <a class=\"btn add-list-item\" href=\"#\" role=\"button\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 470), "add_label", [], "any", true, true, false, 470)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 470), "add_label", [], "any", false, false, false, 470), "oro.form.collection.add")) : ("oro.form.collection.add"))), "html", null, true);
            echo "</a>
            ";
        }
        // line 472
        echo "        </div>
        ";
        // line 473
        if ((($context["handle_primary"] ?? null) && ( !array_key_exists("prototype", $context) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["prototype"] ?? null), "primary", [], "any", true, true, false, 473)))) {
            // line 474
            echo "            ";
            echo twig_call_macro($macros["formFields"], "macro_oro_collection_validate_primary_js", [$context], 474, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 476
        echo "    ";
        $___internal_parse_64_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 445
        echo twig_spaceless($___internal_parse_64_);
    }

    // line 490
    public function block_oro_form_js_validation($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 491
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroForm/Form/fields.html.twig", 491)->unwrap();
        // line 492
        echo "    ";
        $context["pageComponent"] = ["module" => "oroui/js/app/components/view-component", "options" => ["_sourceElement" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 495
($context["form"] ?? null), "vars", [], "any", false, false, false, 495), "id", [], "any", false, false, false, 495)), "view" => "oroform/js/app/views/form-validate-view", "validationOptions" => twig_array_filter($this->env,         // line 497
($context["js_options"] ?? null), function ($__value__, $__key__) use ($context, $macros) { $context["value"] = $__value__; $context["key"] = $__key__; return (($context["key"] ?? null) != "initOn"); })]];
        // line 500
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["js_options"] ?? null), "initOn", [], "any", true, true, false, 500) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["js_options"] ?? null), "initOn", [], "any", false, false, false, 500)))) {
            // line 501
            echo "        ";
            $context["pageComponent"] = twig_array_merge(($context["pageComponent"] ?? null), ["init-on" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 502
($context["js_options"] ?? null), "initOn", [], "any", false, false, false, 502) . " #") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 502), "id", [], "any", false, false, false, 502))]);
            // line 504
            echo "    ";
        }
        // line 505
        echo "    <div ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["pageComponent"] ?? null)], 505, $context, $this->getSourceContext());
        echo "></div>
";
    }

    // line 508
    public function block_oro_entity_create_or_select_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 509
        echo "    ";
        $context["currentMode"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mode", [], "any", false, true, false, 509), "vars", [], "any", false, true, false, 509), "value", [], "any", true, true, false, 509)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mode", [], "any", false, true, false, 509), "vars", [], "any", false, true, false, 509), "value", [], "any", false, false, false, 509), "create")) : ("create"));
        // line 510
        echo "    ";
        $context["viewsContainerId"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 510), "id", [], "any", false, false, false, 510) . "-container");
        // line 511
        echo "
    ";
        // line 512
        ob_start(function () { return ''; });
        // line 513
        echo "        <div class=\"control-group create-select-entity ";
        echo twig_escape_filter($this->env, ($context["currentMode"] ?? null), "html", null, true);
        echo "
            ";
        // line 514
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 514)) {
            echo " ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 514), "html", null, true);
        }
        echo "\"
            id=\"";
        // line 515
        echo twig_escape_filter($this->env, ($context["viewsContainerId"] ?? null), "html", null, true);
        echo "\"
        >
            ";
        // line 517
        if ( !(($context["label"] ?? null) === false)) {
            // line 518
            echo "                <div class=\"control-label wrap\">
                    ";
            // line 519
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label', ["label_attr" => ($context["label_attr"] ?? null)]);
            echo "
                </div>
            ";
        }
        // line 522
        echo "            <div class=\"controls";
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            echo " validation-error";
        }
        echo "\">
                ";
        // line 523
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
                ";
        // line 524
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
            </div>
        </div>
    ";
        $___internal_parse_65_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 512
        echo twig_spaceless($___internal_parse_65_);
    }

    // line 530
    public function block_oro_autocomplete_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 531
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["autocomplete"] ?? null), "selection_template_twig", [], "any", true, true, false, 531) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["autocomplete"] ?? null), "selection_template_twig", [], "any", false, false, false, 531))) {
            // line 532
            echo "        ";
            $context["componentOptions"] = twig_array_merge(($context["componentOptions"] ?? null), ["selection_template" => twig_include($this->env, $context, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 533
($context["autocomplete"] ?? null), "selection_template_twig", [], "any", false, false, false, 533))]);
            // line 535
            echo "    ";
        }
        // line 536
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["data-page-component-module" =>         // line 537
($context["componentModule"] ?? null), "data-page-component-options" => json_encode(        // line 538
($context["componentOptions"] ?? null))]);
        // line 540
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["attr" => ($context["attr"] ?? null)]);
        echo "
";
    }

    // line 543
    public function block_oro_entity_create_or_select_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 544
        echo "    ";
        $context["currentMode"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mode", [], "any", false, true, false, 544), "vars", [], "any", false, true, false, 544), "value", [], "any", true, true, false, 544)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mode", [], "any", false, true, false, 544), "vars", [], "any", false, true, false, 544), "value", [], "any", false, false, false, 544), "create")) : ("create"));
        // line 545
        echo "    ";
        $context["btnGroupId"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 545), "id", [], "any", false, false, false, 545) . "-btn-group");
        // line 546
        echo "    ";
        $context["viewsContainerId"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 546), "id", [], "any", false, false, false, 546) . "-container");
        // line 547
        echo "    ";
        $context["gridWidgetAlias"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 547), "id", [], "any", false, false, false, 547) . "-grid");
        // line 548
        echo "    ";
        $context["routeParametersElement"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 548), "id", [], "any", false, false, false, 548) . "-route-parameters");
        // line 549
        echo "
    <div class=\"create-select-entity-container clearfix\">
        <div id=\"";
        // line 551
        echo twig_escape_filter($this->env, ($context["btnGroupId"] ?? null), "html", null, true);
        echo "\" class=\"buttons-container\">
            <a href=\"#\" class=\"entity-select-btn\" title=\"";
        // line 552
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Choose"), "html", null, true);
        echo "\"";
        if ((($context["disabled"] ?? null) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", true, true, false, 552) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", false, false, false, 552)))) {
            echo " disabled=\"disabled\"";
        }
        echo ">
                <span
                    data-label=\"";
        // line 554
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Choose Existing"), "html", null, true);
        echo "\"
                    data-alt-label-view=\"";
        // line 555
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Choose Another"), "html", null, true);
        echo "\"
                >
                    ";
        // line 557
        if ((($context["currentMode"] ?? null) == "view")) {
            // line 558
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Choose Another"), "html", null, true);
            echo "
                    ";
        } else {
            // line 560
            echo "                        ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Choose Existing"), "html", null, true);
            echo "
                    ";
        }
        // line 562
        echo "                </span>
            </a>

            <a href=\"#\" class=\"entity-create-btn\" title=\"";
        // line 565
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Create New"), "html", null, true);
        echo "\"";
        if ((($context["disabled"] ?? null) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", true, true, false, 565) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", false, false, false, 565)))) {
            echo " disabled=\"disabled\"";
        }
        echo ">
                <span>";
        // line 566
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Create New"), "html", null, true);
        echo "</span>
            </a>

            <a href=\"#\" class=\"entity-cancel-btn\" title=\"";
        // line 569
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "\"";
        if ((($context["disabled"] ?? null) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", true, true, false, 569) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", false, false, false, 569)))) {
            echo " disabled=\"disabled\"";
        }
        echo ">
                <span>";
        // line 570
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</span>
            </a>
        </div>

        <div class=\"entity-create-block\"
            ";
        // line 575
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 575), "required", [], "any", false, false, false, 575)) {
            echo "data-validation-optional-group=\"\"";
        }
        // line 576
        echo "            ";
        if ((($context["currentMode"] ?? null) != "create")) {
            echo "data-validation-ignore=\"\"";
        }
        // line 577
        echo "        >
            ";
        // line 578
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "new_entity", [], "any", false, false, false, 578), 'widget');
        echo "
        </div>

        <div class=\"entity-select-block\">
            ";
        // line 582
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "loadingElement" => (("#" .         // line 584
($context["viewsContainerId"] ?? null)) . " .create-select-entity-container"), "elementFirst" => (        // line 585
($context["currentMode"] ?? null) == "grid"), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_datagrid_widget", ["gridName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 586
($context["form"] ?? null), "vars", [], "any", false, false, false, 586), "grid_name", [], "any", false, false, false, 586)]), "alias" =>         // line 587
($context["gridWidgetAlias"] ?? null)]);
        // line 588
        echo "
        </div>

        <div class=\"entity-view-block ";
        // line 591
        if ((twig_length_filter($this->env, ($context["view_widgets"] ?? null)) > 1)) {
            echo "row-fluid row-fluid-divider";
        }
        echo "\">
            ";
        // line 592
        $context["allRouteParameters"] = [];
        // line 593
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["view_widgets"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["view_widget"]) {
            // line 594
            echo "                <div class=\"responsive-cell\">
                    ";
            // line 595
            $context["routeParameters"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["view_widget"], "route_parameters", [], "any", false, false, false, 595);
            // line 596
            echo "                    ";
            $context["allRouteParameters"] = twig_array_merge(($context["allRouteParameters"] ?? null), [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["view_widget"], "widget_alias", [], "any", false, false, false, 596) => ($context["routeParameters"] ?? null)]);
            // line 597
            echo "                    ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "loadingElement" => (("#" .             // line 599
($context["viewsContainerId"] ?? null)) . " .create-select-entity-container"), "elementFirst" => (            // line 600
($context["currentMode"] ?? null) == "view"), "url" => (((            // line 601
($context["currentMode"] ?? null) == "view")) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["view_widget"], "route_name", [], "any", false, false, false, 601), ($context["routeParameters"] ?? null))) : (null)), "alias" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 602
$context["view_widget"], "widget_alias", [], "any", false, false, false, 602), "title" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 603
$context["view_widget"], "title", [], "any", true, true, false, 603)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["view_widget"], "title", [], "any", false, false, false, 603))) : (null))]);
            // line 604
            echo "
                </div>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['view_widget'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 607
        echo "            <input type=\"hidden\"
               name=\"";
        // line 608
        echo twig_escape_filter($this->env, ($context["routeParametersElement"] ?? null), "html", null, true);
        echo "\"
               id=\"";
        // line 609
        echo twig_escape_filter($this->env, ($context["routeParametersElement"] ?? null), "html", null, true);
        echo "\"
               value=\"";
        // line 610
        echo twig_escape_filter($this->env, json_encode(($context["allRouteParameters"] ?? null)));
        echo "\"
            />
        </div>

        ";
        // line 614
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "existing_entity", [], "any", false, false, false, 614), 'widget');
        echo "
        ";
        // line 615
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mode", [], "any", false, false, false, 615), 'widget');
        echo "
    </div>
    <script>
        loadModules(['jquery', 'oroform/js/create-select-type-handler'],
        function (\$, createSelectTypeHandler) {
            \$(function() {
                createSelectTypeHandler(
                    '#";
        // line 622
        echo twig_escape_filter($this->env, ($context["btnGroupId"] ?? null), "html", null, true);
        echo "',
                    '#";
        // line 623
        echo twig_escape_filter($this->env, ($context["viewsContainerId"] ?? null), "html", null, true);
        echo "',
                    '#";
        // line 624
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mode", [], "any", false, false, false, 624), "vars", [], "any", false, false, false, 624), "id", [], "any", false, false, false, 624), "html", null, true);
        echo "',
                    '#";
        // line 625
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "existing_entity", [], "any", false, false, false, 625), "vars", [], "any", false, false, false, 625), "id", [], "any", false, false, false, 625), "html", null, true);
        echo "',
                    '#";
        // line 626
        echo twig_escape_filter($this->env, ($context["routeParametersElement"] ?? null), "html", null, true);
        echo "',
                    ";
        // line 627
        echo json_encode(($context["gridWidgetAlias"] ?? null));
        echo ",
                    ";
        // line 628
        echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 628), "view_widgets", [], "any", false, false, false, 628));
        echo ",
                    ";
        // line 629
        echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 629), "existing_entity_grid_id", [], "any", false, false, false, 629));
        echo ",
                    ";
        // line 630
        echo json_encode(($context["currentMode"] ?? null));
        echo ",
                    ";
        // line 631
        echo json_encode(($context["allRouteParameters"] ?? null));
        echo "
                );
            });
        });
    </script>
";
    }

    // line 638
    public function block_oro_entity_create_or_select_choice_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 639
        echo "    <div class=\"create-select-entity-choice-container\"
         data-page-component-module=\"oroform/js/app/components/create-or-select-choice-component\"
         data-page-component-options=\"";
        // line 641
        echo twig_escape_filter($this->env, json_encode(["modeSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 642
($context["form"] ?? null), "mode", [], "any", false, false, false, 642), "vars", [], "any", false, false, false, 642), "id", [], "any", false, false, false, 642)), "newEntitySelector" => ".new-entity", "existingEntitySelector" => ".existing-entity", "existingEntityInputSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 645
($context["form"] ?? null), "existing_entity", [], "any", false, false, false, 645), "vars", [], "any", false, false, false, 645), "id", [], "any", false, false, false, 645)), "editable" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 646
($context["form"] ?? null), "vars", [], "any", false, true, false, 646), "editable", [], "any", true, true, false, 646)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 646), "editable", [], "any", false, false, false, 646), false)) : (false)), "disabled_edit_form" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 647
($context["form"] ?? null), "vars", [], "any", false, true, false, 647), "disabled_edit_form", [], "any", true, true, false, 647)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 647), "disabled_edit_form", [], "any", false, false, false, 647), false)) : (false)), "editRoute" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 648
($context["form"] ?? null), "vars", [], "any", false, true, false, 648), "edit_route", [], "any", true, true, false, 648)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 648), "edit_route", [], "any", false, false, false, 648), null)) : (null))]), "html", null, true);
        // line 649
        echo "\">
        <div class=\"existing-entity\">
            ";
        // line 651
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "existing_entity", [], "any", false, false, false, 651), 'widget');
        echo "
        </div>
        <div class=\"new-entity\">
            ";
        // line 654
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "new_entity", [], "any", false, false, false, 654), 'widget');
        echo "
        </div>
        ";
        // line 656
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "mode", [], "any", false, false, false, 656), 'widget');
        echo "
    </div>
";
    }

    // line 660
    public function block_oro_entity_create_or_select_inline_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 661
        echo "    ";
        $context["isButtonsEnabled"] = ( !($context["disabled"] ?? null) && ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", true, true, false, 661) ||  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "readonly", [], "any", false, false, false, 661)));
        // line 662
        echo "    <div id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "-el\"
            ";
        // line 663
        if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "extra_config", [], "any", true, true, false, 663) ||  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "extra_config", [], "any", false, false, false, 663))) {
            // line 664
            echo "                ";
            if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 664), "configs", [], "any", false, true, false, 664), "async_dialogs", [], "any", true, true, false, 664)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 664), "configs", [], "any", false, true, false, 664), "async_dialogs", [], "any", false, false, false, 664), false)) : (false)) === true)) {
                // line 665
                echo "                    ";
                $context["asyncNameSegment"] = "async-";
                // line 666
                echo "                ";
            }
            // line 667
            echo "                ";
            $context["urlParts"] = ["grid" => ["route" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 669
($context["form"] ?? null), "vars", [], "any", false, true, false, 669), "grid_widget_route", [], "any", true, true, false, 669)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 669), "grid_widget_route", [], "any", false, false, false, 669), "oro_datagrid_widget")) : ("oro_datagrid_widget")), "gridWidgetView" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 670
($context["form"] ?? null), "vars", [], "any", false, true, false, 670), "grid_view_widget_route", [], "any", true, true, false, 670)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 670), "grid_view_widget_route", [], "any", false, false, false, 670), "oro_datagrid_widget")) : ("oro_datagrid_widget")), "parameters" => ["gridName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 672
($context["form"] ?? null), "vars", [], "any", false, false, false, 672), "grid_name", [], "any", false, false, false, 672), "params" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 673
($context["form"] ?? null), "vars", [], "any", false, false, false, 673), "grid_parameters", [], "any", false, false, false, 673), "renderParams" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 674
($context["form"] ?? null), "vars", [], "any", false, false, false, 674), "grid_render_parameters", [], "any", false, false, false, 674)]]];
            // line 678
            echo "
                ";
            // line 679
            if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 679), "create_enabled", [], "any", true, true, false, 679)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 679), "create_enabled", [], "any", false, false, false, 679), false)) : (false)) === true)) {
                // line 680
                echo "                    ";
                $context["urlParts"] = twig_array_merge(($context["urlParts"] ?? null), ["create" => ["route" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 682
($context["form"] ?? null), "vars", [], "any", false, false, false, 682), "create_form_route", [], "any", false, false, false, 682), "parameters" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 683
($context["form"] ?? null), "vars", [], "any", false, false, false, 683), "create_form_route_parameters", [], "any", false, false, false, 683)]]);
                // line 686
                echo "                ";
            }
            // line 687
            echo "                ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroForm/Form/fields.html.twig", 687)->unwrap();
            // line 688
            echo "                ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => (("oroform/js/app/components/select-create-inline-type-" . ((            // line 689
array_key_exists("asyncNameSegment", $context)) ? (_twig_default_filter(($context["asyncNameSegment"] ?? null), "")) : (""))) . "component"), "options" => ["inputSelector" => ("#" .             // line 691
($context["id"] ?? null)), "entityLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(            // line 692
($context["label"] ?? null), [], ($context["translation_domain"] ?? null)), "urlParts" =>             // line 693
($context["urlParts"] ?? null), "existingEntityGridId" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 694
($context["form"] ?? null), "vars", [], "any", false, true, false, 694), "existing_entity_grid_id", [], "any", true, true, false, 694)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 694), "existing_entity_grid_id", [], "any", false, false, false, 694), "id")) : ("id")), "createEnabled" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 695
($context["form"] ?? null), "vars", [], "any", false, true, false, 695), "create_enabled", [], "any", true, true, false, 695)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 695), "create_enabled", [], "any", false, false, false, 695), false)) : (false))]]], 688, $context, $this->getSourceContext());
            // line 697
            echo "
            ";
        } else {
            // line 699
            echo "                ";
            $context["_block"] =             $this->renderBlock(("oro_entity_create_or_select_inline_js_" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["configs"] ?? null), "extra_config", [], "any", false, false, false, 699)), $context, $blocks);
            // line 700
            echo "            ";
        }
        // line 701
        echo "        ";
        if (($context["isButtonsEnabled"] ?? null)) {
            echo "class=\"entity-create-or-select-container ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 701), "create_enabled", [], "any", false, false, false, 701)) {
                echo "entity-create-enabled";
            }
            echo "\"";
        }
        echo ">
        <div ";
        // line 702
        if (($context["isButtonsEnabled"] ?? null)) {
            echo "class=\"input-append\"";
        }
        echo ">
            ";
        // line 703
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "

            ";
        // line 705
        if (($context["isButtonsEnabled"] ?? null)) {
            // line 706
            echo "                <button class=\"add-on btn btn-icon btn-square-default entity-select-btn\" type=\"button\" aria-label=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.entity_select", ["%name%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null))]), "html", null, true);
            echo "\">
                    <span class=\"fa-bars fa-offset-none\" aria-hidden=\"true\"></span>
                </button>

                ";
            // line 710
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 710), "create_enabled", [], "any", false, false, false, 710)) {
                // line 711
                echo "                    <button class=\"btn btn-icon btn-square-default entity-create-btn\" type=\"button\" aria-label=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.entity_create", ["%name%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null), [], ($context["translation_domain"] ?? null))]), "html", null, true);
                echo "\">
                        <span class=\"fa-plus fa-offset-none\" aria-hidden=\"true\"></span>
                    </button>
                ";
            }
            // line 715
            echo "            ";
        }
        // line 716
        echo "        </div>
    </div>

    ";
        // line 719
        if ((array_key_exists("_block", $context) &&  !twig_test_empty(($context["_block"] ?? null)))) {
            // line 720
            echo "        ";
            echo ($context["_block"] ?? null);
            echo "
    ";
        }
    }

    // line 724
    public function block_oro_link_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 725
        echo "    ";
        if (( !($context["acl"] ?? null) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted(($context["acl"] ?? null)))) {
            // line 726
            echo "        <a href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["route"] ?? null), ($context["routeParameters"] ?? null)), "html", null, true);
            echo "\" class=\"";
            (((array_key_exists("class", $context) && ($context["class"] ?? null))) ? (print (twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true))) : (print ("")));
            echo "\" style=\"display: block; margin-top: 5px;\">
            ";
            // line 727
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["title"] ?? null)), "html", null, true);
            echo "
        </a>
    ";
        }
    }

    // line 732
    public function block_oro_download_links_type_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 733
        echo "    ";
        ob_start(function () { return ''; });
        // line 734
        echo "        ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["files"] ?? null));
        foreach ($context['_seq'] as $context["fileName"] => $context["route"]) {
            // line 735
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, $context["route"], "html", null, true);
            echo "\" class=\"";
            (((array_key_exists("class", $context) && ($context["class"] ?? null))) ? (print (twig_escape_filter($this->env, ($context["class"] ?? null), "html", null, true))) : (print ("")));
            echo "\" style=\"display: block; margin-top: 5px;\">
                ";
            // line 736
            echo twig_escape_filter($this->env, $context["fileName"], "html", null, true);
            echo "
            </a>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['fileName'], $context['route'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 739
        echo "    ";
        $___internal_parse_66_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 733
        echo twig_spaceless($___internal_parse_66_);
    }

    // line 742
    public function block_oro_simple_color_picker_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 743
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        echo "
";
    }

    // line 746
    public function block_oro_simple_color_picker_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 747
        echo "    ";
        $context["attr"] = twig_array_merge(["data-page-component-module" => "oroui/js/app/components/view-component", "data-page-component-options" => json_encode(twig_array_merge(["view" => "oroform/js/app/views/simple-color-picker-view"],         // line 749
($context["configs"] ?? null)))],         // line 750
($context["attr"] ?? null));
        // line 751
        echo "    ";
        $context["type"] = "hidden";
        // line 752
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 755
    public function block_oro_simple_color_choice_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 756
        echo "    ";
        $context["attr"] = twig_array_merge(["data-page-component-module" => "oroui/js/app/components/view-component", "data-page-component-options" => json_encode(twig_array_merge(["view" => "oroform/js/app/views/simple-color-choice-view"],         // line 758
($context["configs"] ?? null)))],         // line 759
($context["attr"] ?? null));
        // line 760
        echo "    ";
        if ((((($context["required"] ?? null) && (null === ($context["empty_value"] ?? null))) &&  !($context["empty_value_in_choices"] ?? null)) &&  !($context["multiple"] ?? null))) {
            // line 761
            $context["required"] = false;
        }
        // line 763
        echo "<select ";
        $this->displayBlock("widget_attributes", $context, $blocks);
        if (($context["multiple"] ?? null)) {
            echo " multiple=\"multiple\"";
        }
        echo ">
        ";
        // line 764
        if (($context["allow_empty_color"] ?? null)) {
            // line 765
            echo "<option value=\"\" class=\"empty-color\"";
            if ((($context["required"] ?? null) && twig_test_empty(($context["value"] ?? null)))) {
                echo " selected=\"selected\"";
            }
            echo ">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["empty_value"] ?? null), [], ($context["translation_domain"] ?? null)), "html", null, true);
            echo "</option>
            <optgroup label=\"---\"></optgroup>";
        }
        // line 768
        echo "        ";
        $context["options"] = ($context["choices"] ?? null);
        // line 769
        $this->displayBlock("oro_simple_color_choice_widget_options", $context, $blocks);
        // line 770
        echo "</select>
";
    }

    // line 773
    public function block_oro_simple_color_choice_widget_options($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 774
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["options"] ?? null));
        foreach ($context['_seq'] as $context["group_label"] => $context["choice"]) {
            // line 775
            if (twig_test_iterable($context["choice"])) {
                // line 776
                echo "<optgroup label=\"";
                echo twig_escape_filter($this->env, $context["group_label"], "html", null, true);
                echo "\"></optgroup>";
            } else {
                // line 778
                echo "<option value=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "value", [], "any", false, false, false, 778), "html", null, true);
                echo "\"";
                if (Symfony\Bridge\Twig\Extension\twig_is_selected_choice($context["choice"], ($context["value"] ?? null))) {
                    echo " selected=\"selected\"";
                }
                echo ">";
                echo twig_escape_filter($this->env, ((($context["translatable"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 778), [], ($context["translation_domain"] ?? null))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["choice"], "label", [], "any", false, false, false, 778))), "html", null, true);
                echo "</option>";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['group_label'], $context['choice'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    // line 783
    public function block_oro_color_table_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 784
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        echo "
";
    }

    // line 787
    public function block_oro_color_table_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 788
        echo "    ";
        $context["attr"] = twig_array_merge(["data-page-component-module" => "oroui/js/app/components/view-component", "data-page-component-options" => json_encode(twig_array_merge(["view" => "oroform/js/app/views/color-table-view"],         // line 790
($context["configs"] ?? null)))],         // line 791
($context["attr"] ?? null));
        // line 792
        echo "    ";
        $context["type"] = "hidden";
        // line 793
        echo "    ";
        $this->displayBlock("form_widget_simple", $context, $blocks);
        echo "
";
    }

    // line 796
    public function block_oro_resizeable_rich_text_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 797
        echo "    ";
        $context["options"] = ["view" => "oroform/js/app/views/wysiwig-editor/wysiwyg-dialog-view", "editorComponentName" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 799
($context["form"] ?? null), "vars", [], "any", false, true, false, 799), "attr", [], "any", false, true, false, 799), "data-page-component-name", [], "array", true, true, false, 799)) ? ((($__internal_compile_5 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 800
($context["form"] ?? null), "vars", [], "any", false, false, false, 800), "attr", [], "any", false, false, false, 800)) && is_array($__internal_compile_5) || $__internal_compile_5 instanceof ArrayAccess ? ($__internal_compile_5["data-page-component-name"] ?? null) : null)) : ((($__internal_compile_6 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 800), "attr", [], "any", false, false, false, 800)) && is_array($__internal_compile_6) || $__internal_compile_6 instanceof ArrayAccess ? ($__internal_compile_6["data-ftid"] ?? null) : null)))];
        // line 802
        echo "
    <div data-page-component-module=\"oroui/js/app/components/view-component\"
         data-page-component-name=\"wrap_";
        // line 804
        echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 804), "attr", [], "any", false, true, false, 804), "data-page-component-name", [], "array", true, true, false, 804)) ? ((($__internal_compile_7 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 805
($context["form"] ?? null), "vars", [], "any", false, false, false, 805), "attr", [], "any", false, false, false, 805)) && is_array($__internal_compile_7) || $__internal_compile_7 instanceof ArrayAccess ? ($__internal_compile_7["data-page-component-name"] ?? null) : null)) : ((($__internal_compile_8 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 805), "attr", [], "any", false, false, false, 805)) && is_array($__internal_compile_8) || $__internal_compile_8 instanceof ArrayAccess ? ($__internal_compile_8["data-ftid"] ?? null) : null))), "html", null, true);
        echo "\"
         data-page-component-options=\"";
        // line 806
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"
         data-layout=\"separate\" >
        ";
        // line 808
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 812
    public function block_oro_entity_tree_select_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 813
        echo "    ";
        $this->displayBlock("form_row", $context, $blocks);
        echo "
";
    }

    // line 816
    public function block_oro_entity_tree_select_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 817
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroForm/Form/fields.html.twig", 817)->unwrap();
        // line 818
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_renderJsTree", [["treeOptions" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 820
($context["form"] ?? null), "vars", [], "any", false, false, false, 820), "treeOptions", [], "any", false, false, false, 820), "disableActions" => true]], 818, $context, $this->getSourceContext());
        // line 823
        echo "

    ";
        // line 825
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    // line 828
    public function block_button_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 829
        echo "    <div class=\"control-group-container control-group-button\">
        ";
        // line 830
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 834
    public function block_oro_checkbox_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 835
        echo "    <div class=\"oro-checkbox-view\"
         data-page-component-module=\"oroui/js/app/components/view-component\"
         data-page-component-options=\"";
        // line 837
        echo twig_escape_filter($this->env, json_encode(["view" => "oroform/js/app/views/checkbox-view", "selectors" => ["checkbox" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 840
($context["form"] ?? null), "vars", [], "any", false, false, false, 840), "id", [], "any", false, false, false, 840)), "hiddenInput" => (("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 841
($context["form"] ?? null), "vars", [], "any", false, false, false, 841), "id", [], "any", false, false, false, 841)) . "-hidden")]]), "html", null, true);
        // line 843
        echo "\"
    >
        ";
        // line 845
        $this->displayBlock("checkbox_widget", $context, $blocks);
        echo "
        <input id=\"";
        // line 846
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 846), "id", [], "any", false, false, false, 846), "html", null, true);
        echo "-hidden\" type=\"hidden\" value=\"0\" name=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 846), "full_name", [], "any", false, false, false, 846), "html", null, true);
        echo "\" disabled=\"disabled\">
    </div>
";
    }

    // line 390
    public function macro_oro_collection_item_prototype($__widget__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "widget" => $__widget__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 391
            echo "    ";
            if (twig_in_filter("collection", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 391), "block_prefixes", [], "any", false, false, false, 391))) {
                // line 392
                echo "        ";
                $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 392), "prototype", [], "any", false, false, false, 392);
                // line 393
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 393), "prototype", [], "any", false, false, false, 393), "vars", [], "any", false, false, false, 393), "name", [], "any", false, false, false, 393);
                // line 394
                echo "        ";
                $context["disabled"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 394), "disabled", [], "any", false, false, false, 394);
                // line 395
                echo "        ";
                $context["allow_delete"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 395), "allow_delete", [], "any", false, false, false, 395);
                // line 396
                echo "        ";
                $context["allow_add_after"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 396), "allow_add_after", [], "any", false, false, false, 396);
                // line 397
                echo "        ";
                $context["skip_optional_validation_group"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, true, false, 397), "skip_optional_validation_group", [], "any", true, true, false, 397)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, true, false, 397), "skip_optional_validation_group", [], "any", false, false, false, 397), false)) : (false));
                // line 398
                echo "    ";
            } else {
                // line 399
                echo "        ";
                $context["form"] = ($context["widget"] ?? null);
                // line 400
                echo "        ";
                $context["name"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 400), "full_name", [], "any", false, false, false, 400);
                // line 401
                echo "        ";
                $context["disabled"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, false, false, 401), "vars", [], "any", false, false, false, 401), "disabled", [], "any", false, false, false, 401);
                // line 402
                echo "        ";
                $context["allow_delete"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, false, false, 402), "vars", [], "any", false, false, false, 402), "allow_delete", [], "any", false, false, false, 402);
                // line 403
                echo "        ";
                $context["allow_add_after"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, false, false, 403), "vars", [], "any", false, false, false, 403), "allow_add_after", [], "any", false, false, false, 403);
                // line 404
                echo "        ";
                $context["skip_optional_validation_group"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, true, false, 404), "vars", [], "any", false, true, false, 404), "skip_optional_validation_group", [], "any", true, true, false, 404)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "parent", [], "any", false, true, false, 404), "vars", [], "any", false, true, false, 404), "skip_optional_validation_group", [], "any", false, false, false, 404), false)) : (false));
                // line 405
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, true, false, 405), "allow_delete", [], "any", true, true, false, 405)) {
                    // line 406
                    echo "            ";
                    $context["allow_delete"] = (($context["allow_delete"] ?? null) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 406), "allow_delete", [], "any", false, false, false, 406));
                    // line 407
                    echo "        ";
                }
                // line 408
                echo "    ";
            }
            // line 409
            echo "    <div class=\"oro-collection-item\"
        data-content=\"";
            // line 410
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "\"
        ";
            // line 411
            if ( !($context["skip_optional_validation_group"] ?? null)) {
                // line 412
                echo "            data-validation-optional-group
            ";
                // line 413
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 413), "attr", [], "any", false, true, false, 413), "data-validation-optional-group-handler", [], "array", true, true, false, 413)) {
                    // line 414
                    echo "                data-validation-optional-group-handler=\"";
                    echo twig_escape_filter($this->env, (($__internal_compile_9 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 414), "attr", [], "any", false, false, false, 414)) && is_array($__internal_compile_9) || $__internal_compile_9 instanceof ArrayAccess ? ($__internal_compile_9["data-validation-optional-group-handler"] ?? null) : null), "html", null, true);
                    echo "\"
            ";
                }
                // line 416
                echo "        ";
            }
            // line 417
            echo "        ";
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widget"] ?? null), "vars", [], "any", false, false, false, 417), "valid", [], "any", false, false, false, 417) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 418
($context["widget"] ?? null), "parent", [], "any", false, true, false, 418), "vars", [], "any", false, true, false, 418), "validation_ignore_if_not_changed", [], "any", true, true, false, 418)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 419
($context["widget"] ?? null), "parent", [], "any", false, false, false, 419), "vars", [], "any", false, false, false, 419), "validation_ignore_if_not_changed", [], "any", false, false, false, 419))) {
                // line 420
                echo "            data-validation-ignore
        ";
            }
            // line 422
            echo "    >
        <div class=\"row-oro oro-multiselect-holder";
            // line 423
            if ( !($context["allow_delete"] ?? null)) {
                echo " not-removable";
            }
            echo "\">
            ";
            // line 424
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget', ["disabled" => ($context["disabled"] ?? null)]);
            echo "
            ";
            // line 425
            if (($context["allow_delete"] ?? null)) {
                // line 426
                echo "                <button class=\"removeRow btn btn-icon btn-square-light\"
                    aria-label=\"";
                // line 427
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.collection.remove"), "html", null, true);
                echo "\"
                    type=\"button\"
                    data-related=\"";
                // line 429
                echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
                echo "\">
                </button>
            ";
            }
            // line 432
            echo "            ";
            if (($context["allow_add_after"] ?? null)) {
                // line 433
                echo "                <button class=\"addAfterRow btn btn-icon btn-square-light\"
                    aria-label=\"";
                // line 434
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.form.collection.add"), "html", null, true);
                echo "\"
                    type=\"button\"
                    data-related=\"";
                // line 436
                echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
                echo "\">
                </button>
            ";
            }
            // line 439
            echo "        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 479
    public function macro_oro_collection_validate_primary_js($__context__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "context" => $__context__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 480
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroForm/Form/fields.html.twig", 480)->unwrap();
            // line 481
            echo "    <div ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["_sourceElement" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 484
($context["context"] ?? null), "id", [], "any", false, false, false, 484)), "view" => "oroform/js/app/views/fields-groups-collection-view"]]], 481, $context, $this->getSourceContext());
            // line 487
            echo "></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroForm/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  2091 => 487,  2089 => 484,  2087 => 481,  2084 => 480,  2071 => 479,  2060 => 439,  2054 => 436,  2049 => 434,  2046 => 433,  2043 => 432,  2037 => 429,  2032 => 427,  2029 => 426,  2027 => 425,  2023 => 424,  2017 => 423,  2014 => 422,  2010 => 420,  2008 => 419,  2007 => 418,  2005 => 417,  2002 => 416,  1996 => 414,  1994 => 413,  1991 => 412,  1989 => 411,  1985 => 410,  1982 => 409,  1979 => 408,  1976 => 407,  1973 => 406,  1970 => 405,  1967 => 404,  1964 => 403,  1961 => 402,  1958 => 401,  1955 => 400,  1952 => 399,  1949 => 398,  1946 => 397,  1943 => 396,  1940 => 395,  1937 => 394,  1934 => 393,  1931 => 392,  1928 => 391,  1915 => 390,  1906 => 846,  1902 => 845,  1898 => 843,  1896 => 841,  1895 => 840,  1894 => 837,  1890 => 835,  1886 => 834,  1879 => 830,  1876 => 829,  1872 => 828,  1866 => 825,  1862 => 823,  1860 => 820,  1858 => 818,  1855 => 817,  1851 => 816,  1844 => 813,  1840 => 812,  1833 => 808,  1828 => 806,  1824 => 805,  1823 => 804,  1819 => 802,  1817 => 800,  1816 => 799,  1814 => 797,  1810 => 796,  1803 => 793,  1800 => 792,  1798 => 791,  1797 => 790,  1795 => 788,  1791 => 787,  1784 => 784,  1780 => 783,  1763 => 778,  1758 => 776,  1756 => 775,  1751 => 774,  1747 => 773,  1742 => 770,  1740 => 769,  1737 => 768,  1727 => 765,  1725 => 764,  1717 => 763,  1714 => 761,  1711 => 760,  1709 => 759,  1708 => 758,  1706 => 756,  1702 => 755,  1695 => 752,  1692 => 751,  1690 => 750,  1689 => 749,  1687 => 747,  1683 => 746,  1676 => 743,  1672 => 742,  1668 => 733,  1665 => 739,  1656 => 736,  1649 => 735,  1644 => 734,  1641 => 733,  1637 => 732,  1629 => 727,  1622 => 726,  1619 => 725,  1615 => 724,  1607 => 720,  1605 => 719,  1600 => 716,  1597 => 715,  1589 => 711,  1587 => 710,  1579 => 706,  1577 => 705,  1572 => 703,  1566 => 702,  1555 => 701,  1552 => 700,  1549 => 699,  1545 => 697,  1543 => 695,  1542 => 694,  1541 => 693,  1540 => 692,  1539 => 691,  1538 => 689,  1536 => 688,  1533 => 687,  1530 => 686,  1528 => 683,  1527 => 682,  1525 => 680,  1523 => 679,  1520 => 678,  1518 => 674,  1517 => 673,  1516 => 672,  1515 => 670,  1514 => 669,  1512 => 667,  1509 => 666,  1506 => 665,  1503 => 664,  1501 => 663,  1496 => 662,  1493 => 661,  1489 => 660,  1482 => 656,  1477 => 654,  1471 => 651,  1467 => 649,  1465 => 648,  1464 => 647,  1463 => 646,  1462 => 645,  1461 => 642,  1460 => 641,  1456 => 639,  1452 => 638,  1442 => 631,  1438 => 630,  1434 => 629,  1430 => 628,  1426 => 627,  1422 => 626,  1418 => 625,  1414 => 624,  1410 => 623,  1406 => 622,  1396 => 615,  1392 => 614,  1385 => 610,  1381 => 609,  1377 => 608,  1374 => 607,  1366 => 604,  1364 => 603,  1363 => 602,  1362 => 601,  1361 => 600,  1360 => 599,  1358 => 597,  1355 => 596,  1353 => 595,  1350 => 594,  1345 => 593,  1343 => 592,  1337 => 591,  1332 => 588,  1330 => 587,  1329 => 586,  1328 => 585,  1327 => 584,  1326 => 582,  1319 => 578,  1316 => 577,  1311 => 576,  1307 => 575,  1299 => 570,  1291 => 569,  1285 => 566,  1277 => 565,  1272 => 562,  1266 => 560,  1260 => 558,  1258 => 557,  1253 => 555,  1249 => 554,  1240 => 552,  1236 => 551,  1232 => 549,  1229 => 548,  1226 => 547,  1223 => 546,  1220 => 545,  1217 => 544,  1213 => 543,  1206 => 540,  1204 => 538,  1203 => 537,  1201 => 536,  1198 => 535,  1196 => 533,  1194 => 532,  1191 => 531,  1187 => 530,  1183 => 512,  1176 => 524,  1172 => 523,  1165 => 522,  1159 => 519,  1156 => 518,  1154 => 517,  1149 => 515,  1142 => 514,  1137 => 513,  1135 => 512,  1132 => 511,  1129 => 510,  1126 => 509,  1122 => 508,  1115 => 505,  1112 => 504,  1110 => 502,  1108 => 501,  1105 => 500,  1103 => 497,  1102 => 495,  1100 => 492,  1097 => 491,  1093 => 490,  1089 => 445,  1086 => 476,  1080 => 474,  1078 => 473,  1075 => 472,  1069 => 470,  1067 => 469,  1064 => 468,  1061 => 467,  1052 => 465,  1047 => 464,  1044 => 463,  1035 => 461,  1030 => 460,  1028 => 459,  1022 => 458,  1005 => 457,  1003 => 456,  1000 => 455,  996 => 453,  994 => 452,  991 => 451,  988 => 450,  985 => 449,  982 => 448,  979 => 447,  976 => 446,  973 => 445,  970 => 444,  966 => 443,  960 => 387,  957 => 386,  955 => 384,  953 => 382,  950 => 381,  948 => 376,  947 => 375,  946 => 374,  945 => 373,  944 => 368,  943 => 358,  942 => 357,  941 => 356,  940 => 355,  939 => 350,  936 => 349,  933 => 348,  930 => 347,  927 => 346,  924 => 345,  922 => 344,  919 => 343,  916 => 342,  913 => 341,  910 => 340,  908 => 339,  905 => 338,  902 => 337,  899 => 336,  895 => 335,  889 => 332,  886 => 331,  884 => 329,  882 => 327,  880 => 323,  879 => 322,  878 => 321,  877 => 311,  876 => 310,  875 => 309,  874 => 308,  873 => 303,  870 => 302,  867 => 301,  865 => 300,  862 => 299,  859 => 298,  856 => 297,  854 => 296,  851 => 295,  848 => 294,  845 => 293,  842 => 292,  839 => 291,  835 => 290,  828 => 287,  825 => 286,  821 => 285,  814 => 282,  811 => 281,  807 => 280,  800 => 277,  797 => 276,  793 => 275,  786 => 272,  783 => 271,  779 => 270,  775 => 225,  764 => 264,  761 => 262,  758 => 260,  756 => 259,  754 => 258,  752 => 257,  749 => 254,  748 => 253,  747 => 252,  746 => 251,  745 => 250,  744 => 249,  742 => 248,  740 => 247,  738 => 246,  724 => 245,  721 => 244,  718 => 243,  715 => 242,  712 => 241,  709 => 240,  706 => 239,  704 => 238,  701 => 237,  698 => 236,  695 => 235,  692 => 234,  689 => 233,  686 => 232,  683 => 231,  680 => 230,  677 => 229,  674 => 228,  671 => 227,  668 => 226,  665 => 225,  661 => 224,  655 => 220,  649 => 218,  647 => 217,  643 => 215,  634 => 213,  630 => 212,  624 => 209,  619 => 207,  615 => 206,  611 => 205,  607 => 204,  603 => 203,  599 => 202,  595 => 201,  591 => 200,  587 => 199,  583 => 198,  579 => 196,  576 => 195,  573 => 194,  570 => 193,  567 => 192,  564 => 191,  561 => 190,  558 => 189,  555 => 188,  552 => 187,  550 => 186,  541 => 180,  535 => 178,  531 => 177,  525 => 174,  481 => 133,  474 => 128,  470 => 127,  466 => 97,  462 => 123,  454 => 121,  452 => 120,  449 => 119,  447 => 118,  439 => 116,  437 => 115,  434 => 114,  428 => 113,  425 => 112,  416 => 111,  412 => 110,  404 => 108,  398 => 105,  395 => 104,  392 => 103,  384 => 101,  382 => 100,  362 => 99,  359 => 98,  356 => 97,  352 => 96,  348 => 89,  345 => 93,  336 => 91,  331 => 90,  328 => 89,  324 => 88,  320 => 81,  317 => 85,  308 => 83,  303 => 82,  300 => 81,  296 => 80,  289 => 77,  285 => 76,  275 => 73,  271 => 72,  266 => 69,  264 => 68,  263 => 67,  261 => 65,  258 => 64,  255 => 63,  252 => 62,  249 => 61,  246 => 60,  243 => 59,  240 => 58,  237 => 57,  234 => 56,  231 => 55,  228 => 54,  225 => 53,  223 => 52,  221 => 49,  218 => 48,  215 => 47,  212 => 46,  209 => 45,  206 => 44,  203 => 43,  200 => 42,  197 => 41,  194 => 40,  191 => 39,  188 => 38,  185 => 37,  182 => 36,  179 => 35,  176 => 34,  173 => 33,  169 => 32,  162 => 29,  159 => 28,  156 => 27,  153 => 26,  150 => 23,  147 => 22,  144 => 21,  142 => 20,  139 => 19,  136 => 18,  134 => 16,  132 => 15,  129 => 14,  127 => 13,  124 => 12,  120 => 11,  106 => 8,  102 => 7,  88 => 4,  84 => 3,  73 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroForm/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/views/Form/fields.html.twig");
    }
}
