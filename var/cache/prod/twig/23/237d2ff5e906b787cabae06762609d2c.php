<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAnalytics/label.html.twig */
class __TwigTemplate_4dfe85b090a104df88bf0c40a52dc076 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (((((((array_key_exists("entity", $context) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dataChannel", [], "any", false, true, false, 1), "data", [], "any", true, true, false, 1)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 2
($context["entity"] ?? null), "dataChannel", [], "any", false, true, false, 2), "data", [], "any", false, true, false, 2), "rfm_enabled", [], "any", true, true, false, 2)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dataChannel", [], "any", false, false, false, 2), "data", [], "any", false, false, false, 2), "rfm_enabled", [], "any", false, false, false, 2)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "recency", [], "any", true, true, false, 3)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "frequency", [], "any", true, true, false, 3)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "monetary", [], "any", true, true, false, 3))) {
            // line 5
            echo "    ";
            if (array_key_exists("vertical", $context)) {
                echo "<div class=\"rfm-analytics-label-wrapper\">";
            }
            // line 6
            echo "        <div class=\"pull-right label label-warning rfm-analytics-label";
            if (array_key_exists("vertical", $context)) {
                echo " rfm-analytics-label-vertical";
            }
            echo "\">
            ";
            // line 7
            $context["el"] = ((array_key_exists("vertical", $context)) ? ("p") : ("span"));
            // line 8
            echo "            <";
            echo twig_escape_filter($this->env, ($context["el"] ?? null), "html", null, true);
            echo " title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.recency.title"), "html", null, true);
            echo "\">
                ";
            // line 9
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.recency.label"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recency", [], "any", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recency", [], "any", false, false, false, 9), "-")) : ("-")), "html", null, true);
            echo "
            </";
            // line 10
            echo twig_escape_filter($this->env, ($context["el"] ?? null), "html", null, true);
            echo ">
            <";
            // line 11
            echo twig_escape_filter($this->env, ($context["el"] ?? null), "html", null, true);
            echo " title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.frequency.title"), "html", null, true);
            echo "\">
                ";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.frequency.label"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "frequency", [], "any", true, true, false, 12)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "frequency", [], "any", false, false, false, 12), "-")) : ("-")), "html", null, true);
            echo "
            </";
            // line 13
            echo twig_escape_filter($this->env, ($context["el"] ?? null), "html", null, true);
            echo ">
            <";
            // line 14
            echo twig_escape_filter($this->env, ($context["el"] ?? null), "html", null, true);
            echo " title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.monetary.title"), "html", null, true);
            echo "\">
                ";
            // line 15
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.analytics.rfm.monetary.label"), "html", null, true);
            echo " ";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "monetary", [], "any", true, true, false, 15)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "monetary", [], "any", false, false, false, 15), "-")) : ("-")), "html", null, true);
            echo "
            </";
            // line 16
            echo twig_escape_filter($this->env, ($context["el"] ?? null), "html", null, true);
            echo ">
        </div>
        <div class=\"clearfix\"></div>
    ";
            // line 19
            if (array_key_exists("vertical", $context)) {
                echo "</div>";
            }
        }
    }

    public function getTemplateName()
    {
        return "@OroAnalytics/label.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  106 => 19,  100 => 16,  94 => 15,  88 => 14,  84 => 13,  78 => 12,  72 => 11,  68 => 10,  62 => 9,  55 => 8,  53 => 7,  46 => 6,  41 => 5,  39 => 3,  38 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAnalytics/label.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AnalyticsBundle/Resources/views/label.html.twig");
    }
}
