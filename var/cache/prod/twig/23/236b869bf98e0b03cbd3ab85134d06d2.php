<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/components/select2-relation-component.js */
class __TwigTemplate_06d0891cfd1695b5db0f2f147991b1fc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const Select2Component = require('oro/select2-component');

    const Select2RelationComponent = Select2Component.extend({
        /**
         * @inheritdoc
         */
        constructor: function Select2RelationComponent(options) {
            Select2RelationComponent.__super__.constructor.call(this, options);
        },

        makeQuery: function(query, configs) {
            return [query, configs.target_entity, configs.target_field].join(',');
        }
    });

    return Select2RelationComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/components/select2-relation-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/components/select2-relation-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/components/select2-relation-component.js");
    }
}
