<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/js/activityItemTemplate.html.twig */
class __TwigTemplate_c305abba795a9a7c56a2cf19f505b410 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'activityDetails' => [$this, 'block_activityDetails'],
            'activityActions' => [$this, 'block_activityActions'],
            'activityContent' => [$this, 'block_activityContent'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/js/activityItemTemplate.html.twig", 2)->unwrap();
        // line 3
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroEmail/Email/js/activityItemTemplate.html.twig", 3)->unwrap();
        // line 5
        $context["entityClass"] = "Oro\\Bundle\\EmailBundle\\Entity\\Email";
        // line 6
        $context["entityName"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["entityClass"] ?? null), "label"));
        // line 1
        $this->parent = $this->loadTemplate("@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", "@OroEmail/Email/js/activityItemTemplate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_activityDetails($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        echo twig_escape_filter($this->env, ($context["entityName"] ?? null), "html", null, true);
        echo "
    <%
        var hasLink   = !!data.ownerLink;
        var ownerLink = hasLink
            ? '<a class=\"user\" href=\"' + data.ownerLink + '\">' +  _.escape(data.ownerName) + '</a>'
            : '<span class=\"user\">' + _.escape(data.ownerName) + '</span>';
    %>
    <%= _.template(";
        // line 16
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.sent_by.label"));
        echo ", { interpolate: /\\{\\{(.+?)\\}\\}/g })({
        user: ownerLink,
        date: '<i class=\"date\">' + updatedAt + '</i>'
    }) %>
";
    }

    // line 22
    public function block_activityActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 23
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroEmail/Email/js/activityItemTemplate.html.twig", 23)->unwrap();
        // line 24
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/js/activityItemTemplate.html.twig", 24)->unwrap();
        // line 25
        echo "
    ";
        // line 26
        ob_start(function () { return ''; });
        // line 27
        echo "        ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_user_edit")) {
            // line 28
            echo "            ";
            echo twig_call_macro($macros["AC"], "macro_activity_context_link", [], 28, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 30
        echo "    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 31
        echo "    ";
        $context["actions"] = [0 => ($context["action"] ?? null)];
        // line 32
        echo "
    ";
        // line 33
        ob_start(function () { return ''; });
        // line 34
        echo "        ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
            // line 35
            echo "            <a href=\"#\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "html", null, true);
            echo "\"
               class=\"dropdown-item\"
               data-url=\"<%- routing.generate('oro_email_email_reply', {'id': relatedActivityId, 'entityClass': targetEntityData.class, 'entityId': targetEntityData.id}) %>\"
               ";
            // line 38
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "createOnEvent" => "click", "options" => ["alias" => "reply-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]], 38, $context, $this->getSourceContext());
            // line 55
            echo "><span class=\"fa-reply hide-text\" aria-hidden=\"true\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "html", null, true);
            echo "</span>
                ";
            // line 56
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "html", null, true);
            echo "
            </a>
        ";
        }
        // line 59
        echo "    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 60
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 61
        echo "
    ";
        // line 62
        ob_start(function () { return ''; });
        // line 63
        echo "        ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
            // line 64
            echo "            <a href=\"#\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "html", null, true);
            echo "\"
               class=\"dropdown-item\"
               data-url=\"<%- routing.generate('oro_email_email_forward', {'id': relatedActivityId, 'entityClass': targetEntityData.class, 'entityId': targetEntityData.id}) %>\"
               ";
            // line 67
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "createOnEvent" => "click", "options" => ["alias" => "forward-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]], 67, $context, $this->getSourceContext());
            // line 84
            echo "><span class=\"fa-mail-forward hide-text\" aria-hidden=\"true\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "html", null, true);
            echo "</span>
                ";
            // line 85
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "html", null, true);
            echo "
            </a>
        ";
        }
        // line 88
        echo "    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 89
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 90
        echo "
    ";
        // line 91
        $this->displayParentBlock("activityActions", $context, $blocks);
        echo "
";
    }

    // line 94
    public function block_activityContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 95
        echo "    ";
        // line 96
        echo "    <div class=\"info\"></div>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/js/activityItemTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  194 => 96,  192 => 95,  188 => 94,  182 => 91,  179 => 90,  176 => 89,  173 => 88,  167 => 85,  162 => 84,  160 => 67,  153 => 64,  150 => 63,  148 => 62,  145 => 61,  142 => 60,  139 => 59,  133 => 56,  128 => 55,  126 => 38,  119 => 35,  116 => 34,  114 => 33,  111 => 32,  108 => 31,  105 => 30,  99 => 28,  96 => 27,  94 => 26,  91 => 25,  88 => 24,  85 => 23,  81 => 22,  72 => 16,  61 => 9,  57 => 8,  52 => 1,  50 => 6,  48 => 5,  46 => 3,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/js/activityItemTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/js/activityItemTemplate.html.twig");
    }
}
