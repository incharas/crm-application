<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Opportunity/view.html.twig */
class __TwigTemplate_481f1fc9c50398ade02e3e3eda78e2f1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'stats' => [$this, 'block_stats'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["hasGrantedNameView"] = $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "name");

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%opportunity.name%" => ((        // line 5
($context["hasGrantedNameView"] ?? null)) ? (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "name", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 6), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("view %fieldName% not granted", ["%fieldName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.name.label")])))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroSales/Opportunity/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Opportunity/view.html.twig", 11)->unwrap();
        // line 12
        echo "
    ";
        // line 13
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 14
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["entity"] ?? null), "id", [], "any", false, false, false, 15)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.entity_label")]], 14, $context, $this->getSourceContext());
            // line 17
            echo "
    ";
        }
        // line 19
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 20
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_opportunity", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 21
($context["entity"] ?? null), "id", [], "any", false, false, false, 21)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-contact", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 25
($context["entity"] ?? null), "id", [], "any", false, false, false, 25), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.entity_label")]], 20, $context, $this->getSourceContext());
            // line 27
            echo "
    ";
        }
    }

    // line 31
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Opportunity/view.html.twig", 32)->unwrap();
        // line 33
        echo "
    ";
        // line 34
        $context["breadcrumbs"] = ["entity" =>         // line 35
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.entity_plural_label"), "rawEntityTitle" =>  !        // line 38
($context["hasGrantedNameView"] ?? null), "entityTitle" => ((        // line 39
($context["hasGrantedNameView"] ?? null)) ? (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 40
($context["entity"] ?? null), "name", [], "any", true, true, false, 40)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 40), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))) : (twig_call_macro($macros["UI"], "macro_renderDisabledLabel", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("view %fieldName% not granted", ["%fieldName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.name.label")])], 41, $context, $this->getSourceContext())))];
        // line 43
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 46
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "createdAt")) {
            // line 48
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
            echo ": ";
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 48)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 48)), "html", null, true))) : (print ("N/A")));
            echo "</li>
    ";
        }
        // line 50
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "updatedAt")) {
            // line 51
            echo "        <li>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
            echo ": ";
            ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 51)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 51)), "html", null, true))) : (print ("N/A")));
            echo "</li>
    ";
        }
    }

    // line 55
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Opportunity/view.html.twig", 56)->unwrap();
        // line 57
        echo "
    ";
        // line 58
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    ";
        // line 59
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 59) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "status"))) {
            // line 60
            echo "        <span class=\"page-title__status\">
            ";
            // line 61
            echo twig_call_macro($macros["UI"], "macro_badge", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 61), "name", [], "any", false, false, false, 61), (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 61), "id", [], "any", false, false, false, 61) != "lost")) ? ("enabled") : ("disabled"))], 61, $context, $this->getSourceContext());
            echo "
        </span>
    ";
        }
    }

    // line 66
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "    ";
        ob_start(function () { return ''; });
        // line 68
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_opportunity_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 70
($context["entity"] ?? null), "id", [], "any", false, false, false, 70)])]);
        // line 71
        echo "
    ";
        $context["opportunityInfoWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 73
        echo "
    ";
        // line 74
        $context["generalSubblocks"] = [0 => ["data" => [0 => ($context["opportunityInfoWidget"] ?? null)]]];
        // line 75
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "subblocks" =>         // line 78
($context["generalSubblocks"] ?? null)]];
        // line 81
        echo "
    ";
        // line 82
        $context["id"] = "opportunityView";
        // line 83
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 84
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Opportunity/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  201 => 84,  198 => 83,  196 => 82,  193 => 81,  191 => 78,  189 => 75,  187 => 74,  184 => 73,  180 => 71,  178 => 70,  176 => 68,  173 => 67,  169 => 66,  161 => 61,  158 => 60,  156 => 59,  152 => 58,  149 => 57,  146 => 56,  142 => 55,  132 => 51,  129 => 50,  121 => 48,  118 => 47,  114 => 46,  107 => 43,  105 => 40,  104 => 39,  103 => 38,  102 => 35,  101 => 34,  98 => 33,  95 => 32,  91 => 31,  85 => 27,  83 => 25,  82 => 21,  80 => 20,  77 => 19,  73 => 17,  71 => 15,  69 => 14,  67 => 13,  64 => 12,  61 => 11,  57 => 10,  52 => 1,  50 => 6,  49 => 5,  46 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Opportunity/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Opportunity/view.html.twig");
    }
}
