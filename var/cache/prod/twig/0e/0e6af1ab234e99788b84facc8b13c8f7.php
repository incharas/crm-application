<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroIntegration/Form/javascript.html.twig */
class __TwigTemplate_001a9b80c849ad4b109aca113526a624 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 8
    public function macro_renderIntegrationFormJS($__form__ = null, $__formSelector__ = null, $__csrfTokenFieldFillName__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "form" => $__form__,
            "formSelector" => $__formSelector__,
            "csrfTokenFieldFillName" => $__csrfTokenFieldFillName__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 9
            echo "    ";
            $context["fieldsToSendOnTypeChange"] = [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["form"] ?? null), "type", [], "any", false, false, false, 10), "vars", [], "any", false, false, false, 10), "full_name", [], "any", false, false, false, 10), 1 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 11
($context["form"] ?? null), "name", [], "any", false, false, false, 11), "vars", [], "any", false, false, false, 11), "full_name", [], "any", false, false, false, 11)];
            // line 13
            echo "    ";
            if ((array_key_exists("csrfTokenFieldFillName", $context) && ($context["csrfTokenFieldFillName"] ?? null))) {
                // line 14
                echo "        ";
                $context["fieldsToSendOnTypeChange"] = twig_array_merge(($context["fieldsToSendOnTypeChange"] ?? null), [0 => ($context["csrfTokenFieldFillName"] ?? null)]);
                // line 15
                echo "    ";
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", true, true, false, 15) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", false, false, false, 15))) {
                // line 16
                echo "        ";
                $context["fieldsToSendOnTypeChange"] = twig_array_merge(($context["fieldsToSendOnTypeChange"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "_token", [], "any", false, false, false, 16), "vars", [], "any", false, false, false, 16), "full_name", [], "any", false, false, false, 16)]);
                // line 17
                echo "    ";
            }
            // line 18
            echo "
    ";
            // line 19
            $context["fieldsToSendOnTransportTypeChange"] = twig_array_merge(($context["fieldsToSendOnTypeChange"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "transportType", [], "any", false, false, false, 19), "vars", [], "any", false, false, false, 19), "full_name", [], "any", false, false, false, 19)]);
            // line 20
            echo "
    ";
            // line 21
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroIntegration/Form/javascript.html.twig", 21)->unwrap();
            // line 22
            echo "
    <div ";
            // line 23
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "orointegration/js/channel-view", "options" => ["formSelector" => ("#" . ((            // line 26
array_key_exists("formSelector", $context)) ? (_twig_default_filter(($context["formSelector"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26)))), "typeSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 27
($context["form"] ?? null), "type", [], "any", false, false, false, 27), "vars", [], "any", false, false, false, 27), "id", [], "any", false, false, false, 27)), "transportTypeSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 28
($context["form"] ?? null), "transportType", [], "any", false, false, false, 28), "vars", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28)), "fieldsSets" => ["type" =>             // line 30
($context["fieldsToSendOnTypeChange"] ?? null), "transportType" =>             // line 31
($context["fieldsToSendOnTransportTypeChange"] ?? null)]]]], 23, $context, $this->getSourceContext());
            // line 34
            echo "></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroIntegration/Form/javascript.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 34,  92 => 31,  91 => 30,  90 => 28,  89 => 27,  88 => 26,  87 => 23,  84 => 22,  82 => 21,  79 => 20,  77 => 19,  74 => 18,  71 => 17,  68 => 16,  65 => 15,  62 => 14,  59 => 13,  57 => 11,  56 => 10,  54 => 9,  39 => 8,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroIntegration/Form/javascript.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Resources/views/Form/javascript.html.twig");
    }
}
