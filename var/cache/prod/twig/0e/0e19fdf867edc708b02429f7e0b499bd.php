<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/wysiwig-editor/wysiwyg-dialog-view.js */
class __TwigTemplate_07fce674b87f19875d024d7d86ebd418 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const BaseView = require('oroui/js/app/views/base/view');

    const WysiwygDialogView = BaseView.extend({
        autoRender: true,

        // PLEASE don't make this value less than 180px - IE will display editor with bugs
        // (to adjust need to also change tinymce iframe stylesheet body{min-height:100px} style,
        // see Oro\\Bundle\\FormBundle\\Resources\\public\\css\\scss\\tinymce\\wysiwyg-editor.scss)
        minimalWysiwygEditorHeight: 180,

        /**
         * Name of related WYSIWYG editor component
         *
         * @type {string}
         */
        editorComponentName: null,

        listen: {
            'component:parentResize': 'resizeEditor'
        },

        /**
         * @inheritdoc
         */
        constructor: function WysiwygDialogView(options) {
            WysiwygDialogView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            _.extend(this, _.pick(options, ['minimalWysiwygEditorHeight', 'editorComponentName']));
            WysiwygDialogView.__super__.initialize.call(this, options);
        },

        render: function() {
            this._deferredRender();
            this.initLayout().done(() => {
                if (this.getDialogContainer().length) {
                    // there's dialog widget -- subscribe to resize event
                    this.listenTo(this.getEditorView(), 'resize', this.resizeEditor());
                }
                this._resolveDeferredRender();
            });
        },

        resizeEditor: function() {
            if (this.\$el.closest('[data-spy=\"scroll\"]').length) {
                // switch off resizer in case an editor is inside of scroll spy
                return;
            }
            this.getEditorView().setHeight(this.calcWysiwygHeight());
        },

        calcWysiwygHeight: function() {
            const content = this.getDialogContainer();
            const widgetContent = this.\$el.closest('.widget-content')[0] || content.children().first().get(0);
            const editorHeight = this.getEditorView().getHeight();
            const style = getComputedStyle(widgetContent);
            const availableHeight = editorHeight +
                content[0].offsetHeight - parseFloat(style.marginTop) - parseFloat(style.marginBottom) +
                -widgetContent.offsetHeight;
            return Math.floor(Math.max(availableHeight, this.minimalWysiwygEditorHeight));
        },

        getEditorView: function() {
            const editor = this.pageComponent(this.editorComponentName);
            if (!editor) {
                throw new Error('Could not find message editor');
            }
            return editor.view;
        },

        getDialogContainer: function() {
            return this.\$el.closest('.ui-widget-content');
        }
    });

    return WysiwygDialogView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/wysiwig-editor/wysiwyg-dialog-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/wysiwig-editor/wysiwyg-dialog-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/wysiwig-editor/wysiwyg-dialog-view.js");
    }
}
