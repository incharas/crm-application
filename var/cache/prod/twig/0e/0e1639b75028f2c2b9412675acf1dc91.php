<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Group/update.html.twig */
class __TwigTemplate_5b40cfbb75547968d2e0bafea989bf41 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroUser/Group/update.html.twig", 2)->unwrap();
        // line 4
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "value", [], "any", false, false, false, 4), "id", [], "any", false, false, false, 4);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%group%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["form"] ?? null), "vars", [], "any", false, false, false, 5), "value", [], "any", false, false, false, 5), "name", [], "any", false, false, false, 5), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.group.entity_label")]]);
        // line 6
        $context["gridName"] = "group-users-grid";
        // line 7
        $context["formAction"] = ((($context["entityId"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_group_update", ["id" => ($context["entityId"] ?? null)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_group_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroUser/Group/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Group/update.html.twig", 10)->unwrap();
        // line 11
        echo "
    ";
        // line 12
        if ((($context["entityId"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "value", [], "any", false, false, false, 12)))) {
            // line 13
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_group", ["id" =>             // line 14
($context["entityId"] ?? null)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_group_index"), "aCss" => "no-hash remove-button", "dataId" =>             // line 17
($context["entityId"] ?? null), "id" => "btn-remove-group", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.group.entity_label")]], 13, $context, $this->getSourceContext());
            // line 20
            echo "
        ";
            // line 21
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 21, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 23
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_group_index")], 23, $context, $this->getSourceContext());
        echo "
    ";
        // line 24
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_user_group_index"]], 24, $context, $this->getSourceContext());
        // line 27
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_group_create")) {
            // line 28
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_user_group_create"]], 28, $context, $this->getSourceContext()));
            // line 31
            echo "    ";
        }
        // line 32
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 32), "value", [], "any", false, false, false, 32), "id", [], "any", false, false, false, 32) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_group_update"))) {
            // line 33
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_user_group_update", "params" => ["id" => "\$id"]]], 33, $context, $this->getSourceContext()));
            // line 37
            echo "    ";
        }
        // line 38
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 38, $context, $this->getSourceContext());
        echo "
";
    }

    // line 41
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 43
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 44
($context["form"] ?? null), "vars", [], "any", false, false, false, 44), "value", [], "any", false, false, false, 44), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_group_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.group.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 47
($context["form"] ?? null), "vars", [], "any", false, false, false, 47), "value", [], "any", false, false, false, 47), "name", [], "any", false, false, false, 47)];
            // line 49
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 51
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.group.entity_label")]);
            // line 52
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroUser/Group/update.html.twig", 52)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 53
            echo "    ";
        }
    }

    // line 56
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 57
        echo "    ";
        $context["id"] = "group-profile";
        // line 58
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 64
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "appendUsers", [], "any", false, false, false, 64), 'widget', ["id" => "groupAppendUsers"]), 1 =>         // line 65
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "removeUsers", [], "any", false, false, false, 65), 'widget', ["id" => "groupRemoveUsers"]), 2 =>         // line 66
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 66), 'row')]]]]];
        // line 71
        echo "
    ";
        // line 72
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 73
        echo "
    ";
        // line 74
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_plural_label"), "subblocks" => [0 => ["title" => null, "useSpan" => false, "data" => [0 => twig_call_macro($macros["dataGrid"], "macro_renderGrid", [        // line 81
($context["gridName"] ?? null), ["group_id" => ($context["entityId"] ?? null)], ["cssClass" => "inner-grid"]], 81, $context, $this->getSourceContext())]]]]]);
        // line 85
        echo "
    ";
        // line 86
        $context["data"] = ["formErrors" => ((        // line 87
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 88
($context["dataBlocks"] ?? null)];
        // line 90
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Group/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  177 => 90,  175 => 88,  174 => 87,  173 => 86,  170 => 85,  168 => 81,  167 => 74,  164 => 73,  162 => 72,  159 => 71,  157 => 66,  156 => 65,  155 => 64,  153 => 58,  150 => 57,  146 => 56,  141 => 53,  138 => 52,  135 => 51,  129 => 49,  127 => 47,  126 => 44,  124 => 43,  121 => 42,  117 => 41,  110 => 38,  107 => 37,  104 => 33,  101 => 32,  98 => 31,  95 => 28,  92 => 27,  90 => 24,  85 => 23,  80 => 21,  77 => 20,  75 => 17,  74 => 14,  72 => 13,  70 => 12,  67 => 11,  64 => 10,  60 => 9,  55 => 1,  53 => 7,  51 => 6,  49 => 5,  46 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Group/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Group/update.html.twig");
    }
}
