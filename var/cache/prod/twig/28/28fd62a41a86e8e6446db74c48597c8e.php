<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/content-sidebar.scss */
class __TwigTemplate_b2715a23cafb5a9647268407f71bd4d9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.content-with-sidebar {
    &--container {
        @include safe-area-offset(padding, 0);
    }

    &--controls {
        float: none;
    }

    &--control {
        width: auto;
    }

    &--header {
        padding: \$content-padding;
    }

    &--sidebar {
        max-width: \$content-sidebar-mobile-max-width;
        min-height: \$content-sidebar-mobile-min-height;

        overflow: visible;

        &.content-sidebar-maximized {
            width: 100%;
        }

        &.content-sidebar-minimized {
            width: 0;
            visibility: hidden;

            .content-with-sidebar--header {
                padding-top: 0;
            }

            .content-with-sidebar--controls {
                position: absolute;
                left: 0;
                z-index: \$mobile-app-header-z-index - 10;
                height: 40px;
                width: 32px;
                background-color: \$content-with-sidebar-controls-background;
                border-radius: 0 50% 50% 0;
                visibility: visible;

                .line-pattern {
                    position: fixed;
                    left: 0;
                    height: 100vh;
                    width: 5px;
                    background-color: \$content-with-sidebar-controls-background;
                    content: '';
                }
            }

            .content-with-sidebar--control {
                i {
                    color: \$content-with-sidebar-controls-color;
                    padding: 12px 0;
                    margin-left: 8px;
                }
            }
        }

        .sidebar-items {
            padding: \$content-sidebar-items-mobile-offset-inner;
        }

        .jstree-wrapper {
            padding: \$content-sidebar-jstree-wrapper-mobile-inner-offset;

            .jstree {
                margin: \$content-sidebar-jstree-wrapper-jstree-mobile-inner-offset;
            }
        }
    }
}

.layout-content,
.scrollable-container {
    .content-with-sidebar {
        &--sidebar {
            margin: \$content-sidebar-mobile-margin;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/content-sidebar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/content-sidebar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/content-sidebar.scss");
    }
}
