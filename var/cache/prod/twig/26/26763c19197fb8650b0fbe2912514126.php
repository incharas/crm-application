<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Opportunity/widget/info.html.twig */
class __TwigTemplate_61993e89d0527ca0abccee370d19424f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/Opportunity/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroSales/Opportunity/widget/info.html.twig", 2)->unwrap();
        // line 3
        $macros["channel"] = $this->macros["channel"] = $this->loadTemplate("@OroChannel/macros.html.twig", "@OroSales/Opportunity/widget/info.html.twig", 3)->unwrap();
        // line 4
        $macros["currency"] = $this->macros["currency"] = $this->loadTemplate("@OroCurrency/macros.html.twig", "@OroSales/Opportunity/widget/info.html.twig", 4)->unwrap();
        // line 5
        $macros["sales"] = $this->macros["sales"] = $this->loadTemplate("@OroSales/macros.html.twig", "@OroSales/Opportunity/widget/info.html.twig", 5)->unwrap();
        // line 6
        $macros["Tag"] = $this->macros["Tag"] = $this->loadTemplate("@OroTag/macros.html.twig", "@OroSales/Opportunity/widget/info.html.twig", 6)->unwrap();
        // line 7
        echo "
<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal\">
        <div class=\"responsive-block border-right\">
            <div class=\"row-fluid\">
                <div class=\"responsive-block\">
                    ";
        // line 13
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 13), ($context["entity"] ?? null), "name"], 13, $context, $this->getSourceContext());
        echo "
                    ";
        // line 14
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.status.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 14), ($context["entity"] ?? null), "status"], 14, $context, $this->getSourceContext());
        echo "

                    ";
        // line 16
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "dataChannel", [], "array", true, true, false, 16)) {
            // line 17
            echo "                        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", ($context["entity"] ?? null), "channel")) {
                // line 18
                echo "                            ";
                echo twig_call_macro($macros["channel"], "macro_renderChannelProperty", [($context["entity"] ?? null), "oro.sales.lead.data_channel.label"], 18, $context, $this->getSourceContext());
                echo "
                        ";
            }
            // line 20
            echo "                    ";
        }
        // line 21
        echo "
                    ";
        // line 22
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.probability.label"), ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "probability", [], "any", false, false, false, 22))) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "probability", [], "any", false, false, false, 22))) : ("")), ($context["entity"] ?? null), "probability"], 22, $context, $this->getSourceContext());
        echo "
                    ";
        // line 23
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.budget_amount.label"), ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "budgetAmount", [], "any", false, false, false, 23), "value", [], "any", false, false, false, 23))) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatCurrency(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "budgetAmount", [], "any", false, false, false, 23), "value", [], "any", false, false, false, 23), ["currency" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "budgetAmount", [], "any", false, false, false, 23), "currency", [], "any", false, false, false, 23)])) : ("")), ($context["entity"] ?? null), "budgetAmount"], 23, $context, $this->getSourceContext());
        echo "
                    ";
        // line 24
        echo twig_call_macro($macros["currency"], "macro_convert_to_base_currency", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "budgetAmount", [], "any", false, false, false, 24), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.budget_base_currency.label"), ($context["entity"] ?? null), "budgetAmount"], 24, $context, $this->getSourceContext());
        echo "

                    ";
        // line 26
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 26, $context, $this->getSourceContext());
        echo "
                    ";
        // line 27
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.close_date.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closeDate", [], "any", false, false, false, 27)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closeDate", [], "any", false, false, false, 27))) : ("")), ($context["entity"] ?? null), "closeDate"], 27, $context, $this->getSourceContext());
        echo "
                    ";
        // line 28
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.close_revenue.label"), ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closeRevenue", [], "any", false, false, false, 28), "value", [], "any", false, false, false, 28))) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatCurrency(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closeRevenue", [], "any", false, false, false, 28), "value", [], "any", false, false, false, 28), ["currency" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closeRevenue", [], "any", false, false, false, 28), "currency", [], "any", false, false, false, 28)])) : ("")), ($context["entity"] ?? null), "closeRevenue"], 28, $context, $this->getSourceContext());
        echo "
                    ";
        // line 29
        echo twig_call_macro($macros["currency"], "macro_convert_to_base_currency", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closeRevenue", [], "any", false, false, false, 29), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.budget_base_currency.label"), ($context["entity"] ?? null), "closeRevenue"], 29, $context, $this->getSourceContext());
        echo "
                    ";
        // line 30
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.close_reason.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closeReason", [], "any", false, false, false, 30), ($context["entity"] ?? null), "closeReason"], 30, $context, $this->getSourceContext());
        echo "
                    ";
        // line 31
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tag.entity_plural_label"), twig_call_macro($macros["Tag"], "macro_renderView", [($context["entity"] ?? null)], 31, $context, $this->getSourceContext())], 31, $context, $this->getSourceContext());
        echo "
                </div>

                <div class=\"responsive-block\">
                    ";
        // line 35
        echo twig_call_macro($macros["sales"], "macro_render_customer_info", [($context["entity"] ?? null)], 35, $context, $this->getSourceContext());
        echo "
                    ";
        // line 36
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.contact.label"), twig_call_macro($macros["ui"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 38
($context["entity"] ?? null), "contact", [], "any", false, false, false, 38), $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "contact", [], "any", false, false, false, 38)), "oro_contact_view"], 38, $context, $this->getSourceContext())], 36, $context, $this->getSourceContext());
        // line 39
        echo "

                    ";
        // line 41
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.lead.label"), twig_call_macro($macros["ui"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 43
($context["entity"] ?? null), "lead", [], "any", false, false, false, 43), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lead", [], "any", false, false, false, 43)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lead", [], "any", false, false, false, 43), "name", [], "any", false, false, false, 43)) : (null)), "oro_sales_lead_view"], 43, $context, $this->getSourceContext())], 41, $context, $this->getSourceContext());
        // line 44
        echo "
                </div>
            </div>
        </div>
        <div class=\"responsive-block\">
            ";
        // line 49
        echo twig_call_macro($macros["ui"], "macro_renderCollapsibleHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.customer_need.label"), $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "customerNeed", [], "any", false, false, false, 49)), ($context["entity"] ?? null), "customerNeed"], 49, $context, $this->getSourceContext());
        echo "
            ";
        // line 50
        echo twig_call_macro($macros["ui"], "macro_renderCollapsibleHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.proposed_solution.label"), $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "proposedSolution", [], "any", false, false, false, 50)), ($context["entity"] ?? null), "proposedSolution"], 50, $context, $this->getSourceContext());
        echo "
            ";
        // line 51
        echo twig_call_macro($macros["ui"], "macro_renderCollapsibleHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.opportunity.notes.label"), $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "notes", [], "any", false, false, false, 51)), ($context["entity"] ?? null), "notes"], 51, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Opportunity/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 51,  148 => 50,  144 => 49,  137 => 44,  135 => 43,  134 => 41,  130 => 39,  128 => 38,  127 => 36,  123 => 35,  116 => 31,  112 => 30,  108 => 29,  104 => 28,  100 => 27,  96 => 26,  91 => 24,  87 => 23,  83 => 22,  80 => 21,  77 => 20,  71 => 18,  68 => 17,  66 => 16,  61 => 14,  57 => 13,  49 => 7,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Opportunity/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Opportunity/widget/info.html.twig");
    }
}
