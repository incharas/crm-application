<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSearch/Search/searchResults.html.twig */
class __TwigTemplate_acf49aa0b642e029ecb1a966cab6b836 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroSearch/Search/searchResults.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroSearch/Search/searchResults.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%keyword%" => ((        // line 4
array_key_exists("searchString", $context)) ? (_twig_default_filter(($context["searchString"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.result.empty"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.result.empty")))]]);
        // line 5
        $context["gridName"] = "search-grid";
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    <div class=\"container-fluid search-header clearfix\">
        <h2>
            ";
        // line 10
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Search results", [], "messages");
        // line 11
        echo "        </h2>
        <form method=\"get\" action=\"";
        // line 12
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_search_results");
        echo "\" class=\"form-inline search-form\">
            <input type=\"hidden\" name=\"from\" value=\"\" />
            <div class=\"input-group\">
                <input type=\"text\" id=\"search\" class=\"form-control span2 search\" name=\"search\" value=\"";
        // line 15
        echo twig_escape_filter($this->env, ($context["searchString"] ?? null), "html", null, true);
        echo "\">
                <div class=\"input-group-append\">
                    <button type=\"submit\" class=\"btn btn-square-default btn-search\">";
        // line 17
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Search"), "html", null, true);
        echo "</button>
                </div>
            </div>
        </form>
    </div>

    ";
        // line 23
        if ((((array_key_exists("groupedResults", $context) && twig_in_filter("", twig_get_array_keys_filter(($context["groupedResults"] ?? null)))) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = ($context["groupedResults"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[""] ?? null) : null), "count", [], "any", false, false, false, 23) > 0)) || (twig_length_filter($this->env, ($context["groupedResults"] ?? null)) > 1))) {
            // line 24
            echo "        ";
            $context["togglerId"] = uniqid("dropdown-");
            // line 25
            echo "        <div class=\"oro-page collapsible-sidebar clearfix\">
            <div class=\"oro-page-sidebar search-entity-types-column dropdown\">
                <a href=\"#\" role=\"button\" id=\"";
            // line 27
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\" class=\"dropdown-toggle\" data-toggle=\"dropdown\"
                   aria-label=\"";
            // line 28
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.result.entity_filter"), "html", null, true);
            echo "\" aria-haspopup=\"true\" aria-expanded=\"false\">
                    <span class=\"fa-filter\" aria-hidden=\"true\"></span>
                    ";
            // line 30
            if (($context["selectedResult"] ?? null)) {
                // line 31
                echo "                        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["selectedResult"] ?? null), "class", [], "any", false, false, false, 31)) {
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["selectedResult"] ?? null), "label", [], "any", false, false, false, 31), "html", null, true);
                } else {
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.result.all"), "html", null, true);
                }
                // line 32
                echo "                        (";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["selectedResult"] ?? null), "count", [], "any", false, false, false, 32), "html", null, true);
                echo ")
                    ";
            }
            // line 34
            echo "                </a>
                <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"";
            // line 35
            echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
            echo "\">
                ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["groupedResults"] ?? null));
            foreach ($context['_seq'] as $context["alias"] => $context["type"]) {
                // line 37
                echo "                    ";
                $context["selected"] = ($context["alias"] == ($context["from"] ?? null));
                // line 38
                echo "                    ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "class", [], "any", false, false, false, 38)) {
                    // line 39
                    echo "                        ";
                    $context["label"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "label", [], "any", false, false, false, 39);
                    // line 40
                    echo "                        ";
                    $context["iconClass"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "icon", [], "any", false, false, false, 40);
                    // line 41
                    echo "                    ";
                } else {
                    // line 42
                    echo "                        ";
                    $context["label"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.search.result.all");
                    // line 43
                    echo "                        ";
                    $context["iconClass"] = "fa-search";
                    // line 44
                    echo "                    ";
                }
                // line 45
                echo "
                    ";
                // line 46
                if (twig_test_empty(($context["iconClass"] ?? null))) {
                    // line 47
                    echo "                        ";
                    $context["iconClass"] = "fa-file";
                    // line 48
                    echo "                    ";
                }
                // line 49
                echo "
                    <li";
                // line 50
                if (($context["selected"] ?? null)) {
                    echo " class=\"selected\"";
                }
                echo ">
                        <a href=\"";
                // line 51
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_search_results", ["from" => $context["alias"], "search" => ($context["searchString"] ?? null)]), "html", null, true);
                echo "\" ";
                if (($context["selected"] ?? null)) {
                    echo "aria-selected=\"true\"";
                }
                echo " title=\"";
                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                echo "\">
                            <span class=\"";
                // line 52
                echo twig_escape_filter($this->env, ($context["iconClass"] ?? null), "html", null, true);
                echo "\" aria-hidden=\"true\"></span>
                            <span class=\"search-label\">";
                // line 53
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
                echo "</span>
                            <span>&nbsp;(";
                // line 54
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["type"], "count", [], "any", false, false, false, 54), "html", null, true);
                echo ")</span>
                        </a>
                    </li>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['alias'], $context['type'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "                </ul>
            </div>
            <div class=\"oro-page-body search-results-column\">
                <div id=\"search-result-grid\">
                    ";
            // line 62
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ["from" => ($context["from"] ?? null), "search" => ($context["searchString"] ?? null)], ["cssClass" => "search-grid grid-without-header"]], 62, $context, $this->getSourceContext());
            echo "
                </div>
            </div>
        </div>
    ";
        } else {
            // line 67
            echo "    <div class=\"no-data\">
        ";
            // line 68
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("No results were found to match your search.", [], "messages");
            // line 69
            echo "        <br />
        ";
            // line 70
            echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\TranslationExtension')->trans("Try modifying your search criteria or creating a new ...", [], "messages");
            // line 71
            echo "    </div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroSearch/Search/searchResults.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  226 => 71,  224 => 70,  221 => 69,  219 => 68,  216 => 67,  208 => 62,  202 => 58,  192 => 54,  188 => 53,  184 => 52,  174 => 51,  168 => 50,  165 => 49,  162 => 48,  159 => 47,  157 => 46,  154 => 45,  151 => 44,  148 => 43,  145 => 42,  142 => 41,  139 => 40,  136 => 39,  133 => 38,  130 => 37,  126 => 36,  122 => 35,  119 => 34,  113 => 32,  106 => 31,  104 => 30,  99 => 28,  95 => 27,  91 => 25,  88 => 24,  86 => 23,  77 => 17,  72 => 15,  66 => 12,  63 => 11,  61 => 10,  57 => 8,  53 => 7,  49 => 1,  47 => 5,  45 => 4,  42 => 2,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSearch/Search/searchResults.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SearchBundle/Resources/views/Search/searchResults.html.twig");
    }
}
