<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Reset/dialog/forcePasswordResetConfirmation.html.twig */
class __TwigTemplate_f65351cd446f427ed7f04c37ec059738 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (array_key_exists("formAction", $context)) {
            // line 2
            echo "    <div class=\"widget-content\">
        <div class=\"form-container\">
            <form action=\"";
            // line 4
            echo twig_escape_filter($this->env, ($context["formAction"] ?? null), "html", null, true);
            echo "\" method=\"post\">
                <div class=\"modal-body ui-dialog-body\">
                    <p>";
            // line 6
            echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.password.force_reset.popup.message", ["{{ user }}" => (("<b>" . twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 7
($context["entity"] ?? null), "username", [], "any", false, false, false, 7))) . "</b>")]);
            // line 8
            echo "</p>
                </div>

                <div class=\"widget-actions form-actions\">
                    <button class=\"btn\" type=\"reset\">";
            // line 12
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
            echo "</button>
                    <button class=\"btn btn-success\" type=\"submit\">";
            // line 14
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.password.force_reset.popup.button"), "html", null, true);
            // line 15
            echo "</button>
                </div>
            </form>
        </div>
    </div>
";
        } else {
            // line 21
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["trigger" => [0 => ["eventFunction" => "execute", "name" => "refreshPage"]], "remove" => true]];
            // line 30
            echo "
    ";
            // line 31
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
";
        }
        // line 33
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Reset/dialog/forcePasswordResetConfirmation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  82 => 33,  77 => 31,  74 => 30,  71 => 21,  63 => 15,  61 => 14,  57 => 12,  51 => 8,  49 => 7,  48 => 6,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Reset/dialog/forcePasswordResetConfirmation.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Reset/dialog/forcePasswordResetConfirmation.html.twig");
    }
}
