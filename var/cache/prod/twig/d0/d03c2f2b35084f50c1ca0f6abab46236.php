<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivityList/ActivityList/widget/activities.html.twig */
class __TwigTemplate_b9202a57dfda8f050b25fb8776dcb625 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'widget_content' => [$this, 'block_widget_content'],
            'widget_actions' => [$this, 'block_widget_actions'],
            'items_container' => [$this, 'block_items_container'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroActivityList/ActivityList/widget/activities.html.twig", 1)->unwrap();
        // line 2
        $context["containerExtraClass"] = ((array_key_exists("containerExtraClass", $context)) ? (($context["containerExtraClass"] ?? null)) : (""));
        // line 3
        echo "<div class=\"widget-content activity-list ";
        echo twig_escape_filter($this->env, ($context["containerExtraClass"] ?? null), "html", null, true);
        echo "\">
    ";
        // line 4
        $context["pager"] = ["current" => 1, "pagesize" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_activity_list.per_page"), "total" => 1, "count" => 1, "sortingField" => $this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_activity_list.sorting_field")];
        // line 11
        echo "    ";
        $this->displayBlock('widget_content', $context, $blocks);
        // line 102
        echo "</div>
";
    }

    // line 11
    public function block_widget_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "
        ";
        // line 13
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("oro_activity_list_before", $context)) ? (_twig_default_filter(($context["oro_activity_list_before"] ?? null), "oro_activity_list_before")) : ("oro_activity_list_before")), ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 13)]);
        // line 14
        echo "
        ";
        // line 15
        $this->displayBlock('widget_actions', $context, $blocks);
        // line 55
        echo "        ";
        $this->displayBlock('items_container', $context, $blocks);
        // line 98
        echo "
        ";
        // line 99
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("oro_activity_list_after", $context)) ? (_twig_default_filter(($context["oro_activity_list_after"] ?? null), "oro_activity_list_after")) : ("oro_activity_list_after")), ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 99)]);
        // line 100
        echo "
    ";
    }

    // line 15
    public function block_widget_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "        <div class=\"grid-toolbar\">
            <div class=\"filter-box\">
                <div class=\"filter-container\"></div>
            </div>
            <div class=\"pagination\">
                ";
        // line 21
        $context["direction"] = ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_activity_list.sorting_direction") == "DESC");
        // line 22
        echo "                ";
        $context["prevButtonText"] = ((($context["direction"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.newer")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.older")));
        // line 23
        echo "                ";
        $context["nextButtonText"] = ((($context["direction"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.older")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activitylist.pagination.newer")));
        // line 24
        echo "
                <button data-section=\"top\"
                        data-action-name=\"goto_previous\"
                        class=\"btn pagination-previous\"
                        title=\"";
        // line 28
        echo twig_escape_filter($this->env, ($context["prevButtonText"] ?? null), "html", null, true);
        echo "\"
                        ";
        // line 29
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "current", [], "any", false, false, false, 29) == 1)) {
            echo "disabled";
        }
        // line 30
        echo "                        type=\"button\"
                >
                    <span class=\"fa-chevron-left\" aria-hidden=\"true\"></span>
                    ";
        // line 33
        echo twig_escape_filter($this->env, ($context["prevButtonText"] ?? null), "html", null, true);
        echo "
                </button>
                <button data-section=\"top\"
                        data-action-name=\"goto_next\"
                        class=\"btn icon-end pagination-next\"
                        title=\"";
        // line 38
        echo twig_escape_filter($this->env, ($context["nextButtonText"] ?? null), "html", null, true);
        echo "\"
                        type=\"button\"
                >
                    ";
        // line 41
        echo twig_escape_filter($this->env, ($context["nextButtonText"] ?? null), "html", null, true);
        echo "
                    <span class=\"fa-chevron-right\" aria-hidden=\"true\"></span>
                </button>
            </div>
            <div class=\"actions-panel\">
                ";
        // line 46
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "action btn btn-icon", "iCss" => "fa-refresh", "label" => (" " . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Refresh")), "dataAttributes" => ["action-name" => "refresh", "section" => "top"]]], 46, $context, $this->getSourceContext());
        // line 51
        echo "
            </div>
        </div>
        ";
    }

    // line 55
    public function block_items_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "            ";
        $context["options"] = ["widgetId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 57
($context["app"] ?? null), "request", [], "any", false, false, false, 57), "get", [0 => "_wid"], "method", false, false, false, 57), "ignoreHead" => ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_activity_list.grouping") == false), "activityListData" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_activity_list_api_get_list", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 62
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 63
($context["entity"] ?? null), "id", [], "any", false, false, false, 63)])), "activityListOptions" => ["configuration" =>         // line 67
($context["configuration"] ?? null), "template" => "#template-activity-list", "itemTemplate" => "#template-activity-item", "urls" => ["route" => "oro_activity_list_api_get_list", "parameters" => ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 73
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 74
($context["entity"] ?? null), "id", [], "any", false, false, false, 74)]], "loadingContainerSelector" => ".activity-list", "pager" =>         // line 78
($context["pager"] ?? null), "dateRangeFilterMetadata" =>         // line 79
($context["dateRangeFilterMetadata"] ?? null), "routes" => []], "commentOptions" => ["listTemplate" => "#template-activity-item-comment", "canCreate" => $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_comment_create")]];
        // line 87
        echo "
            <div class=\"container-fluid accordion\"
                data-page-component-module=\"oroactivitylist/js/app/components/activity-list-component\"
                data-page-component-options=\"";
        // line 90
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\"></div>
            ";
        // line 91
        $this->loadTemplate("@OroActivityList/ActivityList/js/list.html.twig", "@OroActivityList/ActivityList/widget/activities.html.twig", 91)->display(twig_array_merge($context, ["id" => "template-activity-list"]));
        // line 92
        echo "            ";
        $this->loadTemplate("@OroActivityList/ActivityList/js/view.html.twig", "@OroActivityList/ActivityList/widget/activities.html.twig", 92)->display(twig_array_merge($context, ["id" => "template-activity-item"]));
        // line 93
        echo "            ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["configuration"] ?? null));
        $context['loop'] = [
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        ];
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["activityClass"] => $context["activityOptions"]) {
            // line 94
            echo "                ";
            $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["activityOptions"], "template", [], "any", false, false, false, 94), "@OroActivityList/ActivityList/widget/activities.html.twig", 94)->display(twig_array_merge($context, ["id" => ("template-activity-item-" . $context["activityClass"])]));
            // line 95
            echo "            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['activityClass'], $context['activityOptions'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 96
        echo "            ";
        $this->loadTemplate("@OroComment/Comment/js/list.html.twig", "@OroActivityList/ActivityList/widget/activities.html.twig", 96)->display(twig_array_merge($context, ["id" => "template-activity-item-comment"]));
        // line 97
        echo "        ";
    }

    public function getTemplateName()
    {
        return "@OroActivityList/ActivityList/widget/activities.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 97,  219 => 96,  205 => 95,  202 => 94,  184 => 93,  181 => 92,  179 => 91,  175 => 90,  170 => 87,  168 => 79,  167 => 78,  166 => 74,  165 => 73,  164 => 67,  163 => 63,  162 => 62,  161 => 57,  159 => 56,  155 => 55,  148 => 51,  146 => 46,  138 => 41,  132 => 38,  124 => 33,  119 => 30,  115 => 29,  111 => 28,  105 => 24,  102 => 23,  99 => 22,  97 => 21,  90 => 16,  86 => 15,  81 => 100,  79 => 99,  76 => 98,  73 => 55,  71 => 15,  68 => 14,  66 => 13,  63 => 12,  59 => 11,  54 => 102,  51 => 11,  49 => 4,  44 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivityList/ActivityList/widget/activities.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActivityListBundle/Resources/views/ActivityList/widget/activities.html.twig");
    }
}
