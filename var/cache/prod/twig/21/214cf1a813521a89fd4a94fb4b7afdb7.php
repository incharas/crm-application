<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/range.js */
class __TwigTemplate_58150915dc235dcfb458a0d14e7ab95f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const __ = require('orotranslation/js/translator');
    const numberFormatter = require('orolocale/js/formatter/number');
    const defaultParam = {
        minMessage: 'This value should be ";
        // line 7
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " or more.',
        maxMessage: 'This value should be ";
        // line 8
        echo twig_escape_filter($this->env, ($context["limit"] ?? null), "js", null, true);
        echo " or less.',
        notInRangeMessage: 'This value should be between ";
        // line 9
        echo twig_escape_filter($this->env, ($context["min"] ?? null), "js", null, true);
        echo " and ";
        echo twig_escape_filter($this->env, ($context["max"] ?? null), "js", null, true);
        echo ".',
        invalidMessage: 'This value should be a valid number.'
    };

    /**
     * @export oroform/js/validator/range
     */
    /* TODO: currently this validator support only numeric range. To consistence with backend validator
            it needs to support DateTime range as well
     */
    return [
        'Range',
        function(value, element, param) {
            value = numberFormatter.unformatStrict(value);
            return this.optional(element) ||
                !(isNaN(value) ||
                    (param.min !== null && value < numberFormatter.unformatStrict(param.min)) ||
                    (param.max !== null && value > numberFormatter.unformatStrict(param.max)));
        },
        function(param, element) {
            let message;
            const placeholders = {};
            const value = this.elementValue(element);
            const normalizedValue = numberFormatter.unformatStrict(value);
            const normalizedMin = numberFormatter.unformatStrict(param.min);
            const normalizedMax = numberFormatter.unformatStrict(param.max);
            param = Object.assign({}, defaultParam, param);
            if (isNaN(normalizedValue)) {
                message = param.invalidMessage;
            } else if (param.max !== null && param.min !== null &&
                (normalizedValue < normalizedMin || normalizedValue > normalizedMax)) {
                message = param.notInRangeMessage;
                placeholders.max = numberFormatter.formatDecimal(param.max);
                placeholders.min = numberFormatter.formatDecimal(param.min);
            } else if (param.min !== null && normalizedValue < normalizedMin) {
                message = param.minMessage;
                placeholders.limit = numberFormatter.formatDecimal(param.min);
            } else if (param.max !== null && normalizedValue > normalizedMax) {
                message = param.maxMessage;
                placeholders.limit = numberFormatter.formatDecimal(param.max);
            }
            placeholders.value = numberFormatter.formatDecimal(value);
            return __(message, placeholders);
        }
    ];
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/range.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 9,  49 => 8,  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/range.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/range.js");
    }
}
