<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/editor/multi-select-editor-view.js */
class __TwigTemplate_ecb4b9df0b06ca3deca8c6fc7b062987 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const SelectEditorView = require('./select-editor-view');
    const _ = require('underscore');
    const select2autosizer = require('oroui/js/tools/select2-autosizer');

    /**
     * Multi-select content editor. Please note that it requires column data format
     * corresponding to multi-select-cell.
     *
     * ### Column configuration samples:
     * ``` yml
     * datagrids:
     *   {grid-uid}:
     *     inline_editing:
     *       enable: true
     *     # <grid configuration> goes here
     *     columns:
     *       # Sample 1. Full configuration
     *       {column-name-1}:
     *         inline_editing:
     *           editor:
     *             view: oroform/js/app/views/editor/multi-select-editor-view
     *             view_options:
     *               placeholder: '<placeholder>'
     *               css_class_name: '<class-name>'
     *               maximumSelectionLength: 3
     *           validation_rules:
     *             NotBlank: true
     *           save_api_accessor:
     *               route: '<route>'
     *               query_parameter_names:
     *                  - '<parameter1>'
     *                  - '<parameter2>'
     * ```
     *
     * ### Options in yml:
     *
     * Column option name                                  | Description
     * :---------------------------------------------------|:-----------
     * inline_editing.editor.view_options.placeholder      | Optional. Placeholder translation key for an empty element
     * inline_editing.editor.view_options.placeholder_raw  | Optional. Raw placeholder value
     * inline_editing.editor.view_options.css_class_name   | Optional. Additional css class name for editor view DOM el
     * inline_editing.editor.view_options.maximumSelectionLength | Optional. Maximum selection length
     * inline_editing.validation_rules          | Optional. Validation rules. See [documentation](../reference/js_validation.md#conformity-server-side-validations-to-client-once)
     * inline_editing.save_api_accessor                    | Optional. Sets accessor module, route, parameters etc.
     *
     * ### Constructor parameters
     *
     * @class
     * @param {Object} options - Options container
     * @param {Object} options.model - Current row model
     * @param {string} options.placeholder - Placeholder translation key for an empty element
     * @param {string} options.placeholder_raw - Raw placeholder value. It overrides placeholder translation key
     * @param {string} options.maximumSelectionLength - Maximum selection length
     * @param {Object} options.validationRules - Validation rules. See [documentation here](../reference/js_validation.md#conformity-server-side-validations-to-client-once)
     * @param {string} options.value - initial value of edited field
     *
     * @augments [SelectEditorView](./select-editor-view.md)
     * @exports MultiSelectEditorView
     */
    const MultiSelectEditorView = SelectEditorView.extend(/** @lends MultiSelectEditorView.prototype */{
        className: 'multi-select-editor',

        /**
         * @inheritdoc
         */
        constructor: function MultiSelectEditorView(options) {
            MultiSelectEditorView.__super__.constructor.call(this, options);
        },

        /**
         * @inheritdoc
         */
        initialize: function(options) {
            options.ignore_value_field_name = true;
            this.maximumSelectionLength = options.maximumSelectionLength;
            MultiSelectEditorView.__super__.initialize.call(this, options);
        },

        events: {
            'change input[name=value]': 'autoSize'
        },

        listen: {
            'change:visibility': 'autoSize'
        },

        autoSize: function() {
            select2autosizer.applyTo(this.\$el, this);
        },

        getSelect2Options: function() {
            const options = MultiSelectEditorView.__super__.getSelect2Options.call(this);
            options.multiple = true;
            options.maximumSelectionLength = this.maximumSelectionLength;
            return options;
        },

        formatRawValue: function(value) {
            return this.parseRawValue(value).join(',');
        },

        parseRawValue: function(value) {
            if (_.isString(value)) {
                value = JSON.parse(value);
            }
            if (_.isNull(value) || value === void 0) {
                // assume empty
                return [];
            }
            return value;
        },

        getValue: function() {
            const select2Value = this.\$('input[name=value]').val();
            let ids;
            if (select2Value !== '') {
                ids = select2Value.split(',').map(function(id) {
                    return id;
                });
            } else {
                ids = [];
            }
            return ids;
        }
    });

    return MultiSelectEditorView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/editor/multi-select-editor-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/editor/multi-select-editor-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/editor/multi-select-editor-view.js");
    }
}
