<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNotification/EmailNotification/Datagrid/Property/recipientUsersList.html.twig */
class __TwigTemplate_e35fe33ddb7ca50255d171ae4f693738 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        ob_start(function () { return ''; });
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["value"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["recipient"]) {
            // line 3
            echo "        ";
            if ( !twig_test_empty($context["recipient"])) {
                // line 4
                echo "            <li>";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName($context["recipient"]), "html", null, true);
                echo " <bdo dir=\"ltr\">&lt;";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "email", [], "any", false, false, false, 4), "html", null, true);
                echo "&gt;</bdo></li>
        ";
            }
            // line 6
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipient'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        $context["listItems"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 8
        if ( !twig_test_empty(($context["listItems"] ?? null))) {
            // line 9
            echo "<ul>
    ";
            // line 10
            echo ($context["listItems"] ?? null);
            echo "
</ul>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroNotification/EmailNotification/Datagrid/Property/recipientUsersList.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 10,  63 => 9,  61 => 8,  54 => 6,  46 => 4,  43 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNotification/EmailNotification/Datagrid/Property/recipientUsersList.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NotificationBundle/Resources/views/EmailNotification/Datagrid/Property/recipientUsersList.html.twig");
    }
}
