<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Role/clone.html.twig */
class __TwigTemplate_a06bec4db0f4e09f2d07068bf40e24e9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUser/Role/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $context["entity"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "data", [], "any", false, false, false, 2);
        // line 3
        $context["form"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "formView", [], "any", false, false, false, 3);
        // line 4
        $context["privilegesConfig"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "privilegesConfig", [], "any", false, false, false, 4);
        // line 5
        $context["allow_delete"] = false;
        // line 6
        $context["tabsOptions"] = ["data" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "tabs", [], "any", false, false, false, 6)];
        // line 7
        $context["capabilitySetOptions"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["context"] ?? null), "capabilitySetOptions", [], "any", false, false, false, 7);
        // line 1
        $this->parent = $this->loadTemplate("@OroUser/Role/update.html.twig", "@OroUser/Role/clone.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroUser/Role/clone.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 1,  51 => 7,  49 => 6,  47 => 5,  45 => 4,  43 => 3,  41 => 2,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Role/clone.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Role/clone.html.twig");
    }
}
