<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/js/groupedActivityItemTemplate.html.twig */
class __TwigTemplate_e0b2966df91c9099fbbf21705aff0fe6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'activityIcon' => [$this, 'block_activityIcon'],
            'activityDetails' => [$this, 'block_activityDetails'],
            'activityActions' => [$this, 'block_activityActions'],
            'activityContent' => [$this, 'block_activityContent'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/js/groupedActivityItemTemplate.html.twig", 2)->unwrap();
        // line 3
        $macros["EA"] = $this->macros["EA"] = $this->loadTemplate("@OroEmail/actions.html.twig", "@OroEmail/Email/js/groupedActivityItemTemplate.html.twig", 3)->unwrap();
        // line 5
        $context["entityClass"] = "Oro\\Bundle\\EmailBundle\\Entity\\Email";
        // line 6
        $context["entityName"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["entityClass"] ?? null), "label"));
        // line 1
        $this->parent = $this->loadTemplate("@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", "@OroEmail/Email/js/groupedActivityItemTemplate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_activityIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    <% if(is_head && !ignoreHead) { %>
        <span class=\"icon-email-thread\" aria-hidden=\"true\"></span>
    <% } else { %>
        <span class=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["entityClass"] ?? null), "icon"), "html_attr");
        echo "\" aria-hidden=\"true\"></span>
    <% } %>
";
    }

    // line 16
    public function block_activityDetails($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
        echo twig_escape_filter($this->env, ($context["entityName"] ?? null), "html", null, true);
        echo "
    <%
        var hasLink   = !!data.ownerLink;
        var ownerLink = hasLink
                ? '<a class=\"user\" href=\"' + data.ownerLink + '\">' +  _.escape(data.ownerName) + '</a>'
                : '<span class=\"user\">' + _.escape(data.ownerName) + '</span>';
        var updatedAt = updatedAt;
        var subject = subject;
        if(is_head && !ignoreHead) {
            ownerLink = hasLink
                ? '<a class=\"user\" href=\"' + data.ownerLink + '\">' +  _.escape(data.headOwnerName) + '</a>'
                : '<span class=\"user\">' + _.escape(data.headOwnerName) + '</span>';
            updatedAt = dateFormatter.formatSmartDateTime(data.headSentAt);
            subject = data.headSubject;
        }
    %>
    <%= _.template(
        ";
        // line 34
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.sent_by.label"));
        echo ",
        { interpolate: /\\{\\{(.+?)\\}\\}/g }
    )({
        user: ownerLink,
        date: '<span class=\"date\">' + updatedAt + '</span>'
    }) %>
";
    }

    // line 42
    public function block_activityActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/js/groupedActivityItemTemplate.html.twig", 43)->unwrap();
        // line 44
        echo "
    ";
        // line 45
        ob_start(function () { return ''; });
        // line 46
        echo "        ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_user_edit")) {
            // line 47
            echo "            <a href=\"#\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.placeholder"), "html_attr");
            echo "\"
               class=\"dropdown-item\"
               data-url=\"<%- routing.generate('oro_activity_context', {'id': relatedActivityId, 'activity': '";
            // line 49
            echo twig_escape_filter($this->env, twig_replace_filter(($context["entityClass"] ?? null), ["\\" => "_"]), "html", null, true);
            echo "' }) %>\"
                    ";
            // line 50
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "createOnEvent" => "click", "options" => ["alias" => "activity-context-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.add_context_entity.label"), "allowMaximize" => true, "allowMinimize" => true, "modal" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded", "height" => 600]]]], 50, $context, $this->getSourceContext());
            // line 69
            echo "><span class=\"fa-link hide-text\" aria-hidden=\"true\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.placeholder"), "html", null, true);
            echo "</span>
                ";
            // line 70
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.placeholder"), "html", null, true);
            echo "
            </a>
        ";
        }
        // line 73
        echo "    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 74
        echo "    ";
        $context["actions"] = [0 => ($context["action"] ?? null)];
        // line 75
        echo "
    ";
        // line 76
        ob_start(function () { return ''; });
        // line 77
        echo "        ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
            // line 78
            echo "            <a href=\"#\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "html", null, true);
            echo "\"
               class=\"dropdown-item\"
               data-url=\"<%- routing.generate('oro_email_email_reply', {'id': relatedActivityId, 'entityClass': targetEntityData.class, 'entityId': targetEntityData.id}) %>\"
               ";
            // line 81
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "createOnEvent" => "click", "options" => ["alias" => "email-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]], 81, $context, $this->getSourceContext());
            // line 98
            echo "><span class=\"fa-reply hide-text\" aria-hidden=\"true\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "html", null, true);
            echo "</span>
                ";
            // line 99
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply"), "html", null, true);
            echo "
            </a>
        ";
        }
        // line 102
        echo "    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 103
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 104
        echo "
    ";
        // line 105
        ob_start(function () { return ''; });
        // line 106
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
            // line 107
            echo "        <a href=\"#\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply_all"), "html_attr");
            echo "\"
           class=\"dropdown-item\"
           data-url=\"<%- routing.generate('oro_email_email_reply_all', {'id': relatedActivityId, 'entityClass': targetEntityData.class, 'entityId': targetEntityData.id}) %>\"
                ";
            // line 110
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "createOnEvent" => "click", "options" => ["alias" => "email-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]], 110, $context, $this->getSourceContext());
            // line 127
            echo "><span class=\"fa-reply-all hide-text\" aria-hidden=\"true\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply_all"), "html", null, true);
            echo "</span>
            ";
            // line 128
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.reply_all"), "html", null, true);
            echo "
        </a>
    ";
        }
        // line 131
        echo "    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 132
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 133
        echo "
    ";
        // line 134
        ob_start(function () { return ''; });
        // line 135
        echo "        ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_email_email_create")) {
            // line 136
            echo "            <a href=\"#\" title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "html", null, true);
            echo "\"
               class=\"dropdown-item\"
               data-url=\"<%- routing.generate('oro_email_email_forward', {'id': relatedActivityId, 'entityClass': targetEntityData.class, 'entityId': targetEntityData.id}) %>\"
               ";
            // line 139
            echo twig_call_macro($macros["UI"], "macro_renderWidgetAttributes", [["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "createOnEvent" => "click", "options" => ["alias" => "forward-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]], 139, $context, $this->getSourceContext());
            // line 156
            echo "><span class=\"fa-mail-forward hide-text\" aria-hidden=\"true\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "html", null, true);
            echo "</span>
                ";
            // line 157
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.forward"), "html", null, true);
            echo "
            </a>
        ";
        }
        // line 160
        echo "    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 161
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 162
        echo "
    ";
        // line 163
        ob_start(function () { return ''; });
        // line 164
        echo "        <a href=\"<%- routing.generate('oro_email_thread_view', {'id': relatedActivityId}) %>\"
           class=\"dropdown-item\"
           title=\"";
        // line 166
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.view"), "html_attr");
        echo "\"><span
                class=\"fa-eye hide-text\" aria-hidden=\"true\">";
        // line 167
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.view"), "html", null, true);
        echo "</span>
            ";
        // line 168
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.view"), "html", null, true);
        echo "
        </a>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 171
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 172
        echo "
    ";
        // line 173
        $this->displayParentBlock("activityActions", $context, $blocks);
        echo "
";
    }

    // line 176
    public function block_activityContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 177
        echo "    <div class=\"activity-item-email-content\">
        ";
        // line 179
        echo "        <div class=\"info <% if (is_head && !ignoreHead) { %>thread<% } %>\" id=\"grouped-entity-<%- data.entityId %>\"></div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/js/groupedActivityItemTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  298 => 179,  295 => 177,  291 => 176,  285 => 173,  282 => 172,  279 => 171,  273 => 168,  269 => 167,  265 => 166,  261 => 164,  259 => 163,  256 => 162,  253 => 161,  250 => 160,  244 => 157,  239 => 156,  237 => 139,  230 => 136,  227 => 135,  225 => 134,  222 => 133,  219 => 132,  216 => 131,  210 => 128,  205 => 127,  203 => 110,  196 => 107,  193 => 106,  191 => 105,  188 => 104,  185 => 103,  182 => 102,  176 => 99,  171 => 98,  169 => 81,  162 => 78,  159 => 77,  157 => 76,  154 => 75,  151 => 74,  148 => 73,  142 => 70,  137 => 69,  135 => 50,  131 => 49,  125 => 47,  122 => 46,  120 => 45,  117 => 44,  114 => 43,  110 => 42,  99 => 34,  78 => 17,  74 => 16,  67 => 12,  62 => 9,  58 => 8,  53 => 1,  51 => 6,  49 => 5,  47 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/js/groupedActivityItemTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/js/groupedActivityItemTemplate.html.twig");
    }
}
