<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/layouts/default/page/layout.html.twig */
class __TwigTemplate_d9753bb9725cc33d31c9f27658927555 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'datagrid_row_widget' => [$this, 'block_datagrid_row_widget'],
            'datagrid_cell_widget' => [$this, 'block_datagrid_cell_widget'],
            'datagrid_cell_value_widget' => [$this, 'block_datagrid_cell_value_widget'],
            'datagrid_header_row_widget' => [$this, 'block_datagrid_header_row_widget'],
            'datagrid_header_cell_widget' => [$this, 'block_datagrid_header_cell_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('datagrid_row_widget', $context, $blocks);
        // line 6
        echo "
";
        // line 7
        $this->displayBlock('datagrid_cell_widget', $context, $blocks);
        // line 14
        $this->displayBlock('datagrid_cell_value_widget', $context, $blocks);
        // line 18
        $this->displayBlock('datagrid_header_row_widget', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('datagrid_header_cell_widget', $context, $blocks);
    }

    // line 1
    public function block_datagrid_row_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <script type=\"text/html\" id=\"template-datagrid-row\">
        ";
        // line 3
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </script>
";
    }

    // line 7
    public function block_datagrid_cell_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => ""]);
        // line 11
        echo "    <td <%= attributes('";
        echo twig_escape_filter($this->env, ($context["column_name"] ?? null), "html", null, true);
        echo "', ";
        echo json_encode(($context["attr"] ?? null));
        echo ") %>>";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</td>
";
    }

    // line 14
    public function block_datagrid_cell_value_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 15
        echo "<%- render('";
        echo twig_escape_filter($this->env, ($context["column_name"] ?? null), "html", null, true);
        echo "') %>";
    }

    // line 18
    public function block_datagrid_header_row_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    <script type=\"text/html\" id=\"template-datagrid-header-row\">
        ";
        // line 20
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </script>
";
    }

    // line 24
    public function block_datagrid_header_cell_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => ""]);
        // line 28
        echo "    <th <%= attributes('";
        echo twig_escape_filter($this->env, ($context["column_name"] ?? null), "html", null, true);
        echo "', ";
        echo json_encode(($context["attr"] ?? null));
        echo ") %>><%- render('";
        echo twig_escape_filter($this->env, ($context["column_name"] ?? null), "html", null, true);
        echo "') %></th>
";
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/layouts/default/page/layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  123 => 28,  120 => 25,  116 => 24,  109 => 20,  106 => 19,  102 => 18,  96 => 15,  92 => 14,  81 => 11,  78 => 8,  74 => 7,  67 => 3,  64 => 2,  60 => 1,  56 => 24,  53 => 23,  51 => 18,  49 => 14,  47 => 7,  44 => 6,  42 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/layouts/default/page/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/layouts/default/page/layout.html.twig");
    }
}
