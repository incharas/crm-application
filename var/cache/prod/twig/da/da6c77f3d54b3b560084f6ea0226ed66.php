<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/react-app-component.js */
class __TwigTemplate_685261220f1c38101a62795feec133f0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import React from 'react';
import ReactDOM from 'react-dom';
import BaseComponent from 'oroui/js/app/components/base/component';
import loadModules from 'oroui/js/app/services/load-modules';

/**
 * Creates a React app with passed 'react_app' module and mount in _sourceElement
 */
const ReactAppComponent = BaseComponent.extend({
    app: null,

    appContainerElement: null,

    constructor: function ReactAppComponent(...args) {
        ReactAppComponent.__super__.constructor.apply(this, args);
    },

    initialize(options) {
        this._initVueApp(options);
    },

    async _initVueApp(options) {
        const {
            _sourceElement,
            _subPromises,
            name,
            reactApp,
            ...props
        } = options || {};

        this.appContainerElement = _sourceElement[0];

        this._deferredInit();
        const App = await this._loadModule(reactApp);

        if (this.disposed) {
            this._resolveDeferredInit();
            return;
        }

        this.createApp(App, props);

        this._resolveDeferredInit();
    },

    createApp(App, props) {
        this.app = ReactDOM.render(
            React.createElement(App, props),
            this.appContainerElement
        );
    },

    async _loadModule(reactApp) {
        if (!reactApp) {
            throw new Error('Missing app module name');
        }

        return await loadModules(reactApp);
    },

    dispose() {
        if (this.disposed) {
            return;
        }

        ReactDOM.unmountComponentAtNode(this.appContainerElement);
        delete this.app;

        ReactAppComponent.__super__.dispose.call(this);
    }
});

export default ReactAppComponent;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/react-app-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/react-app-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/react-app-component.js");
    }
}
