<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/js/extend/bootstrap.js */
class __TwigTemplate_fd241db4949893d6ef7daf1a4ba66367 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    require('oroui/js/extend/bootstrap/bootstrap-dropdown');
    require('oroui/js/extend/bootstrap/bootstrap-modal');
    require('oroui/js/extend/bootstrap/bootstrap-popover');
    require('oroui/js/extend/bootstrap/bootstrap-scrollspy');
    require('oroui/js/extend/bootstrap/bootstrap-tooltip');
    require('oroui/js/extend/bootstrap/bootstrap-typeahead');
    require('oroui/js/extend/bootstrap/bootstrap-tabs');
    require('oroui/js/extend/bootstrap/bootstrap-collapse');
    require('bootstrap');
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/js/extend/bootstrap.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/js/extend/bootstrap.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/js/extend/bootstrap.js");
    }
}
