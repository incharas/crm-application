<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/macros.html.twig */
class __TwigTemplate_fafc0367e831c4f3a0dd68d07d05ee47 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 13
    public function macro_renderStartButton($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 14
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroHangoutsCall/macros.html.twig", 14)->unwrap();
            // line 15
            echo "    ";
            $context["pageComponent"] = ["module" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 16
($context["parameters"] ?? null), "componentModule", [], "any", true, true, false, 16)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "componentModule", [], "any", false, false, false, 16)) : ("oroui/js/app/components/view-component")), "options" => ["view" => "orohangoutscall/js/app/views/start-button-view", "autoRender" => true, "hangoutOptions" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 20
($context["parameters"] ?? null), "hangoutOptions", [], "any", true, true, false, 20)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "hangoutOptions", [], "any", false, false, false, 20)) : ([]))]];
            // line 23
            echo "    ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "componentName", [], "any", true, true, false, 23)) {
                // line 24
                echo "        ";
                $context["pageComponent"] = twig_array_merge(($context["pageComponent"] ?? null), ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 25
($context["parameters"] ?? null), "componentName", [], "any", false, false, false, 25)]);
                // line 27
                echo "    ";
            }
            // line 28
            echo "    <div class=\"start-hangout-button-placeholder";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", true, true, false, 28)) {
                echo " ";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "class", [], "any", false, false, false, 28), "html", null, true);
            }
            echo "\"
            ";
            // line 29
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", true, true, false, 29) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", false, false, false, 29)))) {
                // line 30
                echo "                ";
                echo twig_call_macro($macros["UI"], "macro_renderAttributes", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "dataAttributes", [], "any", false, false, false, 30)], 30, $context, $this->getSourceContext());
                echo "
            ";
            }
            // line 32
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["pageComponent"] ?? null)], 32, $context, $this->getSourceContext());
            echo "></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  86 => 32,  80 => 30,  78 => 29,  70 => 28,  67 => 27,  65 => 25,  63 => 24,  60 => 23,  58 => 20,  57 => 16,  55 => 15,  52 => 14,  39 => 13,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/macros.html.twig");
    }
}
