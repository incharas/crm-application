<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/textarea.scss */
class __TwigTemplate_e68169283569a30bb7c10db613af0999 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

.textarea {
    min-height: \$textarea-min-height;
    resize: vertical;

    @include base-ui-element(
        \$use-base-style-for-textarea,
        \$textarea-padding,
        \$textarea-font-size,
        \$textarea-font-family,
        \$textarea-line-height,
        \$textarea-border,
        \$textarea-border-radius,
        \$textarea-background-color,
        \$textarea-color
    );

    @include placeholder {
        color: \$textarea-placeholder-color;
    }

    @include element-state('hover') {
        border-color: \$textarea-border-color-hover-state;
    }

    @include element-state('focus') {
        border-color: \$textarea-border-color-focus-state;
        box-shadow: \$textarea-box-shadow-focus-state;
    }

    &.focus-visible {
        border-color: \$textarea-border-color-focus-state;
    }

    @include element-state('error') {
        border-color: \$textarea-border-color-error-state;
    }

    @include element-state('disabled') {
        background: \$textarea-border-color-disabled-background;

        @include base-disabled-style();

        @include element-state('hover') {
            border-color: \$textarea-border-color-disabled-hover-border-color;
        }
    }

    @include element-state('error') {
        box-shadow: \$textarea-box-shadow-error-state;
    }

    @include element-state('disabled') {
        background: \$textarea-background-color-disabled-state;
        box-shadow: none;
        border: \$textarea-border;
    }

    &--size-m {
        padding: \$textarea-padding--m;
    }

    &--size-s {
        padding: \$textarea-padding--s;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/textarea.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/textarea.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/textarea.scss");
    }
}
