<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/components/select-create-inline-type-async-component.js */
class __TwigTemplate_a6ae3178f3185ee1a69938115bfe4fb6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const SelectCreateInlineTypeAsyncView = require('oroform/js/app/views/select-create-inline-type-async-view');
    const SelectCreateInlineTypeComponent = require('oroform/js/app/components/select-create-inline-type-component');

    const SelectCreateInlineTypeAsyncComponent = SelectCreateInlineTypeComponent.extend({
        ViewConstructor: SelectCreateInlineTypeAsyncView,

        /**
         * @inheritdoc
         */
        constructor: function SelectCreateInlineTypeAsyncComponent(options) {
            SelectCreateInlineTypeAsyncComponent.__super__.constructor.call(this, options);
        }
    });

    return SelectCreateInlineTypeAsyncComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/components/select-create-inline-type-async-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/components/select-create-inline-type-async-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/components/select-create-inline-type-async-component.js");
    }
}
