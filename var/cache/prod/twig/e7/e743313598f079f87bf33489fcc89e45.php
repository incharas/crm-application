<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTracking/TrackingWebsite/view.html.twig */
class __TwigTemplate_a9a4c43c14dcb24f30c89c715bdfbca5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTracking/TrackingWebsite/view.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroTracking/TrackingWebsite/view.html.twig", 3)->unwrap();
        // line 4
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroTracking/TrackingWebsite/view.html.twig", 4)->unwrap();
        // line 5
        $macros["U"] = $this->macros["U"] = $this->loadTemplate("@OroUser/macros.html.twig", "@OroTracking/TrackingWebsite/view.html.twig", 5)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "name", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 6), "N/A")) : ("N/A"))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroTracking/TrackingWebsite/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTracking/TrackingWebsite/view.html.twig", 9)->unwrap();
        // line 10
        echo "
    ";
        // line 11
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 12
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tracking_website_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["entity"] ?? null), "id", [], "any", false, false, false, 13)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.entity_label")]], 12, $context, $this->getSourceContext());
            // line 15
            echo "
    ";
        }
        // line 17
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 18
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_tracking_website", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 19
($context["entity"] ?? null), "id", [], "any", false, false, false, 19)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tracking_website_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-user", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 23
($context["entity"] ?? null), "id", [], "any", false, false, false, 23), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.entity_label")]], 18, $context, $this->getSourceContext());
            // line 25
            echo "
    ";
        }
    }

    // line 29
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 31
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_tracking_website_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 34
($context["entity"] ?? null), "name", [], "any", false, false, false, 34)];
        // line 36
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 39
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTracking/TrackingWebsite/view.html.twig", 40)->unwrap();
        // line 42
        ob_start(function () { return ''; });
        // line 43
        echo "<div class=\"row-fluid\">
        <div class=\"responsive-block\">
            ";
        // line 45
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.name.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 45)], 45, $context, $this->getSourceContext());
        echo "
            ";
        // line 46
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.identifier.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "identifier", [], "any", false, false, false, 46)], 46, $context, $this->getSourceContext());
        // line 48
        ob_start(function () { return ''; });
        // line 49
        echo "<a target=\"_blank\" href=\"";
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\SecurityBundle\Twig\OroSecurityExtension']->stripDangerousProtocols(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "url", [], "any", false, false, false, 49)), "html", null, true);
        echo "\">
                ";
        // line 50
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "url", [], "any", false, false, false, 50), "html", null, true);
        echo "
            </a>";
        $context["url"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 53
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.url.label"), ($context["url"] ?? null)], 53, $context, $this->getSourceContext());
        // line 55
        ob_start(function () { return ''; });
        // line 56
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 56)) {
            // line 57
            echo twig_call_macro($macros["U"], "macro_render_user_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 57)], 57, $context, $this->getSourceContext());
            echo "
                ";
            // line 58
            echo twig_call_macro($macros["U"], "macro_user_business_unit_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 58)], 58, $context, $this->getSourceContext());
        }
        $context["ownerData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 61
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.trackingwebsite.owner.label"), ($context["ownerData"] ?? null)], 61, $context, $this->getSourceContext());
        echo "
        </div>
        <div class=\"responsive-block\">
            ";
        // line 64
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 64, $context, $this->getSourceContext());
        echo "
        </div>
    </div>";
        $context["generalInformation"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 68
        ob_start(function () { return ''; });
        // line 69
        echo "<div class=\"row-fluid\">
        <div class=\"responsive-block\">
            <p>";
        // line 71
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.help.event_tooltip"), "html", null, true);
        echo "</p>
            ";
        // line 72
        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 72), "secure", [], "any", false, false, false, 72)) {
            // line 73
            echo "                <p><strong>";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.help.secure"), "html", null, true);
            echo "</strong></p>
            ";
        }
        // line 75
        echo "            <textarea class=\"code code-script\">";
        // line 76
        $this->loadTemplate("@OroTracking/TrackingWebsite/script.js.twig", "@OroTracking/TrackingWebsite/view.html.twig", 76)->display($context);
        // line 77
        echo "</textarea>
        </div>
    </div>";
        $context["trackingCode"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 82
        ob_start(function () { return ''; });
        // line 83
        echo "        ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["website-tracking-events-grid", ["websiteIdentifier" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 85
($context["entity"] ?? null), "identifier", [], "any", false, false, false, 85)], ["cssClass" => "inner-grid"]], 84, $context, $this->getSourceContext());
        // line 87
        echo "
    ";
        $context["trackingEventsGrid"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 89
        echo "
    ";
        // line 90
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.block.general"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 95
($context["generalInformation"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.block.tracking"), "subblocks" => [0 => ["data" => [0 =>         // line 101
($context["trackingCode"] ?? null)]]]], 2 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.tracking.block.events"), "subblocks" => [0 => ["data" => [0 =>         // line 107
($context["trackingEventsGrid"] ?? null)]]]]];
        // line 111
        echo "
    ";
        // line 112
        $context["id"] = "websiteView";
        // line 113
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 114
        echo "
    ";
        // line 115
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTracking/TrackingWebsite/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  223 => 115,  220 => 114,  217 => 113,  215 => 112,  212 => 111,  210 => 107,  209 => 101,  208 => 95,  207 => 90,  204 => 89,  200 => 87,  198 => 85,  196 => 83,  194 => 82,  189 => 77,  187 => 76,  185 => 75,  179 => 73,  177 => 72,  173 => 71,  169 => 69,  167 => 68,  161 => 64,  155 => 61,  151 => 58,  147 => 57,  145 => 56,  143 => 55,  141 => 53,  136 => 50,  131 => 49,  129 => 48,  127 => 46,  123 => 45,  119 => 43,  117 => 42,  114 => 40,  110 => 39,  103 => 36,  101 => 34,  100 => 31,  98 => 30,  94 => 29,  88 => 25,  86 => 23,  85 => 19,  83 => 18,  80 => 17,  76 => 15,  74 => 13,  72 => 12,  70 => 11,  67 => 10,  64 => 9,  60 => 8,  55 => 1,  53 => 6,  50 => 5,  48 => 4,  46 => 3,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTracking/TrackingWebsite/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/TrackingBundle/Resources/views/TrackingWebsite/view.html.twig");
    }
}
