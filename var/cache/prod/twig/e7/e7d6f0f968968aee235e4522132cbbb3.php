<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/jstree.scss */
class __TwigTemplate_c90b60298d054218106f5668f72de0f7 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:color';

\$tree-link-color: \$primary-200 !default;
\$tree-link-bg-color: \$extra-200 !default;
\$tree-icon-color: \$primary-400 !default;
\$tree-icon-color-hover: color.adjust(\$tree-icon-color, \$lightness: -10%) !default;

\$jstree-wholerow-ul-width: 100% !default;
\$jstree-wholerow-ul-offset-bottom: 10px !default;

\$jstree-default-node-position: static !default;
\$jstree-default-node-background: transparent !default;
\$jstree-default-node-offset-left: 24px !default;

\$jstree-default-container-node-offset-left: \$jstree-default-node-offset-left !default;

\$jstree-default-ocl-background: transparent !default;

\$jstree-themeicon-display: none !default;

\$jstree-open-font-family: \$fa-font-family !default;
\$jstree-open-font-size: 15px !default;
\$jstree-open-font-weight: font-weight('normal') !default;
\$jstree-open-font-style: normal !default;
\$jstree-open-color: \$primary-550 !default;

\$jstree-open-jocl-content: \$fa-var-angle-down !default;
\$jstree-closed-jocl-content: var(--fa-var-angle-right) !default;

\$jstree-icon-width: 21px !default;
\$jstree-icon-height: 36px !default;
\$jstree-icon-font-size: 16px !default;
\$jstree-icon-line-height: \$jstree-icon-height !default;

\$jstree-wholerow-hovered-height: 35px !default;
\$jstree-wholerow-hovered-background: \$tree-link-bg-color !default;
\$jstree-wholerow-disabled-background: #efefef !default;
\$jstree-wholerow-disabled-clicked-background: color.adjust(
    \$jstree-wholerow-disabled-background, \$lightness: -10%
) !default;

\$jstree-anchor-display: inline-block !default;
\$jstree-anchor-width: 90% !default;
\$jstree-anchor-height: 36px !default;
\$jstree-anchor-inner-offset: 0 8px 7px 8px !default;
\$jstree-anchor-line-height: 36px !default;
\$jstree-anchor-color: \$tree-link-color !default;
\$jstree-anchor-border-radius: 0 !default;
\$jstree-anchor-box-shadow: none !default;
\$jstree-anchor-text-overflow: ellipsis !default;
\$jstree-anchor-overflow: hidden !default;

\$jstree-anchor-active-color: \$tree-link-color !default;

\$jstree-wholerow-transition: none !default;

\$jstree-search-font-style: normal !default;
\$jstree-search-font-weight: font-weight('normal') !default;

\$jstree-icon-checkbox-display: inline-block !default;
\$jstree-icon-checkbox-height: 16px !default;
\$jstree-icon-checkbox-width: 16px !default;
\$jstree-icon-checkbox-border: 2px solid \$primary-750 !default;
\$jstree-icon-checkbox-background: \$primary-inverse !default;
\$jstree-icon-checkbox-border-radius: 3px !default;
\$jstree-icon-checkbox-cursor: pointer !default;
\$jstree-icon-checkbox-vertical-align: text-bottom !default;
\$jstree-icon-checkbox-position: relative !default;
\$jstree-icon-checkbox-color: \$primary-inverse !default;
\$jstree-icon-checkbox-offset: 0 8px 0 -6px !default;

\$jstree-icon-checkbox-icon-position: absolute !default;
\$jstree-icon-checkbox-icon-top: 50% !default;
\$jstree-icon-checkbox-icon-left: 50% !default;
\$jstree-icon-checkbox-icon-transform: translate(-50%, -50%) !default;
\$jstree-icon-checkbox-icon-offset-left: -1px !default;
\$jstree-icon-checkbox-icon-font-size: 10px !default;
\$jstree-icon-checkbox-icon-line-height: 1 !default;
\$jstree-icon-checkbox-icon-font-family: \$fa-font-family !default;

\$jstree-checked-background: \$secondary !default;
\$jstree-checked-border-color: \$secondary !default;
\$jstree-checked-content: '\\f00c' !default;

\$jstree-loading-inner-offset: \$content-padding - 4px !default;

\$jstree-search-component-position: relative !default;
\$jstree-search-component-bottom-offset: \$content-padding-medium !default;

\$jstree-search-component-clear-active-search-clear-display: block !default;
\$jstree-search-component-clear-active-search-search-display: none !default;

\$jstree-search-component-input-width: 100% !default;
\$jstree-search-component-input-bottom-offset: 0 !default;

\$jstree-search-component-clear-active-input-display: none !default;

\$jstree-search-component-clear-icon-display: none !default;
\$jstree-search-component-clear-icon-position: absolute !default;
\$jstree-search-component-clear-icon-top: 50% !default;
\$jstree-search-component-clear-offset-top: -12px !default;
\$jstree-search-component-clear-icon-right: 10px !default;
\$jstree-search-component-clear-icon-font-size: 16px !default;
\$jstree-search-component-clear-icon-color: \$tree-icon-color !default;
\$jstree-search-component-clear-icon-cursor: pointer !default;

\$jstree-search-component-clear-icon-hover-color: \$tree-icon-color-hover !default;

\$jstree-search-component-search-icon-display: block !default;
\$jstree-search-component-search-icon-pointer-events: none !default;

\$tree-empty-content-sidebar-inner-offset: \$content-padding-medium !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/jstree.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/jstree.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/jstree.scss");
    }
}
