<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/oro-tabs.scss */
class __TwigTemplate_33e0ee92f65910c4b6afafad9695833c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$oro-tabs-vertical-display: flex !default;

\$oro-tabs-head-width: 180px !default;
\$oro-tabs-head-flex-grow: 0 !default;
\$oro-tabs-head-flex-shrink: 0 !default;
\$oro-tabs-head-outer-offset-right: -2px !default;
\$oro-tabs-head-position: relative !default;

\$oro-tabs-content-flex-grow: 1 !default;
\$oro-tabs-content-border-left: 2px solid \$primary-750 !default;

\$oro-tabs-subtitle-inner-offset: 8px 12px !default;
\$oro-tabs-subtitle-font-weight: font-weight('bold') !default;

\$oro-tabs-fill-tab-border: none !default;
\$oro-tabs-fill-tab-box-shadow: none !default;
\$oro-tabs-fill-tab-box-sizing: border-box !default;
\$oro-tabs-fill-tab-width: 100% !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/oro-tabs.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/oro-tabs.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/oro-tabs.scss");
    }
}
