<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app.js */
class __TwigTemplate_940377e35b275a29c29730fd6e5396fb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "const config = require('module-config').default(module.id);

if ('publicPath' in config) {
    // eslint-disable-next-line no-undef, camelcase
    __webpack_public_path__ = config.publicPath;
}

module.exports = Promise.all(
    require('./polyfills').default
).then(() => Promise.all([
    require('oronavigation/js/routes-loader'),
    require('orotranslation/js/translation-loader')
])).then(() => {
    const \$ = require('jquery');
    const _ = require('underscore');
    const Application = require('oroui/js/app/application');
    const routes = require('oroui/js/app/routes');
    const promises = require('app-modules').default;
    promises.push(\$.when(\$.ready));

    return Promise.all(promises).then(() => {
        const options = _.extend({}, config, {
            // load routers
            routes: function(match) {
                let i;
                for (i = 0; i < routes.length; i += 1) {
                    match(routes[i][0], routes[i][1]);
                }
            },
            // define template for page title
            titleTemplate: function(data) {
                return data.subtitle || '';
            }
        });
        return new Application(options);
    });
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app.js");
    }
}
