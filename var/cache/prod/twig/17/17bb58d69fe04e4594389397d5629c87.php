<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Calendar/Autocomplete/selection.html.twig */
class __TwigTemplate_b432b8a6bead600281522d8fe927543b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<picture>
    <% _.each(avatar.sources || [], function(source) { %>
    <source srcset=\"<%- source.srcset %>\" type=\"<%- source.type %>\">
    <% }); %>
    <img src=\"<% if (avatar.src) { %><%- avatar.src %><% } else { %>";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroui/img/avatar-xsmall.png"), "html_attr");
        echo "<% } %>\" width=\"16\" height=\"16\">
</picture>&nbsp;<%- fullName %>
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Calendar/Autocomplete/selection.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 5,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Calendar/Autocomplete/selection.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Calendar/Autocomplete/selection.html.twig");
    }
}
