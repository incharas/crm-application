<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/messenger-module.js */
class __TwigTemplate_8f5fac7e27b5bc7ce350dc93da3f04a9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import mediator from 'oroui/js/mediator';
import messenger from 'oroui/js/messenger';

/**
 * Init messenger's handlers
 */
mediator.setHandler('addMessage', messenger.addMessage, messenger);
mediator.setHandler('showMessage', messenger.notificationMessage, messenger);
mediator.setHandler('showProcessingMessage', messenger.showProcessingMessage, messenger);
mediator.setHandler('showFlashMessage', messenger.notificationFlashMessage, messenger);
mediator.setHandler('showErrorMessage', messenger.showErrorMessage, messenger);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/messenger-module.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/messenger-module.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/messenger-module.js");
    }
}
