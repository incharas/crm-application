<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/macros/direction.html */
class __TwigTemplate_d85ef96cf9d93e12cc953df249f80684 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (obj.content) {
    var renderedAttrs = '';
    var attributes = ['dir', 'class'];

    obj.dir = obj.dir ? obj.dir : 'ltr';
    _.each(obj, function(value, key) {
        if (attributes.indexOf(key) !== -1) {
            var attr = key + '=\"' + _.escape(value) + '\" ';

            renderedAttrs = renderedAttrs + attr;
        }
    })

    renderedAttrs = renderedAttrs.trim();
    %>
    <span <%= renderedAttrs %>><%- obj.content %></span>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/macros/direction.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/macros/direction.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/macros/direction.html");
    }
}
