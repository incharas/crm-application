<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/inline-editable-wrapper-view.js */
class __TwigTemplate_937299134b6e443de274a42b441c0d3c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';
    const __ = require('orotranslation/js/translator');
    const BaseView = require('oroui/js/app/views/base/view');

    /**
     * Tags view, able to handle either `collection` of tags or plain array of `items`.
     *
     * @class
     */
    const InlineEditorWrapperView = BaseView.extend({
        template: require('tpl-loader!oroform/templates/inline-editable-wrapper-view.html'),

        events: {
            'dblclick': 'onInlineEditingStart',
            'click [data-role=\"start-editing\"]': 'onInlineEditingStart'
        },

        /**
         * @inheritdoc
         */
        constructor: function InlineEditorWrapperView(options) {
            InlineEditorWrapperView.__super__.constructor.call(this, options);
        },

        setElement: function(element) {
            element.addClass('inline-editable-wrapper');
            element.attr('title', __('oro.form.inlineEditing.helpMessage'));
            return InlineEditorWrapperView.__super__.setElement.call(this, element);
        },

        onInlineEditingStart: function() {
            this.trigger('start-editing');
        },

        getContainer: function() {
            return this.\$('[data-role=\"container\"]');
        }
    });

    return InlineEditorWrapperView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/inline-editable-wrapper-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/inline-editable-wrapper-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/inline-editable-wrapper-view.js");
    }
}
