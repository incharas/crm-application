<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChart/Chart/stackedbar.html.twig */
class __TwigTemplate_b5d71bfa51b87e386e1c5989034be95d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["chart"] = $this->macros["chart"] = $this->loadTemplate("@OroChart/macros.html.twig", "@OroChart/Chart/stackedbar.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 22
        echo "
";
        // line 23
        $context["lableTrans"] = ["data_schema" => ["label" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 26
($context["options"] ?? null), "data_schema", [], "any", false, true, false, 26), "label", [], "any", false, true, false, 26), "label", [], "any", true, true, false, 26)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, true, false, 26), "label", [], "any", false, true, false, 26), "label", [], "any", false, false, false, 26), "N/A")) : ("N/A")))], "value" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 29
($context["options"] ?? null), "data_schema", [], "any", false, true, false, 29), "value", [], "any", false, true, false, 29), "label", [], "any", true, true, false, 29)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, true, false, 29), "value", [], "any", false, true, false, 29), "label", [], "any", false, false, false, 29), "N/A")) : ("N/A")))]]];
        // line 33
        echo "
";
        // line 34
        $context["options"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(($context["options"] ?? null), ($context["lableTrans"] ?? null));
        // line 35
        if ((twig_length_filter($this->env, ($context["data"] ?? null)) > 0)) {
            // line 36
            echo "    <div class=\"stackedbar-chart\">
        ";
            // line 37
            echo twig_call_macro($macros["chart"], "macro_renderChart", [($context["data"] ?? null), ($context["options"] ?? null), ($context["config"] ?? null)], 37, $context, $this->getSourceContext());
            echo "
        <p class=\"chart-hint\">";
            // line 38
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.chart.stackedbar_chart.hint"), "html", null, true);
            echo " </p>
    </div>
";
        } else {
            // line 41
            echo "    <div class=\"no-data\">
        ";
            // line 42
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.no_data_found"), "html", null, true);
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroChart/Chart/stackedbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 42,  69 => 41,  63 => 38,  59 => 37,  56 => 36,  54 => 35,  52 => 34,  49 => 33,  47 => 29,  46 => 26,  45 => 23,  42 => 22,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChart/Chart/stackedbar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ChartBundle/Resources/views/Chart/stackedbar.html.twig");
    }
}
