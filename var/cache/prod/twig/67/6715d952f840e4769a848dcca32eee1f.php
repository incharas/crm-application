<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/actions/update.html.twig */
class __TwigTemplate_e9e64b239d91dbecd0275fea97d4cc9e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'before_content_addition' => [$this, 'block_before_content_addition'],
            'content' => [$this, 'block_content'],
            'widget_context' => [$this, 'block_widget_context'],
            'page_widget_actions' => [$this, 'block_page_widget_actions'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageActions' => [$this, 'block_pageActions'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'pageTitleIcon' => [$this, 'block_pageTitleIcon'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'breadcrumbMessage' => [$this, 'block_breadcrumbMessage'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
            'sync_content_tags' => [$this, 'block_sync_content_tags'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 2
        return $this->loadTemplate(((($context["isWidgetContext"] ?? null)) ? ("@OroForm/Layout/widgetForm.html.twig") : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 2))), "@OroUI/actions/update.html.twig", 2);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["isWidgetContext"] = ((array_key_exists("isWidgetContext", $context)) ? (($context["isWidgetContext"] ?? null)) : (false));
        // line 4
        $macros["syncMacro"] = $this->macros["syncMacro"] = $this->loadTemplate("@OroSync/Include/contentTags.html.twig", "@OroUI/actions/update.html.twig", 4)->unwrap();
        // line 5
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/actions/update.html.twig", 5)->unwrap();
        // line 7
        $context["entity"] = ((array_key_exists("entity", $context)) ? (($context["entity"] ?? null)) : (null));
        // line 2
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_before_content_addition($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_before_content_addition", $context)) ? (_twig_default_filter(($context["update_before_content_addition"] ?? null), "update_before_content_addition")) : ("update_before_content_addition")), ["entity" => ($context["entity"] ?? null)]);
    }

    // line 13
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/actions/update.html.twig", 14)->unwrap();
        // line 15
        echo "
    ";
        // line 16
        if (((($context["isWidgetContext"] ?? null) && array_key_exists("savedId", $context)) && ($context["savedId"] ?? null))) {
            // line 17
            echo "        ";
            $this->displayBlock('widget_context', $context, $blocks);
            // line 26
            echo "    ";
        } else {
            // line 27
            echo "    ";
            $context["formAction"] = ((array_key_exists("formAction", $context)) ? (_twig_default_filter(($context["formAction"] ?? null))) : (""));
            // line 28
            echo "    ";
            if (( !array_key_exists("addQueryParameters", $context) || (($context["addQueryParameters"] ?? null) == true))) {
                // line 29
                echo "        ";
                $context["formAction"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->addUrlQuery(($context["formAction"] ?? null));
                // line 30
                echo "    ";
            }
            // line 31
            echo "    ";
            $context["formAttr"] = twig_array_merge(((array_key_exists("formAttr", $context)) ? (_twig_default_filter(($context["formAttr"] ?? null), [])) : ([])), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 32
($context["form"] ?? null), "vars", [], "any", false, false, false, 32), "id", [], "any", false, false, false, 32), "data-collect" => "true"]);
            // line 35
            echo "
    ";
            // line 36
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["formAttr"] ?? null));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 37
                echo "        ";
                if (twig_test_iterable($context["value"])) {
                    // line 38
                    echo "            ";
                    $context["formAttr"] = twig_array_merge(($context["formAttr"] ?? null), [$context["key"] => json_encode($context["value"])]);
                    // line 39
                    echo "        ";
                }
                // line 40
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 41
            echo "
    ";
            // line 42
            if (array_key_exists("pageComponent", $context)) {
                // line 43
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["pageComponent"] ?? null));
                foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                    // line 44
                    echo "            ";
                    if (($context["key"] == "layout")) {
                        // line 45
                        echo "                ";
                        $context["formAttr"] = twig_array_merge(($context["formAttr"] ?? null), ["data-layout" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pageComponent"] ?? null), "layout", [], "any", false, false, false, 45)]);
                        // line 46
                        echo "            ";
                    } elseif (twig_test_iterable($context["value"])) {
                        // line 47
                        echo "                ";
                        $context["formAttr"] = twig_array_merge(($context["formAttr"] ?? null), [("data-page-component-" . $context["key"]) => json_encode($context["value"])]);
                        // line 48
                        echo "            ";
                    } else {
                        // line 49
                        echo "                ";
                        $context["formAttr"] = twig_array_merge(($context["formAttr"] ?? null), [("data-page-component-" . $context["key"]) => $context["value"]]);
                        // line 50
                        echo "            ";
                    }
                    // line 51
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 52
                echo "    ";
            }
            // line 53
            echo "    ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_start', ["action" => ($context["formAction"] ?? null), "attr" => ($context["formAttr"] ?? null)]);
            echo "
        ";
            // line 54
            if (($context["isWidgetContext"] ?? null)) {
                // line 55
                echo "            ";
                $this->displayBlock('page_widget_actions', $context, $blocks);
                // line 61
                echo "        ";
            } else {
                // line 62
                echo "        <div class=\"container-fluid page-title\">
            ";
                // line 63
                ob_start(function () { return ''; });
                // line 64
                echo "                ";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_navButtons_before", $context)) ? (_twig_default_filter(($context["update_navButtons_before"] ?? null), "update_navButtons_before")) : ("update_navButtons_before")), ["entity" => ($context["entity"] ?? null)]);
                // line 65
                echo "                ";
                $this->displayBlock('navButtons', $context, $blocks);
                // line 66
                echo "                ";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_navButtons_after", $context)) ? (_twig_default_filter(($context["update_navButtons_after"] ?? null), "update_navButtons_after")) : ("update_navButtons_after")), ["entity" => ($context["entity"] ?? null)]);
                // line 67
                echo "                ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 67), "method", [], "any", false, false, false, 67) != "GET")) {
                    // line 68
                    echo "                    ";
                    $context["inputAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 68), "default_input_action", [], "any", true, true, false, 68)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 69
($context["form"] ?? null), "vars", [], "any", false, false, false, 69), "default_input_action", [], "any", false, false, false, 69)) : (""));
                    // line 72
                    echo "                    <input
                            type=\"hidden\"
                            name=\"input_action\"
                            value=\"";
                    // line 75
                    echo twig_escape_filter($this->env, ($context["inputAction"] ?? null), "html", null, true);
                    echo "\"
                            data-form-id=\"";
                    // line 76
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 76), "id", [], "any", false, false, false, 76), "html", null, true);
                    echo "\"
                    >
                ";
                }
                // line 79
                echo "            ";
                $context["titleButtonsBlock"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 80
                echo "
            ";
                // line 81
                ob_start(function () { return ''; });
                // line 82
                echo "                ";
                $this->displayBlock('pageActions', $context, $blocks);
                // line 99
                echo "            ";
                $context["pageActionsBlock"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 100
                echo "
            <div class=\"navigation navbar-extra navbar-extra-right\">
                ";
                // line 102
                $this->displayBlock('pageHeader', $context, $blocks);
                // line 174
                echo "            </div>
        </div>
        ";
            }
            // line 177
            echo "        <div class=\"layout-content\">
            ";
            // line 178
            $this->displayBlock('content_data', $context, $blocks);
            // line 252
            echo "        </div>
        ";
            // line 253
            $this->displayBlock('sync_content_tags', $context, $blocks);
            // line 257
            echo "    ";
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end', ["render_rest" => false]);
            echo "
    ";
            // line 258
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
    ";
        }
    }

    // line 17
    public function block_widget_context($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "            <div ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/widget-form-component", "options" => ["_wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 21
($context["app"] ?? null), "request", [], "any", false, false, false, 21), "get", [0 => "_wid"], "method", false, false, false, 21), "data" => ((        // line 22
array_key_exists("savedId", $context)) ? (_twig_default_filter(($context["savedId"] ?? null), null)) : (null))]]], 18, $context, $this->getSourceContext());
        // line 24
        echo "></div>
        ";
    }

    // line 55
    public function block_page_widget_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 56
        echo "            <div class=\"widget-actions\">
                <button type=\"reset\" class=\"btn\">";
        // line 57
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
                <button type=\"submit\" class=\"btn btn-success\">";
        // line 58
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "html", null, true);
        echo "</button>
            </div>
            ";
    }

    // line 65
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_navButtons", $context)) ? (_twig_default_filter(($context["update_navButtons"] ?? null), "update_navButtons")) : ("update_navButtons")), ["entity" => ($context["entity"] ?? null)]);
    }

    // line 82
    public function block_pageActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 83
        echo "                    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_pageActions_before", $context)) ? (_twig_default_filter(($context["update_pageActions_before"] ?? null), "update_pageActions_before")) : ("update_pageActions_before")), ["entity" => ($context["entity"] ?? null)]);
        // line 84
        echo "
                    ";
        // line 85
        $context["audit_entity_id"] = ((array_key_exists("audit_entity_id", $context)) ? (($context["audit_entity_id"] ?? null)) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 85), "value", [], "any", false, true, false, 85), "id", [], "any", true, true, false, 85)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 85), "value", [], "any", false, true, false, 85), "id", [], "any", false, false, false, 85))) : (""))));
        // line 86
        echo "                    ";
        if (($context["audit_entity_id"] ?? null)) {
            // line 87
            echo "                        ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("change_history_block", $context)) ? (_twig_default_filter(($context["change_history_block"] ?? null), "change_history_block")) : ("change_history_block")), ["entity" => ((            // line 88
array_key_exists("entity", $context)) ? (_twig_default_filter(($context["entity"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 88), "value", [], "any", false, false, false, 88))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 88), "value", [], "any", false, false, false, 88))), "entity_class" => ((            // line 89
array_key_exists("audit_entity_class", $context)) ? (_twig_default_filter(($context["audit_entity_class"] ?? null), null)) : (null)), "id" =>             // line 90
($context["audit_entity_id"] ?? null), "title" => ((            // line 91
array_key_exists("audit_title", $context)) ? (_twig_default_filter(($context["audit_title"] ?? null), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 91), "value", [], "any", false, true, false, 91), "__toString", [], "any", true, true, false, 91)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 91), "value", [], "any", false, false, false, 91), "__toString", [], "any", false, false, false, 91)) : (null)))) : (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, true, false, 91), "value", [], "any", false, true, false, 91), "__toString", [], "any", true, true, false, 91)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 91), "value", [], "any", false, false, false, 91), "__toString", [], "any", false, false, false, 91)) : (null)))), "audit_path" => ((            // line 92
array_key_exists("audit_path", $context)) ? (_twig_default_filter(($context["audit_path"] ?? null), "oro_dataaudit_history")) : ("oro_dataaudit_history")), "audit_show_change_history" => ((            // line 93
array_key_exists("audit_show_change_history", $context)) ? (_twig_default_filter(($context["audit_show_change_history"] ?? null), false)) : (false))]);
            // line 95
            echo "                    ";
        }
        // line 96
        echo "
                    ";
        // line 97
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_pageActions_after", $context)) ? (_twig_default_filter(($context["update_pageActions_after"] ?? null), "update_pageActions_after")) : ("update_pageActions_after")), ["entity" => ($context["entity"] ?? null)]);
        // line 98
        echo "                ";
    }

    // line 102
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 103
        echo "                    <div class=\"row\">
                        <div class=\"pull-left pull-left-extra\">
                            ";
        // line 105
        $this->displayBlock('pageTitleIcon', $context, $blocks);
        // line 106
        echo "
                            <div class=\"page-title__path\">
                                <div class=\"top-row\">
                                    ";
        // line 109
        $this->displayBlock('breadcrumbs', $context, $blocks);
        // line 141
        echo "                                </div>
                            </div>
                        </div>
                        <div class=\"pull-right title-buttons-container\">
                            ";
        // line 145
        echo twig_escape_filter($this->env, ($context["titleButtonsBlock"] ?? null), "html", null, true);
        echo "
                        </div>
                    </div>
                    <div class=\"row inline-info\">
                        <div class=\"pull-left-extra\">
                            <div class=\"clearfix\">
                                <ul class=\"inline\">
                                    ";
        // line 152
        $this->displayBlock('stats', $context, $blocks);
        // line 162
        echo "                                </ul>
                            </div>
                        </div>
                        <div class=\"pull-right page-title__entity-info-state\">
                            <div class=\"inline-decorate-container\">
                                <ul class=\"inline-decorate\">
                                    ";
        // line 168
        echo twig_escape_filter($this->env, ($context["pageActionsBlock"] ?? null), "html", null, true);
        echo "
                                </ul>
                            </div>
                        </div>
                    </div>
                ";
    }

    // line 105
    public function block_pageTitleIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 109
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 110
        echo "                                        ";
        if (array_key_exists("breadcrumbs", $context)) {
            // line 111
            echo "                                            <div class=\"page-title__entity-title-wrapper\">
                                                <div class=\"sub-title\">";
            // line 113
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexPath", [], "any", true, true, false, 113)) {
                // line 114
                echo "<a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->addUrlQuery(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexPath", [], "any", false, false, false, 114)), "html", null, true);
                echo "\">";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", false, false, false, 114), "html", null, true);
                echo "</a>";
            } else {
                // line 116
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "indexLabel", [], "any", false, false, false, 116), "html", null, true);
            }
            // line 118
            echo "</div>
                                                <span class=\"separator\">/</span>
                                                ";
            // line 120
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "additional", [], "any", true, true, false, 120)) {
                // line 121
                echo "                                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "additional", [], "any", false, false, false, 121));
                foreach ($context['_seq'] as $context["_key"] => $context["breadcrumb"]) {
                    // line 122
                    echo "                                                        <div class=\"sub-title\">";
                    // line 123
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexPath", [], "any", true, true, false, 123)) {
                        // line 124
                        echo "<a href=\"";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexPath", [], "any", false, false, false, 124), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexLabel", [], "any", false, false, false, 124), "html", null, true);
                        echo "</a>";
                    } else {
                        // line 126
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["breadcrumb"], "indexLabel", [], "any", false, false, false, 126), "html", null, true);
                    }
                    // line 128
                    echo "</div>
                                                        <span class=\"separator\">/</span>
                                                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['breadcrumb'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 131
                echo "                                                ";
            }
            // line 132
            echo "                                                ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "rawEntityTitle", [], "any", true, true, false, 132) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "rawEntityTitle", [], "any", false, false, false, 132))) {
                // line 133
                echo "                                                    <h1 class=\"page-title__entity-title\">";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entityTitle", [], "any", false, false, false, 133);
                echo "</h1>
                                                ";
            } else {
                // line 135
                echo "                                                    <h1 class=\"page-title__entity-title\">";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entityTitle", [], "any", false, false, false, 135), "html", null, true);
                echo "</h1>
                                                ";
            }
            // line 137
            echo "                                            </div>
                                        ";
        }
        // line 139
        echo "                                        ";
        $this->displayBlock('breadcrumbMessage', $context, $blocks);
        // line 140
        echo "                                    ";
    }

    // line 139
    public function block_breadcrumbMessage($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 152
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 153
        echo "                                        ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, true, false, 153), "createdAt", [], "any", true, true, false, 153) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, true, false, 153), "updatedAt", [], "any", true, true, false, 153))) {
            // line 154
            echo "                                            ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 154), "createdAt")) {
                // line 155
                echo "                                                <li>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
                echo ": ";
                echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 155), "createdAt", [], "any", false, false, false, 155)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 155), "createdAt", [], "any", false, false, false, 155))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))), "html", null, true);
                echo "</li>
                                            ";
            }
            // line 157
            echo "                                            ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("VIEW", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 157), "updatedAt")) {
                // line 158
                echo "                                                <li>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
                echo ": ";
                echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 158), "updatedAt", [], "any", false, false, false, 158)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["breadcrumbs"] ?? null), "entity", [], "any", false, false, false, 158), "updatedAt", [], "any", false, false, false, 158))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.empty"))), "html", null, true);
                echo "</li>
                                            ";
            }
            // line 160
            echo "                                        ";
        }
        // line 161
        echo "                                    ";
    }

    // line 178
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 179
        echo "                ";
        $context["data"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->processForm($this->env, ($context["data"] ?? null), ($context["form"] ?? null), ($context["entity"] ?? null));
        // line 180
        echo "
                ";
        // line 181
        if (((($context["entity"] ?? null) && array_key_exists("data", $context)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "dataBlocks", [], "any", true, true, false, 181))) {
            // line 182
            echo "                    ";
            $context["dataBlocks"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "dataBlocks", [], "any", false, false, false, 182);
            // line 184
            ob_start(function () { return ''; });
            // line 185
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_content_data_activities", $context)) ? (_twig_default_filter(($context["update_content_data_activities"] ?? null), "update_content_data_activities")) : ("update_content_data_activities")), ["entity" => ($context["entity"] ?? null)]);
            $context["activitiesData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 188
            if ( !twig_test_empty(($context["activitiesData"] ?? null))) {
                // line 189
                echo "                        ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.sections.activities"), "subblocks" => [0 => ["spanClass" => "empty", "data" => [0 =>                 // line 194
($context["activitiesData"] ?? null)]]]]]);
                // line 198
                echo "                    ";
            }
            // line 200
            ob_start(function () { return ''; });
            // line 201
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_content_data_communications", $context)) ? (_twig_default_filter(($context["update_content_data_communications"] ?? null), "update_content_data_communications")) : ("update_content_data_communications")), ["entity" => ($context["entity"] ?? null)]);
            $context["communicationsData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 204
            if ( !twig_test_empty(($context["communicationsData"] ?? null))) {
                // line 205
                echo "                        ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Communications"), "subblocks" => [0 => ["spanClass" => "empty", "data" => [0 =>                 // line 210
($context["communicationsData"] ?? null)]]]]]);
                // line 214
                echo "                    ";
            }
            // line 216
            ob_start(function () { return ''; });
            // line 217
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_content_data_additional_information", $context)) ? (_twig_default_filter(($context["update_content_data_additional_information"] ?? null), "update_content_data_additional_information")) : ("update_content_data_additional_information")), ["entity" => ($context["entity"] ?? null)]);
            $context["additionalInformationData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 220
            if ( !twig_test_empty(($context["additionalInformationData"] ?? null))) {
                // line 221
                echo "                        ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional Information"), "subblocks" => [0 => ["spanClass" => "empty", "data" => [0 =>                 // line 226
($context["additionalInformationData"] ?? null)]]]]]);
                // line 230
                echo "                    ";
            }
            // line 232
            ob_start(function () { return ''; });
            // line 233
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("update_content_data_comments", $context)) ? (_twig_default_filter(($context["update_content_data_comments"] ?? null), "update_content_data_comments")) : ("update_content_data_comments")), ["entity" => ($context["entity"] ?? null)]);
            $context["commentsData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 236
            if ( !twig_test_empty(($context["commentsData"] ?? null))) {
                // line 237
                echo "                        ";
                $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.comment.entity_plural_label"), "subblocks" => [0 => ["spanClass" => "responsive-cell activity-list-widget", "data" => [0 =>                 // line 242
($context["commentsData"] ?? null)]]]]]);
                // line 246
                echo "                    ";
            }
            // line 247
            echo "
                    ";
            // line 248
            $context["data"] = twig_array_merge(($context["data"] ?? null), ["dataBlocks" => ($context["dataBlocks"] ?? null)]);
            // line 249
            echo "                ";
        }
        // line 250
        echo "                ";
        echo twig_call_macro($macros["UI"], "macro_scrollData", [($context["id"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null), ($context["form"] ?? null)], 250, $context, $this->getSourceContext());
        echo "
            ";
    }

    // line 253
    public function block_sync_content_tags($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 254
        echo "            ";
        // line 255
        echo "            ";
        echo twig_call_macro($macros["syncMacro"], "macro_syncContentTags", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 255), "value", [], "any", false, false, false, 255)], 255, $context, $this->getSourceContext());
        echo "
        ";
    }

    public function getTemplateName()
    {
        return "@OroUI/actions/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  619 => 255,  617 => 254,  613 => 253,  606 => 250,  603 => 249,  601 => 248,  598 => 247,  595 => 246,  593 => 242,  591 => 237,  589 => 236,  586 => 233,  584 => 232,  581 => 230,  579 => 226,  577 => 221,  575 => 220,  572 => 217,  570 => 216,  567 => 214,  565 => 210,  563 => 205,  561 => 204,  558 => 201,  556 => 200,  553 => 198,  551 => 194,  549 => 189,  547 => 188,  544 => 185,  542 => 184,  539 => 182,  537 => 181,  534 => 180,  531 => 179,  527 => 178,  523 => 161,  520 => 160,  512 => 158,  509 => 157,  501 => 155,  498 => 154,  495 => 153,  491 => 152,  485 => 139,  481 => 140,  478 => 139,  474 => 137,  468 => 135,  462 => 133,  459 => 132,  456 => 131,  448 => 128,  445 => 126,  438 => 124,  436 => 123,  434 => 122,  429 => 121,  427 => 120,  423 => 118,  420 => 116,  413 => 114,  411 => 113,  408 => 111,  405 => 110,  401 => 109,  395 => 105,  385 => 168,  377 => 162,  375 => 152,  365 => 145,  359 => 141,  357 => 109,  352 => 106,  350 => 105,  346 => 103,  342 => 102,  338 => 98,  336 => 97,  333 => 96,  330 => 95,  328 => 93,  327 => 92,  326 => 91,  325 => 90,  324 => 89,  323 => 88,  321 => 87,  318 => 86,  316 => 85,  313 => 84,  310 => 83,  306 => 82,  299 => 65,  292 => 58,  288 => 57,  285 => 56,  281 => 55,  276 => 24,  274 => 22,  273 => 21,  271 => 18,  267 => 17,  260 => 258,  255 => 257,  253 => 253,  250 => 252,  248 => 178,  245 => 177,  240 => 174,  238 => 102,  234 => 100,  231 => 99,  228 => 82,  226 => 81,  223 => 80,  220 => 79,  214 => 76,  210 => 75,  205 => 72,  203 => 69,  201 => 68,  198 => 67,  195 => 66,  192 => 65,  189 => 64,  187 => 63,  184 => 62,  181 => 61,  178 => 55,  176 => 54,  171 => 53,  168 => 52,  162 => 51,  159 => 50,  156 => 49,  153 => 48,  150 => 47,  147 => 46,  144 => 45,  141 => 44,  136 => 43,  134 => 42,  131 => 41,  125 => 40,  122 => 39,  119 => 38,  116 => 37,  112 => 36,  109 => 35,  107 => 32,  105 => 31,  102 => 30,  99 => 29,  96 => 28,  93 => 27,  90 => 26,  87 => 17,  85 => 16,  82 => 15,  79 => 14,  75 => 13,  70 => 10,  66 => 9,  62 => 2,  60 => 7,  58 => 5,  56 => 4,  54 => 1,  47 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/actions/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/actions/update.html.twig");
    }
}
