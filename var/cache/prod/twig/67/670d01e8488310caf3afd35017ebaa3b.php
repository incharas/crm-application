<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/loading/loading-mask.scss */
class __TwigTemplate_35f8266db2d373fb11abdad50c354cac extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:meta';

\$loading-mask: 'loader-mask';

@if (meta.variable-exists('loading-mask-prefix')) {
    \$loading-mask: \$loading-mask-prefix + \$loading-mask;
}

.view-loading {
    height: \$loading-view-height;
}

.#{\$loading-mask} {
    position: absolute;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index: \$loading-mask-z-index;

    display: none;

    overflow: hidden;

    background-color: var(--loading-mask-background-color, rgba(\$loading-mask-background-color, .5));

    .loader-frame {
        @include loader();
    }

    .loader-content {
        display: none;
    }
}

.loading {
    position: relative;

    min-height: \$loading-mask-icon-size + 6px;

    @at-root body#{&} {
        height: 100vh;
        overflow: hidden;
    }

    > .#{\$loading-mask}.shown {
        // show only first level loaders
        display: flex;
        flex-direction: column;
        align-items: center;
        justify-content: center;
    }

    /**
        1. hide all nested loaders
        2. hide rest of first level loaders, except first one
     */
    .loading .#{\$loading-mask}.shown,
    > .#{\$loading-mask}.shown ~ .#{\$loading-mask}.shown {
        display: none;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/loading/loading-mask.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/loading/loading-mask.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/loading/loading-mask.scss");
    }
}
