<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroIntegration/Integration/Datagrid/type.html.twig */
class __TwigTemplate_292832c0f248b54e2bb6b4e222c180ec extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["types"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["registry"] ?? null), "getRegisteredChannelTypes", [], "method", false, false, false, 1);
        // line 2
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["choices"] ?? null));
        foreach ($context['_seq'] as $context["choiceLabel"] => $context["choiceValue"]) {
            // line 3
            echo "    ";
            if (($context["choiceValue"] == ($context["value"] ?? null))) {
                // line 4
                echo "        ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["types"] ?? null), ($context["value"] ?? null), [], "array", true, true, false, 4) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["types"] ?? null), ($context["value"] ?? null), [], "array", false, true, false, 4), "icon", [], "any", true, true, false, 4))) {
                    // line 5
                    echo "            <span class=\"integration-icon\" style=\"background: url(";
                    echo twig_escape_filter($this->env, json_encode($this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = ($context["types"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[($context["value"] ?? null)] ?? null) : null), "icon", [], "any", false, false, false, 5))), "html", null, true);
                    echo ") 0 0 no-repeat\" ></span>
        ";
                }
                // line 7
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["choiceLabel"]), "html", null, true);
                echo "
    ";
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['choiceLabel'], $context['choiceValue'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
    }

    public function getTemplateName()
    {
        return "@OroIntegration/Integration/Datagrid/type.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 7,  49 => 5,  46 => 4,  43 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroIntegration/Integration/Datagrid/type.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Resources/views/Integration/Datagrid/type.html.twig");
    }
}
