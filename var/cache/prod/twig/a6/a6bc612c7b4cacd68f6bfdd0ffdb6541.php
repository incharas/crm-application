<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/layout.html.twig */
class __TwigTemplate_984e38756ec24fd101165fa3f80aae4f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'head' => [$this, 'block_head'],
            'title' => [$this, 'block_title'],
            'bodyClass' => [$this, 'block_bodyClass'],
            'messages' => [$this, 'block_messages'],
            'header' => [$this, 'block_header'],
            'main' => [$this, 'block_main'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<!DOCTYPE html>
<html lang=\"";
        // line 2
        echo twig_escape_filter($this->env, twig_slice($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getLanguage(), 0, 2), "html", null, true);
        echo "\"";
        echo (($this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->isRtlMode()) ? (" dir=\"rtl\"") : (""));
        echo "
      class=\"";
        // line 3
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            echo "mobile";
        } else {
            echo "desktop";
        }
        echo "-version\">
<head>
    ";
        // line 5
        $this->displayBlock('head', $context, $blocks);
        // line 14
        echo "</head>
<body class=\"";
        // line 15
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
            echo "mobile";
        } else {
            echo "desktop";
        }
        echo "-version ";
        $this->displayBlock('bodyClass', $context, $blocks);
        echo "\">
    ";
        // line 16
        $this->loadTemplate("@OroUI/Default/noscript.html.twig", "@OroUser/layout.html.twig", 16)->display($context);
        // line 17
        echo "    <div id=\"page\" class=\"app-page\">
        <div id=\"central-panel\" class=\"app-page__central-panel\">
            ";
        // line 19
        ob_start(function () { return ''; });
        // line 20
        echo "            ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 20), "flashbag", [], "any", false, false, false, 20), "peekAll", [], "any", false, false, false, 20)) > 0)) {
            // line 21
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "session", [], "any", false, false, false, 21), "flashbag", [], "any", false, false, false, 21), "all", [], "any", false, false, false, 21));
            foreach ($context['_seq'] as $context["type"] => $context["messages"]) {
                // line 22
                echo "                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["messages"]);
                foreach ($context['_seq'] as $context["_key"] => $context["message"]) {
                    // line 23
                    echo "                    <div class=\"alert";
                    (($context["type"]) ? (print (twig_escape_filter($this->env, (" alert-" . $context["type"]), "html", null, true))) : (print ("")));
                    echo "\" role=\"alert\">
                    ";
                    // line 24
                    echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["message"]));
                    echo "
                    </div>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['message'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 27
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['type'], $context['messages'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 28
            echo "            ";
        }
        // line 29
        echo "            ";
        $context["messagesContent"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 30
        echo "
            ";
        // line 31
        $this->displayBlock('messages', $context, $blocks);
        // line 34
        echo "
            ";
        // line 35
        $this->displayBlock('header', $context, $blocks);
        // line 37
        echo "
            ";
        // line 38
        $this->displayBlock('main', $context, $blocks);
        // line 42
        echo "        </div>
    </div>
</body>
</html>
";
    }

    // line 5
    public function block_head($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
    <meta name=\"viewport\" content=\"width=device-width, height=device-height, initial-scale=1.0, user-scalable=no\">
    ";
        // line 8
        if ($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeIcon()) {
            // line 9
            echo "        <link rel=\"shortcut icon\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl($this->extensions['Oro\Bundle\ThemeBundle\Twig\ThemeExtension']->getThemeIcon()), "html", null, true);
            echo "\">
    ";
        }
        // line 11
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("head_style", $context)) ? (_twig_default_filter(($context["head_style"] ?? null), "head_style")) : ("head_style")), array());
        // line 12
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("head_script", $context)) ? (_twig_default_filter(($context["head_script"] ?? null), "head_script")) : ("head_script")), array());
        // line 13
        echo "    ";
    }

    // line 6
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\NavigationBundle\Twig\TitleExtension']->render(), "html", null, true);
    }

    // line 15
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 31
    public function block_messages($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 32
        echo "                ";
        echo twig_escape_filter($this->env, ($context["messagesContent"] ?? null), "html", null, true);
        echo "
            ";
    }

    // line 35
    public function block_header($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "            ";
    }

    // line 38
    public function block_main($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 39
        echo "                ";
        $this->displayBlock('content', $context, $blocks);
        // line 41
        echo "            ";
    }

    // line 39
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 40
        echo "                ";
    }

    public function getTemplateName()
    {
        return "@OroUser/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  222 => 40,  218 => 39,  214 => 41,  211 => 39,  207 => 38,  203 => 36,  199 => 35,  192 => 32,  188 => 31,  182 => 15,  175 => 6,  171 => 13,  168 => 12,  165 => 11,  159 => 9,  157 => 8,  151 => 6,  147 => 5,  139 => 42,  137 => 38,  134 => 37,  132 => 35,  129 => 34,  127 => 31,  124 => 30,  121 => 29,  118 => 28,  112 => 27,  103 => 24,  98 => 23,  93 => 22,  88 => 21,  85 => 20,  83 => 19,  79 => 17,  77 => 16,  67 => 15,  64 => 14,  62 => 5,  53 => 3,  47 => 2,  44 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/layout.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/layout.html.twig");
    }
}
