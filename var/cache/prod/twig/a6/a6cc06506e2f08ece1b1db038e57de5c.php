<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/components/ajax-button.js */
class __TwigTemplate_6916baa863dc967e64a25ef6822bf676 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');
    const mediator = require('oroui/js/mediator');
    const options = {};

    function onClick(e) {
        e.preventDefault();

        const method = \$(e.currentTarget).data('method');
        const url = \$(e.currentTarget).data('url');
        const redirect = \$(e.currentTarget).data('redirect');
        const successMessage = \$(e.currentTarget).data('success-message');
        const errorMessage = \$(e.currentTarget).data('error-message');

        mediator.execute('showLoading');

        \$.ajax({
            url: url,
            type: method,
            success: function(data) {
                mediator.execute(
                    'showFlashMessage',
                    'success',
                    data && data.message ? data.message : __(successMessage)
                );
                mediator.execute('redirectTo', {url: redirect}, {redirect: true});
            },
            errorHandlerMessage: function(event, response) {
                const responseText = \$.parseJSON(response.responseText);
                return responseText.message ? responseText.message : __(errorMessage);
            },
            complete: function() {
                mediator.execute('hideLoading');
            }
        });
    }

    return function(additionalOptions) {
        _.extend(options, additionalOptions || {});
        const button = options._sourceElement;
        button.click(onClick);
    };
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/components/ajax-button.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/components/ajax-button.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/components/ajax-button.js");
    }
}
