<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/multiple-entity/collection.js */
class __TwigTemplate_71cceaedcb64234dedd8128c6783724c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'underscore', 'backbone', './model'
], function(_, Backbone, EntityModel) {
    'use strict';

    /**
     * @export  oroform/js/multiple-entity/collection
     * @class   oroform.MultipleEntity.Collection
     * @extends Backbone.Collection
     */
    const multipleEntityCollection = Backbone.Collection.extend({
        model: EntityModel,

        /**
         * @inheritdoc
         */
        constructor: function multipleEntityCollection(...args) {
            multipleEntityCollection.__super__.constructor.apply(this, args);
        },

        /**
         * @inheritdoc
         */
        initialize: function() {
            this.on('change:isDefault', this.onIsDefaultChange, this);
        },

        onIsDefaultChange: function(item) {
            // Only 1 item allowed to be default
            if (item.get('isDefault')) {
                const defaultItems = this.where({isDefault: true});
                _.each(defaultItems, function(defaultItem) {
                    if (defaultItem.get('id') !== item.get('id')) {
                        defaultItem.set('isDefault', false);
                    }
                });
                this.trigger('defaultChange', item);
            }
        }
    });

    return multipleEntityCollection;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/multiple-entity/collection.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/multiple-entity/collection.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/multiple-entity/collection.js");
    }
}
