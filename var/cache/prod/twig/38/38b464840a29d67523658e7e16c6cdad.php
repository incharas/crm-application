<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Call/widget/calls.html.twig */
class __TwigTemplate_a7b421d7d84f3e8863a7aa73add8a5e0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroCall/Call/widget/calls.html.twig", 1)->unwrap();
        // line 2
        echo "
<div class=\"widget-content\">
    ";
        // line 4
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["widget-calls-grid", ($context["datagridParameters"] ?? null)], 4, $context, $this->getSourceContext());
        echo "
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroCall/Call/widget/calls.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Call/widget/calls.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Call/widget/calls.html.twig");
    }
}
