<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/variables/inline-editing-variables.scss */
class __TwigTemplate_833df87099306ef95ea45129c2e7de0d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$inline-editor-actions-inner-offset: \$input-border-width !default;

\$inline-editor-action-item-color: \$primary-200 !default;
\$inline-editor-action-item-color-hover: \$additional-dark !default;
// closest to #68686a 0.5 opacity
\$inline-editor-action-item-color-disabled: \$primary-700 !default;

\$inline-editor-action-item-fields-last-offset: 66px !default;
\$inline-editor-inner-offset: -\$inline-editor-action-item-fields-last-offset !default;

\$inline-editor-wrapper-width: 150px !default;

\$inline-editor-number-editor-width: 100% !default;

\$inline-editor-datetime-editor-width: 225px !default;

\$inline-editor-has-datepicker-width: 105px !default;
\$inline-editor-has-datepicker-offset: 9px !default;

\$inline-editor-has-timepicker-input-width: 144px !default;

\$inline-editor-select-editor-width: 180px !default;

\$inline-editor-select2-choice-width: calc(100% - 52px) !default;

\$inline-editor-select2-divider-color: \$primary-750 !default;

\$inline-editor-multi-select2-choices-width: 130px !default;

\$inline-editor-select2-drop-box-shadow: 1px 3px 9px 3px rgba(0 0 0 / 8%) !default;
\$inline-editor-select2-drop-above-box-shadow: 1px -3px 9px 3px rgba(0 0 0 / 8%) !default;

\$inline-editor-select2-border-color: \$primary-inverse !default;
\$inline-editor-select2-box-shadow: 1px 1px 9px 3px rgba(0 0 0 / 8%) !default;
\$inline-editor-select2-drop-placeholder-height: 4px !default;
\$inline-editor-select2-drop-placeholder-color: \$primary-inverse !default;

\$inline-editor-select2-drop-height: 45px !default;

\$inline-editor-text-editor-top: 0 !default;
\$inline-editor-text-editor-bottom: 0 !default;
\$inline-editor-inner-fields-height: 100% !default;
\$inline-editor-inner-textarea-max-height: max(100%, 150px) !default;
\$inline-editor-inner-outer-offset: 68px !default;
\$inline-editor-inner-textarea-offset: 11px \$inline-editor-inner-outer-offset 11px 9px !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/variables/inline-editing-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/variables/inline-editing-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/variables/inline-editing-variables.scss");
    }
}
