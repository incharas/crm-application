<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAccount/Account/widget/contactsInfo.html.twig */
class __TwigTemplate_008ef8177a6a72e1927414e431e84e2c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"widget-content\">
    ";
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroAccount/Account/widget/contactsInfo.html.twig", 2)->unwrap();
        // line 3
        echo "    ";
        $context["gridName"] = "account-contacts-update-grid";
        // line 4
        echo "
    ";
        // line 5
        $context["params"] = ["_parameters" => ["data_in" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["app"] ?? null), "request", [], "any", false, false, false, 7), "get", [0 => "added"], "method", false, false, false, 7)) ? (twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 7), "get", [0 => "added"], "method", false, false, false, 7), ",")) : ([])), "data_not_in" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["app"] ?? null), "request", [], "any", false, false, false, 8), "get", [0 => "removed"], "method", false, false, false, 8)) ? (twig_split_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 8), "get", [0 => "removed"], "method", false, false, false, 8), ",")) : ([]))]];
        // line 11
        echo "
    ";
        // line 12
        if (($context["account"] ?? null)) {
            // line 13
            echo "        ";
            $context["params"] = twig_array_merge(($context["params"] ?? null), ["account" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 14
($context["account"] ?? null), "id", [], "any", false, false, false, 14)]);
            // line 16
            echo "    ";
        }
        // line 17
        echo "
    ";
        // line 18
        $this->displayBlock('content', $context, $blocks);
        // line 23
        echo "
    ";
        // line 24
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroAccount/Account/widget/contactsInfo.html.twig", 24)->unwrap();
        // line 25
        echo "
    <div ";
        // line 26
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroaccount/js/app/components/account-contact-component", "options" => ["wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 29
($context["app"] ?? null), "request", [], "any", false, false, false, 29), "get", [0 => "_wid"], "method", false, false, false, 29), "gridName" =>         // line 30
($context["gridName"] ?? null), "addedVal" => "#appendContacts", "removedVal" => "#removeContacts", "columnName" => "hasContact", "link" => "oro_contact_info"]]], 26, $context, $this->getSourceContext());
        // line 36
        echo "></div>

    <div class=\"widget-actions\">
        <button type=\"reset\" class=\"btn\">";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
        <button type=\"button\" class=\"btn btn-primary\" data-action-name=\"select\">";
        // line 40
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Select"), "html", null, true);
        echo "</button>
    </div>
</div>
";
    }

    // line 18
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "        ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ($context["params"] ?? null)], 19, $context, $this->getSourceContext());
        echo "
        <input type=\"hidden\" name=\"appendContacts\" id=\"appendContacts\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 20), "get", [0 => "added"], "method", false, false, false, 20), "html", null, true);
        echo "\" />
        <input type=\"hidden\" name=\"removeContacts\" id=\"removeContacts\" value=\"";
        // line 21
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 21), "get", [0 => "removed"], "method", false, false, false, 21), "html", null, true);
        echo "\" />
    ";
    }

    public function getTemplateName()
    {
        return "@OroAccount/Account/widget/contactsInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  112 => 21,  108 => 20,  103 => 19,  99 => 18,  91 => 40,  87 => 39,  82 => 36,  80 => 30,  79 => 29,  78 => 26,  75 => 25,  73 => 24,  70 => 23,  68 => 18,  65 => 17,  62 => 16,  60 => 14,  58 => 13,  56 => 12,  53 => 11,  51 => 8,  50 => 7,  49 => 5,  46 => 4,  43 => 3,  41 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAccount/Account/widget/contactsInfo.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AccountBundle/Resources/views/Account/widget/contactsInfo.html.twig");
    }
}
