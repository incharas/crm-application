<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/accordion.scss */
class __TwigTemplate_c0e6e6b0d49624c01dc4ac3df8042e2c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.accordion {
    &-toggle {
        background: \$accordion-toggle-background;
        font-size: \$accordion-toggle-font-size;
        font-weight: \$accordion-toggle-font-weight;
        margin: \$accordion-toggle-offset;
        cursor: pointer;
        display: block;
    }

    &-group {
        &.responsive-section {
            border-radius: \$accordion-group-responsive-section-border-radius;
            margin-bottom: \$accordion-group-responsive-section-offset-bottom;
            border: \$accordion-group-responsive-section-border;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/accordion.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/accordion.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/accordion.scss");
    }
}
