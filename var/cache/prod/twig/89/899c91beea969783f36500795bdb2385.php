<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/history.html */
class __TwigTemplate_5ea5eea6401946bdd05426248112f89b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div>
    <button type=\"button\" class=\"btn btn-default undo-btn\"<% if (!states || index < 1) { %> disabled<% } %>><i class=\"fa-reply\"></i> <%- _.__('Undo') %></button>
    <button type=\"button\" class=\"btn btn-default redo-btn\"<% if (!states || index + 1 >= states.length) { %> disabled<% } %>><i class=\"fa-share\"></i> <%- _.__('Redo') %></button>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/history.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/history.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/history.html");
    }
}
