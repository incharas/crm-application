<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroLocale/Localization/update.html.twig */
class __TwigTemplate_b49ba64823e61304342c71b66b2ab3c1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroLocale/Localization/update.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%name%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["entity"] ?? null), "name", [], "any", true, true, false, 5)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 5), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.locale.localization.entity_label")]]);
        // line 9
        if ( !array_key_exists("formAction", $context)) {
            // line 10
            $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 10)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_locale_localization_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 10)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_locale_localization_create")));
        }
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroLocale/Localization/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 13
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 15
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_locale_localization_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.locale.localization.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 18
($context["entity"] ?? null), "id", [], "any", false, false, false, 18)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.locale.localization.navigation.view", ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 18)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.locale.localization.entity_label")])))];
        // line 20
        echo "
    ";
        // line 21
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 24
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroLocale/Localization/update.html.twig", 25)->unwrap();
        // line 26
        echo "
    ";
        // line 27
        $this->displayParentBlock("navButtons", $context, $blocks);
        echo "

    ";
        // line 29
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_locale_localization_index")], 29, $context, $this->getSourceContext());
        echo "
    ";
        // line 30
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 30) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_locale_localization_update")) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_locale_localization_create"))) {
            // line 31
            echo "        ";
            $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_locale_localization_update", "params" => ["id" => "\$id"]]], 31, $context, $this->getSourceContext());
            // line 35
            echo "        ";
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_locale_localization_view")) {
                // line 36
                echo "            ";
                $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_locale_localization_view", "params" => ["id" => "\$id"]]], 36, $context, $this->getSourceContext()));
                // line 40
                echo "        ";
            }
            // line 41
            echo "
        ";
            // line 42
            echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 42, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 44
        echo "
";
    }

    // line 47
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "    ";
        $context["id"] = "localization-edit";
        // line 49
        echo "
    ";
        // line 50
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.locale.localization.sections.general"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 56
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget')]]]]];
        // line 61
        echo "
    ";
        // line 62
        $context["data"] = ["formErrors" =>         // line 63
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors'), "dataBlocks" =>         // line 64
($context["dataBlocks"] ?? null)];
        // line 66
        echo "
    ";
        // line 67
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroLocale/Localization/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  146 => 67,  143 => 66,  141 => 64,  140 => 63,  139 => 62,  136 => 61,  134 => 56,  133 => 50,  130 => 49,  127 => 48,  123 => 47,  118 => 44,  113 => 42,  110 => 41,  107 => 40,  104 => 36,  101 => 35,  98 => 31,  96 => 30,  92 => 29,  87 => 27,  84 => 26,  81 => 25,  77 => 24,  71 => 21,  68 => 20,  66 => 18,  65 => 15,  63 => 14,  59 => 13,  54 => 1,  51 => 10,  49 => 9,  47 => 5,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroLocale/Localization/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/LocaleBundle/Resources/views/Localization/update.html.twig");
    }
}
