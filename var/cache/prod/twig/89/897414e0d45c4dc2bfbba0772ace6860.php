<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/application_menu_desktop_sided.html.twig */
class __TwigTemplate_d43968272c34a6cf8fa7dacd90e09ba2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'list_wrapper' => [$this, 'block_list_wrapper'],
            'item' => [$this, 'block_item'],
            'linkElement' => [$this, 'block_linkElement'],
            'item_content' => [$this, 'block_item_content'],
            'label' => [$this, 'block_label'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/Menu/application_menu_desktop_sided.html.twig", 2)->unwrap();
        // line 3
        $macros["Navigation"] = $this->macros["Navigation"] = $this->loadTemplate("@OroNavigation/macros.html.twig", "@OroNavigation/Menu/application_menu_desktop_sided.html.twig", 3)->unwrap();
        // line 1
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/application_menu_desktop_sided.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_list_wrapper($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        // line 7
        $context["childrenClasses"] = twig_array_merge(($context["childrenClasses"] ?? null), [0 => "menu", 1 => ("menu-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 7))]);
        // line 8
        $context["listAttributes"] = twig_array_merge(($context["childrenAttributes"] ?? null), ["class" => twig_join_filter(array_unique(($context["childrenClasses"] ?? null)), " ")]);
        // line 9
        echo "
    ";
        // line 10
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 10)) {
            // line 11
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 11) == 1)) {
                // line 12
                echo "            <div class=\"dropdown-menu-wrapper dropdown-menu-wrapper__scrollable\">";
                // line 13
                $this->displayBlock("list", $context, $blocks);
                // line 14
                echo "</div>
        ";
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 15
($context["item"] ?? null), "level", [], "any", false, false, false, 15) == 2)) {
                // line 16
                echo "            <div class=\"dropdown-menu-wrapper dropdown-menu-wrapper__child\">";
                // line 17
                $this->displayBlock("list", $context, $blocks);
                // line 18
                echo "</div>
        ";
            } else {
                // line 20
                $this->displayBlock("list", $context, $blocks);
            }
            // line 22
            echo "    ";
        }
    }

    // line 25
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 26) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 26))) {
            // line 27
            $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => "dropdown", 1 => ("dropdown-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 27))]);
            // line 28
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 28) != 1)) {
                // line 29
                $context["childrenClasses"] = twig_array_merge(($context["childrenClasses"] ?? null), [0 => "dropdown-menu"]);
                // line 30
                echo "        ";
            }
        } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 31
($context["item"] ?? null), "level", [], "any", false, false, false, 31) == 1)) {
            // line 32
            $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => "dropdown", 1 => "dropdown-empty", 2 => ("dropdown-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 32))]);
            // line 33
            echo "    ";
        }
        // line 34
        echo "    ";
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", true, true, false, 34) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 34), "routes", [], "any", true, true, false, 34)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, true, false, 34), "routes", [], "any", false, true, false, 34), 0, [], "array", true, true, false, 34))) {
            // line 35
            echo "        ";
            if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 35), "routes", [], "any", false, false, false, 35)) > 1)) {
                // line 36
                echo "            ";
                $context["itemAttributes"] = twig_array_merge(($context["itemAttributes"] ?? null), ["data-routes" => json_encode(twig_slice($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 36), "routes", [], "any", false, false, false, 36), 1))]);
                // line 37
                echo "        ";
            }
            // line 38
            echo "        ";
            $context["itemAttributes"] = twig_array_merge(($context["itemAttributes"] ?? null), ["data-route" => (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extras", [], "any", false, false, false, 38), "routes", [], "any", false, false, false, 38)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null)]);
            // line 39
            echo "    ";
        }
        // line 40
        echo "
    ";
        // line 41
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 41) == 1)) {
            // line 42
            echo "        ";
            $context["hasValidChildren"] = false;
            // line 43
            echo "
        ";
            // line 44
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["item"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["sub_item"]) {
                // line 45
                echo "            ";
                if ( !($context["hasValidChildren"] ?? null)) {
                    // line 46
                    $context["showNonAuthorized"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "extras", [], "any", false, true, false, 46), "show_non_authorized", [], "any", true, true, false, 46) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "extras", [], "any", false, false, false, 46), "show_non_authorized", [], "any", false, false, false, 46));
                    // line 47
                    $context["displayable"] = (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "extras", [], "any", false, false, false, 47), "isAllowed", [], "any", false, false, false, 47) || ($context["showNonAuthorized"] ?? null));
                    // line 48
                    if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "displayed", [], "any", false, false, false, 48) && ($context["displayable"] ?? null)) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["sub_item"], "getExtra", [0 => "divider"], "method", false, false, false, 48))) {
                        // line 49
                        echo "                    ";
                        $context["hasValidChildren"] = true;
                        // line 50
                        echo "                ";
                    }
                    // line 51
                    echo "            ";
                }
                // line 52
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['sub_item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 53
            echo "
        ";
            // line 54
            if (((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 54)) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 54) != "#")) || ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 54) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 54)) && ($context["hasValidChildren"] ?? null)))) {
                // line 55
                echo "            ";
                $this->displayBlock("item_renderer", $context, $blocks);
                echo "
        ";
            }
            // line 57
            echo "    ";
        } else {
            // line 58
            echo "        ";
            $this->displayBlock("item_renderer", $context, $blocks);
            echo "
    ";
        }
    }

    // line 62
    public function block_linkElement($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/application_menu_desktop_sided.html.twig", 63)->unwrap();
        // line 64
        echo "
    ";
        // line 65
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 65) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 65)) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 65) === 0))) {
            // line 66
            echo "        ";
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [            // line 67
($context["linkAttributes"] ?? null), "class", [0 => "dropdown-toggle"]], 67, $context, $this->getSourceContext()), "data-toggle" => "dropdown"]);
            // line 70
            echo "    ";
        }
        // line 71
        echo "
    ";
        // line 72
        if ((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 72)) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 72) == "#"))) {
            // line 73
            echo "        ";
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [            // line 74
($context["linkAttributes"] ?? null), "class", [0 => "unclickable"]], 74, $context, $this->getSourceContext())]);
            // line 76
            echo "
        ";
            // line 77
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 77) > 1)) {
                // line 78
                echo "            ";
                $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["tabindex" =>  -1]);
                // line 81
                echo "        ";
            }
            // line 82
            echo "    ";
        }
        // line 83
        echo "
    <a href=\"";
        // line 84
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 84), "html", null, true);
        echo "\"";
        echo twig_call_macro($macros["oro_menu"], "macro_attributes", [($context["linkAttributes"] ?? null)], 84, $context, $this->getSourceContext());
        echo ">
        <span class=\"title ";
        // line 85
        echo twig_escape_filter($this->env, ("title-level-" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getLevel", [], "method", false, false, false, 85)), "html", null, true);
        echo "\" title=\"";
        // line 86
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "translate_disabled", 1 => false], "method", false, false, false, 86) == false)) {
            // line 87
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getLabel", [], "method", false, false, false, 87), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "translateParams", 1 => []], "method", false, false, false, 87), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "translateDomain", 1 => "messages"], "method", false, false, false, 87)), "html", null, true);
        } else {
            // line 89
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getLabel", [], "method", false, false, false, 89), "html", null, true);
        }
        // line 91
        echo "\">";
        echo twig_trim_filter(        $this->renderBlock("label", $context, $blocks));
        echo "</span>
    </a>
";
    }

    // line 95
    public function block_item_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 96
        echo "   ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 96) == 1)) {
            // line 97
            echo "       ";
            $context["linkAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "linkAttributes", [], "any", false, false, false, 97);
            // line 98
            echo "       ";
            $this->displayBlock("linkElement", $context, $blocks);
            echo "

       <div class=\"dropdown-menu\">";
            // line 101
            $context["wrapperContent"] =             $this->renderBlock("list_wrapper", $context, $blocks);
            // line 102
            if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "uri", [], "any", false, false, false, 102) != "#") || ($context["wrapperContent"] ?? null))) {
                // line 103
                echo ($context["wrapperContent"] ?? null);
            }
            // line 105
            echo "       </div>
   ";
        } else {
            // line 107
            echo "       ";
            $this->displayParentBlock("item_content", $context, $blocks);
            echo "
   ";
        }
    }

    // line 111
    public function block_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 112
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 112) == 1) &&  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "getExtra", [0 => "icon"], "method", false, false, false, 112))) {
            // line 113
            echo "        <span class=\"fa-th menu-icon\" aria-hidden=\"true\"></span>
        ";
            // line 114
            $this->displayParentBlock("label", $context, $blocks);
            echo "
    ";
        } else {
            // line 116
            echo "        ";
            $this->displayParentBlock("label", $context, $blocks);
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/application_menu_desktop_sided.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  319 => 116,  314 => 114,  311 => 113,  309 => 112,  305 => 111,  297 => 107,  293 => 105,  290 => 103,  288 => 102,  286 => 101,  280 => 98,  277 => 97,  274 => 96,  270 => 95,  262 => 91,  259 => 89,  256 => 87,  254 => 86,  251 => 85,  245 => 84,  242 => 83,  239 => 82,  236 => 81,  233 => 78,  231 => 77,  228 => 76,  226 => 74,  224 => 73,  222 => 72,  219 => 71,  216 => 70,  214 => 67,  212 => 66,  210 => 65,  207 => 64,  204 => 63,  200 => 62,  192 => 58,  189 => 57,  183 => 55,  181 => 54,  178 => 53,  172 => 52,  169 => 51,  166 => 50,  163 => 49,  161 => 48,  159 => 47,  157 => 46,  154 => 45,  150 => 44,  147 => 43,  144 => 42,  142 => 41,  139 => 40,  136 => 39,  133 => 38,  130 => 37,  127 => 36,  124 => 35,  121 => 34,  118 => 33,  116 => 32,  114 => 31,  111 => 30,  109 => 29,  107 => 28,  105 => 27,  102 => 26,  98 => 25,  93 => 22,  90 => 20,  86 => 18,  84 => 17,  82 => 16,  80 => 15,  77 => 14,  75 => 13,  73 => 12,  70 => 11,  68 => 10,  65 => 9,  63 => 8,  61 => 7,  59 => 6,  55 => 5,  50 => 1,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/application_menu_desktop_sided.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/application_menu_desktop_sided.html.twig");
    }
}
