<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntity/Select/entity_field/selection_multiple_with_relations.html.twig */
class __TwigTemplate_19d9f61bff32da57e2bb9cc5a75b8557 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% _.each(context.splitFieldId(id), function (val, index, list) { %>&#32;<%- val.entity.label %>&nbsp;<b><%- val.label %></b>&nbsp;<% if (index < list.length - 1) { %>><% } %><% }) %>
";
    }

    public function getTemplateName()
    {
        return "@OroEntity/Select/entity_field/selection_multiple_with_relations.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntity/Select/entity_field/selection_multiple_with_relations.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Resources/views/Select/entity_field/selection_multiple_with_relations.html.twig");
    }
}
