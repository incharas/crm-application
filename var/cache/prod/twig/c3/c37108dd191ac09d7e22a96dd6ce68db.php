<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Datagrid/aclGrid.html.twig */
class __TwigTemplate_999f5daf7d9c7017bc2a9ff3b5c5f408 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroWorkflow/Datagrid/aclGrid.html.twig", 1)->unwrap();
        // line 2
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["workflow-permission-grid", ["role" =>         // line 4
($context["entity"] ?? null)], ["cssClass" => "workflow-permission-grid inner-permissions-grid", "themeOptions" => ["readonly" =>         // line 5
($context["isReadonly"] ?? null)]]], 2, $context, $this->getSourceContext());
        // line 6
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Datagrid/aclGrid.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  43 => 6,  41 => 5,  40 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Datagrid/aclGrid.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Datagrid/aclGrid.html.twig");
    }
}
