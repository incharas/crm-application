<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDraft/Draft/saveAsNewDraft.html.twig */
class __TwigTemplate_7cb2914e7d19f2eabda2f4e37bd1c7e9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDraft/Draft/saveAsNewDraft.html.twig", 1)->unwrap();
        // line 2
        echo twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["class" => "btn", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.draft.operations.update.save_as_new_draft.label"), "action" => "save_as_draft"]], 2, $context, $this->getSourceContext());
        // line 6
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroDraft/Draft/saveAsNewDraft.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  41 => 6,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDraft/Draft/saveAsNewDraft.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DraftBundle/Resources/views/Draft/saveAsNewDraft.html.twig");
    }
}
