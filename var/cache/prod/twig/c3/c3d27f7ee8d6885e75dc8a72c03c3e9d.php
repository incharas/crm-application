<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNotification/EmailNotification/update.html.twig */
class __TwigTemplate_b5cc0f0f93df7760a6c7ce1d785d3ace extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => "@OroUser/Form/fields.html.twig", 2 => "@OroEmail/Form/fields.html.twig"], true);
        // line 8
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 8), "value", [], "any", false, false, false, 8), "id", [], "any", false, false, false, 8)) {

            $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%id%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 9
($context["form"] ?? null), "vars", [], "any", false, false, false, 9), "value", [], "any", false, false, false, 9), "id", [], "any", false, false, false, 9)]]);
        }
        // line 12
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 12), "value", [], "any", false, false, false, 12), "id", [], "any", false, false, false, 12)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_notification_emailnotification_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "value", [], "any", false, false, false, 13), "id", [], "any", false, false, false, 13)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_notification_emailnotification_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroNotification/EmailNotification/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 17
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNotification/EmailNotification/update.html.twig", 18)->unwrap();
        // line 19
        echo "
    ";
        // line 20
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20), "id", [], "any", false, false, false, 20) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 20), "value", [], "any", false, false, false, 20)))) {
            // line 21
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_emailnotication", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["form"] ?? null), "vars", [], "any", false, false, false, 22), "value", [], "any", false, false, false, 22), "id", [], "any", false, false, false, 22)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_notification_emailnotification_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-emailnotification", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 26
($context["form"] ?? null), "vars", [], "any", false, false, false, 26), "value", [], "any", false, false, false, 26), "id", [], "any", false, false, false, 26), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.emailnotification.entity_label")]], 21, $context, $this->getSourceContext());
            // line 28
            echo "
        ";
            // line 29
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 29, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 31
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_notification_emailnotification_index")], 31, $context, $this->getSourceContext());
        echo "
    ";
        // line 32
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_notification_emailnotification_index"]], 32, $context, $this->getSourceContext());
        // line 35
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_notification_emailnotification_create")) {
            // line 36
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_notification_emailnotification_create"]], 36, $context, $this->getSourceContext()));
            // line 39
            echo "    ";
        }
        // line 40
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 40), "value", [], "any", false, false, false, 40), "id", [], "any", false, false, false, 40) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_notification_emailnotification_update"))) {
            // line 41
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_notification_emailnotification_update", "params" => ["id" => "\$id"]]], 41, $context, $this->getSourceContext()));
            // line 45
            echo "    ";
        }
        // line 46
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 46, $context, $this->getSourceContext());
        echo "
";
    }

    // line 49
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 50
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 50), "value", [], "any", false, false, false, 50), "id", [], "any", false, false, false, 50)) {
            // line 51
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 52
($context["form"] ?? null), "vars", [], "any", false, false, false, 52), "value", [], "any", false, false, false, 52), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_notification_emailnotification_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.emailnotification.entity_plural_label"), "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.emailnotification.edit_entity", ["%name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 55
($context["form"] ?? null), "vars", [], "any", false, false, false, 55), "value", [], "any", false, false, false, 55), "id", [], "any", false, false, false, 55)])];
            // line 58
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 60
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.emailnotification.entity_label")]);
            // line 61
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroNotification/EmailNotification/update.html.twig", 61)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 62
            echo "    ";
        }
    }

    // line 65
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNotification/EmailNotification/update.html.twig", 66)->unwrap();
        // line 67
        echo "
    ";
        // line 68
        $context["id"] = "emailnotificaton-edit";
        // line 69
        echo "
    ";
        // line 70
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recipientList", [], "any", false, false, false, 70), "vars", [], "any", false, false, false, 70), "errors", [], "any", false, false, false, 70)) == 0)) {
            // line 71
            $context["subblocks"] = [0 => twig_call_macro($macros["UI"], "macro_renderHtmlProperty", ["", ("* " . $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.form.recipient_list.empty"))], 71, $context, $this->getSourceContext())];
            // line 72
            echo "    ";
        } else {
            // line 73
            echo "        ";
            $context["subblocks"] = [0 => twig_call_macro($macros["UI"], "macro_renderHtmlProperty", ["", $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recipientList", [], "any", false, false, false, 73), 'errors')], 73, $context, $this->getSourceContext())];
        }
        // line 75
        echo "
    ";
        // line 76
        $context["recipientList"] = ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.notification.emailnotification.recipient_list.label"), "class" => "", "subblocks" => [0 => ["title" => "", "data" => twig_array_merge(((        // line 81
array_key_exists("subblocks", $context)) ? (_twig_default_filter(($context["subblocks"] ?? null), [])) : ([])), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "recipientList", [], "any", false, false, false, 81), 'widget')])]]];
        // line 84
        echo "
    ";
        // line 85
        $context["data"] = ["formErrors" => ((        // line 86
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 94
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget')]]]], 1 =>         // line 98
($context["recipientList"] ?? null)]];
        // line 101
        echo "
    <div class=\"responsive-form-inner\">
        ";
        // line 103
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
    </div>
";
    }

    // line 107
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 108
        echo "    <div class=\"scrollable-container\"
        data-page-component-module=\"oroui/js/app/components/view-component\"
        data-page-component-options=\"";
        // line 110
        echo twig_escape_filter($this->env, json_encode(["view" => "oronotification/js/app/views/email-notification-form-view", "selectors" => ["form" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 113
($context["form"] ?? null), "vars", [], "any", false, false, false, 113), "id", [], "any", false, false, false, 113)), "listenChangeElements" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 114
($context["form"] ?? null), "vars", [], "any", false, false, false, 114), "listenChangeElements", [], "any", false, false, false, 114)]]), "html", null, true);
        // line 116
        echo "\">
        ";
        // line 117
        $this->displayParentBlock("content", $context, $blocks);
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroNotification/EmailNotification/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  213 => 117,  210 => 116,  208 => 114,  207 => 113,  206 => 110,  202 => 108,  198 => 107,  191 => 103,  187 => 101,  185 => 98,  184 => 94,  183 => 86,  182 => 85,  179 => 84,  177 => 81,  176 => 76,  173 => 75,  169 => 73,  166 => 72,  164 => 71,  162 => 70,  159 => 69,  157 => 68,  154 => 67,  151 => 66,  147 => 65,  142 => 62,  139 => 61,  136 => 60,  130 => 58,  128 => 55,  127 => 52,  125 => 51,  122 => 50,  118 => 49,  111 => 46,  108 => 45,  105 => 41,  102 => 40,  99 => 39,  96 => 36,  93 => 35,  91 => 32,  86 => 31,  81 => 29,  78 => 28,  76 => 26,  75 => 22,  73 => 21,  71 => 20,  68 => 19,  65 => 18,  61 => 17,  56 => 1,  54 => 13,  53 => 12,  50 => 9,  47 => 8,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNotification/EmailNotification/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NotificationBundle/Resources/views/EmailNotification/update.html.twig");
    }
}
