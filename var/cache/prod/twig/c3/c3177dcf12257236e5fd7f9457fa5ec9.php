<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroIntegration/Integration/Datagrid/message.html.twig */
class __TwigTemplate_88dc9d0dac80e05d80f7351404a3616e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo twig_nl2br(twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true));
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroIntegration/Integration/Datagrid/message.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroIntegration/Integration/Datagrid/message.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Resources/views/Integration/Datagrid/message.html.twig");
    }
}
