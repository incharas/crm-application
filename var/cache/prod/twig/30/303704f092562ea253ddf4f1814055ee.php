<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Default/navbar/sided.html.twig */
class __TwigTemplate_d79981795bc24f639c30cf35d14b6347 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        // line 2
        $_trait_0 = $this->loadTemplate("@OroUI/Default/navbar/blocks.html.twig", "@OroUI/Default/navbar/sided.html.twig", 2);
        if (!$_trait_0->isTraitable()) {
            throw new RuntimeError('Template "'."@OroUI/Default/navbar/blocks.html.twig".'" cannot be used as a trait.', 2, $this->source);
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            [
            ]
        );
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_ui.navbar_position") == "left"))) {
            // line 2
            echo "    ";
            // line 3
            echo "    ";
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/Default/navbar/sided.html.twig", 3)->unwrap();
            // line 4
            echo "
    <div id=\"side-menu\" class=\"main-menu-sided minimized\"
        ";
            // line 6
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/jquery-widget-component", "options" => ["widgetModule" => "oroui/js/desktop/side-menu", "toggleSelector" => "#main-menu-toggler"]]], 6, $context, $this->getSourceContext());
            // line 12
            echo ">
        <header class=\"main-menu__header\">
            ";
            // line 14
            ob_start(function () { return ''; });
            // line 15
            echo "                ";
            ob_start(function () { return ''; });
            // line 16
            echo "                    ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("organization_name", $context)) ? (_twig_default_filter(($context["organization_name"] ?? null), "organization_name")) : ("organization_name")), array());
            // line 17
            echo "                ";
            $___internal_parse_23_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 15
            echo twig_spaceless($___internal_parse_23_);
            // line 18
            echo "            ";
            $context["organization_name"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 19
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_app_logo", [($context["organization_name"] ?? null)], 19, $context, $this->getSourceContext());
            echo "
        </header>

        <nav class=\"scroller\" id=\"main-menu\"
                ";
            // line 23
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/main-menu-view"]], 23, $context, $this->getSourceContext());
            echo ">
            ";
            // line 24
            $this->displayBlock("application_menu", $context, $blocks);
            echo "
        </nav>

        <button id=\"main-menu-toggler\" type=\"button\" class=\"main-menu-toggler\" aria-label=\"";
            // line 27
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.navbar.toggler_label"), "html", null, true);
            echo "\"></button>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroUI/Default/navbar/sided.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  100 => 27,  94 => 24,  90 => 23,  82 => 19,  79 => 18,  77 => 15,  74 => 17,  71 => 16,  68 => 15,  66 => 14,  62 => 12,  60 => 6,  56 => 4,  53 => 3,  51 => 2,  49 => 1,  30 => 2,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Default/navbar/sided.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Default/navbar/sided.html.twig");
    }
}
