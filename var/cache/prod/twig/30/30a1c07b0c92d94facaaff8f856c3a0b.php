<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/bootstrap/bootstrap-scrollspy.js */
class __TwigTemplate_bec4b93c4b49f70b1843e319cc52bfe4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const MutationObserver = window.MutationObserver;

    const NAME = 'scrollspy';
    const DATA_KEY = 'bs.scrollspy';
    const EVENT_KEY = '.' + DATA_KEY;
    const DATA_API_KEY = '.data-api';
    const NAV_LINKS = '.nav > a';
    const Event = {
        LOAD_DATA_API: 'load' + EVENT_KEY + DATA_API_KEY
    };
    const Selector = {
        DATA_SPY: '[data-spy=\"scroll\"]'
    };

    require('bootstrap-scrollspy');

    const ScrollSpy = \$.fn[NAME].Constructor;
    const JQUERY_NO_CONFLICT = \$.fn[NAME].noConflict();

    const OroScrollSpy = function OroScrollSpy(element, options) {
        ScrollSpy.call(this, element, options);

        this._selector += ', ' + this._config.target + ' ' + NAV_LINKS;

        if (!MutationObserver) {
            return;
        }

        const \$element = \$(element);
        const \$collection = \$element.is('body') ? \$element.children() : \$element;

        this._mutationObserver = new MutationObserver(_.debounce(function(mutations) {
            // Destroy scrollspy if element is not exist in the DOM
            if (\$(document).find(\$element).length) {
                \$element.scrollspy('refresh');
            } else {
                this.dispose();
            }
        }.bind(this), 50));

        \$collection.each(function(index, element) {
            this._mutationObserver.observe(element, {
                attributes: true,
                childList: true,
                subtree: true,
                characterData: true
            });
        }.bind(this));
    };

    OroScrollSpy.__super__ = ScrollSpy.prototype;
    OroScrollSpy.prototype = Object.assign(Object.create(ScrollSpy.prototype), {
        constructor: OroScrollSpy,

        /**
         * Method for destroy scrollspy, disable event listener
         * disconnect observer if that exist
         */
        dispose: function() {
            if (this._mutationObserver) {
                this._mutationObserver.disconnect();
                this._mutationObserver = null;
            }

            return ScrollSpy.prototype.dispose.call(this);
        }
    });

    OroScrollSpy._jQueryInterface = function _jQueryInterface(config) {
        return this.each(function() {
            let data = \$(this).data(DATA_KEY);
            const _config = typeof config === 'object' && config;

            if (!data) {
                data = new OroScrollSpy(this, _config);
                \$(this).data(DATA_KEY, data);
            }
            if (typeof config === 'string') {
                if (typeof data[config] === 'undefined') {
                    throw new TypeError('No method named ' + config);
                }

                data[config]();
            }
        });
    };

    Object.defineProperties(OroScrollSpy, {
        VERSION: {
            configurable: true,
            get: function get() {
                return ScrollSpy.VERSION;
            }
        },
        Default: {
            configurable: true,
            get: function get() {
                return ScrollSpy.Default;
            }
        }
    });

    \$(window).off(Event.LOAD_DATA_API).on(Event.LOAD_DATA_API, function() {
        const scrollSpys = \$.makeArray(\$(Selector.DATA_SPY));

        for (let i = scrollSpys.length; i--;) {
            const \$spy = \$(scrollSpys[i]);

            ScrollSpy._jQueryInterface.call(\$spy, \$spy.data());
        }
    });

    \$.fn[NAME] = OroScrollSpy._jQueryInterface;
    \$.fn[NAME].Constructor = OroScrollSpy;

    \$.fn[NAME].noConflict = function() {
        \$.fn[NAME] = JQUERY_NO_CONFLICT;
        return OroScrollSpy._jQueryInterface;
    };

    return OroScrollSpy;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-scrollspy.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/bootstrap/bootstrap-scrollspy.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/bootstrap/bootstrap-scrollspy.js");
    }
}
