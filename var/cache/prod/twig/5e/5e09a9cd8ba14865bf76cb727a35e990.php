<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroScope/macros.html.twig */
class __TwigTemplate_de5331cc6448c844f6646b269e32163c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "
";
    }

    // line 1
    public function macro_renderRestrictionsView($__scopeEntities__ = null, $__scopes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "scopeEntities" => $__scopeEntities__,
            "scopes" => $__scopes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["self"] = $this;
            // line 3
            echo "    ";
            if ((twig_test_empty(($context["scopes"] ?? null)) || $this->extensions['Oro\Bundle\ScopeBundle\Twig\ScopeExtension']->isScopesEmpty(($context["scopeEntities"] ?? null), ($context["scopes"] ?? null)))) {
                // line 4
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
                echo "
    ";
            } else {
                // line 6
                echo "        ";
                $context["gridHtml"] = twig_call_macro($macros["self"], "macro_renderRestrictionsViewGrid", [($context["scopeEntities"] ?? null), ($context["scopes"] ?? null)], 6, $context, $this->getSourceContext());
                // line 7
                echo "        ";
                echo twig_escape_filter($this->env, ($context["gridHtml"] ?? null), "html", null, true);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 11
    public function macro_renderRestrictionsViewGrid($__scopeEntities__ = null, $__scopes__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "scopeEntities" => $__scopeEntities__,
            "scopes" => $__scopes__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 12
            echo "    <div class=\"grid-container\">
        <table class=\"grid grid-main-container table table-bordered table-condensed table-fixed table-hover\">
            <thead>
                <tr>
                    ";
            // line 16
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["scopeEntities"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["entityClass"]) {
                // line 17
                echo "                        <th><span>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue($context["entityClass"], "label")), "html", null, true);
                echo "</span></th>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entityClass'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 19
            echo "                </tr>
            </thead>
            <tbody>
            ";
            // line 22
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["scopes"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["scope"]) {
                // line 23
                echo "                <tr>
                    ";
                // line 24
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["scopeEntities"] ?? null));
                foreach ($context['_seq'] as $context["fieldName"] => $context["entityClass"]) {
                    // line 25
                    echo "                        <td>
                            ";
                    // line 26
                    echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scope"], $context["fieldName"], [], "any", false, false, false, 26)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["scope"], $context["fieldName"], [], "any", false, false, false, 26)) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Any"))), "html", null, true);
                    echo "
                        </td>
                    ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['fieldName'], $context['entityClass'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 29
                echo "                </tr>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['scope'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 31
            echo "            </tbody>
        </table>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroScope/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  152 => 31,  145 => 29,  136 => 26,  133 => 25,  129 => 24,  126 => 23,  122 => 22,  117 => 19,  108 => 17,  104 => 16,  98 => 12,  84 => 11,  71 => 7,  68 => 6,  62 => 4,  59 => 3,  56 => 2,  42 => 1,  37 => 10,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroScope/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ScopeBundle/Resources/views/macros.html.twig");
    }
}
