<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/main-menu/main-menu-sided.scss */
class __TwigTemplate_d52dcd5e6d87643eacdb3d740dc5b71d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.main-menu-sided {
    position: relative;

    float: left;
    width: \$menu-side-width;
    height: 100%;
    padding-bottom: 45px;

    background-color: \$menu-background-color;

    .accordion {
        margin: 0;

        .title {
            @include text-line-truncate();

            display: block;
        }

        a {
            display: block;
        }
    }

    .accordion-group {
        width: \$menu-side-width;
        margin-bottom: 0;

        border: none;
        border-radius: 0;

        .menu {
            margin: 0;
            padding: 0;

            .title,
            .accordion-toggle {
                color: \$menu-item-text-color;

                &::before {
                    content: '';

                    display: inline-block;
                    height: 16px;
                    min-width: 16px;
                    margin-right: 8px;

                    font-weight: font-weight('light');
                    line-height: 1;
                    text-align: center;

                    color: \$menu-accordion-icon-color;

                    transition: \$menu-animation;
                }
            }

            .accordion-toggle {
                font-weight: font-weight('bold');

                @include fa-icon(\$menu-icon, before, true, true) {
                    transform: rotate(90deg);
                }

                &.collapsed {
                    font-weight: font-weight('light');

                    &::before {
                        transform: rotate(0);
                    }
                }
            }

            .title {
                display: block;
                padding: 8px 15px;

                &:hover {
                    background-color: \$menu-background-color-active;
                }

                \$menu-title-offset: 0;

                @for \$i from 1 through 5 {
                    \$selector: '&-level-' + \$i;

                    #{\$selector} {
                        padding-left: \$menu-title-offset * 1px;
                    }

                    \$menu-title-offset: \$menu-title-offset + 24;
                }
            }
        }

        li {
            display: block;
        }
    }

    .accordion-body {
        background-color: \$menu-dropdown-background-color;

        > a {
            display: none;
        }

        .active {
            background-color: \$menu-background-color-active;
        }

        .accordion-group {
            background-color: transparent;
        }
    }

    // First level
    .accordion-toggle.title-level-1 {
        position: relative;
        padding: 12px 22px 12px 24px;

        background-color: \$menu-dropdown-background-color-active;
        color: \$menu-item-first-level-text-color;

        .menu-icon {
            min-width: 16px;
            margin-right: 8px;
            margin-left: 0;

            text-align: center;
        }

        &.collapsed {
            background-color: \$menu-background-color;
        }

        &:hover {
            background-color: \$menu-dropdown-background-color-active;
        }
    }

    .scroller {
        position: relative;

        height: calc(100% - #{\$menu-header-height});

        overflow: hidden;
    }

    &.minimized {
        width: \$menu-side-width-minimized;

        .unclickable {
            display: block;
        }

        .dropdown {
            position: static;
        }

        .title-level-1 {
            display: block;
            padding: 13px 6px;

            font-size: 11px;
            line-height: 1.2;

            color: \$menu-item-first-level-text-color;

            word-break: break-word;

            .menu-icon {
                display: block;
                width: 100%;
                margin: 0 0 5px;
                font-size: 24px;
            }
        }

        // First level
        .dropdown-level-1 {
            text-align: center;

            width: \$menu-side-width-minimized;

            .dropdown-menu {
                display: none;
            }

            &:hover {
                background-color: \$menu-dropdown-background-color-active;

                .unclickable {
                    color: \$menu-item-first-level-text-color-active;
                }
            }

            &.active {
                background-color: \$menu-dropdown-background-color;

                .title-level-1 {
                    color: \$menu-minimized-item-first-level-text-color-active;
                }
            }
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/main-menu/main-menu-sided.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/main-menu/main-menu-sided.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/main-menu/main-menu-sided.scss");
    }
}
