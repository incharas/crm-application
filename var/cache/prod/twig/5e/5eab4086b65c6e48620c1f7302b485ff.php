<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroPlatform/Platform/systemInfo.html.twig */
class __TwigTemplate_786426d0fe814fdbe5ab9e6d765c1ea4 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["entity"] = null;
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroPlatform/Platform/systemInfo.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroPlatform/Platform/systemInfo.html.twig", 6)->display(twig_array_merge($context, ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.system_info")]));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 11
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        $macros["systeminfo"] = $this;
        // line 13
        echo "
    ";
        // line 14
        ob_start(function () { return ''; });
        // line 15
        echo "        <div class=\"row-fluid\">
            <div class=\"responsive-block package-list\">
                ";
        // line 17
        echo twig_call_macro($macros["systeminfo"], "macro_deploymentVariables", [($context["deploymentVariables"] ?? null)], 17, $context, $this->getSourceContext());
        echo "
            </div>
        </div>
    ";
        $context["deploymentSection"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 21
        echo "
    ";
        // line 22
        ob_start(function () { return ''; });
        // line 23
        echo "        <div class=\"row-fluid\">
            <div class=\"responsive-block package-list\">
                <h3>";
        // line 25
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.caption.oro"), "html", null, true);
        echo "</h3>
                ";
        // line 26
        echo twig_call_macro($macros["systeminfo"], "macro_packagesTable", [($context["oroPackages"] ?? null)], 26, $context, $this->getSourceContext());
        echo "
            </div>
            <div class=\"responsive-block package-list\">
                <h3>";
        // line 29
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.caption.thirdParty"), "html", null, true);
        echo "</h3>
                ";
        // line 30
        echo twig_call_macro($macros["systeminfo"], "macro_packagesTable", [($context["thirdPartyPackages"] ?? null)], 30, $context, $this->getSourceContext());
        echo "
            </div>
        </div>
    ";
        $context["packagesSection"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 34
        echo "
    ";
        // line 35
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.deployment_variables"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 40
($context["deploymentSection"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.packages"), "subblocks" => [0 => ["data" => [0 =>         // line 46
($context["packagesSection"] ?? null)]]]]];
        // line 50
        echo "
    ";
        // line 51
        $context["id"] = "system_info";
        // line 52
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 53
        echo "
    ";
        // line 54
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    // line 57
    public function macro_packagesTable($__packages__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "packages" => $__packages__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 58
            echo "    ";
            if (twig_length_filter($this->env, ($context["packages"] ?? null))) {
                // line 59
                echo "        <table class=\"table table-bordered table-striped \">
            <thead>
            <tr>
                <th>";
                // line 62
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.package.name"), "html", null, true);
                echo "</th>
                <th>";
                // line 63
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.package.version"), "html", null, true);
                echo "</th>
                <th>";
                // line 64
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.package.license"), "html", null, true);
                echo "</th>
            </tr>
            </thead>
            <tbody>
            ";
                // line 68
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["packages"] ?? null));
                foreach ($context['_seq'] as $context["packageName"] => $context["package"]) {
                    // line 69
                    echo "                <tr>
                    <td>";
                    // line 70
                    echo twig_escape_filter($this->env, $context["packageName"], "html", null, true);
                    echo "</td>
                    <td>";
                    // line 71
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["package"], "pretty_version", [], "any", false, false, false, 71), "html", null, true);
                    echo "</td>
                    <td>
                        ";
                    // line 73
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["package"], "license", [], "any", false, false, false, 73));
                    foreach ($context['_seq'] as $context["_key"] => $context["license"]) {
                        // line 74
                        echo "                            ";
                        echo twig_escape_filter($this->env, $context["license"], "html", null, true);
                        echo "
                        ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['license'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 76
                    echo "                    </td>
                </tr>
            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['packageName'], $context['package'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 79
                echo "            </tbody>
        </table>
    ";
            } else {
                // line 82
                echo "        <div class=\"well\">
            ";
                // line 83
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.platform.no_packages"), "html", null, true);
                echo "
        </div>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 88
    public function macro_deploymentVariables($__variables__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "variables" => $__variables__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 89
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroPlatform/Platform/systemInfo.html.twig", 89)->unwrap();
            // line 90
            echo "    <div class=\"widget-content\">
        <div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
            // line 93
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["variables"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["variable"]) {
                // line 94
                echo "                    ";
                echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "label", [], "any", false, false, false, 94)), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "value", [], "any", true, true, false, 94)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["variable"], "value", [], "any", false, false, false, 94), "N/A")) : ("N/A"))], 94, $context, $this->getSourceContext());
                echo "
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['variable'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 96
            echo "            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroPlatform/Platform/systemInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  271 => 96,  262 => 94,  258 => 93,  253 => 90,  250 => 89,  237 => 88,  224 => 83,  221 => 82,  216 => 79,  208 => 76,  199 => 74,  195 => 73,  190 => 71,  186 => 70,  183 => 69,  179 => 68,  172 => 64,  168 => 63,  164 => 62,  159 => 59,  156 => 58,  143 => 57,  137 => 54,  134 => 53,  131 => 52,  129 => 51,  126 => 50,  124 => 46,  123 => 40,  122 => 35,  119 => 34,  112 => 30,  108 => 29,  102 => 26,  98 => 25,  94 => 23,  92 => 22,  89 => 21,  82 => 17,  78 => 15,  76 => 14,  73 => 13,  70 => 12,  66 => 11,  60 => 9,  55 => 6,  51 => 5,  46 => 1,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroPlatform/Platform/systemInfo.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/PlatformBundle/Resources/views/Platform/systemInfo.html.twig");
    }
}
