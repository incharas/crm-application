<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Calendar/viewDefault.html.twig */
class __TwigTemplate_00d21cae0d8ea6a8a558f9445a8f465e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'breadcrumb' => [$this, 'block_breadcrumb'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'ownerLink' => [$this, 'block_ownerLink'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroCalendar/Calendar/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroCalendar/Calendar/view.html.twig", "@OroCalendar/Calendar/viewDefault.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_breadcrumb($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["breadcrumbs"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.my_calendar")]];
        // line 5
        echo "    ";
        $this->loadTemplate("@OroNavigation/Menu/breadcrumbs.html.twig", "@OroCalendar/Calendar/viewDefault.html.twig", 5)->display($context);
    }

    // line 8
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $context["breadcrumbs"] = ["entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.my_calendar")];
        // line 10
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 13
    public function block_ownerLink($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Calendar/viewDefault.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 14,  74 => 13,  67 => 10,  64 => 9,  60 => 8,  55 => 5,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Calendar/viewDefault.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Calendar/viewDefault.html.twig");
    }
}
