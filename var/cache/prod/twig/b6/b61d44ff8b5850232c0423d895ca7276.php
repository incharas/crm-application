<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/tab-collection-item.html */
class __TwigTemplate_a097647354cb9bf9b8a1f485d9829bba extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<a  href=\"#\"
    id=\"<%- uniqueId %>\"
    class=\"nav-link <% if(obj.active) { %>active<% } %>\"
    role=\"tab\"
    data-emulate-btn-press=\"\"
    data-toggle=\"tab\"
    aria-controls=\"<% if(obj.controlTabPanel) { %><%- controlTabPanel %><% } else { %><%- id %><% } %>\"
    aria-selected=\"<% if(obj.active) { %>true<% } else { %>false<% } %>\"
    >
    <%- label %>
</a>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/tab-collection-item.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/tab-collection-item.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/tab-collection-item.html");
    }
}
