<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/lazy-validation-collection-view.js */
class __TwigTemplate_0d7274d965630ed12104fe1832355d3b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import BaseView from 'oroui/js/app/views/base/view';

const LazyValidationCollectionView = BaseView.extend({
    events: {
        'change .oro-collection-item[data-validation-ignore]': 'enableValidation'
    },

    constructor: function LazyValidationCollectionView(options) {
        LazyValidationCollectionView.__super__.constructor.call(this, options);
    },

    enableValidation(e) {
        this.\$(e.currentTarget).removeAttr('data-validation-ignore');
    }
});

export default LazyValidationCollectionView;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/lazy-validation-collection-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/lazy-validation-collection-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/lazy-validation-collection-view.js");
    }
}
