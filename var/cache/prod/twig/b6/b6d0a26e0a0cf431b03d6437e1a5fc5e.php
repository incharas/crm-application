<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/variables/select2/select2-container-multi-config.scss */
class __TwigTemplate_a71a10d30ddadc344b56b56e04f007aa extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

\$select2-container-multi-search-choice-border: 1px solid get-color('additional', 'middle') !default;
\$select2-container-multi-search-choice-background-image: linear-gradient(
    to bottom, get-color('additional', 'base') 30%,
    get-color('additional', 'light') 50%,
    get-color('additional', 'middle') 100%
) !default;
\$select2-container-multi-search-choice-color: get-color('additional', 'dark') !default;

\$select2-container-multi-input-color: get-color('additional', 'middle') !default;
\$select2-container-multi-input-radius: \$base-ui-popup-border-radius !default;

\$select2-container-multi-disabled-choices-background: get-color('additional', 'middle') !default;

\$select2-container-multi-choice-close-color: get-color('additional', 'dark') !default;

\$select2-container-multi-disabled-choice-border: 1px solid get-color('additional', 'middle') !default;
\$select2-container-multi-disabled-choice-background: get-color('additional', 'light') !default;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/variables/select2/select2-container-multi-config.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/variables/select2/select2-container-multi-config.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/variables/select2/select2-container-multi-config.scss");
    }
}
