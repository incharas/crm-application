<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/User/Autocomplete/result.html.twig */
class __TwigTemplate_372e2a0237145693be8963251ddb096d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (id !== null) { %>
    <span class=\"select2-result-label-icon\">
        <picture>
            <% _.each(avatar.sources, function(source) { %>
            <source srcset=\"<%- source.srcset %>\" type=\"<%- source.type %>\">
            <% }); %>
            <img src=\"<% if (avatar.src) { %><%- avatar.src %><% } else { %>";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroui/img/avatar-xsmall.png"), "html_attr");
        echo "<% } %>\" width=\"16\" height=\"16\">
        </picture>
    </span><span class=\"select2-result-label-title\"><%= highlight(_.escape(fullName)) %> - <%= highlight(_.escape(email)) %> (<%= highlight(_.escape(username)) %>)</span>
<% } else { %>
    <span class=\"select2-result-label-title\"><%= highlight(_.escape(email)) %></span>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroUser/User/Autocomplete/result.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  45 => 7,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/User/Autocomplete/result.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/User/Autocomplete/result.html.twig");
    }
}
