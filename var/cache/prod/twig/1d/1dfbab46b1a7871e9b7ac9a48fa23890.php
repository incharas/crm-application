<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/models/history-state-model.js */
class __TwigTemplate_9d1405c9d27d702b4372081a7052fc0a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseModel = require('oroui/js/app/models/base/model');

    const HistoryStateModel = BaseModel.extend({
        /**
         * @inheritdoc
         */
        constructor: function HistoryStateModel(attrs, options) {
            HistoryStateModel.__super__.constructor.call(this, attrs, options);
        },

        defaults: function() {
            return {
                data: {}
            };
        }
    });

    return HistoryStateModel;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/models/history-state-model.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/models/history-state-model.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/models/history-state-model.js");
    }
}
