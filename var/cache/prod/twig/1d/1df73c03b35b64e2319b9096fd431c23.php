<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroAccount/Dashboard/myAccountsActivity.html.twig */
class __TwigTemplate_bc38bccf0a901c041debcdc21845e5b0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'actions' => [$this, 'block_actions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/grid.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroAccount/Dashboard/myAccountsActivity.html.twig", 2)->unwrap();
        // line 4
        $context["gridName"] = "dashboard-my-accounts-activity-grid";
        // line 1
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/grid.html.twig", "@OroAccount/Dashboard/myAccountsActivity.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), twig_array_merge(["routerEnabled" => false, "enableFilters" => false], ((        // line 10
array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])))], 7, $context, $this->getSourceContext());
        echo "
";
    }

    // line 13
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 14
        echo "    ";
        $context["actions"] = [0 => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_index"), "type" => "link", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.my_accounts_activity.view_all")]];
        // line 19
        echo "
    ";
        // line 20
        $this->displayParentBlock("actions", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroAccount/Dashboard/myAccountsActivity.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 20,  71 => 19,  68 => 14,  64 => 13,  58 => 10,  56 => 7,  52 => 6,  47 => 1,  45 => 4,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroAccount/Dashboard/myAccountsActivity.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/AccountBundle/Resources/views/Dashboard/myAccountsActivity.html.twig");
    }
}
