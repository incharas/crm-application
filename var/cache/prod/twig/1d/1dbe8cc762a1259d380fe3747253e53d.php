<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/dropdown.scss */
class __TwigTemplate_570e6d4860164ed122a18cf63bf26892 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.dropdown-menu {
    // Support of old markup
    > li > a {
        @extend %dropdown-item;
    }
}

.dropdown-item {
    font-size: \$dropdown-item-font-size;
    line-height: \$dropdown-item-line-height;
    padding-top: \$dropdown-item-inner-offset-top;
    padding-bottom: \$dropdown-item-inner-offset-bottom;

    [class^='fa-'],
    [class*=' fa-'] {
        margin-right: \$dropdown-item-icon-fa-offset;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/dropdown.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/dropdown.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/dropdown.scss");
    }
}
