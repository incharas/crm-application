<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/main.scss */
class __TwigTemplate_a9ff05d4587cfe1cf07041954d929763 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import 'no-data';
@import 'attribute-item';
@import 'flash-messages';
@import 'progressbar';
@import 'checkbox';
@import 'checkbox-label';
@import 'content-sidebar';
@import 'datepicker';
@import 'dialog';
@import 'dropdown';
@import 'drag-and-drop-view';
@import 'error-page';
@import 'forms';
@import 'form-description';
@import 'form';
@import 'header';
@import 'highlight-text';
@import 'jstree-actions';
@import 'jstree';
@import 'jstree-wrapper';
@import 'loading-bar';
@import 'loading-mask';
@import 'main-menu';
@import 'nav';
@import 'oro-tabs';
@import 'tabs';
@import 'page-header';
@import 'page-toolbar';
@import 'scrollspy';
@import 'select2';
@import 'widget-picker';
@import 'tables';
@import 'zoomable-area';
@import 'label';
@import 'load-more';
@import 'scroll-hints';
@import 'image-preview-modal';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/main.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/main.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/main.scss");
    }
}
