<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validator/url.js */
class __TwigTemplate_cb1d34d62e3fee102d96bb32d58a9733 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const __ = require('orotranslation/js/translator');
    const XRegExp = require('xregexp');
    require('jquery.validate');

    const defaultParam = {
        message: 'This value is not a valid URL.',
        protocols: ['http', 'https']
    };

    // JS version of \\Symfony\\Component\\Validator\\Constraints\\UrlValidator::PATTERN
    const patternURL = '^(";
        // line 14
        echo twig_escape_filter($this->env, ($context["protocols"] ?? null), "js", null, true);
        echo ")://' + // protocol
        '(([\\\\pL\\\\pN-]+:)?([\\\\pL\\\\pN-]+)@)?' + // basic auth
        '(' +
            '([-\\\\pL\\\\pN\\\\pS\\\\.])+(\\\\.?([\\\\pL\\\\pN]|xn\\\\-\\\\-[\\\\pL\\\\pN-]+)+\\\\.?)' + // a domain name
                '|' + // or
            '\\\\d{1,3}\\\\.\\\\d{1,3}\\\\.\\\\d{1,3}\\\\.\\\\d{1,3}' + // an IP address
                '|' + // or
            '\\\\[' +
            '(?:(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){6})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(' +
            '?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\\\\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])' +
            '))))))|(?:(?:::(?:(?:(?:[0-9a-f]{1,4})):){5})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(' +
            '?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\\\\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-' +
            '9])))))))|(?:(?:(?:(?:(?:[0-9a-f]{1,4})))?::(?:(?:(?:[0-9a-f]{1,4})):){4})(?:(?:(?:(?:(?:[0-9a-f]{1,' +
            '4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\\\\.){3}(?:(?:25[0-5' +
            ']|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,1}(?:(?:[0-9a-f]{1,4}))' +
            ')?::(?:(?:(?:[0-9a-f]{1,4})):){3})(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:' +
            '(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\\\\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(' +
            '?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,2}(?:(?:[0-9a-f]{1,4})))?::(?:(?:(?:[0-9a-f]{1,4})):){2})(?:(?:' +
            '(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]' +
            '))\\\\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,3}(' +
            '?:(?:[0-9a-f]{1,4})))?::(?:(?:[0-9a-f]{1,4})):)(?:(?:(?:(?:(?:[0-9a-f]{1,4})):(?:(?:[0-9a-f]{1,4})))' +
            '|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\\\\.){3}(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[' +
            '0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,4}(?:(?:[0-9a-f]{1,4})))?::)(?:(?:(?:(?:(?:[0-9a-f' +
            ']{1,4})):(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:25[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9]))\\\\.){3}(?:(?:25' +
            '[0-5]|(?:[1-9]|1[0-9]|2[0-4])?[0-9])))))))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,5}(?:(?:[0-9a-f]{1,' +
            '4})))?::)(?:(?:[0-9a-f]{1,4})))|(?:(?:(?:(?:(?:(?:[0-9a-f]{1,4})):){0,6}(?:(?:[0-9a-f]{1,4})))?::))))' +
            '\\\\]' + // an IPv6 address
        ')' +
        '(:[0-9]+)?' + // a port (optional)
        '(\\/?|\\/\\\\S+|\\\\?\\\\S*|\\\\#\\\\S*)\$';// a /, nothing, a / with something, a query or a fragment

    /**
     * @export oroform/js/validator/url
     */
    return [
        'Url',
        function(value, element, param) {
            param = Object.assign({}, defaultParam, param);
            const protocols = param.protocols
                .map(protocol => protocol.replace(/[\\-\\[\\]\\/{}()*+?.\\\\^\$|]/g, '\\\\\$&'))
                .join('|');
            const pattern = patternURL.replace('";
        // line 55
        echo twig_escape_filter($this->env, ($context["protocols"] ?? null), "js", null, true);
        echo "', protocols);
            const regexp = new XRegExp(pattern);
            return this.optional(element) || regexp.test(value);
        },
        function(param) {
            param = Object.assign({}, defaultParam, param);
            return __(param.message);
        }
    ];
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validator/url.js";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 55,  52 => 14,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validator/url.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validator/url.js");
    }
}
