<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/variables/app-header.scss */
class __TwigTemplate_d840bd6bb6bd8eef44410c181b87b5d0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$app-header-height: 32px !default;
\$app-header-max-width: 100% !default;
\$app-header-thick-height: 48px !default;
\$app-header-h-padding: \$content-padding !default;
\$app-header-font-size: 14px !default;
\$app-header-title-font-size: 18px !default;
\$app-header-title-line-height: 1 !default;
\$app-header-icon-font-size: 16px !default;
\$app-header-icon-font-width: 24px !default;
\$app-header-color: \$primary !default;
\$app-header-bg-color: \$primary-900 !default;
\$app-header-logo-text-color: \$primary !default;
\$app-header-text-color: \$primary-300 !default;
\$app-header-text-hover-color: \$primary-100 !default;
\$app-header-divider-color: \$primary-800 !default;
\$app-header-divider-width: 1px !default;
\$app-header-search-input-min-width: 220px !default;
\$app-header-search-select-max-width: 200px !default;
\$app-header-dropdown-title-text-transform: none !default;
\$app-header-app-logo-width: 16px !default;
\$app-header-app-logo-margin-end: 8px !default;
\$app-header-logo-wrapper-margin-end: 10px !default;
\$app-header-logo-font-size: 17px !default;
\$app-header-logo-link-font-size: 1.2em !default;
\$app-header-avatar-size: 30px !default;
\$app-header-avatar-border-radius: 50% !default;
\$app-header-avatar-margin: 0 3px 0 12px !default;
\$app-header-search-and-shortcuts-container-padding: 0 5px !default;
\$app-header-search-and-shortcuts-margin: 0 5px !default;
\$app-header-search-and-shortcuts-dropdown-start: -5px !default;
\$app-header-search-and-shortcuts-arrow-start: 12px !default;
\$app-header-search-suggestion-item-font-size: 13px !default;
\$app-header-search-suggestion-item-description-font-size: 14px !default;
\$desktop-app-header-dropdown-menu-padding: \$desktop-content-padding !default;
\$desktop-app-header-user-menu-margin-start: 15px !default;
\$desktop-app-header-user-menu-offset: -12px !default;
\$desktop-app-header-user-menu-item-margin: 0 8px !default;
\$desktop-app-header-user-menu-last-item-margin-end: -3px !default;
\$desktop-app-header-z-index: 925 !default;
\$desktop-app-header-shortcut-dropdown-item-padding: 2px 10px !default;
\$desktop-app-header-search-dropdown-menu-padding: 16px !default;
\$desktop-app-header-user-menu-divider-margin: 8px 0 !default;
\$desktop-app-header-user-menu-divider-border-bottom: 1px solid \$primary-900 !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/variables/app-header.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/variables/app-header.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/variables/app-header.scss");
    }
}
