<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/drag-and-drop-view.scss */
class __TwigTemplate_144741ffdac8db95a414e3d13405b5f0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.drag-n-drop-sorting-view {
    &__wrapper {
        &.disabled {
            .removeRow {
                pointer-events: \$drag-n-drop-sorting-view-wrapper-remove-fow-pointer-events;
            }
        }
    }

    &__column {
        width: \$drag-n-drop-sorting-view-column-width;
    }

    &__column_options {
        min-width: \$drag-n-drop-sorting-view-column-options-min-width;
    }

    .table-bordered {
        display: \$drag-n-drop-sorting-view-table-bordered-display;
    }

    .grid-container {
        margin-bottom: \$drag-n-drop-sorting-view-grid-container-offset-bottom;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/drag-and-drop-view.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/drag-and-drop-view.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/drag-and-drop-view.scss");
    }
}
