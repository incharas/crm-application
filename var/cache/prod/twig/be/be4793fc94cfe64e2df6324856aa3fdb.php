<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSync/ping_js.html.twig */
class __TwigTemplate_459c34447cf7091581ed3603be028f4c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->checkWsConnected()) {
            // line 2
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSync/ping_js.html.twig", 2)->unwrap();
            // line 3
            echo "
<div ";
            // line 4
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orosync/js/app/components/ping-handler"]], 4, $context, $this->getSourceContext());
            // line 6
            echo " ></div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroSync/ping_js.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  46 => 6,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSync/ping_js.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SyncBundle/Resources/views/ping_js.html.twig");
    }
}
