<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/services/progress.js */
class __TwigTemplate_663a636183a3a4b41c993e07725c71cd extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import Backbone from 'backbone';

class Progress {
    constructor(steps) {
        this.todo = steps;
        this.done = 0;
    }

    step() {
        this.done++;
        const progress = this.progress();
        this.trigger('progress', progress);
        if (progress === 100) {
            this.trigger('done');
        }
    }

    progress() {
        const {done, todo} = this;
        return Math.round(done / todo * 100);
    }
}

Object.setPrototypeOf(Progress.prototype, Backbone.Events);

export default Progress;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/services/progress.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/services/progress.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/services/progress.js");
    }
}
