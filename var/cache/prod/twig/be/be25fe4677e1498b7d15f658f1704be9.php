<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/modules/ignore-tabbable.js */
class __TwigTemplate_ef86759be6ffa7cce52bad1f46717e6f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import \$ from 'jquery';
import manageFocus from 'oroui/js/tools/manage-focus';

/**
 * Perverts focus from getting into container with `[data-ignore-tabbable]` attribute
 */
const onFocusin = event => {
    const \$receivingFocus = \$(event.target);
    if (!\$receivingFocus.is('[data-ignore-tabbable] *')) {
        // nothing to do
        return;
    }

    event.preventDefault();
    event.stopImmediatePropagation();

    const allTabbable = \$('body').find(':tabbable');
    const index = allTabbable.toArray().indexOf(event.target);
    const reverse = Boolean(event.relatedTarget) &&
        index < allTabbable.toArray().indexOf(event.relatedTarget);
    if (index !== -1) {
        const tabbables = (reverse ? allTabbable.slice(0, index) : allTabbable.slice(index + 1))
            .not('[data-ignore-tabbable] *').toArray();
        const nextTabbable = reverse
            ? manageFocus.getLastTabbable(tabbables)
            : manageFocus.getFirstTabbable(tabbables);

        if (nextTabbable) {
            \$(nextTabbable).focus();
            if (nextTabbable.nodeName.toLowerCase() === 'input' && typeof nextTabbable.select !== 'undefined') {
                nextTabbable.select();
            }

            // natural blur action for current receivingFocus element
            return;
        }
    }

    \$receivingFocus.blur();
};

document.addEventListener('focusin', onFocusin, true);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/modules/ignore-tabbable.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/modules/ignore-tabbable.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/modules/ignore-tabbable.js");
    }
}
