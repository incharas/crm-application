<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Sync/outdated_js.html.twig */
class __TwigTemplate_512c030449257b8572fc8647a7f8e95d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->checkWsConnected()) {
            // line 2
            echo "    ";
            $context["roles"] = [];
            // line 3
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 3), "userRoles", [], "any", false, false, false, 3));
            foreach ($context['_seq'] as $context["_key"] => $context["role"]) {
                // line 4
                echo "        ";
                $context["roles"] = twig_array_merge(($context["roles"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["role"], "role", [], "any", false, false, false, 4)]);
                // line 5
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['role'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 6
            echo "<script>
    loadModules(['orosync/js/sync', 'oroui/js/modal', 'oroui/js/mediator', 'oroui/js/messenger', 'orotranslation/js/translator'],
    function(sync, Modal, mediator, messenger, __) {
        var notifier = null;
        var sendNotification = true;

        mediator.on('page:beforeChange', function() {
            if (notifier) {
                notifier.close();
            }
            sendNotification = false;
        });

        sync.subscribe('oro/outdated_user_page', function (response) {
            var roles = ";
            // line 20
            echo json_encode(($context["roles"] ?? null));
            echo ";

            if (roles.indexOf(response.role) != -1) {
                if (notifier) {
                    notifier.close();
                }

                if (sendNotification) {
                    notifier = messenger.notificationMessage(
                            'warning',
                            __('oro.role.content_outdated')
                    );
                }
            }
        });
    });
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroUser/Sync/outdated_js.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  72 => 20,  56 => 6,  50 => 5,  47 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Sync/outdated_js.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Sync/outdated_js.html.twig");
    }
}
