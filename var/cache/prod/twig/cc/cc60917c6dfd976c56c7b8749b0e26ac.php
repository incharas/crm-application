<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWindows/states.html.twig */
class __TwigTemplate_5039f0a9c93cc2ea9939c2c71d22e9fc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (twig_length_filter($this->env, ($context["windowStates"] ?? null))) {
            // line 2
            echo "    <div style=\"display: none\" id=\"widget-states-container\" data-layout=\"separate\">
        ";
            // line 3
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["windowStates"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["windowState"]) {
                // line 4
                echo "            <div id=\"widget-restored-state-";
                echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["windowState"], "id", [], "any", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["windowState"], "id", [], "any", false, false, false, 4), "none")) : ("none")), "html", null, true);
                echo "\">
                ";
                // line 5
                echo $this->extensions['Oro\Bundle\WindowsBundle\Twig\WindowsExtension']->renderFragment($context["windowState"]);
                echo "
            </div>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['windowState'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 8
            echo "    </div>
    <script>
        loadModules(['jquery', 'oro/dialog-widget', 'orowindows/js/dialog/state/model'],
        function(\$, DialogWidget, StateModel) {
            \$(function(){
                ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["windowStates"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["windowState"]) {
                // line 14
                echo "                    ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["windowState"], "renderedSuccessfully", [], "any", false, false, false, 14)) {
                    // line 15
                    echo "                        new DialogWidget({
                            autoRender: true,
                            model: new StateModel(";
                    // line 17
                    echo json_encode(["data" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["windowState"], "data", [], "any", false, false, false, 17), "id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["windowState"], "id", [], "any", false, false, false, 17)]);
                    echo ")
                        });
                    ";
                }
                // line 20
                echo "                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['windowState'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 21
            echo "            });
        });
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroWindows/states.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 21,  84 => 20,  78 => 17,  74 => 15,  71 => 14,  67 => 13,  60 => 8,  51 => 5,  46 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWindows/states.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WindowsBundle/Resources/views/states.html.twig");
    }
}
