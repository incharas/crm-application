<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/Form/fields.html.twig */
class __TwigTemplate_45b81df08a4a37a559c1e0b4fd86b774 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_calendar_choice_row' => [$this, 'block_oro_calendar_choice_row'],
            'oro_calendar_choice_template_row' => [$this, 'block_oro_calendar_choice_template_row'],
            'oro_calendar_event_recurrence_row' => [$this, 'block_oro_calendar_event_recurrence_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_calendar_choice_row', $context, $blocks);
        // line 10
        echo "
";
        // line 11
        $this->displayBlock('oro_calendar_choice_template_row', $context, $blocks);
        // line 50
        echo "
";
        // line 51
        $this->displayBlock('oro_calendar_event_recurrence_row', $context, $blocks);
    }

    // line 1
    public function block_oro_calendar_choice_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        if ( !twig_test_empty(($context["choices"] ?? null))) {
            // line 3
            echo "        ";
            if ((twig_length_filter($this->env, ($context["choices"] ?? null)) > 1)) {
                // line 4
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
                echo "
        ";
            } else {
                // line 6
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row', ["label" => false]);
                echo "
        ";
            }
            // line 8
            echo "    ";
        }
    }

    // line 11
    public function block_oro_calendar_choice_template_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    <% var ";
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "Template = '';
    if (";
        // line 13
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "TemplateType === 'single') {
        ";
        // line 14
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "Template = '";
        // line 15
        ob_start(function () { return ''; });
        // line 16
        echo "        <div class=\"control-group\">
            <div class=\"controls\">
                <div id=\"";
        // line 18
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" class=\"horizontal validate-group\">
                    <div class=\"oro-clearfix\">
                        <label for=\"";
        // line 20
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "_0\" class=\"checkbox-label\">
                            <input type=\"checkbox\" id=\"";
        // line 21
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "_0\" name=\"";
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "[]\" value=\"< %- calendars[0].uid % >\">
                            < %- ";
        // line 22
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.add_to_calendar"));
        echo ".replace(\"%name%\", calendars[0].name) % ><em>&nbsp;</em>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        ";
        $___internal_parse_83_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 15
        echo twig_spaceless($___internal_parse_83_);
        // line 29
        echo "';
    } else if (";
        // line 30
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "TemplateType === 'multiple') {
        ";
        // line 31
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "Template = '";
        // line 32
        ob_start(function () { return ''; });
        // line 33
        echo "        <div class=\"control-group\">
            <label data-required=\"1\" class=\"control-label required\" for=\"";
        // line 34
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
        echo "<em>*</em></label>
            <div class=\"controls\">
                <select id=\"";
        // line 36
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, ($context["full_name"] ?? null), "html", null, true);
        echo "\" data-required=\"1\">
                < % for (var i = 0; i < calendars.length; i++) { % >
                    <option value=\"< %- calendars[i].uid % >\">< %- calendars[i].name % ></option>
                < % } % >
                </select>
            </div>
        </div>
        ";
        $___internal_parse_84_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 32
        echo twig_spaceless($___internal_parse_84_);
        // line 44
        echo "';
    } %>
    <% if (";
        // line 46
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "Template) { %>
        <%= _.template(";
        // line 47
        echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
        echo "Template.replace(/\\< %/g, '<' + '%').replace(/% \\>/g, '%' + '>'))({calendars: calendars}) %>
    <% } %>
";
    }

    // line 51
    public function block_oro_calendar_event_recurrence_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/Form/fields.html.twig", 52)->unwrap();
        // line 53
        echo "    ";
        $context["modelAttrs"] = [];
        // line 54
        echo "    ";
        $context["errors"] = [];
        // line 55
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 55));
        foreach ($context['_seq'] as $context["_key"] => $context["field"]) {
            // line 56
            echo "        ";
            if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["field"], "vars", [], "any", false, false, false, 56), "errors", [], "any", false, false, false, 56))) {
                // line 57
                echo "            ";
                $context["fieldErrors"] = [];
                // line 58
                echo "            ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["field"], "vars", [], "any", false, false, false, 58), "errors", [], "any", false, false, false, 58));
                foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                    // line 59
                    echo "                ";
                    $context["fieldErrors"] = twig_array_merge(($context["fieldErrors"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 59)]);
                    // line 60
                    echo "            ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 61
                echo "            ";
                $context["errors"] = twig_array_merge(($context["errors"] ?? null), [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 62
$context["field"], "vars", [], "any", false, false, false, 62), "label", [], "any", false, false, false, 62)), "name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 63
$context["field"], "vars", [], "any", false, false, false, 63), "name", [], "any", false, false, false, 63), "messages" =>                 // line 64
($context["fieldErrors"] ?? null)]]);
                // line 66
                echo "        ";
            }
            // line 67
            echo "        ";
            if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["field"], "vars", [], "any", false, false, false, 67), "value", [], "any", false, false, false, 67))) {
                // line 68
                echo "            ";
                $context["modelAttrs"] = twig_array_merge(($context["modelAttrs"] ?? null), [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["field"], "vars", [], "any", false, false, false, 68), "name", [], "any", false, false, false, 68) => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["field"], "vars", [], "any", false, false, false, 68), "value", [], "any", false, false, false, 68)]);
                // line 69
                echo "        ";
            }
            // line 70
            echo "        ";
            Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["field"], "setRendered", [], "any", false, false, false, 70);
            // line 71
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['field'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 72
        echo "
    ";
        // line 73
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
    <div ";
        // line 74
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orocalendar/js/app/components/calendar-event-recurrence-component", "name" => "calendar-event-recurrence", "options" => ["inputNamePrefixes" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 78
($context["form"] ?? null), "vars", [], "any", false, false, false, 78), "full_name", [], "any", false, false, false, 78), "errors" =>         // line 79
($context["errors"] ?? null), "modelAttrs" =>         // line 80
($context["modelAttrs"] ?? null)]]], 74, $context, $this->getSourceContext());
        // line 82
        echo "></div>
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  259 => 82,  257 => 80,  256 => 79,  255 => 78,  254 => 74,  250 => 73,  247 => 72,  241 => 71,  238 => 70,  235 => 69,  232 => 68,  229 => 67,  226 => 66,  224 => 64,  223 => 63,  222 => 62,  220 => 61,  214 => 60,  211 => 59,  206 => 58,  203 => 57,  200 => 56,  195 => 55,  192 => 54,  189 => 53,  186 => 52,  182 => 51,  175 => 47,  171 => 46,  167 => 44,  165 => 32,  152 => 36,  145 => 34,  142 => 33,  140 => 32,  137 => 31,  133 => 30,  130 => 29,  128 => 15,  118 => 22,  112 => 21,  108 => 20,  103 => 18,  99 => 16,  97 => 15,  94 => 14,  90 => 13,  85 => 12,  81 => 11,  76 => 8,  70 => 6,  64 => 4,  61 => 3,  58 => 2,  54 => 1,  50 => 51,  47 => 50,  45 => 11,  42 => 10,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/Form/fields.html.twig");
    }
}
