<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/font-awesome/_path.scss */
class __TwigTemplate_30c87033ef74c4ee9bf5bad564db096e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@font-face {
    font-family: FontAwesome;
    src:
        url('#{\$fa-font-path}/fontawesome-webfont.woff2') format('woff2'),
        url('#{\$fa-font-path}/fontawesome-webfont.woff') format('woff');
    font-weight: font-weight('normal');
    font-style: normal;
    font-display: swap;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/font-awesome/_path.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/font-awesome/_path.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/font-awesome/_path.scss");
    }
}
