<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/element-state.scss */
class __TwigTemplate_80d4974137bb1f34b5de6da770f77964 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@mixin element-state(\$state) {
    @if \$state == 'active' {
        &:active {
            @content;
        }
    } @else if \$state == 'hover' {
        &:hover {
            @content;
        }
    } @else if \$state == 'visited' {
        &--visited,
        &:visited {
            @content;
        }
    } @else if \$state == 'focus' {
        &--focus,
        &:focus {
            @content;
        }
    } @else if \$state == 'warning' {
        &--warning,
        &.warning {
            @content;
        }
    } @else if \$state == 'success' {
        &--success,
        &.success {
            @content;
        }
    } @else if \$state == 'error' {
        &--error,
        &.error {
            @content;
        }
    } @else if \$state == 'disabled' {
        &--disabled,
        &.disabled,
        &:disabled {
            @content;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/element-state.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/element-state.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/element-state.scss");
    }
}
