<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/lib/simplecolorpicker/jquery.simplecolorpicker-fontawesome.css */
class __TwigTemplate_9c9c611f8eff50ec17f8ceddc9fcd20a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo ".simplecolorpicker.fontawesome span.color[data-selected]:after {
  font-family: 'FontAwesome';
  -webkit-font-smoothing: antialiased;

  content: '\\f00c'; /* Ok/check mark */

  margin-right: 1px;
  margin-left: 1px;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/lib/simplecolorpicker/jquery.simplecolorpicker-fontawesome.css";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/lib/simplecolorpicker/jquery.simplecolorpicker-fontawesome.css", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/lib/simplecolorpicker/jquery.simplecolorpicker-fontawesome.css");
    }
}
