<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Configuration/userConfig.html.twig */
class __TwigTemplate_5b99e2c6ec34388385c8df3c9e5ea57b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroConfig/configPage.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Configuration/userConfig.html.twig", 2)->unwrap();
        // line 3
        $context["fullname"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["entity"] ?? null)), "N/A");

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%username%" =>         // line 4
($context["fullname"] ?? null)]]);
        // line 6
        $context["pageTitle"] = [0 => twig_call_macro($macros["UI"], "macro_link", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_index"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_plural_label")]], 7, $context, $this->getSourceContext()), 1 => twig_call_macro($macros["UI"], "macro_link", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["entity"] ?? null), "id", [], "any", false, false, false, 12)]), "label" =>         // line 13
($context["fullname"] ?? null)]], 11, $context, $this->getSourceContext()), 2 => ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.user_configuration.label") . ((        // line 15
($context["scopeInfo"] ?? null)) ? (twig_call_macro($macros["UI"], "macro_tooltip", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["scopeInfo"] ?? null))], 15, $context, $this->getSourceContext())) : ("")))];
        // line 19
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(        // line 20
($context["routeName"] ?? null), twig_array_merge(        // line 21
($context["routeParameters"] ?? null), ["activeGroup" => ($context["activeGroup"] ?? null), "activeSubGroup" => ($context["activeSubGroup"] ?? null)]));
        // line 24
        $context["routeName"] = ($context["routeName"] ?? null);
        // line 25
        $context["routeParameters"] = ($context["routeParameters"] ?? null);
        // line 1
        $this->parent = $this->loadTemplate("@OroConfig/configPage.html.twig", "@OroUser/Configuration/userConfig.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroUser/Configuration/userConfig.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  61 => 1,  59 => 25,  57 => 24,  55 => 21,  54 => 20,  53 => 19,  51 => 15,  50 => 13,  49 => 12,  48 => 6,  46 => 4,  43 => 3,  41 => 2,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Configuration/userConfig.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Configuration/userConfig.html.twig");
    }
}
