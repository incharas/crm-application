<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/activityLink.html.twig */
class __TwigTemplate_d67f17c7eb1e316695d1554fae9be5a2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'action_controll' => [$this, 'block_action_controll'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["options"] = ["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_email_create", ["to" => ((        // line 4
array_key_exists("email", $context)) ? (twig_escape_filter($this->env, ($context["email"] ?? null), "html")) : ($this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getFullNameEmail(($context["entity"] ?? null)))), "entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 5
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "id", [], "any", false, false, false, 6)]), "aCss" => "dropdown-item no-hash", "iCss" => "fa-envelope", "class" => ((        // line 10
array_key_exists("cssClass", $context)) ? (($context["cssClass"] ?? null)) : ("")), "role" => ((        // line 11
array_key_exists("role", $context)) ? (($context["role"] ?? null)) : ("")), "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["entity"] ?? null), "id", [], "any", false, false, false, 12), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "widget" => ["type" => "dialog", "multiple" => true, "refresh-widget-alias" => "activity-list-widget", "options" => ["alias" => "email-dialog", "method" => "POST", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.send_email"), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]];
        // line 33
        echo "
";
        // line 34
        $this->displayBlock('action_controll', $context, $blocks);
    }

    public function block_action_controll($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 35
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Email/activityLink.html.twig", 35)->unwrap();
        // line 36
        echo "
    ";
        // line 37
        if ($this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isFeatureEnabled("email")) {
            // line 38
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_clientLink", [($context["options"] ?? null)], 38, $context, $this->getSourceContext());
            echo "
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/activityLink.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 38,  62 => 37,  59 => 36,  56 => 35,  49 => 34,  46 => 33,  44 => 12,  43 => 11,  42 => 10,  41 => 6,  40 => 5,  39 => 4,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/activityLink.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/activityLink.html.twig");
    }
}
