<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/dynamic-accessible-button-view.js */
class __TwigTemplate_d8cca334081028fc80e79c952c40a0e6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import \$ from 'jquery';
import BaseView from 'oroui/js/app/views/base/view';

const DynamicAccessibleButtonView = BaseView.extend({
    /**
     * @inheritdoc
     */
    optionNames: BaseView.prototype.optionNames.concat([
        'proxyEvents', 'proxyElement', 'dataAttrName'
    ]),

    /**
     * @inheritdoc
     */
    autoRender: false,

    /**
     * Events to listen to on a proxyElement
     */
    proxyEvents: null,

    /**
     * Specific element on the DOM to listen to
     */
    proxyElement: null,

    /**
     * Specific 'data-*' attribute to read data from
     */
    dataAttrName: 'id',

    /**
     * @inheritdoc
     */
    constructor: function DynamicAccessibleButtonView(options) {
        this.proxyHandler = this.proxyHandler.bind(this);
        DynamicAccessibleButtonView.__super__.constructor.call(this, options);
    },

    /**
     * @inheritdoc
     */
    initialize(options) {
        if (typeof this.proxyEvents === 'string') {
            this.proxyEvents = [this.proxyEvents];
        }
        DynamicAccessibleButtonView.__super__.initialize.call(this, options);
    },

    /**
     * @param {Event} e
     */
    proxyHandler(e) {
        if (\$(e.target).attr(`data-\${this.dataAttrName}`)) {
            this._updateAttributes(\$(e.target).data(this.dataAttrName));
        }
    },

    /**
     * @inheritdoc
     */
    delegateEvents: function() {
        DynamicAccessibleButtonView.__super__.delegateEvents.call(this);

        if (Array.isArray(this.proxyEvents)) {
            this.proxyEvents.forEach(event => {
                \$(this.proxyElement).on(event, this.proxyHandler);
            });
        }

        return this;
    },

    /**
     * @inheritdoc
     */
    undelegateEvents: function() {
        DynamicAccessibleButtonView.__super__.undelegateEvents.call(this);

        if (Array.isArray(this.proxyEvents)) {
            this.proxyEvents.forEach(event => {
                \$(this.proxyElement).off(event, this.proxyHandler);
            });
        }

        return this;
    },

    /**
     * @param {string|number|boolean} predicate
     * @private
     */
    _updateAttributes(predicate) {
        let options = this.\$el.data('attributes');

        if (!Array.isArray(options)) {
            return;
        }

        options = options.find(obj => obj.id === predicate);

        if (options === void 0) {
            return;
        }

        this.\$el.attr('href', options.url);
    }
});

export default DynamicAccessibleButtonView;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/dynamic-accessible-button-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/dynamic-accessible-button-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/dynamic-accessible-button-view.js");
    }
}
