<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/jstree.scss */
class __TwigTemplate_256edc3874dac98c075377a4bfc3aa5b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.jstree-default {
    .jstree-node {
        margin-left: 20px;
    }

    .jstree-wholerow {
        height: \$jstree-mobile-wholerow-height;
    }

    .jstree-wholerow-hovered,
    .jstree-node.jstree-open {
        background: transparent;
    }

    .jstree-wholerow-clicked {
        background: \$tree-link-bg-color;
    }

    .jstree-open > .jstree-ocl::before,
    .jstree-closed > .jstree-ocl::before {
        font-size: \$base-font-size--l;
    }

    .jstree-anchor {
        font-size: \$jstree-mobile-anchor-font-size;
        padding: \$jstree-mobile-anchor-inner-offset;
        line-height: \$jstree-mobile-anchor-line-height;
        height: \$jstree-mobile-wholerow-height;

        &.jstree-clicked {
            font-weight: font-weight('bold');
        }
    }

    .jstree-icon.jstree-ocl {
        line-height: \$jstree-mobile-wholerow-height;
        height: \$jstree-mobile-wholerow-height;
    }
}

.controls {
    .jstree-wrapper {
        max-width: 100%;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/jstree.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/jstree.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/jstree.scss");
    }
}
