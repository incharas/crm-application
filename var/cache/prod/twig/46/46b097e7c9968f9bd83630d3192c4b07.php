<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/lib/minicolors/readme.md */
class __TwigTemplate_0605675f482f58754eeed2e0c272faae extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "# jQuery MiniColors: A tiny color picker built on jQuery

_Copyright Cory LaViska for A Beautiful Site, LLC. (http://www.abeautifulsite.net/)_

_Licensed under the MIT license_

## Demo & Documentation

http://labs.abeautifulsite.net/jquery-minicolors/";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/lib/minicolors/readme.md";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/lib/minicolors/readme.md", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/lib/minicolors/readme.md");
    }
}
