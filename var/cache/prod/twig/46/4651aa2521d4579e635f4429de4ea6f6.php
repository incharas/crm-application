<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/pagination.scss */
class __TwigTemplate_dae505950e95725b44906ae9ead30c5b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import '~@oroinc/bootstrap/scss/pagination';

.page-link {
    border: none;

    [class^='fa-'],
    [class*=' fa-'] {
        &.hide-text {
            color: \$pagination-fa-color;

            &::before,
            &::after {
                font-size: px2rem(16px);
            }
        }
    }

    &:hover {
        [class^='fa-'],
        [class*=' fa-'] {
            &.hide-text {
                color: \$pagination-fa-hover-color;
            }
        }
    }

    &:active {
        [class^='fa-'],
        [class*=' fa-'] {
            &.hide-text {
                color: \$pagination-fa-active-color;
            }
        }
    }

    &:disabled,
    &.disabled {
        [class^='fa-'],
        [class*=' fa-'] {
            &.hide-text {
                color: \$pagination-fa-disabled-color;
            }
        }
    }
}

.pagination {
    margin: 0;
    align-items: center;

    &--mini {
        font-size: \$base-font-size--s;

        .page-link {
            padding: \$pagination-mini-padding-y \$pagination-mini-padding-x;

            [class^='fa-'],
            [class*=' fa-'] {
                &.hide-text {
                    &::before,
                    &::after {
                        font-size: px2rem(14px);
                    }
                }
            }
        }
    }

    &-previous {
        margin-right: 4px;
    }

    &-next {
        margin-left: 4px;
    }
}

.page-item {
    &.disabled .page-link {
        opacity: .5;
    }
}

.page-current {
    position: relative;
    display: block;
    padding: \$pagination-current-padding-y \$pagination-current-padding-x;
    margin-left: -\$pagination-border-width;
    line-height: \$pagination-line-height;
    color: \$pagination-color;
}

.page-input {
    width: 40px;
    margin: 0;
    text-align: center;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/pagination.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/pagination.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/pagination.scss");
    }
}
