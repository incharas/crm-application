<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/inline-editing-validation.scss */
class __TwigTemplate_bfcd7bed2b86adae7fca9db1ed0a96b5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.editable {
    span.validation-failed {
        @include floating-validation-message();

        display: block;
        white-space: nowrap;
        position: absolute;
        top: -6px;
        bottom: auto;

        > span {
            position: absolute;
            /* stylelint-disable declaration-no-important */
            top: auto !important;
            left: auto !important;
            /* stylelint-enable declaration-no-important */
            margin-top: 0;
            text-align: left;
        }

        .validation-failed__icon {
            overflow: hidden;
        }

        &::after {
            border-width: 6px 4px 0;
            margin-top: 0;
        }
    }

    &.error-message-below {
        .validation-failed {
            bottom: -7px;
            top: auto;

            > span {
                > span {
                    top: 0;
                    bottom: auto;
                }
            }

            &::after {
                border-width: 0 4px 6px;
                top: auto;
                bottom: 100%;
            }
        }
    }

    &.error-message-right {
        .validation-failed {
            bottom: 0;
            top: auto;
            left: 100%;
            margin-left: 6px;

            &::after {
                border-width: 4px 6px 4px 0;
                border-color: transparent \$error-message-balloon-background;
                top: -16px;
                left: -6px;
            }
        }
    }

    &.error-message-left {
        .validation-failed {
            bottom: 0;
            top: auto;
            right: 100%;
            margin-right: 6px;

            /* stylelint-disable no-descending-specificity */
            > span {
                margin-left: -280px;

                > span {
                    right: 0;
                }
            }
            /* stylelint-enable no-descending-specificity */

            &::after {
                border-width: 4px 0 4px 6px;
                border-color: transparent \$error-message-balloon-background;
                top: -16px;
                right: -6px;
                left: auto;
            }
        }
    }

    .error-holder {
        position: absolute;
        display: none;
        top: 0;
        left: 0;
        right: 0;
        z-index: 1;
    }

    &.error-message-below .error-holder {
        top: auto;
        bottom: 0;
    }

    &.error-message-left,
    &.error-message-right {
        .error-holder {
            top: 24px;
        }
    }

    &.view-mode:hover .error-holder {
        display: block;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/inline-editing-validation.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/inline-editing-validation.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/inline-editing-validation.scss");
    }
}
