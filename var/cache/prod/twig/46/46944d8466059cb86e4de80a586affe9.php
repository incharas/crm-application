<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/drag-n-drop-sorting.js */
class __TwigTemplate_63d471b88b49f48be733221994b92568 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const BaseView = require('oroui/js/app/views/base/view');
    require('jquery-ui/widgets/sortable');

    const DraggableSortingView = BaseView.extend({
        /**
         * @inheritdoc
         */
        constructor: function DraggableSortingView(options) {
            DraggableSortingView.__super__.constructor.call(this, options);
        },

        render: function() {
            this.initSortable();
            this.reindexValues();
            return this;
        },

        reindexValues: function() {
            let index = 1;
            this.\$('[name\$=\"[_position]\"]').each(function() {
                \$(this).val(index++);
            });
        },

        initSortable: function() {
            this.\$('.sortable-wrapper').sortable({
                tolerance: 'pointer',
                delay: 100,
                containment: 'parent',
                handle: '[data-name=\"sortable-handle\"]',
                stop: this.reindexValues.bind(this)
            });
        }
    });

    return DraggableSortingView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/drag-n-drop-sorting.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/drag-n-drop-sorting.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/drag-n-drop-sorting.js");
    }
}
