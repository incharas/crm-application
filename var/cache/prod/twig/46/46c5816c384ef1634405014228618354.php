<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/templates/dropdown-select.html */
class __TwigTemplate_6412a21fa66090b836e8285d93a80565 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% var togglerId = _.uniqueId('dropdown-') %>
<% if (useButtonGroup) { %>
    <div class=\"dropdown btn-group\">
<% } %>
        <a href=\"#\" role=\"button\" id=\"<%- togglerId %>\" class=\"<%- buttonClass %> dropdown-toggle<% if (!useCaret) { %> dropdown-toggle--no-caret<% } %>\" data-toggle=\"dropdown\"
           aria-haspopup=\"true\" aria-expanded=\"false\"<% if (label) { %> aria-label=\"<%- label %>\"<% } %>>
            <span class=\"current-label\"><%- selectedLabel %></span>
        </a>
        <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"<%- togglerId %>\">
            <% _.each(options, function (option) { %>
                <li<% if (option.selected) { %> class=\"selected\"<% } %>>
                    <a href=\"#\" data-value=\"<%- option.value %>\"<% if (option.selected) { %> aria-selected=\"true\"<% } %>><%- option.label %></a>
                </li>
            <% }); %>
        </ul>
<% if (useButtonGroup) { %>
    </div>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/templates/dropdown-select.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/templates/dropdown-select.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/templates/dropdown-select.html");
    }
}
