<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/Widget/widget/transitionForm.html.twig */
class __TwigTemplate_30d9f73f9cbf0b2bd6519aede38f2889 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'transition_widget_class' => [$this, 'block_transition_widget_class'],
            'transition_widget' => [$this, 'block_transition_widget'],
            'transition_form' => [$this, 'block_transition_form'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/Widget/widget/transitionForm.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        if (($context["saved"] ?? null)) {
            // line 4
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["trigger" => [0 => ["eventBroker" => "widget", "name" => "formSave", "args" => [0 => ((            // line 9
array_key_exists("data", $context)) ? (_twig_default_filter(($context["data"] ?? null), null)) : (null))]]]]];
            // line 13
            echo "
    ";
            // line 14
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
";
        } else {
            // line 16
            echo "    <div class=\"";
            $this->displayBlock('transition_widget_class', $context, $blocks);
            echo "\">
        ";
            // line 17
            $this->displayBlock('transition_widget', $context, $blocks);
            // line 54
            echo "    </div>
";
        }
    }

    // line 16
    public function block_transition_widget_class($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "widget-content";
    }

    // line 17
    public function block_transition_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "            ";
        if ((twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 18), "errors", [], "any", false, false, false, 18)) > 0)) {
            // line 19
            echo "                <div class=\"alert alert-error\" role=\"alert\" ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroworkflow/js/app/views/transition/transition-error-view", "options" => ["wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 22
($context["app"] ?? null), "request", [], "any", false, false, false, 22), "get", [0 => "_wid"], "method", false, false, false, 22)]]], 19, $context, $this->getSourceContext());
            // line 24
            echo ">
                    ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
                </div>
            ";
        }
        // line 28
        echo "
            ";
        // line 29
        $this->displayBlock('transition_form', $context, $blocks);
        // line 53
        echo "        ";
    }

    // line 29
    public function block_transition_form($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 30
        echo "                ";
        $context["frontendMessage"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, false, false, 30), "message", [], "any", false, false, false, 30);
        // line 31
        echo "                ";
        $context["transitionMessage"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "content", [], "any", false, false, false, 31), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "message_parameters", [], "any", false, false, false, 31), "workflows");
        // line 32
        echo "                ";
        if ((($context["transitionMessage"] ?? null) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["frontendMessage"] ?? null), "content", [], "any", false, false, false, 32))) {
            // line 33
            echo "                    ";
            // line 34
            echo "                    ";
            $context["transitionMessage"] = "";
            // line 35
            echo "                ";
        }
        // line 36
        echo "
                ";
        // line 37
        if (($context["transitionMessage"] ?? null)) {
            // line 38
            echo "                    <div class=\"alert\" role=\"alert\">";
            echo twig_escape_filter($this->env, ($context["transitionMessage"] ?? null), "html", null, true);
            echo "</div>
                ";
        }
        // line 40
        echo "                <form method=\"post\"
                      data-nohash=\"true\"
                      data-disable-autofocus=\"true\"
                      ";
        // line 43
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "displayType", [], "any", false, false, false, 43) != "dialog")) {
            echo "data-collect=\"true\"";
        }
        // line 44
        echo "                      id=\"";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 44), "id", [], "any", false, false, false, 44), "html", null, true);
        echo "\"
                      name=\"";
        // line 45
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 45), "name", [], "any", false, false, false, 45), "html", null, true);
        echo "\"
                      action=\"";
        // line 46
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 46), "uri", [], "any", false, false, false, 46), "html", null, true);
        echo "\"
                      class=\"form-dialog oro-workflow-transition-form\"
                      ";
        // line 48
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, true, false, 48), "pageComponent", [], "any", true, true, false, 48)) {
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["transition"] ?? null), "frontendOptions", [], "any", false, false, false, 48), "pageComponent", [], "any", false, false, false, 48)], 48, $context, $this->getSourceContext());
        }
        echo ">
                    ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'row');
        echo "
                </form>
                ";
        // line 51
        echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
        echo "
            ";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/Widget/widget/transitionForm.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 51,  169 => 49,  163 => 48,  158 => 46,  154 => 45,  149 => 44,  145 => 43,  140 => 40,  134 => 38,  132 => 37,  129 => 36,  126 => 35,  123 => 34,  121 => 33,  118 => 32,  115 => 31,  112 => 30,  108 => 29,  104 => 53,  102 => 29,  99 => 28,  93 => 25,  90 => 24,  88 => 22,  86 => 19,  83 => 18,  79 => 17,  72 => 16,  66 => 54,  64 => 17,  59 => 16,  54 => 14,  51 => 13,  49 => 9,  47 => 4,  45 => 3,  42 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/Widget/widget/transitionForm.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/Widget/widget/transitionForm.html.twig");
    }
}
