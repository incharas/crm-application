<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityPagination/Placeholder/entityPagination.html.twig */
class __TwigTemplate_4c70493edb2e7639622410f4e6cc06c5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityPagination/Placeholder/entityPagination.html.twig", 1)->unwrap();
        // line 2
        $context["first_route"] = ((array_key_exists("first_route", $context)) ? (_twig_default_filter(($context["first_route"] ?? null), "oro_entity_pagination_first")) : ("oro_entity_pagination_first"));
        // line 3
        $context["previous_route"] = ((array_key_exists("previous_route", $context)) ? (_twig_default_filter(($context["previous_route"] ?? null), "oro_entity_pagination_previous")) : ("oro_entity_pagination_previous"));
        // line 4
        $context["next_route"] = ((array_key_exists("next_route", $context)) ? (_twig_default_filter(($context["next_route"] ?? null), "oro_entity_pagination_next")) : ("oro_entity_pagination_next"));
        // line 5
        $context["last_route"] = ((array_key_exists("last_route", $context)) ? (_twig_default_filter(($context["last_route"] ?? null), "oro_entity_pagination_last")) : ("oro_entity_pagination_last"));
        // line 6
        echo "
";
        // line 7
        if ((array_key_exists("entity", $context) && ($context["entity"] ?? null))) {
            // line 8
            echo "    ";
            $context["isDataCollected"] = $this->extensions['Oro\Bundle\EntityPaginationBundle\Twig\EntityPaginationExtension']->collectData(($context["scope"] ?? null));
            // line 9
            echo "    ";
            $context["pager"] = $this->extensions['Oro\Bundle\EntityPaginationBundle\Twig\EntityPaginationExtension']->getPager(($context["entity"] ?? null), ($context["scope"] ?? null));
            // line 10
            echo "
    ";
            // line 11
            if ((($context["isDataCollected"] ?? null) && ($context["pager"] ?? null))) {
                // line 12
                echo "        ";
                $context["infoMessageShown"] = $this->extensions['Oro\Bundle\EntityPaginationBundle\Twig\EntityPaginationExtension']->showInfoMessage(($context["entity"] ?? null), ($context["scope"] ?? null));
                // line 13
                echo "        ";
                $context["currentRoute"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 13), "attributes", [], "any", false, false, false, 13), "get", [0 => "_route"], "method", false, false, false, 13);
                // line 14
                echo "        ";
                $context["currentParams"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 14), "attributes", [], "any", false, false, false, 14), "get", [0 => "_route_params"], "method", false, false, false, 14);
                // line 15
                echo "        ";
                $context["queryParams"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 15), "query", [], "any", false, false, false, 15), "all", [], "any", false, false, false, 15);
                // line 16
                echo "        ";
                $context["allParams"] = ["_entityName" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(($context["entity"] ?? null), true), "_scope" => ($context["scope"] ?? null), "_routeName" => ($context["currentRoute"] ?? null)];
                // line 17
                echo "        ";
                $context["allParams"] = twig_array_merge(($context["allParams"] ?? null), ($context["currentParams"] ?? null));
                // line 18
                echo "        ";
                $context["allParams"] = twig_array_merge(($context["allParams"] ?? null), ($context["queryParams"] ?? null));
                // line 19
                echo "
        ";
                // line 20
                $context["componentName"] = "oroui/js/app/components/hidden-redirect-component";
                // line 21
                echo "        ";
                $context["componentOptions"] = ["type" => "warning"];
                // line 22
                echo "        
        <div id=\"entity-pagination\" class=\"entity-pagination\" ";
                // line 23
                if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()) {
                    // line 24
                    echo "            ";
                    echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/sticky-element/sticky-element-view", "options" => ["stickyOptions" => ["enabled" => true, "relativeTo" => "body"]]]], 24, $context, $this->getSourceContext());
                    // line 32
                    echo "
        ";
                }
                // line 33
                echo ">
            <ul class=\"pagination pagination--mini\">
                <li class=\"page-item";
                // line 35
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "current", [], "any", false, false, false, 35) == 1)) {
                    echo " disabled";
                }
                echo "\">
                    <a href=\"";
                // line 36
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["first_route"] ?? null), ($context["allParams"] ?? null)), "html", null, true);
                echo "\"
                       class=\"page-link\" 
                       data-page-component-module=\"";
                // line 38
                echo twig_escape_filter($this->env, ($context["componentName"] ?? null), "html", null, true);
                echo "\"
                       data-page-component-options=\"";
                // line 39
                echo twig_escape_filter($this->env, json_encode(($context["componentOptions"] ?? null)), "html", null, true);
                echo "\">
                        ";
                // line 40
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_pagination.first"), "html", null, true);
                echo "
                    </a>
                </li>
                <li class=\"page-item";
                // line 43
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "current", [], "any", false, false, false, 43) == 1)) {
                    echo " disabled";
                }
                echo "\">
                    <a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["previous_route"] ?? null), ($context["allParams"] ?? null)), "html", null, true);
                echo "\"
                       class=\"page-link\"
                       data-page-component-module=\"";
                // line 46
                echo twig_escape_filter($this->env, ($context["componentName"] ?? null), "html", null, true);
                echo "\"
                       data-page-component-options=\"";
                // line 47
                echo twig_escape_filter($this->env, json_encode(($context["componentOptions"] ?? null)), "html", null, true);
                echo "\">
                        <i class=\"fa-chevron-left hide-text\"></i>
                    </a>
                </li>
                <li class=\"page-item\">
                    <div class=\"page-current\">";
                // line 52
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "current", [], "any", false, false, false, 52), "html", null, true);
                echo "</div>
                </li>
                <li class=\"page-item";
                // line 54
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "current", [], "any", false, false, false, 54) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "total", [], "any", false, false, false, 54))) {
                    echo " disabled";
                }
                echo "\">
                    <a href=\"";
                // line 55
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["next_route"] ?? null), ($context["allParams"] ?? null)), "html", null, true);
                echo "\"
                       class=\"page-link\"
                       data-page-component-module=\"";
                // line 57
                echo twig_escape_filter($this->env, ($context["componentName"] ?? null), "html", null, true);
                echo "\"
                       data-page-component-options=\"";
                // line 58
                echo twig_escape_filter($this->env, json_encode(($context["componentOptions"] ?? null)), "html", null, true);
                echo "\">
                        <i class=\"fa-chevron-right hide-text\"></i>
                    </a>
                </li>
                <li class=\"page-item";
                // line 62
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "current", [], "any", false, false, false, 62) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "total", [], "any", false, false, false, 62))) {
                    echo " disabled";
                }
                echo "\">
                    <a href=\"";
                // line 63
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["last_route"] ?? null), ($context["allParams"] ?? null)), "html", null, true);
                echo "\"
                       class=\"page-link\"
                       data-page-component-module=\"";
                // line 65
                echo twig_escape_filter($this->env, ($context["componentName"] ?? null), "html", null, true);
                echo "\"
                       data-page-component-options=\"";
                // line 66
                echo twig_escape_filter($this->env, json_encode(($context["componentOptions"] ?? null)), "html", null, true);
                echo "\">
                        ";
                // line 67
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_pagination.last"), "html", null, true);
                echo "
                    </a>
                </li>
            </ul>

            <div class=\"entity-pagination_total\">
                ";
                // line 73
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.entity_pagination.pager_of_%count%_record|pager_of_%count%_records", ["%count%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pager"] ?? null), "total", [], "any", false, false, false, 73)]), "html", null, true);
                echo "
            </div>
        </div>
    ";
            }
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityPagination/Placeholder/entityPagination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 73,  202 => 67,  198 => 66,  194 => 65,  189 => 63,  183 => 62,  176 => 58,  172 => 57,  167 => 55,  161 => 54,  156 => 52,  148 => 47,  144 => 46,  139 => 44,  133 => 43,  127 => 40,  123 => 39,  119 => 38,  114 => 36,  108 => 35,  104 => 33,  100 => 32,  97 => 24,  95 => 23,  92 => 22,  89 => 21,  87 => 20,  84 => 19,  81 => 18,  78 => 17,  75 => 16,  72 => 15,  69 => 14,  66 => 13,  63 => 12,  61 => 11,  58 => 10,  55 => 9,  52 => 8,  50 => 7,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityPagination/Placeholder/entityPagination.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityPaginationBundle/Resources/views/Placeholder/entityPagination.html.twig");
    }
}
