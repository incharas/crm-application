<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/jstree-actions.scss */
class __TwigTemplate_e57087c3ef2b217adf579da54257379f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:color';

\$jstree-actions-display: none !default;

\$jstree-actions-handle-offset: 0 3px !default;
\$jstree-actions-handle-color: \$primary-400 !default;

\$jstree-actions-handle-hover-color: color.adjust(\$jstree-actions-handle-color, \$lightness: -10%) !default;

\$jstree-actions-menu-position: absolute !default;

\$jstree-actions-menu-inline-margin: 0 !default;
\$jstree-actions-menu-inline-list-style: none !default;

\$jstree-actions-menu-item-gap: 8px;
\$jstree-actions-menu-inline-li-display: flex !default;
\$jstree-actions-menu-inline-li-align-items: center !default;

\$jstree-actions-menu-inline-li-before-content: '' !default;
\$jstree-actions-menu-inline-li-before-width: 1px !default;
\$jstree-actions-menu-inline-li-before-height: 14px !default;
\$jstree-actions-menu-separator-color: \$primary-750 !default;

\$jstree-actions-icon-text-align: center !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/jstree-actions.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/jstree-actions.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/jstree-actions.scss");
    }
}
