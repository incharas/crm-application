<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/widget-picker.scss */
class __TwigTemplate_c2de4cb2d79c54626f3dd41aa51a6ea2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.widget-picker {
    &__description-toggler {
        height: \$widget-picker-icon-height;
        width: \$widget-picker-icon-width;

        .collapse-action-icon {
            height: \$btn-mobile-font-size;
        }
    }
}

@media only screen and (max-width: \$medium-devices-media-breakpoint) {
    .widget-picker {
        &__modal .modal-body {
            display: flex;
            flex-direction: column;
            padding-bottom: 0;
        }

        &__filter {
            display: block;
            flex-grow: 0;
            flex-shrink: 0;
        }

        &__container {
            max-height: none;
            overflow: auto;
        }

        &__filter.empty::after,
        &__filter-clear {
            margin: 4px;
        }

        &__item {
            flex-wrap: wrap;
            position: relative;
            padding-bottom: \$widget-picker-mobile-item-padding-bottom;
        }

        &__summary-row {
            margin-bottom: \$widget-picker-mobile-summary-row-margin-bottom;
        }

        &__item.loading .widget-picker__actions-column::after {
            margin-right: \$content-padding;
        }

        &__toggler-column {
            width: auto;
            padding-right: \$content-padding;
            padding-bottom: 0;
            border-bottom: 0 none;
        }

        &__icon-column {
            display: none;
        }

        &__description-toggler {
            width: \$widget-picker-description-collapse-font-size;
            height: auto;

            .collapse-action-icon {
                height: auto;
            }
        }

        &__info-column {
            flex-grow: 1;
            flex-basis: 0;
            padding-right: 0;
            padding-bottom: 0;
            border-bottom: 0 none;
        }

        &__title-cell {
            display: flex;
            align-items: center;
        }

        &__title-text {
            flex-grow: 1;
            margin-bottom: 0;
        }

        &__description-cell {
            margin: \$widget-picker-mobile-description-cell-margin;
        }

        &__added-badge {
            white-space: nowrap;
        }

        &__actions-column {
            padding-right: 0;
            position: absolute;
            bottom: \$widget-picker-mobile-actions-column-bottom;
            width: 100%;
            text-align: right;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/widget-picker.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/widget-picker.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/widget-picker.scss");
    }
}
