<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/dropdown.html.twig */
class __TwigTemplate_8369040dd37104857dd44e624615eb30 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'root' => [$this, 'block_root'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/dropdown.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $context["listAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "childrenAttributes", [], "any", false, false, false, 4);
        // line 5
        echo "    ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/dropdown.html.twig", 5)->unwrap();
        // line 6
        echo "    ";
        $context["listAttributes"] = twig_array_merge(($context["listAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [        // line 7
($context["listAttributes"] ?? null), "class", twig_array_merge([0 => "dropdown-menu"], [0 => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "class", [], "any", true, true, false, 7)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "class", [], "any", false, false, false, 7), "")) : (""))])], 7, $context, $this->getSourceContext()), "role" => "menu"]);
        // line 10
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "togglerId", [], "any", true, true, false, 10)) {
            // line 11
            echo "        ";
            $context["listAttributes"] = twig_array_merge(($context["listAttributes"] ?? null), ["aria-labelledby" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 12
($context["options"] ?? null), "togglerId", [], "any", false, false, false, 12)]);
            // line 14
            echo "    ";
        }
        // line 15
        echo "    ";
        $this->displayBlock("list", $context, $blocks);
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/dropdown.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  70 => 15,  67 => 14,  65 => 12,  63 => 11,  60 => 10,  58 => 7,  56 => 6,  53 => 5,  50 => 4,  46 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/dropdown.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/dropdown.html.twig");
    }
}
