<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/multiple-entity/templates/multiple-entities.html */
class __TwigTemplate_b2737c0a02d29b0c6c92c206c5a76780 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"actions clearfix\">
    <div class=\"clearfix\">
        <div class=\"pull-right\">
            <button type=\"button\" class=\"btn btn-medium add-btn\" data-purpose=\"open-dialog-widget\"><%- _.__('Add') %></button>
        </div>
    </div>
    <div class=\"entities list-group\"></div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/multiple-entity/templates/multiple-entities.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/multiple-entity/templates/multiple-entities.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/multiple-entity/templates/multiple-entities.html");
    }
}
