<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/simple-color-choice-view.js */
class __TwigTemplate_42a0e902388816d15bca1bf15e2a2a70 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(['underscore', 'oroui/js/app/views/base/view', 'jquery.simplecolorpicker'
], function(_, BaseView) {
    'use strict';

    const SimpleColorChoiceView = BaseView.extend({
        events: {
            enable: 'enable',
            disable: 'disable'
        },

        /**
         * @inheritdoc
         */
        constructor: function SimpleColorChoiceView(options) {
            SimpleColorChoiceView.__super__.constructor.call(this, options);
        },

        /**
         * @constructor
         * @param {Object} options
         */
        initialize: function(options) {
            this.\$el.simplecolorpicker(_.defaults(_.omit(options, ['el']), {
                emptyColor: '#FFFFFF'
            }));
        },
        /**
         * @inheritdoc
         */
        dispose: function() {
            if (this.disposed) {
                return;
            }
            if (this.\$el.data('simplecolorpicker')) {
                this.\$el.simplecolorpicker('destroy');
            }
            SimpleColorChoiceView.__super__.dispose.call(this);
        },

        enable: function() {
            this.\$el.simplecolorpicker('enable');
        },

        disable: function() {
            this.\$el.simplecolorpicker('enable', false);
        }
    });

    return SimpleColorChoiceView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/simple-color-choice-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/simple-color-choice-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/simple-color-choice-view.js");
    }
}
