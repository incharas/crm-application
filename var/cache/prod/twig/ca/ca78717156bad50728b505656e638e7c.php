<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/layouts/default/default.html.twig */
class __TwigTemplate_96bee74f07147ce55a16eb27226225f1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'datagrid_widget' => [$this, 'block_datagrid_widget'],
            'taggable_datagrid_widget' => [$this, 'block_taggable_datagrid_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["grid_parameters"] = twig_array_merge(["enableFullScreenLayout" => true], ($context["grid_parameters"] ?? null));
        // line 2
        $this->displayBlock('datagrid_widget', $context, $blocks);
        // line 22
        echo "
";
        // line 23
        $this->displayBlock('taggable_datagrid_widget', $context, $blocks);
    }

    // line 2
    public function block_datagrid_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        echo "    ";
        if ((($context["split_to_cells"] ?? null) != false)) {
            // line 4
            echo "        ";
            $context["themeOptions"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["grid_render_parameters"] ?? null), "themeOptions", [], "any", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["grid_render_parameters"] ?? null), "themeOptions", [], "any", false, false, false, 4), [])) : ([]));
            // line 5
            echo "        ";
            $context["themeOptions"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["themeOptions"] ?? null), ["headerRowTemplateSelector" => "#template-datagrid-header-row", "rowTemplateSelector" => "#template-datagrid-row"]);
            // line 9
            echo "        ";
            $context["toolbarOptions"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["grid_render_parameters"] ?? null), "toolbarOptions", [], "any", true, true, false, 9)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["grid_render_parameters"] ?? null), "toolbarOptions", [], "any", false, false, false, 9), [])) : ([]));
            // line 10
            echo "        ";
            $context["toolbarOptions"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["toolbarOptions"] ?? null), ["datagridSettings" => ["addSorting" => false]]);
            // line 13
            echo "        ";
            $context["grid_render_parameters"] = twig_array_merge(($context["grid_render_parameters"] ?? null), ["themeOptions" =>             // line 14
($context["themeOptions"] ?? null), "toolbarOptions" =>             // line 15
($context["toolbarOptions"] ?? null)]);
            // line 17
            echo "        ";
            $this->displayBlock("container_widget", $context, $blocks);
            echo "
    ";
        }
        // line 19
        echo "    ";
        $macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroDataGrid/layouts/default/default.html.twig", 19)->unwrap();
        // line 20
        echo "    ";
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["grid_full_name"] ?? null), ($context["grid_parameters"] ?? null), ($context["grid_render_parameters"] ?? null)], 20, $context, $this->getSourceContext());
        echo "
";
    }

    // line 23
    public function block_taggable_datagrid_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "    ";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "

    ";
        // line 26
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDataGrid/layouts/default/default.html.twig", 26)->unwrap();
        // line 27
        echo "
    <div class=\"hidden\" ";
        // line 28
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orodatagrid/js/app/components/datagrid-allow-tracking-component", "options" => ["gridName" =>         // line 31
($context["grid_full_name"] ?? null)]]], 28, $context, $this->getSourceContext());
        // line 33
        echo "></div>
";
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/layouts/default/default.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  108 => 33,  106 => 31,  105 => 28,  102 => 27,  100 => 26,  94 => 24,  90 => 23,  83 => 20,  80 => 19,  74 => 17,  72 => 15,  71 => 14,  69 => 13,  66 => 10,  63 => 9,  60 => 5,  57 => 4,  54 => 3,  50 => 2,  46 => 23,  43 => 22,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/layouts/default/default.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/layouts/default/default.html.twig");
    }
}
