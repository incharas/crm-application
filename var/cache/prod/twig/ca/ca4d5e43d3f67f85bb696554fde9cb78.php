<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCall/Datagrid/Column/subject.html.twig */
class __TwigTemplate_050b1c7bf283f7e486b3eaa96831dea1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($context["value"] ?? null)) {
            // line 2
            echo "    ";
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCall/Datagrid/Column/subject.html.twig", 2)->unwrap();
            // line 3
            echo "
    ";
            // line 4
            $context["callId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "id"], "method", false, false, false, 4);
            // line 5
            echo "    ";
            echo twig_call_macro($macros["UI"], "macro_clientLink", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_call_widget_info", ["id" =>             // line 6
($context["callId"] ?? null)]), "aCss" => "no-hash", "label" =>             // line 8
($context["value"] ?? null), "widget" => ["type" => "dialog", "multiple" => false, "options" => ["alias" => ("call_info_widget_" .             // line 13
($context["callId"] ?? null)), "dialogOptions" => ["title" =>             // line 15
($context["value"] ?? null), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 600]]]]], 5, $context, $this->getSourceContext());
            // line 24
            echo "
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCall/Datagrid/Column/subject.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  54 => 24,  52 => 15,  51 => 13,  50 => 8,  49 => 6,  47 => 5,  45 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCall/Datagrid/Column/subject.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-call-bundle/Resources/views/Datagrid/Column/subject.html.twig");
    }
}
