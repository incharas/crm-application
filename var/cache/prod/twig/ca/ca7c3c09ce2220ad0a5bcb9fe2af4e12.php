<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/AutoResponseRule/dialog/update.html.twig */
class __TwigTemplate_7d9a2456848f8ed344b4c16948e49d97 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/AutoResponseRule/dialog/update.html.twig", 1)->unwrap();
        // line 2
        $macros["QD"] = $this->macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroEmail/AutoResponseRule/dialog/update.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        if (($context["saved"] ?? null)) {
            // line 5
            echo "    ";
            $context["widgetResponse"] = ["widget" => ["triggerSuccess" => true, "trigger" => [0 => ["name" => "auto_response_rule:save", "args" => [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "value", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)]]], "remove" => true]];
            // line 15
            echo "
    ";
            // line 16
            echo json_encode(($context["widgetResponse"] ?? null));
            echo "
";
        } else {
            // line 18
            echo "    <div class=\"widget-content\">
        ";
            // line 19
            $context["pageComponent"] = ["view" => "oroemail/js/app/views/email-template-editor-view", "layout" => "separate"];
            // line 23
            echo "        <div class=\"form-container\" ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["pageComponent"] ?? null)], 23, $context, $this->getSourceContext());
            echo ">
            ";
            // line 24
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(            // line 25
($context["form"] ?? null), 'form_start', ["attr" => ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 28
($context["form"] ?? null), "vars", [], "any", false, false, false, 28), "id", [], "any", false, false, false, 28)], "action" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["app"] ?? null), "request", [], "any", false, false, false, 31), "attributes", [], "any", false, false, false, 31), "get", [0 => "_route"], "method", false, false, false, 31), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 32
($context["app"] ?? null), "request", [], "any", false, false, false, 32), "attributes", [], "any", false, false, false, 32), "get", [0 => "_route_params"], "method", false, false, false, 32))]);
            // line 35
            echo "
                <fieldset class=\"form form-horizontal\">
                    <div>
                        ";
            // line 38
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "active", [], "any", false, false, false, 38), 'row');
            echo "
                        ";
            // line 39
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 39), 'row');
            echo "
                        <div class=\"control-group\">
                            <label class=\"required control-label\">
                                ";
            // line 42
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.autoresponserule.conditions.label"), "html", null, true);
            echo "
                            </label>
                            <div class=\"controls\">
                                ";
            // line 45
            echo twig_call_macro($macros["QD"], "macro_query_designer_condition_builder", [["id" => "oro_email_autoresponserule-condition-builder", "fieldConditionModule" => "oroemail/js/app/views/email-field-condition-view", "fieldConditionOptions" => ["entityData" =>             // line 49
($context["emailEntityData"] ?? null)], "metadata" =>             // line 51
($context["metadata"] ?? null)]], 45, $context, $this->getSourceContext());
            // line 52
            echo "
                            </div>
                        </div>
                        ";
            // line 55
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
            echo "
                    </div>
                    <div class=\"widget-actions form-actions\" style=\"display: none;\">
                        <button class=\"btn\" type=\"reset\">";
            // line 58
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
            echo "</button>
                        ";
            // line 59
            $context["label"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 59), "data", [], "any", false, false, false, 59), "id", [], "any", false, false, false, 59)) ? ("Update") : ("Add"));
            // line 60
            echo "                        <button class=\"btn btn-primary\" type=\"submit\">";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
            echo "</button>
                    </div>
                </fieldset>
            ";
            // line 63
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock(($context["form"] ?? null), 'form_end');
            echo "
            ";
            // line 64
            echo $this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null));
            echo "
            <div";
            // line 65
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroemail/js/app/components/auto-response-rule-component"]], 65, $context, $this->getSourceContext());
            // line 67
            echo "></div>
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/AutoResponseRule/dialog/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 67,  131 => 65,  127 => 64,  123 => 63,  116 => 60,  114 => 59,  110 => 58,  104 => 55,  99 => 52,  97 => 51,  96 => 49,  95 => 45,  89 => 42,  83 => 39,  79 => 38,  74 => 35,  72 => 32,  71 => 31,  70 => 28,  69 => 25,  68 => 24,  63 => 23,  61 => 19,  58 => 18,  53 => 16,  50 => 15,  48 => 10,  46 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/AutoResponseRule/dialog/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/AutoResponseRule/dialog/update.html.twig");
    }
}
