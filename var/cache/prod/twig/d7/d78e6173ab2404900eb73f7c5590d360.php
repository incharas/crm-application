<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCase/Case/userCases.html.twig */
class __TwigTemplate_78c9da6218d222417e13d125fac1dff9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_case_view")) {
            // line 2
            echo "    ";
            $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroCase/Case/userCases.html.twig", 2)->unwrap();
            // line 3
            echo "
    <div class=\"responsive-cell\">
        <div class=\"box-type1\">
            <div class=\"title\">
                <span class=\"widget-title\">";
            // line 7
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.entity_plural_label"), "html", null, true);
            echo "</span>
            </div>

            <div class=\"row-fluid\">
                ";
            // line 11
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["user-cases-grid", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 11)], ["cssClass" => "inner-grid"]], 11, $context, $this->getSourceContext());
            echo "
            </div>
        </div>
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCase/Case/userCases.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  55 => 11,  48 => 7,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCase/Case/userCases.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/CaseBundle/Resources/views/Case/userCases.html.twig");
    }
}
