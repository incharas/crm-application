<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/multiple-entity/templates/multiple-entity.html */
class __TwigTemplate_cba6deafd60395e1fb3adfa7d686d6b9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"list-group-item__inner\">
    <a href=\"#\" class=\"btn btn-icon btn-square-lighter remove-btn\" title=\"<%- _.__('Remove') %>\">
        <span class=\"fa-close\"></span>
    </a>
    <div class=\"list-group-item__header\">
        <% if (hasDefault) { %>
        <div class=\"list-group-item__default\">
            <input type=\"radio\" class=\"default-selector\"
                   name=\"<%- name %>_default\"
                   title=\"<%- _.__('Default') %>\"
                   value=\"<%- id %>\"
            <% if(isDefault) { %>
            checked=\"checked\"
            <% } %>
            />
        </div>
        <% } %>
        <h4 class=\"list-group-item__title\">
            <a href=\"<%- link %>\" class=\"list-group-item__link entity-info\" title=\"<%- label %>\"><%- label %></a>
        </h4>
    </div>
    <div class=\"list-group-item__extra-info\">
        <% _.each(extraData, function(item) { %>
        <% if (!_.isUndefined(item.label)) { %>
            <% if (!item.hasOwnProperty('value') || !item.value) { item.value = _.__('N/A'); } %>
            <div><%- item.label %>: <%- item.value %></div>
        <% } %>
        <% }); %>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/multiple-entity/templates/multiple-entity.html";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/multiple-entity/templates/multiple-entity.html", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/multiple-entity/templates/multiple-entity.html");
    }
}
