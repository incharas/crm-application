<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/base/view.js */
class __TwigTemplate_8e1922b26de045bc4b9da35d4a30d05a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const _ = require('underscore');
    const Chaplin = require('chaplin');
    const BaseCollection = require('../../models/base/collection');

    const BaseView = Chaplin.View.extend(/** @lends BaseView.prototype */{
        /**
         * @inheritdoc
         */
        constructor: function BaseView(options) {
            BaseView.__super__.constructor.call(this, options);
        },

        getTemplateFunction: function(templateKey) {
            templateKey = templateKey || 'template';
            const template = this[templateKey];
            let templateFunc = null;

            // If templateSelector is set in an extended view
            if (this[templateKey + 'Selector'] && \$(this[templateKey + 'Selector']).length) {
                templateFunc = _.template(\$(this[templateKey + 'Selector']).html());
            } else if (typeof template === 'string') {
                templateFunc = _.template(template);
                // share a compiled template with all instances built with same constructor
                this.constructor.prototype[templateKey] = templateFunc;
            } else {
                templateFunc = template;
            }

            if (typeof templateFunc === 'function') {
                return data => templateFunc(data).trim();
            }

            return templateFunc;
        },

        getTemplateData: function() {
            const data = BaseView.__super__.getTemplateData.call(this);
            if (!this.model && this.collection && this.collection instanceof BaseCollection) {
                _.extend(data, this.collection.serializeExtraData());
            }
            return data;
        },

        /**
         * Tries to find element in already declared regions, otherwise calls super _ensureElement method
         *
         * @private
         * @override
         */
        _ensureElement: function() {
            let \$el;
            const el = this.el;

            if (el && typeof el === 'string' && el.substr(0, 7) === 'region:') {
                \$el = this._findRegionElem(el.substr(7));
            }

            if (\$el) {
                this.setElement(\$el);
            } else {
                BaseView.__super__._ensureElement.call(this);
            }
        },

        /**
         * Tries to find element by region name
         *
         * @param {string} name
         * @returns {jQuery|undefined}
         * @private
         */
        _findRegionElem: function(name) {
            let \$el;
            const region = Chaplin.mediator.execute('region:find', name);
            if (region) {
                const instance = region.instance;
                if (instance.container) {
                    \$el = instance.region ? \$(instance.container).find(region.selector) : instance.container;
                } else {
                    \$el = instance.\$(region.selector);
                }
            }
            return \$el;
        },

        render: function() {
            BaseView.__super__.render.call(this);
            this.initControls();
            return this;
        }
    }, {
        /**
         * Resolves element declared in view's options
         *
         * @param {string|jQuery} el value of view's element declaration in options
         * @return {jQuery}
         */
        resolveElOption: function(el) {
            let \$el;
            if (typeof el === 'string' && el.substr(0, 7) === 'region:') {
                \$el = BaseView.prototype._findRegionElem(el.substr(7));
            } else {
                \$el = \$(el);
            }
            return \$el;
        }
    });

    return BaseView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/base/view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/base/view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/base/view.js");
    }
}
