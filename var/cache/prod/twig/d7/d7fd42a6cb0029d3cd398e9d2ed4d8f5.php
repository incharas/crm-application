<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/css/scss/inline-editable-wrapper.scss */
class __TwigTemplate_59072f1f972d3eb31d2b792ac9077375 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.inline-editable-wrapper {
    &.loading {
        min-height: auto;

        .inline-actions-element {
            &::before {
                position: absolute;
                top: \$inline-editable-wrapper-loading-top;
                right: \$inline-editable-wrapper-loading-end;

                display: block;

                content: '';

                @include loader(\$inline-editable-wrapper-loading-size, \$inline-editable-wrapper-loading-width);
            }

            &:hover .inline-actions-element_actions {
                visibility: hidden;
            }
        }
    }
}

.inline-editable-wrapper:not(:hover) {
    &.save-success .inline-actions-element {
        &_wrapper {
            background: \$cell-success-background;
        }
    }

    &.save-fail .inline-actions-element {
        &_wrapper {
            background: \$cell-error-background;
        }
    }

    &.loading .inline-actions-element {
        &_wrapper {
            background: \$cell-loading-background;
        }
    }
}

.inline-actions-btn {
    padding: \$inline-actions-btn-offset;

    font-size: \$base-font-size--m;
    line-height: inherit;

    border-width: 0;
    background-color: transparent;
    color: \$inline-actions-btn-color;

    cursor: pointer;

    &:hover {
        color: \$inline-actions-btn-color-hover;
    }

    &--size-s {
        line-height: \$inline-actions-btn-s-line-height;
        padding: \$inline-actions-btn-s-offset;
    }

    @at-root .has-error .inline-actions-btn {
        color: \$inline-actions-btn-color-error;

        &:hover {
            color: \$inline-actions-btn-color-error;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/css/scss/inline-editable-wrapper.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/css/scss/inline-editable-wrapper.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/css/scss/inline-editable-wrapper.scss");
    }
}
