<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/User/view.html.twig */
class __TwigTemplate_e9207e4d0597dedc7539c39a041f789f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'pageTitleIcon' => [$this, 'block_pageTitleIcon'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'stats' => [$this, 'block_stats'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/User/view.html.twig", 2)->unwrap();
        // line 4
        $context["fullname"] = _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["entity"] ?? null)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"));

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%username%" =>         // line 5
($context["fullname"] ?? null)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroUser/User/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/User/view.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_navButtons", $context)) ? (_twig_default_filter(($context["view_navButtons"] ?? null), "view_navButtons")) : ("view_navButtons")), ["entity" => ($context["entity"] ?? null)]);
        // line 11
        echo "    ";
        if ($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop()) {
            // line 12
            echo "        ";
            if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("CONFIGURE", ($context["entity"] ?? null)) &&  !($context["isProfileView"] ?? null))) {
                // line 13
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_config", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 14
($context["entity"] ?? null), "id", [], "any", false, false, false, 14)]), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.user_configuration.label"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.user_configuration.label"), "iCss" => "fa-cog"]], 13, $context, $this->getSourceContext());
                // line 18
                echo "
        ";
            } elseif ((            // line 19
($context["isProfileView"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("update_own_configuration"))) {
                // line 20
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_profile_configuration"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.user_configuration.label"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.user_configuration.label"), "iCss" => "fa-cog"]], 20, $context, $this->getSourceContext());
                // line 25
                echo "
        ";
            }
            // line 27
            echo "    ";
        }
        // line 28
        echo "    ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)) &&  !($context["isProfileView"] ?? null))) {
            // line 29
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 30
($context["entity"] ?? null), "id", [], "any", false, false, false, 30)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_label")]], 29, $context, $this->getSourceContext());
            // line 32
            echo "
    ";
        } elseif ((        // line 33
($context["isProfileView"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("update_own_profile"))) {
            // line 34
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_profile_update"), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("profile")]], 34, $context, $this->getSourceContext());
            // line 37
            echo "
    ";
        }
        // line 39
        echo "    ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 39) != Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 39), "id", [], "any", false, false, false, 39)))) {
            // line 40
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_user", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 41
($context["entity"] ?? null), "id", [], "any", false, false, false, 41)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-user", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 45
($context["entity"] ?? null), "id", [], "any", false, false, false, 45), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_label"), "disabled" =>  !            // line 47
($context["allow_delete"] ?? null)]], 40, $context, $this->getSourceContext());
            // line 48
            echo "
    ";
        }
    }

    // line 52
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 54
($context["entity"] ?? null), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_plural_label"), "entityTitle" =>         // line 56
($context["fullname"] ?? null)];
        // line 58
        echo "    ";
        if ($this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("oro_user_index", "routes")) {
            // line 59
            echo "        ";
            $context["breadcrumbs"] = twig_array_merge(($context["breadcrumbs"] ?? null), ["indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_index")]);
            // line 60
            echo "    ";
        }
        // line 61
        echo "
    ";
        // line 62
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 65
    public function block_pageTitleIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 66
        echo "    <div class=\"page-title__icon\">";
        // line 67
        $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroUser/User/view.html.twig", 67)->display(twig_array_merge($context, ["file" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 68
($context["entity"] ?? null), "avatar", [], "any", false, false, false, 68), "filter" => "avatar_med", "img_attrs" => ["alt" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(        // line 70
($context["entity"] ?? null))]]));
        // line 72
        echo "</div>
";
    }

    // line 75
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 76
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/User/view.html.twig", 76)->unwrap();
        // line 77
        echo "
    ";
        // line 78
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <span class=\"page-title__status\">
        ";
        // line 80
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "enabled", [], "any", false, false, false, 80)) {
            // line 81
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.enabled.enabled"), "enabled"], 81, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 83
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.enabled.disabled"), "disabled"], 83, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 85
        echo "
        ";
        // line 86
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "authStatus", [], "any", false, false, false, 86), "id", [], "any", false, false, false, 86) == "expired")) {
            // line 87
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "authStatus", [], "any", false, false, false, 87), "name", [], "any", false, false, false, 87), "disabled", "fa-unlock-alt"], 87, $context, $this->getSourceContext());
            echo "
        ";
        } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 88
($context["entity"] ?? null), "authStatus", [], "any", false, false, false, 88), "id", [], "any", false, false, false, 88) == "active")) {
            // line 89
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "authStatus", [], "any", false, false, false, 89), "name", [], "any", false, false, false, 89), "enabled", "fa-unlock"], 89, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 91
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "authStatus", [], "any", false, false, false, 91), "name", [], "any", false, false, false, 91), "tentatively", "fa-lock"], 91, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 93
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_page_title_status", $context)) ? (_twig_default_filter(($context["view_page_title_status"] ?? null), "view_page_title_status")) : ("view_page_title_status")), ["entity" => ($context["entity"] ?? null)]);
        // line 94
        echo "    </span>
";
    }

    // line 97
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 98
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_view_user_login_attempt")) {
            // line 99
            echo "        ";
            ob_start(function () { return ''; });
            // line 100
            echo "            ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.last_login.label"), "html", null, true);
            echo ": <a title=\"";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.userloginattempt.entity_description"), "html", null, true);
            echo "\" href=\"";
            echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->generateGridUrl("oro_user_login_attempts", "user-login-attempts-grid", ["f[user][value]" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 100)]), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lastLogin", [], "any", false, false, false, 100)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lastLogin", [], "any", false, false, false, 100))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "html", null, true);
            echo "</a>
        ";
            $context["loginAttempts"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 102
            echo "    ";
        } else {
            // line 103
            echo "        ";
            ob_start(function () { return ''; });
            // line 104
            echo "            ";
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.last_login.label"), "html", null, true);
            echo ": ";
            echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lastLogin", [], "any", false, false, false, 104)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "lastLogin", [], "any", false, false, false, 104))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "html", null, true);
            echo "
        ";
            $context["loginAttempts"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 106
            echo "    ";
        }
        // line 107
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 107)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 107))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "html", null, true);
        echo "</li>
    <li>";
        // line 108
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 108)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 108))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "html", null, true);
        echo "</li>
    <li>";
        // line 109
        echo twig_escape_filter($this->env, ($context["loginAttempts"] ?? null), "html", null, true);
        echo "</li>
    <li>";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.login_count.label"), "html", null, true);
        echo ": ";
        echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "loginCount", [], "any", true, true, false, 110)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "loginCount", [], "any", false, false, false, 110), 0)) : (0)), "html", null, true);
        echo "</li>
";
    }

    // line 113
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 114
        echo "    ";
        ob_start(function () { return ''; });
        // line 115
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_widget_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 117
($context["entity"] ?? null), "id", [], "any", false, false, false, 117), "viewProfile" => ($context["isProfileView"] ?? null)]), "separateLayout" => false]);
        // line 119
        echo "
    ";
        $context["userInformationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 121
        echo "    ";
        ob_start(function () { return ''; });
        // line 122
        echo "        ";
        if (((($context["isProfileView"] ?? null) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 122), "id", [], "any", false, false, false, 122) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 122))) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("MANAGE_API_KEY", ($context["entity"] ?? null)))) {
            // line 123
            echo "            ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_apigen", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 125
($context["entity"] ?? null), "id", [], "any", false, false, false, 125)]), "alias" => "user-apikey-gen-widget", "elementFirst" => true, "separateLayout" => false]);
            // line 129
            echo "
        ";
        }
        // line 131
        echo "    ";
        $context["apiKeyWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 132
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "subblocks" => [0 => ["data" => [0 =>         // line 136
($context["userInformationWidget"] ?? null)]], 1 => ["data" => [0 =>         // line 137
($context["apiKeyWidget"] ?? null)]]]]];
        // line 141
        echo "
    ";
        // line 142
        $context["id"] = "userView";
        // line 143
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 144
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroUser/User/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  342 => 144,  339 => 143,  337 => 142,  334 => 141,  332 => 137,  331 => 136,  329 => 132,  326 => 131,  322 => 129,  320 => 125,  318 => 123,  315 => 122,  312 => 121,  308 => 119,  306 => 117,  304 => 115,  301 => 114,  297 => 113,  289 => 110,  285 => 109,  279 => 108,  272 => 107,  269 => 106,  261 => 104,  258 => 103,  255 => 102,  243 => 100,  240 => 99,  237 => 98,  233 => 97,  228 => 94,  225 => 93,  219 => 91,  213 => 89,  211 => 88,  206 => 87,  204 => 86,  201 => 85,  195 => 83,  189 => 81,  187 => 80,  182 => 78,  179 => 77,  176 => 76,  172 => 75,  167 => 72,  165 => 70,  164 => 68,  163 => 67,  161 => 66,  157 => 65,  151 => 62,  148 => 61,  145 => 60,  142 => 59,  139 => 58,  137 => 56,  136 => 54,  134 => 53,  130 => 52,  124 => 48,  122 => 47,  121 => 45,  120 => 41,  118 => 40,  115 => 39,  111 => 37,  108 => 34,  106 => 33,  103 => 32,  101 => 30,  99 => 29,  96 => 28,  93 => 27,  89 => 25,  86 => 20,  84 => 19,  81 => 18,  79 => 14,  77 => 13,  74 => 12,  71 => 11,  69 => 10,  66 => 9,  63 => 8,  59 => 7,  54 => 1,  52 => 5,  49 => 4,  47 => 2,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/User/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/User/view.html.twig");
    }
}
