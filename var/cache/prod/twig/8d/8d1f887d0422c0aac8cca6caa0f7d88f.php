<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/header.scss */
class __TwigTemplate_02dd5378de5f6c71757cfeb3a8557127 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$header-dropdown-menu-header-font-weight: font-weight('bold') !default;
\$header-dropdown-menu-header-margin-bottom: 12px !default;
\$header-dropdown-menu-box-shadow: -1px -1px 9px 3px rgba(0 0 0 / 5%) !default;
\$header-dropdown-item-form-padding: 10px 0 20px !default;
\$header-search-dropdown-min-width: 400px !default;
\$header-search-dropdown-menu-background: \$primary-inverse !default;
\$header-search-field-container-margin-end: 8px !default;
\$header-search-suggestion-list-separator-color: \$primary-860 !default;
\$header-search-suggestion-list-separator-width: 1px !default;
\$header-search-suggestion-list-shadow: 1px 1px 9px 3px rgba(0 0 0 / 8%) !default;
\$header-search-suggestion-item-description-color: \$primary-200 !default;
\$header-search-suggestion-item-description-line-height: 1.5 !default;
\$header-search-suggestion-item-entity-color: \$primary-550 !default;
\$header-search-suggestion-item-h-margin: 16px !default;
\$header-search-suggestion-item-v-margin: 8px !default;
\$header-search-selected-suggestion-item-color: \$primary-inverse !default;
\$header-search-selected-suggestion-item-bg: #3875d7 !default;
\$header-search-suggestion-loader-mask-padding: 6px !default;
\$header-search-suggestion-loader-size: 24px !default;
\$header-search-suggestion-loader-thickness: 5px !default;
\$header-search-suggestion-list-max-height: 402px !default;
\$header-search-no-data-padding: \$no-data-offset !default;
\$header-shortcut-dropdown-width: 310px !default;
\$header-shortcut-dropdown-search-margin-bottom: 6px !default;
\$header-shortcut-typeahead-padding: 8px 0 !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/header.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/header.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/header.scss");
    }
}
