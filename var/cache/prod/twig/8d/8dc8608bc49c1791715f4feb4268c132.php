<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCurrency/Form/fields.html.twig */
class __TwigTemplate_b9bf9ac9b8467e6ac987eec68bdab2a3 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_currency_price_widget' => [$this, 'block_oro_currency_price_widget'],
            'oro_multicurrency_row' => [$this, 'block_oro_multicurrency_row'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_currency_price_widget', $context, $blocks);
        // line 11
        echo "
";
        // line 12
        $this->displayBlock('oro_multicurrency_row', $context, $blocks);
        // line 59
        echo "
";
    }

    // line 1
    public function block_oro_currency_price_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <div class=\"currency-select-enabled\">
        <div class=\"multicurrency-fields-wrapper value-field input-append\">
            ";
        // line 4
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 4), 'widget');
        echo "
            ";
        // line 5
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 5), 'widget');
        echo "
            ";
        // line 6
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 6), 'errors');
        echo "
            ";
        // line 7
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 7), 'errors');
        echo "
        </div>
    </div>
";
    }

    // line 12
    public function block_oro_multicurrency_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    <div class=\"control-group";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", true, true, false, 13)) {
            echo " ";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 13), "html", null, true);
        }
        echo "\">
        <div class=\"control-label wrap\">
            ";
        // line 15
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'label');
        echo "
        </div>
        <div class=\"controls";
        // line 17
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            echo " validation-error";
        }
        echo "\">
            ";
        // line 18
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCurrency/Form/fields.html.twig", 18)->unwrap();
        // line 19
        echo "            <div class=\"currency-select-enabled\" ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["view" => "orocurrency/js/app/views/multicurrency-control-view", "autoRender" => true, "rates" =>         // line 24
($context["currencyRates"] ?? null)]]], 19, $context, $this->getSourceContext());
        // line 26
        echo ">
                <div class=\"multicurrency-fields-wrapper value-field input-append\">
                    ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 28), 'widget');
        echo "
                    ";
        // line 29
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 29), "vars", [], "any", false, false, false, 29), "hidden_field", [], "any", false, false, false, 29)) {
            // line 30
            echo "                        <span class=\"add-on\">";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 30), "vars", [], "any", false, false, false, 30), "choices", [], "any", false, false, false, 30)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null), "label", [], "any", false, false, false, 30), "html", null, true);
            echo "</span>
                        <input name=\"";
            // line 31
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 31), "vars", [], "any", false, false, false, 31), "full_name", [], "any", false, false, false, 31), "html", null, true);
            echo "\" type=\"hidden\" value=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 31), "vars", [], "any", false, false, false, 31), "value", [], "any", false, false, false, 31), "html", null, true);
            echo "\">
                    ";
        } else {
            // line 33
            echo "                        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 33), 'widget');
            echo "
                    ";
        }
        // line 35
        echo "                </div>
                ";
        // line 36
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "baseCurrencyValue", [], "any", true, true, false, 36)) {
            // line 37
            echo "                    <div class=\"base-currency\">
                        <div class=\"base-currency-field input-append\">
                            ";
            // line 39
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "baseCurrencyValue", [], "any", false, false, false, 39), 'widget');
            echo "
                            ";
            // line 40
            if (array_key_exists("defaultCurrency", $context)) {
                // line 41
                echo "                                <span class=\"add-on\">";
                echo twig_escape_filter($this->env, ($context["defaultCurrency"] ?? null), "html", null, true);
                echo "</span>
                            ";
            } else {
                // line 43
                echo "                                <span class=\"add-on\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 43), 'widget');
                echo "</span>
                            ";
            }
            // line 45
            echo "                        </div>
                    </div>
                ";
        }
        // line 48
        echo "                <div class=\"default-currency-equivalent\" data-name=\"default-currency-equivalent\"></div>
                ";
        // line 49
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
        echo "
                ";
        // line 50
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "currency", [], "any", false, false, false, 50), 'errors');
        echo "
                ";
        // line 51
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "value", [], "any", false, false, false, 51), 'errors');
        echo "
                ";
        // line 52
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "baseCurrencyValue", [], "any", true, true, false, 52)) {
            // line 53
            echo "                    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "baseCurrencyValue", [], "any", false, false, false, 53), 'errors');
            echo "
                ";
        }
        // line 55
        echo "            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroCurrency/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  192 => 55,  186 => 53,  184 => 52,  180 => 51,  176 => 50,  172 => 49,  169 => 48,  164 => 45,  158 => 43,  152 => 41,  150 => 40,  146 => 39,  142 => 37,  140 => 36,  137 => 35,  131 => 33,  124 => 31,  119 => 30,  117 => 29,  113 => 28,  109 => 26,  107 => 24,  105 => 19,  103 => 18,  97 => 17,  92 => 15,  83 => 13,  79 => 12,  71 => 7,  67 => 6,  63 => 5,  59 => 4,  55 => 2,  51 => 1,  46 => 59,  44 => 12,  41 => 11,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCurrency/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/CurrencyBundle/Resources/views/Form/fields.html.twig");
    }
}
