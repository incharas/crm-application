<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/page_title_block.html.twig */
class __TwigTemplate_4d090dbffe285c87e71219d0b9955ff1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<div class=\"row\">
    ";
        // line 2
        if (array_key_exists("title", $context)) {
            // line 3
            echo "        <div class=\"pull-left-extra\">
            <h1 class=\"page-title__entity-title\">";
            // line 4
            echo twig_escape_filter($this->env, ($context["title"] ?? null), "html", null, true);
            echo "</h1>
        </div>
    ";
        }
        // line 7
        echo "    ";
        if ((array_key_exists("titleButtonsBlock", $context) &&  !twig_test_empty(twig_trim_filter(($context["titleButtonsBlock"] ?? null))))) {
            // line 8
            echo "        <div class=\"pull-right title-buttons-container\">
            ";
            // line 9
            echo twig_escape_filter($this->env, ($context["titleButtonsBlock"] ?? null), "html", null, true);
            echo "
        </div>
    ";
        }
        // line 12
        echo "</div>
<div class=\"row inline-info\">
    ";
        // line 14
        if ((array_key_exists("pageStatsBlock", $context) &&  !twig_test_empty(twig_trim_filter(($context["pageStatsBlock"] ?? null))))) {
            // line 15
            echo "    <div class=\"pull-left-extra\">
        <div class=\"clearfix\">
            <ul class=\"inline\">
                ";
            // line 18
            echo twig_escape_filter($this->env, ($context["pageStatsBlock"] ?? null), "html", null, true);
            echo "
            </ul>
        </div>
    </div>
    ";
        }
        // line 23
        echo "    ";
        if ((array_key_exists("pageActionsBlock", $context) &&  !twig_test_empty(twig_trim_filter(($context["pageActionsBlock"] ?? null))))) {
            // line 24
            echo "    <div class=\"pull-right page-title__entity-info-state\">
        <div class=\"inline-decorate-container\">
            <ul class=\"inline-decorate\">
                ";
            // line 27
            echo twig_escape_filter($this->env, ($context["pageActionsBlock"] ?? null), "html", null, true);
            echo "
            </ul>
        </div>
    </div>
    ";
        }
        // line 32
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/page_title_block.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 32,  90 => 27,  85 => 24,  82 => 23,  74 => 18,  69 => 15,  67 => 14,  63 => 12,  57 => 9,  54 => 8,  51 => 7,  45 => 4,  42 => 3,  40 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/page_title_block.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/page_title_block.html.twig");
    }
}
