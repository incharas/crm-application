<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSync/sync_js.html.twig */
class __TwigTemplate_b4055c86bafb9c838ad77985aefe0591 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ($this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->checkWsConnected()) {
            // line 2
            echo "    ";
            $context["ws"] = $this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->getWsConfig();
            // line 3
            echo "<script>
    loadModules(['jquery', 'orosync/js/sync', 'orosync/js/sync/wamp', 'routing'],
    function(\$, sync, Wamp, routing){
        \$(document).on('click.action.data-api', '[data-action=sync-connect]', function (e) {
            sync.reconnect();
            \$(e.target).closest('.alert').alert('close');
            e.preventDefault();
        });

        sync(new Wamp({
            secure: ";
            // line 13
            echo ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 13), "headers", [], "any", false, false, false, 13), "get", [0 => "X-Forwarded-Proto"], "method", false, false, false, 13) == "https") || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 13), "isSecure", [], "method", false, false, false, 13))) ? ("true") : ("false"));
            echo ",
            host: '";
            // line 14
            echo twig_escape_filter($this->env, (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ws"] ?? null), "host", [], "any", false, false, false, 14) == "*")) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 14), "host", [], "any", false, false, false, 14)) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ws"] ?? null), "host", [], "any", false, false, false, 14))), "html", null, true);
            echo "',
            port: ";
            // line 15
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ws"] ?? null), "port", [], "any", false, false, false, 15));
            echo ",
            path: '";
            // line 16
            echo twig_escape_filter($this->env, twig_trim_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["ws"] ?? null), "path", [], "any", false, false, false, 16), "/"), "html", null, true);
            echo "',
            retryDelay: 60000,
            syncTicketUrl: routing.generate('oro_sync_ticket')
        }));
    });
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroSync/sync_js.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  66 => 16,  62 => 15,  58 => 14,  54 => 13,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSync/sync_js.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/SyncBundle/Resources/views/sync_js.html.twig");
    }
}
