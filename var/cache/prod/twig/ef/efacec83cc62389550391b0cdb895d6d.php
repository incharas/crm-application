<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityPagination/Placeholder/viewPagination.html.twig */
class __TwigTemplate_a249c01eccce47064895c4de9fa37c68 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["scope"] = twig_constant("Oro\\Bundle\\EntityPaginationBundle\\Manager\\EntityPaginationManager::VIEW_SCOPE");
        // line 2
        $this->loadTemplate("@OroEntityPagination/Placeholder/entityPagination.html.twig", "@OroEntityPagination/Placeholder/viewPagination.html.twig", 2)->display(twig_array_merge($context, ["scope" => ($context["scope"] ?? null)]));
    }

    public function getTemplateName()
    {
        return "@OroEntityPagination/Placeholder/viewPagination.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityPagination/Placeholder/viewPagination.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityPaginationBundle/Resources/views/Placeholder/viewPagination.html.twig");
    }
}
