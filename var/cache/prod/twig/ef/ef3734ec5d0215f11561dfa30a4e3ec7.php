<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImap/credentialsTopicSubscribe.html.twig */
class __TwigTemplate_e44a57157a043a0a4e64e11129f2ff39 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 1) && $this->extensions['Oro\Bundle\SyncBundle\Twig\OroSyncExtension']->checkWsConnected())) {
            // line 2
            echo "<script>
    loadModules(['orosync/js/sync', 'oroui/js/messenger', 'orotranslation/js/translator'],
        function (sync, messenger, __) {
            sync.subscribe('oro/imap_sync_fail/";
            // line 5
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 5), "id", [], "any", false, false, false, 5), "html", null, true);
            echo "', function (response) {
                messenger.notificationMessage(
                    'error',
                    __('oro.imap.sync.flash_message.user_box_failed', {username: response.username, host: response.host})
                );
            });
            ";
            // line 11
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_imap_sync_origin_credential_notifications")) {
                // line 12
                echo "            sync.subscribe('oro/imap_sync_fail/*', function (response) {
                messenger.notificationMessage(
                    'error',
                    __('oro.imap.sync.flash_message.system_box_failed', {username: response.username, host: response.host})
                );
            });
            ";
            }
            // line 19
            echo "        });
</script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroImap/credentialsTopicSubscribe.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 19,  55 => 12,  53 => 11,  44 => 5,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImap/credentialsTopicSubscribe.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImapBundle/Resources/views/credentialsTopicSubscribe.html.twig");
    }
}
