<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/font-awesome/_placeholders.scss */
class __TwigTemplate_7f387198a2c538de3d4c67f0ae79c9f9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

/* stylelint-disable scss/at-extend-no-missing-placeholder*/
/* Creating placeholders for Font's awesome classes */

%fa-spinner {
    @extend .fa-spinner;
}

%fa-spin {
    @extend .fa-spin;
}

%fa-pulse {
    @extend .fa-pulse;
}

";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/font-awesome/_placeholders.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/font-awesome/_placeholders.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/font-awesome/_placeholders.scss");
    }
}
