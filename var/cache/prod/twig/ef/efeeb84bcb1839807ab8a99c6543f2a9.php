<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDataGrid/layouts/default/imports/datagrid_toolbar/layout_mobile.html.twig */
class __TwigTemplate_47503d35b2fe5fd302124876d568e34a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '__datagrid_toolbar__datagrid_toolbar_actions_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_actions_widget'],
            '__datagrid_toolbar__datagrid_toolbar_page_size_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_page_size_widget'],
            '__datagrid_toolbar__datagrid_toolbar_pagination_container_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_pagination_container_widget'],
            '__datagrid_toolbar__datagrid_toolbar_pagination_widget' => [$this, 'block___datagrid_toolbar__datagrid_toolbar_pagination_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_actions_widget', $context, $blocks);
        // line 8
        echo "
";
        // line 9
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_page_size_widget', $context, $blocks);
        // line 16
        echo "
";
        // line 17
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_pagination_container_widget', $context, $blocks);
        // line 23
        echo "
";
        // line 24
        $this->displayBlock('__datagrid_toolbar__datagrid_toolbar_pagination_widget', $context, $blocks);
    }

    // line 1
    public function block___datagrid_toolbar__datagrid_toolbar_actions_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["attr"] ?? null), "class", [], "any", true, true, false, 3)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 3), "")) : ("")) . " actions-panel"), "data-grid-actions-panel" => ""]);
        // line 6
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 9
    public function block___datagrid_toolbar__datagrid_toolbar_page_size_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["attr"] ?? null), "class", [], "any", true, true, false, 11)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 11), "")) : ("")) . " page-size"), "data-grid-pagesize" => ""]);
        // line 14
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 17
    public function block___datagrid_toolbar__datagrid_toolbar_pagination_container_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 18
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 19
($context["attr"] ?? null), "class", [], "any", true, true, false, 19)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 19), "")) : ("")) . " pagination-container")]);
        // line 21
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    // line 24
    public function block___datagrid_toolbar__datagrid_toolbar_pagination_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 25
        echo "    ";
        $context["attr"] = twig_array_merge(($context["attr"] ?? null), ["class" => (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 26
($context["attr"] ?? null), "class", [], "any", true, true, false, 26)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["attr"] ?? null), "class", [], "any", false, false, false, 26), "")) : ("")) . " pagination"), "data-grid-pagination" => ""]);
        // line 29
        echo "    <div";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">";
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "@OroDataGrid/layouts/default/imports/datagrid_toolbar/layout_mobile.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  119 => 29,  117 => 26,  115 => 25,  111 => 24,  102 => 21,  100 => 19,  98 => 18,  94 => 17,  85 => 14,  83 => 11,  81 => 10,  77 => 9,  68 => 6,  66 => 3,  64 => 2,  60 => 1,  56 => 24,  53 => 23,  51 => 17,  48 => 16,  46 => 9,  43 => 8,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDataGrid/layouts/default/imports/datagrid_toolbar/layout_mobile.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/layouts/default/imports/datagrid_toolbar/layout_mobile.html.twig");
    }
}
