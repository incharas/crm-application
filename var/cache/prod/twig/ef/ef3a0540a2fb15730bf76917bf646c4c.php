<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivityList/ActivityList/js/view.html.twig */
class __TwigTemplate_4f13faedcced600c6dc61d2e2ff71c07 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'activityIcon' => [$this, 'block_activityIcon'],
            'activityDetails' => [$this, 'block_activityDetails'],
            'activityShortMessage' => [$this, 'block_activityShortMessage'],
            'activityDate' => [$this, 'block_activityDate'],
            'activityActions' => [$this, 'block_activityActions'],
            'activityContent' => [$this, 'block_activityContent'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<script type=\"text/html\" id=\"";
        echo twig_escape_filter($this->env, ($context["id"] ?? null), "html_attr");
        echo "\">
    <% var labelId = _.uniqueId('label-') %>
    <% var regionId = _.uniqueId('region-') %>
    <div class=\"accordion-group\">
        <div class=\"accordion-heading\">
            <a href=\"#<%- regionId %>\" data-toggle=\"collapse\" aria-expanded=\"<%- collapsed ? 'false' : 'true' %>\"
               aria-controls=\"<%- regionId %>\"
               class=\"btn btn-icon btn-light-custom accordion-icon<% if (collapsed) { %> collapsed<% } %>\"
               title=\"<%- _.__(collapsed ? 'Expand' : 'Collapse')%>\"
               role=\"button\"
            >
                <span class=\"fa-icon\"></span>
            </a>
            <div class=\"icon\">
                ";
        // line 15
        $this->displayBlock('activityIcon', $context, $blocks);
        // line 17
        echo "            </div>
            <div id=\"<%- labelId %>\" class=\"details\">
                ";
        // line 19
        $this->displayBlock('activityDetails', $context, $blocks);
        // line 21
        echo "            </div>
            <div class=\"extra-info\">
                <div class=\"message-item message\">
                    <div class=\"message-description\">
                        <b class=\"message-subject\"><%- _.unescape(subject) %></b>
                        <% if (!_.isUndefined(description) && !_.isEmpty(description)) { %>
                        - <%= description %>
                        <% } %>
                    </div>
                    ";
        // line 30
        $this->displayBlock('activityShortMessage', $context, $blocks);
        // line 32
        echo "                </div>
                <div class=\"comment-count\"<% if (!commentCount) { %> style=\"display:none\"<% } %>
                    title=\"<%- _.__('oro.activitylist.comment.quantity_label') %>\">
                    <span class=\"fa-comment\" aria-hidden=\"true\"></span><span class=\"count\"><%- commentCount %></span>
                </div>
            ";
        // line 37
        $this->displayBlock('activityDate', $context, $blocks);
        // line 42
        echo "            </div>
            <div class=\"actions\">
                ";
        // line 44
        $this->displayBlock('activityActions', $context, $blocks);
        // line 46
        echo "            </div>
        </div>
        <div id=\"<%- regionId %>\" role=\"region\" aria-labelledby=\"<%- labelId %>\" class=\"accordion-body collapse<% if (!collapsed) { %> show<% } %>\"
             data-collapsed-title=\"";
        // line 49
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Expand"), "html", null, true);
        echo "\" data-expanded-title=\"";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Collapse"), "html", null, true);
        echo "\">
            <div class=\"message\">
                ";
        // line 51
        $this->displayBlock('activityContent', $context, $blocks);
        // line 63
        echo "            </div>
        </div>
    </div>
</script>
";
    }

    // line 15
    public function block_activityIcon($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "                ";
    }

    // line 19
    public function block_activityDetails($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "                ";
    }

    // line 30
    public function block_activityShortMessage($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 31
        echo "                    ";
    }

    // line 37
    public function block_activityDate($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "                <div class=\"created-at\">
                    <span class=\"date\"><%- updatedAt %></span>
                </div>
            ";
    }

    // line 44
    public function block_activityActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 45
        echo "                ";
    }

    // line 51
    public function block_activityContent($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 52
        echo "                    <div class=\"activity-item-content\">
                        ";
        // line 54
        echo "                        <div class=\"info responsive-cell\"></div>
                        <% if (has_comments && commentable) { %>
                        <div class=\"responsive-cell\">
                            ";
        // line 58
        echo "                            <div class=\"comment\"></div>
                        </div>
                    </div>
                    <% } %>
                ";
    }

    public function getTemplateName()
    {
        return "@OroActivityList/ActivityList/js/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  174 => 58,  169 => 54,  166 => 52,  162 => 51,  158 => 45,  154 => 44,  147 => 38,  143 => 37,  139 => 31,  135 => 30,  131 => 20,  127 => 19,  123 => 16,  119 => 15,  111 => 63,  109 => 51,  102 => 49,  97 => 46,  95 => 44,  91 => 42,  89 => 37,  82 => 32,  80 => 30,  69 => 21,  67 => 19,  63 => 17,  61 => 15,  43 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivityList/ActivityList/js/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ActivityListBundle/Resources/views/ActivityList/js/view.html.twig");
    }
}
