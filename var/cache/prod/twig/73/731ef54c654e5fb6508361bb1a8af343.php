<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChart/macros.html.twig */
class __TwigTemplate_cf8ee6443706d111a17f20f65c2c280a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 10
    public function macro_renderChart($__data__ = null, $__options__ = null, $__config__ = null, $__isMobileVersion__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "data" => $__data__,
            "options" => $__options__,
            "config" => $__config__,
            "isMobileVersion" => $__isMobileVersion__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 11
            echo "    ";
            $context["name"] = twig_replace_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "name", [], "any", false, false, false, 11), ["_chart" => ""]);
            // line 12
            echo "    ";
            $context["params"] = ["ratio" => ((            // line 13
($context["isMobileVersion"] ?? null)) ? ("0.8") : ("0.35")), "data" =>             // line 14
($context["data"] ?? null), "options" =>             // line 15
($context["options"] ?? null), "config" =>             // line 16
($context["config"] ?? null), "date" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 17
($context["options"] ?? null), "settings", [], "any", false, true, false, 17), "quarterDate", [], "any", true, true, false, 17)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "settings", [], "any", false, false, false, 17), "quarterDate", [], "any", false, false, false, 17), ["timeZone" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()])) : (""))];
            // line 19
            echo "

    <div data-page-component-module=\"orochart/js/app/components/";
            // line 21
            echo twig_escape_filter($this->env, ($context["name"] ?? null), "html", null, true);
            echo "-chart-component\"
         data-page-component-options=\"";
            // line 22
            echo twig_escape_filter($this->env, json_encode(($context["params"] ?? null)), "html", null, true);
            echo "\"></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroChart/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 22,  70 => 21,  66 => 19,  64 => 17,  63 => 16,  62 => 15,  61 => 14,  60 => 13,  58 => 12,  55 => 11,  39 => 10,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChart/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ChartBundle/Resources/views/macros.html.twig");
    }
}
