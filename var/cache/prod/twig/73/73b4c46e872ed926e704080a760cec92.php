<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroActivityContact/ActivityContact/metrics.html.twig */
class __TwigTemplate_345f2e05e32abe894414249125497a39 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["metrics"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_activity_contact_metrics", ["entityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(        // line 3
($context["entity"] ?? null), true), "entityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 3)]), "alias" => "activity-contact-widget"]);
        // line 6
        if ( !twig_test_empty(twig_trim_filter(($context["metrics"] ?? null)))) {
            // line 7
            echo "    <li>";
            echo ($context["metrics"] ?? null);
            echo "</li>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroActivityContact/ActivityContact/metrics.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 7,  40 => 6,  38 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroActivityContact/ActivityContact/metrics.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ActivityContactBundle/Resources/views/ActivityContact/metrics.html.twig");
    }
}
