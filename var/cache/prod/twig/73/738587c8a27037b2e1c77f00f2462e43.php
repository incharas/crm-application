<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReport/Form/fields.html.twig */
class __TwigTemplate_b16c114ca79aa9b8e1b9552f867bbe90 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'oro_report_chart_widget' => [$this, 'block_oro_report_chart_widget'],
            'oro_report_chart_data_schema_collection_row' => [$this, 'block_oro_report_chart_data_schema_collection_row'],
            'oro_report_chart_data_schema_row' => [$this, 'block_oro_report_chart_data_schema_row'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroChart/Form/fields.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroChart/Form/fields.html.twig", "@OroReport/Form/fields.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_oro_report_chart_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    ";
        $this->displayBlock("oro_chart_widget", $context, $blocks);
        echo "
";
    }

    // line 7
    public function block_oro_report_chart_data_schema_collection_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    // line 11
    public function block_oro_report_chart_data_schema_row($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'widget');
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroReport/Form/fields.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  74 => 12,  70 => 11,  63 => 8,  59 => 7,  52 => 4,  48 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReport/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ReportBundle/Resources/views/Form/fields.html.twig");
    }
}
