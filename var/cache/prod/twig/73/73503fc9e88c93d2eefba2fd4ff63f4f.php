<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/bootstrap/variables/badge.scss */
class __TwigTemplate_d99ccd6f7c5dc31bd71385bb1354e52c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:map';

\$badge-theme-success: (
    'background': \$success-ultra-light,
    'color': \$success-darken,
    'icon-color': \$success-light
) !default;

\$badge-theme-danger: (
    'background': \$danger-lighten,
    'color': \$danger-ultra-dark,
    'icon-color': \$danger-darken
) !default;

\$badge-theme-warning: (
    'background': \$warning-light,
    'color': \$warning-darken,
    'icon-color': \$warning-dark
) !default;

\$badge-theme-info: (
    'background': \$info-lighten,
    'color': \$info-ultra-dark,
    'icon-color': \$info-dark
) !default;

\$badge-theme-primary: (
    'background': \$primary-860,
    'color': \$primary-400,
    'icon-color': \$primary-600
) !default;

\$badge-theme-keys: () !default;
\$badge-theme-keys: map.merge(
    (
        'enabled': \$badge-theme-success,
        'disabled': \$badge-theme-primary,
        'tentatively': \$badge-theme-warning,
        'info': \$badge-theme-info,
        'warning': \$badge-theme-warning
    ),
    \$badge-theme-keys
);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/bootstrap/variables/badge.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/bootstrap/variables/badge.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/bootstrap/variables/badge.scss");
    }
}
