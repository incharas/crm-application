<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Email/dialog/view.html.twig */
class __TwigTemplate_32973f35f4115f93467d72178a26cb46 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'page_container' => [$this, 'block_page_container'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        $this->displayBlock('page_container', $context, $blocks);
    }

    public function block_page_container($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_thread_widget", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["entity"] ?? null), "id", [], "any", false, false, false, 8), "showSingleEmail" =>  !$this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_activity_list.grouping")]), "alias" => "thread-view"]);
        // line 10
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Email/dialog/view.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  49 => 10,  47 => 8,  45 => 6,  38 => 5,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Email/dialog/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Email/dialog/view.html.twig");
    }
}
