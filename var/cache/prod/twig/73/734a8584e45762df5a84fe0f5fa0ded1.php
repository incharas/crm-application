<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/nav.scss */
class __TwigTemplate_7e8a1590dd74cd4c0c41a887d4ec470b extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$navbar-li-a-color: \$primary-200 !default;
\$navbar-li-a-text-shadow: none !default;
\$navbar-li-a-hover-color: \$primary-200 !default;

\$navbar-active-a-color: \$primary-200 !default;
\$navbar-active-a-font-weight: font-weight('bold') !default;
\$navbar-active-a-hover-color: \$primary-200 !default;

\$nav-tabs-border-width: 2px !default;
\$nav-tabs-border-color: \$primary-750 !default;
\$nav-tabs-active-border-color: \$secondary !default;

\$nav-tabs-column-inverse-border-color: \$primary-inverse !default;

\$nav-tabs-dropdown-toggle-caret-color: \$primary-200 !default;

\$nav-tabs-offset-bottom: 0 !default;
\$nav-tabs-inner-offset-left: \$nav-tabs-border-width !default;
\$nav-tabs-border-bottom: \$nav-tabs-border-width solid \$nav-tabs-border-color !default;
\$nav-tabs-display: flex !default;
\$nav-tabs-flex-wrap: wrap !default;
\$nav-tabs-responsive-flex-wrap: nowrap !default;

\$nav-item-float: none !default;
\$nav-item-offset-bottom: -\$nav-tabs-border-width !default;

\$nav-link-background: \$primary-900 !default;
\$nav-link-font-weight: font-weight('bold') !default;
\$nav-link-color: \$primary-200 !default;
\$nav-link-border: \$nav-tabs-border-width solid \$nav-tabs-border-color !default;
\$nav-link-border-radius: 0 !default;
\$nav-link-offset: 0 0 0 -#{\$nav-tabs-border-width} !default;

\$nav-link-hover-background: \$primary-830 !default;
\$nav-link-hover-color: \$primary-200 !default;
\$nav-link-hover-outline: none !default;

\$nav-link-active-background: \$primary-inverse !default;
\$nav-link-active-color: \$primary-200 !default;
\$nav-link-active-border-top-color: \$nav-tabs-active-border-color !default;
\$nav-link-active-border-bottom-color: transparent !default;

\$nav-link-column-active-border-top-color: \$nav-tabs-border-color !default;
\$nav-link-column-active-border-left-color: \$nav-tabs-active-border-color !default;
\$nav-link-column-active-border-right-color: \$nav-tabs-column-inverse-border-color !default;
\$nav-link-column-active-border-bottom-color: \$nav-tabs-border-color !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/nav.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/nav.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/nav.scss");
    }
}
