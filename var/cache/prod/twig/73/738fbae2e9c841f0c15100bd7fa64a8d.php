<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmbeddedForm/EmbeddedForm/view.html.twig */
class __TwigTemplate_8e839d0023d15c7b8423745c28dbabfc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
            'stats' => [$this, 'block_stats'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmbeddedForm/EmbeddedForm/view.html.twig", 2)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%form.title%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "title", [], "any", false, false, false, 3)]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroEmbeddedForm/EmbeddedForm/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmbeddedForm/EmbeddedForm/view.html.twig", 6)->unwrap();
        // line 7
        echo "
    ";
        // line 8
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_embedded_form_delete")) {
            // line 9
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_embedded_form_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 10
($context["entity"] ?? null), "id", [], "any", false, false, false, 10)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_embedded_form_list"), "aCss" => "no-hash remove-button", "id" => "btn-remove-embedded-form", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.entity_label")]], 9, $context, $this->getSourceContext());
            // line 15
            echo "
        ";
            // line 16
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 16, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 18
        echo "
    ";
        // line 19
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_embedded_form_delete")) {
            // line 20
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_embedded_form_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 20)]), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Edit"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Edit")]], 20, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 22
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_embedded_form_list"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel")]], 22, $context, $this->getSourceContext());
        echo "
";
    }

    // line 25
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 27
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_embedded_form_list"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 30
($context["entity"] ?? null), "title", [], "any", false, false, false, 30)];
        // line 32
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 35
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 36
        echo "    ";
        $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmbeddedForm/EmbeddedForm/view.html.twig", 36)->unwrap();
        // line 37
        echo "
    ";
        // line 38
        $context["id"] = "embedded-form-get-code";
        // line 39
        echo "
    ";
        // line 40
        $this->displayBlock('stats', $context, $blocks);
        // line 42
        echo "
    ";
        // line 43
        ob_start(function () { return ''; });
        // line 44
        echo "        <div class=\"widget-content\">
            <div class=\"row-fluid form-horizontal\">
                <div class=\"responsive-block\">
                    ";
        // line 47
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.title.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 47)], 47, $context, $this->getSourceContext());
        echo "
                    ";
        // line 48
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.form_type.label"), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((array_key_exists("label", $context)) ? (_twig_default_filter(($context["label"] ?? null), "N/A")) : ("N/A")))], 48, $context, $this->getSourceContext());
        echo "

                    ";
        // line 51
        echo "                    <div class=\"control-group\">
                        <label class=\"control-label\">";
        // line 52
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.css.label"), "html", null, true);
        echo "</label>

                        <div class=\"controls\" style=\"overflow-y: scroll; height: 180px;\">
                            <bdo dir=\"ltr\">";
        // line 55
        echo twig_nl2br(twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "css", [], "any", false, false, false, 55), "html", null, true));
        echo "</bdo>
                        </div>
                    </div>

                    ";
        // line 59
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.success_message.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "successMessage", [], "any", false, false, false, 59), null, null, ["dir" => "ltr"]], 59, $context, $this->getSourceContext());
        echo "
                    ";
        // line 60
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.allowed_domains.label"), $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(twig_nl2br(twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "allowedDomains", [], "any", false, false, false, 60), "html", null, true)))], 60, $context, $this->getSourceContext());
        echo "
                </div>
            </div>
        </div>
    ";
        $context["formDetails"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 65
        echo "    ";
        ob_start(function () { return ''; });
        // line 66
        echo "    <div class=\"widget-content\">
        <iframe
            id=\"embedded-form\"
            src=\"";
        // line 69
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("oro_embedded_form_submit", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 69)]), "html", null, true);
        echo "\"
            width=\"640\"
            height=\"800\"
            frameborder=\"0\"></iframe>
    </div>
    ";
        $context["formPreview"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 75
        echo "
    ";
        // line 76
        ob_start(function () { return ''; });
        // line 77
        echo "        <textarea id=\"oro_embeddedform_iframe\" cols=\"60\" rows=\"10\" class=\"fill-tab\" style=\"height: 300px\" dir=\"ltr\">";
        // line 79
        echo "<div id=\"embedded-form-container-";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 79), "html", null, true);
        echo "\"></div>
<script src=\"";
        // line 80
        echo twig_escape_filter($this->env, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 80), "getSchemeAndHttpHost", [], "method", false, false, false, 80) . $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroembeddedform/js/embed.form.js")), "html", null, true);
        echo "\"></script>
<script>
    new ORO.EmbedForm({
        container: 'embedded-form-container-";
        // line 83
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 83), "html", null, true);
        echo "',
        iframe: {
            src: \"";
        // line 85
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("oro_embedded_form_submit", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 85)]), "html", null, true);
        echo "\",
            width: 640,
            height: 800,
            frameBorder: 0
        }
    });
</script>
";
        // line 93
        echo "</textarea>
    ";
        $context["getCodeIframe"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 95
        echo "
    ";
        // line 96
        ob_start(function () { return ''; });
        // line 97
        echo "        <div class=\"alert alert-warning\" role=\"alert\">";
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.tabs.inline.description");
        echo "</div>
        <textarea id=\"oro_embeddedform_inline\" cols=\"60\" rows=\"10\" class=\"fill-tab\" style=\"height: 300px\" dir=\"ltr\">";
        // line 100
        echo "<div id=\"embedded-form-container-";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 100), "html", null, true);
        echo "\"></div>
<script src=\"";
        // line 101
        echo twig_escape_filter($this->env, (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 101), "getSchemeAndHttpHost", [], "method", false, false, false, 101) . $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("bundles/oroembeddedform/js/embed.form.js")), "html", null, true);
        echo "\"></script>
<script>
    new ORO.EmbedForm({
        container: 'embedded-form-container-";
        // line 104
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 104), "html", null, true);
        echo "',
        xhr: {
            url: \"";
        // line 106
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getUrl("oro_embedded_form_submit", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 106), "inline" => true]), "html", null, true);
        echo "\"
        }
    });
</script>
";
        // line 111
        echo "</textarea>
    ";
        $context["getCodeInline"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 113
        echo "
    ";
        // line 114
        $context["getCodeTabs"] = [0 => ["alias" => "oro_embeddedform_iframe", "widgetType" => "block", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.tabs.iframe.label"), "url" => "#oro_embeddedform_iframe", "content" =>         // line 120
($context["getCodeIframe"] ?? null)], 1 => ["alias" => "oro_embeddedform_inline", "widgetType" => "block", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.embeddedform.tabs.inline.label"), "url" => "#oro_embeddedform_inline", "content" =>         // line 127
($context["getCodeInline"] ?? null)]];
        // line 130
        echo "
    ";
        // line 131
        ob_start(function () { return ''; });
        // line 132
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_embedded_form_info", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 134
($context["entity"] ?? null), "id", [], "any", false, false, false, 134)])]);
        // line 135
        echo "
    ";
        $context["informationWidget"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 137
        echo "
    ";
        // line 138
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Details"), "data" => twig_array_merge([0 =>         // line 144
($context["informationWidget"] ?? null)], [0 => ($context["formDetails"] ?? null)])], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Form Preview"), "data" => [0 =>         // line 148
($context["formPreview"] ?? null)]]]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Get code"), "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Code"), "data" => [0 => $this->extensions['Oro\Bundle\UIBundle\Twig\TabExtension']->tabPanel($this->env,         // line 156
($context["getCodeTabs"] ?? null))]]]]];
        // line 160
        echo "
    ";
        // line 161
        $context["data"] = ["dataBlocks" =>         // line 162
($context["dataBlocks"] ?? null)];
        // line 164
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    // line 40
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@OroEmbeddedForm/EmbeddedForm/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  313 => 41,  309 => 40,  302 => 164,  300 => 162,  299 => 161,  296 => 160,  294 => 156,  293 => 148,  292 => 144,  291 => 138,  288 => 137,  284 => 135,  282 => 134,  280 => 132,  278 => 131,  275 => 130,  273 => 127,  272 => 120,  271 => 114,  268 => 113,  264 => 111,  257 => 106,  252 => 104,  246 => 101,  241 => 100,  236 => 97,  234 => 96,  231 => 95,  227 => 93,  217 => 85,  212 => 83,  206 => 80,  201 => 79,  199 => 77,  197 => 76,  194 => 75,  185 => 69,  180 => 66,  177 => 65,  169 => 60,  165 => 59,  158 => 55,  152 => 52,  149 => 51,  144 => 48,  140 => 47,  135 => 44,  133 => 43,  130 => 42,  128 => 40,  125 => 39,  123 => 38,  120 => 37,  117 => 36,  113 => 35,  106 => 32,  104 => 30,  103 => 27,  101 => 26,  97 => 25,  90 => 22,  84 => 20,  82 => 19,  79 => 18,  74 => 16,  71 => 15,  69 => 10,  67 => 9,  65 => 8,  62 => 7,  59 => 6,  55 => 5,  50 => 1,  48 => 3,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmbeddedForm/EmbeddedForm/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/EmbeddedForm/view.html.twig");
    }
}
