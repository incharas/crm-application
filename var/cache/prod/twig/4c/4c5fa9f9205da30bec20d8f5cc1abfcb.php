<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/collection-tools.js */
class __TwigTemplate_fad05e71d290350595df1df55b11b416 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const BaseCollection = require('oroui/js/app/models/base/collection');

    const collectionTools = {
        /**
         * Creates collection
         *
         * @param {Backbone.Collection} collection - collection to use as a base
         * @param {Object} options - options
         * @param {Function|Object|Array} options.criteria - criteria to filter children, see [_.iteratee](https://lodash.com/docs#iteratee)
         *                                                   for a list of available values
         * @param {Function} options.collection - constructor for filtered collection
         */
        createFilteredCollection: function(collection, options) {
            const criteria = _.iteratee(options.criteria);
            const Collection = options.collection || BaseCollection;

            const filteredCollection = new Collection(collection.filter(criteria), _.omit(options, ['criteria']));

            filteredCollection.listenTo(collection, 'change add remove reset sort', function() {
                filteredCollection.reset(collection.filter(criteria));
            });

            return filteredCollection;
        }
    };

    return collectionTools;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/collection-tools.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/collection-tools.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/collection-tools.js");
    }
}
