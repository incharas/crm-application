<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/Menu/navbar.html.twig */
class __TwigTemplate_fd5ab42c544ff39360d55bcc734854da extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'root' => [$this, 'block_root'],
            'brand' => [$this, 'block_brand'],
            'item' => [$this, 'block_item'],
            'linkElement' => [$this, 'block_linkElement'],
            'label' => [$this, 'block_label'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroNavigation/Menu/menu.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $this->parent = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/navbar.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_root($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 4
        echo "    <div class=\"navbar\">
        <div class=\"navbar-inner\">
            ";
        // line 6
        $this->displayBlock("brand", $context, $blocks);
        echo "

            ";
        // line 8
        $context["listAttributes"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "childrenAttributes", [], "any", false, false, false, 8);
        // line 9
        echo "            ";
        $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/navbar.html.twig", 9)->unwrap();
        // line 10
        echo "            ";
        $context["listAttributes"] = twig_array_merge(($context["listAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [        // line 11
($context["listAttributes"] ?? null), "class", [0 => "nav"]], 11, $context, $this->getSourceContext())]);
        // line 13
        echo "            ";
        $this->displayBlock("list", $context, $blocks);
        // line 14
        echo "</div>
    </div>
";
    }

    // line 18
    public function block_brand($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        if ( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extra", [0 => "brand"], "method", false, false, false, 19))) {
            // line 20
            echo "        ";
            $context["brandLink"] = (( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extra", [0 => "brandLink"], "method", false, false, false, 20))) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extra", [0 => "brandLink"], "method", false, false, false, 20)) : ("/"));
            // line 21
            echo "        <a class=\"brand\" href=\"";
            echo twig_escape_filter($this->env, ($context["brandLink"] ?? null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "extra", [0 => "brand"], "method", false, false, false, 21), "html", null, true);
            echo "</a>
    ";
        }
    }

    // line 25
    public function block_item($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 26
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 26) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 26))) {
            // line 27
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 27) > 0)) {
                // line 28
                $context["childrenClasses"] = twig_array_merge(($context["childrenClasses"] ?? null), [0 => "dropdown-menu"]);
                // line 29
                echo "        ";
            }
            // line 30
            echo "        ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 30) == 1)) {
                // line 31
                $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => "dropdown"]);
                // line 32
                echo "        ";
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 32) > 1)) {
                // line 33
                $context["classes"] = twig_array_merge(($context["classes"] ?? null), [0 => "dropdown-submenu"]);
                // line 34
                echo "        ";
            }
        }
        // line 37
        echo "
    ";
        // line 38
        $this->displayBlock("item_renderer", $context, $blocks);
        echo "
";
    }

    // line 41
    public function block_linkElement($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 42
        if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 42) == 1) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 42)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 42))) {
            // line 43
            echo "        ";
            $macros["oro_menu"] = $this->loadTemplate("@OroNavigation/Menu/menu.html.twig", "@OroNavigation/Menu/navbar.html.twig", 43)->unwrap();
            // line 44
            $context["linkAttributes"] = twig_array_merge(($context["linkAttributes"] ?? null), ["class" => twig_call_macro($macros["oro_menu"], "macro_add_attribute_values", [            // line 45
($context["linkAttributes"] ?? null), "class", [0 => "dropdown-toggle"]], 45, $context, $this->getSourceContext()), "data-toggle" => "dropdown"]);
        }
        // line 48
        echo "
    ";
        // line 49
        $this->displayParentBlock("linkElement", $context, $blocks);
        echo "
";
    }

    // line 52
    public function block_label($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    ";
        $this->displayParentBlock("label", $context, $blocks);
        echo "
    ";
        // line 54
        if (((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "hasChildren", [], "any", false, false, false, 54) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "level", [], "any", false, false, false, 54) == 1)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "displayChildren", [], "any", false, false, false, 54))) {
            // line 55
            echo "        <b class=\"caret\"></b>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroNavigation/Menu/navbar.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  171 => 55,  169 => 54,  164 => 53,  160 => 52,  154 => 49,  151 => 48,  148 => 45,  147 => 44,  144 => 43,  142 => 42,  138 => 41,  132 => 38,  129 => 37,  125 => 34,  123 => 33,  120 => 32,  118 => 31,  115 => 30,  112 => 29,  110 => 28,  107 => 27,  105 => 26,  101 => 25,  91 => 21,  88 => 20,  85 => 19,  81 => 18,  75 => 14,  72 => 13,  70 => 11,  68 => 10,  65 => 9,  63 => 8,  58 => 6,  54 => 4,  50 => 3,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/Menu/navbar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/Menu/navbar.html.twig");
    }
}
