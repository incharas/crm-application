<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Role/update.html.twig */
class __TwigTemplate_bc2b75ed61ada0ccb31fd8520d9aef38 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroUser/Role/update.html.twig", 2)->unwrap();
        // line 4
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 4), "value", [], "any", false, false, false, 4), "id", [], "any", false, false, false, 4);

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%role%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["form"] ?? null), "vars", [], "any", false, false, false, 5), "value", [], "any", false, false, false, 5), "label", [], "any", false, false, false, 5), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.entity_label")]]);
        // line 6
        $context["gridName"] = "role-users-grid";
        // line 7
        $context["formAction"] = ((($context["entityId"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_role_update", ["id" => ($context["entityId"] ?? null)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_role_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroUser/Role/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 9
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 10
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Role/update.html.twig", 10)->unwrap();
        // line 11
        echo "
    ";
        // line 12
        $this->displayParentBlock("navButtons", $context, $blocks);
        echo "
    ";
        // line 13
        if ((($context["entityId"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 13), "value", [], "any", false, false, false, 13)))) {
            // line 14
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_role", ["id" =>             // line 15
($context["entityId"] ?? null)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_role_index"), "aCss" => "no-hash remove-button", "dataId" =>             // line 18
($context["entityId"] ?? null), "id" => "btn-remove-role", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.entity_label"), "disabled" =>  !            // line 21
($context["allow_delete"] ?? null)]], 14, $context, $this->getSourceContext());
            // line 23
            echo "
        ";
            // line 24
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 24, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 26
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_role_index")], 26, $context, $this->getSourceContext());
        echo "
    ";
        // line 27
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["class" => "btn-success role-submit", "route" => "oro_user_role_view", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 27, $context, $this->getSourceContext());
        // line 32
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_role_create")) {
            // line 33
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_user_role_create"]], 33, $context, $this->getSourceContext()));
            // line 36
            echo "    ";
        }
        // line 37
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 37), "value", [], "any", false, false, false, 37), "id", [], "any", false, false, false, 37) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_user_role_update"))) {
            // line 38
            echo "        ";
            // line 39
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["class" => "btn-success main-group role-submit", "route" => "oro_user_role_update", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 39, $context, $this->getSourceContext()));
            // line 44
            echo "    ";
        }
        // line 45
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 45, $context, $this->getSourceContext());
        echo "
    ";
        // line 46
        $context["fields"] = [];
        // line 47
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["name"] => $context["child"]) {
            // line 48
            echo "        ";
            if (!twig_in_filter($context["name"], [0 => "appendUsers", 1 => "removeUsers", 2 => "privileges"])) {
                // line 49
                echo "            ";
                $context["fields"] = twig_array_merge(($context["fields"] ?? null), [$context["name"] => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 49), "id", [], "any", false, false, false, 49))]);
                // line 50
                echo "        ";
            }
            // line 51
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['name'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 52
        echo "    ";
        $context["options"] = ["elSelector" => ".btn-success.role-submit", "formName" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 54
($context["form"] ?? null), "vars", [], "any", false, false, false, 54), "name", [], "any", false, false, false, 54), "formSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 55
($context["form"] ?? null), "vars", [], "any", false, false, false, 55), "id", [], "any", false, false, false, 55)), "privilegesSelector" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 56
($context["form"] ?? null), "privileges", [], "any", false, false, false, 56), "vars", [], "any", false, false, false, 56), "id", [], "any", false, false, false, 56)), "appendUsersSelector" => "#roleAppendUsers", "removeUsersSelector" => "#roleRemoveUsers", "fields" =>         // line 59
($context["fields"] ?? null)];
        // line 61
        echo "    <div data-page-component-module=\"orouser/js/views/role-view\"
         data-page-component-options=\"";
        // line 62
        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
        echo "\">
    </div>
";
    }

    // line 66
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 68
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 69
($context["form"] ?? null), "vars", [], "any", false, false, false, 69), "value", [], "any", false, false, false, 69), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_role_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 72
($context["form"] ?? null), "vars", [], "any", false, false, false, 72), "value", [], "any", false, false, false, 72), "label", [], "any", false, false, false, 72)];
            // line 75
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 77
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.entity_label")]);
            // line 78
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroUser/Role/update.html.twig", 78)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 79
            echo "    ";
        }
    }

    // line 82
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 83
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Role/update.html.twig", 83)->unwrap();
        // line 84
        echo "
    ";
        // line 85
        $context["id"] = "role-profile";
        // line 86
        echo "
    ";
        // line 87
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 93
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "appendUsers", [], "any", false, false, false, 93), 'widget', ["id" => "roleAppendUsers"]), 1 =>         // line 94
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "removeUsers", [], "any", false, false, false, 94), 'widget', ["id" => "roleRemoveUsers"]), 2 =>         // line 95
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "label", [], "any", false, false, false, 95), 'row')]]]]];
        // line 100
        echo "
    ";
        // line 101
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 102
        echo "
    ";
        // line 103
        ob_start(function () { return ''; });
        // line 104
        echo "        ";
        $context["entityTabPanelId"] = uniqid("entity-tab-panel-");
        // line 105
        echo "        ";
        $context["tabsOptions"] = twig_array_merge(($context["tabsOptions"] ?? null), ["controlTabPanel" =>         // line 106
($context["entityTabPanelId"] ?? null)]);
        // line 108
        echo "
        <div ";
        // line 109
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orouser/js/components/role/entity-category-tabs-component", "options" =>         // line 111
($context["tabsOptions"] ?? null)]], 109, $context, $this->getSourceContext());
        // line 112
        echo "></div>
        <div id=\"";
        // line 113
        echo twig_escape_filter($this->env, ($context["entityTabPanelId"] ?? null), "html", null, true);
        echo "\" class=\"tab-content\" role=\"tabpanel\">
            ";
        // line 114
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["role-permission-grid", ["role" => ($context["entity"] ?? null)], ["cssClass" => "inner-permissions-grid", "themeOptions" => ["readonly" => false]]], 114, $context, $this->getSourceContext());
        echo "
            <div ";
        // line 115
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orouser/js/components/role/capability-set-component", "options" =>         // line 117
($context["capabilitySetOptions"] ?? null)]], 115, $context, $this->getSourceContext());
        // line 118
        echo "></div>
        </div>
    ";
        $context["rolePermissionsGrid"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 121
        echo "
    ";
        // line 122
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 =         // line 123
($context["privilegesConfig"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0["entity"] ?? null) : null), "label", [], "any", false, false, false, 123)), "subblocks" => [0 => ["title" => "", "useSpan" => false, "data" => [0 =>         // line 129
($context["rolePermissionsGrid"] ?? null)]]]]]);
        // line 134
        echo "
    ";
        // line 135
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_plural_label"), "subblocks" => [0 => ["title" => null, "useSpan" => false, "data" => [0 => twig_call_macro($macros["dataGrid"], "macro_renderGrid", [        // line 141
($context["gridName"] ?? null), ["role_id" => ($context["entityId"] ?? null)], ["cssClass" => "inner-grid"]], 141, $context, $this->getSourceContext())]]]]]);
        // line 145
        echo "
    ";
        // line 146
        $context["data"] = ["formErrors" => ((        // line 147
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 148
($context["dataBlocks"] ?? null)];
        // line 150
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroUser/Role/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  272 => 150,  270 => 148,  269 => 147,  268 => 146,  265 => 145,  263 => 141,  262 => 135,  259 => 134,  257 => 129,  256 => 123,  255 => 122,  252 => 121,  247 => 118,  245 => 117,  244 => 115,  240 => 114,  236 => 113,  233 => 112,  231 => 111,  230 => 109,  227 => 108,  225 => 106,  223 => 105,  220 => 104,  218 => 103,  215 => 102,  213 => 101,  210 => 100,  208 => 95,  207 => 94,  206 => 93,  205 => 87,  202 => 86,  200 => 85,  197 => 84,  194 => 83,  190 => 82,  185 => 79,  182 => 78,  179 => 77,  173 => 75,  171 => 72,  170 => 69,  168 => 68,  165 => 67,  161 => 66,  154 => 62,  151 => 61,  149 => 59,  148 => 56,  147 => 55,  146 => 54,  144 => 52,  138 => 51,  135 => 50,  132 => 49,  129 => 48,  124 => 47,  122 => 46,  117 => 45,  114 => 44,  111 => 39,  109 => 38,  106 => 37,  103 => 36,  100 => 33,  97 => 32,  95 => 27,  90 => 26,  85 => 24,  82 => 23,  80 => 21,  79 => 18,  78 => 15,  76 => 14,  74 => 13,  70 => 12,  67 => 11,  64 => 10,  60 => 9,  55 => 1,  53 => 7,  51 => 6,  49 => 5,  46 => 4,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Role/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Role/update.html.twig");
    }
}
