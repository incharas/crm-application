<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/validation-message-handler/date-validation-message-handler-view.js */
class __TwigTemplate_2217e20841e567dfe4c529a7e10e977d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const AbstractValidationMessageHandlerView =
        require('oroform/js/app/views/validation-message-handler/abstract-validation-message-handler-view');

    const DateValidationMessageHandlerView = AbstractValidationMessageHandlerView.extend({
        events: {
            'datepicker:dialogHide': 'onDatepickerDialogReposition',
            'datepicker:dialogReposition': 'onDatepickerDialogReposition'
        },

        /**
         * @inheritdoc
         */
        constructor: function DateValidationMessageHandlerView(options) {
            DateValidationMessageHandlerView.__super__.constructor.call(this, options);
        },

        isActive: function() {
            return this.\$el.data('datepicker').dpDiv.is(':visible') && this.\$el.is(\$.datepicker._lastInput) &&
                this.\$el.hasClass('ui-datepicker-dialog-is-below');
        },

        getPopperReferenceElement: function() {
            return this.\$el;
        },

        onDatepickerDialogReposition: function(e, position) {
            this.active = position === 'below';

            this.update();
        }
    }, {
        test: function(element) {
            return \$(element).hasClass('datepicker-input');
        }
    });

    return DateValidationMessageHandlerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/validation-message-handler/date-validation-message-handler-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/validation-message-handler/date-validation-message-handler-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/validation-message-handler/date-validation-message-handler-view.js");
    }
}
