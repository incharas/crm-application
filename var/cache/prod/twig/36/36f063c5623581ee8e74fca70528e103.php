<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/Dashboard/recentEmails.html.twig */
class __TwigTemplate_1c4d3279c11a776b99a449936b79652f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'actions' => [$this, 'block_actions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/tabbedWidget.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEmail/Dashboard/recentEmails.html.twig", 2)->unwrap();
        // line 4
        ob_start(function () { return ''; });
        // line 5
        echo "    <div class=\"email-mail-count-circle\" ";
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["view" => "oroemail/js/app/views/unread-emails-counter-view", "count" =>         // line 9
($context["unreadMailCount"] ?? null), "autoRender" => true]]], 5, $context, $this->getSourceContext());
        // line 12
        echo "></div>
";
        $context["unreadMailCount"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 1
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/tabbedWidget.html.twig", "@OroEmail/Dashboard/recentEmails.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 15
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "    ";
        $context["tabs"] = [0 => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_dashboard_recent_emails", ["widget" =>         // line 19
($context["widgetName"] ?? null), "activeTab" => "inbox", "contentType" => "tab"]), "name" => "inbox", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.recent_emails.inbox")], 1 => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_dashboard_recent_emails", ["widget" =>         // line 27
($context["widgetName"] ?? null), "activeTab" => "sent", "contentType" => "tab"]), "name" => "sent", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.recent_emails.sent")], 2 => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_dashboard_recent_emails", ["widget" =>         // line 35
($context["widgetName"] ?? null), "activeTab" => "new", "contentType" => "tab"]), "name" => "unread", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.recent_emails.unread"), "afterHtml" =>         // line 39
($context["unreadMailCount"] ?? null)]];
        // line 41
        echo "
    ";
        // line 42
        $this->displayParentBlock("content", $context, $blocks);
        echo "
";
    }

    // line 45
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 46
        echo "    ";
        $context["actions"] = [0 => ["url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_user_emails", ["id" =>         // line 47
($context["loggedUserId"] ?? null)]), "type" => "link", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.recent_emails.view_all")]];
        // line 51
        echo "
    ";
        // line 52
        $this->displayParentBlock("actions", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroEmail/Dashboard/recentEmails.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 52,  88 => 51,  86 => 47,  84 => 46,  80 => 45,  74 => 42,  71 => 41,  69 => 39,  68 => 35,  67 => 27,  66 => 19,  64 => 16,  60 => 15,  55 => 1,  51 => 12,  49 => 9,  47 => 5,  45 => 4,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/Dashboard/recentEmails.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/Dashboard/recentEmails.html.twig");
    }
}
