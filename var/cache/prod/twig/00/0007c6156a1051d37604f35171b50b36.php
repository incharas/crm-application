<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroImportExport/ImportExport/buttons.html.twig */
class __TwigTemplate_0029505e87514009d3c3e82b6335e91c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroImportExport/ImportExport/buttons.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        $context["hasImportProcessor"] = (array_key_exists("importProcessor", $context) &&  !twig_test_empty(($context["importProcessor"] ?? null)));
        // line 4
        $context["importAllowed"] = (($context["hasImportProcessor"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_importexport_import"));
        // line 5
        $context["importLabel"] = ((array_key_exists("importLabel", $context)) ? (_twig_default_filter(($context["importLabel"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.label"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.label")));
        // line 6
        $context["importValidationLabel"] = ((array_key_exists("importValidationLabel", $context)) ? (_twig_default_filter(($context["importValidationLabel"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.validation_label"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.validation_label")));
        // line 7
        echo "
";
        // line 8
        $context["hasExportProcessor"] = (array_key_exists("exportProcessor", $context) &&  !twig_test_empty(($context["exportProcessor"] ?? null)));
        // line 9
        $context["exportAllowed"] = (($context["hasExportProcessor"] ?? null) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_importexport_export"));
        // line 10
        if (($context["exportAllowed"] ?? null)) {
            // line 11
            echo "    ";
            $context["isExportPopupRequired"] = (twig_test_iterable(($context["exportProcessor"] ?? null)) && (twig_length_filter($this->env, ($context["exportProcessor"] ?? null)) > 1));
        }
        // line 13
        $context["exportLabel"] = ((array_key_exists("exportLabel", $context)) ? (_twig_default_filter(($context["exportLabel"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export.label"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export.label")));
        // line 14
        echo "
";
        // line 15
        $context["hasExportTemplateProcessor"] = (array_key_exists("exportTemplateProcessor", $context) &&  !twig_test_empty(($context["exportTemplateProcessor"] ?? null)));
        // line 16
        if (($context["hasExportTemplateProcessor"] ?? null)) {
            // line 17
            echo "    ";
            $context["isExportTemplatePopupRequired"] = twig_test_iterable(($context["exportTemplateProcessor"] ?? null));
            // line 18
            echo "    ";
            $context["exportTemplateProcessor"] = ((twig_test_iterable(($context["exportTemplateProcessor"] ?? null))) ? ((($__internal_compile_0 = ($context["exportTemplateProcessor"] ?? null)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null)) : (($context["exportTemplateProcessor"] ?? null)));
        }
        // line 20
        echo "
";
        // line 21
        $context["exportTemplateLabel"] = ((array_key_exists("exportTemplateLabel", $context)) ? (_twig_default_filter(($context["exportTemplateLabel"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export_template.label"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export_template.label")));
        // line 22
        echo "
";
        // line 23
        if ((($context["exportAllowed"] ?? null) || ($context["importAllowed"] ?? null))) {
            // line 24
            echo "    <span class=\"pull-left\"
        data-page-component-module=\"oroui/js/app/components/view-component\"
        data-page-component-options=\"";
            // line 26
            echo twig_escape_filter($this->env, json_encode(["view" => "oroimportexport/js/app/views/import-export-buttons-view", "data" => ["entity" =>             // line 29
($context["entity_class"] ?? null), "routeOptions" => ((            // line 30
array_key_exists("options", $context)) ? (_twig_default_filter(($context["options"] ?? null), [])) : ([])), "importTitle" => ((            // line 32
array_key_exists("importTitle", $context)) ? (_twig_default_filter(($context["importTitle"] ?? null), ($context["importLabel"] ?? null))) : (($context["importLabel"] ?? null))), "importJob" => ((            // line 33
array_key_exists("importJob", $context)) ? (_twig_default_filter(($context["importJob"] ?? null), null)) : (null)), "importValidateJob" => ((            // line 34
array_key_exists("importValidateJob", $context)) ? (_twig_default_filter(($context["importValidateJob"] ?? null), null)) : (null)), "exportTitle" => ((            // line 36
array_key_exists("exportTitle", $context)) ? (_twig_default_filter(($context["exportTitle"] ?? null), ($context["exportLabel"] ?? null))) : (($context["exportLabel"] ?? null))), "exportJob" => ((            // line 37
array_key_exists("exportJob", $context)) ? (_twig_default_filter(($context["exportJob"] ?? null), null)) : (null)), "exportProcessor" => ((            // line 38
array_key_exists("exportProcessor", $context)) ? (_twig_default_filter(($context["exportProcessor"] ?? null), null)) : (null)), "isExportPopupRequired" => ((            // line 39
array_key_exists("isExportPopupRequired", $context)) ? (_twig_default_filter(($context["isExportPopupRequired"] ?? null), false)) : (false)), "exportTemplateTitle" => ((            // line 41
array_key_exists("exportTemplateTitle", $context)) ? (_twig_default_filter(($context["exportTemplateTitle"] ?? null), ($context["exportLabel"] ?? null))) : (($context["exportLabel"] ?? null))), "exportTemplateJob" => ((            // line 42
array_key_exists("exportTemplateJob", $context)) ? (_twig_default_filter(($context["exportTemplateJob"] ?? null), null)) : (null)), "exportTemplateProcessor" => ((            // line 43
array_key_exists("exportTemplateProcessor", $context)) ? (_twig_default_filter(($context["exportTemplateProcessor"] ?? null), null)) : (null)), "isExportTemplatePopupRequired" => ((            // line 44
array_key_exists("isExportTemplatePopupRequired", $context)) ? (_twig_default_filter(($context["isExportTemplatePopupRequired"] ?? null), false)) : (false)), "filePrefix" => ((            // line 46
array_key_exists("filePrefix", $context)) ? (_twig_default_filter(($context["filePrefix"] ?? null), null)) : (null)), "datagridName" => ((            // line 47
array_key_exists("datagridName", $context)) ? (_twig_default_filter(($context["datagridName"] ?? null), null)) : (null)), "afterRefreshPageMessage" => ((            // line 48
array_key_exists("afterRefreshPageMessage", $context)) ? (_twig_default_filter(($context["afterRefreshPageMessage"] ?? null), null)) : (null)), "refreshPageOnSuccess" => ((            // line 49
array_key_exists("refreshPageOnSuccess", $context)) ? (_twig_default_filter(($context["refreshPageOnSuccess"] ?? null), false)) : (false))]]), "html", null, true);
            // line 51
            echo "\"
    >
    ";
            // line 53
            if (($context["exportAllowed"] ?? null)) {
                // line 54
                echo "        <div class=\"btn-group\">
            <a href=\"#\" role=\"button\" class=\"btn export-btn icons-holder-text no-hash\">
                <span class=\"fa-upload hide-text\">";
                // line 56
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export.label"), "html", null, true);
                echo "</span>";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.export.label"), "html", null, true);
                echo "
            </a>
        </div>
    ";
            }
            // line 60
            echo "
    ";
            // line 61
            if (($context["importAllowed"] ?? null)) {
                // line 62
                echo "        <div class=\"btn-group\">
            ";
                // line 63
                ob_start(function () { return ''; });
                // line 64
                echo "                <a href=\"#\" role=\"button\" class=\"btn import-btn icons-holder-text no-hash\">
                    <span class=\"fa-download hide-text\">";
                // line 65
                echo twig_escape_filter($this->env, ($context["importLabel"] ?? null), "html", null, true);
                echo "</span>";
                echo twig_escape_filter($this->env, ($context["importLabel"] ?? null), "html", null, true);
                echo "
                </a>
                <a href=\"#\" role=\"button\" class=\"btn import-validation-btn icons-holder-text no-hash\">
                    <span class=\"icon-download-alt hide-text\">";
                // line 68
                echo twig_escape_filter($this->env, ($context["importValidationLabel"] ?? null), "html", null, true);
                echo "</span>";
                echo twig_escape_filter($this->env, ($context["importValidationLabel"] ?? null), "html", null, true);
                echo "
                </a>
                ";
                // line 70
                if (($context["hasExportTemplateProcessor"] ?? null)) {
                    // line 71
                    echo "                    <a href=\"#\" role=\"button\" class=\"btn template-btn icons-holder-text no-hash\">
                        <span class=\"fa-file-o hide-text\"></span>";
                    // line 72
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.importexport.import.tempate"), "html", null, true);
                    echo "
                    </a>
                ";
                }
                // line 75
                echo "            ";
                $context["buttonsHtml"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 76
                echo "
            ";
                // line 77
                $context["parameters"] = ["html" =>                 // line 78
($context["buttonsHtml"] ?? null)];
                // line 80
                echo "            ";
                echo twig_call_macro($macros["UI"], "macro_pinnedDropdownButton", [($context["parameters"] ?? null)], 80, $context, $this->getSourceContext());
                echo "
        </div>
    ";
            }
            // line 83
            echo "    </span>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroImportExport/ImportExport/buttons.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  186 => 83,  179 => 80,  177 => 78,  176 => 77,  173 => 76,  170 => 75,  164 => 72,  161 => 71,  159 => 70,  152 => 68,  144 => 65,  141 => 64,  139 => 63,  136 => 62,  134 => 61,  131 => 60,  122 => 56,  118 => 54,  116 => 53,  112 => 51,  110 => 49,  109 => 48,  108 => 47,  107 => 46,  106 => 44,  105 => 43,  104 => 42,  103 => 41,  102 => 39,  101 => 38,  100 => 37,  99 => 36,  98 => 34,  97 => 33,  96 => 32,  95 => 30,  94 => 29,  93 => 26,  89 => 24,  87 => 23,  84 => 22,  82 => 21,  79 => 20,  75 => 18,  72 => 17,  70 => 16,  68 => 15,  65 => 14,  63 => 13,  59 => 11,  57 => 10,  55 => 9,  53 => 8,  50 => 7,  48 => 6,  46 => 5,  44 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroImportExport/ImportExport/buttons.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Resources/views/ImportExport/buttons.html.twig");
    }
}
