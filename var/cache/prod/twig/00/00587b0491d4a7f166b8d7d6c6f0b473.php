<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/extend/jquery-ui/widgets/dialog.js */
class __TwigTemplate_751f0bca89c83a4954e7c1dedd1b6056 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "import \$ from 'jquery';
import 'jquery-ui/widgets/dialog';

\$.widget('ui.dialog', \$.ui.dialog, {
    /**
     * Replace method because some browsers return string 'auto' if property z-index not specified.
     * */
    _moveToTop: function() {
        const zIndex = this.uiDialog.css('z-index');
        const numberRegexp = /^\\d+\$/;
        if (typeof zIndex === 'string' && !numberRegexp.test(zIndex)) {
            this.uiDialog.css('z-index', 910);
        }
        this._super();
    },

    _title: function(title) {
        title.html(
            \$('<span/>', {'class': 'ui-dialog-title__inner'}).text(this.options.title)
        );
    }
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/extend/jquery-ui/widgets/dialog.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/extend/jquery-ui/widgets/dialog.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/extend/jquery-ui/widgets/dialog.js");
    }
}
