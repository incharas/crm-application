<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/jstree/jstree-actions.scss */
class __TwigTemplate_ddccf5062a891dc9bbd7b972d9b7cc97 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.jstree-actions {
    display: \$jstree-actions-display;

    &__handle {
        color: \$jstree-actions-handle-color;

        &.btn {
            padding: \$jstree-actions-handle-offset;
            width: 16px;
        }

        &:hover {
            color: \$jstree-actions-handle-hover-color;
        }
    }

    &__menu {
        position: \$jstree-actions-menu-position;

        &--inline {
            margin: \$jstree-actions-menu-inline-margin;
            align-items: \$jstree-actions-menu-inline-li-align-items;
            list-style: \$jstree-actions-menu-inline-list-style;

            li {
                display: \$jstree-actions-menu-inline-li-display;
                padding-right: \$jstree-actions-menu-item-gap;

                &:not(:first-child)::before {
                    content: \$jstree-actions-menu-inline-li-before-content;
                    height: \$jstree-actions-menu-inline-li-before-height;
                    border-left: 1px solid \$jstree-actions-menu-separator-color;
                    margin-right: \$jstree-actions-menu-item-gap;
                    align-self: center;
                }

                .action {
                    padding: 0;
                }

                [class*='fa-'] {
                    display: none;
                }
            }
        }
    }

    &__icon {
        text-align: \$jstree-actions-icon-text-align;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/jstree/jstree-actions.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/jstree/jstree-actions.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/jstree/jstree-actions.scss");
    }
}
