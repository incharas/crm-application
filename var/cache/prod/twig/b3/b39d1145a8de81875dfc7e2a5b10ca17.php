<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/mobile/inline-actions.scss */
class __TwigTemplate_12aa1df67ce66629be7c5a843115e0bb extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

.inline-actions-element_actions {
    .btn {
        font: 0/0 a, sans-serif;
        width: 18px;
        height: 18px;
        margin-left: \$content-padding-small;

        &:first-of-type {
            margin-left: 0;
        }

        [class^='fa-'],
        [class*=' fa-'] {
            width: 18px;
            height: 18px;

            &::before {
                font-size: 18px;
                line-height: 18px;
                height: 18px;
            }
        }
    }
}

.inline-actions-element {
    top: 0;
    padding: 0;
    margin: 0;

    .inline-actions-element_actions {
        visibility: visible;
    }

    .inline-actions-element_wrapper {
        line-height: 18px;
        padding: 0;
        margin: 0 8px 0 0;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/mobile/inline-actions.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/mobile/inline-actions.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/mobile/inline-actions.scss");
    }
}
