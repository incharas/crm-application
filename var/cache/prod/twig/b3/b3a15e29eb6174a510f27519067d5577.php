<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDotmailer/DataField/update.html.twig */
class __TwigTemplate_2428ebfccb2cb6c89cb2e1b2daa2e602 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'bodyClass' => [$this, 'block_bodyClass'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafield.entity_label")]]);
        // line 7
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 7);
        // line 9
        $context["formAction"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_create");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroDotmailer/DataField/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_bodyClass($context, array $blocks = [])
    {
        $macros = $this->macros;
        echo "dotmailer-page";
    }

    // line 11
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 12
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDotmailer/DataField/update.html.twig", 12)->unwrap();
        // line 13
        echo "
    ";
        // line 14
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_dotmailer_datafield_view", "params" => ["id" => "\$id"]]], 14, $context, $this->getSourceContext());
        // line 18
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_dotmailer_datafield_create")) {
            // line 19
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_dotmailer_datafield_create"]], 19, $context, $this->getSourceContext()));
            // line 22
            echo "    ";
        }
        // line 23
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 23, $context, $this->getSourceContext());
        echo "
    ";
        // line 24
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_index")], 24, $context, $this->getSourceContext());
        echo "
";
    }

    // line 27
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 29
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 30
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dotmailer_datafield_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafield.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 33
($context["entity"] ?? null), "name", [], "any", false, false, false, 33)];
            // line 35
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 37
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.datafield.entity_label")]);
            // line 38
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroDotmailer/DataField/update.html.twig", 38)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 39
            echo "    ";
        }
    }

    // line 42
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    ";
        $context["id"] = "dotmailer-datafield-form";
        // line 44
        echo "
    ";
        // line 45
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.block.general"), "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 50
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "channel", [], "any", false, false, false, 50), 'row'), 1 =>         // line 51
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 51), 'row'), 2 =>         // line 52
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 52), 'row'), 3 =>         // line 53
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "visibility", [], "any", false, false, false, 53), 'row'), 4 =>         // line 54
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "defaultValue", [], "any", false, false, false, 54), 'row')]]]]];
        // line 58
        echo "
    ";
        // line 59
        $context["additionalData"] = [0 =>         // line 60
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "notes", [], "any", false, false, false, 60), 'row')];
        // line 62
        echo "
    ";
        // line 63
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dotmailer.block.additional"), ($context["additionalData"] ?? null)));
        // line 64
        echo "
    ";
        // line 65
        $context["data"] = ["formErrors" => ((        // line 66
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 67
($context["dataBlocks"] ?? null)];
        // line 69
        echo "    ";
        // line 70
        echo "    ";
        $context["fieldsToSendOnTypeChange"] = [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 71
($context["form"] ?? null), "type", [], "any", false, false, false, 71), "vars", [], "any", false, false, false, 71), "full_name", [], "any", false, false, false, 71), 1 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 72
($context["form"] ?? null), "name", [], "any", false, false, false, 72), "vars", [], "any", false, false, false, 72), "full_name", [], "any", false, false, false, 72), 2 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 73
($context["form"] ?? null), "visibility", [], "any", false, false, false, 73), "vars", [], "any", false, false, false, 73), "full_name", [], "any", false, false, false, 73), 3 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 74
($context["form"] ?? null), "notes", [], "any", false, false, false, 74), "vars", [], "any", false, false, false, 74), "full_name", [], "any", false, false, false, 74), 4 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 75
($context["form"] ?? null), "_token", [], "any", false, false, false, 75), "vars", [], "any", false, false, false, 75), "full_name", [], "any", false, false, false, 75)];
        // line 77
        echo "    <script type=\"text/javascript\">
        loadModules(['orodotmailer/js/datafield-view'], function (DataFieldView) {
            \"use strict\";

            \$(function () {
                var options = {
                    formSelector: '#";
        // line 83
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 83), "id", [], "any", false, false, false, 83), "html", null, true);
        echo "',
                    typeSelector: '#";
        // line 84
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 84), "vars", [], "any", false, false, false, 84), "id", [], "any", false, false, false, 84), "html", null, true);
        echo "',
                    fieldsSets: {
                        type: ";
        // line 86
        echo json_encode(($context["fieldsToSendOnTypeChange"] ?? null));
        echo ",
                    }
                };

                new DataFieldView(options);
            });
        });
    </script>
    ";
        // line 94
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroDotmailer/DataField/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  198 => 94,  187 => 86,  182 => 84,  178 => 83,  170 => 77,  168 => 75,  167 => 74,  166 => 73,  165 => 72,  164 => 71,  162 => 70,  160 => 69,  158 => 67,  157 => 66,  156 => 65,  153 => 64,  151 => 63,  148 => 62,  146 => 60,  145 => 59,  142 => 58,  140 => 54,  139 => 53,  138 => 52,  137 => 51,  136 => 50,  135 => 45,  132 => 44,  129 => 43,  125 => 42,  120 => 39,  117 => 38,  114 => 37,  108 => 35,  106 => 33,  105 => 30,  103 => 29,  100 => 28,  96 => 27,  90 => 24,  85 => 23,  82 => 22,  79 => 19,  76 => 18,  74 => 14,  71 => 13,  68 => 12,  64 => 11,  57 => 10,  52 => 1,  50 => 9,  48 => 7,  46 => 4,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDotmailer/DataField/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-dotmailer/Resources/views/DataField/update.html.twig");
    }
}
