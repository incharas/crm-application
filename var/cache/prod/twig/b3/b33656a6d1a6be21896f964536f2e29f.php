<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/components/select2-grid-component.js */
class __TwigTemplate_39eb4df8a74c7c665eea551150a8e300 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const _ = require('underscore');
    const Select2Component = require('oro/select2-component');

    const Select2GridComponent = Select2Component.extend({
        /**
         * @inheritdoc
         */
        constructor: function Select2GridComponent(options) {
            Select2GridComponent.__super__.constructor.call(this, options);
        },

        preConfig: function(config) {
            Select2GridComponent.__super__.preConfig.call(this, config);
            const that = this;
            const grid = config.grid;
            const gridName = grid.name;
            _.extend(config.ajax, {
                data: function(query, page, searchById) {
                    const result = {};
                    let sortByKey;
                    if (searchById) {
                        result[gridName + '[_pager][_page]'] = 1;
                        result[gridName + '[_pager][_per_page]'] = 1;
                        result[gridName + '[_filter][id][type]'] = 3;
                        result[gridName + '[_filter][id][value]'] = query;
                    } else {
                        sortByKey = grid.sort_by || config.properties[0];
                        result[gridName + '[_pager][_page]'] = page;
                        result[gridName + '[_pager][_per_page]'] = that.perPage;
                        result[gridName + '[_sort_by][' + sortByKey + ']'] = grid.sort_order || 'ASC';
                        result[gridName + '[_filter][' + config.properties[0] + '][type]'] = 1;
                        result[gridName + '[_filter][' + config.properties[0] + '][value]'] = query;
                    }
                    return result;
                },
                results: function(data, page) {
                    return {
                        results: data.data,
                        more: page * that.perPage < data.options.totalRecords
                    };
                }
            });
            return config;
        }
    });
    return Select2GridComponent;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/components/select2-grid-component.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/components/select2-grid-component.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/components/select2-grid-component.js");
    }
}
