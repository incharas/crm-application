<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroHangoutsCall/js_modules_config.html.twig */
class __TwigTemplate_b4f4ae83fd68989315aaf2a3bf38f4d6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroHangoutsCall/js_modules_config.html.twig", 1)->unwrap();
        // line 2
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["orohangoutscall/js/app/views/start-button-view" => ["initialApps" => $this->extensions['Oro\Bundle\HangoutsCallBundle\Twig\OroHangoutsCallExtension']->getInitialApps(), "appHost" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 5
($context["app"] ?? null), "request", [], "any", false, false, false, 5), "host", [], "any", false, false, false, 5)]]], 2, $context, $this->getSourceContext());
        // line 7
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroHangoutsCall/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 7,  40 => 5,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroHangoutsCall/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-hangouts-call-bundle/Resources/views/js_modules_config.html.twig");
    }
}
