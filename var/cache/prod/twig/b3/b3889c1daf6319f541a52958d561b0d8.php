<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/extend/tinymce.js */
class __TwigTemplate_0f9c300bbf5643c17bb09a5cce1306d8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "const _ = require('underscore');
const tinyMCE = require('tinymce');
require.context(
    '!file-loader?name=[path][name].[ext]&outputPath=../_static/&context=tinymce!tinymce/icons',
    true,
    /.*/
);
require.context(
    '!file-loader?name=[path][name].[ext]&outputPath=../_static/&context=tinymce!tinymce/models',
    true,
    /.*/
);
require.context(
    '!file-loader?name=[path][name].[ext]&outputPath=../_static/&context=tinymce!tinymce/plugins',
    true,
    /.*/
);
require.context(
    '!file-loader?name=[path][name].[ext]&outputPath=../_static/&context=tinymce!tinymce/skins',
    true,
    /.*/
);
require.context(
    '!file-loader?name=[path][name].[ext]&outputPath=../_static/&context=tinymce!tinymce/themes',
    true,
    /.*/
);
const moduleConfig = require('module-config').default(module.id);

_.extend(tinyMCE, _.pick(moduleConfig, 'baseURL'));

module.exports = tinyMCE;
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/extend/tinymce.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/extend/tinymce.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/extend/tinymce.js");
    }
}
