<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReport/Report/update.html.twig */
class __TwigTemplate_3faf76431952634b775ab8662cabc495 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'head_script' => [$this, 'block_head_script'],
            'stylesheets' => [$this, 'block_stylesheets'],
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $this->env->getRuntime("Symfony\\Component\\Form\\FormRenderer")->setTheme(($context["form"] ?? null), [0 => "@OroForm/Form/fields.html.twig", 1 => "@OroReport/Form/fields.html.twig"], true);
        // line 6
        $macros["QD"] = $this->macros["QD"] = $this->loadTemplate("@OroQueryDesigner/macros.html.twig", "@OroReport/Report/update.html.twig", 6)->unwrap();
        // line 7
        $macros["segmentQD"] = $this->macros["segmentQD"] = $this->loadTemplate("@OroSegment/macros.html.twig", "@OroReport/Report/update.html.twig", 7)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%report.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["entity"] ?? null), "name", [], "any", false, false, false, 9)]]);
        // line 10
        $context["formAction"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "value", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 10), "value", [], "any", false, false, false, 10), "id", [], "any", false, false, false, 10)])) : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_create")));
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroReport/Report/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 12
    public function block_head_script($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    ";
        $this->displayParentBlock("head_script", $context, $blocks);
        echo "

    ";
        // line 15
        $this->displayBlock('stylesheets', $context, $blocks);
    }

    public function block_stylesheets($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 16
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'stylesheet');
        echo "
    ";
    }

    // line 20
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroReport/Report/update.html.twig", 21)->unwrap();
        // line 22
        echo "
    ";
        // line 23
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 23), "value", [], "any", false, false, false, 23), "id", [], "any", false, false, false, 23) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("CREATE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 23), "value", [], "any", false, false, false, 23)))) {
            // line 24
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_clone", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 25
($context["form"] ?? null), "vars", [], "any", false, false, false, 25), "value", [], "any", false, false, false, 25), "id", [], "any", false, false, false, 25)]), "iCss" => "fa-files-o", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.action.clone.button.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.action.clone.button.label")]], 24, $context, $this->getSourceContext());
            // line 29
            echo "
    ";
        }
        // line 31
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 31), "value", [], "any", false, false, false, 31), "id", [], "any", false, false, false, 31) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 31), "value", [], "any", false, false, false, 31)))) {
            // line 32
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_report", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 33
($context["form"] ?? null), "vars", [], "any", false, false, false, 33), "value", [], "any", false, false, false, 33), "id", [], "any", false, false, false, 33)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-report", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 37
($context["form"] ?? null), "vars", [], "any", false, false, false, 37), "value", [], "any", false, false, false, 37), "id", [], "any", false, false, false, 37), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.entity_label")]], 32, $context, $this->getSourceContext());
            // line 39
            echo "

        ";
            // line 41
            echo twig_call_macro($macros["UI"], "macro_buttonSeparator", [], 41, $context, $this->getSourceContext());
            echo "
    ";
        }
        // line 43
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_index")], 43, $context, $this->getSourceContext());
        echo "
    ";
        // line 44
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_report_view", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 44, $context, $this->getSourceContext());
        // line 48
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_report_create")) {
            // line 49
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_report_create"]], 49, $context, $this->getSourceContext()));
            // line 52
            echo "    ";
        }
        // line 53
        echo "    ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 53), "value", [], "any", false, false, false, 53), "id", [], "any", false, false, false, 53) || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_report_update"))) {
            // line 54
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_report_update", "params" => ["id" => "\$id", "_enableContentProviders" => "mainMenu"]]], 54, $context, $this->getSourceContext()));
            // line 58
            echo "    ";
        }
        // line 59
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 59, $context, $this->getSourceContext());
        echo "
";
    }

    // line 62
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 63
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 63), "value", [], "any", false, false, false, 63), "id", [], "any", false, false, false, 63)) {
            // line 64
            echo "        ";
            $context["breadcrumbs"] = ["entity" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 65
($context["form"] ?? null), "vars", [], "any", false, false, false, 65), "value", [], "any", false, false, false, 65), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 68
($context["entity"] ?? null), "name", [], "any", false, false, false, 68)];
            // line 70
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 72
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.entity_label")]);
            // line 73
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroReport/Report/update.html.twig", 73)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 74
            echo "    ";
        }
    }

    // line 77
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 78
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroReport/Report/update.html.twig", 78)->unwrap();
        // line 79
        echo "
    ";
        // line 80
        $context["id"] = "report-profile";
        // line 81
        echo "    ";
        $context["ownerDataBlock"] = ["dataBlocks" => [0 => ["subblocks" => [0 => ["data" => []]]]]];
        // line 88
        echo "
    ";
        // line 89
        $context["ownerDataBlock"] = $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->processForm($this->env, ($context["ownerDataBlock"] ?? null), ($context["form"] ?? null));
        // line 90
        echo "    ";
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 97
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 97), 'row', ["label" => "oro.report.name.label"]), 1 =>         // line 98
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 98), 'row', ["label" => "oro.report.description.label", "attr" => ["class" => "report-descr"]])]], 1 => ["title" => "", "data" => twig_array_merge([0 =>         // line 109
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "entity", [], "any", false, false, false, 109), 'row', ["label" => "oro.report.entity.label"]), 1 =>         // line 110
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "type", [], "any", false, false, false, 110), 'row', ["label" => "oro.report.type.label"])], Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_0 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, (($__internal_compile_1 = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 111
($context["ownerDataBlock"] ?? null), "dataBlocks", [], "any", false, false, false, 111)) && is_array($__internal_compile_1) || $__internal_compile_1 instanceof ArrayAccess ? ($__internal_compile_1[0] ?? null) : null), "subblocks", [], "any", false, false, false, 111)) && is_array($__internal_compile_0) || $__internal_compile_0 instanceof ArrayAccess ? ($__internal_compile_0[0] ?? null) : null), "data", [], "any", false, false, false, 111))]]]];
        // line 116
        echo "
    ";
        // line 117
        $context["type"] = "oro_report";
        // line 118
        echo "    ";
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.form.designer"), "content_attr" => ["id" => (        // line 120
($context["type"] ?? null) . "-designer")], "subblocks" => [0 => ["data" => [0 => twig_call_macro($macros["UI"], "macro_scrollSubblock", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.form.columns"), [0 => twig_call_macro($macros["QD"], "macro_query_designer_column_form", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 128
($context["form"] ?? null), "column", [], "any", false, false, false, 128), ["id" => (        // line 129
($context["type"] ?? null) . "-column-form")]], 127, $context, $this->getSourceContext()), 1 => twig_call_macro($macros["QD"], "macro_query_designer_column_list", [["id" => (        // line 133
($context["type"] ?? null) . "-column-list"), "rowId" => (        // line 134
($context["type"] ?? null) . "-column-row")]], 131, $context, $this->getSourceContext())], "", "", (        // line 140
($context["type"] ?? null) . "-columns")], 124, $context, $this->getSourceContext()), 1 => twig_call_macro($macros["UI"], "macro_scrollSubblock", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.form.grouping"), [0 => twig_call_macro($macros["QD"], "macro_query_designer_grouping_form", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 146
($context["form"] ?? null), "grouping", [], "any", false, false, false, 146), ["id" => (        // line 147
($context["type"] ?? null) . "-grouping-form")]], 145, $context, $this->getSourceContext()), 1 => twig_call_macro($macros["QD"], "macro_query_designer_grouping_list", [["id" => (        // line 149
($context["type"] ?? null) . "-grouping-list")]], 149, $context, $this->getSourceContext()), 2 => twig_call_macro($macros["QD"], "macro_query_designer_grouping_item_template", [(        // line 150
($context["type"] ?? null) . "-grouping-item-row")], 150, $context, $this->getSourceContext())], "", "", (        // line 154
($context["type"] ?? null) . "-columns")], 142, $context, $this->getSourceContext()), 2 => twig_call_macro($macros["UI"], "macro_scrollSubblock", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.form.date_group_section.label"), [0 => twig_call_macro($macros["QD"], "macro_query_designer_date_grouping_form", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 160
($context["form"] ?? null), "dateGrouping", [], "any", false, false, false, 160)], 159, $context, $this->getSourceContext())], "", "", (        // line 165
($context["type"] ?? null) . "-columns")], 156, $context, $this->getSourceContext())]], 1 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.form.filters"), "spanClass" => (        // line 171
($context["type"] ?? null) . "-filters responsive-cell"), "data" => [0 => twig_call_macro($macros["segmentQD"], "macro_query_designer_condition_builder", [["id" => (        // line 174
($context["type"] ?? null) . "-condition-builder"), "page_limit" => twig_constant("\\Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager::PER_PAGE"), "metadata" =>         // line 176
($context["metadata"] ?? null), "fieldConditionOptions" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 177
($context["form"] ?? null), "vars", [], "any", false, false, false, 177), "field_condition_options", [], "any", false, false, false, 177)]], 173, $context, $this->getSourceContext())]]]]]);
        // line 183
        echo "
    ";
        // line 184
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.form.chart_designer"), "subblocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.reporttype.chart.label"), "data" => [0 =>         // line 190
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "chartOptions", [], "any", false, false, false, 190), 'widget', ["label" => "oro.report.reporttype.chart.label"])]]]]]);
        // line 197
        echo "
    ";
        // line 198
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 199
        echo "
    ";
        // line 200
        $context["data"] = ["formErrors" => ((        // line 201
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 202
($context["dataBlocks"] ?? null), "hiddenData" =>         // line 203
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "definition", [], "any", false, false, false, 203), 'widget')];
        // line 205
        echo "
    ";
        // line 206
        echo twig_call_macro($macros["UI"], "macro_scrollData", [($context["id"] ?? null), ($context["data"] ?? null), ($context["entity"] ?? null), ($context["form"] ?? null)], 206, $context, $this->getSourceContext());
        echo "

    ";
        // line 208
        echo twig_call_macro($macros["QD"], "macro_query_designer_column_chain_template", ["column-chain-template"], 208, $context, $this->getSourceContext());
        echo "
    ";
        // line 209
        echo twig_call_macro($macros["segmentQD"], "macro_initJsWidgets", [($context["type"] ?? null), ($context["form"] ?? null), ($context["entities"] ?? null), ($context["metadata"] ?? null)], 209, $context, $this->getSourceContext());
        echo "

";
    }

    public function getTemplateName()
    {
        return "@OroReport/Report/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  268 => 209,  264 => 208,  259 => 206,  256 => 205,  254 => 203,  253 => 202,  252 => 201,  251 => 200,  248 => 199,  246 => 198,  243 => 197,  241 => 190,  240 => 184,  237 => 183,  235 => 177,  234 => 176,  233 => 174,  232 => 171,  231 => 165,  230 => 160,  229 => 154,  228 => 150,  227 => 149,  226 => 147,  225 => 146,  224 => 140,  223 => 134,  222 => 133,  221 => 129,  220 => 128,  219 => 120,  217 => 118,  215 => 117,  212 => 116,  210 => 111,  209 => 110,  208 => 109,  207 => 98,  206 => 97,  204 => 90,  202 => 89,  199 => 88,  196 => 81,  194 => 80,  191 => 79,  188 => 78,  184 => 77,  179 => 74,  176 => 73,  173 => 72,  167 => 70,  165 => 68,  164 => 65,  162 => 64,  159 => 63,  155 => 62,  148 => 59,  145 => 58,  142 => 54,  139 => 53,  136 => 52,  133 => 49,  130 => 48,  128 => 44,  123 => 43,  118 => 41,  114 => 39,  112 => 37,  111 => 33,  109 => 32,  106 => 31,  102 => 29,  100 => 25,  98 => 24,  96 => 23,  93 => 22,  90 => 21,  86 => 20,  79 => 16,  72 => 15,  66 => 13,  62 => 12,  57 => 1,  55 => 10,  53 => 9,  50 => 7,  48 => 6,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReport/Report/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ReportBundle/Resources/views/Report/update.html.twig");
    }
}
