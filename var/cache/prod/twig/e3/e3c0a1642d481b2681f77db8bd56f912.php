<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/dialog/create.html.twig */
class __TwigTemplate_9c35bb9685a3de5a97ad82c53461feb8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content_data' => [$this, 'block_content_data'],
            'widget_context' => [$this, 'block_widget_context'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/dialog/create.html.twig", 2)->unwrap();
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroOAuth2Server/Client/dialog/create.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 4
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 5
        echo "    ";
        if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 5), "valid", [], "any", false, false, false, 5) && twig_length_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 5), "errors", [], "any", false, false, false, 5)))) {
            // line 6
            echo "        <div class=\"alert alert-error\" role=\"alert\">
            <div class=\"message\">
                ";
            // line 8
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors');
            echo "
            </div>
        </div>
    ";
        }
        // line 12
        echo "    <fieldset class=\"form form-horizontal\">
        ";
        // line 13
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", true, true, false, 13)) {
            // line 14
            echo "            ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "organization", [], "any", false, false, false, 14), 'row');
            echo "
        ";
        }
        // line 16
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 16), 'row');
        echo "
        ";
        // line 17
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "active", [], "any", false, false, false, 17), 'row');
        echo "
        ";
        // line 18
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", true, true, false, 18)) {
            // line 19
            echo "            ";
            if (twig_in_filter("hidden", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 19), "vars", [], "any", false, false, false, 19), "block_prefixes", [], "any", false, false, false, 19))) {
                // line 20
                echo "                <div data-validation-ignore=\"true\">";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 20), 'row');
                echo "</div>
            ";
            } else {
                // line 22
                echo "                ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "grants", [], "any", false, false, false, 22), 'row', ["group_attr" => ["class" => "client-grants"]]);
                echo "
            ";
            }
            // line 24
            echo "        ";
        }
        // line 25
        echo "        ";
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'rest');
        echo "
        <div class=\"widget-actions form-actions\">
            <button class=\"btn\" type=\"reset\">";
        // line 27
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
        echo "</button>
            <button class=\"btn btn-primary\" type=\"submit\">";
        // line 28
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "html", null, true);
        echo "</button>
        </div>
    </fieldset>
";
    }

    // line 33
    public function block_widget_context($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/dialog/create.html.twig", 34)->unwrap();
        // line 35
        echo "
    <div class=\"widget-content\">
        <div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 39
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.created_message"), "html", null, true);
        echo "
            </div>
        </div>
        <div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block alert-info\">
                ";
        // line 44
        echo $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.created_warning_message");
        echo "
            </div>
        </div>
        <div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                <div>
                    <strong>";
        // line 50
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.identifier.label"), "html", null, true);
        echo ":</strong>
                    ";
        // line 51
        $context["client_id"] = uniqid("client-id-");
        // line 52
        echo "                    <span class=\"text-nowrap\" id=\"";
        echo twig_escape_filter($this->env, ($context["client_id"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "identifier", [], "any", false, false, false, 52), "html", null, true);
        echo "</span>";
        // line 53
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "btn btn-icon", "iCss" => "fa-copy", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.button.copy_to_clipboard.label"), "labelInIcon" => false, "pageComponent" => ["view" => ["view" => "oroui/js/app/views/element-value-copy-to-clipboard-view", "elementSelector" => ("#" .         // line 61
($context["client_id"] ?? null))]]]], 53, $context, $this->getSourceContext());
        // line 65
        echo "</div>
                <div>
                    <strong>";
        // line 67
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.oauth2server.client.secret.label"), "html", null, true);
        echo ":</strong>
                    ";
        // line 68
        $context["client_secret_id"] = uniqid("client-secret-");
        // line 69
        echo "                    <span class=\"text-nowrap\" id=\"";
        echo twig_escape_filter($this->env, ($context["client_secret_id"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "plainSecret", [], "any", false, false, false, 69), "html", null, true);
        echo "</span>";
        // line 70
        echo twig_call_macro($macros["UI"], "macro_clientLink", [["aCss" => "btn btn-icon", "iCss" => "fa-copy", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.button.copy_to_clipboard.label"), "labelInIcon" => false, "pageComponent" => ["view" => ["view" => "oroui/js/app/views/element-value-copy-to-clipboard-view", "elementSelector" => ("#" .         // line 78
($context["client_secret_id"] ?? null))]]]], 70, $context, $this->getSourceContext());
        // line 82
        echo "</div>
            </div>
        </div>
    </div>
    <div class=\"widget-actions\">
        <button class=\"btn\" type=\"reset\">";
        // line 87
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Close"), "html", null, true);
        echo "</button>
    </div>

    <div ";
        // line 90
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/widget-form-component", "options" => ["_wid" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 93
($context["app"] ?? null), "request", [], "any", false, false, false, 93), "get", [0 => "_wid"], "method", false, false, false, 93), "data" => ((        // line 94
array_key_exists("savedId", $context)) ? (_twig_default_filter(($context["savedId"] ?? null), null)) : (null)), "preventRemove" => true]]], 90, $context, $this->getSourceContext());
        // line 97
        echo "></div>
";
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/dialog/create.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 97,  205 => 94,  204 => 93,  203 => 90,  197 => 87,  190 => 82,  188 => 78,  187 => 70,  181 => 69,  179 => 68,  175 => 67,  171 => 65,  169 => 61,  168 => 53,  162 => 52,  160 => 51,  156 => 50,  147 => 44,  139 => 39,  133 => 35,  130 => 34,  126 => 33,  118 => 28,  114 => 27,  108 => 25,  105 => 24,  99 => 22,  93 => 20,  90 => 19,  88 => 18,  84 => 17,  79 => 16,  73 => 14,  71 => 13,  68 => 12,  61 => 8,  57 => 6,  54 => 5,  50 => 4,  45 => 1,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/dialog/create.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/dialog/create.html.twig");
    }
}
