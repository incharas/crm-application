<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroReport/Report/table/view.html.twig */
class __TwigTemplate_7a9bda7bc08de7cf0e081eecf2ae818d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'after_breadcrumbs' => [$this, 'block_after_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroReport/Report/table/view.html.twig", 2)->unwrap();
        // line 3
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroReport/Report/table/view.html.twig", 3)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%report.name%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 4
($context["entity"] ?? null), "name", [], "any", false, false, false, 4), "%report.group%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["reportGroup"] ?? null))]]);
        // line 5
        $context["pageTitle"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 5);
        // line 6
        $context["displaySQL"] = ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_report.display_sql_query") && $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop());
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroReport/Report/table/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroReport/Report/table/view.html.twig", 9)->unwrap();
        // line 10
        echo "
    ";
        // line 11
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)))) {
            // line 12
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 13
($context["entity"] ?? null), "id", [], "any", false, false, false, 13)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.entity_label")]], 12, $context, $this->getSourceContext());
            // line 15
            echo "
    ";
        }
        // line 17
        echo "    ";
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("CREATE", ($context["entity"] ?? null)))) {
            // line 18
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_button", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_clone", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 19
($context["entity"] ?? null), "id", [], "any", false, false, false, 19)]), "iCss" => "fa-files-o", "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.action.clone.button.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.action.clone.button.label")]], 18, $context, $this->getSourceContext());
            // line 23
            echo "
    ";
        }
        // line 25
        echo "    ";
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isDesktop() && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)))) {
            // line 26
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_report", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 27
($context["entity"] ?? null), "id", [], "any", false, false, false, 27)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_index"), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 30
($context["entity"] ?? null), "id", [], "any", false, false, false, 30), "id" => "btn-remove-report", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.entity_label")]], 26, $context, $this->getSourceContext());
            // line 33
            echo "
    ";
        }
    }

    // line 37
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 38
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 39
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_report_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.report.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 42
($context["entity"] ?? null), "name", [], "any", false, false, false, 42)];
        // line 44
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 47
    public function block_after_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "    <div class=\"pull-left\">
        <div class=\"grid-views-holder\"></div>
    </div>
";
    }

    // line 53
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 54
        echo "    ";
        $macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroReport/Report/table/view.html.twig", 54)->unwrap();
        // line 55
        echo "
    ";
        // line 56
        if (array_key_exists("gridName", $context)) {
            // line 57
            echo "        ";
            if (array_key_exists("chartView", $context)) {
                // line 58
                echo "            <div class=\"chart-wrapper\">
                ";
                // line 59
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["chartView"] ?? null), "render", [], "method", false, false, false, 59);
                echo "
            </div>
        ";
            }
            // line 62
            echo "        ";
            $context["renderParams"] = twig_array_merge(((array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])), ["enableFullScreenLayout" =>  !            // line 63
($context["displaySQL"] ?? null), "enableViews" => true, "showViewsInCustomElement" => ".page-title > .navbar-extra .pull-left-extra .grid-views-holder"]);
            // line 67
            echo "        ";
            $context["gridParams"] = ["_grid_view" => [], "_tags" => ["_disabled" => true]];
            // line 71
            echo "        ";
            if (($context["displaySQL"] ?? null)) {
                // line 72
                echo "            ";
                $context["gridParams"] = twig_array_merge(($context["gridParams"] ?? null), ["display_sql_query" => true]);
                // line 73
                echo "        ";
            }
            // line 74
            echo "        ";
            $context["params"] = twig_array_merge(((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), ($context["gridParams"] ?? null));
            // line 76
            echo "        ";
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ($context["params"] ?? null), ($context["renderParams"] ?? null)], 76, $context, $this->getSourceContext());
            echo "
    ";
        } else {
            // line 78
            echo "        <div class=\"container-fluid\">
            <div class=\"grid-container\">
                <div class=\"no-data\">
                    ";
            // line 81
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Invalid report configuration"), "html", null, true);
            echo "
                </div>
            </div>
        </div>
    ";
        }
    }

    // line 88
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 89
        echo "    ";
        $this->displayParentBlock("content", $context, $blocks);
        echo "
    ";
        // line 90
        if (($context["displaySQL"] ?? null)) {
            // line 91
            echo "        <div class=\"sql-query-panel\" data-role=\"sql-query-panel\"></div>
    ";
        }
    }

    public function getTemplateName()
    {
        return "@OroReport/Report/table/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  209 => 91,  207 => 90,  202 => 89,  198 => 88,  188 => 81,  183 => 78,  177 => 76,  174 => 74,  171 => 73,  168 => 72,  165 => 71,  162 => 67,  160 => 63,  158 => 62,  152 => 59,  149 => 58,  146 => 57,  144 => 56,  141 => 55,  138 => 54,  134 => 53,  127 => 48,  123 => 47,  116 => 44,  114 => 42,  113 => 39,  111 => 38,  107 => 37,  101 => 33,  99 => 30,  98 => 27,  96 => 26,  93 => 25,  89 => 23,  87 => 19,  85 => 18,  82 => 17,  78 => 15,  76 => 13,  74 => 12,  72 => 11,  69 => 10,  66 => 9,  62 => 8,  57 => 1,  55 => 6,  53 => 5,  51 => 4,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroReport/Report/table/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ReportBundle/Resources/views/Report/table/view.html.twig");
    }
}
