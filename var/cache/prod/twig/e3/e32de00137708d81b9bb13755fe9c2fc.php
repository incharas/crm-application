<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroChart/Chart/multiline.html.twig */
class __TwigTemplate_62e76e43635afab79a9e14598d811834 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["chart"] = $this->macros["chart"] = $this->loadTemplate("@OroChart/macros.html.twig", "@OroChart/Chart/multiline.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 22
        $context["lableTrans"] = ["data_schema" => ["label" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 25
($context["options"] ?? null), "data_schema", [], "any", false, false, false, 25), "label", [], "any", false, false, false, 25), "label", [], "any", false, false, false, 25))], "value" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 28
($context["options"] ?? null), "data_schema", [], "any", false, false, false, 28), "value", [], "any", false, false, false, 28), "label", [], "any", false, false, false, 28))]]];
        // line 32
        $context["options"] = Oro\Component\PhpUtils\ArrayUtil::arrayMergeRecursiveDistinct(($context["options"] ?? null), ($context["lableTrans"] ?? null));
        // line 33
        if ((twig_length_filter($this->env, ($context["data"] ?? null)) > 0)) {
            // line 34
            echo "    ";
            echo twig_call_macro($macros["chart"], "macro_renderChart", [($context["data"] ?? null), ($context["options"] ?? null), ($context["config"] ?? null), $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()], 34, $context, $this->getSourceContext());
            echo "
";
        } else {
            // line 36
            echo "    <div class=\"no-data\">
        ";
            // line 37
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.no_data_found"), "html", null, true);
            echo "
    </div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroChart/Chart/multiline.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  59 => 37,  56 => 36,  50 => 34,  48 => 33,  46 => 32,  44 => 28,  43 => 25,  42 => 22,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroChart/Chart/multiline.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/ChartBundle/Resources/views/Chart/multiline.html.twig");
    }
}
