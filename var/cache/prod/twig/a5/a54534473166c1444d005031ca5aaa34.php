<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/simplecolorpicker.scss */
class __TwigTemplate_5ad1b7740113136a99290cc6153fe84c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

/* stylelint-disable no-descending-specificity */

.simplecolorpicker span.color {
    &[data-selected] {
        border: 1px solid transparent;
    }

    &:hover,
    &[data-selected]:hover {
        border: 1px solid transparent;
    }
}

.simplecolorpicker.icon,
.simplecolorpicker span.color {
    width: \$checkbox-size;
    height: \$checkbox-size;
    margin: 4px;

    line-height: 1;

    border-radius: \$checkbox-border-radius;

    &[data-color=''],
    &[data-color='#FFFFFF'],
    &[data-color='#ffffff'] {
        border-color: \$primary-750;

        &:hover {
            border-color: \$primary-750;
        }
    }
}

.simplecolorpicker.fontawesome {
    span.color {
        position: relative;

        &[data-selected]::after {
            margin: 0;

            line-height: 1;
        }
    }
}

.simplecolorpicker.inline {
    padding: 8px 12px;
}

.simplecolorpicker.inline.with-empty-color,
.simplecolorpicker.inline.with-custom-color {
    max-width: 294px;
}

.simplecolorpicker {
    .minicolors-swatch {
        position: relative;
        vertical-align: top;
        background-position: -62px -1px;
    }
}

.minicolors {
    \$position: 12px;

    .minicolors-panel {
        height: 222px;
        width: 196px;
        padding: 8px;

        border: \$dropdown-border-width solid \$dropdown-border-color;

        box-sizing: border-box;

        @include border-radius(\$dropdown-border-radius);
        @include box-shadow(\$dropdown-box-shadow);

        .minicolors-grid {
            top: \$position;
            left: \$position;
        }

        .minicolors-slider {
            top: \$position;
            left: auto;
            right: \$position;
        }
    }

    .form-actions {
        position: absolute;
        top: auto;
        right: \$position;
        bottom: \$position;

        width: 100%;

        box-sizing: border-box;

        .btn {
            margin-left: 6px;
        }
    }
}

.minicolors-swatch {
    position: relative;

    width: \$checkbox-size;
    height: \$checkbox-size;
    margin: 0;

    border-radius: \$checkbox-border-radius;
    background-position: -62px -1px;

    border-color: transparent;
    cursor: pointer;
}

.with-empty-color > .minicolors-swatch {
    border-color: inherit;
}

.custom-color {
    line-height: 1;

    &[data-selected] {
        @include fa-icon(\$checkbox-icon-checked, before, true) {
            color: \$primary-inverse;
        }
    }
}

.custom-color-wrapper {
    display: inline-block;
    min-width: \$dropdown-item-icon-fa-size;
    margin-right: \$dropdown-item-icon-fa-offset;
    vertical-align: middle;

    line-height: 1;
}

.custom-color-name {
    vertical-align: middle;
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/simplecolorpicker.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/simplecolorpicker.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/simplecolorpicker.scss");
    }
}
