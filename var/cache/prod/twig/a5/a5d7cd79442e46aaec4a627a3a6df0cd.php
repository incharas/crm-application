<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/main.scss */
class __TwigTemplate_83d934363a8c284caef27fa2d07e1b2f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@import 'root-css-variables';
@import 'app-page';
@import 'attribute-item';
@import 'drag-and-drop-support';
@import 'drag-and-drop-view';
@import 'create-select-entity';
@import 'entity';
@import 'ui-sortable';
@import 'form/form';
@import 'loading/loading-mask';
@import 'loading/loading-dots';
@import 'loading/loading-bar';
@import 'dropdown/dropdown-mask';
@import 'zoomable-area';
@import 'select2';
@import 'datepicker';
@import 'simplecolorpicker';
@import 'dialog';
@import 'label';
@import 'page-header';
@import 'page-toolbar';
@import 'app-header';
@import 'form/form-description';
@import 'form/form-signin';
@import 'form/forms';
@import 'progressbar';
@import 'footer';
@import 'fs-toolbar';
@import 'flash-messages';
@import 'inline-actions';
@import 'error-page';
@import 'nav';
@import 'no-data';
@import 'tab/tabs';
@import 'tab/oro-tabs';
@import 'tab/tab-collection';
@import 'checkbox';
@import 'checkbox-label';
@import 'jstree/jstree-actions';
@import 'jstree/jstree-wrapper';
@import 'jstree/jstree';
@import 'content-sidebar';
@import 'highlight-text';
@import 'scrollspy';
@import 'widget-picker';
@import 'tables';
@import 'other-scroll-container';
@import 'load-more';
@import 'loading-indicator';
@import 'scrolling-overlay';
@import 'scroll-hints';
@import 'image-preview-modal';
@import 'social-list';
@import 'noscript-container';
@import 'buttons-row';
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/main.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/main.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/main.scss");
    }
}
