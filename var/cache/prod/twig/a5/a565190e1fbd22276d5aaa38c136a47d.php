<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/lib/minicolors/jquery.minicolors.js */
class __TwigTemplate_8bf5d8f6f8626dd3f0cbb5ac9361953f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/*
 * jQuery MiniColors: A tiny color picker built on jQuery
 *
 * Copyright: Cory LaViska for A Beautiful Site, LLC.
 *
 * Contributions and bug reports: https://github.com/claviska/jquery-minicolors
 *
 * Licensed under the MIT license: http://opensource.org/licenses/MIT
 *
 */
if(jQuery) (function(\$) {

\t// Defaults
\t\$.minicolors = {
\t\tdefaults: {
\t\t\tanimationSpeed: 50,
\t\t\tanimationEasing: 'swing',
\t\t\tchange: null,
\t\t\tchangeDelay: 0,
\t\t\tcontrol: 'hue',
\t\t\tdataUris: true,
\t\t\tdefaultValue: '',
\t\t\thide: null,
\t\t\thideSpeed: 100,
\t\t\tinline: false,
\t\t\tletterCase: 'lowercase',
\t\t\topacity: false,
\t\t\tposition: 'bottom left',
\t\t\tshow: null,
\t\t\tshowSpeed: 100,
\t\t\ttheme: 'default'
\t\t}
\t};

\t// Public methods
\t\$.extend(\$.fn, {
\t\tminicolors: function(method, data) {

\t\t\tswitch(method) {

\t\t\t\t// Destroy the control
\t\t\t\tcase 'destroy':
\t\t\t\t\t\$(this).each( function() {
\t\t\t\t\t\tdestroy(\$(this));
\t\t\t\t\t});
\t\t\t\t\treturn \$(this);

\t\t\t\t// Hide the color picker
\t\t\t\tcase 'hide':
\t\t\t\t\thide();
\t\t\t\t\treturn \$(this);

\t\t\t\t// Get/set opacity
\t\t\t\tcase 'opacity':
\t\t\t\t\t// Getter
\t\t\t\t\tif( data === undefined ) {
\t\t\t\t\t\t// Getter
\t\t\t\t\t\treturn \$(this).attr('data-opacity');
\t\t\t\t\t} else {
\t\t\t\t\t\t// Setter
\t\t\t\t\t\t\$(this).each( function() {
\t\t\t\t\t\t\tupdateFromInput(\$(this).attr('data-opacity', data));
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t\treturn \$(this);

\t\t\t\t// Get an RGB(A) object based on the current color/opacity
\t\t\t\tcase 'rgbObject':
\t\t\t\t\treturn rgbObject(\$(this), method === 'rgbaObject');

\t\t\t\t// Get an RGB(A) string based on the current color/opacity
\t\t\t\tcase 'rgbString':
\t\t\t\tcase 'rgbaString':
\t\t\t\t\treturn rgbString(\$(this), method === 'rgbaString');

\t\t\t\t// Get/set settings on the fly
\t\t\t\tcase 'settings':
\t\t\t\t\tif( data === undefined ) {
\t\t\t\t\t\treturn \$(this).data('minicolors-settings');
\t\t\t\t\t} else {
\t\t\t\t\t\t// Setter
\t\t\t\t\t\t\$(this).each( function() {
\t\t\t\t\t\t\tvar settings = \$(this).data('minicolors-settings') || {};
\t\t\t\t\t\t\tdestroy(\$(this));
\t\t\t\t\t\t\t\$(this).minicolors(\$.extend(true, settings, data));
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t\treturn \$(this);

\t\t\t\t// Show the color picker
\t\t\t\tcase 'show':
\t\t\t\t\tshow( \$(this).eq(0) );
\t\t\t\t\treturn \$(this);

\t\t\t\t// Get/set the hex color value
\t\t\t\tcase 'value':
\t\t\t\t\tif( data === undefined ) {
\t\t\t\t\t\t// Getter
\t\t\t\t\t\treturn \$(this).val();
\t\t\t\t\t} else {
\t\t\t\t\t\t// Setter
\t\t\t\t\t\t\$(this).each( function() {
\t\t\t\t\t\t\tupdateFromInput(\$(this).val(data));
\t\t\t\t\t\t});
\t\t\t\t\t}
\t\t\t\t\treturn \$(this);

\t\t\t\t// Initializes the control
\t\t\t\tdefault:
\t\t\t\t\tif( method !== 'create' ) data = method;
\t\t\t\t\t\$(this).each( function() {
\t\t\t\t\t\tinit(\$(this), data);
\t\t\t\t\t});
\t\t\t\t\treturn \$(this);

\t\t\t}

\t\t}
\t});

\t// Initialize input elements
\tfunction init(input, settings) {

\t\tvar minicolors = \$('<div class=\"minicolors\" />'),
\t\t\tdefaults = \$.minicolors.defaults;

\t\t// Do nothing if already initialized
\t\tif( input.data('minicolors-initialized') ) return;

\t\t// Handle settings
\t\tsettings = \$.extend(true, {}, defaults, settings);

\t\t// The wrapper
\t\tminicolors
\t\t\t.addClass('minicolors-theme-' + settings.theme)
\t\t\t.toggleClass('minicolors-with-opacity', settings.opacity)
\t\t\t.toggleClass('minicolors-no-data-uris', settings.dataUris !== true);

\t\t// Custom positioning
\t\tif( settings.position !== undefined ) {
\t\t\t\$.each(settings.position.split(' '), function() {
\t\t\t\tminicolors.addClass('minicolors-position-' + this);
\t\t\t});
\t\t}

\t\t// The input
\t\tinput
\t\t\t.addClass('minicolors-input')
\t\t\t.data('minicolors-initialized', false)
\t\t\t.data('minicolors-settings', settings);
\t\tvar panel =
\t\t\t\t'<div class=\"minicolors-panel minicolors-slider-' + settings.control + '\">' +
\t\t\t\t\t'<div class=\"minicolors-slider minicolors-sprite\">' +
\t\t\t\t\t\t'<div class=\"minicolors-picker\"></div>' +
\t\t\t\t\t'</div>' +
\t\t\t\t\t'<div class=\"minicolors-opacity-slider minicolors-sprite\">' +
\t\t\t\t\t\t'<div class=\"minicolors-picker\"></div>' +
\t\t\t\t\t'</div>' +
\t\t\t\t\t'<div class=\"minicolors-grid minicolors-sprite\">' +
\t\t\t\t\t\t'<div class=\"minicolors-grid-inner\"></div>' +
\t\t\t\t\t\t'<div class=\"minicolors-picker\"><div></div></div>' +
\t\t\t\t\t'</div>' +
\t\t\t\t'</div>'
\t\t\t;
        if (input.is('input')) {
            input
                .prop('size', 7)
                .wrap(minicolors)
                .after(panel);
        } else {
            input
                .after(minicolors.append(panel));
        }

\t\t// The swatch
\t\tif( !settings.inline ) {
            if (input.is('input')) {
                input.after('<span class=\"minicolors-swatch minicolors-sprite\"><span class=\"minicolors-swatch-color\"></span></span>');
                input.next('.minicolors-swatch').on('click', function(event) {
                    event.preventDefault();
                    input.focus();
                });
            } else {
                input
                    .addClass('minicolors-swatch minicolors-sprite')
                    .on('click', function(event) {
                        event.preventDefault();
                    });
            }
\t\t}

\t\t// Prevent text selection in IE
\t\tinput.parent().find('.minicolors-panel').on('selectstart', function() { return false; }).end();

\t\t// Inline controls
\t\tif( settings.inline ) input.parent().addClass('minicolors-inline');

\t\tupdateFromInput(input, false);

\t\tinput.data('minicolors-initialized', true);

\t}

\t// Returns the input back to its original state
\tfunction destroy(input) {

\t\tvar minicolors = input.parent();

\t\t// Revert the input element
\t\tinput
\t\t\t.removeData('minicolors-initialized')
\t\t\t.removeData('minicolors-settings')
\t\t\t.removeProp('size')
\t\t\t.removeClass('minicolors-input');

\t\t// Remove the wrap and destroy whatever remains
\t\tminicolors.before(input).remove();

\t}

\t// Shows the specified dropdown panel
\tfunction show(input) {

\t\tvar minicolors = input.parent(),
\t\t\tpanel = minicolors.find('.minicolors-panel'),
\t\t\tsettings = input.data('minicolors-settings');

\t\t// Do nothing if uninitialized, disabled, inline, or already open
\t\tif( !input.data('minicolors-initialized') ||
\t\t\tinput.prop('disabled') ||
\t\t\tminicolors.hasClass('minicolors-inline') ||
\t\t\tminicolors.hasClass('minicolors-focus')
\t\t) return;

\t\thide();

\t\tminicolors.addClass('minicolors-focus');
\t\tpanel
\t\t\t.stop(true, true)
\t\t\t.fadeIn(settings.showSpeed, function() {
\t\t\t\tif( settings.show ) settings.show.call(input.get(0));
\t\t\t});

\t}

\t// Hides all dropdown panels
\tfunction hide() {

\t\t\$('.minicolors-focus').each( function() {

\t\t\tvar minicolors = \$(this),
\t\t\t\tinput = minicolors.find('.minicolors-input'),
\t\t\t\tpanel = minicolors.find('.minicolors-panel'),
\t\t\t\tsettings = input.data('minicolors-settings');

\t\t\tpanel.fadeOut(settings.hideSpeed, function() {
\t\t\t\tif( settings.hide ) settings.hide.call(input.get(0));
\t\t\t\tminicolors.removeClass('minicolors-focus');
\t\t\t});

\t\t});
\t}

\t// Moves the selected picker
\tfunction move(target, event, animate) {

\t\tvar input = target.parents('.minicolors').find('.minicolors-input');
        if (!input.length) {
            input = target.closest('.minicolors').parent().find('.minicolors-input');
        }
\t\tvar settings = input.data('minicolors-settings'),
\t\t\tpicker = target.find('[class\$=-picker]'),
\t\t\toffsetX = target.offset().left,
\t\t\toffsetY = target.offset().top,
\t\t\tx = Math.round(event.pageX - offsetX),
\t\t\ty = Math.round(event.pageY - offsetY),
\t\t\tduration = animate ? settings.animationSpeed : 0,
\t\t\twx, wy, r, phi;

\t\t// Touch support
\t\tif( event.originalEvent.changedTouches ) {
\t\t\tx = event.originalEvent.changedTouches[0].pageX - offsetX;
\t\t\ty = event.originalEvent.changedTouches[0].pageY - offsetY;
\t\t}

\t\t// Constrain picker to its container
\t\tif( x < 0 ) x = 0;
\t\tif( y < 0 ) y = 0;
\t\tif( x > target.width() ) x = target.width();
\t\tif( y > target.height() ) y = target.height();

\t\t// Constrain color wheel values to the wheel
\t\tif( target.parent().is('.minicolors-slider-wheel') && picker.parent().is('.minicolors-grid') ) {
\t\t\twx = 75 - x;
\t\t\twy = 75 - y;
\t\t\tr = Math.sqrt(wx * wx + wy * wy);
\t\t\tphi = Math.atan2(wy, wx);
\t\t\tif( phi < 0 ) phi += Math.PI * 2;
\t\t\tif( r > 75 ) {
\t\t\t\tr = 75;
\t\t\t\tx = 75 - (75 * Math.cos(phi));
\t\t\t\ty = 75 - (75 * Math.sin(phi));
\t\t\t}
\t\t\tx = Math.round(x);
\t\t\ty = Math.round(y);
\t\t}

\t\t// Move the picker
\t\tif( target.is('.minicolors-grid') ) {
\t\t\tpicker
\t\t\t\t.stop(true)
\t\t\t\t.animate({
\t\t\t\t\ttop: y + 'px',
\t\t\t\t\tleft: x + 'px'
\t\t\t\t}, duration, settings.animationEasing, function() {
\t\t\t\t\tupdateFromControl(input, target);
\t\t\t\t});
\t\t} else {
\t\t\tpicker
\t\t\t\t.stop(true)
\t\t\t\t.animate({
\t\t\t\t\ttop: y + 'px'
\t\t\t\t}, duration, settings.animationEasing, function() {
\t\t\t\t\tupdateFromControl(input, target);
\t\t\t\t});
\t\t}

\t}

\t// Sets the input based on the color picker values
\tfunction updateFromControl(input, target) {

\t\tfunction getCoords(picker, container) {

\t\t\tvar left, top;
\t\t\tif( !picker.length || !container ) return null;
\t\t\tleft = picker.offset().left;
\t\t\ttop = picker.offset().top;

\t\t\treturn {
\t\t\t\tx: left - container.offset().left + (picker.outerWidth() / 2),
\t\t\t\ty: top - container.offset().top + (picker.outerHeight() / 2)
\t\t\t};

\t\t}

\t\tvar hue, saturation, brightness, x, y, r, phi,

\t\t\thex = input.val(),
\t\t\topacity = input.attr('data-opacity'),

\t\t\t// Helpful references
\t\t\tminicolors = input.parent(),
\t\t\tsettings = input.data('minicolors-settings'),
\t\t\tswatch = input.is('input') ? minicolors.find('.minicolors-swatch') : input,
            swatchColorHolder = input.is('input') ? input.find('SPAN') : input,

\t\t\t// Panel objects
\t\t\tgrid = minicolors.find('.minicolors-grid'),
\t\t\tslider = minicolors.find('.minicolors-slider'),
\t\t\topacitySlider = minicolors.find('.minicolors-opacity-slider'),

\t\t\t// Picker objects
\t\t\tgridPicker = grid.find('[class\$=-picker]'),
\t\t\tsliderPicker = slider.find('[class\$=-picker]'),
\t\t\topacityPicker = opacitySlider.find('[class\$=-picker]'),

\t\t\t// Picker positions
\t\t\tgridPos = getCoords(gridPicker, grid),
\t\t\tsliderPos = getCoords(sliderPicker, slider),
\t\t\topacityPos = getCoords(opacityPicker, opacitySlider);

\t\t// Handle colors
\t\tif( target.is('.minicolors-grid, .minicolors-slider') ) {

\t\t\t// Determine HSB values
\t\t\tswitch(settings.control) {

\t\t\t\tcase 'wheel':
\t\t\t\t\t// Calculate hue, saturation, and brightness
\t\t\t\t\tx = (grid.width() / 2) - gridPos.x;
\t\t\t\t\ty = (grid.height() / 2) - gridPos.y;
\t\t\t\t\tr = Math.sqrt(x * x + y * y);
\t\t\t\t\tphi = Math.atan2(y, x);
\t\t\t\t\tif( phi < 0 ) phi += Math.PI * 2;
\t\t\t\t\tif( r > 75 ) {
\t\t\t\t\t\tr = 75;
\t\t\t\t\t\tgridPos.x = 69 - (75 * Math.cos(phi));
\t\t\t\t\t\tgridPos.y = 69 - (75 * Math.sin(phi));
\t\t\t\t\t}
\t\t\t\t\tsaturation = keepWithin(r / 0.75, 0, 100);
\t\t\t\t\thue = keepWithin(phi * 180 / Math.PI, 0, 360);
\t\t\t\t\tbrightness = keepWithin(100 - Math.floor(sliderPos.y * (100 / slider.height())), 0, 100);
\t\t\t\t\thex = hsb2hex({
\t\t\t\t\t\th: hue,
\t\t\t\t\t\ts: saturation,
\t\t\t\t\t\tb: brightness
\t\t\t\t\t});

\t\t\t\t\t// Update UI
\t\t\t\t\tslider.css('backgroundColor', hsb2hex({ h: hue, s: saturation, b: 100 }));
\t\t\t\t\tbreak;

\t\t\t\tcase 'saturation':
\t\t\t\t\t// Calculate hue, saturation, and brightness
\t\t\t\t\thue = keepWithin(parseInt(gridPos.x * (360 / grid.width()), 10), 0, 360);
\t\t\t\t\tsaturation = keepWithin(100 - Math.floor(sliderPos.y * (100 / slider.height())), 0, 100);
\t\t\t\t\tbrightness = keepWithin(100 - Math.floor(gridPos.y * (100 / grid.height())), 0, 100);
\t\t\t\t\thex = hsb2hex({
\t\t\t\t\t\th: hue,
\t\t\t\t\t\ts: saturation,
\t\t\t\t\t\tb: brightness
\t\t\t\t\t});

\t\t\t\t\t// Update UI
\t\t\t\t\tslider.css('backgroundColor', hsb2hex({ h: hue, s: 100, b: brightness }));
\t\t\t\t\tminicolors.find('.minicolors-grid-inner').css('opacity', saturation / 100);
\t\t\t\t\tbreak;

\t\t\t\tcase 'brightness':
\t\t\t\t\t// Calculate hue, saturation, and brightness
\t\t\t\t\thue = keepWithin(parseInt(gridPos.x * (360 / grid.width()), 10), 0, 360);
\t\t\t\t\tsaturation = keepWithin(100 - Math.floor(gridPos.y * (100 / grid.height())), 0, 100);
\t\t\t\t\tbrightness = keepWithin(100 - Math.floor(sliderPos.y * (100 / slider.height())), 0, 100);
\t\t\t\t\thex = hsb2hex({
\t\t\t\t\t\th: hue,
\t\t\t\t\t\ts: saturation,
\t\t\t\t\t\tb: brightness
\t\t\t\t\t});

\t\t\t\t\t// Update UI
\t\t\t\t\tslider.css('backgroundColor', hsb2hex({ h: hue, s: saturation, b: 100 }));
\t\t\t\t\tminicolors.find('.minicolors-grid-inner').css('opacity', 1 - (brightness / 100));
\t\t\t\t\tbreak;

\t\t\t\tdefault:
\t\t\t\t\t// Calculate hue, saturation, and brightness
\t\t\t\t\thue = keepWithin(360 - parseInt(sliderPos.y * (360 / slider.height()), 10), 0, 360);
\t\t\t\t\tsaturation = keepWithin(Math.floor(gridPos.x * (100 / grid.width())), 0, 100);
\t\t\t\t\tbrightness = keepWithin(100 - Math.floor(gridPos.y * (100 / grid.height())), 0, 100);
\t\t\t\t\thex = hsb2hex({
\t\t\t\t\t\th: hue,
\t\t\t\t\t\ts: saturation,
\t\t\t\t\t\tb: brightness
\t\t\t\t\t});

\t\t\t\t\t// Update UI
\t\t\t\t\tgrid.css('backgroundColor', hsb2hex({ h: hue, s: 100, b: 100 }));
\t\t\t\t\tbreak;

\t\t\t}

\t\t\t// Adjust case
\t\t\tinput.val( convertCase(hex, settings.letterCase) );

\t\t}

\t\t// Handle opacity
\t\tif( target.is('.minicolors-opacity-slider') ) {
\t\t\tif( settings.opacity ) {
\t\t\t\topacity = parseFloat(1 - (opacityPos.y / opacitySlider.height())).toFixed(2);
\t\t\t} else {
\t\t\t\topacity = 1;
\t\t\t}
\t\t\tif( settings.opacity ) input.attr('data-opacity', opacity);
\t\t}

\t\t// Set swatch color
        swatchColorHolder.css({
\t\t\tbackgroundColor: hex,
\t\t\topacity: opacity
\t\t});

\t\t// Handle change event
\t\tdoChange(input, hex, opacity);

\t}

\t// Sets the color picker values from the input
\tfunction updateFromInput(input, preserveInputValue) {

\t\tvar hex,
\t\t\thsb,
\t\t\topacity,
\t\t\tx, y, r, phi,

\t\t\t// Helpful references
\t\t\tminicolors = input.parent(),
\t\t\tsettings = input.data('minicolors-settings'),
\t\t\tswatch = input.is('input') ? minicolors.find('.minicolors-swatch') : input,
            swatchColorHolder = input.is('input') ? input.find('SPAN') : input,

\t\t\t// Panel objects
\t\t\tgrid = minicolors.find('.minicolors-grid'),
\t\t\tslider = minicolors.find('.minicolors-slider'),
\t\t\topacitySlider = minicolors.find('.minicolors-opacity-slider'),

\t\t\t// Picker objects
\t\t\tgridPicker = grid.find('[class\$=-picker]'),
\t\t\tsliderPicker = slider.find('[class\$=-picker]'),
\t\t\topacityPicker = opacitySlider.find('[class\$=-picker]');

\t\t// Determine hex/HSB values
\t\thex = convertCase(parseHex(input.val(), true), settings.letterCase);
\t\tif( !hex ){
\t\t\thex = convertCase(parseHex(settings.defaultValue, true), settings.letterCase);
\t\t}
\t\thsb = hex2hsb(hex);

\t\t// Update input value
\t\tif( !preserveInputValue ) input.val(hex);

\t\t// Determine opacity value
\t\tif( settings.opacity ) {
\t\t\t// Get from data-opacity attribute and keep within 0-1 range
\t\t\topacity = input.attr('data-opacity') === '' ? 1 : keepWithin(parseFloat(input.attr('data-opacity')).toFixed(2), 0, 1);
\t\t\tif( isNaN(opacity) ) opacity = 1;
\t\t\tinput.attr('data-opacity', opacity);
            swatchColorHolder.css('opacity', opacity);

\t\t\t// Set opacity picker position
\t\t\ty = keepWithin(opacitySlider.height() - (opacitySlider.height() * opacity), 0, opacitySlider.height());
\t\t\topacityPicker.css('top', y + 'px');
\t\t}

\t\t// Update swatch
        swatchColorHolder.css('backgroundColor', hex);

\t\t// Determine picker locations
\t\tswitch(settings.control) {

\t\t\tcase 'wheel':
\t\t\t\t// Set grid position
\t\t\t\tr = keepWithin(Math.ceil(hsb.s * 0.75), 0, grid.height() / 2);
\t\t\t\tphi = hsb.h * Math.PI / 180;
\t\t\t\tx = keepWithin(75 - Math.cos(phi) * r, 0, grid.width());
\t\t\t\ty = keepWithin(75 - Math.sin(phi) * r, 0, grid.height());
\t\t\t\tgridPicker.css({
\t\t\t\t\ttop: y + 'px',
\t\t\t\t\tleft: x + 'px'
\t\t\t\t});

\t\t\t\t// Set slider position
\t\t\t\ty = 150 - (hsb.b / (100 / grid.height()));
\t\t\t\tif( hex === '' ) y = 0;
\t\t\t\tsliderPicker.css('top', y + 'px');

\t\t\t\t// Update panel color
\t\t\t\tslider.css('backgroundColor', hsb2hex({ h: hsb.h, s: hsb.s, b: 100 }));
\t\t\t\tbreak;

\t\t\tcase 'saturation':
\t\t\t\t// Set grid position
\t\t\t\tx = keepWithin((5 * hsb.h) / 12, 0, 150);
\t\t\t\ty = keepWithin(grid.height() - Math.ceil(hsb.b / (100 / grid.height())), 0, grid.height());
\t\t\t\tgridPicker.css({
\t\t\t\t\ttop: y + 'px',
\t\t\t\t\tleft: x + 'px'
\t\t\t\t});

\t\t\t\t// Set slider position
\t\t\t\ty = keepWithin(slider.height() - (hsb.s * (slider.height() / 100)), 0, slider.height());
\t\t\t\tsliderPicker.css('top', y + 'px');

\t\t\t\t// Update UI
\t\t\t\tslider.css('backgroundColor', hsb2hex({ h: hsb.h, s: 100, b: hsb.b }));
\t\t\t\tminicolors.find('.minicolors-grid-inner').css('opacity', hsb.s / 100);
\t\t\t\tbreak;

\t\t\tcase 'brightness':
\t\t\t\t// Set grid position
\t\t\t\tx = keepWithin((5 * hsb.h) / 12, 0, 150);
\t\t\t\ty = keepWithin(grid.height() - Math.ceil(hsb.s / (100 / grid.height())), 0, grid.height());
\t\t\t\tgridPicker.css({
\t\t\t\t\ttop: y + 'px',
\t\t\t\t\tleft: x + 'px'
\t\t\t\t});

\t\t\t\t// Set slider position
\t\t\t\ty = keepWithin(slider.height() - (hsb.b * (slider.height() / 100)), 0, slider.height());
\t\t\t\tsliderPicker.css('top', y + 'px');

\t\t\t\t// Update UI
\t\t\t\tslider.css('backgroundColor', hsb2hex({ h: hsb.h, s: hsb.s, b: 100 }));
\t\t\t\tminicolors.find('.minicolors-grid-inner').css('opacity', 1 - (hsb.b / 100));
\t\t\t\tbreak;

\t\t\tdefault:
\t\t\t\t// Set grid position
\t\t\t\tx = keepWithin(Math.ceil(hsb.s / (100 / grid.width())), 0, grid.width());
\t\t\t\ty = keepWithin(grid.height() - Math.ceil(hsb.b / (100 / grid.height())), 0, grid.height());
\t\t\t\tgridPicker.css({
\t\t\t\t\ttop: y + 'px',
\t\t\t\t\tleft: x + 'px'
\t\t\t\t});

\t\t\t\t// Set slider position
\t\t\t\ty = keepWithin(slider.height() - (hsb.h / (360 / slider.height())), 0, slider.height());
\t\t\t\tsliderPicker.css('top', y + 'px');

\t\t\t\t// Update panel color
\t\t\t\tgrid.css('backgroundColor', hsb2hex({ h: hsb.h, s: 100, b: 100 }));
\t\t\t\tbreak;

\t\t}

\t\t// Fire change event, but only if minicolors is fully initialized
\t\tif( input.data('minicolors-initialized') ) {
\t\t\tdoChange(input, hex, opacity);
\t\t}

\t}

\t// Runs the change and changeDelay callbacks
\tfunction doChange(input, hex, opacity) {

\t\tvar settings = input.data('minicolors-settings'),
\t\t\tlastChange = input.data('minicolors-lastChange');

\t\t// Only run if it actually changed
\t\tif( !lastChange || lastChange.hex !== hex || lastChange.opacity !== opacity ) {

\t\t\t// Remember last-changed value
\t\t\tinput.data('minicolors-lastChange', {
\t\t\t\thex: hex,
\t\t\t\topacity: opacity
\t\t\t});

\t\t\t// Fire change event
\t\t\tif( settings.change ) {
\t\t\t\tif( settings.changeDelay ) {
\t\t\t\t\t// Call after a delay
\t\t\t\t\tclearTimeout(input.data('minicolors-changeTimeout'));
\t\t\t\t\tinput.data('minicolors-changeTimeout', setTimeout( function() {
\t\t\t\t\t\tsettings.change.call(input.get(0), hex, opacity);
\t\t\t\t\t}, settings.changeDelay));
\t\t\t\t} else {
\t\t\t\t\t// Call immediately
\t\t\t\t\tsettings.change.call(input.get(0), convertCase(hex, settings.letterCase), opacity);
\t\t\t\t}
\t\t\t}
\t\t\tinput.trigger('change').trigger('input');
\t\t}

\t}

\t// Generates an RGB(A) object based on the input's value
\tfunction rgbObject(input) {
\t\tvar hex = parseHex(\$(input).val(), true),
\t\t\trgb = hex2rgb(hex),
\t\t\topacity = \$(input).attr('data-opacity');
\t\tif( !rgb ) return null;
\t\tif( opacity !== undefined ) \$.extend(rgb, { a: parseFloat(opacity) });
\t\treturn rgb;
\t}

\t// Genearates an RGB(A) string based on the input's value
\tfunction rgbString(input, alpha) {
\t\tvar hex = parseHex(\$(input).val(), true),
\t\t\trgb = hex2rgb(hex),
\t\t\topacity = \$(input).attr('data-opacity');
\t\tif( !rgb ) return null;
\t\tif( opacity === undefined ) opacity = 1;
\t\tif( alpha ) {
\t\t\treturn 'rgba(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ', ' + parseFloat(opacity) + ')';
\t\t} else {
\t\t\treturn 'rgb(' + rgb.r + ', ' + rgb.g + ', ' + rgb.b + ')';
\t\t}
\t}

\t// Converts to the letter case specified in settings
\tfunction convertCase(string, letterCase) {
\t\treturn letterCase === 'uppercase' ? string.toUpperCase() : string.toLowerCase();
\t}

\t// Parses a string and returns a valid hex string when possible
\tfunction parseHex(string, expand) {
\t\tstring = string.replace(/[^A-F0-9]/ig, '');
\t\tif( string.length !== 3 && string.length !== 6 ) return '';
\t\tif( string.length === 3 && expand ) {
\t\t\tstring = string[0] + string[0] + string[1] + string[1] + string[2] + string[2];
\t\t}
\t\treturn '#' + string;
\t}

\t// Keeps value within min and max
\tfunction keepWithin(value, min, max) {
\t\tif( value < min ) value = min;
\t\tif( value > max ) value = max;
\t\treturn value;
\t}

\t// Converts an HSB object to an RGB object
\tfunction hsb2rgb(hsb) {
\t\tvar rgb = {};
\t\tvar h = Math.round(hsb.h);
\t\tvar s = Math.round(hsb.s * 255 / 100);
\t\tvar v = Math.round(hsb.b * 255 / 100);
\t\tif(s === 0) {
\t\t\trgb.r = rgb.g = rgb.b = v;
\t\t} else {
\t\t\tvar t1 = v;
\t\t\tvar t2 = (255 - s) * v / 255;
\t\t\tvar t3 = (t1 - t2) * (h % 60) / 60;
\t\t\tif( h === 360 ) h = 0;
\t\t\tif( h < 60 ) { rgb.r = t1; rgb.b = t2; rgb.g = t2 + t3; }
\t\t\telse if( h < 120 ) {rgb.g = t1; rgb.b = t2; rgb.r = t1 - t3; }
\t\t\telse if( h < 180 ) {rgb.g = t1; rgb.r = t2; rgb.b = t2 + t3; }
\t\t\telse if( h < 240 ) {rgb.b = t1; rgb.r = t2; rgb.g = t1 - t3; }
\t\t\telse if( h < 300 ) {rgb.b = t1; rgb.g = t2; rgb.r = t2 + t3; }
\t\t\telse if( h < 360 ) {rgb.r = t1; rgb.g = t2; rgb.b = t1 - t3; }
\t\t\telse { rgb.r = 0; rgb.g = 0; rgb.b = 0; }
\t\t}
\t\treturn {
\t\t\tr: Math.round(rgb.r),
\t\t\tg: Math.round(rgb.g),
\t\t\tb: Math.round(rgb.b)
\t\t};
\t}

\t// Converts an RGB object to a hex string
\tfunction rgb2hex(rgb) {
\t\tvar hex = [
\t\t\trgb.r.toString(16),
\t\t\trgb.g.toString(16),
\t\t\trgb.b.toString(16)
\t\t];
\t\t\$.each(hex, function(nr, val) {
\t\t\tif (val.length === 1) hex[nr] = '0' + val;
\t\t});
\t\treturn '#' + hex.join('');
\t}

\t// Converts an HSB object to a hex string
\tfunction hsb2hex(hsb) {
\t\treturn rgb2hex(hsb2rgb(hsb));
\t}

\t// Converts a hex string to an HSB object
\tfunction hex2hsb(hex) {
\t\tvar hsb = rgb2hsb(hex2rgb(hex));
\t\tif( hsb.s === 0 ) hsb.h = 360;
\t\treturn hsb;
\t}

\t// Converts an RGB object to an HSB object
\tfunction rgb2hsb(rgb) {
\t\tvar hsb = { h: 0, s: 0, b: 0 };
\t\tvar min = Math.min(rgb.r, rgb.g, rgb.b);
\t\tvar max = Math.max(rgb.r, rgb.g, rgb.b);
\t\tvar delta = max - min;
\t\thsb.b = max;
\t\thsb.s = max !== 0 ? 255 * delta / max : 0;
\t\tif( hsb.s !== 0 ) {
\t\t\tif( rgb.r === max ) {
\t\t\t\thsb.h = (rgb.g - rgb.b) / delta;
\t\t\t} else if( rgb.g === max ) {
\t\t\t\thsb.h = 2 + (rgb.b - rgb.r) / delta;
\t\t\t} else {
\t\t\t\thsb.h = 4 + (rgb.r - rgb.g) / delta;
\t\t\t}
\t\t} else {
\t\t\thsb.h = -1;
\t\t}
\t\thsb.h *= 60;
\t\tif( hsb.h < 0 ) {
\t\t\thsb.h += 360;
\t\t}
\t\thsb.s *= 100/255;
\t\thsb.b *= 100/255;
\t\treturn hsb;
\t}

\t// Converts a hex string to an RGB object
\tfunction hex2rgb(hex) {
\t\thex = parseInt(((hex.indexOf('#') > -1) ? hex.substring(1) : hex), 16);
\t\treturn {
\t\t\tr: hex >> 16,
\t\t\tg: (hex & 0x00FF00) >> 8,
\t\t\tb: (hex & 0x0000FF)
\t\t};
\t}

\t// Handle events
\t\$(document)
\t\t// Hide on clicks outside of the control
\t\t.on('mousedown.minicolors touchstart.minicolors', function(event) {
            var elements = \$(event.target).parents().add(event.target);
\t\t\tif( !elements.hasClass('minicolors-input') && !elements.hasClass('minicolors') ) {
\t\t\t\thide();
\t\t\t}
\t\t})
\t\t// Start moving
\t\t.on('mousedown.minicolors touchstart.minicolors', '.minicolors-grid, .minicolors-slider, .minicolors-opacity-slider', function(event) {
\t\t\tvar target = \$(this);
\t\t\tevent.preventDefault();
\t\t\t\$(document).data('minicolors-target', target);
\t\t\tmove(target, event, true);
\t\t})
\t\t// Move pickers
\t\t.on('mousemove.minicolors touchmove.minicolors', function(event) {
\t\t\tvar target = \$(document).data('minicolors-target');
\t\t\tif( target ) move(target, event);
\t\t})
\t\t// Stop moving
\t\t.on('mouseup.minicolors touchend.minicolors', function() {
\t\t\t\$(this).removeData('minicolors-target');
\t\t})
\t\t// Show panel when swatch is clicked
\t\t.on('mousedown.minicolors touchstart.minicolors', '.minicolors-swatch', function(event) {
\t\t\tvar input = \$(this).parent().find('.minicolors-input');
\t\t\tevent.preventDefault();
\t\t\tshow(input);
\t\t})
\t\t// Show on focus
\t\t.on('focus.minicolors', '.minicolors-input', function() {
\t\t\tvar input = \$(this);
\t\t\tif( !input.data('minicolors-initialized') ) return;
\t\t\tshow(input);
\t\t})
\t\t// Fix hex on blur
\t\t.on('blur.minicolors', '.minicolors-input', function() {
\t\t\tvar input = \$(this),
\t\t\t\tsettings = input.data('minicolors-settings');
\t\t\tif( !input.data('minicolors-initialized') ) return;

\t\t\t// Parse Hex
\t\t\tinput.val(parseHex(input.val(), true));

\t\t\t// Is it blank?
\t\t\tif( input.val() === '' ) input.val(parseHex(settings.defaultValue, true));

\t\t\t// Adjust case
\t\t\tinput.val( convertCase(input.val(), settings.letterCase) );

\t\t})
\t\t// Handle keypresses
\t\t.on('keydown.minicolors', '.minicolors-input', function(event) {
\t\t\tvar input = \$(this);
\t\t\tif( !input.data('minicolors-initialized') ) return;
\t\t\tswitch(event.keyCode) {
\t\t\t\tcase 9: // tab
\t\t\t\t\thide();
\t\t\t\t\tbreak;
\t\t\t\tcase 13: // enter
\t\t\t\tcase 27: // esc
\t\t\t\t\thide();
\t\t\t\t\tinput.blur();
\t\t\t\t\tbreak;
\t\t\t}
\t\t})
\t\t// Update on keyup
\t\t.on('keyup.minicolors', '.minicolors-input', function() {
\t\t\tvar input = \$(this);
\t\t\tif( !input.data('minicolors-initialized') ) return;
\t\t\tupdateFromInput(input, true);
\t\t})
\t\t// Update on paste
\t\t.on('paste.minicolors', '.minicolors-input', function() {
\t\t\tvar input = \$(this);
\t\t\tif( !input.data('minicolors-initialized') ) return;
\t\t\tsetTimeout( function() {
\t\t\t\tupdateFromInput(input, true);
\t\t\t}, 1);
\t\t});

})(jQuery);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/lib/minicolors/jquery.minicolors.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/lib/minicolors/jquery.minicolors.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/lib/minicolors/jquery.minicolors.js");
    }
}
