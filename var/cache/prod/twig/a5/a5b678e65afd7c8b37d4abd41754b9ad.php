<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/templates.html.twig */
class __TwigTemplate_a71eda82903f14f66c853447b172a528 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 124
        echo "
";
    }

    // line 26
    public function macro_rawLink($__parameters__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "parameters" => $__parameters__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 27
            echo "        ";
            ob_start(function () { return ''; });
            // line 28
            echo "            <a href=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "path", [], "any", false, false, false, 28), "html", null, true);
            echo "\" ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "data", [], "any", false, false, false, 28));
            foreach ($context['_seq'] as $context["dataItemName"] => $context["dataItemValue"]) {
                echo " data-";
                echo twig_escape_filter($this->env, $context["dataItemName"], "html", null, true);
                echo "=\"";
                echo twig_escape_filter($this->env, $context["dataItemValue"], "html_attr");
                echo "\"";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['dataItemName'], $context['dataItemValue'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            echo " title=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "title", [], "any", false, false, false, 28), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, twig_trim_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["parameters"] ?? null), "label", [], "any", false, false, false, 28)), "html", null, true);
            echo "</a>
        ";
            $___internal_parse_82_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 27
            echo twig_spaceless($___internal_parse_82_);
            // line 30
            echo "    ";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 1
    public function macro_calendar_event_view_template($__id__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "id" => $__id__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "
<script type=\"text/template\" id=\"activity-context-activity-list\">
    <div class=\"context-item\" style=\"border: none\" data-cid=\"<%- entity.cid %>\">
        <span data-id=\"<%- entity.get('targetId') %>\">
            <span class=\"context-icon <%- entity.get('icon') %>\" aria-hidden=\"true\"></span>

            <% if (entity.get('link')) { %>
                <a href=\"<%- entity.get('link') %>\">
                    <span class=\"context-label\" title=\"<%- entity.get('title') %>\"><%- entity.get('title') %></span>
                </a>
            <% } else { %>
                <span class=\"context-label\" title=\"<%- entity.get('title') %>\"><%- entity.get('title') %></span>
            <% } %>
            <% if (editable) { %>
                <span class=\"fa-close\" aria-hidden=\"true\" data-role=\"delete-item\"></span>
            <% } %>
        </span>
    </div>
</script>

<script type=\"text/html\" id=\"";
            // line 22
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "\">
    ";
            // line 23
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/templates.html.twig", 23)->unwrap();
            // line 24
            echo "    ";
            $macros["invitations"] = $this->loadTemplate("@OroCalendar/invitations.html.twig", "@OroCalendar/templates.html.twig", 24)->unwrap();
            // line 25
            echo "    ";
            $macros["templates"] = $this;
            // line 26
            echo "    ";
            // line 31
            echo "    <div class=\"widget-content\">
        <div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
            // line 34
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.title.label"), "<%= formatter.string(title) %>"], 34, $context, $this->getSourceContext());
            echo "
                ";
            // line 35
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.description.label"), "<div class=\"cms-content cms-content--no-scroll\"><%= formatter.html(description) %></div>"], 35, $context, $this->getSourceContext());
            echo "
                ";
            // line 36
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.start.label"), "<%= formatter.dateTime(start) %>"], 36, $context, $this->getSourceContext());
            echo "
                ";
            // line 37
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.end.label"), "<%= formatter.dateTime(end) %>"], 37, $context, $this->getSourceContext());
            echo "
                ";
            // line 38
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.all_day.label"), "<%= formatter.bool(allDay) %>"], 38, $context, $this->getSourceContext());
            echo "
                ";
            // line 39
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.organizer.label"), "<%= organizerHTML %>"], 39, $context, $this->getSourceContext());
            echo "
                <% if (recurrence) { %>
                    ";
            // line 41
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.recurrence.label"), "<%= formatter.string(recurrencePattern) %>"], 41, $context, $this->getSourceContext());
            echo "
                <% } %>
                <% if (recurringEventId && recurrencePattern) { %>
                    ";
            // line 44
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.recurrence.exception.label"), "<%= formatter.string(recurrencePattern) %>"], 44, $context, $this->getSourceContext());
            echo "
                <% } %>

                <% var attendeesView = _.map(attendees, function (attendee) {
                    var attendeeName = attendee.displayName;
                    if (attendee.fullName) {
                        attendeeName = attendee.fullName;
                    }

                    if (attendee.email) {
                        attendeeName = attendeeName ? attendeeName + ' (' + attendee.email + ')' : attendee.email;
                    }

                    return attendeeName;
                }); %>
                <% if (attendeesView.length > 0) { %>
                    ";
            // line 60
            echo twig_call_macro($macros["UI"], "macro_renderAttribute", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.attendees.label"), "<div class=\"calendar-event-attendees-widget\"><%- attendeesView.join(\", \") %></div>"], 60, $context, $this->getSourceContext());
            echo "
                <% } %>
                <% if (editableInvitationStatus) { %>
                    <% var properties = []; %>
                    ";
            // line 64
            $context["statuses"] = [0 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_ACCEPTED"), 1 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_TENTATIVE"), 2 => twig_constant("Oro\\Bundle\\CalendarBundle\\Entity\\Attendee::STATUS_DECLINED")];
            // line 69
            echo "                    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["statuses"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["status"]) {
                // line 70
                echo "                        <% if (invitationStatus !== \"";
                echo twig_escape_filter($this->env, $context["status"], "html", null, true);
                echo "\") { %>
                            <% var link='";
                // line 71
                echo twig_call_macro($macros["templates"], "macro_rawLink", [["label" => twig_call_macro($macros["invitations"], "macro_calendar_event_invitation_going_status", [                // line 73
$context["status"]], 73, $context, $this->getSourceContext()), "title" => twig_call_macro($macros["invitations"], "macro_calendar_event_invitation_going_status", [                // line 74
$context["status"]], 74, $context, $this->getSourceContext()), "path" => "%path%", "data" => ["page-component-module" => "oroui/js/app/components/view-component", "page-component-options" => json_encode(["view" => "orocalendar/js/app/views/change-status-view", "triggerEventName" => "widget_success:attendee_status:change"])]]], 72, $context, $this->getSourceContext());
                // line 84
                echo "';
                                link = link.replace('%path%', invitationUrls[\"";
                // line 85
                echo twig_escape_filter($this->env, $context["status"], "html", null, true);
                echo "\"]);
                                properties.push(link);
                            %>
                        <% } else { %>
                            <% properties.push('";
                // line 89
                echo twig_call_macro($macros["invitations"], "macro_calendar_event_invitation_going_status", [$context["status"]], 89, $context, $this->getSourceContext());
                echo "'); %>
                        <% } %>
                    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['status'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 92
            echo "                    <div class=\"invitation-response\">
                        ";
            // line 93
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.action.going_status.label"), "<%= formatter.html(properties.join(\"&nbsp\")) %>"], 93, $context, $this->getSourceContext());
            echo "
                    </div>
                <% } %>

                <div class=\"activity-context-activity\">
                ";
            // line 98
            echo twig_call_macro($macros["UI"], "macro_renderAttribute", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.activity.contexts.label"), "<div class=\"activity-context-activity-items\"></div>"], 98, $context, $this->getSourceContext());
            echo "
                </div>
                ";
            // line 100
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("calendar_event_widget_view_additional_properties", $context)) ? (_twig_default_filter(($context["calendar_event_widget_view_additional_properties"] ?? null), "calendar_event_widget_view_additional_properties")) : ("calendar_event_widget_view_additional_properties")), array());
            // line 101
            echo "            </div>
            <div class=\"widget-actions form-actions\" style=\"display: none;\">
                <% if (id != null && removable) { %>
                ";
            // line 104
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["aCss" => "no-hash", "id" => "btn-remove-calendarevent", "dataMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.delete_event.confirmation"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.delete_event.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Delete"), "data" => ["action-name" => "delete", "url" => "oro_calendar_event_delete"]]], 105, $context, $this->getSourceContext());
            // line 113
            echo "
                <% } %>
                <% if (id == null || (id != null && editable)) { %>
                <button class=\"btn\" type=\"button\" data-action-name=\"edit\">";
            // line 116
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.edit"), "html", null, true);
            echo "</button>
                <% } %>
                ";
            // line 118
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("calendar_event_widget_view_actions_jstemplate", $context)) ? (_twig_default_filter(($context["calendar_event_widget_view_actions_jstemplate"] ?? null), "calendar_event_widget_view_actions_jstemplate")) : ("calendar_event_widget_view_actions_jstemplate")), array());
            // line 119
            echo "            </div>
        </div>
    </div>
</script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 125
    public function macro_calendar_event_form_template($__id__ = null, $__form__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "id" => $__id__,
            "form" => $__form__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 126
            echo "<script type=\"text/html\" id=\"";
            echo twig_escape_filter($this->env, ($context["id"] ?? null), "html", null, true);
            echo "\">
    ";
            // line 127
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/templates.html.twig", 127)->unwrap();
            // line 128
            echo "    ";
            $macros["invitations"] = $this->loadTemplate("@OroCalendar/invitations.html.twig", "@OroCalendar/templates.html.twig", 128)->unwrap();
            // line 129
            echo "    ";
            $context["calendarEventDateRange"] = ["module" => "orocalendar/js/app/components/calendar-event-date-range-component", "name" => "calendar-event-date-range", "options" => ["nativeMode" => $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()]];
            // line 136
            echo "    ";
            $context["calendarEventRecurrence"] = ["module" => "orocalendar/js/app/components/calendar-event-recurrence-component", "name" => "calendar-event-recurrence", "options" => ["inputNamePrefixes" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 140
($context["form"] ?? null), "recurrence", [], "any", false, false, false, 140), "vars", [], "any", false, false, false, 140), "full_name", [], "any", false, false, false, 140)]];
            // line 143
            echo "    ";
            ob_start(function () { return ''; });
            // line 144
            echo "        <div class=\"control-group-container\" ";
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["calendarEventDateRange"] ?? null)], 144, $context, $this->getSourceContext());
            echo ">
            ";
            // line 145
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "start", [], "any", false, false, false, 145), 'row');
            echo "
            ";
            // line 146
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "end", [], "any", false, false, false, 146), 'row');
            echo "
            ";
            // line 147
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "allDay", [], "any", false, false, false, 147), 'row');
            echo "
        </div>
        <% if (editAsException || recurringEventId) { %>
            ";
            // line 150
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.recurrence.exception.label"), "<%= recurrencePattern %>"], 150, $context, $this->getSourceContext());
            // line 153
            echo "
        <% } else { %>
            <div class=\"control-group-container\" ";
            // line 155
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [($context["calendarEventRecurrence"] ?? null)], 155, $context, $this->getSourceContext());
            echo "></div>
        <% } %>
    ";
            $context["rightColumn"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 158
            echo "    ";
            ob_start(function () { return ''; });
            // line 159
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendar", [], "any", true, true, false, 159)) {
                // line 160
                echo "            <% if (!id) { %>
                ";
                // line 161
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendar", [], "any", false, false, false, 161), 'row');
                echo "
            <% } %>
        ";
            }
            // line 164
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "title", [], "any", false, false, false, 164), 'row');
            echo "
        ";
            // line 165
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "description", [], "any", false, false, false, 165), 'row');
            echo "
        ";
            // line 166
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "backgroundColor", [], "any", false, false, false, 166), 'row');
            echo "
        ";
            // line 167
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendarUid", [], "any", true, true, false, 167)) {
                // line 168
                echo "            ";
                echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "calendarUid", [], "any", false, false, false, 168), 'row');
                echo "
        ";
            }
            // line 170
            echo "        ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "attendees", [], "any", false, false, false, 170), 'row');
            echo "
        <% if (!recurrence) { %>
            ";
            // line 172
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "reminders", [], "any", false, false, false, 172), 'row');
            echo "
        <% } %>
        ";
            // line 174
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "notifyAttendees", [], "any", false, false, false, 174), 'row');
            echo "
        ";
            // line 175
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "children", [], "any", false, false, false, 175));
            foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
                // line 176
                echo "            ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, true, false, 176), "extra_field", [], "any", true, true, false, 176) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "vars", [], "any", false, false, false, 176), "extra_field", [], "any", false, false, false, 176))) {
                    // line 177
                    echo "                ";
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'row');
                    echo "
            ";
                }
                // line 179
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 180
            echo "        ";
            echo twig_call_macro($macros["invitations"], "macro_notify_attendees_component", [], 180, $context, $this->getSourceContext());
            echo "
    ";
            $context["leftColumn"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 182
            echo "
    <div class=\"widget-content\">
        <div class=\"alert alert-error\" role=\"alert\" style=\"display: none;\"></div>
        <div class=\"form-container\">
            <form id=\"";
            // line 186
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 186), "id", [], "any", false, false, false, 186), "html", null, true);
            echo "\" name=\"";
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 186), "name", [], "any", false, false, false, 186), "html", null, true);
            echo "\" action=\"#\">
                <fieldset class=\"form form-horizontal\">
                    <div class=\"span6\">
                        ";
            // line 189
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->prepareJsTemplateContent(($context["leftColumn"] ?? null));
            echo "
                    </div>
                    <div class=\"span6\">
                        ";
            // line 192
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->prepareJsTemplateContent(($context["rightColumn"] ?? null));
            echo "
                    </div>
                    <div class=\"widget-actions form-actions\" style=\"display: none;\">
                        <% if (id != null && removable) { %>
                        ";
            // line 196
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["aCss" => "no-hash", "id" => "btn-remove-calendarevent", "dataMessage" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.delete_event.confirmation"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.delete_event.title"), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Delete"), "data" => ["action-name" => "delete", "url" => "oro_calendar_event_delete"]]], 197, $context, $this->getSourceContext());
            // line 205
            echo "
                        <% } %>
                        <button class=\"btn\" type=\"reset\">";
            // line 207
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cancel"), "html", null, true);
            echo "</button>
                        <% if (id == null || (id != null && editable)) { %>
                        <button class=\"btn btn-primary\" type=\"submit\">";
            // line 209
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Save"), "html", null, true);
            echo "</button>
                        <% } %>
                    </div>
                </fieldset>
            </form>
        </div>
        ";
            // line 215
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->prepareJsTemplateContent($this->extensions['Oro\Bundle\FormBundle\Twig\FormExtension']->renderFormJsValidationBlock($this->env, ($context["form"] ?? null)));
            echo "
    </div>
</script>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroCalendar/templates.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  476 => 215,  467 => 209,  462 => 207,  458 => 205,  456 => 196,  449 => 192,  443 => 189,  435 => 186,  429 => 182,  423 => 180,  417 => 179,  411 => 177,  408 => 176,  404 => 175,  400 => 174,  395 => 172,  389 => 170,  383 => 168,  381 => 167,  377 => 166,  373 => 165,  368 => 164,  362 => 161,  359 => 160,  356 => 159,  353 => 158,  347 => 155,  343 => 153,  341 => 150,  335 => 147,  331 => 146,  327 => 145,  322 => 144,  319 => 143,  317 => 140,  315 => 136,  312 => 129,  309 => 128,  307 => 127,  302 => 126,  288 => 125,  275 => 119,  273 => 118,  268 => 116,  263 => 113,  261 => 104,  256 => 101,  254 => 100,  249 => 98,  241 => 93,  238 => 92,  229 => 89,  222 => 85,  219 => 84,  217 => 74,  216 => 73,  215 => 71,  210 => 70,  205 => 69,  203 => 64,  196 => 60,  177 => 44,  171 => 41,  166 => 39,  162 => 38,  158 => 37,  154 => 36,  150 => 35,  146 => 34,  141 => 31,  139 => 26,  136 => 25,  133 => 24,  131 => 23,  127 => 22,  105 => 2,  92 => 1,  83 => 30,  81 => 27,  58 => 28,  55 => 27,  42 => 26,  37 => 124,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/templates.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/templates.html.twig");
    }
}
