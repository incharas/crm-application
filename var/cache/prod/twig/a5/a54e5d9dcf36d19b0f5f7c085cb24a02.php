<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/app/views/validation-message-handler/input-validation-message-handler-view.js */
class __TwigTemplate_fc20b6bc649352d9fd6ba3879d2a9728 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const AbstractValidationMessageHandlerView =
        require('oroform/js/app/views/validation-message-handler/abstract-validation-message-handler-view');

    const InputValidationMessageHandlerView = AbstractValidationMessageHandlerView.extend({
        useMessageLabelWidth: false,

        events: {
            'validate-element': 'onUpdate',
            'focus': 'onUpdate'
        },

        /**
         * @inheritdoc
         */
        constructor: function InputValidationMessageHandlerView(options) {
            InputValidationMessageHandlerView.__super__.constructor.call(this, options);
        },

        isActive() {
            return this.\$el.hasClass('error') && this.\$el.is(':focus');
        },

        onUpdate() {
            this.active = this.isActive();

            this.update();
        },

        getPopperReferenceElement() {
            return this.\$el;
        },

        update() {
            InputValidationMessageHandlerView.__super__.update.call(this);

            this.label.addClass('hide');
        },

        render() {
            InputValidationMessageHandlerView.__super__.render.call(this);

            this.update();

            return this;
        },

        dispose() {
            if (this.disposed) {
                return;
            }

            this.label.removeClass('hide');

            InputValidationMessageHandlerView.__super__.dispose.call(this);
        }
    }, {
        test(element) {
            return \$(element).is('[data-floating-error]');
        }
    });

    return InputValidationMessageHandlerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/app/views/validation-message-handler/input-validation-message-handler-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/app/views/validation-message-handler/input-validation-message-handler-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/app/views/validation-message-handler/input-validation-message-handler-view.js");
    }
}
