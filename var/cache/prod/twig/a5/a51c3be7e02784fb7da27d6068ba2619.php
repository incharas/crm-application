<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEmail/macros.html.twig */
class __TwigTemplate_b4c026eed4eacba76a9abdb17fc0d6d5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 273
        echo "
";
        // line 297
        echo "
";
        // line 377
        echo "
";
        // line 443
        echo "
";
        // line 457
        echo "
";
        // line 483
        echo "
";
        // line 503
        echo "
";
        // line 522
        echo "
";
        // line 587
        echo "
";
        // line 597
        echo "
";
    }

    // line 1
    public function macro_renderAvailableVariablesWidget($__entityName__ = null, $__entityChoiceFieldId__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entityName" => $__entityName__,
            "entityChoiceFieldId" => $__entityChoiceFieldId__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $context["system"] = "oro-email-template-variables-system";
            // line 3
            echo "    ";
            $context["systemTab"] = (($context["system"] ?? null) . "-tab");
            // line 4
            echo "    ";
            $context["entity"] = "oro-email-template-variables-entity";
            // line 5
            echo "    ";
            $context["entityTab"] = (($context["entity"] ?? null) . "-tab");
            // line 6
            echo "    <script type=\"text/template\" id=\"oro-email-template-variables-template\">
        <div class=\"emailtemplate-variables oro-tabs tabbable\">
            <div class=\"oro-tabs__head\">
                <ul class=\"nav nav-tabs\" role=\"tablist\">
                    <li class=\"nav-item\" >
                        <a id=\"";
            // line 11
            echo twig_escape_filter($this->env, ($context["systemTab"] ?? null), "html", null, true);
            echo "\"
                           href=\"#";
            // line 12
            echo twig_escape_filter($this->env, ($context["system"] ?? null), "html", null, true);
            echo "\"
                           class=\"nav-link active\"
                           data-target=\"#";
            // line 14
            echo twig_escape_filter($this->env, ($context["system"] ?? null), "html", null, true);
            echo "\"
                           data-toggle=\"tab\"
                           role=\"tab\"
                           aria-controls=\"";
            // line 17
            echo twig_escape_filter($this->env, ($context["system"] ?? null), "html", null, true);
            echo "\"
                           aria-selected=\"true\"
                        >
                            ";
            // line 20
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.variables.system"), "html", null, true);
            echo "
                        </a>
                    </li>
                    <li class=\"nav-item\">
                        <a id=\"";
            // line 24
            echo twig_escape_filter($this->env, ($context["entityTab"] ?? null), "html", null, true);
            echo "\"
                           href=\"#";
            // line 25
            echo twig_escape_filter($this->env, ($context["entity"] ?? null), "html", null, true);
            echo "\"
                           class=\"nav-link\"
                           data-target=\"#";
            // line 27
            echo twig_escape_filter($this->env, ($context["entity"] ?? null), "html", null, true);
            echo "\"
                           data-toggle=\"tab\"
                           role=\"tab\"
                           aria-controls=\"";
            // line 30
            echo twig_escape_filter($this->env, ($context["entity"] ?? null), "html", null, true);
            echo "\"
                           aria-selected=\"false\"
                        >
                            ";
            // line 33
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailtemplate.variables.entity"), "html", null, true);
            echo "
                        </a>
                    </li>
                </ul>
            </div>
            <div class=\"oro-tabs__content\">
                <div class=\"tab-content\">
                    <div id=\"";
            // line 40
            echo twig_escape_filter($this->env, ($context["system"] ?? null), "html", null, true);
            echo "\"
                         class=\"variables tab-pane active\"
                         role=\"tabpanel\"
                         aria-labelledby=\"";
            // line 43
            echo twig_escape_filter($this->env, ($context["systemTab"] ?? null), "html", null, true);
            echo "\"
                    >
                        <%= variables.system %>
                    </div>
                    <div id=\"";
            // line 47
            echo twig_escape_filter($this->env, ($context["entity"] ?? null), "html", null, true);
            echo "\"
                         class=\"variables tab-pane\"
                         role=\"tabpanel\"
                         aria-labelledby=\"";
            // line 50
            echo twig_escape_filter($this->env, ($context["entityTab"] ?? null), "html", null, true);
            echo "\"
                    >
                    </div>
                </div>
            </div>
        </div>
    </script>
    <script type=\"text/template\" id=\"oro-email-template-variables-system-template\">
        <ul class=\"nav\">
            <% _.each(variables, function(variable, varName) { %>
            <li>
                <a href=\"#\"
                   class=\"variable\"
                   title=\"<%- _.__('oro.email.emailtemplate.variable_title', {'variable_label': variable.label}) %>\">";
            // line 63
            echo "{{ <%- root %>.<%- varName %><% if(variable.filter){%>|<%- variable.filter %><% } %> }}";
            echo "</a>
                <span>&ndash; <%- variable.label %></span>
            </li>
            <% }); %>
        </ul>
    </script>
    <script type=\"text/template\" id=\"oro-email-template-variables-entity-variable-template\">
        <li>
            <a href=\"#\"
               class=\"variable\"
               title=\"<%- _.__('oro.email.emailtemplate.variable_title', {'variable_label': variable.label}) %>\"
            >";
            // line 74
            echo "{{ <%- varValue %> }}";
            echo "</a>
            <span>&ndash;</span>
            <ul class=\"caption\">
                <% for (var i = 1; i < breadcrumbs.length; i++) { %>
                <li>
                    <span><%- pathLabels[breadcrumbs[i]] %></span>
                    <span>/</span>
                </li>
                <% } %>
                <li><%- variable.label %></li>
            </ul>
        </li>
    </script>
    <script type=\"text/template\" id=\"oro-email-template-variables-entity-template\">
        <% var breadcrumbs = path.split('/'); breadcrumbs[0] = root; %>
        <ul class=\"breadcrumb\">
            <% var breadcrumbPath = ''; %>
            <% for (var i = 0; i < breadcrumbs.length; i++) { %>
            <% breadcrumbPath += '/' + breadcrumbs[i]; %>
            <% breadcrumbItemLabel = (i === 0 ? entityLabel : pathLabels[breadcrumbs[i]]); %>
            <li<% if (i === breadcrumbs.length - 1) { %> class=\"active\"<% } %>>
                <% if (i !== breadcrumbs.length - 1) { %>
                <a href=\"#\"
                   class=\"reference\"
                   data-path=\"<%- breadcrumbPath.substring(root.length + 1) %>\"><%- breadcrumbItemLabel %></a>
                <span class=\"divider\">/&nbsp;</span>
                <% } else { %>
                    <%- breadcrumbItemLabel %>
                <% } %>
            </li>
            <% } %>
        </ul>
        <% var varPrefix = path.split('/'); varPrefix[0] = root; varPrefix = varPrefix.join('.'); %>
        <% if (!_.isEmpty(fields) || !_.isEmpty(relations)) { %>
        <ul class=\"nav groups\">
            <% if (!_.isEmpty(fields)) { %>
            <li>
                <div class=\"group-label\"><%- _.__('oro.entity.field_choice.fields') %></div>
                <ul class=\"nav\">
                <% var variableTemplate =  _.template(\$('#oro-email-template-variables-entity-variable-template').html()) %>
                <% _.each(fields, function(variable, varName) { %>
                    <%= variableTemplate({
                        varValue:    varPrefix + '.' + varName,
                        breadcrumbs: breadcrumbs,
                        pathLabels:  pathLabels,
                        variable:    variable
                    }) %>
                    ";
            // line 133
            echo "
                    ";
            // line 151
            echo "                <% }); %>
                </ul>
            </li>
            <% } %>
            <% if (!_.isEmpty(relations)) { %>
            <li>
                <div class=\"group-label\"><%- _.__('oro.entity.field_choice.relations') %></div>
                <ul class=\"nav\">
                    <% _.each(relations, function(variable, varName) { %>
                    <li>
                        <a href=\"#\"
                           class=\"reference\"
                           data-path=\"<%- path + '/' + varName %>\"
                           title=\"<%- _.__('oro.email.emailtemplate.reference_title', {'variable_label': variable.label}) %>\">";
            // line 164
            echo "{{ <%- varPrefix %>.<%- varName %> }}";
            echo "</a>
                        <span>&ndash; <%- variable.label %></span>
                    </li>
                    <% }); %>
                </ul>
            </li>
            <% } %>
        </ul>
        <% } %>
    </script>

    ";
            // line 175
            $context["options"] = ["name" => "email-template-variables", "entityChoice" => ("#" .             // line 177
($context["entityChoiceFieldId"] ?? null)), "view" => ["templateSelector" => "#oro-email-template-variables-template", "sectionTemplateSelector" => "#oro-email-template-variables-{sectionName}-template", "sectionContentSelector" => "#oro-email-template-variables-{sectionName}", "sectionTabSelector" => "#oro-email-template-variables-{sectionName}-tab"], "model" => ["attributes" => $this->env->getRuntime('Symfony\Bridge\Twig\Extension\HttpKernelRuntime')->renderFragment($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_get_emailtemplate_variables")), "entityName" =>             // line 186
($context["entityName"] ?? null), "entityLabel" => ((            // line 187
($context["entityName"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["entityName"] ?? null), "label"))) : (""))]];
            // line 190
            echo "    <div data-page-component-module=\"oroemail/js/app/components/email-variable-component\"
         data-page-component-options=\"";
            // line 191
            echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
            echo "\"
         data-page-component-name=\"email-template-variables\"></div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 199
    public function macro_email_address_text($__emailAddress__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailAddress" => $__emailAddress__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 200
            echo twig_escape_filter($this->env, _twig_default_filter($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 200)), "N/A"), "html", null, true);

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 209
    public function macro_email_address_link($__emailAddress__ = null, $__label__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailAddress" => $__emailAddress__,
            "label" => $__label__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 210
            $macros["emailMacros"] = $this;
            // line 211
            echo "
    ";
            // line 212
            echo twig_call_macro($macros["emailMacros"], "macro_renderEmailAddressLink", [_twig_default_filter($this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(            // line 213
($context["label"] ?? null)), twig_call_macro($macros["emailMacros"], "macro_email_address_text", [($context["emailAddress"] ?? null)], 213, $context, $this->getSourceContext())), $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 214
($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 214)), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 215
($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 215), "id", [], "any", false, false, false, 215)], 212, $context, $this->getSourceContext());

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 225
    public function macro_renderEmailAddressLink($__label__ = null, $__ownerClass__ = null, $__ownerId__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "label" => $__label__,
            "ownerClass" => $__ownerClass__,
            "ownerId" => $__ownerId__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 226
            $context["route"] = $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassRoute(($context["ownerClass"] ?? null));
            // line 227
            echo "    ";
            if ( !(null === ($context["route"] ?? null))) {
                // line 228
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["route"] ?? null), ["id" => ($context["ownerId"] ?? null)]), "html", null, true);
                echo "\">";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["label"] ?? null));
                echo "</a>
    ";
            } else {
                // line 230
                echo "        ";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["label"] ?? null));
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 239
    public function macro_email_address_recipient_link($__emailAddress__ = null, $__emailAddressName__ = null, $__label__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailAddress" => $__emailAddress__,
            "emailAddressName" => $__emailAddressName__,
            "label" => $__label__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 240
            $context["label"] = _twig_default_filter($this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["label"] ?? null)), ((array_key_exists("emailAddressName", $context)) ? (_twig_default_filter(($context["emailAddressName"] ?? null), "N/A")) : ("N/A")));
            // line 241
            echo "    ";
            $context["route"] = $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassRoute($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 241)));
            // line 242
            echo "    ";
            if ( !(null === ($context["route"] ?? null))) {
                // line 243
                echo "        <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(($context["route"] ?? null), ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 243), "id", [], "any", false, false, false, 243)]), "html", null, true);
                echo "\" dir=\"ltr\">
            ";
                // line 244
                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                echo "</a>
    ";
            } else {
                // line 246
                echo "        <bdo dir=\"ltr\">";
                echo twig_escape_filter($this->env, ($context["label"] ?? null), "html", null, true);
                echo "</bdo>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 258
    public function macro_email_address($__emailAddress__ = null, $__emailAddressName__ = null, $__noLink__ = null, $__knownOnly__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailAddress" => $__emailAddress__,
            "emailAddressName" => $__emailAddressName__,
            "noLink" => $__noLink__,
            "knownOnly" => $__knownOnly__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 259
            $macros["emailMacros"] = $this;
            // line 260
            echo "
    ";
            // line 261
            if ((null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 261))) {
                // line 262
                if ( !((array_key_exists("knownOnly", $context)) ? (_twig_default_filter(($context["knownOnly"] ?? null), false)) : (false))) {
                    // line 263
                    echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["emailAddressName"] ?? null));
                }
            } else {
                // line 266
                if (((array_key_exists("noLink", $context)) ? (_twig_default_filter(($context["noLink"] ?? null), false)) : (false))) {
                    // line 267
                    echo twig_call_macro($macros["emailMacros"], "macro_email_address_text", [($context["emailAddress"] ?? null)], 267, $context, $this->getSourceContext());
                } else {
                    // line 269
                    echo twig_call_macro($macros["emailMacros"], "macro_email_address_link", [($context["emailAddress"] ?? null)], 269, $context, $this->getSourceContext());
                }
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 282
    public function macro_email_address_recipient($__emailAddress__ = null, $__emailAddressName__ = null, $__noLink__ = null, $__knownOnly__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailAddress" => $__emailAddress__,
            "emailAddressName" => $__emailAddressName__,
            "noLink" => $__noLink__,
            "knownOnly" => $__knownOnly__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 283
            $macros["emailMacros"] = $this;
            // line 284
            echo "
    ";
            // line 285
            if ((null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 285))) {
                // line 286
                if ( !((array_key_exists("knownOnly", $context)) ? (_twig_default_filter(($context["knownOnly"] ?? null), false)) : (false))) {
                    // line 287
                    echo twig_escape_filter($this->env, ($context["emailAddressName"] ?? null), "html", null, true);
                }
            } else {
                // line 290
                if (((array_key_exists("noLink", $context)) ? (_twig_default_filter(($context["noLink"] ?? null), false)) : (false))) {
                    // line 291
                    echo twig_escape_filter($this->env, ($context["emailAddressName"] ?? null), "html", null, true);
                } else {
                    // line 293
                    echo twig_call_macro($macros["emailMacros"], "macro_email_address_recipient_link", [($context["emailAddress"] ?? null), ($context["emailAddressName"] ?? null)], 293, $context, $this->getSourceContext());
                }
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 304
    public function macro_email_address_simple($__email__ = null, $__title__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "email" => $__email__,
            "title" => $__title__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 305
            if ( !twig_test_empty(($context["email"] ?? null))) {
                // line 306
                echo "        ";
                $context["emailAddress"] = null;
                // line 307
                echo "        ";
                // line 308
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "email", [], "any", true, true, false, 308)) {
                    // line 309
                    echo "            ";
                    if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "email", [], "any", false, false, false, 309))) {
                        // line 310
                        echo "                ";
                        $context["emailAddress"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "email", [], "any", false, false, false, 310);
                        // line 311
                        echo "            ";
                    }
                    // line 312
                    echo "            ";
                    // line 313
                    echo "        ";
                } else {
                    // line 314
                    echo "            ";
                    $context["emailAddress"] = ($context["email"] ?? null);
                    // line 315
                    echo "        ";
                }
                // line 316
                echo "
        ";
                // line 317
                if (twig_test_empty(($context["title"] ?? null))) {
                    // line 318
                    echo "            ";
                    $context["title"] = ($context["emailAddress"] ?? null);
                    // line 319
                    echo "        ";
                }
                // line 320
                echo "
        ";
                // line 321
                if (($context["emailAddress"] ?? null)) {
                    // line 322
                    echo "            <a href=\"mailto:";
                    echo twig_escape_filter($this->env, ($context["emailAddress"] ?? null), "html_attr");
                    echo "\" aria-label=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.emailuser.email.aria_label", ["%email%" => $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["title"] ?? null))]), "html", null, true);
                    echo "\" title=\"";
                    echo twig_escape_filter($this->env, ($context["title"] ?? null), "html_attr");
                    echo "\" class=\"email\"><bdo dir=\"ltr\">";
                    echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(($context["title"] ?? null));
                    echo "</bdo></a>
        ";
                }
                // line 324
                echo "    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 333
    public function macro_renderEmailWithActions($__email__ = null, $__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "email" => $__email__,
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 334
            $macros["emailMacros"] = $this;
            // line 335
            echo "
    ";
            // line 336
            if ( !twig_test_empty(($context["email"] ?? null))) {
                // line 337
                ob_start(function () { return ''; });
                // line 338
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("email_actions", $context)) ? (_twig_default_filter(($context["email_actions"] ?? null), "email_actions")) : ("email_actions")), ["email" => ($context["email"] ?? null), "entity" => ($context["entity"] ?? null)]);
                $context["actions"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 340
                $context["actions"] = twig_trim_filter(($context["actions"] ?? null));
                // line 341
                echo "        <span class=\"inline-actions-element";
                if (twig_test_empty(($context["actions"] ?? null))) {
                    echo " inline-actions-element_no-actions";
                }
                echo "\">
            <span class=\"inline-actions-element_wrapper\">";
                // line 342
                echo twig_call_macro($macros["emailMacros"], "macro_email_address_simple", [($context["email"] ?? null)], 342, $context, $this->getSourceContext());
                echo "</span>
            ";
                // line 343
                if ( !twig_test_empty(($context["actions"] ?? null))) {
                    // line 344
                    echo "<span class=\"inline-actions-element_actions email-actions\">";
                    echo ($context["actions"] ?? null);
                    echo "</span>";
                }
                // line 346
                echo "        </span>
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 357
    public function macro_recipient_email_addresses($__recipients__ = null, $__noLink__ = null, $__knownOnly__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "recipients" => $__recipients__,
            "noLink" => $__noLink__,
            "knownOnly" => $__knownOnly__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 358
            $macros["emailMacros"] = $this;
            // line 359
            echo "
    ";
            // line 360
            $context["addresses"] = [];
            // line 361
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recipients"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recipient"]) {
                // line 362
                $context["address"] = twig_call_macro($macros["emailMacros"], "macro_email_address_recipient", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "emailAddress", [], "any", false, false, false, 362), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "name", [], "any", false, false, false, 362), ($context["noLink"] ?? null), ($context["knownOnly"] ?? null)], 362, $context, $this->getSourceContext());
                // line 363
                if ((twig_length_filter($this->env, ($context["address"] ?? null)) > 0)) {
                    // line 364
                    $context["addresses"] = twig_array_merge(($context["addresses"] ?? null), [0 => ($context["address"] ?? null)]);
                    // line 365
                    echo "        ";
                } else {
                    // line 366
                    echo "            ";
                    $context["addresses"] = twig_array_merge(($context["addresses"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "emailAddress", [], "any", false, false, false, 366), "email", [], "any", false, false, false, 366)]);
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipient'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 370
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["addresses"] ?? null));
            $context['_iterated'] = false;
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["address"]) {
                // line 371
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags($context["address"]);
                // line 372
                if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 372)) {
                    echo "; ";
                }
                // line 373
                echo "    ";
                $context['_iterated'] = true;
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            if (!$context['_iterated']) {
                // line 374
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['address'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 385
    public function macro_attachments($__attachments__ = null, $__target__ = null, $__hasGrantReattach__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "attachments" => $__attachments__,
            "target" => $__target__,
            "hasGrantReattach" => $__hasGrantReattach__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 386
            $context["galleryUid"] = twig_random($this->env);
            // line 387
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["attachments"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["attachment"]) {
                // line 388
                if ((null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "embeddedContentId", [], "any", false, false, false, 388))) {
                    // line 389
                    echo "            ";
                    $context["canCopyToRecord"] = ((((($context["hasGrantReattach"] ?? null) && array_key_exists("target", $context)) && $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->canReAttach($context["attachment"], ($context["target"] ?? null)))) ? (true) : (false));
                    // line 390
                    echo "            ";
                    $context["attachmentUrl"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_attachment", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "id", [], "any", false, false, false, 390)]);
                    // line 391
                    echo "            ";
                    $context["isImage"] = $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getTypeIsImage(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "contentType", [], "any", false, false, false, 391));
                    // line 392
                    echo "            ";
                    $context["isPreviewAvailable"] = $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->isPreviewAvailable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "contentType", [], "any", false, false, false, 392));
                    // line 393
                    echo "            ";
                    $context["icon"] = $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getAttachmentIcon($context["attachment"]);
                    // line 394
                    echo "            <li class=\"email-attachments-list-item\">
                <div class=\"email-attachments-file\">
                    ";
                    // line 396
                    if (($context["isImage"] ?? null)) {
                        // line 397
                        echo "                        <a data-gallery=\"gallery-";
                        echo twig_escape_filter($this->env, ($context["galleryUid"] ?? null), "html", null, true);
                        echo "\"
                           data-filename=\"";
                        // line 398
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "fileName", [], "any", false, false, false, 398), "html", null, true);
                        echo "\"
                           class=\"no-hash\"
                           href=\"";
                        // line 400
                        echo twig_escape_filter($this->env, ($context["attachmentUrl"] ?? null), "html", null, true);
                        echo "\">
                            <span class=\"thumbnail\" style=\"background: url('";
                        // line 401
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_resize_email_attachment", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "id", [], "any", false, false, false, 401), "width" => 110, "height" => 80]), "html", null, true);
                        echo "') 50% 50% no-repeat;\"></span>
                        </a>
                    ";
                    } else {
                        // line 404
                        echo "                        <div class=\"thumbnail\">
                            <span class=\"fa ";
                        // line 405
                        echo twig_escape_filter($this->env, ($context["icon"] ?? null), "html", null, true);
                        echo " fa-offset-none\" aria-hidden=\"true\"></span>
                        </div>
                    ";
                    }
                    // line 408
                    echo "                </div>
                <div class=\"dropdown link-to-record\">
                    ";
                    // line 410
                    $context["togglerId"] = uniqid("dropdown-");
                    // line 411
                    echo "                    <a class=\"no-hash dropdown-toggle dropdown-toggle--no-caret\" id=\"";
                    echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                    echo "\" href=\"#\" role=\"button\" data-toggle=\"dropdown\">
                        <span class=\"fa ";
                    // line 412
                    echo twig_escape_filter($this->env, ($context["icon"] ?? null), "html", null, true);
                    echo "\" aria-hidden=\"true\"></span> ";
                    echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\FormatExtension']->formatFilename(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "fileName", [], "any", false, false, false, 412), 11, 4, 5), "html", null, true);
                    echo "
                    </a>
                    <ul class=\"dropdown-menu ";
                    // line 414
                    echo ((($context["canCopyToRecord"] ?? null)) ? ("") : ("one"));
                    echo "\" role=\"menu\" aria-labelledby=\"";
                    echo twig_escape_filter($this->env, ($context["togglerId"] ?? null), "html", null, true);
                    echo "\">
                        ";
                    // line 415
                    if (($context["isPreviewAvailable"] ?? null)) {
                        // line 416
                        echo "                            <a class=\"view-image no-hash\" tabindex=\"-1\" data-gallery=\"gallery-";
                        echo twig_escape_filter($this->env, ($context["galleryUid"] ?? null), "html", null, true);
                        echo "\" href=\"";
                        echo twig_escape_filter($this->env, ($context["attachmentUrl"] ?? null), "html", null, true);
                        echo "\">";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.attachment.view"), "html", null, true);
                        echo "</a>
                        ";
                    }
                    // line 418
                    echo "                        <a class=\"no-hash\" tabindex=\"-1\" href=\"";
                    echo twig_escape_filter($this->env, ($context["attachmentUrl"] ?? null), "html", null, true);
                    echo "\">
                            ";
                    // line 419
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.attachment.save"), "html", null, true);
                    echo "<span>(";
                    echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFileSize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["attachment"], "size", [], "any", false, false, false, 419)), "html", null, true);
                    echo ")</span>
                        </a>
                        ";
                    // line 421
                    if (($context["canCopyToRecord"] ?? null)) {
                        // line 422
                        echo "                            ";
                        $context["options"] = ["view" => "oroemail/js/app/views/email-attachment-link-view", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_attachment_link", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 425
$context["attachment"], "id", [], "any", false, false, false, 425), "targetActivityClass" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(                        // line 426
($context["target"] ?? null)), "targetActivityId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 427
($context["target"] ?? null), "id", [], "any", false, false, false, 427)])];
                        // line 430
                        echo "                        <a tabindex=\"-1\" data-page-component-module=\"oroui/js/app/components/view-component\"
                            class=\"attachment\"
                            data-page-component-options=\"";
                        // line 432
                        echo twig_escape_filter($this->env, json_encode(($context["options"] ?? null)), "html", null, true);
                        echo "\"
                            href=\"";
                        // line 433
                        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "url", [], "any", false, false, false, 433), "html", null, true);
                        echo "\">
                            ";
                        // line 434
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.email.attachment.copy_to_record"), "html", null, true);
                        echo "
                        </a>
                        ";
                    }
                    // line 437
                    echo "                    </ul>
                </div>
            </li>
        ";
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['attachment'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 450
    public function macro_body($__emailBody__ = null, $__cssClass__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailBody" => $__emailBody__,
            "cssClass" => $__cssClass__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 451
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailBody"] ?? null), "bodyIsText", [], "any", false, false, false, 451)) {
                // line 452
                echo "<pre class=\"email-body";
                if (array_key_exists("cssClass", $context)) {
                    echo " ";
                    echo twig_escape_filter($this->env, ($context["cssClass"] ?? null), "html", null, true);
                }
                echo "\">";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailBody"] ?? null), "bodyContent", [], "any", false, false, false, 452), "html", null, true);
                echo "</pre>";
            } else {
                // line 454
                echo "<iframe sandbox=\"\" class=\"email-body";
                if (array_key_exists("cssClass", $context)) {
                    echo " ";
                    echo twig_escape_filter($this->env, ($context["cssClass"] ?? null), "html", null, true);
                }
                echo "\" src=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_email_body", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailBody"] ?? null), "id", [], "any", false, false, false, 454)]), "html", null, true);
                echo "\"></iframe>";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 467
    public function macro_email_participant_name_or_me($__emailAddress__ = null, $__emailAddressName__ = null, $__noLink__ = null, $__knownOnly__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailAddress" => $__emailAddress__,
            "emailAddressName" => $__emailAddressName__,
            "noLink" => $__noLink__,
            "knownOnly" => $__knownOnly__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 468
            $macros["emailMacros"] = $this;
            // line 469
            echo "
    ";
            // line 470
            if ((( !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 470)) && ($this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 471
($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 471), true) == $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getClassName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 471), true))) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 472
($context["emailAddress"] ?? null), "owner", [], "any", false, false, false, 472), "id", [], "any", false, false, false, 472) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "user", [], "any", false, false, false, 472), "id", [], "any", false, false, false, 472)))) {
                // line 473
                if (((array_key_exists("noLink", $context)) ? (_twig_default_filter(($context["noLink"] ?? null), false)) : (false))) {
                    // line 474
                    echo twig_escape_filter($this->env, twig_lower_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Me")), "html", null, true);
                } else {
                    // line 476
                    echo twig_call_macro($macros["emailMacros"], "macro_email_address_link", [($context["emailAddress"] ?? null), twig_lower_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Me"))], 476, $context, $this->getSourceContext());
                }
            } else {
                // line 479
                $context["name"] = $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(_twig_default_filter($this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailAddressName(($context["emailAddressName"] ?? null)), $this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailAddress(($context["emailAddressName"] ?? null))));
                // line 480
                echo twig_call_macro($macros["emailMacros"], "macro_email_address", [($context["emailAddress"] ?? null), ($context["name"] ?? null), ($context["noLink"] ?? null), ($context["knownOnly"] ?? null)], 480, $context, $this->getSourceContext());
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 491
    public function macro_email_participants_name($__recipients__ = null, $__noLink__ = null, $__knownOnly__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "recipients" => $__recipients__,
            "noLink" => $__noLink__,
            "knownOnly" => $__knownOnly__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 492
            $macros["emailMacros"] = $this;
            // line 493
            echo "
    ";
            // line 494
            $context["recipientHtmlCollection"] = [];
            // line 495
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["recipients"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["recipient"]) {
                // line 496
                echo "        ";
                ob_start(function () { return ''; });
                // line 497
                echo "<span class=\"email-recipient\">";
                echo twig_call_macro($macros["emailMacros"], "macro_email_participant_name_or_me", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "emailAddress", [], "any", false, false, false, 497), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "name", [], "any", false, false, false, 497), ($context["noLink"] ?? null), ($context["knownOnly"] ?? null)], 497, $context, $this->getSourceContext());
                echo "</span>";
                $context["recipientHtml"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 499
                echo "        ";
                $context["recipientHtmlCollection"] = twig_array_merge(($context["recipientHtmlCollection"] ?? null), [0 => ($context["recipientHtml"] ?? null)]);
                // line 500
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipient'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 501
            echo "    ";
            echo twig_join_filter(($context["recipientHtmlCollection"] ?? null), ", ");

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 509
    public function macro_date_smart_format($__date__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "date" => $__date__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 510
            if (($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(twig_date_converter($this->env, ($context["date"] ?? null))) == $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(twig_date_converter($this->env)))) {
                // line 512
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatTime(($context["date"] ?? null), ["timeZone" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()]), "html", null, true);
            } elseif (($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(twig_date_converter($this->env,             // line 513
($context["date"] ?? null))) == $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(twig_date_converter($this->env, "-1days")))) {
                // line 514
                echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("yesterday")), "html", null, true);
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, twig_date_converter($this->env,             // line 515
($context["date"] ?? null)), "format", [0 => "Y"], "method", false, false, false, 515) == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, twig_date_converter($this->env), "format", [0 => "Y"], "method", false, false, false, 515))) {
                // line 517
                echo "        ";
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDay(($context["date"] ?? null), ["timeZone" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()]), "html", null, true);
            } else {
                // line 519
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(($context["date"] ?? null), ["timeZone" => $this->extensions['Oro\Bundle\LocaleBundle\Twig\LocaleExtension']->getTimeZone()]), "html", null, true);
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 528
    public function macro_email_detailed_info_table($__email__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "email" => $__email__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 529
            echo "    ";
            $context["recipientsTo"] = [];
            // line 530
            echo "    ";
            $context["recipientsCc"] = [];
            // line 531
            echo "    ";
            $context["recipientsBcc"] = [];
            // line 532
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "recipients", [], "any", false, false, false, 532));
            foreach ($context['_seq'] as $context["_key"] => $context["recipient"]) {
                // line 533
                echo "        ";
                $context["emailAddressName"] = $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags($this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailAddressName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "name", [], "any", false, false, false, 533)));
                // line 534
                echo "        ";
                $context["emailAddress"] = $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags($this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailAddress(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "name", [], "any", false, false, false, 534)));
                // line 535
                echo "        ";
                if (((($context["emailAddressName"] ?? null) == "") && (($context["emailAddress"] ?? null) == ""))) {
                    // line 536
                    echo "            ";
                    $context["recipientName"] = "";
                    // line 537
                    echo "        ";
                } else {
                    // line 538
                    echo "            ";
                    $context["recipientName"] = (((($context["emailAddressName"] ?? null) . " <bdo dir=\"ltr\">&lt;") . ($context["emailAddress"] ?? null)) . "&gt;</bdo>");
                    // line 539
                    echo "        ";
                }
                // line 540
                echo "        ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "type", [], "any", false, false, false, 540) == "to")) {
                    // line 541
                    echo "            ";
                    $context["recipientsTo"] = twig_array_merge(($context["recipientsTo"] ?? null), [0 => ($context["recipientName"] ?? null)]);
                    // line 542
                    echo "        ";
                } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "type", [], "any", false, false, false, 542) == "cc")) {
                    // line 543
                    echo "            ";
                    $context["recipientsCc"] = twig_array_merge(($context["recipientsCc"] ?? null), [0 => ($context["recipientName"] ?? null)]);
                    // line 544
                    echo "        ";
                } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["recipient"], "type", [], "any", false, false, false, 544) == "bcc")) {
                    // line 545
                    echo "            ";
                    $context["recipientsBcc"] = twig_array_merge(($context["recipientsBcc"] ?? null), [0 => ($context["recipientName"] ?? null)]);
                    // line 546
                    echo "        ";
                }
                // line 547
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['recipient'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 548
            echo "    ";
            $context["fromUserName"] = $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags($this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailAddressName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromName", [], "any", false, false, false, 548)));
            // line 549
            echo "    ";
            $context["fromEmailAddress"] = (("<bdo dir=\"ltr\">&lt;" . $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags($this->extensions['Oro\Bundle\EmailBundle\Twig\EmailExtension']->getEmailAddress(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["email"] ?? null), "fromName", [], "any", false, false, false, 549)))) . "&gt;</bdo>");
            // line 550
            echo "    ";
            $context["data"] = [0 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("From"), "value" => ((            // line 552
($context["fromUserName"] ?? null)) ? (((("<b>" . ($context["fromUserName"] ?? null)) . "</b> ") . ($context["fromEmailAddress"] ?? null))) : ((("<b>" . ($context["fromEmailAddress"] ?? null)) . "</b>"))), "cssClass" => "autor"], 1 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("To"), "value" => twig_join_filter(            // line 556
($context["recipientsTo"] ?? null), ",<br/>")], 2 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Cc"), "value" => twig_join_filter(            // line 559
($context["recipientsCc"] ?? null), ",<br/>")], 3 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Bcc"), "value" => twig_join_filter(            // line 562
($context["recipientsBcc"] ?? null), ",<br/>")], 4 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Date"), "value" => twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 565
($context["email"] ?? null), "sentAt", [], "any", false, false, false, 565)))], 5 => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Subject"), "value" => $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlStripTags(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 568
($context["email"] ?? null), "subject", [], "any", false, false, false, 568))]];
            // line 570
            echo "    <div class=\"email-detail-info\">
        <table class=\"email-detail-info__table\">
            ";
            // line 572
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["data"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
                // line 573
                echo "                ";
                if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "value", [], "any", false, false, false, 573))) {
                    // line 574
                    echo "                    <tr class=\"email-detail-info__tr\">
                        <td class=\"email-detail-info__td\">
                            <div class=\"email-detail-info__label\">";
                    // line 576
                    echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "label", [], "any", false, false, false, 576);
                    echo ":</div>
                        </td>
                        <td class=\"email-detail-info__td\">
                            <div  class=\"email-detail-info__value ";
                    // line 579
                    ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "cssClass", [], "any", true, true, false, 579)) ? (print (twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "cssClass", [], "any", false, false, false, 579), "html", null, true))) : (print ("")));
                    echo "\">";
                    echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["item"], "value", [], "any", false, false, false, 579);
                    echo "</div>
                        </td>
                    </tr>
                ";
                }
                // line 583
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 584
            echo "        </table>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 593
    public function macro_email_short_body($__emailBody__ = null, $__length__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "emailBody" => $__emailBody__,
            "length" => $__length__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 594
            $context["length"] = ((array_key_exists("length", $context)) ? (_twig_default_filter(($context["length"] ?? null), 150)) : (150));
            // line 595
            echo twig_replace_filter(twig_slice($this->env, $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->pregReplace(twig_striptags(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["emailBody"] ?? null), "textBody", [], "any", false, false, false, 595)), "/-{2,}/", "--"), 0, ($context["length"] ?? null)), ["--" => "&mdash;"]);
            echo "
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 601
    public function macro_renderMailboxConfigTitleAndButtons($__pageTitle__ = null, $__buttons__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "pageTitle" => $__pageTitle__,
            "buttons" => $__buttons__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 602
            echo "    <div class=\"container-fluid page-title\">
        <div class=\"navigation navbar-extra navbar-extra-right\">
            <div class=\"row\">
                <div class=\"pull-left pull-left-extra\">
                    <div class=\"page-title__path\">
                        <div class=\"top-row\">
                            <div class=\"page-title__entity-title-wrapper\">
                                ";
            // line 609
            if (twig_test_iterable(($context["pageTitle"] ?? null))) {
                // line 610
                echo "                                    ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["pageTitle"] ?? null));
                $context['loop'] = [
                  'parent' => $context['_parent'],
                  'index0' => 0,
                  'index'  => 1,
                  'first'  => true,
                ];
                if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                    $length = count($context['_seq']);
                    $context['loop']['revindex0'] = $length - 1;
                    $context['loop']['revindex'] = $length;
                    $context['loop']['length'] = $length;
                    $context['loop']['last'] = 1 === $length;
                }
                foreach ($context['_seq'] as $context["_key"] => $context["title"]) {
                    // line 611
                    echo "                                        ";
                    if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 611)) {
                        // line 612
                        echo "                                            <div class=\"sub-title\">
                                                ";
                        // line 613
                        if (((twig_test_iterable($context["title"]) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "link", [], "any", true, true, false, 613)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "label", [], "any", true, true, false, 613))) {
                            // line 614
                            echo "                                                    <a href=\"";
                            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "link", [], "any", false, false, false, 614), "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "label", [], "any", false, false, false, 614), "html", null, true);
                            echo "</a>
                                                ";
                        } else {
                            // line 616
                            echo "                                                    ";
                            echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                            echo "
                                                ";
                        }
                        // line 618
                        echo "                                            </div>
                                            <span class=\"separator\">/</span>
                                        ";
                    } else {
                        // line 621
                        echo "                                            <h1 class=\"page-title__entity-title\">
                                                ";
                        // line 622
                        if (((twig_test_iterable($context["title"]) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "link", [], "any", true, true, false, 622)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "label", [], "any", true, true, false, 622))) {
                            // line 623
                            echo "                                                    <a href=\"";
                            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "link", [], "any", false, false, false, 623), "html", null, true);
                            echo "\">";
                            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["title"], "label", [], "any", false, false, false, 623), "html", null, true);
                            echo "</a>
                                                ";
                        } else {
                            // line 625
                            echo "                                                    ";
                            echo twig_escape_filter($this->env, $context["title"], "html", null, true);
                            echo "
                                                ";
                        }
                        // line 627
                        echo "                                            </h1>
                                        ";
                    }
                    // line 629
                    echo "                                    ";
                    ++$context['loop']['index0'];
                    ++$context['loop']['index'];
                    $context['loop']['first'] = false;
                    if (isset($context['loop']['length'])) {
                        --$context['loop']['revindex0'];
                        --$context['loop']['revindex'];
                        $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                    }
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['title'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 630
                echo "                                ";
            } else {
                // line 631
                echo "                                    <h1 class=\"page-title__entity-title\">
                                        ";
                // line 632
                echo twig_escape_filter($this->env, ($context["pageTitle"] ?? null), "html", null, true);
                echo "
                                    </h1>
                                ";
            }
            // line 635
            echo "                            </div>
                        </div>
                    </div>
                </div>
                <div class=\"pull-right title-buttons-container\">
                    ";
            // line 640
            echo ($context["buttons"] ?? null);
            echo "
                </div>
            </div>
        </div>
    </div>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroEmail/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  1410 => 640,  1403 => 635,  1397 => 632,  1394 => 631,  1391 => 630,  1377 => 629,  1373 => 627,  1367 => 625,  1359 => 623,  1357 => 622,  1354 => 621,  1349 => 618,  1343 => 616,  1335 => 614,  1333 => 613,  1330 => 612,  1327 => 611,  1309 => 610,  1307 => 609,  1298 => 602,  1284 => 601,  1273 => 595,  1271 => 594,  1257 => 593,  1246 => 584,  1240 => 583,  1231 => 579,  1225 => 576,  1221 => 574,  1218 => 573,  1214 => 572,  1210 => 570,  1208 => 568,  1207 => 565,  1206 => 562,  1205 => 559,  1204 => 556,  1203 => 552,  1201 => 550,  1198 => 549,  1195 => 548,  1189 => 547,  1186 => 546,  1183 => 545,  1180 => 544,  1177 => 543,  1174 => 542,  1171 => 541,  1168 => 540,  1165 => 539,  1162 => 538,  1159 => 537,  1156 => 536,  1153 => 535,  1150 => 534,  1147 => 533,  1142 => 532,  1139 => 531,  1136 => 530,  1133 => 529,  1120 => 528,  1110 => 519,  1106 => 517,  1104 => 515,  1102 => 514,  1100 => 513,  1097 => 512,  1095 => 510,  1082 => 509,  1072 => 501,  1066 => 500,  1063 => 499,  1058 => 497,  1055 => 496,  1050 => 495,  1048 => 494,  1045 => 493,  1043 => 492,  1028 => 491,  1018 => 480,  1016 => 479,  1012 => 476,  1009 => 474,  1007 => 473,  1005 => 472,  1004 => 471,  1003 => 470,  1000 => 469,  998 => 468,  982 => 467,  965 => 454,  955 => 452,  953 => 451,  939 => 450,  922 => 437,  916 => 434,  912 => 433,  908 => 432,  904 => 430,  902 => 427,  901 => 426,  900 => 425,  898 => 422,  896 => 421,  889 => 419,  884 => 418,  874 => 416,  872 => 415,  866 => 414,  859 => 412,  854 => 411,  852 => 410,  848 => 408,  842 => 405,  839 => 404,  833 => 401,  829 => 400,  824 => 398,  819 => 397,  817 => 396,  813 => 394,  810 => 393,  807 => 392,  804 => 391,  801 => 390,  798 => 389,  796 => 388,  792 => 387,  790 => 386,  775 => 385,  761 => 374,  748 => 373,  744 => 372,  742 => 371,  724 => 370,  716 => 366,  713 => 365,  711 => 364,  709 => 363,  707 => 362,  703 => 361,  701 => 360,  698 => 359,  696 => 358,  681 => 357,  670 => 346,  665 => 344,  663 => 343,  659 => 342,  652 => 341,  650 => 340,  647 => 338,  645 => 337,  643 => 336,  640 => 335,  638 => 334,  624 => 333,  614 => 324,  602 => 322,  600 => 321,  597 => 320,  594 => 319,  591 => 318,  589 => 317,  586 => 316,  583 => 315,  580 => 314,  577 => 313,  575 => 312,  572 => 311,  569 => 310,  566 => 309,  563 => 308,  561 => 307,  558 => 306,  556 => 305,  542 => 304,  531 => 293,  528 => 291,  526 => 290,  522 => 287,  520 => 286,  518 => 285,  515 => 284,  513 => 283,  497 => 282,  486 => 269,  483 => 267,  481 => 266,  477 => 263,  475 => 262,  473 => 261,  470 => 260,  468 => 259,  452 => 258,  439 => 246,  434 => 244,  429 => 243,  426 => 242,  423 => 241,  421 => 240,  406 => 239,  393 => 230,  385 => 228,  382 => 227,  380 => 226,  365 => 225,  356 => 215,  355 => 214,  354 => 213,  353 => 212,  350 => 211,  348 => 210,  334 => 209,  325 => 200,  312 => 199,  300 => 191,  297 => 190,  295 => 187,  294 => 186,  293 => 177,  292 => 175,  278 => 164,  263 => 151,  260 => 133,  210 => 74,  196 => 63,  180 => 50,  174 => 47,  167 => 43,  161 => 40,  151 => 33,  145 => 30,  139 => 27,  134 => 25,  130 => 24,  123 => 20,  117 => 17,  111 => 14,  106 => 12,  102 => 11,  95 => 6,  92 => 5,  89 => 4,  86 => 3,  83 => 2,  69 => 1,  64 => 597,  61 => 587,  58 => 522,  55 => 503,  52 => 483,  49 => 457,  46 => 443,  43 => 377,  40 => 297,  37 => 273,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEmail/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Resources/views/macros.html.twig");
    }
}
