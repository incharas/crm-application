<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/oro-toolbar.scss */
class __TwigTemplate_afb4287387d1f65c8f416aabcfb7dd58 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

.oro-toolbar {
    position: relative;

    display: inline-block;
    vertical-align: top;

    &__icon {
        display: inline-block;
        vertical-align: top;

        margin-left: \$oro-toolbar-icon-offset;
    }

    &__content {
        min-width: \$oro-toolbar-content-min-width;

        padding: \$oro-toolbar-content-inner-offset;

        line-height: \$oro-toolbar-content-line-height;
        font-family: \$oro-toolbar-content-font-family;
        text-align: \$oro-toolbar-content-text-align;

        cursor: pointer;
    }

    &__dropdown {
        position: absolute;
        top: 100%;
        right: 0;
        left: auto;
        margin: 0;
        padding: \$oro-toolbar-dropdown-inner-offset;
        z-index: \$oro-toolbar-dropdown-z-index;

        display: none;
        min-width: \$oro-toolbar-toolbar-min-width;

        font-size: \$base-font-size;

        background: \$base-ui-popup-background;
        border-radius: \$base-ui-popup-border-radius;
        box-shadow: \$base-ui-popup-box-shadow;
    }

    &__list {
        text-align: left;

        white-space: nowrap;

        @include list-normalize;

        &-item {
            margin-top: \$oro-toolbar-list-item-offset;

            &:first-child {
                margin-top: 0;
            }
        }
    }

    &__link {
        display: block;
        line-height: 2;

        color: \$oro-toolbar-link-color;

        &:hover {
            color: \$oro-toolbar-link-hover-color;
        }

        &--active {
            font-weight: font-weight('bold');
        }
    }

    &__currency,
    &__text {
        display: inline-block;
        vertical-align: baseline;
    }

    &__currency {
        margin-right: \$offset-y-m * .5;
    }

    // toolbar open
    &.active,
    &.show {
        .oro-toolbar__content {
            box-shadow: \$oro-toolbar-open-content-box-shadow;
        }

        .oro-toolbar__dropdown {
            display: block;
        }
    }

    // toolbar in fullscreen popup
    &.fullscreen-mode {
        display: block;

        .oro-toolbar__list-item {
            margin-top: 0;
        }

        .oro-toolbar__link {
            padding: 8px 16px;

            text-transform: uppercase;

            border-bottom: \$oro-toolbar-fullscreen-link-border;
        }
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/oro-toolbar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/oro-toolbar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/oro-toolbar.scss");
    }
}
