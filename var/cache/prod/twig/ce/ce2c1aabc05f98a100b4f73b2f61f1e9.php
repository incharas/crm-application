<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/layouts/default/page/noscript.html.twig */
class __TwigTemplate_ef053b1f9d09c25b27b02ffba470924e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_nosctipt_widget' => [$this, 'block__nosctipt_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_nosctipt_widget', $context, $blocks);
    }

    public function block__nosctipt_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <noscript>
        <div class=\"notification-flash notification-flash--error\" role=\"alert\">
            <div class=\"notification-flash__text notification-flash__text--center\">
                ";
        // line 5
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.disabled_js"), "html", null, true);
        echo "
            </div>
        </div>
    </noscript>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/layouts/default/page/noscript.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  50 => 5,  45 => 2,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/layouts/default/page/noscript.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/noscript.html.twig");
    }
}
