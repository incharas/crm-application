<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNote/Note/js/activityItemTemplate.html.twig */
class __TwigTemplate_990a48b560cfaf86a580f7ceb71c0939 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'activityDetails' => [$this, 'block_activityDetails'],
            'activityActions' => [$this, 'block_activityActions'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroActivityList/ActivityList/js/activityItemTemplate.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroNote/Note/js/activityItemTemplate.html.twig", 2)->unwrap();
        // line 3
        $context["entityClass"] = "Oro\\Bundle\\NoteBundle\\Entity\\Note";
        // line 4
        $context["entityName"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($this->extensions['Oro\Bundle\EntityConfigBundle\Twig\ConfigExtension']->getClassConfigValue(($context["entityClass"] ?? null), "label"));
        // line 1
        $this->parent = $this->loadTemplate("@OroActivityList/ActivityList/js/activityItemTemplate.html.twig", "@OroNote/Note/js/activityItemTemplate.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_activityDetails($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        echo twig_escape_filter($this->env, ($context["entityName"] ?? null), "html", null, true);
        echo "
    <% var template = (verb == 'create')
        ? ";
        // line 9
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_added_by"));
        echo "
        : ";
        // line 10
        echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_updated_by"));
        echo ";
    %>
    <%= _.template(template, { interpolate: /\\{\\{(.+?)\\}\\}/g })({
        user: owner_url ? '<a class=\"user\" href=\"' + owner_url + '\">' +  _.escape(owner) + '</a>' :  '<span class=\"user\">' + _.escape(owner) + '</span>',
        date: '<i class=\"date\">' + createdAt + '</i>',
        editor: editor_url ? '<a class=\"user\" href=\"' + editor_url + '\">' +  _.escape(editor) + '</a>' : _.escape(editor),
        editor_date: '<i class=\"date\">' + updatedAt + '</i>'
    }) %>
";
    }

    // line 20
    public function block_activityActions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroNote/Note/js/activityItemTemplate.html.twig", 21)->unwrap();
        // line 22
        echo "
    ";
        // line 23
        ob_start(function () { return ''; });
        // line 24
        echo "        ";
        // line 25
        echo "        <% if (editable) { %>
            ";
        // line 26
        echo twig_call_macro($macros["AC"], "macro_activity_context_link", [], 26, $context, $this->getSourceContext());
        echo "
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 29
        echo "    ";
        $context["actions"] = [0 => ($context["action"] ?? null)];
        // line 30
        echo "
    ";
        // line 31
        ob_start(function () { return ''; });
        // line 32
        echo "        <% if (editable) { %>
        <a href=\"#\" class=\"dropdown-item action item-edit-button\"
           title=\"";
        // line 34
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_update", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\"
           data-action-extra-options=\"";
        // line 35
        echo twig_escape_filter($this->env, json_encode(["dialogOptions" => ["width" => 1000]]), "html", null, true);
        echo "\">
            <span class=\"fa-pencil-square-o hide-text\" aria-hidden=\"true\">";
        // line 36
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_update", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 37
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_update", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 41
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 42
        echo "
    ";
        // line 43
        ob_start(function () { return ''; });
        // line 44
        echo "        <% if (removable) { %>
        <a href=\"#\" class=\"dropdown-item action item-remove-button\"
           title=\"";
        // line 46
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_delete", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "\">
            <span class=\"fa-trash-o hide-text\" aria-hidden=\"true\">";
        // line 47
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_delete", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "</span>
            ";
        // line 48
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.note.note_delete", ["{{ entity }}" => ($context["entityName"] ?? null)]), "html", null, true);
        echo "
        </a>
        <% } %>
    ";
        $context["action"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 52
        echo "    ";
        $context["actions"] = twig_array_merge(($context["actions"] ?? null), [0 => ($context["action"] ?? null)]);
        // line 53
        echo "
    ";
        // line 54
        $this->displayParentBlock("activityActions", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroNote/Note/js/activityItemTemplate.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  168 => 54,  165 => 53,  162 => 52,  155 => 48,  151 => 47,  147 => 46,  143 => 44,  141 => 43,  138 => 42,  135 => 41,  128 => 37,  124 => 36,  120 => 35,  116 => 34,  112 => 32,  110 => 31,  107 => 30,  104 => 29,  98 => 26,  95 => 25,  93 => 24,  91 => 23,  88 => 22,  85 => 21,  81 => 20,  68 => 10,  64 => 9,  58 => 7,  54 => 6,  49 => 1,  47 => 4,  45 => 3,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNote/Note/js/activityItemTemplate.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NoteBundle/Resources/views/Note/js/activityItemTemplate.html.twig");
    }
}
