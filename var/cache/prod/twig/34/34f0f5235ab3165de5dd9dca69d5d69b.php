<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/CalendarEvent/view.html.twig */
class __TwigTemplate_8ca434d7dd305dcdf19200b73e55a1b9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'stats' => [$this, 'block_stats'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 2)->unwrap();
        // line 3
        $macros["CalendarUI"] = $this->macros["CalendarUI"] = $this->loadTemplate("@OroCalendar/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 3)->unwrap();
        // line 4
        $macros["invitations"] = $this->macros["invitations"] = $this->loadTemplate("@OroCalendar/invitations.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 4)->unwrap();
        // line 5
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 5)->unwrap();
        // line 6
        $macros["AC"] = $this->macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 6)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.title%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 8
($context["entity"] ?? null), "title", [], "any", true, true, false, 8)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 8), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 11)->unwrap();
        // line 12
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 12)->unwrap();
        // line 13
        echo "
    ";
        // line 14
        if ((twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "parent", [], "any", false, false, false, 14)) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null)))) {
            // line 15
            echo "        ";
            // line 16
            echo "        ";
            echo twig_call_macro($macros["AC"], "macro_addContextButton", [($context["entity"] ?? null)], 16, $context, $this->getSourceContext());
            echo "
        ";
            // line 17
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["entity"] ?? null), "id", [], "any", false, false, false, 18)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_label")]], 17, $context, $this->getSourceContext());
            // line 20
            echo "
    ";
        }
        // line 22
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 23
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_delete", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 24
($context["entity"] ?? null), "id", [], "any", false, false, false, 24), "notifyAttendees" => "all"]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_index"), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 27
($context["entity"] ?? null), "id", [], "any", false, false, false, 27), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_label")]], 23, $context, $this->getSourceContext());
            // line 29
            echo "
    ";
        }
    }

    // line 33
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 34
        echo "    ";
        $macros["AC"] = $this->loadTemplate("@OroActivity/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 34)->unwrap();
        // line 35
        echo "
    ";
        // line 37
        echo "    <li class=\"context-data activity-context-activity-block\">
        ";
        // line 38
        echo twig_call_macro($macros["AC"], "macro_activity_contexts", [($context["entity"] ?? null)], 38, $context, $this->getSourceContext());
        echo "
    </li>
";
    }

    // line 42
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 44
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_calendar_event_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 47
($context["entity"] ?? null), "title", [], "any", true, true, false, 47)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 47), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))];
        // line 49
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 52
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "    ";
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "
    <div class=\"pull-left\">
        ";
        // line 55
        $context["statusCode"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "invitationStatus", [], "any", false, false, false, 55);
        // line 56
        echo "        ";
        $context["invitationClass"] = twig_call_macro($macros["invitations"], "macro_get_invitatition_badge_class", [($context["statusCode"] ?? null)], 56, $context, $this->getSourceContext());
        // line 57
        echo "        ";
        if (($context["invitationClass"] ?? null)) {
            // line 58
            echo "            <div class=\"invitation-status badge badge-";
            echo twig_escape_filter($this->env, ($context["invitationClass"] ?? null), "html", null, true);
            echo " status-";
            echo twig_escape_filter($this->env, ($context["invitationClass"] ?? null), "html", null, true);
            echo "\">
                <i class=\"icon-status-";
            // line 59
            echo twig_escape_filter($this->env, ($context["invitationClass"] ?? null), "html", null, true);
            echo " fa-circle\"></i>
                ";
            // line 60
            echo twig_call_macro($macros["invitations"], "macro_calendar_event_invitation_status", [($context["statusCode"] ?? null)], 60, $context, $this->getSourceContext());
            echo "
            </div>
        ";
        }
        // line 63
        echo "    </div>
";
    }

    // line 66
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 67
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 67)->unwrap();
        // line 69
        ob_start(function () { return ''; });
        // line 70
        echo "<div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 72
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.title.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "title", [], "any", false, false, false, 72)], 72, $context, $this->getSourceContext());
        echo "
                ";
        // line 73
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 73)], 73, $context, $this->getSourceContext());
        echo "
                ";
        // line 74
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.start.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "start", [], "any", false, false, false, 74))], 74, $context, $this->getSourceContext());
        echo "
                ";
        // line 75
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.end.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "end", [], "any", false, false, false, 75))], 75, $context, $this->getSourceContext());
        echo "
                ";
        // line 76
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.all_day.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "allDay", [], "any", false, false, false, 76)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No")))], 76, $context, $this->getSourceContext());
        echo "
                ";
        // line 77
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.organizer.label"), twig_call_macro($macros["CalendarUI"], "macro_renderOrganizer", [($context["entity"] ?? null)], 77, $context, $this->getSourceContext())], 77, $context, $this->getSourceContext());
        echo "
                ";
        // line 78
        $context["invitationEvent"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "parent", [], "any", false, false, false, 78)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "parent", [], "any", false, false, false, 78)) : (($context["entity"] ?? null)));
        // line 79
        echo "                ";
        if ((($context["invitationEvent"] ?? null) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["invitationEvent"] ?? null), "attendees", [], "any", false, false, false, 79)))) {
            // line 80
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.attendees.label"), twig_call_macro($macros["invitations"], "macro_calendar_event_invitation", [($context["invitationEvent"] ?? null)], 80, $context, $this->getSourceContext())], 80, $context, $this->getSourceContext());
            echo "
                ";
        }
        // line 82
        echo "                ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurrence", [], "any", false, false, false, 82)) {
            // line 83
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.recurrence.label"), $this->extensions['Oro\Bundle\CalendarBundle\Twig\RecurrenceExtension']->getRecurrenceTextValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurrence", [], "any", false, false, false, 83))], 83, $context, $this->getSourceContext());
            echo "
                ";
        }
        // line 85
        echo "                ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurringEvent", [], "any", false, false, false, 85)) {
            // line 86
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.calendarevent.recurrence.exception.label"), $this->extensions['Oro\Bundle\CalendarBundle\Twig\RecurrenceExtension']->getRecurrenceTextValue(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurringEvent", [], "any", false, false, false, 86), "recurrence", [], "any", false, false, false, 86)) ? (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "recurringEvent", [], "any", false, false, false, 86), "recurrence", [], "any", false, false, false, 86)) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "parent", [], "any", false, false, false, 86), "recurringEvent", [], "any", false, false, false, 86), "recurrence", [], "any", false, false, false, 86))))], 86, $context, $this->getSourceContext());
            echo "
                ";
        }
        // line 88
        echo "                ";
        if ((array_key_exists("canChangeInvitationStatus", $context) && ($context["canChangeInvitationStatus"] ?? null))) {
            // line 89
            echo "                    ";
            $this->loadTemplate("@OroCalendar/CalendarEvent/invitationControl.html.twig", "@OroCalendar/CalendarEvent/view.html.twig", 89)->display(twig_array_merge($context, ["entity" => ($context["entity"] ?? null), "triggerEventName" => ""]));
            // line 90
            echo "                ";
        }
        // line 91
        echo "            </div>
            <div class=\"responsive-block\">
                ";
        // line 93
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 93, $context, $this->getSourceContext());
        echo "
            </div>
        </div>";
        $context["calendarEventInformation"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 98
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["data" => [0 =>         // line 103
($context["calendarEventInformation"] ?? null)]]]]];
        // line 107
        echo "
    ";
        // line 108
        $context["id"] = "calendarEventView";
        // line 109
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 110
        echo "
    ";
        // line 111
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/CalendarEvent/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  282 => 111,  279 => 110,  276 => 109,  274 => 108,  271 => 107,  269 => 103,  268 => 98,  262 => 93,  258 => 91,  255 => 90,  252 => 89,  249 => 88,  243 => 86,  240 => 85,  234 => 83,  231 => 82,  225 => 80,  222 => 79,  220 => 78,  216 => 77,  212 => 76,  208 => 75,  204 => 74,  200 => 73,  196 => 72,  192 => 70,  190 => 69,  187 => 67,  183 => 66,  178 => 63,  172 => 60,  168 => 59,  161 => 58,  158 => 57,  155 => 56,  153 => 55,  147 => 53,  143 => 52,  136 => 49,  134 => 47,  133 => 44,  131 => 43,  127 => 42,  120 => 38,  117 => 37,  114 => 35,  111 => 34,  107 => 33,  101 => 29,  99 => 27,  98 => 24,  96 => 23,  93 => 22,  89 => 20,  87 => 18,  86 => 17,  81 => 16,  79 => 15,  77 => 14,  74 => 13,  71 => 12,  68 => 11,  64 => 10,  59 => 1,  57 => 8,  54 => 6,  52 => 5,  50 => 4,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/CalendarEvent/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/CalendarEvent/view.html.twig");
    }
}
