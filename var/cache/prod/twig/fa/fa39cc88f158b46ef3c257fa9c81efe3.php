<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/default/scss/settings/mixins/table-base.scss */
class __TwigTemplate_db119879dccc718ec924edb56c1e28f2 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@mixin table-base(
    \$table-width:         \$table-width,
    \$table-text-align:    \$table-text-align,
    \$table-margin-bottom: \$table-margin-bottom,
    \$head-padding:        \$table-head-padding,
    \$row-padding:         \$table-row-padding
) {
    width: \$table-width;
    margin-bottom: \$table-margin-bottom;

    border-spacing: 0;
    border-collapse: collapse;

    &__td {
        padding: \$row-padding;

        text-align: \$table-text-align;
    }

    &__th {
        padding: \$head-padding;

        text-align: \$table-text-align;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/default/scss/settings/mixins/table-base.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/default/scss/settings/mixins/table-base.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/default/scss/settings/mixins/table-base.scss");
    }
}
