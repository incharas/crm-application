<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroWorkflow/WorkflowDefinition/configure.html.twig */
class __TwigTemplate_24713e0b1d9465ff10da8ff2f8fb4d79 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'stats' => [$this, 'block_stats'],
            'navButtons' => [$this, 'block_navButtons'],
            'breadcrumbs' => [$this, 'block_breadcrumbs'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%workflow_definition.label%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "label", [], "any", false, false, false, 3), [], "workflows")]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroWorkflow/WorkflowDefinition/configure.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 5
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 6
        echo "    <li>";
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.created_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 6)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "createdAt", [], "any", false, false, false, 6)), "html", null, true))) : (print ("N/A")));
        echo "</li>
    <li>";
        // line 7
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.updated_at"), "html", null, true);
        echo ": ";
        ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 7)) ? (print (twig_escape_filter($this->env, $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "updatedAt", [], "any", false, false, false, 7)), "html", null, true))) : (print ("N/A")));
        echo "</li>
";
    }

    // line 10
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/configure.html.twig", 11)->unwrap();
        // line 12
        echo "
    ";
        // line 13
        $context["html"] = (twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_workflow_definition_view", "params" => ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 14
($context["entity"] ?? null), "name", [], "any", false, false, false, 14)]]], 13, $context, $this->getSourceContext()) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [], 15, $context, $this->getSourceContext()));
        // line 16
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 16, $context, $this->getSourceContext());
        echo "

    ";
        // line 18
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_view", ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "name", [], "any", false, false, false, 18)])], 18, $context, $this->getSourceContext());
        echo "
";
    }

    // line 21
    public function block_breadcrumbs($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 22
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroWorkflow/WorkflowDefinition/configure.html.twig", 22)->unwrap();
        // line 23
        echo "
    ";
        // line 24
        $context["breadcrumbs"] = ["entity" =>         // line 25
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.entity_plural_label"), "additional" => [0 => ["indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_workflow_definition_view", ["name" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 29
($context["entity"] ?? null), "name", [], "any", false, false, false, 29)]), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 30
($context["entity"] ?? null), "label", [], "any", false, false, false, 30), [], "workflows")]], "entityTitle" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.configuration.label")];
        // line 34
        echo "
    ";
        // line 35
        $this->displayParentBlock("breadcrumbs", $context, $blocks);
        echo "

    <span class=\"page-title__status\">
        ";
        // line 38
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "isActive", [], "any", false, false, false, 38)) {
            // line 39
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Active"), "enabled"], 39, $context, $this->getSourceContext());
            echo "
        ";
        } else {
            // line 41
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_badge", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Inactive"), "disabled"], 41, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 43
        echo "    </span>
";
    }

    // line 46
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 47
        echo "    ";
        $context["data"] = [];
        // line 48
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["child"]) {
            // line 49
            echo "        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["child"], "children", [], "any", false, false, false, 49)) {
                // line 50
                echo "            ";
                $context["data"] = twig_array_merge(($context["data"] ?? null), [0 =>                 $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock($context["child"], 'form_row_collection')]);
                // line 51
                echo "        ";
            } else {
                // line 52
                echo "            ";
                $context["data"] = twig_array_merge(($context["data"] ?? null), [0 => $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["child"], 'row')]);
                // line 53
                echo "        ";
            }
            // line 54
            echo "    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['child'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 55
        echo "
    ";
        // line 56
        $context["id"] = "workflow-variables";
        // line 57
        echo "    ";
        $context["data"] = ["dataBlocks" => [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.workflow.workflowdefinition.configuration.label"), "class" => "nav-item active", "subblocks" => [0 => ["data" =>         // line 61
($context["data"] ?? null)]]]]];
        // line 64
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroWorkflow/WorkflowDefinition/configure.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  182 => 64,  180 => 61,  178 => 57,  176 => 56,  173 => 55,  167 => 54,  164 => 53,  161 => 52,  158 => 51,  155 => 50,  152 => 49,  147 => 48,  144 => 47,  140 => 46,  135 => 43,  129 => 41,  123 => 39,  121 => 38,  115 => 35,  112 => 34,  110 => 30,  109 => 29,  108 => 25,  107 => 24,  104 => 23,  101 => 22,  97 => 21,  91 => 18,  85 => 16,  83 => 14,  82 => 13,  79 => 12,  76 => 11,  72 => 10,  64 => 7,  57 => 6,  53 => 5,  48 => 1,  46 => 3,  38 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroWorkflow/WorkflowDefinition/configure.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Resources/views/WorkflowDefinition/configure.html.twig");
    }
}
