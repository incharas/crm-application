<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/B2bCustomer/widget/customerInfo.html.twig */
class __TwigTemplate_25fdc6ce67da2ccd348f09e8ad58896f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroSales/B2bCustomer/widget/customerInfo.html.twig", 1)->unwrap();
        // line 2
        $macros["address"] = $this->macros["address"] = $this->loadTemplate("@OroAddress/macros.html.twig", "@OroSales/B2bCustomer/widget/customerInfo.html.twig", 2)->unwrap();
        // line 3
        echo "
";
        // line 4
        $context["activeTab"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 4), "get", [0 => "_activeTab"], "method", true, true, false, 4)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, true, false, 4), "get", [0 => "_activeTab"], "method", false, false, false, 4), null)) : (null));
        // line 5
        $context["uniqueSuffix"] = ((("_customer_" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["customer"] ?? null), "id", [], "any", false, false, false, 5)) . "_channel_") . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["channel"] ?? null), "id", [], "any", false, false, false, 5));
        // line 6
        ob_start(function () { return ''; });
        // line 7
        echo "    <div class=\"row-fluid\">
        <div class=\"responsive-block\">
            ";
        // line 9
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.name.label"), twig_call_macro($macros["UI"], "macro_entityViewLink", [        // line 11
($context["customer"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["customer"] ?? null), "name", [], "any", false, false, false, 11), "oro_sales_b2bcustomer_view"], 11, $context, $this->getSourceContext())], 9, $context, $this->getSourceContext());
        // line 12
        echo "
            ";
        // line 13
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.contact.label"), twig_call_macro($macros["UI"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["customer"] ?? null), "contact", [], "any", false, false, false, 15), $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["customer"] ?? null), "contact", [], "any", false, false, false, 15)), "oro_contact_view"], 15, $context, $this->getSourceContext())], 13, $context, $this->getSourceContext());
        // line 16
        echo "
        </div>
        <div class=\"responsive-block\">
            ";
        // line 19
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.shipping_address.label"), twig_call_macro($macros["address"], "macro_renderAddress", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["customer"] ?? null), "shippingAddress", [], "any", false, false, false, 19), true], 19, $context, $this->getSourceContext())], 19, $context, $this->getSourceContext());
        echo "
            ";
        // line 20
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.billing_address.label"), twig_call_macro($macros["address"], "macro_renderAddress", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["customer"] ?? null), "billingAddress", [], "any", false, false, false, 20), true], 20, $context, $this->getSourceContext())], 20, $context, $this->getSourceContext());
        echo "
        </div>
    </div>
";
        $context["customer_general_info"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 24
        $context["tabs"] = [0 => ["alias" => ("oro_sales_b2bcustomer_general_info" .         // line 26
($context["uniqueSuffix"] ?? null)), "widgetType" => "block", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.sections.general"), "content" =>         // line 29
($context["customer_general_info"] ?? null), "url" => null]];
        // line 33
        echo "
";
        // line 34
        if ($this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\SalesBundle\\Entity\\Lead", "entities")) {
            // line 35
            $context["tabs"] = twig_array_merge(($context["tabs"] ?? null), [0 => ["alias" => ("oro_sales_b2bcustomer_leads" .             // line 37
($context["uniqueSuffix"] ?? null)), "widgetType" => "block", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.leads.label"), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_b2bcustomer_widget_leads", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 40
($context["customer"] ?? null), "id", [], "any", false, false, false, 40)])]]);
        }
        // line 44
        echo "
";
        // line 45
        if ($this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\SalesBundle\\Entity\\Opportunity", "entities")) {
            // line 46
            $context["tabs"] = twig_array_merge(($context["tabs"] ?? null), [0 => ["alias" => ("oro_sales_b2bcustomer_opportunities" .             // line 48
($context["uniqueSuffix"] ?? null)), "widgetType" => "block", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.opportunities.label"), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_sales_b2bcustomer_widget_opportunities", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 51
($context["customer"] ?? null), "id", [], "any", false, false, false, 51)])]]);
        }
        // line 55
        echo "
";
        // line 56
        $context["tabPanelOptions"] = ["subTabs" => $this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile()];
        // line 59
        if (($context["activeTab"] ?? null)) {
            // line 60
            echo "    ";
            $context["tabPanelOptions"] = twig_array_merge(($context["tabPanelOptions"] ?? null), ["activeTabAlias" => (("oro_sales_b2bcustomer_" .             // line 61
($context["activeTab"] ?? null)) . ($context["uniqueSuffix"] ?? null))]);
        }
        // line 64
        echo "
<div class=\"widget-content row-fluid\">
    <div class=\"account-customer-title\">";
        // line 66
        echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(($context["customer"] ?? null)), "html", null, true);
        echo "
        <div class=\"pull-right label label-large label-success orocrm-channel-lifetime-value-label\">
            ";
        // line 68
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.b2bcustomer.lifetime.label"), "html", null, true);
        echo ":
            <b>";
        // line 69
        echo $this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatCurrency($this->extensions['Oro\Bundle\ChannelBundle\Twig\ChannelExtension']->getLifetimeValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["customer"] ?? null), "account", [], "any", false, false, false, 69), ($context["channel"] ?? null)));
        echo "</b>
        </div>
    </div>
    <div class=\"customer-without-border-tabs\">
        ";
        // line 73
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\TabExtension']->tabPanel($this->env, ($context["tabs"] ?? null), ($context["tabPanelOptions"] ?? null));
        echo "
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/B2bCustomer/widget/customerInfo.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  135 => 73,  128 => 69,  124 => 68,  119 => 66,  115 => 64,  112 => 61,  110 => 60,  108 => 59,  106 => 56,  103 => 55,  100 => 51,  99 => 48,  98 => 46,  96 => 45,  93 => 44,  90 => 40,  89 => 37,  88 => 35,  86 => 34,  83 => 33,  81 => 29,  80 => 26,  79 => 24,  72 => 20,  68 => 19,  63 => 16,  61 => 15,  60 => 13,  57 => 12,  55 => 11,  54 => 9,  50 => 7,  48 => 6,  46 => 5,  44 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/B2bCustomer/widget/customerInfo.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/B2bCustomer/widget/customerInfo.html.twig");
    }
}
