<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCase/Case/view.html.twig */
class __TwigTemplate_9882837b49f42ee29c9566a6f6393c9d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCase/Case/view.html.twig", 2)->unwrap();
        // line 3
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroCase/Case/view.html.twig", 3)->unwrap();
        // line 4
        $macros["U"] = $this->macros["U"] = $this->loadTemplate("@OroUser/macros.html.twig", "@OroCase/Case/view.html.twig", 4)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.subject%" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "subject", [], "any", true, true, false, 6)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "subject", [], "any", false, false, false, 6), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))]]);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroCase/Case/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 8
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 9
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCase/Case/view.html.twig", 9)->unwrap();
        // line 10
        echo "
    ";
        // line 11
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_case_comment_create") && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_case_comment_view"))) {
            // line 12
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_clientButton", [["id" => "add-case-comment-button", "aCss" => "no-hash", "iCss" => "fa-comment-o hide-text", "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.casecomment.action.add"), "dataAttributes" => ["purpose" => "open-dialog-widget"]]], 12, $context, $this->getSourceContext());
            // line 18
            echo "
        <script>
            loadModules(['jquery', 'oroui/js/mediator'],
            function(\$, mediator){
                \$('#add-case-comment-button').on('click', function(e) {
                    e.preventDefault();
                    mediator.trigger('comment:add', this);
                });
            });
        </script>
    ";
        }
        // line 29
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("EDIT", ($context["entity"] ?? null))) {
            // line 30
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_editButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_update", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 31
($context["entity"] ?? null), "id", [], "any", false, false, false, 31)]), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.entity_label")]], 30, $context, $this->getSourceContext());
            // line 33
            echo "
    ";
        }
        // line 35
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null))) {
            // line 36
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_api_delete_case", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 37
($context["entity"] ?? null), "id", [], "any", false, false, false, 37)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_index"), "aCss" => "no-hash remove-button", "id" => "btn-remove-user", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 41
($context["entity"] ?? null), "id", [], "any", false, false, false, 41), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.entity_label")]], 36, $context, $this->getSourceContext());
            // line 43
            echo "
    ";
        }
    }

    // line 47
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 48
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 49
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 52
($context["entity"] ?? null), "subject", [], "any", false, false, false, 52)];
        // line 54
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 57
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 58
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCase/Case/view.html.twig", 58)->unwrap();
        // line 60
        ob_start(function () { return ''; });
        // line 61
        echo "<div class=\"row-fluid form-horizontal\">
            <div class=\"responsive-block\">
                ";
        // line 63
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.subject.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "subject", [], "any", false, false, false, 63)], 63, $context, $this->getSourceContext());
        echo "
                ";
        // line 64
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 64)], 64, $context, $this->getSourceContext());
        echo "
                ";
        // line 65
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.resolution.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "resolution", [], "any", false, false, false, 65)], 65, $context, $this->getSourceContext());
        echo "
                ";
        // line 66
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.source.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "source", [], "any", false, false, false, 66)], 66, $context, $this->getSourceContext());
        echo "
                ";
        // line 67
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.status.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "status", [], "any", false, false, false, 67)], 67, $context, $this->getSourceContext());
        echo "
                ";
        // line 68
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.priority.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "priority", [], "any", false, false, false, 68)], 68, $context, $this->getSourceContext());
        echo "
                ";
        // line 69
        echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.reported_at.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "reportedAt", [], "any", false, false, false, 69))], 69, $context, $this->getSourceContext());
        echo "
                ";
        // line 70
        if ( !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closedAt", [], "any", false, false, false, 70))) {
            // line 71
            echo "                    ";
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.closed_at.label"), $this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "closedAt", [], "any", false, false, false, 71))], 71, $context, $this->getSourceContext());
            echo "
                ";
        }
        // line 74
        ob_start(function () { return ''; });
        // line 75
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 75)) {
            // line 76
            echo twig_call_macro($macros["U"], "macro_render_user_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 76)], 76, $context, $this->getSourceContext());
            echo "
                        ";
            // line 77
            echo twig_call_macro($macros["U"], "macro_user_business_unit_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "owner", [], "any", false, false, false, 77)], 77, $context, $this->getSourceContext());
        }
        $context["owner"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 80
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.owner.label"), ($context["owner"] ?? null)], 80, $context, $this->getSourceContext());
        // line 82
        ob_start(function () { return ''; });
        // line 83
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "assignedTo", [], "any", false, false, false, 83)) {
            // line 84
            echo twig_call_macro($macros["U"], "macro_render_user_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "assignedTo", [], "any", false, false, false, 84)], 84, $context, $this->getSourceContext());
            echo "
                        ";
            // line 85
            echo twig_call_macro($macros["U"], "macro_user_business_unit_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "assignedTo", [], "any", false, false, false, 85)], 85, $context, $this->getSourceContext());
        }
        $context["assignedTo"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 88
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.assigned_to.label"), ($context["assignedTo"] ?? null)], 88, $context, $this->getSourceContext());
        echo "

                ";
        // line 90
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.related_contact.label"), twig_call_macro($macros["UI"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 92
($context["entity"] ?? null), "relatedContact", [], "any", false, false, false, 92), $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "relatedContact", [], "any", false, false, false, 92)), "oro_contact_view"], 92, $context, $this->getSourceContext())], 90, $context, $this->getSourceContext());
        // line 95
        ob_start(function () { return ''; });
        // line 96
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "relatedAccount", [], "any", false, false, false, 96)) {
            // line 97
            if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_account_view")) {
                // line 98
                echo "                            <a href=\"";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "relatedAccount", [], "any", false, false, false, 98), "id", [], "any", false, false, false, 98)]), "html", null, true);
                echo "\">
                                ";
                // line 99
                echo twig_call_macro($macros["UI"], "macro_renderEntityViewLabel", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "relatedAccount", [], "any", false, false, false, 99), "name", "oro.account.entity_label"], 99, $context, $this->getSourceContext());
                echo "
                            </a>
                        ";
            } else {
                // line 102
                echo "                            ";
                echo twig_call_macro($macros["UI"], "macro_renderEntityViewLabel", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "relatedAccount", [], "any", false, false, false, 102), "name"], 102, $context, $this->getSourceContext());
                echo "
                        ";
            }
        }
        $context["relatedAccount"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 106
        echo twig_call_macro($macros["UI"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.caseentity.related_account.label"), ($context["relatedAccount"] ?? null)], 106, $context, $this->getSourceContext());
        echo "
            </div>
            <div class=\"responsive-block\">
                ";
        // line 109
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 109, $context, $this->getSourceContext());
        echo "
            </div>
        </div>";
        $context["caseInformation"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 114
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.block.general"), "subblocks" => [0 => ["data" => [0 =>         // line 118
($context["caseInformation"] ?? null)]]]]];
        // line 122
        echo "
    ";
        // line 123
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_case_comment_view")) {
            // line 124
            echo "        ";
            ob_start(function () { return ''; });
            // line 125
            echo "            ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderWidget($this->env, ["widgetType" => "block", "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_case_widget_comments", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 127
($context["entity"] ?? null), "id", [], "any", false, false, false, 127)]), "cssClass" => "list-widget comments-widget", "title" => ""]);
            // line 130
            echo "
        ";
            $context["caseComments"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
            // line 132
            echo "
        ";
            // line 133
            $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.case.block.comments"), "subblocks" => [0 => ["data" => [0 =>             // line 137
($context["caseComments"] ?? null)]]]]]);
            // line 141
            echo "    ";
        }
        // line 142
        echo "
    ";
        // line 143
        $context["id"] = "caseView";
        // line 144
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 145
        echo "
    ";
        // line 146
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCase/Case/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  286 => 146,  283 => 145,  280 => 144,  278 => 143,  275 => 142,  272 => 141,  270 => 137,  269 => 133,  266 => 132,  262 => 130,  260 => 127,  258 => 125,  255 => 124,  253 => 123,  250 => 122,  248 => 118,  247 => 114,  241 => 109,  235 => 106,  227 => 102,  221 => 99,  216 => 98,  214 => 97,  212 => 96,  210 => 95,  208 => 92,  207 => 90,  202 => 88,  198 => 85,  194 => 84,  192 => 83,  190 => 82,  188 => 80,  184 => 77,  180 => 76,  178 => 75,  176 => 74,  170 => 71,  168 => 70,  164 => 69,  160 => 68,  156 => 67,  152 => 66,  148 => 65,  144 => 64,  140 => 63,  136 => 61,  134 => 60,  131 => 58,  127 => 57,  120 => 54,  118 => 52,  117 => 49,  115 => 48,  111 => 47,  105 => 43,  103 => 41,  102 => 37,  100 => 36,  97 => 35,  93 => 33,  91 => 31,  89 => 30,  86 => 29,  73 => 18,  70 => 12,  68 => 11,  65 => 10,  62 => 9,  58 => 8,  53 => 1,  51 => 6,  48 => 4,  46 => 3,  44 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCase/Case/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/CaseBundle/Resources/views/Case/view.html.twig");
    }
}
