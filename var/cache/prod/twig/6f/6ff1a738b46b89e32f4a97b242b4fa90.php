<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroContact/Contact/widget/info.html.twig */
class __TwigTemplate_91dbd22c4bd8b0c8028cbb4be5eb403e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["ui"] = $this->macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContact/Contact/widget/info.html.twig", 1)->unwrap();
        // line 2
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroContact/Contact/widget/info.html.twig", 2)->unwrap();
        // line 3
        $macros["U"] = $this->macros["U"] = $this->loadTemplate("@OroUser/macros.html.twig", "@OroContact/Contact/widget/info.html.twig", 3)->unwrap();
        // line 4
        $macros["contactInfo"] = $this->macros["contactInfo"] = $this;
        // line 5
        echo "
";
        // line 45
        echo "<div class=\"widget-content\">
    <div class=\"row-fluid form-horizontal contact-info\">
        <div class=\"responsive-block\">";
        // line 48
        ob_start(function () { return ''; });
        // line 49
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "skype", [], "any", false, false, false, 49)) {
            // line 50
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "skype", [], "any", false, false, false, 50), "html", null, true);
            echo " ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->getSkypeButton($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "skype", [], "any", false, false, false, 50));
        }
        $context["skypeData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 54
        ob_start(function () { return ''; });
        // line 55
        if ((((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "twitter", [], "any", false, false, false, 55) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "facebook", [], "any", false, false, false, 55)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "googlePlus", [], "any", false, false, false, 55)) || Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "linkedIn", [], "any", false, false, false, 55))) {
            // line 56
            echo "<ul class=\"social-list extra-list\">
                        ";
            // line 57
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "twitter", [], "any", false, false, false, 57)) {
                // line 58
                echo "                            <li>
                                <a class=\"no-hash\" href=\"";
                // line 59
                echo twig_call_macro($macros["contactInfo"], "macro_getSocialUrl", ["twitter", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "twitter", [], "any", false, false, false, 59)], 59, $context, $this->getSourceContext());
                echo "\" target=\"_blank\" title=\"Twitter\">
                                    <i class=\"fa-twitter\" aria-hidden=\"true\"></i>
                                    <span class=\"sr-only\">Twitter</span>
                                </a>
                            </li>
                        ";
            }
            // line 65
            echo "                        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "facebook", [], "any", false, false, false, 65)) {
                // line 66
                echo "                            <li>
                                <a class=\"no-hash\" href=\"";
                // line 67
                echo twig_call_macro($macros["contactInfo"], "macro_getSocialUrl", ["facebook", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "facebook", [], "any", false, false, false, 67)], 67, $context, $this->getSourceContext());
                echo "\" target=\"_blank\" title=\"Facebook\">
                                    <i class=\"fa-facebook\" aria-hidden=\"true\"></i>
                                    <span class=\"sr-only\">Facebook</span>
                                </a>
                            </li>
                        ";
            }
            // line 73
            echo "                        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "googlePlus", [], "any", false, false, false, 73)) {
                // line 74
                echo "                            <li>
                                <a class=\"no-hash\" href=\"";
                // line 75
                echo twig_call_macro($macros["contactInfo"], "macro_getSocialUrl", ["google_plus", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "googlePlus", [], "any", false, false, false, 75)], 75, $context, $this->getSourceContext());
                echo "\" target=\"_blank\" title=\"Google+\">
                                    <i class=\"fa-google-plus\" aria-hidden=\"true\"></i>
                                    <span class=\"sr-only\">Google+</span>
                                </a>
                            </li>
                        ";
            }
            // line 81
            echo "                        ";
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "linkedIn", [], "any", false, false, false, 81)) {
                // line 82
                echo "                            <li>
                                <a class=\"no-hash\" href=\"";
                // line 83
                echo twig_call_macro($macros["contactInfo"], "macro_getSocialUrl", ["linked_in", Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "linkedIn", [], "any", false, false, false, 83)], 83, $context, $this->getSourceContext());
                echo "\" target=\"_blank\" title=\"LinkedIn\">
                                    <i class=\"fa-linkedin\" aria-hidden=\"true\"></i>
                                    <span class=\"sr-only\">LinkedIn</span>
                                </a>
                            </li>
                        ";
            }
            // line 89
            echo "                    </ul>";
        }
        $context["socialData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 93
        echo twig_call_macro($macros["ui"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "description", [], "any", false, false, false, 93)], 93, $context, $this->getSourceContext());
        echo "
            ";
        // line 94
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.phones.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phones", [], "any", false, false, false, 94), "count", [], "any", false, false, false, 94)) ? (twig_call_macro($macros["contactInfo"], "macro_renderCollectionWithPrimaryElement", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "phones", [], "any", false, false, false, 94), false, ($context["entity"] ?? null)], 94, $context, $this->getSourceContext())) : (null))], 94, $context, $this->getSourceContext());
        echo "
            ";
        // line 95
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.emails.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emails", [], "any", false, false, false, 95), "count", [], "any", false, false, false, 95)) ? (twig_call_macro($macros["contactInfo"], "macro_renderCollectionWithPrimaryElement", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "emails", [], "any", false, false, false, 95), true, ($context["entity"] ?? null)], 95, $context, $this->getSourceContext())) : (null))], 95, $context, $this->getSourceContext());
        echo "
            ";
        // line 96
        $context["faxPhone"] = ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "fax", [], "any", false, false, false, 96)) ? (twig_call_macro($macros["ui"], "macro_renderPhone", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "fax", [], "any", false, false, false, 96)], 96, $context, $this->getSourceContext())) : (null));
        // line 97
        echo "            ";
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.fax.label"), ($context["faxPhone"] ?? null), null, null, ["dir" => ((($context["faxPhone"] ?? null)) ? ("ltr") : (null))]], 97, $context, $this->getSourceContext());
        echo "
            ";
        // line 98
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.skype.label"), ($context["skypeData"] ?? null)], 98, $context, $this->getSourceContext());
        echo "
            ";
        // line 99
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.method.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "method", [], "any", false, false, false, 99)], 99, $context, $this->getSourceContext());
        echo "
            ";
        // line 100
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.social.label"), ($context["socialData"] ?? null)], 100, $context, $this->getSourceContext());
        echo "

            ";
        // line 102
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null)], 102, $context, $this->getSourceContext());
        echo "
        </div>

        <div class=\"responsive-block\">";
        // line 106
        ob_start(function () { return ''; });
        // line 107
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "accounts", [], "any", false, false, false, 107), "count", [], "any", false, false, false, 107)) {
            // line 108
            $context["accountViewGranted"] = $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_account_view");
            // line 109
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "accounts", [], "any", false, false, false, 109));
            $context['loop'] = [
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            ];
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["account"]) {
                // line 110
                if (($context["accountViewGranted"] ?? null)) {
                    // line 111
                    echo "<a href=\"";
                    echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_account_view", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["account"], "id", [], "any", false, false, false, 111)]), "html", null, true);
                    echo "\">";
                    echo twig_call_macro($macros["ui"], "macro_renderEntityViewLabel", [$context["account"], "name", "oro.account.entity_label"], 111, $context, $this->getSourceContext());
                    echo "</a>";
                } else {
                    // line 113
                    echo twig_call_macro($macros["ui"], "macro_renderEntityViewLabel", [$context["account"], "name"], 113, $context, $this->getSourceContext());
                }
                // line 115
                if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 115)) {
                    echo ", ";
                }
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['account'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
        }
        $context["accountsData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 120
        ob_start(function () { return ''; });
        // line 121
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "assignedTo", [], "any", false, false, false, 121)) {
            // line 122
            echo twig_call_macro($macros["U"], "macro_render_user_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "assignedTo", [], "any", false, false, false, 122)], 122, $context, $this->getSourceContext());
            echo "
                    ";
            // line 123
            echo twig_call_macro($macros["U"], "macro_user_business_unit_name", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "assignedTo", [], "any", false, false, false, 123)], 123, $context, $this->getSourceContext());
        }
        $context["assignedToData"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 127
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.job_title.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "jobTitle", [], "any", false, false, false, 127)], 127, $context, $this->getSourceContext());
        echo "
            ";
        // line 128
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.accounts.label"), ($context["accountsData"] ?? null)], 128, $context, $this->getSourceContext());
        echo "
            ";
        // line 129
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.birthday.label"), twig_call_macro($macros["ui"], "macro_render_birthday", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "birthday", [], "any", false, false, false, 129)], 129, $context, $this->getSourceContext())], 129, $context, $this->getSourceContext());
        echo "
            ";
        // line 130
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.gender.label"), $this->extensions['Oro\Bundle\UserBundle\Twig\OroUserExtension']->getGenderLabel(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "gender", [], "any", false, false, false, 130))], 130, $context, $this->getSourceContext());
        echo "
            ";
        // line 131
        echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.source.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "source", [], "any", false, false, false, 131)], 131, $context, $this->getSourceContext());
        echo "
            ";
        // line 132
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.assigned_to.label"), ($context["assignedToData"] ?? null)], 132, $context, $this->getSourceContext());
        echo "
            ";
        // line 133
        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.reports_to.label"), twig_call_macro($macros["ui"], "macro_entityViewLink", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 135
($context["entity"] ?? null), "reportsTo", [], "any", false, false, false, 135), $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "reportsTo", [], "any", false, false, false, 135)), "oro_contact_view"], 135, $context, $this->getSourceContext())], 133, $context, $this->getSourceContext());
        // line 136
        echo "

            ";
        // line 138
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "groups", [], "any", false, false, false, 138), "count", [], "any", false, false, false, 138) || $this->extensions['Oro\Bundle\FeatureToggleBundle\Twig\FeatureExtension']->isResourceEnabled("Oro\\Bundle\\ContactBundle\\Entity\\Group", "entities"))) {
            // line 139
            echo "            ";
            echo twig_call_macro($macros["ui"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.contact.groups.label"), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "groups", [], "any", false, false, false, 139), "count", [], "any", false, false, false, 139)) ? (twig_join_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "groupLabels", [], "any", false, false, false, 139), ", ")) : (null))], 139, $context, $this->getSourceContext());
            echo "
            ";
        }
        // line 141
        echo "        </div>
    </div>
</div>
";
    }

    // line 6
    public function macro_renderCollectionWithPrimaryElement($__collection__ = null, $__isEmail__ = null, $__entity__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "collection" => $__collection__,
            "isEmail" => $__isEmail__,
            "entity" => $__entity__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 7
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroContact/Contact/widget/info.html.twig", 7)->unwrap();
            // line 8
            echo "    ";
            $macros["email"] = $this->loadTemplate("@OroEmail/macros.html.twig", "@OroContact/Contact/widget/info.html.twig", 8)->unwrap();
            // line 9
            echo "
    ";
            // line 10
            $context["primaryElement"] = null;
            // line 11
            echo "    ";
            $context["elements"] = [];
            // line 12
            echo "
    ";
            // line 13
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["collection"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 14
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["element"], "primary", [], "any", false, false, false, 14)) {
                    // line 15
                    echo "            ";
                    $context["primaryElement"] = $context["element"];
                    // line 16
                    echo "        ";
                } else {
                    // line 17
                    echo "            ";
                    $context["elements"] = twig_array_merge(($context["elements"] ?? null), [0 => $context["element"]]);
                    // line 18
                    echo "        ";
                }
                // line 19
                echo "    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 20
            echo "    ";
            if (($context["primaryElement"] ?? null)) {
                // line 21
                echo "        ";
                $context["elements"] = twig_array_merge([0 => ($context["primaryElement"] ?? null)], ($context["elements"] ?? null));
                // line 22
                echo "    ";
            }
            // line 23
            echo "
    <ul class=\"extra-list\">";
            // line 25
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["elements"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["element"]) {
                // line 26
                echo "            <li class=\"contact-collection-element";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["element"], "primary", [], "any", false, false, false, 26)) {
                    echo " primary";
                }
                echo "\">
                ";
                // line 27
                if (($context["isEmail"] ?? null)) {
                    // line 28
                    echo "                    ";
                    echo twig_call_macro($macros["email"], "macro_renderEmailWithActions", [$context["element"], ($context["entity"] ?? null)], 28, $context, $this->getSourceContext());
                    echo "
                ";
                } else {
                    // line 30
                    echo "                    ";
                    echo twig_call_macro($macros["ui"], "macro_renderPhoneWithActions", [$context["element"], ($context["entity"] ?? null)], 30, $context, $this->getSourceContext());
                    echo "
                ";
                }
                // line 32
                echo "            </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['element'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 34
            echo "</ul>
";

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 37
    public function macro_getSocialUrl($__type__ = null, $__value__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "type" => $__type__,
            "value" => $__value__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 38
            if (((twig_slice($this->env, ($context["value"] ?? null), 0, 5) == "http:") || (twig_slice($this->env, ($context["value"] ?? null), 0, 6) == "https:"))) {
                // line 39
                echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
            } else {
                // line 41
                echo twig_escape_filter($this->env, $this->extensions['Oro\Bundle\ContactBundle\Twig\ContactExtension']->getSocialUrl(($context["type"] ?? null), ($context["value"] ?? null)), "html", null, true);
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroContact/Contact/widget/info.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  411 => 41,  408 => 39,  406 => 38,  392 => 37,  382 => 34,  375 => 32,  369 => 30,  363 => 28,  361 => 27,  354 => 26,  350 => 25,  347 => 23,  344 => 22,  341 => 21,  338 => 20,  332 => 19,  329 => 18,  326 => 17,  323 => 16,  320 => 15,  317 => 14,  313 => 13,  310 => 12,  307 => 11,  305 => 10,  302 => 9,  299 => 8,  296 => 7,  281 => 6,  274 => 141,  268 => 139,  266 => 138,  262 => 136,  260 => 135,  259 => 133,  255 => 132,  251 => 131,  247 => 130,  243 => 129,  239 => 128,  235 => 127,  231 => 123,  227 => 122,  225 => 121,  223 => 120,  205 => 115,  202 => 113,  195 => 111,  193 => 110,  176 => 109,  174 => 108,  172 => 107,  170 => 106,  164 => 102,  159 => 100,  155 => 99,  151 => 98,  146 => 97,  144 => 96,  140 => 95,  136 => 94,  132 => 93,  128 => 89,  119 => 83,  116 => 82,  113 => 81,  104 => 75,  101 => 74,  98 => 73,  89 => 67,  86 => 66,  83 => 65,  74 => 59,  71 => 58,  69 => 57,  66 => 56,  64 => 55,  62 => 54,  56 => 50,  54 => 49,  52 => 48,  48 => 45,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroContact/Contact/widget/info.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactBundle/Resources/views/Contact/widget/info.html.twig");
    }
}
