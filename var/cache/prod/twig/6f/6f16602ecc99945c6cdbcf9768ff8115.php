<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/scroll-hints.scss */
class __TwigTemplate_a3517b391378690087977f9d7249c4ae extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$scroll-hint-size: 18px !default;
\$scroll-hint-top-bg: linear-gradient(
    to bottom,
    rgba(\$primary-inverse, .9) 25%,
    rgba(\$primary-inverse, 0) 100%
) !default;
\$scroll-bottom-top-bg: linear-gradient(
    360deg,
    rgba(\$primary-inverse, .9) 25%,
    rgba(\$primary-inverse, 0) 100%
) !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/scroll-hints.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/scroll-hints.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/scroll-hints.scss");
    }
}
