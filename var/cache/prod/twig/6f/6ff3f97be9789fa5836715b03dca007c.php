<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroOAuth2Server/Client/Datagrid/owners.html.twig */
class __TwigTemplate_e2878aa4eb1e30d0bb2f3bb4b8c9bf0d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "owner"], "method", false, false, false, 1)) {
            // line 2
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroOAuth2Server/Client/Datagrid/owners.html.twig", 2)->unwrap();
            // line 3
            $context["owner"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "owner"], "method", false, false, false, 3);
            // line 4
            $context["isFrontend"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["record"] ?? null), "getValue", [0 => "frontend"], "method", false, false, false, 4);
            // line 5
            $context["ownerRoute"] = ((($context["isFrontend"] ?? null)) ? ("oro_customer_customer_user_view") : ("oro_user_view"));
            // line 6
            echo twig_call_macro($macros["UI"], "macro_entityViewLink", [($context["owner"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["owner"] ?? null), "fullName", [], "any", false, false, false, 6), ($context["ownerRoute"] ?? null), "VIEW"], 6, $context, $this->getSourceContext());
        }
    }

    public function getTemplateName()
    {
        return "@OroOAuth2Server/Client/Datagrid/owners.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 6,  45 => 5,  43 => 4,  41 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroOAuth2Server/Client/Datagrid/owners.html.twig", "/websites/frogdata/crm-application/vendor/oro/oauth2-server/src/Oro/Bundle/OAuth2ServerBundle/Resources/views/Client/Datagrid/owners.html.twig");
    }
}
