<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/page/before-content-addition-view.js */
class __TwigTemplate_cb7d0cf4d6a0537db44de850fb95e51a extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(['./../base/page-region-view'
], function(PageRegionView) {
    'use strict';

    const PageBeforeContentAdditionView = PageRegionView.extend({
        pageItems: ['beforeContentAddition'],

        /**
         * @inheritdoc
         */
        constructor: function PageBeforeContentAdditionView(options) {
            PageBeforeContentAdditionView.__super__.constructor.call(this, options);
        },

        template: function(data) {
            return data.beforeContentAddition;
        },

        render: function() {
            PageBeforeContentAdditionView.__super__.render.call(this);

            if (this.data) {
                this.initLayout();
            }
            return this;
        }
    });

    return PageBeforeContentAdditionView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/page/before-content-addition-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/page/before-content-addition-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/page/before-content-addition-view.js");
    }
}
