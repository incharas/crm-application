<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/Default/navbar/top.html.twig */
class __TwigTemplate_b7c1b119032bdec562c651d315ffa938 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        // line 3
        $_trait_0 = $this->loadTemplate("@OroUI/Default/navbar/blocks.html.twig", "@OroUI/Default/navbar/top.html.twig", 3);
        if (!$_trait_0->isTraitable()) {
            throw new RuntimeError('Template "'."@OroUI/Default/navbar/blocks.html.twig".'" cannot be used as a trait.', 3, $this->source);
        }
        $_trait_0_blocks = $_trait_0->getBlocks();

        $this->traits = $_trait_0_blocks;

        $this->blocks = array_merge(
            $this->traits,
            [
            ]
        );
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (($this->extensions['Oro\Bundle\ViewSwitcherBundle\Twig\DemoMobileExtension']->isMobile() || ($this->extensions['Oro\Bundle\ConfigBundle\Twig\ConfigExtension']->getConfigValue("oro_ui.navbar_position") == "top"))) {
            // line 2
            $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUI/Default/navbar/top.html.twig", 2)->unwrap();
            // line 4
            echo "<nav id=\"main-menu\" class=\"main-menu-top\"
    ";
            // line 5
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["view" => "oroui/js/app/views/page/main-menu-view"]], 5, $context, $this->getSourceContext());
            echo ">
    ";
            // line 6
            $this->displayBlock("application_menu", $context, $blocks);
            echo "
</nav>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroUI/Default/navbar/top.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 6,  56 => 5,  53 => 4,  51 => 2,  49 => 1,  30 => 3,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/Default/navbar/top.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/Default/navbar/top.html.twig");
    }
}
