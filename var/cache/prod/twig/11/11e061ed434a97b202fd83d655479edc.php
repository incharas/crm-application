<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/desktop/variables/form-variables.scss */
class __TwigTemplate_a569797c5e56796b8d669bf09c65f14c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$form-item-collection-field-width: 261px !default;
\$form-item-collection-field-append-width: \$input-append-width !default;
\$form-item-collection-field-append-sortable-width: 234px !default;

\$form-item-collection-element-offset-bottom: 5px !default;
\$form-item-collection-element-width: 259px !default;

\$form-item-collection-selector-width: 262px !default;
\$form-item-collection-selector-offset-left: 0 !default;
\$form-item-collection-select2-width: 271px !default;

\$form-item-collection-removable-field-width: 280px !default;
\$form-item-collection-removable-append-field-width: 257px !default;
\$form-item-collection-removable-append-sortable-field-width: 226px !default;

\$form-item-collection-datepicker-field-min-width: 120px !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/desktop/variables/form-variables.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/desktop/variables/form-variables.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/desktop/variables/form-variables.scss");
    }
}
