<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/js/validation-message-handlers.js */
class __TwigTemplate_10cb59e87b2d3c5ead6283aab39a24e9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const ValidationMessageHandlers = [
        require('oroform/js/app/views/validation-message-handler/select2-validation-message-handler-view'),
        require('oroform/js/app/views/validation-message-handler/date-validation-message-handler-view'),
        require('oroform/js/app/views/validation-message-handler/time-validation-message-handler-view'),
        require('oroform/js/app/views/validation-message-handler/datetime-validation-message-handler-view'),
        require('oroform/js/app/views/validation-message-handler/input-validation-message-handler-view')
    ];

    return ValidationMessageHandlers;
});
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/js/validation-message-handlers.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/js/validation-message-handlers.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/js/validation-message-handlers.js");
    }
}
