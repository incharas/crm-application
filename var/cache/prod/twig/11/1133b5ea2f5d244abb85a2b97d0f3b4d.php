<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroEntityConfig/macros.html.twig */
class __TwigTemplate_a9813a8749dd28ca998af9a2026dccf8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "
";
        // line 39
        echo "
";
        // line 62
        echo "
";
        // line 98
        echo "
";
        // line 112
        echo "
";
    }

    // line 1
    public function macro_renderDynamicFields($__entity__ = null, $__entity_class__ = null, $__ignoredFields__ = [], ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "entity_class" => $__entity_class__,
            "ignoredFields" => $__ignoredFields__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 2
            echo "    ";
            $macros["entityConfigMacros"] = $this;
            // line 3
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/macros.html.twig", 3)->unwrap();
            // line 4
            echo "    ";
            $context["dynamicFields"] = $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\DynamicFieldsExtensionAttributeDecorator']->getFields(($context["entity"] ?? null), ($context["entity_class"] ?? null));
            // line 5
            echo "    ";
            if ((array_key_exists("dynamicFields", $context) && twig_length_filter($this->env, ($context["dynamicFields"] ?? null)))) {
                // line 6
                echo "        ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable(($context["dynamicFields"] ?? null));
                foreach ($context['_seq'] as $context["fieldName"] => $context["item"]) {
                    // line 7
                    echo "            ";
                    if (!twig_in_filter($context["fieldName"], ($context["ignoredFields"] ?? null))) {
                        // line 8
                        echo "                ";
                        echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 9
$context["item"], "label", [], "any", false, false, false, 9)), twig_call_macro($macros["entityConfigMacros"], "macro_formatDynamicFieldValue", [                        // line 11
($context["entity"] ?? null),                         // line 12
($context["entity_class"] ?? null),                         // line 13
$context["fieldName"],                         // line 14
$context["item"]], 10, $context, $this->getSourceContext())], 8, $context, $this->getSourceContext());
                        // line 16
                        echo "
            ";
                    }
                    // line 18
                    echo "        ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['fieldName'], $context['item'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 19
                echo "    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 22
    public function macro_renderDynamicField($__entity__ = null, $__field__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "field" => $__field__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 23
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/macros.html.twig", 23)->unwrap();
            // line 24
            echo "    ";
            $macros["entityConfigMacros"] = $this;
            // line 25
            echo "
    ";
            // line 26
            $context["dynamicField"] = $this->extensions['Oro\Bundle\EntityConfigBundle\Twig\DynamicFieldsExtensionAttributeDecorator']->getField(($context["entity"] ?? null), ($context["field"] ?? null));
            // line 27
            echo "    ";
            if (($context["dynamicField"] ?? null)) {
                // line 28
                echo "        ";
                echo twig_call_macro($macros["ui"], "macro_renderHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 29
($context["dynamicField"] ?? null), "label", [], "any", false, false, false, 29)), twig_call_macro($macros["entityConfigMacros"], "macro_formatDynamicFieldValue", [                // line 31
($context["entity"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 32
($context["field"] ?? null), "entity", [], "any", false, false, false, 32), "className", [], "any", false, false, false, 32), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 33
($context["field"] ?? null), "fieldName", [], "any", false, false, false, 33),                 // line 34
($context["dynamicField"] ?? null)], 30, $context, $this->getSourceContext())], 28, $context, $this->getSourceContext());
                // line 36
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 40
    public function macro_formatDynamicFieldValue($__entity__ = null, $__entity_class__ = null, $__field_name__ = null, $__item__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "entity_class" => $__entity_class__,
            "field_name" => $__field_name__,
            "item" => $__item__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 41
            echo "    ";
            $macros["entityConfigMacros"] = $this;
            // line 42
            echo "
    ";
            // line 43
            $context["hasLink"] = false;
            // line 44
            echo "    ";
            $context["fieldValue"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 44);
            // line 45
            echo "    ";
            if (twig_test_iterable(($context["fieldValue"] ?? null))) {
                // line 46
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "values", [], "any", true, true, false, 46)) {
                    // line 47
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["fieldValue"] ?? null), "values", [], "any", false, false, false, 47));
                    $context['_iterated'] = false;
                    $context['loop'] = [
                      'parent' => $context['_parent'],
                      'index0' => 0,
                      'index'  => 1,
                      'first'  => true,
                    ];
                    if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof \Countable)) {
                        $length = count($context['_seq']);
                        $context['loop']['revindex0'] = $length - 1;
                        $context['loop']['revindex'] = $length;
                        $context['loop']['length'] = $length;
                        $context['loop']['last'] = 1 === $length;
                    }
                    foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                        // line 48
                        echo twig_call_macro($macros["entityConfigMacros"], "macro_renderField", [$context["value"]], 48, $context, $this->getSourceContext());
                        // line 49
                        if ( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["loop"], "last", [], "any", false, false, false, 49)) {
                            // line 50
                            echo ",&nbsp;";
                        }
                        $context['_iterated'] = true;
                        ++$context['loop']['index0'];
                        ++$context['loop']['index'];
                        $context['loop']['first'] = false;
                        if (isset($context['loop']['length'])) {
                            --$context['loop']['revindex0'];
                            --$context['loop']['revindex'];
                            $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                        }
                    }
                    if (!$context['_iterated']) {
                        // line 53
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                } else {
                    // line 56
                    echo "            ";
                    echo twig_call_macro($macros["entityConfigMacros"], "macro_renderField", [($context["fieldValue"] ?? null)], 56, $context, $this->getSourceContext());
                    echo "
        ";
                }
                // line 58
                echo "    ";
            } else {
                // line 59
                echo "        ";
                echo twig_call_macro($macros["entityConfigMacros"], "macro_renderFormatted", [($context["entity"] ?? null), ($context["entity_class"] ?? null), ($context["field_name"] ?? null), ($context["item"] ?? null)], 59, $context, $this->getSourceContext());
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 63
    public function macro_renderFormatted($__entity__ = null, $__entity_class__ = null, $__field_name__ = null, $__item__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "entity" => $__entity__,
            "entity_class" => $__entity_class__,
            "field_name" => $__field_name__,
            "item" => $__item__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 64
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/macros.html.twig", 64)->unwrap();
            // line 65
            echo "    ";
            $context["value"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 65);
            // line 66
            echo "    ";
            $context["type"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "type", [], "any", false, false, false, 66);
            // line 67
            echo "
    ";
            // line 68
            if (((($context["type"] ?? null) == "text") && ($context["value"] ?? null))) {
                // line 69
                echo "        ";
                ob_start(function () { return ''; });
                // line 70
                echo "            ";
                echo twig_nl2br(twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true));
                echo "
        ";
                $context["value"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
                // line 72
                echo "    ";
            } elseif ((($context["type"] ?? null) == "html")) {
                // line 73
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_renderCollapsibleHtml", [_twig_default_filter($this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(                // line 74
($context["value"] ?? null)), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")),                 // line 75
($context["entity"] ?? null), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 76
($context["item"] ?? null), "label", [], "any", false, false, false, 76)], 73, $context, $this->getSourceContext());
                // line 77
                echo "
    ";
            } elseif (((            // line 78
($context["type"] ?? null) == "boolean") &&  !(null === ($context["value"] ?? null)))) {
                // line 79
                echo "        ";
                $context["value"] = ((($context["value"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Yes")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("No")));
                // line 80
                echo "    ";
            } elseif ((($context["type"] ?? null) == "money")) {
                // line 81
                echo "        ";
                $context["value"] = ((($context["value"] ?? null)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatCurrency(($context["value"] ?? null))) : (null));
                // line 82
                echo "    ";
            } elseif (((($context["type"] ?? null) == "decimal") || (($context["type"] ?? null) == "float"))) {
                // line 83
                echo "        ";
                $context["value"] = ((($context["value"] ?? null)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatDecimal(($context["value"] ?? null))) : (null));
                // line 84
                echo "    ";
            } elseif ((($context["type"] ?? null) == "percent")) {
                // line 85
                echo "        ";
                $context["value"] = ((($context["value"] ?? null)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\NumberExtension']->formatPercent(($context["value"] ?? null))) : (null));
                // line 86
                echo "    ";
            } elseif ((($context["type"] ?? null) == "date")) {
                // line 87
                echo "        ";
                $context["value"] = ((($context["value"] ?? null)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDate(($context["value"] ?? null))) : (null));
                // line 88
                echo "    ";
            } elseif ((($context["type"] ?? null) == "datetime")) {
                // line 89
                echo "        ";
                $context["value"] = ((($context["value"] ?? null)) ? ($this->extensions['Oro\Bundle\LocaleBundle\Twig\DateTimeOrganizationExtension']->formatDateTime(($context["value"] ?? null))) : (null));
                // line 90
                echo "    ";
            }
            // line 91
            echo "
    ";
            // line 92
            if ((($context["type"] ?? null) == "wysiwyg")) {
                // line 93
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_renderCollapsibleWysiwygContentPreview", [($context["value"] ?? null)], 93, $context, $this->getSourceContext());
                echo "
    ";
            } elseif ((            // line 94
($context["type"] ?? null) != "html")) {
                // line 95
                echo "        ";
                echo twig_escape_filter($this->env, ((array_key_exists("value", $context)) ? (_twig_default_filter(($context["value"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))), "html", null, true);
                echo "
    ";
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 99
    public function macro_renderField($__value__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "value" => $__value__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 100
            echo "    ";
            $macros["ui"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/macros.html.twig", 100)->unwrap();
            // line 101
            echo "
    ";
            // line 102
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "template", [], "any", true, true, false, 102)) {
                // line 103
                echo "        ";
                echo twig_include($this->env, $context, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "template", [], "any", false, false, false, 103), ["data" => ($context["value"] ?? null)]);
                echo "
    ";
            } elseif ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 104
($context["value"] ?? null), "link", [], "any", true, true, false, 104) && (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "link", [], "any", false, false, false, 104) != false))) {
                // line 105
                echo "        ";
                echo twig_call_macro($macros["ui"], "macro_renderUrl", [Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "link", [], "any", false, false, false, 105), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "title", [], "any", false, false, false, 105)], 105, $context, $this->getSourceContext());
                echo "
    ";
            } elseif (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 106
($context["value"] ?? null), "raw", [], "any", true, true, false, 106)) {
                // line 107
                echo "        ";
                echo Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "title", [], "any", false, false, false, 107);
                echo "
    ";
            } else {
                // line 109
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "title", [], "any", false, false, false, 109));
            }

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    // line 113
    public function macro_displayLayoutActions($__actions__ = null, ...$__varargs__)
    {
        $macros = $this->macros;
        $context = $this->env->mergeGlobals([
            "actions" => $__actions__,
            "varargs" => $__varargs__,
        ]);

        $blocks = [];

        ob_start(function () { return ''; });
        try {
            // line 114
            echo "    ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroEntityConfig/macros.html.twig", 114)->unwrap();
            // line 115
            echo "    ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["actions"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["button"]) {
                // line 116
                echo "        ";
                $context["componentData"] = [];
                // line 117
                echo "        ";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "page_component_module", [], "any", true, true, false, 117)) {
                    // line 118
                    echo "            ";
                    $context["componentData"] = ["page-component-module" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "page_component_module", [], "any", false, false, false, 118)];
                    // line 119
                    echo "
            ";
                    // line 120
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "page_component_options", [], "any", true, true, false, 120)) {
                        // line 121
                        echo "                ";
                        $context["componentData"] = twig_array_merge(($context["componentData"] ?? null), ["page-component-options" => json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 122
$context["button"], "page_component_options", [], "any", false, false, false, 122))]);
                        // line 125
                        echo "            ";
                    }
                    // line 126
                    echo "
        ";
                }
                // line 128
                echo "        ";
                echo twig_call_macro($macros["UI"], "macro_button", [["path" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 129
$context["button"], "void", [], "any", true, true, false, 129)) ? ("#") : ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "route", [], "any", false, false, false, 129), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "args", [], "any", true, true, false, 129)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "args", [], "any", false, false, false, 129), [])) : ([]))))), "data" => twig_array_merge(["url" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 131
$context["button"], "void", [], "any", true, true, false, 131)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "route", [], "any", false, false, false, 131), ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "args", [], "any", true, true, false, 131)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "args", [], "any", false, false, false, 131), [])) : ([])))) : (null))],                 // line 132
($context["componentData"] ?? null)), "iCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 133
$context["button"], "iCss", [], "any", true, true, false, 133)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "iCss", [], "any", false, false, false, 133), "")) : ("")), "aCss" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 134
$context["button"], "aCss", [], "any", true, true, false, 134)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "aCss", [], "any", false, false, false, 134), "")) : ("")), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 135
$context["button"], "title", [], "any", true, true, false, 135)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "title", [], "any", false, false, false, 135), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "name", [], "any", false, false, false, 135))) : (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["button"], "name", [], "any", false, false, false, 135)))), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                 // line 136
$context["button"], "name", [], "any", false, false, false, 136))]], 128, $context, $this->getSourceContext());
                // line 137
                echo "
    ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['button'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;

            return ('' === $tmp = ob_get_contents()) ? '' : new Markup($tmp, $this->env->getCharset());
        } finally {
            ob_end_clean();
        }
    }

    public function getTemplateName()
    {
        return "@OroEntityConfig/macros.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  483 => 137,  481 => 136,  480 => 135,  479 => 134,  478 => 133,  477 => 132,  476 => 131,  475 => 129,  473 => 128,  469 => 126,  466 => 125,  464 => 122,  462 => 121,  460 => 120,  457 => 119,  454 => 118,  451 => 117,  448 => 116,  443 => 115,  440 => 114,  427 => 113,  417 => 109,  411 => 107,  409 => 106,  404 => 105,  402 => 104,  397 => 103,  395 => 102,  392 => 101,  389 => 100,  376 => 99,  363 => 95,  361 => 94,  356 => 93,  354 => 92,  351 => 91,  348 => 90,  345 => 89,  342 => 88,  339 => 87,  336 => 86,  333 => 85,  330 => 84,  327 => 83,  324 => 82,  321 => 81,  318 => 80,  315 => 79,  313 => 78,  310 => 77,  308 => 76,  307 => 75,  306 => 74,  304 => 73,  301 => 72,  295 => 70,  292 => 69,  290 => 68,  287 => 67,  284 => 66,  281 => 65,  278 => 64,  262 => 63,  249 => 59,  246 => 58,  240 => 56,  233 => 53,  219 => 50,  217 => 49,  215 => 48,  197 => 47,  194 => 46,  191 => 45,  188 => 44,  186 => 43,  183 => 42,  180 => 41,  164 => 40,  153 => 36,  151 => 34,  150 => 33,  149 => 32,  148 => 31,  147 => 29,  145 => 28,  142 => 27,  140 => 26,  137 => 25,  134 => 24,  131 => 23,  117 => 22,  107 => 19,  101 => 18,  97 => 16,  95 => 14,  94 => 13,  93 => 12,  92 => 11,  91 => 9,  89 => 8,  86 => 7,  81 => 6,  78 => 5,  75 => 4,  72 => 3,  69 => 2,  54 => 1,  49 => 112,  46 => 98,  43 => 62,  40 => 39,  37 => 21,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroEntityConfig/macros.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/macros.html.twig");
    }
}
