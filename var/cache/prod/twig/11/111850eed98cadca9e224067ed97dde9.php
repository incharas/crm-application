<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/menuUpdate/index.html.twig */
class __TwigTemplate_28f27e8d1c26375a30983f54478c462e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_datagrid' => [$this, 'block_content_datagrid'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return $this->loadTemplate(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["bap"] ?? null), "layout", [], "any", false, false, false, 1), "@OroNavigation/menuUpdate/index.html.twig", 1);
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/index.html.twig", 3)->unwrap();
        // line 4
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroNavigation/menuUpdate/index.html.twig", 4)->unwrap();
        // line 6
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.navigation.menuupdate.menus");
        // line 8
        if (array_key_exists("entityClass", $context)) {
            // line 9
            $context["buttonsPlaceholderData"] = ["entity_class" => ($context["entityClass"] ?? null)];
        }
        // line 1
        $this->getParent($context)->display($context, array_merge($this->blocks, $blocks));
    }

    // line 12
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 13
        echo "    <div class=\"container-fluid page-title\">
        <div class=\"navigation navbar-extra navbar-extra-right\">
            <div class=\"row\">
                <div class=\"pull-left pull-left-extra\">
                    ";
        // line 17
        echo twig_include($this->env, $context, "@OroNavigation/menuUpdate/pageHeader.html.twig");
        echo "
                </div>
                ";
        // line 19
        if (array_key_exists("entityClass", $context)) {
            // line 20
            echo "                    <div class=\"pull-right title-buttons-container invisible\"
                         data-page-component-module=\"oroui/js/app/components/view-component\"
                         data-page-component-options=\"";
            // line 22
            echo twig_escape_filter($this->env, json_encode(["view" => "oroui/js/app/views/hidden-initialization-view"]), "html", null, true);
            echo "\"
                         data-layout=\"separate\">
                        ";
            // line 24
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("index_navButtons_before", $context)) ? (_twig_default_filter(($context["index_navButtons_before"] ?? null), "index_navButtons_before")) : ("index_navButtons_before")), ($context["buttonsPlaceholderData"] ?? null));
            // line 25
            echo "                        ";
            $this->displayBlock('navButtons', $context, $blocks);
            // line 26
            echo "                        ";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("index_navButtons_after", $context)) ? (_twig_default_filter(($context["index_navButtons_after"] ?? null), "index_navButtons_after")) : ("index_navButtons_after")), ($context["buttonsPlaceholderData"] ?? null));
            // line 27
            echo "                    </div>
                ";
        }
        // line 29
        echo "            </div>
        </div>
    </div>
    ";
        // line 32
        $this->displayBlock('content_datagrid', $context, $blocks);
    }

    // line 25
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
    }

    // line 32
    public function block_content_datagrid($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 33
        echo "        ";
        if (array_key_exists("gridName", $context)) {
            // line 34
            echo "            ";
            if (array_key_exists("gridScope", $context)) {
                // line 35
                echo "                ";
                $context["gridName"] = $this->extensions['Oro\Bundle\DataGridBundle\Twig\DataGridExtension']->buildGridFullName(($context["gridName"] ?? null), ($context["gridScope"] ?? null));
                // line 36
                echo "            ";
            }
            // line 37
            echo "            ";
            $context["renderParams"] = twig_array_merge(["enableFullScreenLayout" => true, "enableViews" => true, "showViewsInNavbar" => true], ((            // line 42
array_key_exists("renderParams", $context)) ? (_twig_default_filter(($context["renderParams"] ?? null), [])) : ([])));
            // line 43
            echo "            ";
            echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", [($context["gridName"] ?? null), ((array_key_exists("params", $context)) ? (_twig_default_filter(($context["params"] ?? null), [])) : ([])), ($context["renderParams"] ?? null)], 43, $context, $this->getSourceContext());
            echo "

            ";
            // line 46
            echo "            ";
            $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroNavigation/menuUpdate/index.html.twig", 46)->unwrap();
            // line 47
            echo "
            <div ";
            // line 48
            echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orodatagrid/js/app/components/datagrid-allow-tracking-component", "options" => ["gridName" =>             // line 51
($context["gridName"] ?? null)]]], 48, $context, $this->getSourceContext());
            // line 53
            echo "></div>
        ";
        }
        // line 55
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/menuUpdate/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 55,  147 => 53,  145 => 51,  144 => 48,  141 => 47,  138 => 46,  132 => 43,  130 => 42,  128 => 37,  125 => 36,  122 => 35,  119 => 34,  116 => 33,  112 => 32,  106 => 25,  102 => 32,  97 => 29,  93 => 27,  90 => 26,  87 => 25,  85 => 24,  80 => 22,  76 => 20,  74 => 19,  69 => 17,  63 => 13,  59 => 12,  55 => 1,  52 => 9,  50 => 8,  48 => 6,  46 => 4,  44 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/menuUpdate/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/menuUpdate/index.html.twig");
    }
}
