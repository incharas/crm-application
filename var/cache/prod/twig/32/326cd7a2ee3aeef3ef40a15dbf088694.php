<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/datepicker/datetimepicker-view.js */
class __TwigTemplate_c8bb0037233c147f55b5a833973a0071 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const \$ = require('jquery');
    const DatePickerView = require('./datepicker-view');
    const dateTimePickerViewMixin = require('./datetimepicker-view-mixin');

    const DateTimePickerView = DatePickerView.extend(Object.assign({}, dateTimePickerViewMixin, {
        /**
         * Default options
         */
        defaults: \$.extend(true, {}, DatePickerView.prototype.defaults, dateTimePickerViewMixin.defaults),

        /**
         * @inheritdoc
         */
        constructor: function DateTimePickerView(options) {
            DateTimePickerView.__super__.constructor.call(this, options);
        },

        /**
         * Returns supper prototype for datetime picker view mixin
         *
         * @returns {Object}
         * @final
         * @protected
         */
        _super: function() {
            return DateTimePickerView.__super__;
        }
    }));

    return DateTimePickerView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/datepicker/datetimepicker-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/datepicker/datetimepicker-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/datepicker/datetimepicker-view.js");
    }
}
