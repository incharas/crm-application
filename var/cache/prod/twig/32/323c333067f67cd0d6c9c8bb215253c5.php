<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Autocomplete/customer/result.html.twig */
class __TwigTemplate_9e5e6e6a35e568de5fc61fb1d1640a44 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<% if (!_.isEmpty(obj.icon)) { %>
    <% if (icon.type === 'file-path') { %>
        <img src=\"<%- icon.data.path %>\" class=\"separated-img\">
    <% } else if (icon.type === 'icon') { %>
        <i class=\"<%- icon.data.class %> hide-text separated-img\"></i>
    <% } %>
<% } %>

<%= highlight(_.escape(text)) %>
<% if (typeof(label) !== \"undefined\") { %>
<span class=\"type\">(<%- label %>)</span>
<% } %>

<% if (typeof(matchValue) !== \"undefined\") { %>
    <div class=\"match-value\"><%= highlight(_.escape(matchValue)) %></div>
<% } %>

<% if (id === null) { %>
    <span class=\"select2__result-entry-info\">
        (<%= _.__('oro.sales.form.add_new_customer', {'account': _.escape(context.account)}) %>)
    </span>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroSales/Autocomplete/customer/result.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Autocomplete/customer/result.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Autocomplete/customer/result.html.twig");
    }
}
