<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/label.scss */
class __TwigTemplate_d0c148320322e20103a693753e4b9ba8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

@use 'sass:map';

\$label-theme-success: (
    'background': \$success-ultra-light,
    'color': \$success-darken
) !default;

\$label-theme-info: (
    'background': \$info,
    'color': \$primary-inverse
) !default;

\$label-theme-warning: (
    'background': \$warning-light,
    'color': \$warning-darken
) !default;

\$label-theme-keys: () !default;
\$label-theme-keys: map.merge(
    (
        'success': \$label-theme-success,
        'info': \$label-theme-info,
        'warning': \$label-theme-warning
    ),
    \$label-theme-keys
);
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/label.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/label.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/label.scss");
    }
}
