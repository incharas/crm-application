<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroFormBundlePublic/default/scss/components/floating-validation-message.scss */
class __TwigTemplate_2a3050ba20f38a1c43aca2acb5274717 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: default; */

@use 'sass:selector';

// More about placeholders https://github.com/sass/sass/issues/2808#issuecomment-574413393
%base-floating-validation-message,
.floating-validation-message {
    display: block;
    position: absolute;
    top: \$floating-validation-message-top;
    bottom: \$floating-validation-message-bottom;
    z-index: 2;
    width: auto;
    height: 0;
    margin: 0;
    white-space: nowrap;
    background-color: transparent;

    &::after {
        content: '';
        display: block;
        position: absolute;
        top: \$floating-validation-message-after-top;
        left: \$floating-validation-message-after-left;
        margin-top: -6px;
        border-style: solid;
        border-color: \$floating-validation-message-after-color;
        border-width: 6px 4px 0;
    }

    > span {
        position: absolute;
        top: \$floating-validation-message-label-top;
        left: \$floating-validation-message-label-left;
        text-align: \$floating-validation-message-label-text-align;
        background-color: \$floating-validation-message-label-background-color;
        color: \$floating-validation-message-label-color;
        padding: \$floating-validation-message-label-inner-offset;
        border-radius: \$floating-validation-message-label-border-radius;
    }

    @at-root #{selector.append(&, '__icon')} {
        display: \$floating-validation-message-icon-display;
    }
}
";
    }

    public function getTemplateName()
    {
        return "@OroFormBundlePublic/default/scss/components/floating-validation-message.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroFormBundlePublic/default/scss/components/floating-validation-message.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/public/default/scss/components/floating-validation-message.scss");
    }
}
