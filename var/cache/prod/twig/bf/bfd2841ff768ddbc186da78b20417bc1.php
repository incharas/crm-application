<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/layout-subtree-manager.js */
class __TwigTemplate_09a121bb457a5bfc95c3971c0892bb26 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define([
    'jquery',
    'underscore',
    'oroui/js/mediator'
], function(\$, _, mediator) {
    'use strict';

    /**
     * @export oroui/js/layout-subtree-manager
     * @name   oro.layoutSubtreeManager
     */
    const layoutSubtreeManager = {
        url: window.location.href,

        method: 'get',

        viewsCollection: {},

        reloadEvents: {},

        /**
         * Add layout subtree instance to registry.
         *
         * @param {Object} view LayoutSubtreeView instance
         */
        addView: function(view) {
            const blockId = view.options.blockId;

            this.viewsCollection[blockId] = view;

            if (view.options.reloadEvents instanceof Array) {
                view.options.reloadEvents.map((function(eventItem) {
                    if (!this.reloadEvents[eventItem]) {
                        this.reloadEvents[eventItem] = [];
                        mediator.on(eventItem, this._reloadLayouts.bind(this, eventItem), this);
                    }

                    if (!this.reloadEvents[eventItem].includes(blockId)) {
                        this.reloadEvents[eventItem].push(blockId);
                    }
                }).bind(this));
            }
        },

        /**
         * Remove layout subtree instance from registry.
         *
         * @param {Object} view LayoutSubtreeView instance
         */
        removeView: function(view) {
            const blockId = view.options.blockId;

            delete this.viewsCollection[blockId];

            Object.keys(this.reloadEvents).map((function(eventName) {
                const eventBlockIds = this.reloadEvents[eventName];
                const index = eventBlockIds.indexOf(blockId);
                if (index > -1) {
                    eventBlockIds.splice(index, 1);
                }
                if (!eventBlockIds.length) {
                    delete this.reloadEvents[eventName];
                    mediator.off(eventName, null, this);
                }
            }).bind(this));
        },

        /**
         * Call view methods from collection.
         *
         * @param {Array} blockIds
         * @param {String} methodName
         * @param {Function|Array} [methodArguments]
         */
        _callViewMethod: function(blockIds, methodName, methodArguments) {
            blockIds.map((function(blockId) {
                const view = this.viewsCollection[blockId];
                if (!view) {
                    return;
                }

                let viewArguments = methodArguments || [];
                if (typeof viewArguments === 'function') {
                    viewArguments = viewArguments(blockId);
                }
                view[methodName](...viewArguments);
            }).bind(this));
        },

        /**
         * Send ajax request to server with query(ies) for update elements depends from event.
         *
         * @param {string} event name of fired event
         * @param {Object} options
         */
        _reloadLayouts: function(event, options) {
            const self = this;
            const eventBlockIds = this.reloadEvents[event] || [];
            if (!(eventBlockIds instanceof Array) || !eventBlockIds.length) {
                return;
            }

            options = options || {
                layoutSubtreeUrl: null,
                layoutSubtreeCallback: null,
                layoutSubtreeFailCallback: null
            };

            this._callViewMethod(eventBlockIds, 'beforeContentLoading');
            \$.ajax({
                url: options.layoutSubtreeUrl || this.url,
                type: this.method,
                data: {
                    layout_block_ids: eventBlockIds
                }
            })
                .done(function(content) {
                    self._callViewMethod(eventBlockIds, 'setContent', function(blockId) {
                        return [content[blockId]];
                    });
                    self._callViewMethod(eventBlockIds, 'afterContentLoading');
                    if (options.layoutSubtreeCallback) {
                        options.layoutSubtreeCallback();
                    }
                })
                .fail(function(jqxhr) {
                    self._callViewMethod(eventBlockIds, 'contentLoadingFail');
                    if (options.layoutSubtreeFailCallback) {
                        options.layoutSubtreeFailCallback(jqxhr);
                    }
                });
        },

        /**
         * Send ajax request to server and update block for layout by ID.
         *
         * @param {String} blockId
         * @param {object} data
         * @param {Function} callback
         */
        get: function(blockId, data, callback) {
            data = data || {};
            data.layout_block_ids = [blockId];
            \$.ajax({
                url: document.location.pathname,
                type: this.method,
                data: data || {}
            }).done(function(content) {
                if (_.isFunction(callback)) {
                    callback(content[blockId] || '');
                }
            }).fail(function(jqxhr) {
                if (_.isFunction(callback)) {
                    callback('');
                }
            });
        }
    };

    return layoutSubtreeManager;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/layout-subtree-manager.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/layout-subtree-manager.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/layout-subtree-manager.js");
    }
}
