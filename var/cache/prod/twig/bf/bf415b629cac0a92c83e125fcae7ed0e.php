<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTask/Task/assignTaskButton.html.twig */
class __TwigTemplate_7d1ebd753b693145d95d894dbb2a259f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroTask/Task/assignTaskButton.html.twig", 1)->unwrap();
        // line 2
        echo "
";
        // line 3
        echo twig_call_macro($macros["UI"], "macro_clientButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_task_create", $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getActionParams(        // line 4
($context["entity"] ?? null), twig_constant("Oro\\Bundle\\TaskBundle\\EventListener\\FormSetOwnerEventListener::ACTION_ASSIGN"))), "aCss" => "no-hash", "iCss" => "fa-tasks", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 7
($context["entity"] ?? null), "id", [], "any", false, false, false, 7), "label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.assign_entity"), "title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.assign_entity.title"), "widget" => ["type" => "dialog", "multiple" => true, "reload-grid-name" => "user-tasks-grid", "refresh-widget-alias" => "activity-list-widget", "options" => ["alias" => "assign-task-dialog", "dialogOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.task.assign_entity.widget_title", ["%username%" => $this->extensions['Oro\Bundle\EntityBundle\Twig\EntityExtension']->getEntityName(        // line 18
($context["entity"] ?? null))]), "allowMaximize" => true, "allowMinimize" => true, "dblclick" => "maximize", "maximizedHeightDecreaseBy" => "minimize-bar", "width" => 1000, "minWidth" => "expanded"]]]]], 3, $context, $this->getSourceContext());
        // line 28
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTask/Task/assignTaskButton.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  47 => 28,  45 => 18,  44 => 7,  43 => 4,  42 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTask/Task/assignTaskButton.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm-task-bundle/Resources/views/Task/assignTaskButton.html.twig");
    }
}
