<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/css/scss/oro/settings/progressbar.scss */
class __TwigTemplate_95c91c828b148c1b96992f3130aa3f4f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "/* @theme: admin.oro; */

\$progress-container-size: 130px !default;

\$progressbar-infinite-height: 6px !default;
\$progressbar-infinite-offset: 6px !default;
\$progressbar-infinite-background-color: #f8b800 !default;

\$progressbar-infinite-line-point-size: 48px !default;
\$progressbar-infinite-line-background-color: \$primary-860 !default;
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/css/scss/oro/settings/progressbar.scss";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/css/scss/oro/settings/progressbar.scss", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/css/scss/oro/settings/progressbar.scss");
    }
}
