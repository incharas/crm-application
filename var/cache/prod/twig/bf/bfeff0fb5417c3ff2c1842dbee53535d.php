<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCampaign/Chart/multiline.html.twig */
class __TwigTemplate_b12f165195fbce87286060a780d0dd57 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        if ((twig_length_filter($this->env, ($context["data"] ?? null)) > 0)) {
            // line 21
            echo "    ";
            $context["containerId"] = ("chart-container-" . twig_random($this->env));
            // line 22
            echo "    <div class=\"chart-container\">
        <div class=\"clearfix\">
            <div id=\"";
            // line 24
            echo twig_escape_filter($this->env, ($context["containerId"] ?? null), "html", null, true);
            echo "-chart\" class=\"multiline-chart chart pull-left\"></div>
        </div>
    </div>
    <script type=\"text/javascript\">
        loadModules(['jquery', 'oroui/js/layout', 'flotr2', 'orochart/js/data_formatter', 'oroui/js/mediator'],
            function (\$, layout, Flotr, dataFormatter, mediator) {
                \$(function () {
                    var \$chart = \$('#";
            // line 31
            echo twig_escape_filter($this->env, ($context["containerId"] ?? null), "html", null, true);
            echo "-chart');
                    var \$widgetContent = \$chart.parents('.chart-container').parent();
                    var setChartSize = function () {
                        var chartWidth = Math.round(\$widgetContent.width() * 0.75);
                        if (chartWidth != \$chart.width()) {
                            \$chart.width(chartWidth);
                            \$chart.height(Math.min(Math.round(chartWidth * 0.4), 450));
                            return true;
                        }
                        return false;
                    };
                    var setChartContainerSize = function () {
                        \$chart.closest('.clearfix').width(\$chart.width());
                    };
                    var drawChart = function () {
                        var xFormat = ";
            // line 46
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, false, false, 46), "label", [], "any", false, false, false, 46), "type", [], "any", false, false, false, 46));
            echo ";
                        var yFormat = ";
            // line 47
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, false, false, 47), "value", [], "any", false, false, false, 47), "type", [], "any", false, false, false, 47));
            echo ";
                        if (!\$chart.get(0).clientWidth) {
                            return;
                        }

                        var rawData = ";
            // line 52
            echo json_encode(($context["data"] ?? null));
            echo ";
                        var length = _.size(rawData[_.first(_.keys(rawData))]);

                        if (dataFormatter.isValueNumerical(xFormat)) {
                            var sort = function (rawData) {
                                rawData.sort(function (first, second) {
                                    if (first.label == null) {
                                        return -1;
                                    }
                                    if (second.label == null) {
                                        return 1;
                                    }
                                    var firstLabel = dataFormatter.parseValue(first.label, xFormat);
                                    var secondLabel = dataFormatter.parseValue(second.label, xFormat);
                                    return firstLabel - secondLabel;
                                });
                            };

                            _.each(rawData, sort);
                        }

                        var getXLabel = function (data) {
                            var label = dataFormatter.formatValue(data, xFormat);
                            if (label === null) {
                                var number = parseInt(data);
                                if (rawData.length > number) {
                                    label = rawData[number]['label'] === null ? '";
            // line 78
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "' : rawData[number]['label'];
                                } else {
                                    label = '';
                                }
                            }
                            return label;
                        };

                        var getYLabel = function (data) {
                            var label = dataFormatter.formatValue(data, yFormat);
                            if (label === null) {
                                var number = parseInt(data);
                                if (rawData.length > number) {
                                    label = rawData[data]['value'] === null ? '";
            // line 91
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"), "html", null, true);
            echo "' : rawData[data]['value'];
                                } else {
                                    label = '';
                                }
                            }
                            return label;
                        };

                        var connectDots = ";
            // line 99
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "settings", [], "any", false, false, false, 99), "connect_dots_with_line", [], "any", false, false, false, 99));
            echo ";
                        var colors = ";
            // line 100
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["config"] ?? null), "default_settings", [], "any", false, false, false, 100), "chartColors", [], "any", false, false, false, 100));
            echo ";

                        var makeChart = function (rawData, count, key) {
                            var chartData = [];
                            var prevYValue = 0;

                            for (var i in rawData) {
                                var yValue = dataFormatter.parseValue(rawData[i]['value'], yFormat);
                                yValue = yValue === null ? parseInt(i) : yValue;
                                yValue = yValue + prevYValue;
                                var xValue = dataFormatter.parseValue(rawData[i]['label'], xFormat);
                                xValue = xValue === null ? parseInt(i) : xValue;

                                var item = [xValue, yValue];
                                chartData.push(item);
                                prevYValue = yValue;
                            }

                            return {
                                label: key,
                                data: chartData,
                                color: colors[Math.ceil(colors.length/count)],
                                markers: { show: false },
                                points: { show: true }
                            };
                        };

                        var charts = [];
                        var count = 0;

                        _.each(rawData, function (rawData, key) {
                            var result = makeChart(rawData, count, key);
                            count++;

                            charts.push(result);
                        });

                        Flotr.draw(
                                \$chart.get(0),
                                charts,
                                {
                                    colors: colors,
                                    fontColor: ";
            // line 142
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "settings", [], "any", false, false, false, 142), "chartFontColor", [], "any", false, false, false, 142));
            echo ",
                                    fontSize: ";
            // line 143
            echo json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "settings", [], "any", false, false, false, 143), "chartFontSize", [], "any", false, false, false, 143));
            echo ",
                                    lines: {
                                        show: connectDots
                                    },
                                    mouse: {
                                        track: true,
                                        relative: true,
                                        trackFormatter: function (pointData) {
                                            return ";
            // line 151
            echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, false, false, 151), "label", [], "any", false, false, false, 151), "label", [], "any", false, false, false, 151)));
            echo "
                                                + ': ' + getXLabel(pointData.x)
                                                + ';</br>' + ";
            // line 153
            echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, false, false, 153), "value", [], "any", false, false, false, 153), "label", [], "any", false, false, false, 153)));
            echo "
                                                + ': ' + getYLabel(pointData.y);
                                        }
                                    },
                                    yaxis: {
                                        autoscale: true,
                                        autoscaleMargin: 1,
                                        tickFormatter: function (y) {
                                            return getYLabel(y);
                                        },
                                        title: ";
            // line 163
            echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, false, false, 163), "value", [], "any", false, false, false, 163), "label", [], "any", false, false, false, 163)));
            echo "
                                    },
                                    xaxis: {
                                        autoscale: true,
                                        autoscaleMargin: 0,
                                        labelsAngle: 45,
                                        mode: 'time',
                                        noTicks: length * 2,
                                        tickFormatter: function (x) {
                                            return getXLabel(x);
                                        },
                                        title: ";
            // line 174
            echo json_encode($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["options"] ?? null), "data_schema", [], "any", false, false, false, 174), "label", [], "any", false, false, false, 174), "label", [], "any", false, false, false, 174)));
            echo "
                                    },
                                    HtmlText: false,
                                    grid: {
                                        verticalLines: false,
                                        labelMargin: 10
                                    },
                                    legend: {
                                        show: true,
                                        noColumns: 1,
                                        position: 'nw'
                                    }
                                }
                        );
                    };

                    mediator.on('page:afterChange', function(){
                        setChartSize();
                        drawChart();
                        setChartContainerSize();
                    });

                    \$(window).resize(function () {
                        if (setChartSize()) {
                            drawChart();
                            setChartContainerSize();
                        }
                    });
                });
            });
    </script>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroCampaign/Chart/multiline.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  238 => 174,  224 => 163,  211 => 153,  206 => 151,  195 => 143,  191 => 142,  146 => 100,  142 => 99,  131 => 91,  115 => 78,  86 => 52,  78 => 47,  74 => 46,  56 => 31,  46 => 24,  42 => 22,  39 => 21,  37 => 20,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCampaign/Chart/multiline.html.twig", "/websites/frogdata/crm-application/vendor/oro/marketing/src/Oro/Bundle/CampaignBundle/Resources/views/Chart/multiline.html.twig");
    }
}
