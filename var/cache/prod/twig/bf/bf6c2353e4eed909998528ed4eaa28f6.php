<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroCalendar/SystemCalendar/update.html.twig */
class __TwigTemplate_1c4693ecb7200ab4e2d57d87133d102c extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'pageHeader' => [$this, 'block_pageHeader'],
            'content_data' => [$this, 'block_content_data'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/update.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%entity.title%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 3
($context["entity"] ?? null), "name", [], "any", false, false, false, 3), "%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.entity_label")]]);
        // line 4
        $context["entityId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 4);
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/update.html.twig", "@OroCalendar/SystemCalendar/update.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 6
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 7
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroCalendar/SystemCalendar/update.html.twig", 7)->unwrap();
        // line 8
        echo "
    ";
        // line 9
        $context["html"] = twig_call_macro($macros["UI"], "macro_saveAndCloseButton", [["route" => "oro_system_calendar_view", "params" => ["id" => "\$id"]]], 9, $context, $this->getSourceContext());
        // line 13
        echo "    ";
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_calendar_create")) {
            // line 14
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndNewButton", [["route" => "oro_system_calendar_create"]], 14, $context, $this->getSourceContext()));
            // line 17
            echo "    ";
        }
        // line 18
        echo "    ";
        if (($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_public_calendar_management") || $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_system_calendar_management"))) {
            // line 19
            echo "        ";
            $context["html"] = (($context["html"] ?? null) . twig_call_macro($macros["UI"], "macro_saveAndStayButton", [["route" => "oro_system_calendar_update", "params" => ["id" => "\$id"]]], 19, $context, $this->getSourceContext()));
            // line 23
            echo "    ";
        }
        // line 24
        echo "    ";
        echo twig_call_macro($macros["UI"], "macro_dropdownSaveButton", [["html" => ($context["html"] ?? null)]], 24, $context, $this->getSourceContext());
        echo "
    ";
        // line 25
        echo twig_call_macro($macros["UI"], "macro_cancelButton", [$this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_index")], 25, $context, $this->getSourceContext());
        echo "
";
    }

    // line 28
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    ";
        if (($context["entityId"] ?? null)) {
            // line 30
            echo "        ";
            $context["breadcrumbs"] = ["entity" =>             // line 31
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_system_calendar_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.entity_plural_label"), "entityTitle" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 34
($context["entity"] ?? null), "name", [], "any", false, false, false, 34)];
            // line 36
            echo "        ";
            $this->displayParentBlock("pageHeader", $context, $blocks);
            echo "
    ";
        } else {
            // line 38
            echo "        ";
            $context["title"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.calendar.systemcalendar.entity_label")]);
            // line 39
            echo "        ";
            $this->loadTemplate("@OroUI/page_title_block.html.twig", "@OroCalendar/SystemCalendar/update.html.twig", 39)->display(twig_array_merge($context, ["title" => ($context["title"] ?? null)]));
            // line 40
            echo "    ";
        }
    }

    // line 43
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 44
        echo "    ";
        $context["id"] = "systemcalendar-form";
        // line 45
        echo "
    ";
        // line 46
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General Information"), "class" => "active", "subblocks" => [0 => ["title" => "", "data" => [0 =>         // line 53
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "name", [], "any", false, false, false, 53), 'row'), 1 =>         // line 54
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "backgroundColor", [], "any", false, false, false, 54), 'row'), 2 =>         // line 55
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "public", [], "any", false, false, false, 55), 'row')]]]]];
        // line 60
        echo "
    ";
        // line 61
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), $this->extensions['Oro\Bundle\UIBundle\Twig\UiExtension']->renderAdditionalData($this->env, ($context["form"] ?? null), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional")));
        // line 62
        echo "
    ";
        // line 63
        $context["data"] = ["formErrors" => ((        // line 64
$this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) ? ($this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(($context["form"] ?? null), 'errors')) : (null)), "dataBlocks" =>         // line 65
($context["dataBlocks"] ?? null)];
        // line 67
        echo "    ";
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroCalendar/SystemCalendar/update.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 67,  149 => 65,  148 => 64,  147 => 63,  144 => 62,  142 => 61,  139 => 60,  137 => 55,  136 => 54,  135 => 53,  134 => 46,  131 => 45,  128 => 44,  124 => 43,  119 => 40,  116 => 39,  113 => 38,  107 => 36,  105 => 34,  104 => 31,  102 => 30,  99 => 29,  95 => 28,  89 => 25,  84 => 24,  81 => 23,  78 => 19,  75 => 18,  72 => 17,  69 => 14,  66 => 13,  64 => 9,  61 => 8,  58 => 7,  54 => 6,  49 => 1,  47 => 4,  45 => 3,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroCalendar/SystemCalendar/update.html.twig", "/websites/frogdata/crm-application/vendor/oro/calendar-bundle/Resources/views/SystemCalendar/update.html.twig");
    }
}
