<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Dashboard/widget.html.twig */
class __TwigTemplate_cc9020ae18de9d1baf913f18ee5e6a4e extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'content' => [$this, 'block_content'],
            'actions' => [$this, 'block_actions'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["widgetId"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 1), "get", [0 => "_wid"], "method", false, false, false, 1);
        // line 2
        $context["widgetContentId"] = ("widget-content-" . ($context["widgetId"] ?? null));
        // line 3
        $context["widgetType"] = ((array_key_exists("widgetType", $context)) ? (_twig_default_filter(($context["widgetType"] ?? null), "dashboard")) : ("dashboard"));
        // line 4
        $context["widgetClass"] = twig_lower_filter($this->env, twig_replace_filter(($context["widgetName"] ?? null), ["_" => "-"]));
        // line 5
        if ( !array_key_exists("widgetTitle", $context)) {
            // line 6
            echo "    ";
            if ( !array_key_exists("widgetLabel", $context)) {
                // line 7
                echo "        ";
                $context["widgetTitle"] = false;
                // line 8
                echo "    ";
            } elseif (((array_key_exists("widgetConfiguration", $context) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widgetConfiguration"] ?? null), "title", [], "any", true, true, false, 8)) &&  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widgetConfiguration"] ?? null), "title", [], "any", false, false, false, 8), "value", [], "any", false, false, false, 8)))) {
                // line 9
                echo "        ";
                $context["widgetTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["widgetConfiguration"] ?? null), "title", [], "any", false, false, false, 9), "value", [], "any", false, false, false, 9));
                // line 10
                echo "    ";
            } else {
                // line 11
                echo "        ";
                $context["widgetTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["widgetLabel"] ?? null));
                // line 12
                echo "    ";
            }
        }
        // line 14
        echo "
<div id=\"";
        // line 15
        echo twig_escape_filter($this->env, ($context["widgetContentId"] ?? null), "html", null, true);
        echo "\" class=\"invisible widget-content ";
        echo twig_escape_filter($this->env, ($context["widgetType"] ?? null), "html", null, true);
        echo "-widget-content ";
        echo twig_escape_filter($this->env, ($context["widgetClass"] ?? null), "html", null, true);
        echo "-widget-content\" data-widget-title=\"";
        echo twig_escape_filter($this->env, ($context["widgetTitle"] ?? null), "html", null, true);
        echo "\">
    ";
        // line 16
        $this->displayBlock('content', $context, $blocks);
        // line 18
        echo "    ";
        if (array_key_exists("widgetConfiguration", $context)) {
            // line 19
            echo "        ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["widgetConfiguration"] ?? null));
            foreach ($context['_seq'] as $context["configName"] => $context["config"]) {
                // line 20
                echo "            ";
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "show_on_widget", [], "any", false, false, false, 20) &&  !(null === Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "value", [], "any", false, false, false, 20)))) {
                    // line 21
                    echo "                ";
                    if (twig_test_iterable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "value", [], "any", false, false, false, 21))) {
                        // line 22
                        echo "                    ";
                        $context['_parent'] = $context;
                        $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "value", [], "any", false, false, false, 22));
                        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                            // line 23
                            echo "                <div class=\"widget-config-data\"><strong>";
                            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["key"]), "html", null, true);
                            echo "</strong>: ";
                            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                            echo "</div>
                    ";
                        }
                        $_parent = $context['_parent'];
                        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
                        $context = array_intersect_key($context, $_parent) + $_parent;
                        // line 25
                        echo "                ";
                    } else {
                        // line 26
                        echo "                    ";
                        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "options", [], "any", true, true, false, 26) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "options", [], "any", false, true, false, 26), "label", [], "any", true, true, false, 26))) {
                            // line 27
                            echo "                        ";
                            $context["label"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "options", [], "any", false, false, false, 27), "label", [], "any", false, false, false, 27);
                            // line 28
                            echo "                    ";
                        } else {
                            // line 29
                            echo "                        ";
                            $context["label"] = $context["configName"];
                            // line 30
                            echo "                    ";
                        }
                        // line 31
                        echo "                    ";
                        $context["value"] = null;
                        // line 32
                        echo "                    ";
                        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "options", [], "any", true, true, false, 32) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "options", [], "any", false, true, false, 32), "choices", [], "any", true, true, false, 32))) {
                            // line 33
                            echo "                        ";
                            $context['_parent'] = $context;
                            $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "options", [], "any", false, false, false, 33), "choices", [], "any", false, false, false, 33));
                            foreach ($context['_seq'] as $context["choiceLabel"] => $context["choiceValue"]) {
                                // line 34
                                echo "                            ";
                                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "value", [], "any", false, false, false, 34) == $context["choiceValue"])) {
                                    // line 35
                                    echo "                                ";
                                    $context["value"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans($context["choiceLabel"]);
                                    // line 36
                                    echo "                            ";
                                }
                                // line 37
                                echo "                        ";
                            }
                            $_parent = $context['_parent'];
                            unset($context['_seq'], $context['_iterated'], $context['choiceLabel'], $context['choiceValue'], $context['_parent'], $context['loop']);
                            $context = array_intersect_key($context, $_parent) + $_parent;
                            // line 38
                            echo "                    ";
                        }
                        // line 39
                        echo "                    ";
                        if ((null === ($context["value"] ?? null))) {
                            // line 40
                            echo "                        ";
                            $context["value"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["config"], "value", [], "any", false, false, false, 40);
                            // line 41
                            echo "                    ";
                        }
                        // line 42
                        echo "                    ";
                        if ((($context["value"] ?? null) === true)) {
                            // line 43
                            echo "                        ";
                            $context["value"] = ((($context["value"] ?? null)) ? ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.widget.option_value.boolean.true")) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.widget.option_value.boolean.false")));
                            // line 46
                            echo "                    ";
                        }
                        // line 47
                        echo "                    <div class=\"widget-config-data\"><strong>";
                        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(($context["label"] ?? null)), "html", null, true);
                        echo "</strong>: ";
                        echo twig_escape_filter($this->env, ($context["value"] ?? null), "html", null, true);
                        echo "</div>
                ";
                    }
                    // line 49
                    echo "            ";
                }
                // line 50
                echo "        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['configName'], $context['config'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 51
            echo "    ";
        }
        // line 52
        echo "    ";
        $this->displayBlock('actions', $context, $blocks);
        // line 73
        echo "</div>
";
    }

    // line 16
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 17
        echo "    ";
    }

    // line 52
    public function block_actions($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 53
        echo "        <div class=\"widget-actions\">
            ";
        // line 54
        if (array_key_exists("actions", $context)) {
            // line 55
            echo "                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["actions"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
                // line 56
                if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "type", [], "any", true, true, false, 56) || (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "type", [], "any", false, false, false, 56) == "button"))) {
                    // line 57
                    echo "                        ";
                    $context["cssType"] = ("btn btn-sm " . ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "class", [], "any", true, true, false, 57)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "class", [], "any", false, false, false, 57), "btn-primary")) : ("btn-primary")));
                    // line 58
                    echo "                    ";
                } else {
                    // line 59
                    echo "                        ";
                    $context["cssType"] = "dashboard-link";
                    // line 60
                    echo "                    ";
                }
                // line 61
                echo "                    <a class=\"dashboard-btn  ";
                echo twig_escape_filter($this->env, ($context["cssType"] ?? null), "html", null, true);
                if (( !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "url", [], "any", true, true, false, 61) ||  !Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "url", [], "any", false, false, false, 61))) {
                    echo " no-hash";
                }
                echo "\"
                        href=\"";
                // line 62
                echo twig_escape_filter($this->env, ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "url", [], "any", true, true, false, 62)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "url", [], "any", false, false, false, 62), "#")) : ("#")), "html", null, true);
                echo "\"
                        ";
                // line 63
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "data", [], "any", true, true, false, 63)) {
                    // line 64
                    echo "                            ";
                    $context['_parent'] = $context;
                    $context['_seq'] = twig_ensure_traversable(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "data", [], "any", false, false, false, 64));
                    foreach ($context['_seq'] as $context["dataItemName"] => $context["dataItemValue"]) {
                        // line 65
                        echo "                            data-";
                        echo twig_escape_filter($this->env, $context["dataItemName"], "html", null, true);
                        echo "=\"";
                        echo twig_escape_filter($this->env, $context["dataItemValue"], "html_attr");
                        echo "\"
                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['dataItemName'], $context['dataItemValue'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 67
                    echo "                        ";
                }
                // line 68
                echo "                    >";
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "icon", [], "any", true, true, false, 68)) {
                    echo "<i class=\"fa-";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "icon", [], "any", false, false, false, 68), "html", null, true);
                    echo "\"></i> ";
                }
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["action"], "label", [], "any", false, false, false, 68), "html", null, true);
                echo "</a>";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 70
            echo "            ";
        }
        // line 71
        echo "        </div>
    ";
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Dashboard/widget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  294 => 71,  291 => 70,  278 => 68,  275 => 67,  264 => 65,  259 => 64,  257 => 63,  253 => 62,  245 => 61,  242 => 60,  239 => 59,  236 => 58,  233 => 57,  231 => 56,  226 => 55,  224 => 54,  221 => 53,  217 => 52,  213 => 17,  209 => 16,  204 => 73,  201 => 52,  198 => 51,  192 => 50,  189 => 49,  181 => 47,  178 => 46,  175 => 43,  172 => 42,  169 => 41,  166 => 40,  163 => 39,  160 => 38,  154 => 37,  151 => 36,  148 => 35,  145 => 34,  140 => 33,  137 => 32,  134 => 31,  131 => 30,  128 => 29,  125 => 28,  122 => 27,  119 => 26,  116 => 25,  105 => 23,  100 => 22,  97 => 21,  94 => 20,  89 => 19,  86 => 18,  84 => 16,  74 => 15,  71 => 14,  67 => 12,  64 => 11,  61 => 10,  58 => 9,  55 => 8,  52 => 7,  49 => 6,  47 => 5,  45 => 4,  43 => 3,  41 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Dashboard/widget.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Dashboard/widget.html.twig");
    }
}
