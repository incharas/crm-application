<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroNavigation/js_modules_config.html.twig */
class __TwigTemplate_5923680a48f1ef8403a6276ea2142c51 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "debug", [], "any", false, false, false, 1)) {
            // line 2
            echo "    ";
            $context["routes"] = $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_navigation_js_routing_js", ["_format" => "json"]);
        } else {
            // line 4
            echo "    ";
            $context["routes"] = $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl((("media/js/" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["data"] ?? null), "js_routing_filename_prefix", [], "any", false, false, false, 4)) . "routes.json"), "routing");
        }
        // line 6
        $macros["Asset"] = $this->macros["Asset"] = $this->loadTemplate("@OroAsset/Asset.html.twig", "@OroNavigation/js_modules_config.html.twig", 6)->unwrap();
        // line 7
        echo twig_call_macro($macros["Asset"], "macro_js_modules_config", [["oronavigation/js/routes-loader" => ["debug" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 9
($context["app"] ?? null), "debug", [], "any", false, false, false, 9)) ? (true) : (false)), "data" => ["base_url" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 11
($context["app"] ?? null), "request", [], "any", false, false, false, 11), "baseUrl", [], "any", false, false, false, 11), "scheme" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 12
($context["app"] ?? null), "request", [], "any", false, false, false, 12), "scheme", [], "any", false, false, false, 12), "host" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 13
($context["app"] ?? null), "request", [], "any", false, false, false, 13), "host", [], "any", false, false, false, 13)], "routesResource" =>         // line 15
($context["routes"] ?? null)]]], 7, $context, $this->getSourceContext());
        // line 17
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroNavigation/js_modules_config.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  56 => 17,  54 => 15,  53 => 13,  52 => 12,  51 => 11,  50 => 9,  49 => 7,  47 => 6,  43 => 4,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroNavigation/js_modules_config.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/NavigationBundle/Resources/views/js_modules_config.html.twig");
    }
}
