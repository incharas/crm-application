<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUser/Role/view.html.twig */
class __TwigTemplate_050ec9bc9935afc0ce6fc37ae0f9fd73 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'pageHeader' => [$this, 'block_pageHeader'],
            'navButtonContainer' => [$this, 'block_navButtonContainer'],
            'navButtons' => [$this, 'block_navButtons'],
            'content_data' => [$this, 'block_content_data'],
            'stats' => [$this, 'block_stats'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/view.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["dataGrid"] = $this->macros["dataGrid"] = $this->loadTemplate("@OroDataGrid/macros.html.twig", "@OroUser/Role/view.html.twig", 2)->unwrap();
        // line 3
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Role/view.html.twig", 3)->unwrap();
        // line 4
        $macros["entityConfig"] = $this->macros["entityConfig"] = $this->loadTemplate("@OroEntityConfig/macros.html.twig", "@OroUser/Role/view.html.twig", 4)->unwrap();

        $this->env->getExtension("Oro\Bundle\NavigationBundle\Twig\TitleExtension")->set(["params" => ["%role%" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 6
($context["entity"] ?? null), "label", [], "any", false, false, false, 6)]]);
        // line 8
        $context["gridName"] = "role-view-users-grid";
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/view.html.twig", "@OroUser/Role/view.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 10
    public function block_pageHeader($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $context["breadcrumbs"] = ["entity" =>         // line 12
($context["entity"] ?? null), "indexPath" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_role_index"), "indexLabel" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.entity_plural_label"), "entityTitle" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 15
($context["entity"] ?? null), "label", [], "any", true, true, false, 15)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "label", [], "any", false, false, false, 15), $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A"))) : ($this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("N/A")))];
        // line 17
        echo "    ";
        $this->displayParentBlock("pageHeader", $context, $blocks);
        echo "
";
    }

    // line 20
    public function block_navButtonContainer($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 21
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Role/view.html.twig", 21)->unwrap();
        // line 22
        echo "
    ";
        // line 23
        $this->displayBlock('navButtons', $context, $blocks);
    }

    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 24
        echo "        ";
        echo $this->extensions['Oro\Bundle\UIBundle\Twig\PlaceholderExtension']->renderPlaceholder($this->env, ((array_key_exists("view_navButtons", $context)) ? (_twig_default_filter(($context["view_navButtons"] ?? null), "view_navButtons")) : ("view_navButtons")), ["entity" => ($context["entity"] ?? null)]);
        // line 25
        echo "        ";
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", true, true, false, 25) && $this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("DELETE", ($context["entity"] ?? null)))) {
            // line 26
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_deleteButton", [["dataUrl" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_api_delete_role", ["id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 27
($context["entity"] ?? null), "id", [], "any", false, false, false, 27)]), "dataRedirect" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_user_role_index"), "aCss" => "no-hash remove-button", "dataId" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 30
($context["entity"] ?? null), "id", [], "any", false, false, false, 30), "id" => "btn-remove-role", "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.entity_label"), "disabled" =>  !            // line 33
($context["allow_delete"] ?? null)]], 26, $context, $this->getSourceContext());
            // line 35
            echo "
        ";
        }
        // line 37
        echo "    ";
    }

    // line 40
    public function block_content_data($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 41
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroUser/Role/view.html.twig", 41)->unwrap();
        // line 42
        echo "
    ";
        // line 43
        $context["dataBlocks"] = [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("General"), "subblocks" => [0 => ["data" => [0 => twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.role.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 48
($context["entity"] ?? null), "label", [], "any", false, false, false, 48)], 48, $context, $this->getSourceContext())]]]]];
        // line 53
        echo "
    ";
        // line 54
        ob_start(function () { return ''; });
        // line 55
        echo "        ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "organization", [], "any", true, true, false, 55)) {
            // line 56
            echo "            ";
            echo twig_call_macro($macros["UI"], "macro_renderProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.organization.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "organization", [], "any", false, false, false, 56)], 56, $context, $this->getSourceContext());
            echo "
        ";
        }
        // line 58
        echo "        ";
        echo twig_call_macro($macros["UI"], "macro_renderSwitchableHtmlProperty", [$this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.role.extend_description.label"), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "extendDescription", [], "any", false, false, false, 58)], 58, $context, $this->getSourceContext());
        echo "
        ";
        // line 59
        $context["ignoredFields"] = [0 => "organization", 1 => "extend_description"];
        // line 60
        echo "        ";
        echo twig_call_macro($macros["entityConfig"], "macro_renderDynamicFields", [($context["entity"] ?? null), ($context["entity_class"] ?? null), ($context["ignoredFields"] ?? null)], 60, $context, $this->getSourceContext());
        echo "
    ";
        $context["additionalBlock"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 62
        echo "
    ";
        // line 63
        ob_start(function () { return ''; });
        // line 64
        echo "        ";
        $context["entityTabPanelId"] = uniqid("entity-tab-panel-");
        // line 65
        echo "        ";
        $context["tabsOptions"] = twig_array_merge(($context["tabsOptions"] ?? null), ["controlTabPanel" =>         // line 66
($context["entityTabPanelId"] ?? null)]);
        // line 68
        echo "
        <div ";
        // line 69
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orouser/js/components/role/entity-category-tabs-component", "options" =>         // line 71
($context["tabsOptions"] ?? null)]], 69, $context, $this->getSourceContext());
        // line 72
        echo "></div>
        <div id=\"";
        // line 73
        echo twig_escape_filter($this->env, ($context["entityTabPanelId"] ?? null), "html", null, true);
        echo "\" class=\"tab-content\" role=\"tabpanel\">
            ";
        // line 74
        echo twig_call_macro($macros["dataGrid"], "macro_renderGrid", ["role-permission-grid", ["role" => ($context["entity"] ?? null)], ["cssClass" => "inner-permissions-grid", "themeOptions" => ["readonly" => true]]], 74, $context, $this->getSourceContext());
        echo "
            <div ";
        // line 75
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orouser/js/components/role/capability-set-component", "options" =>         // line 77
($context["capabilitySetOptions"] ?? null)]], 75, $context, $this->getSourceContext());
        // line 78
        echo "></div>
        </div>
    ";
        $context["rolePermissionsGrid"] = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 81
        echo "
    ";
        // line 82
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Additional"), "subblocks" => [0 => ["data" => [0 =>         // line 86
($context["additionalBlock"] ?? null)]]]]]);
        // line 90
        echo "
    ";
        // line 91
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("Entity"), "subblocks" => [0 => ["data" => [0 =>         // line 95
($context["rolePermissionsGrid"] ?? null)]]]]]);
        // line 99
        echo "
    ";
        // line 100
        $context["dataBlocks"] = twig_array_merge(($context["dataBlocks"] ?? null), [0 => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.user.entity_plural_label"), "subblocks" => [0 => ["title" => null, "useSpan" => false, "data" => [0 => twig_call_macro($macros["dataGrid"], "macro_renderGrid", [        // line 106
($context["gridName"] ?? null), ["role_id" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["entity"] ?? null), "id", [], "any", false, false, false, 106)], ["cssClass" => "inner-grid"]], 106, $context, $this->getSourceContext())]]]]]);
        // line 110
        echo "
    ";
        // line 111
        $context["id"] = "user-role-view";
        // line 112
        echo "    ";
        $context["data"] = ["dataBlocks" => ($context["dataBlocks"] ?? null)];
        // line 113
        echo "
    ";
        // line 114
        $this->displayParentBlock("content_data", $context, $blocks);
        echo "
";
    }

    // line 117
    public function block_stats($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 118
        echo "    ";
    }

    public function getTemplateName()
    {
        return "@OroUser/Role/view.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  231 => 118,  227 => 117,  221 => 114,  218 => 113,  215 => 112,  213 => 111,  210 => 110,  208 => 106,  207 => 100,  204 => 99,  202 => 95,  201 => 91,  198 => 90,  196 => 86,  195 => 82,  192 => 81,  187 => 78,  185 => 77,  184 => 75,  180 => 74,  176 => 73,  173 => 72,  171 => 71,  170 => 69,  167 => 68,  165 => 66,  163 => 65,  160 => 64,  158 => 63,  155 => 62,  149 => 60,  147 => 59,  142 => 58,  136 => 56,  133 => 55,  131 => 54,  128 => 53,  126 => 48,  125 => 43,  122 => 42,  119 => 41,  115 => 40,  111 => 37,  107 => 35,  105 => 33,  104 => 30,  103 => 27,  101 => 26,  98 => 25,  95 => 24,  88 => 23,  85 => 22,  82 => 21,  78 => 20,  71 => 17,  69 => 15,  68 => 12,  66 => 11,  62 => 10,  57 => 1,  55 => 8,  53 => 6,  50 => 4,  48 => 3,  46 => 2,  39 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUser/Role/view.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UserBundle/Resources/views/Role/view.html.twig");
    }
}
