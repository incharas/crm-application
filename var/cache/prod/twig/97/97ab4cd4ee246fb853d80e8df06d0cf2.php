<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Dashboard/bigNumberSubwidget.html.twig */
class __TwigTemplate_f721b2e3685e6c33e62f080c09f401a0 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $context["itemTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "label", [], "any", false, false, false, 1));
        // line 2
        echo "<span class=\"title\" title=\"";
        echo twig_escape_filter($this->env, ($context["itemTitle"] ?? null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, ($context["itemTitle"] ?? null), "html", null, true);
        echo "</span>
<h3 class=\"value\">";
        // line 3
        echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 3), "value", [], "any", false, false, false, 3), "html", null, true);
        echo "</h3>
";
        // line 4
        if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, true, false, 4), "deviation", [], "any", true, true, false, 4) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, true, false, 4), "previousRange", [], "any", true, true, false, 4))) {
            // line 5
            echo "<div class=\"deviation\">
    <span class=\"deviation ";
            // line 6
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, true, false, 6), "isPositive", [], "any", true, true, false, 6)) {
                if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 6), "isPositive", [], "any", false, false, false, 6)) {
                    echo "positive";
                } else {
                    echo "negative";
                }
            }
            echo "\">
        ";
            // line 7
            if (twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 7), "deviation", [], "any", false, false, false, 7))) {
                // line 8
                echo "            ";
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.widget.big_number.no_changes"), "html", null, true);
                echo "
        ";
            } else {
                // line 10
                echo "            ";
                echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 10), "deviation", [], "any", false, false, false, 10));
                echo "
        ";
            }
            // line 12
            echo "    </span>
</div>
<div class=\"deviation\">
    <span class=\"deviation\">
        ";
            // line 16
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.widget.big_number.compare_to.label"), "html", null, true);
            echo ":
    </span>
    ";
            // line 18
            $context["range"] = $this->extensions['Oro\Bundle\DashboardBundle\Twig\DashboardExtension']->getViewValue(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["item"] ?? null), "value", [], "any", false, false, false, 18), "previousRange", [], "any", false, false, false, 18));
            // line 19
            echo "    <span class=\"date-range\" title=\"";
            echo twig_escape_filter($this->env, ($context["range"] ?? null), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, ($context["range"] ?? null), "html", null, true);
            echo "</span>
</div>
";
        }
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Dashboard/bigNumberSubwidget.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  92 => 19,  90 => 18,  85 => 16,  79 => 12,  73 => 10,  67 => 8,  65 => 7,  55 => 6,  52 => 5,  50 => 4,  46 => 3,  39 => 2,  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Dashboard/bigNumberSubwidget.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Dashboard/bigNumberSubwidget.html.twig");
    }
}
