<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUI/layouts/default/page/middle_bar.html.twig */
class __TwigTemplate_09585ab0b4fc15a5246b37c49feb0c7d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_middle_bar_widget' => [$this, 'block__middle_bar_widget'],
            '_middle_bar_logo_widget' => [$this, 'block__middle_bar_logo_widget'],
            '_middle_bar_center_widget' => [$this, 'block__middle_bar_center_widget'],
            '_middle_bar_right_widget' => [$this, 'block__middle_bar_right_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_middle_bar_widget', $context, $blocks);
        // line 9
        echo "
";
        // line 10
        $this->displayBlock('_middle_bar_logo_widget', $context, $blocks);
        // line 18
        echo "
";
        // line 19
        $this->displayBlock('_middle_bar_center_widget', $context, $blocks);
        // line 27
        echo "
";
        // line 28
        $this->displayBlock('_middle_bar_right_widget', $context, $blocks);
    }

    // line 1
    public function block__middle_bar_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " grid middlebar"]);
        // line 5
        echo "    <div ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 6
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 10
    public function block__middle_bar_logo_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 11
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " grid-col middlebar__left"]);
        // line 14
        echo "    <div ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 15
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 19
    public function block__middle_bar_center_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 20
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " grid-col middlebar__center"]);
        // line 23
        echo "    <div ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 24
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    // line 28
    public function block__middle_bar_right_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 29
        echo "    ";
        $context["attr"] = $this->extensions['Oro\Bundle\LayoutBundle\Twig\LayoutExtension']->defaultAttributes(($context["attr"] ?? null), ["~class" => " grid-col middlebar__right"]);
        // line 32
        echo "    <div ";
        $this->displayBlock("block_attributes", $context, $blocks);
        echo ">
        ";
        // line 33
        echo $this->env->getRuntime("Oro\Bundle\LayoutBundle\Twig\TwigRenderer")->searchAndRenderBlock(($context["block"] ?? null), 'widget');
        echo "
    </div>
";
    }

    public function getTemplateName()
    {
        return "@OroUI/layouts/default/page/middle_bar.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  129 => 33,  124 => 32,  121 => 29,  117 => 28,  110 => 24,  105 => 23,  102 => 20,  98 => 19,  91 => 15,  86 => 14,  83 => 11,  79 => 10,  72 => 6,  67 => 5,  64 => 2,  60 => 1,  56 => 28,  53 => 27,  51 => 19,  48 => 18,  46 => 10,  43 => 9,  41 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUI/layouts/default/page/middle_bar.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/middle_bar.html.twig");
    }
}
