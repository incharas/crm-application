<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTag/Tag/Autocomplete/result.html.twig */
class __TwigTemplate_f0274d3fb36bb2f8a1c7be68b325fcac extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "<%= highlight(_.escape(name)) %><% if(typeof isNew != 'undefined') { %>
<span class=\"select2__result-entry-info\">
    ( <%- _.__('oro.tag.inline_editing.new_tag') %> )
</span>
<% } %>
";
    }

    public function getTemplateName()
    {
        return "@OroTag/Tag/Autocomplete/result.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTag/Tag/Autocomplete/result.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TagBundle/Resources/views/Tag/Autocomplete/result.html.twig");
    }
}
