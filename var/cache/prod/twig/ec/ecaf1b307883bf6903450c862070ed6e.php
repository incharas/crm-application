<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDigitalAsset/Form/fields.html.twig */
class __TwigTemplate_d845cfc39dca04174036ed2a45c62692 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            '_oro_digital_asset_sourceFile_widget' => [$this, 'block__oro_digital_asset_sourceFile_widget'],
            '_oro_digital_asset_sourceFile_file_errors' => [$this, 'block__oro_digital_asset_sourceFile_file_errors'],
            'oro_file_with_digital_asset_widget' => [$this, 'block_oro_file_with_digital_asset_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('_oro_digital_asset_sourceFile_widget', $context, $blocks);
        // line 25
        echo "
";
        // line 26
        $this->displayBlock('_oro_digital_asset_sourceFile_file_errors', $context, $blocks);
        // line 41
        echo "
";
        // line 42
        $this->displayBlock('oro_file_with_digital_asset_widget', $context, $blocks);
    }

    // line 1
    public function block__oro_digital_asset_sourceFile_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    ";
        $this->displayBlock("oro_file_widget", $context, $blocks);
        // line 4
        if ((( !twig_test_empty(($context["value"] ?? null)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "id", [], "any", false, false, false, 4)) && $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getTypeIsImage(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "mimeType", [], "any", false, false, false, 4)))) {
            // line 5
            $context["pictureSources"] = $this->extensions['Oro\Bundle\AttachmentBundle\Twig\FileExtension']->getFilteredPictureSources(($context["value"] ?? null), "digital_asset_large");
            // line 6
            echo "        <div class=\"preview\">
            <a class=\"view-image no-hash\"
               tabindex=\"-1\"
               data-gallery=\"image";
            // line 9
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "id", [], "any", false, false, false, 9), "html", null, true);
            echo "\"
               data-sources=\"";
            // line 10
            echo twig_escape_filter($this->env, json_encode(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pictureSources"] ?? null), "sources", [], "any", false, false, false, 10)), "html", null, true);
            echo "\"
               href=\"";
            // line 11
            echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["pictureSources"] ?? null), "src", [], "any", false, false, false, 11), "html", null, true);
            echo "\"
            >
                ";
            // line 13
            $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroDigitalAsset/Form/fields.html.twig", 13)->display(twig_array_merge($context, ["file" =>             // line 14
($context["value"] ?? null), "filter" => "digital_asset_medium", "img_attrs" => ["class" => "thumbnail", "alt" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,             // line 18
($context["value"] ?? null), "originalFilename", [], "any", false, false, false, 18)]]));
            // line 21
            echo "            </a>
        </div>";
        }
    }

    // line 26
    public function block__oro_digital_asset_sourceFile_file_errors($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 27
        ob_start(function () { return ''; });
        // line 28
        if ((twig_length_filter($this->env, ($context["errors"] ?? null)) > 0)) {
            // line 29
            echo "            ";
            $context["combinedError"] = "";
            // line 30
            echo "            ";
            $context["newErrors"] = [];
            // line 31
            echo "            ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["errors"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 32
                echo "                ";
                if (!twig_in_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 32), ($context["newErrors"] ?? null))) {
                    // line 33
                    echo "                    ";
                    $context["newErrors"] = twig_array_merge(($context["newErrors"] ?? null), [0 => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["error"], "message", [], "any", false, false, false, 33)]);
                    // line 34
                    echo "                ";
                }
                // line 35
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 36
            echo "            ";
            $context["combinedError"] = twig_join_filter(($context["newErrors"] ?? null), "; ");
            // line 37
            echo "            <span class=\"validation-failed\"><span><span>";
            echo $this->extensions['Oro\Bundle\UIBundle\Twig\HtmlTagExtension']->htmlSanitize(($context["combinedError"] ?? null));
            echo "</span></span></span>
        ";
        }
        $___internal_parse_56_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 27
        echo twig_spaceless($___internal_parse_56_);
    }

    // line 42
    public function block_oro_file_with_digital_asset_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 43
        ob_start(function () { return ''; });
        // line 44
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDigitalAsset/Form/fields.html.twig", 44)->unwrap();
        // line 45
        echo "
        <div class=\"digital-asset-item fields-row\"
            ";
        // line 47
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "oroui/js/app/components/view-component", "options" => ["view" => "orodigitalasset/js/app/views/digital-asset-choose-form-view", "isImageType" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 51
($context["dam_widget"] ?? null), "is_image_type", [], "any", false, false, false, 51), "isSet" => (( !twig_test_empty(        // line 52
($context["value"] ?? null)) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["value"] ?? null), "id", [], "any", false, false, false, 52)) ||  !twig_test_empty(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "digitalAsset", [], "any", false, false, false, 52), "vars", [], "any", false, false, false, 52), "data", [], "any", false, false, false, 52))), "widgetOptions" => ["title" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 54
($context["dam_widget"] ?? null), "is_image_type", [], "any", false, false, false, 54)) ? ("oro.digitalasset.dam.dialog.select_image") : ("oro.digitalasset.dam.dialog.select_file"))), "url" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 57
($context["dam_widget"] ?? null), "route", [], "any", true, true, false, 57)) ? (_twig_default_filter(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "route", [], "any", false, false, false, 57), "oro_digital_asset_widget_choose")) : ("oro_digital_asset_widget_choose")), Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "parameters", [], "any", false, false, false, 57)), "dialogOptions" => ["modal" => true, "dialogClass" => "digital-asset-dialog"]], "selectors" => ["digitalAssetInput" => ("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 64
($context["form"] ?? null), "digitalAsset", [], "any", false, false, false, 64), "vars", [], "any", false, false, false, 64), "id", [], "any", false, false, false, 64)), "emptyFileInput" => ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,         // line 65
($context["form"] ?? null), "emptyFile", [], "any", true, true, false, 65)) ? (("#" . Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 65), "vars", [], "any", false, false, false, 65), "id", [], "any", false, false, false, 65))) : (null))]]]], 47, $context, $this->getSourceContext());
        // line 68
        echo "
        >
            <div class=\"digital-asset-value\" data-role=\"digital-asset-value\">";
        // line 71
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "is_valid_digital_asset", [], "any", false, false, false, 71)) {
            // line 72
            if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 72)) {
                // line 73
                echo "                        <a href=\"";
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 73), "download", [], "any", false, false, false, 73), "html", null, true);
                echo "\" class=\"no-hash digital-asset-filename\"
                        title=\"";
                // line 74
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 74), "title", [], "any", false, false, false, 74), "html", null, true);
                echo "\" data-role=\"digital-asset-filename\">
                            <span class=\"digital-asset-filename__preview\">
                                ";
                // line 76
                if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "is_image_type", [], "any", false, false, false, 76) && Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 76), "preview", [], "any", false, false, false, 76))) {
                    // line 77
                    echo "                                    ";
                    if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, true, false, 77), "preview_webp", [], "any", true, true, false, 77)) {
                        // line 78
                        echo "                                        ";
                        $context["sources"] = [0 => ["srcset" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                         // line 79
($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 79), "preview_webp", [], "any", false, false, false, 79), "type" => "image/webp"]];
                        // line 82
                        echo "                                    ";
                    }
                    // line 83
                    echo "                                    ";
                    $this->loadTemplate("@OroAttachment/Twig/picture.html.twig", "@OroDigitalAsset/Form/fields.html.twig", 83)->display(twig_array_merge($context, ["sources" => ((                    // line 84
array_key_exists("sources", $context)) ? (_twig_default_filter(($context["sources"] ?? null), [])) : ([])), "img_attrs" => ["src" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 86
($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 86), "preview", [], "any", false, false, false, 86), "alt" => Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source,                     // line 87
($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 87), "title", [], "any", false, false, false, 87)]]));
                    // line 90
                    echo "                                ";
                } else {
                    // line 91
                    echo "                                    <span class=\"";
                    echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 91), "icon", [], "any", false, false, false, 91), "html", null, true);
                    echo "\" aria-hidden=\"true\"></span>
                                ";
                }
                // line 93
                echo "                            </span>
                            <span class=\"digital-asset-filename__text\">";
                // line 94
                echo twig_escape_filter($this->env, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "preview_metadata", [], "any", false, false, false, 94), "filename", [], "any", false, false, false, 94), "html", null, true);
                echo "</span>
                        </a>
                    ";
            }
        } else {
            // line 98
            echo "<a href=\"#\" class=\"no-hash digital-asset-filename\" data-role=\"digital-asset-filename\">
                        <span class=\"digital-asset-filename__preview\">
                            <span class=\"fa-file-o\" aria-hidden=\"true\"></span>
                        </span>
                        <span class=\"digital-asset-filename__text\">";
            // line 102
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.form.invalid_digital_asset"), "html", null, true);
            echo "</span>
                    </a>";
        }
        // line 105
        echo "</div>

            <div class=\"digital-asset-controls hide\" data-role=\"digital-asset-controls\">
                <a href=\"#\" role=\"button\" data-role=\"digital-asset-update\" class=\"btn btn-icon btn-square-lighter\"
                   title=\"";
        // line 109
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.form.choose_another.label"), "html", null, true);
        echo "\"
                   aria-label=\"";
        // line 110
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.form.choose_another.label"), "html", null, true);
        echo "\">
                    <span class=\"fa-edit\" aria-hidden=\"true\"></span>
                </a>

                ";
        // line 114
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", true, true, false, 114)) {
            // line 115
            echo "                    ";
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "emptyFile", [], "any", false, false, false, 115), 'row');
            echo "

                    <a href=\"#\" role=\"button\" data-role=\"digital-asset-remove\" class=\"btn btn-icon btn-square-lighter\"
                       title=\"";
            // line 118
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.form.remove.label"), "html", null, true);
            echo "\"
                       aria-label=\"";
            // line 119
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.digitalasset.dam.form.remove.label"), "html", null, true);
            echo "\">
                        <span class=\"fa-close\" aria-hidden=\"true\"></span>
                    </a>
                ";
        }
        // line 123
        echo "            </div>

            <a href=\"#\" class=\"btn digital-asset-choose hide\" role=\"button\" data-role=\"digital-asset-choose\">
                ";
        // line 126
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans(((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["dam_widget"] ?? null), "is_image_type", [], "any", false, false, false, 126)) ? ("oro.digitalasset.dam.form.choose_image.label") : ("oro.digitalasset.dam.form.choose_file.label"))), "html", null, true);
        // line 128
        echo "
            </a>

            ";
        // line 131
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "digitalAsset", [], "any", false, false, false, 131), 'row');
        echo "
        </div>

        ";
        // line 134
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "file", [], "any", false, false, false, 134), 'errors');
        echo "
        ";
        // line 135
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "digitalAsset", [], "any", false, false, false, 135), 'errors');
        $___internal_parse_57_ = ('' === $tmp = ob_get_clean()) ? '' : new Markup($tmp, $this->env->getCharset());
        // line 43
        echo twig_spaceless($___internal_parse_57_);
    }

    public function getTemplateName()
    {
        return "@OroDigitalAsset/Form/fields.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  289 => 43,  286 => 135,  282 => 134,  276 => 131,  271 => 128,  269 => 126,  264 => 123,  257 => 119,  253 => 118,  246 => 115,  244 => 114,  237 => 110,  233 => 109,  227 => 105,  222 => 102,  216 => 98,  209 => 94,  206 => 93,  200 => 91,  197 => 90,  195 => 87,  194 => 86,  193 => 84,  191 => 83,  188 => 82,  186 => 79,  184 => 78,  181 => 77,  179 => 76,  174 => 74,  169 => 73,  167 => 72,  165 => 71,  161 => 68,  159 => 65,  158 => 64,  157 => 57,  156 => 54,  155 => 52,  154 => 51,  153 => 47,  149 => 45,  147 => 44,  145 => 43,  141 => 42,  137 => 27,  130 => 37,  127 => 36,  121 => 35,  118 => 34,  115 => 33,  112 => 32,  107 => 31,  104 => 30,  101 => 29,  99 => 28,  97 => 27,  93 => 26,  87 => 21,  85 => 18,  84 => 14,  83 => 13,  78 => 11,  74 => 10,  70 => 9,  65 => 6,  63 => 5,  61 => 4,  58 => 2,  54 => 1,  50 => 42,  47 => 41,  45 => 26,  42 => 25,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDigitalAsset/Form/fields.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DigitalAssetBundle/Resources/views/Form/fields.html.twig");
    }
}
