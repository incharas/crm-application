<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroSales/Dashboard/leadsLaunchpad.html.twig */
class __TwigTemplate_dd8f1f2c8a4709a450958d1fc244becc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroDashboard/Dashboard/launchpad.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 3
        $context["widgetName"] = "leads_launchpad";
        // line 4
        $context["widgetLabel"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.entity_plural_label");
        // line 5
        $context["widgetIcon"] = "phone";
        // line 6
        $context["widgetAcl"] = "oro_sales_lead_view";
        // line 8
        $context["items"] = ["index" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.list"), "route" => "oro_sales_lead_index", "acl" => "oro_sales_lead_view"], "create" => ["label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.ui.create_entity", ["%entityName%" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.sales.lead.entity_label")]), "route" => "oro_sales_lead_create", "acl" => "oro_sales_lead_create"]];
        // line 1
        $this->parent = $this->loadTemplate("@OroDashboard/Dashboard/launchpad.html.twig", "@OroSales/Dashboard/leadsLaunchpad.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    public function getTemplateName()
    {
        return "@OroSales/Dashboard/leadsLaunchpad.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  51 => 1,  49 => 8,  47 => 6,  45 => 5,  43 => 4,  41 => 3,  34 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroSales/Dashboard/leadsLaunchpad.html.twig", "/websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/SalesBundle/Resources/views/Dashboard/leadsLaunchpad.html.twig");
    }
}
