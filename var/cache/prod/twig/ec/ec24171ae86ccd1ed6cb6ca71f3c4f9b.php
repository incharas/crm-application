<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroDashboard/Dashboard/index.html.twig */
class __TwigTemplate_6fbc70f75c67bef09bcc8722d7767bd9 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'navButtons' => [$this, 'block_navButtons'],
            'content' => [$this, 'block_content'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "@OroUI/actions/index.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        $macros["UI"] = $this->macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Dashboard/index.html.twig", 2)->unwrap();
        // line 3
        $context["gridName"] = "dashboards-grid";
        // line 5
        $context["pageTitle"] = $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.management_title");
        // line 1
        $this->parent = $this->loadTemplate("@OroUI/actions/index.html.twig", "@OroDashboard/Dashboard/index.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 7
    public function block_navButtons($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 8
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Dashboard/index.html.twig", 8)->unwrap();
        // line 9
        echo "
    ";
        // line 10
        if ($this->extensions['Symfony\Bridge\Twig\Extension\SecurityExtension']->isGranted("oro_dashboard_create")) {
            // line 11
            echo "        ";
            echo twig_call_macro($macros["UI"], "macro_addButton", [["path" => $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("oro_dashboard_create"), "entity_label" => $this->extensions['Symfony\Bridge\Twig\Extension\TranslationExtension']->trans("oro.dashboard.entity_label")]], 11, $context, $this->getSourceContext());
            // line 14
            echo "
    ";
        }
    }

    // line 18
    public function block_content($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 19
        echo "    ";
        $macros["UI"] = $this->loadTemplate("@OroUI/macros.html.twig", "@OroDashboard/Dashboard/index.html.twig", 19)->unwrap();
        // line 20
        echo "
    ";
        // line 21
        $this->displayParentBlock("content", $context, $blocks);
        echo "

    <div ";
        // line 23
        echo twig_call_macro($macros["UI"], "macro_renderPageComponentAttributes", [["module" => "orodashboard/js/app/components/dashboard-navigation-component", "options" => ["gridName" =>         // line 26
($context["gridName"] ?? null)]]], 23, $context, $this->getSourceContext());
        // line 28
        echo "></div>
";
    }

    public function getTemplateName()
    {
        return "@OroDashboard/Dashboard/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  93 => 28,  91 => 26,  90 => 23,  85 => 21,  82 => 20,  79 => 19,  75 => 18,  69 => 14,  66 => 11,  64 => 10,  61 => 9,  58 => 8,  54 => 7,  49 => 1,  47 => 5,  45 => 3,  43 => 2,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroDashboard/Dashboard/index.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Resources/views/Dashboard/index.html.twig");
    }
}
