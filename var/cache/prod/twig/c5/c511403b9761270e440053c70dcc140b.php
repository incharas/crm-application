<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/app/views/history-navigation-view.js */
class __TwigTemplate_8dd2d4e3bca84658ea62d7f86c46bb67 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';

    const BaseView = require('./base/view');

    const HistoryNavigationView = BaseView.extend({
        autoRender: true,
        template: require('tpl-loader!oroui/templates/history.html'),
        events: {
            'click .undo-btn': 'onUndo',
            'click .redo-btn': 'onRedo'
        },

        listen: {
            'change:index model': 'render'
        },

        /**
         * @inheritdoc
         */
        constructor: function HistoryNavigationView(options) {
            HistoryNavigationView.__super__.constructor.call(this, options);
        },

        onUndo: function() {
            const index = this.model.get('index');
            this.trigger('navigate', index - 1);
        },

        onRedo: function() {
            const index = this.model.get('index');
            this.trigger('navigate', index + 1);
        }
    });

    return HistoryNavigationView;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/app/views/history-navigation-view.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/app/views/history-navigation-view.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/app/views/history-navigation-view.js");
    }
}
