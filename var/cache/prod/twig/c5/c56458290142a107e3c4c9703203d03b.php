<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroTranslation/default.html.twig */
class __TwigTemplate_c7929372b0fb6ab14ed01609a4e179f1 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
            'oro_translations_widget' => [$this, 'block_oro_translations_widget'],
            'oro_translations_gedmo_widget' => [$this, 'block_oro_translations_gedmo_widget'],
            'oro_translationsForms_widget' => [$this, 'block_oro_translationsForms_widget'],
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        $this->displayBlock('oro_translations_widget', $context, $blocks);
        // line 26
        echo "
";
        // line 27
        $this->displayBlock('oro_translations_gedmo_widget', $context, $blocks);
        // line 62
        echo "
";
        // line 63
        $this->displayBlock('oro_translationsForms_widget', $context, $blocks);
    }

    // line 1
    public function block_oro_translations_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 2
        echo "    <div class=\"oro_translations tabbable\">
        <ul class=\"oro_translationsLocales nav nav-tabs\">
        ";
        // line 4
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
            // line 5
            echo "            ";
            $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 5), "name", [], "any", false, false, false, 5);
            // line 6
            echo "
            <li class=\"nav-item";
            // line 7
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 7), "locale", [], "any", false, false, false, 7) == ($context["locale"] ?? null))) {
                echo " active";
            }
            echo "\">
                <a href=\"#\" data-toggle=\"tab\" data-target=\".oro_translationsFields-";
            // line 8
            echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
            echo "\" class=\"nav-link\">
                   ";
            // line 9
            echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ($context["locale"] ?? null)), "html", null, true);
            echo "
                </a>
            </li>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 13
        echo "        </ul>

        <div class=\"oro_translationsFields tab-content\">
        ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
            // line 17
            echo "            ";
            $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 17), "name", [], "any", false, false, false, 17);
            // line 18
            echo "
            <div class=\"oro_translationsFields-";
            // line 19
            echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
            echo " tab-pane ";
            if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 19), "locale", [], "any", false, false, false, 19) == ($context["locale"] ?? null))) {
                echo "active";
            }
            echo "\">
                ";
            // line 20
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["translationsFields"], 'widget');
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 23
        echo "        </div>
    </div>
";
    }

    // line 27
    public function block_oro_translations_gedmo_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 28
        echo "    ";
        if (Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["form"] ?? null), "vars", [], "any", false, false, false, 28), "simple_way", [], "any", false, false, false, 28)) {
            // line 29
            echo "        ";
            $this->displayBlock("oro_translations_widget", $context, $blocks);
            echo "
    ";
        } else {
            // line 31
            echo "        <div class=\"oro_translations tabbable\">
            <ul class=\"oro_translationsLocales nav nav-tabs\">
            ";
            // line 33
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["translationsLocales"]) {
                // line 34
                echo "                ";
                $context["isDefaultLocale"] = ("defaultLocale" == Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsLocales"], "vars", [], "any", false, false, false, 34), "name", [], "any", false, false, false, 34));
                // line 35
                echo "
                ";
                // line 36
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["translationsLocales"]);
                foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
                    // line 37
                    echo "                    ";
                    $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 37), "name", [], "any", false, false, false, 37);
                    // line 38
                    echo "
                    <li class=\"nav-item";
                    // line 39
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 39), "locale", [], "any", false, false, false, 39) == ($context["locale"] ?? null))) {
                        echo " active";
                    }
                    echo "\">
                        <a href=\"#\" data-toggle=\"tab\" data-target=\".oro_translationsFields-";
                    // line 40
                    echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
                    echo "\" class=\"nav-link\">
                            ";
                    // line 41
                    echo twig_escape_filter($this->env, twig_capitalize_string_filter($this->env, ($context["locale"] ?? null)), "html", null, true);
                    echo " ";
                    if (($context["isDefaultLocale"] ?? null)) {
                        echo "[Default]";
                    }
                    // line 42
                    echo "                        </a>
                    </li>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 45
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsLocales'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 46
            echo "            </ul>

            <div class=\"oro_translationsFields tab-content\">
            ";
            // line 49
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["form"] ?? null));
            foreach ($context['_seq'] as $context["_key"] => $context["translationsLocales"]) {
                // line 50
                echo "                ";
                $context['_parent'] = $context;
                $context['_seq'] = twig_ensure_traversable($context["translationsLocales"]);
                foreach ($context['_seq'] as $context["_key"] => $context["translationsFields"]) {
                    // line 51
                    echo "                    ";
                    $context["locale"] = Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, $context["translationsFields"], "vars", [], "any", false, false, false, 51), "name", [], "any", false, false, false, 51);
                    // line 52
                    echo "
                    <div class=\"oro_translationsFields-";
                    // line 53
                    echo twig_escape_filter($this->env, ($context["locale"] ?? null), "html", null, true);
                    echo " tab-pane ";
                    if ((Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, Oro\Bundle\EntityExtendBundle\Twig\Node\GetAttrNode::attribute($this->env, $this->source, ($context["app"] ?? null), "request", [], "any", false, false, false, 53), "locale", [], "any", false, false, false, 53) == ($context["locale"] ?? null))) {
                        echo "active";
                    }
                    echo "\">
                        ";
                    // line 54
                    echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock($context["translationsFields"], 'widget');
                    echo "
                    </div>
                ";
                }
                $_parent = $context['_parent'];
                unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsFields'], $context['_parent'], $context['loop']);
                $context = array_intersect_key($context, $_parent) + $_parent;
                // line 57
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['translationsLocales'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 58
            echo "            </div>
        </div>
    ";
        }
    }

    // line 63
    public function block_oro_translationsForms_widget($context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 64
        echo "    ";
        $this->displayBlock("oro_translations_widget", $context, $blocks);
        echo "
";
    }

    public function getTemplateName()
    {
        return "@OroTranslation/default.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  251 => 64,  247 => 63,  240 => 58,  234 => 57,  225 => 54,  217 => 53,  214 => 52,  211 => 51,  206 => 50,  202 => 49,  197 => 46,  191 => 45,  183 => 42,  177 => 41,  173 => 40,  167 => 39,  164 => 38,  161 => 37,  157 => 36,  154 => 35,  151 => 34,  147 => 33,  143 => 31,  137 => 29,  134 => 28,  130 => 27,  124 => 23,  115 => 20,  107 => 19,  104 => 18,  101 => 17,  97 => 16,  92 => 13,  82 => 9,  78 => 8,  72 => 7,  69 => 6,  66 => 5,  62 => 4,  58 => 2,  54 => 1,  50 => 63,  47 => 62,  45 => 27,  42 => 26,  40 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroTranslation/default.html.twig", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/Resources/views/default.html.twig");
    }
}
