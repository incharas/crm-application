<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* @OroUIBundlePublic/js/tools/unload-messages-group.js */
class __TwigTemplate_d79ff2c28bdf73454c11ef92a5c00d7f extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->parent = false;

        $this->blocks = [
        ];
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        // line 1
        echo "define(function(require) {
    'use strict';
    const \$ = require('jquery');
    const _ = require('underscore');
    const __ = require('orotranslation/js/translator');
    const MultiUseResourceManager = require('./multi-use-resource-manager');

    const UnloadMessagesGroup = MultiUseResourceManager.extend({
        listen: {
            constructResource: function() {
                \$(window).on('beforeunload', this.onBeforeUnload);
            },
            disposeResource: function() {
                \$(window).off('beforeunload', this.onBeforeUnload);
            }
        },

        single: __('oro.ui.unload_message.single'),
        group_title: __('oro.ui.unload_message.group_title'),

        /**
         * @inheritdoc
         */
        constructor: function UnloadMessagesGroup(options) {
            if (options.single) {
                this.single = options.single;
            }
            if (options.group_title) {
                this.group_title = options.group_title;
            }
            this.onBeforeUnload = this.onBeforeUnload.bind(this);
            UnloadMessagesGroup.__super__.constructor.call(this, options);
        },

        /**
         * Window unload handler
         *
         * @returns {string|undefined}
         */
        onBeforeUnload: function() {
            const subMessages = _.countBy(this.holders, function(item) {
                if (_.isString(item)) {
                    return item;
                }
                return '';
            });
            const emptyDescriptionMessagesCount = subMessages[''] ? subMessages[''].length : 0;
            if (emptyDescriptionMessagesCount !== this.holders.length) {
                return this.group_title + ':\\n' + _.map(subMessages, function(count, message) {
                    return '  - ' + (message !== '' ? message : __('oro.ui.unload_message.other')) +
                        (count > 1 ? (' (' + count + ')') : '');
                }).join(':\\n');
            } else {
                return this.single;
            }
        }
    });

    return UnloadMessagesGroup;
});
";
    }

    public function getTemplateName()
    {
        return "@OroUIBundlePublic/js/tools/unload-messages-group.js";
    }

    public function getDebugInfo()
    {
        return array (  37 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("", "@OroUIBundlePublic/js/tools/unload-messages-group.js", "/websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/public/js/tools/unload-messages-group.js");
    }
}
