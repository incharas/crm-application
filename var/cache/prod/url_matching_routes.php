<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/' => [[['_route' => 'oro_default', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::viewAction'], null, null, null, false, false, null]],
        '/sync/ticket' => [[['_route' => 'oro_sync_ticket', '_controller' => 'Oro\\Bundle\\SyncBundle\\Controller\\TicketController::syncTicketAction'], null, ['POST' => 0], null, false, false, null]],
        '/platform/information' => [[['_route' => 'oro_platform_system_info', '_controller' => 'Oro\\Bundle\\PlatformBundle\\Controller\\PlatformController::systemInfoAction'], null, null, null, false, false, null]],
        '/organization/business_unit/create' => [[['_route' => 'oro_business_unit_create', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\BusinessUnitController::createAction'], null, null, null, false, false, null]],
        '/organization/update_current' => [[['_route' => 'oro_organization_update_current', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\OrganizationController::updateCurrentAction'], null, null, null, false, false, null]],
        '/message-queue/jobs' => [[['_route' => 'oro_message_queue_root_jobs', '_controller' => 'Oro\\Bundle\\MessageQueueBundle\\Controller\\JobController::rootJobsAction'], null, null, null, true, false, null]],
        '/email/check-smtp-connection' => [[['_route' => 'oro_email_check_smtp_connection', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::checkSmtpConnectionAction'], null, ['POST' => 0], null, false, false, null]],
        '/email/check-saved-smtp-connection' => [[['_route' => 'oro_email_check_saved_smtp_connection', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::checkSavedSmtpConnectionAction'], null, ['GET' => 0], null, false, false, null]],
        '/email/purge-emails-attachments' => [[['_route' => 'oro_email_purge_emails_attachments', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::purgeEmailsAttachmentsAction'], null, ['POST' => 0], null, false, false, null]],
        '/email/last' => [[['_route' => 'oro_email_last', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::lastAction'], null, null, null, false, false, null]],
        '/email/view-items' => [[['_route' => 'oro_email_items_view', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::itemsAction'], null, null, null, false, false, null]],
        '/email/create' => [[['_route' => 'oro_email_email_create', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::createAction'], null, null, null, false, false, 1]],
        '/email/widget' => [[['_route' => 'oro_email_widget_emails', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::emailsAction'], null, null, null, false, false, 1]],
        '/email/base-widget' => [[['_route' => 'oro_email_widget_base_emails', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::baseEmailsAction'], null, null, null, false, false, 1]],
        '/email/user-emails' => [[['_route' => 'oro_email_user_emails', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::userEmailsAction'], null, null, null, false, false, null]],
        '/email/user-sync-emails' => [[['_route' => 'oro_email_user_sync_emails', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::userEmailsSyncAction'], null, ['POST' => 0], null, false, false, null]],
        '/email/mark_all_as_seen' => [[['_route' => 'oro_email_mark_all_as_seen', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::markAllEmailsAsSeenAction'], null, ['POST' => 0], null, false, false, null]],
        '/email/autocomplete-recipient' => [[['_route' => 'oro_email_autocomplete_recipient', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::autocompleteRecipientAction'], null, null, null, false, false, null]],
        '/email/emailtemplate/create' => [[['_route' => 'oro_email_emailtemplate_create', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailTemplateController::createAction'], null, null, null, false, false, null]],
        '/email/emailorigin/list' => [[['_route' => 'oro_email_emailorigin_list', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\OriginController::listAction'], null, null, null, false, false, null]],
        '/config/mailbox/create' => [[['_route' => 'oro_email_mailbox_create', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Configuration\\MailboxController::createAction'], null, null, null, false, false, null]],
        '/cron/schedule' => [[['_route' => 'oro_cron_schedule_index', '_controller' => 'Oro\\Bundle\\CronBundle\\Controller\\ScheduleController::indexAction'], null, null, null, true, false, null]],
        '/login/check-google' => [
            [['_route' => 'oro_user_google_login'], null, null, null, false, false, null],
            [['_route' => 'oro_google_integration_sso_login_google'], null, null, null, false, false, null],
        ],
        '/user/group/create' => [[['_route' => 'oro_user_group_create', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\GroupController::createAction'], null, null, null, false, false, null]],
        '/user/send-email' => [[['_route' => 'oro_user_reset_send_email', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ResetController::sendEmailAction'], null, ['POST' => 0], null, false, false, null]],
        '/user/reset-request' => [[['_route' => 'oro_user_reset_request', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ResetController::requestAction'], null, ['GET' => 0], null, false, false, null]],
        '/user/mass-password-reset' => [[['_route' => 'oro_user_mass_password_reset', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ResetController::massPasswordResetAction'], null, null, null, true, false, null]],
        '/user/check-email' => [[['_route' => 'oro_user_reset_check_email', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ResetController::checkEmailAction'], null, ['GET' => 0], null, false, false, null]],
        '/user/role/create' => [[['_route' => 'oro_user_role_create', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\RoleController::createAction'], null, null, null, false, false, null]],
        '/user/login' => [[['_route' => 'oro_user_security_login', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\SecurityController::loginAction'], null, null, null, false, false, null]],
        '/user/login-check' => [[['_route' => 'oro_user_security_check', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\SecurityController::checkAction'], null, null, null, false, false, null]],
        '/user/logout' => [[['_route' => 'oro_user_security_logout', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\SecurityController::logoutAction'], null, null, null, false, false, null]],
        '/user/profile/view' => [[['_route' => 'oro_user_profile_view', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::viewProfileAction'], null, null, null, false, false, null]],
        '/user/profile/edit' => [[['_route' => 'oro_user_profile_update', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::updateProfileAction'], null, null, null, false, false, null]],
        '/user/create' => [[['_route' => 'oro_user_create', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::createAction'], null, null, null, false, false, null]],
        '/user/login-attempts' => [[['_route' => 'oro_user_login_attempts', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::loginAttemptsAction'], null, null, null, false, false, null]],
        '/api/userprofile' => [[['_route' => 'oro_rest_api_user_profile', 'entity' => 'userprofile', '_controller' => 'Oro\\Bundle\\ApiBundle\\Controller\\RestApiController::itemAction'], null, null, null, false, false, null]],
        '/translation' => [[['_route' => 'oro_translation_translation_index', '_controller' => 'Oro\\Bundle\\TranslationBundle\\Controller\\TranslationController::indexAction'], null, null, null, true, false, null]],
        '/language' => [[['_route' => 'oro_translation_language_index', '_controller' => 'Oro\\Bundle\\TranslationBundle\\Controller\\LanguageController::indexAction'], null, null, null, true, false, null]],
        '/login/check-office365' => [[['_route' => 'oro_microsoft_integration_sso_login_office365'], null, null, null, false, false, null]],
        '/locale/localization' => [[['_route' => 'oro_locale_localization_index', '_controller' => 'Oro\\Bundle\\LocaleBundle\\Controller\\LocalizationController::indexAction'], null, null, null, true, false, null]],
        '/locale/localization/create' => [[['_route' => 'oro_locale_localization_create', '_controller' => 'Oro\\Bundle\\LocaleBundle\\Controller\\LocalizationController::createAction'], null, null, null, false, false, null]],
        '/entity/config' => [[['_route' => 'oro_entityconfig_index', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::indexAction'], null, null, null, true, false, null]],
        '/entity/extend/entity/create' => [[['_route' => 'oro_entityextend_entity_create', '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigEntityGridController::createAction'], null, null, null, false, false, null]],
        '/cms/digital-asset' => [[['_route' => 'oro_digital_asset_index', '_controller' => 'Oro\\Bundle\\DigitalAssetBundle\\Controller\\DigitalAssetController::indexAction'], null, null, null, true, false, null]],
        '/cms/digital-asset/create' => [[['_route' => 'oro_digital_asset_create', '_controller' => 'Oro\\Bundle\\DigitalAssetBundle\\Controller\\DigitalAssetController::createAction'], null, null, null, false, false, null]],
        '/cms/digital-asset/widget/choose-image' => [[['_route' => 'oro_digital_asset_widget_choose_image', '_controller' => 'Oro\\Bundle\\DigitalAssetBundle\\Controller\\DigitalAssetController::chooseImageAction'], null, null, null, false, false, null]],
        '/cms/digital-asset/widget/choose-file' => [[['_route' => 'oro_digital_asset_widget_choose_file', '_controller' => 'Oro\\Bundle\\DigitalAssetBundle\\Controller\\DigitalAssetController::chooseFileAction'], null, null, null, false, false, null]],
        '/integration' => [[['_route' => 'oro_integration_index', '_controller' => 'Oro\\Bundle\\IntegrationBundle\\Controller\\IntegrationController::indexAction'], null, null, null, true, false, null]],
        '/integration/create' => [[['_route' => 'oro_integration_create', '_controller' => 'Oro\\Bundle\\IntegrationBundle\\Controller\\IntegrationController::createAction'], null, null, null, false, false, null]],
        '/import' => [[['_route' => 'oro_importexport_import_form', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::importFormAction'], null, null, null, false, false, null]],
        '/import_validate_export' => [[['_route' => 'oro_importexport_import_validate_export_template_form', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::importValidateExportTemplateFormAction'], null, null, null, false, false, null]],
        '/import-validate' => [[['_route' => 'oro_importexport_import_validation_form', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::importValidateFormAction'], null, null, null, false, false, null]],
        '/export/config' => [[['_route' => 'oro_importexport_export_config', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::configurableExportAction'], null, null, null, false, false, null]],
        '/export/template/config' => [[['_route' => 'oro_importexport_export_template_config', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::configurableTemplateExportAction'], null, null, null, false, false, null]],
        '/notification/email/create' => [[['_route' => 'oro_notification_emailnotification_create', '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\EmailNotificationController::createAction'], null, null, null, false, false, null]],
        '/notification/notification-alert' => [[['_route' => 'oro_notification_notificationalert_index', '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\NotificationAlertController::indexAction'], null, null, null, true, false, null]],
        '/actionwidget/buttons' => [[['_route' => 'oro_action_widget_buttons', '_controller' => 'Oro\\Bundle\\ActionBundle\\Controller\\WidgetController::buttonsAction'], null, null, null, false, false, null]],
        '/activity-list/segment/activity-condition' => [[['_route' => 'oro_activitylist_segment_activitycondition', '_controller' => 'Oro\\Bundle\\ActivityListBundle\\Controller\\SegmentController::activityConditionAction'], null, null, null, false, false, null]],
        '/embedded-form' => [[['_route' => 'oro_embedded_form_list', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbeddedFormController::indexAction'], null, null, null, true, false, null]],
        '/embedded-form/create' => [[['_route' => 'oro_embedded_form_create', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbeddedFormController::createAction'], null, null, null, false, false, null]],
        '/merge' => [[['_route' => 'oro_entity_merge', '_controller' => 'Oro\\Bundle\\EntityMergeBundle\\Controller\\MergeController::mergeAction'], null, null, null, false, false, null]],
        '/autocomplete/search' => [[['_route' => 'oro_form_autocomplete_search', '_controller' => 'Oro\\Bundle\\FormBundle\\Controller\\AutocompleteController::searchAction'], null, null, null, false, false, null]],
        '/connection/check' => [[['_route' => 'oro_imap_connection_check', '_controller' => 'Oro\\Bundle\\ImapBundle\\Controller\\ConnectionController::checkAction'], null, ['POST' => 0], null, false, false, null]],
        '/imap/connection/account/change' => [[['_route' => 'oro_imap_change_account_type', '_controller' => 'Oro\\Bundle\\ImapBundle\\Controller\\ConnectionController::getFormAction'], null, ['POST' => 0], null, false, false, null]],
        '/gmail/connection/access-token' => [[['_route' => 'oro_imap_gmail_access_token', '_controller' => 'Oro\\Bundle\\ImapBundle\\Controller\\GmailAccessTokenController::accessTokenAction'], null, ['POST' => 0], null, false, false, null]],
        '/gmail/connection/check' => [[['_route' => 'oro_imap_gmail_connection_check', '_controller' => 'Oro\\Bundle\\ImapBundle\\Controller\\CheckConnectionController::checkAction'], null, ['POST' => 0], null, false, false, null]],
        '/microsoft-identity/connection/access-token' => [[['_route' => 'oro_imap_microsoft_access_token', '_controller' => 'Oro\\Bundle\\ImapBundle\\Controller\\MicrosoftAccessTokenController::accessTokenAction'], null, ['POST' => 0, 'GET' => 1], null, false, false, null]],
        '/microsoft-identity/connection/check' => [[['_route' => 'oro_imap_microsoft_connection_check', '_controller' => 'Oro\\Bundle\\ImapBundle\\Controller\\CheckConnectionController::checkAction'], null, ['POST' => 0], null, false, false, null]],
        '/notes/create' => [[['_route' => 'oro_note_create', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\NoteController::createAction'], null, null, null, false, false, null]],
        '/report/create' => [[['_route' => 'oro_report_create', '_controller' => 'Oro\\Bundle\\ReportBundle\\Controller\\ReportController::createAction'], null, null, null, false, false, null]],
        '/search/search-bar' => [[['_route' => 'oro_search_bar', '_controller' => 'Oro\\Bundle\\SearchBundle\\Controller\\SearchController::searchBarAction'], null, null, null, false, false, null]],
        '/search/suggestion' => [[['_route' => 'oro_search_suggestion', '_controller' => 'Oro\\Bundle\\SearchBundle\\Controller\\SearchController::searchSuggestionAction'], null, null, null, false, false, null]],
        '/search' => [[['_route' => 'oro_search_results', '_controller' => 'Oro\\Bundle\\SearchBundle\\Controller\\SearchController::searchResultsAction'], null, null, null, true, false, null]],
        '/segment/create' => [[['_route' => 'oro_segment_create', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\SegmentController::createAction'], null, null, null, false, false, null]],
        '/tag/create' => [[['_route' => 'oro_tag_create', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TagController::createAction'], null, null, null, false, false, null]],
        '/taxonomy/create' => [[['_route' => 'oro_taxonomy_create', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TaxonomyController::createAction'], null, null, null, false, false, null]],
        '/processdefinition' => [[['_route' => 'oro_process_definition_index', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\ProcessDefinitionController::indexAction'], null, null, null, false, false, null]],
        '/workflowdefinition' => [[['_route' => 'oro_workflow_definition_index', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowDefinitionController::indexAction'], null, null, null, false, false, null]],
        '/workflowdefinition/create' => [[['_route' => 'oro_workflow_definition_create', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowDefinitionController::createAction'], null, null, null, false, false, null]],
        '/comments/form' => [[['_route' => 'oro_comment_form', '_controller' => 'Oro\\Bundle\\CommentBundle\\Controller\\CommentController::getFormAction'], null, null, null, false, false, null]],
        '/dashboard/create' => [[['_route' => 'oro_dashboard_create', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::createAction'], null, null, null, false, false, null]],
        '/dashboard/launchpad' => [[['_route' => 'oro_dashboard_quick_launchpad', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::quickLaunchpadAction'], null, null, null, false, false, null]],
        '/calendar/calendarevents/autocomplete/attendees' => [[['_route' => 'oro_calendarevent_autocomplete_attendees', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\AutocompleteController::autocompleteAttendeesAction'], null, null, null, false, false, null]],
        '/calendar/default' => [[['_route' => 'oro_calendar_view_default', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarController::viewDefaultAction'], null, null, null, false, false, null]],
        '/calendar/event' => [[['_route' => 'oro_calendar_event_index', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarEventController::indexAction'], null, null, null, false, false, null]],
        '/calendar/event/create' => [[['_route' => 'oro_calendar_event_create', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarEventController::createAction'], null, null, null, false, false, null]],
        '/system-calendar' => [[['_route' => 'oro_system_calendar_index', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarController::indexAction'], null, null, null, true, false, null]],
        '/system-calendar/create' => [[['_route' => 'oro_system_calendar_create', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarController::createAction'], null, null, null, false, false, null]],
        '/contact/create' => [[['_route' => 'oro_contact_create', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactController::createAction'], null, null, null, false, false, null]],
        '/contact/group/create' => [[['_route' => 'oro_contact_group_create', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\GroupController::createAction'], null, null, null, false, false, null]],
        '/menu/global' => [[['_route' => 'oro_navigation_global_menu_index', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalMenuController::indexAction'], null, null, null, true, false, null]],
        '/shortcutactionslist' => [[['_route' => 'oro_shortcut_actionslist', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\ShortcutController::actionslistAction'], null, null, null, false, false, null]],
        '/menu/user' => [[['_route' => 'oro_navigation_user_menu_index', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserMenuController::indexAction'], null, null, null, true, false, null]],
        '/marketing-list' => [[['_route' => 'oro_marketing_list_index', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\MarketingListController::indexAction'], null, null, null, true, false, null]],
        '/marketing-list/create' => [[['_route' => 'oro_marketing_list_create', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\MarketingListController::createAction'], null, null, null, false, false, null]],
        '/tracking/data/create' => [[['_route' => 'oro_tracking_data_create', '_controller' => 'Oro\\Bundle\\TrackingBundle\\Controller\\TrackingDataController::createAction'], null, null, null, false, false, null]],
        '/tracking/website/create' => [[['_route' => 'oro_tracking_website_create', '_controller' => 'Oro\\Bundle\\TrackingBundle\\Controller\\TrackingWebsiteController::createAction'], null, null, null, false, false, null]],
        '/account/create' => [[['_route' => 'oro_account_create', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\AccountController::createAction'], null, null, null, false, false, null]],
        '/call/create' => [[['_route' => 'oro_call_create', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::createAction'], null, null, null, false, false, null]],
        '/call' => [[['_route' => 'oro_call_index', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::indexAction'], null, null, null, true, false, null]],
        '/call/widget' => [[['_route' => 'oro_call_widget_calls', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::callsAction'], null, null, null, false, false, null]],
        '/call/base-widget' => [[['_route' => 'oro_call_base_widget_calls', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::baseCallsAction'], null, null, null, false, false, null]],
        '/campaign' => [[['_route' => 'oro_campaign_index', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\CampaignController::indexAction'], null, null, null, true, false, null]],
        '/campaign/create' => [[['_route' => 'oro_campaign_create', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\CampaignController::createAction'], null, null, null, false, false, null]],
        '/campaign/email' => [[['_route' => 'oro_email_campaign_index', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\EmailCampaignController::indexAction'], null, null, null, true, false, null]],
        '/campaign/email/create' => [[['_route' => 'oro_email_campaign_create', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\EmailCampaignController::createAction'], null, null, null, false, false, null]],
        '/channel/create' => [[['_route' => 'oro_channel_create', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\ChannelController::createAction'], null, null, null, false, false, null]],
        '/task/my' => [[['_route' => 'oro_task_my_tasks', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskController::myTasksAction'], null, null, null, false, false, null]],
        '/task' => [[['_route' => 'oro_task_index', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskCrudController::indexAction'], null, null, null, true, false, null]],
        '/task/create' => [[['_route' => 'oro_task_create', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskCrudController::createAction'], null, null, null, false, false, null]],
        '/b2bcustomer/create' => [[['_route' => 'oro_sales_b2bcustomer_create', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::createAction'], null, null, null, false, false, null]],
        '/lead/create' => [[['_route' => 'oro_sales_lead_create', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::createAction'], null, null, null, false, false, null]],
        '/opportunity/create' => [[['_route' => 'oro_sales_opportunity_create', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\OpportunityController::createAction'], null, null, null, false, false, null]],
        '/contact-us' => [[['_route' => 'oro_contactus_request_index', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactRequestController::indexAction'], null, null, null, true, false, null]],
        '/contact-us/create' => [[['_route' => 'oro_contactus_request_create', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactRequestController::createAction'], null, null, null, false, false, null]],
        '/contact-reason' => [[['_route' => 'oro_contactus_reason_index', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactReasonController::indexAction'], null, null, null, true, false, null]],
        '/contact-reason/create' => [[['_route' => 'oro_contactus_reason_create', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactReasonController::createAction'], null, null, null, false, false, null]],
        '/case' => [[['_route' => 'oro_case_index', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CaseController::indexAction'], null, null, null, true, false, null]],
        '/case/create' => [[['_route' => 'oro_case_create', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CaseController::createAction'], null, null, null, false, false, null]],
        '/dotmailer/address-book/create' => [[['_route' => 'oro_dotmailer_address_book_create', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\AddressBookController::createAction'], null, null, null, false, false, null]],
        '/dotmailer/data-field/create' => [[['_route' => 'oro_dotmailer_datafield_create', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldController::createAction'], null, null, null, false, false, null]],
        '/dotmailer/data-field/synchronize' => [[['_route' => 'oro_dotmailer_datafield_synchronize', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldController::synchronizeAction'], null, ['POST' => 0], null, false, false, null]],
        '/dotmailer/data-field-mapping/create' => [[['_route' => 'oro_dotmailer_datafield_mapping_create', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldMappingController::createAction'], null, null, null, false, false, null]],
        '/dotmailer/dotmailer/ping' => [[['_route' => 'oro_dotmailer_ping', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DotmailerController::pingAction'], null, null, null, false, false, null]],
        '/dotmailer/oauth/callback' => [[['_route' => 'oro_dotmailer_oauth_callback', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\OauthController::callbackAction'], null, null, ['https' => 0], false, false, null]],
        '/oauth2-token' => [
            [['_route' => 'oro_oauth2_server_auth_token', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\AuthorizationTokenController::tokenAction'], null, ['POST' => 0], null, false, false, null],
            [['_route' => 'oro_oauth2_server_auth_token_options', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\AuthorizationTokenController::optionsAction'], null, ['OPTIONS' => 0], null, false, false, null],
        ],
        '/oauth2' => [[['_route' => 'oro_oauth2_index', 'type' => 'backoffice', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::indexAction'], null, null, null, true, false, null]],
        '/oauth2/frontend' => [[['_route' => 'oro_oauth2_frontend_index', 'type' => 'frontend', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::indexAction'], null, null, null, false, false, null]],
        '/oauth2/create' => [[['_route' => 'oro_oauth2_create', 'type' => 'backoffice', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::createAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/oauth2/create/frontend' => [[['_route' => 'oro_oauth2_frontend_create', 'type' => 'frontend', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::createAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/oauth2/create/client' => [[['_route' => 'oro_oauth2_server_client_create', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::createClientAction'], null, ['GET' => 0, 'POST' => 1], null, false, false, null]],
        '/oauth2-token/login-check' => [[['_route' => 'oro_oauth2_server_login_check', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\LoginController::checkAction'], null, null, null, false, false, null]],
        '/oauth2-token/login' => [[['_route' => 'oro_oauth2_server_login_form', 'type' => 'backoffice', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\LoginController::loginAction'], null, null, null, false, false, null]],
        '/oauth2-token/authorize' => [[['_route' => 'oro_oauth2_server_authenticate', 'type' => 'backoffice', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\AuthorizeClientController::authorizeAction'], null, null, null, false, false, null]],
        '/view-switcher/get-application-url' => [[['_route' => 'oro_view_switcher_frontend_get_application_url', '_controller' => 'Oro\\Bundle\\ViewSwitcherBundle\\Controller\\Frontend\\AjaxApplicationUrlController::getApplicationUrl'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/o(?'
                    .'|rganization/business_unit(?'
                        .'|/(?'
                            .'|view/(\\d+)(*:54)'
                            .'|search/(\\d+)(*:73)'
                            .'|update(?:/(\\d+))?(*:97)'
                        .')'
                        .'|(?:/(html|json))?(*:122)'
                        .'|/widget/(?'
                            .'|info/(\\d+)(*:151)'
                            .'|users/(\\d+)(*:170)'
                        .')'
                    .')'
                    .'|pportunity(?'
                        .'|/(?'
                            .'|view/(\\d+)(*:207)'
                            .'|info/(\\d+)(*:225)'
                            .'|update(?:/(\\d+))?(*:250)'
                        .')'
                        .'|(?:/(html|json))?(*:276)'
                        .'|/create/([^/]++)(?'
                            .'|(*:303)'
                            .'|/([^/]++)(*:320)'
                        .')'
                    .')'
                    .'|auth2/(?'
                        .'|(\\d+)(*:344)'
                        .'|frontend/(\\d+)(*:366)'
                        .'|update/(?'
                            .'|(\\d+)(*:389)'
                            .'|frontend/(\\d+)(*:411)'
                            .'|client/(\\d+)(*:431)'
                        .')'
                        .'|de(?'
                            .'|lete/(\\d+)(*:455)'
                            .'|activate/(\\d+)(*:477)'
                        .')'
                        .'|activate/(\\d+)(*:500)'
                    .')'
                .')'
                .'|/a(?'
                    .'|pi/(?'
                        .'|rest/(?'
                            .'|(latest|v1)/businessunits(?:\\.(json))?(?'
                                .'|(*:570)'
                            .')'
                            .'|(latest|v1)/businessunits/(\\d+)(?:\\.(json))?(?'
                                .'|(*:626)'
                            .')'
                            .'|(latest|v1)/businessunits(?:\\.(json))?(*:673)'
                            .'|(latest|v1)/attachments/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:731)'
                            .')'
                            .'|(latest|v1)/files/(\\d+)(?:\\.(json|binary))?(*:783)'
                            .'|(latest|v1)/message\\-queue/job/interrupt/(\\d+)(?:\\.(json))?(*:850)'
                            .'|(latest|v1)/emails(?:\\.(json))?(*:889)'
                            .'|(latest|v1)/emails/(\\d+)(?:\\.(json))?(?'
                                .'|(*:937)'
                            .')'
                            .'|(latest|v1)/emails(?:\\.(json))?(?'
                                .'|(*:980)'
                            .')'
                            .'|(latest|v1)/autoresponserules/(\\d+)(?:\\.(json))?(*:1037)'
                            .'|(latest|v1)/emailorigins(?:\\.(json))?(*:1083)'
                            .'|(latest|v1)/emailorigins/(\\d+)(?:\\.(json))?(*:1135)'
                            .'|(latest|v1)/emailorigins(?:\\.(json))?(*:1181)'
                            .'|(latest|v1)/emailtemplates/(\\d+)(?:\\.(json))?(*:1235)'
                            .'|(latest|v1)/emailtemplates/list(?:/(\\w+)(?:/(\\d+)(?:/(\\d+)(?:\\.(json))?)?)?)?(*:1321)'
                            .'|(latest|v1)/emailtemplates/variables(?:\\.(json))?(*:1379)'
                            .'|(latest|v1)/emailtemplates/compiled/(\\d+)/(\\d*)(?:\\.(json))?(*:1448)'
                            .'|(latest|v1)/activities/emails/relations(?:\\.(json))?(*:1509)'
                            .'|(latest|v1)/activities/emails/(\\d+)/relations(?:\\.(json))?(*:1576)'
                            .'|(latest|v1)/activities/emails/relations/search(?:\\.(json))?(*:1644)'
                            .'|(latest|v1)/activities/emails/(\\d+)/suggestions(?:\\.(json))?(*:1713)'
                            .'|(latest|v1)/users(?:\\.(json))?(*:1752)'
                            .'|(latest|v1)/users/(\\d+)(?:\\.(json))?(*:1797)'
                            .'|(latest|v1)/users(?:\\.(json))?(*:1836)'
                            .'|(latest|v1)/users/(\\d+)(?:\\.(json))?(?'
                                .'|(*:1884)'
                            .')'
                            .'|(latest|v1)/users/(\\d+)/roles(?:\\.(json))?(*:1936)'
                            .'|(latest|v1)/users/(\\d+)/groups(?:\\.(json))?(*:1988)'
                            .'|(latest|v1)/user/filter(?:\\.(json))?(*:2033)'
                            .'|(latest|v1)/users(?:\\.(json))?(*:2072)'
                            .'|(latest|v1)/roles(?:\\.(json))?(*:2111)'
                            .'|(latest|v1)/roles/(\\d+)(?:\\.(json))?(*:2156)'
                            .'|(latest|v1)/roles(?:\\.(json))?(*:2195)'
                            .'|(latest|v1)/roles/(\\d+)(?:\\.(json))?(?'
                                .'|(*:2243)'
                            .')'
                            .'|(latest|v1)/roles/([^/]++)/byname(?:\\.(json))?(*:2299)'
                            .'|(latest|v1)/roles(?:\\.(json))?(*:2338)'
                            .'|(latest|v1)/groups(?:\\.(json))?(*:2378)'
                            .'|(latest|v1)/groups/(\\d+)(?:\\.(json))?(*:2424)'
                            .'|(latest|v1)/groups(?:\\.(json))?(*:2464)'
                            .'|(latest|v1)/groups/(\\d+)(?:\\.(json))?(?'
                                .'|(*:2513)'
                            .')'
                            .'|(latest|v1)/groups(?:\\.(json))?(*:2554)'
                            .'|(latest|v1)/users/(\\d+)/permissions(?:\\.(json))?(*:2611)'
                            .'|(latest|v1)/user/permissions(?:\\.(json))?(*:2661)'
                            .'|(latest|v1)/gridviews(?:\\.(json))?(*:2704)'
                            .'|(latest|v1)/gridviews/(\\d+)(?:\\.(json))?(?'
                                .'|(*:2756)'
                            .')'
                            .'|(latest|v1)/gridviews/(.+)/default/(\\d+)/gridName(?:/([^/\\.]++)(?:\\.(json))?)?(*:2844)'
                            .'|(latest|v1)/translations(?:\\.(json))?(*:2890)'
                            .'|(latest|v1)/translations/([^/]++)/([^/]++)/([^/]++)/patch(?:\\.(json))?(*:2969)'
                            .'|(latest|v1)/entities/fields(?:\\.(json))?(*:3018)'
                            .'|(latest|v1)/entities(?:\\.(json))?(*:3060)'
                            .'|(latest|v1)/entities/((?:(?:\\w+)_)+(?:\\w+))/fields(?:\\.(json))?(*:3132)'
                            .'|(latest|v1)/entities/aliases(?:\\.(json))?(*:3182)'
                            .'|(latest|v1)/entity_data/([^/]++)/([^/\\.]++)(?:\\.(json))?(*:3247)'
                            .'|(latest|v1)/emailnotications/(\\d+)(?:\\.(json))?(*:3303)'
                            .'|(latest|v1)/activitylist/([^/]++)/(\\d+)(?:\\.(json))?(*:3364)'
                            .'|(latest|v1)/activitylist/(\\d+)(?:\\.(json))?(*:3416)'
                            .'|(latest|v1)/activitylists/(\\d+)/activity/list/item(?:\\.(json))?(*:3488)'
                            .'|(latest|v1)/activitylists/([^/\\.]++)(?:\\.(json))?(*:3546)'
                            .'|(latest|v1)/activitylist/activity/list/option(?:\\.(json))?(*:3613)'
                            .'|(latest|v1)/addresstypes(?:\\.(json))?(*:3659)'
                            .'|(latest|v1)/addresstypes/([^/\\.]++)(?:\\.(json))?(*:3716)'
                            .'|(latest|v1)/countries(?:\\.(json))?(*:3759)'
                            .'|(latest|v1)/countries/([^/\\.]++)(?:\\.(json))?(*:3813)'
                            .'|(latest|v1)/region/([^/\\.]++)(?:\\.(json))?(*:3864)'
                            .'|(latest|v1)/country/regions/([^/\\.]++)(?:\\.(json))?(*:3924)'
                            .'|(latest|v1)/configuration(?:\\.(json))?(*:3971)'
                            .'|(latest|v1)/configuration/([\\w\\-]+[\\w\\-\\/]*)(?:\\.(json))?(*:4037)'
                            .'|(latest|v1)/audits(?:\\.(json))?(*:4077)'
                            .'|(latest|v1)/audits/(\\d+)(?:\\.(json))?(*:4123)'
                            .'|(latest|v1)/audit/fields(?:\\.(json))?(*:4169)'
                            .'|(latest|v1)/audits(?:\\.(json))?(*:4209)'
                            .'|(latest|v1)/autocomplete/search(?:\\.(json))?(*:4262)'
                            .'|(latest|v1)/notes/([^/]++)/(\\d+)(?:\\.(json|html))?(*:4321)'
                            .'|(latest|v1)/notes/(\\d+)(?:\\.(json|html))?(*:4371)'
                            .'|(latest|v1)/notes(?:\\.(json|html))?(*:4415)'
                            .'|(latest|v1)/notes/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:4468)'
                            .')'
                            .'|(latest|v1)/notes(?:\\.(json|html))?(*:4513)'
                            .'|(latest|v1)/querydesigner/entities/fields(?:\\.(json))?(*:4576)'
                            .'|(latest|v1)/reports/(\\d+)(?:\\.(json))?(*:4623)'
                            .'|(latest|v1)/search(?:\\.(json|html))?(*:4668)'
                            .'|(latest|v1)/search/advanced(?:\\.(json))?(*:4717)'
                            .'|(latest|v1)/segment/items(?:\\.(json))?(*:4764)'
                            .'|(latest|v1)/segments/(\\d+)(?:\\.(json))?(*:4812)'
                            .'|(latest|v1)/segments/(\\d+)/runs(?:\\.(json))?(*:4865)'
                            .'|(latest|v1)/sidebarwidgets/([^/\\.]++)(?:\\.(json))?(?'
                                .'|(*:4927)'
                            .')'
                            .'|(latest|v1)/sidebarwidgets(?:\\.(json))?(*:4976)'
                            .'|(latest|v1)/sidebarwidgets/(\\d+)(?:\\.(json))?(?'
                                .'|(*:5033)'
                            .')'
                            .'|(latest|v1)/sidebars/([^/\\.]++)(?:\\.(json))?(*:5087)'
                            .'|(latest|v1)/sidebars(?:\\.(json))?(*:5129)'
                            .'|(latest|v1)/sidebars/(\\d+)(?:\\.(json))?(*:5177)'
                            .'|(latest|v1)/tags/(\\d+)(?:\\.(json))?(*:5221)'
                            .'|(latest|v1)/taxonomies/(\\d+)(?:\\.(json))?(*:5271)'
                            .'|(latest|v1)/tags/([^/]++)/(\\d+)(?:\\.(json))?(*:5324)'
                            .'|(latest|v1)/windows(?:\\.(json))?(?'
                                .'|(*:5368)'
                            .')'
                            .'|(latest|v1)/windows/(\\d+)(?:\\.(json))?(?'
                                .'|(*:5419)'
                            .')'
                            .'|(latest|v1)/workflow/start/([^/]++)/([^/\\.]++)(?:\\.(json))?(*:5488)'
                            .'|(latest|v1)/workflow/transit/(\\d+)/([^/\\.]++)(?:\\.(json))?(*:5555)'
                            .'|(latest|v1)/workflow/(\\d+)(?:\\.(json))?(?'
                                .'|(*:5606)'
                            .')'
                            .'|(latest|v1)/workflow/activate/([^/\\.]++)(?:\\.(json))?(*:5669)'
                            .'|(latest|v1)/workflow/deactivate/([^/\\.]++)(?:\\.(json))?(*:5733)'
                            .'|(latest|v1)/workflowdefinition/([^/\\.]++)(?:\\.(json))?(?'
                                .'|(*:5799)'
                            .')'
                            .'|(latest|v1)/workflowdefinition(?:/([^/\\.]++)(?:\\.(json))?)?(*:5868)'
                            .'|(latest|v1)/workflowdefinition/([^/\\.]++)(?:\\.(json))?(*:5931)'
                            .'|(latest|v1)/workflowentity(?:\\.(json))?(*:5979)'
                            .'|(latest|v1)/process/activate/([^/\\.]++)(?:\\.(json))?(*:6040)'
                            .'|(latest|v1)/process/deactivate/([^/\\.]++)(?:\\.(json))?(*:6103)'
                            .'|(latest|v1)/relation/([^/]++)/(\\d+)/comment(?:\\.(json))?(?'
                                .'|(*:6171)'
                            .')'
                            .'|(latest|v1)/comment/(\\d+)(?:\\.(json))?(?'
                                .'|(*:6222)'
                            .')'
                            .'|(latest|v1)/comment/(\\d+)/removeAttachment(?:\\.(json))?(*:6287)'
                            .'|(latest|v1)/comment/(\\d+)(?:\\.(json))?(*:6334)'
                            .'|(latest|v1)/dashboards/(\\d+)/widgets/(\\d+)(?:\\.(json))?(?'
                                .'|(*:6401)'
                            .')'
                            .'|(latest|v1)/dashboards/(\\d+)/widget/positions(?:\\.(json))?(*:6469)'
                            .'|(latest|v1)/dashboards/widgets/adds/widgets(?:\\.(json))?(*:6534)'
                            .'|(latest|v1)/dashboards/(\\d+)(?:\\.(json))?(*:6584)'
                            .'|(latest|v1)/calendars/(\\d+)/connections(?:\\.(json))?(*:6645)'
                            .'|(latest|v1)/calendarconnections/(\\d+)(?:\\.(json))?(*:6704)'
                            .'|(latest|v1)/calendarconnections(?:\\.(json))?(*:6757)'
                            .'|(latest|v1)/calendarconnections/(\\d+)(?:\\.(json))?(*:6816)'
                            .'|(latest|v1)/calendar/connections(?:\\.(json))?(*:6870)'
                            .'|(latest|v1)/calendarevents(?:\\.(json))?(*:6918)'
                            .'|(latest|v1)/calendarevents/(\\d+)(?:\\.(json))?(*:6972)'
                            .'|(latest|v1)/calendars/(\\d+)/events/(\\d+)(?:\\.(json))?(*:7034)'
                            .'|(latest|v1)/calendarevents/(\\d+)(?:\\.(json))?(*:7088)'
                            .'|(latest|v1)/calendarevents(?:\\.(json))?(*:7136)'
                            .'|(latest|v1)/calendarevents/(\\d+)(?:\\.(json))?(*:7190)'
                            .'|(latest|v1)/calendarevents(?:\\.(json))?(*:7238)'
                            .'|(latest|v1)/calendars/default(?:\\.(json))?(*:7289)'
                            .'|(latest|v1)/systemcalendars/(\\d+)(?:\\.(json))?(*:7344)'
                            .'|(latest|v1)/systemcalendars(?:\\.(json))?(*:7393)'
                            .'|(latest|v1)/contacts(?:\\.(json|html))?(*:7440)'
                            .'|(latest|v1)/contacts/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:7496)'
                            .')'
                            .'|(latest|v1)/contacts(?:\\.(json|html))?(*:7544)'
                            .'|(latest|v1)/contacts/(\\d+)(?:\\.(json|html))?(*:7597)'
                            .'|(latest|v1)/contacts(?:\\.(json|html))?(*:7644)'
                            .'|(latest|v1)/contactgroups(?:\\.(json|html))?(*:7696)'
                            .'|(latest|v1)/contactgroups/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:7757)'
                            .')'
                            .'|(latest|v1)/contactgroups(?:\\.(json|html))?(*:7810)'
                            .'|(latest|v1)/contactgroups/(\\d+)(?:\\.(json|html))?(*:7868)'
                            .'|(latest|v1)/contactgroups(?:\\.(json|html))?(*:7920)'
                            .'|(latest|v1)/contacts/(\\d+)/addresses/(\\d+)(?:\\.(json|html))?(*:7989)'
                            .'|(latest|v1)/contacts/(\\d+)/addresses(?:\\.(json|html))?(*:8052)'
                            .'|(latest|v1)/contacts/(\\d+)/addresses/(\\d+)(?:\\.(json|html))?(*:8121)'
                            .'|(latest|v1)/contacts/(\\d+)/addresses/([^/]++)/by/type(?:\\.(json|html))?(*:8201)'
                            .'|(latest|v1)/contacts/(\\d+)/address/primary(?:\\.(json|html))?(*:8270)'
                            .'|(latest|v1)/contact/addresses(?:\\.(json|html))?(*:8326)'
                            .'|(latest|v1)/contacts/(\\d+)/phones(?:\\.(json|html))?(*:8386)'
                            .'|(latest|v1)/contacts/(\\d+)/phone/primary(?:\\.(json|html))?(*:8453)'
                            .'|(latest|v1)/contacts/phones(?:\\.(json|html))?(*:8507)'
                            .'|(latest|v1)/contacts/(\\d+)/phone(?:\\.(json|html))?(*:8566)'
                            .'|(latest|v1)/contact/phones(?:\\.(json|html))?(*:8619)'
                            .'|(latest|v1)/contacts/emails(?:\\.(json|html))?(*:8673)'
                            .'|(latest|v1)/contacts/(\\d+)/email(?:\\.(json|html))?(*:8732)'
                            .'|(latest|v1)/contact/emails(?:\\.(json|html))?(*:8785)'
                            .'|(latest|v1)/navigationitems/([^/\\.]++)(?:\\.(json))?(?'
                                .'|(*:8848)'
                            .')'
                            .'|(latest|v1)/navigationitems/([^/]++)/ids/(\\d+)(?:\\.(json))?(?'
                                .'|(*:8920)'
                            .')'
                            .'|(latest|v1)/shortcuts/([^/\\.]++)(?:\\.(json))?(*:8975)'
                            .'|(latest|v1)/pagestates(?:\\.(json))?(*:9019)'
                            .'|(latest|v1)/pagestates/(\\d+)(?:\\.(json))?(*:9069)'
                            .'|(latest|v1)/pagestates(?:\\.(json))?(*:9113)'
                            .'|(latest|v1)/pagestates/(\\d+)(?:\\.(json))?(?'
                                .'|(*:9166)'
                            .')'
                            .'|(latest|v1)/pagestate/checkid(?:\\.(json))?(*:9218)'
                            .'|(latest|v1)/marketinglist/contact\\-information/field/type(?:\\.(json|html))?(*:9302)'
                            .'|(latest|v1)/marketinglist/contact\\-information/entity/fields(?:\\.(json|html))?(*:9389)'
                            .'|(latest|v1)/marketinglists/(\\d+)(?:\\.(json|html))?(*:9448)'
                            .'|(latest|v1)/marketinglist/(\\d+)/remove/(\\d+)(?:\\.(json|html))?(*:9519)'
                            .'|(latest|v1)/marketinglist/(\\d+)/unremove/(\\d+)(?:\\.(json|html))?(*:9592)'
                            .'|(latest|v1)/marketinglists/removeditems(?:\\.(json|html))?(*:9658)'
                            .'|(latest|v1)/marketinglists/(\\d+)/removeditem(?:\\.(json|html))?(*:9729)'
                            .'|(latest|v1)/marketinglist/(\\d+)/unsubscribe/(\\d+)(?:\\.(json|html))?(*:9805)'
                            .'|(latest|v1)/marketinglist/(\\d+)/subscribe/(\\d+)(?:\\.(json|html))?(*:9879)'
                            .'|(latest|v1)/marketinglists/unsubscribeditems(?:\\.(json|html))?(*:9950)'
                            .'|(latest|v1)/marketinglists/(\\d+)/unsubscribeditem(?:\\.(json|html))?(*:10026)'
                            .'|(latest|v1)/marketinglists/(\\d+)/segments/runs(?:\\.(json|html))?(*:10100)'
                            .'|(latest|v1)/trackings/(\\d+)/website(?:\\.(json))?(*:10158)'
                            .'|(latest|v1)/accounts(?:\\.(json|html))?(*:10206)'
                            .'|(latest|v1)/accounts/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:10263)'
                            .')'
                            .'|(latest|v1)/accounts(?:\\.(json|html))?(*:10312)'
                            .'|(latest|v1)/accounts/(\\d+)(?:\\.(json|html))?(*:10366)'
                            .'|(latest|v1)/accounts(?:\\.(json|html))?(*:10414)'
                            .'|(latest|v1)/calls(?:\\.(json|html))?(*:10459)'
                            .'|(latest|v1)/calls/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:10513)'
                            .')'
                            .'|(latest|v1)/calls(?:\\.(json|html))?(*:10559)'
                            .'|(latest|v1)/calls/(\\d+)(?:\\.(json|html))?(*:10610)'
                            .'|(latest|v1)/calls(?:\\.(json|html))?(*:10655)'
                            .'|(latest|v1)/emailcampaigns/(\\d*)/email/templates(?:/(\\d+)(?:/(\\d+)(?:\\.(json|html))?)?)?(*:10753)'
                            .'|(latest|v1)/channels(?:\\.(json))?(*:10796)'
                            .'|(latest|v1)/channels/(\\d+)(?:\\.(json))?(*:10845)'
                            .'|(latest|v1)/channels(?:\\.(json))?(*:10888)'
                            .'|(latest|v1)/customers/search(?:\\.(json))?(*:10939)'
                            .'|(latest|v1)/search/customers(?:\\.(json))?(*:10990)'
                            .'|(latest|v1)/reminders/showns(?:\\.(json))?(*:11041)'
                            .'|(latest|v1)/tasks(?:\\.(json|html))?(*:11086)'
                            .'|(latest|v1)/tasks/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:11140)'
                            .')'
                            .'|(latest|v1)/tasks(?:\\.(json|html))?(*:11186)'
                            .'|(latest|v1)/tasks/(\\d+)(?:\\.(json|html))?(*:11237)'
                            .'|(latest|v1)/tasks(?:\\.(json|html))?(*:11282)'
                            .'|(latest|v1)/opportunities(?:\\.(json|html))?(*:11335)'
                            .'|(latest|v1)/opportunities/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:11397)'
                            .')'
                            .'|(latest|v1)/opportunities(?:\\.(json|html))?(*:11451)'
                            .'|(latest|v1)/opportunities/(\\d+)(?:\\.(json|html))?(*:11510)'
                            .'|(latest|v1)/opportunities(?:\\.(json|html))?(*:11563)'
                            .'|(latest|v1)/leads(?:\\.(json|html))?(*:11608)'
                            .'|(latest|v1)/leads/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:11662)'
                            .')'
                            .'|(latest|v1)/leads(?:\\.(json|html))?(*:11708)'
                            .'|(latest|v1)/leads/(\\d+)(?:\\.(json|html))?(*:11759)'
                            .'|(latest|v1)/leads(?:\\.(json|html))?(*:11804)'
                            .'|(latest|v1)/leads/(\\d+)/addresses(?:\\.(json|html))?(*:11865)'
                            .'|(latest|v1)/leads/(\\d+)/address/primary(?:\\.(json|html))?(*:11932)'
                            .'|(latest|v1)/leads/(\\d+)/addresses/(\\d+)(?:\\.(json|html))?(*:11999)'
                            .'|(latest|v1)/lead/addresses(?:\\.(json|html))?(*:12053)'
                            .'|(latest|v1)/b2bcustomers(?:\\.(json|html))?(*:12105)'
                            .'|(latest|v1)/b2bcustomers/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:12166)'
                            .')'
                            .'|(latest|v1)/b2bcustomers(?:\\.(json|html))?(*:12219)'
                            .'|(latest|v1)/b2bcustomers/(\\d+)(?:\\.(json|html))?(*:12277)'
                            .'|(latest|v1)/b2bcustomers(?:\\.(json|html))?(*:12329)'
                            .'|(latest|v1)/leads/phones(?:\\.(json|html))?(*:12381)'
                            .'|(latest|v1)/leads/(\\d+)/phone(?:\\.(json|html))?(*:12438)'
                            .'|(latest|v1)/lead/phones(?:\\.(json|html))?(*:12489)'
                            .'|(latest|v1)/leademails(?:\\.(json|html))?(*:12539)'
                            .'|(latest|v1)/leademails/(\\d+)(?:\\.(json|html))?(*:12595)'
                            .'|(latest|v1)/leademails(?:\\.(json|html))?(*:12645)'
                            .'|(latest|v1)/b2bcustomers/emails(?:\\.(json|html))?(*:12704)'
                            .'|(latest|v1)/b2bcustomers/(\\d+)/email(?:\\.(json|html))?(*:12768)'
                            .'|(latest|v1)/b2bcustomer/emails(?:\\.(json|html))?(*:12826)'
                            .'|(latest|v1)/b2bcustomers/(\\d+)/phones(?:\\.(json|html))?(*:12891)'
                            .'|(latest|v1)/b2bcustomers/(\\d+)/phone/primary(?:\\.(json|html))?(*:12963)'
                            .'|(latest|v1)/b2bcustomers/phones(?:\\.(json|html))?(*:13022)'
                            .'|(latest|v1)/b2bcustomers/(\\d+)/phone(?:\\.(json|html))?(*:13086)'
                            .'|(latest|v1)/b2bcustomer/phones(?:\\.(json|html))?(*:13144)'
                            .'|(latest|v1)/contactrequests/(\\d+)(?:\\.(json))?(*:13200)'
                            .'|(latest|v1)/cases(?:\\.(json|html))?(*:13245)'
                            .'|(latest|v1)/cases/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:13299)'
                            .')'
                            .'|(latest|v1)/cases(?:\\.(json|html))?(*:13345)'
                            .'|(latest|v1)/cases/(\\d+)(?:\\.(json|html))?(*:13396)'
                            .'|(latest|v1)/cases(?:\\.(json|html))?(*:13441)'
                            .'|(latest|v1)/case/(\\d+)/comments(?:\\.(json|html))?(*:13500)'
                            .'|(latest|v1)/case/comments/(\\d+)(?:\\.(json|html))?(?'
                                .'|(*:13562)'
                            .')'
                            .'|(latest|v1)/case/(\\d+)/comment(?:\\.(json|html))?(*:13621)'
                            .'|(latest|v1)/case/comments/(\\d+)(?:\\.(json|html))?(*:13680)'
                            .'|(latest|v1)/case/comments(?:\\.(json|html))?(*:13733)'
                            .'|(latest|v1)/ticket/sync/case/(\\d+)/channel/(\\d+)(?:\\.(json))?(*:13804)'
                            .'|(latest|v1)/dotmailers/(\\d+)/datafield(?:\\.(json|html))?(*:13870)'
                            .'|(latest|v1)/dotmailers/datafields/mappings/fields(?:\\.(json|html))?(*:13947)'
                            .'|(latest|v1)/dotmailers/(\\d+)/datafield/mapping(?:\\.(json|html))?(*:14021)'
                            .'|(latest|v1)/activities/targets(?:\\.(json))?(*:14074)'
                            .'|(latest|v1)/activities/targets/([^/\\.]++)(?:\\.(json))?(*:14138)'
                            .'|(latest|v1)/activities/targets/([^/]++)/([^/\\.]++)(?:\\.(json))?(*:14211)'
                            .'|(latest|v1)/activities/([^/]++)/(\\d+)/context(?:\\.(json))?(*:14279)'
                            .'|(latest|v1)/activities/([^/]++)/(\\d+)/relations(?:\\.(json))?(?'
                                .'|(*:14352)'
                            .')'
                            .'|(latest|v1)/activities/([^/]++)/(\\d+)/([^/]++)/([^/\\.]++)(?:\\.(json))?(*:14433)'
                            .'|(latest|v1)/activities/([^/]++)/relations/search(?:\\.(json))?(*:14504)'
                            .'|(latest|v1)/activities(?:\\.(json))?(*:14549)'
                            .'|(latest|v1)/activities/([^/\\.]++)(?:\\.(json))?(*:14605)'
                            .'|(latest|v1)/activities(?:\\.(json))?(*:14650)'
                            .'|(latest|v1)/([^/\\.]++)(?:\\.(json))?(*:14695)'
                        .')'
                        .'|doc(?'
                            .'|(?:/([\\w-]+))?(*:14726)'
                            .'|/([\\w-]+)/([^/]++)(*:14754)'
                        .')'
                        .'|((?:(?!(?:rest)(?:/|$))[^/]+))/([^/]++)(*:14804)'
                        .'|((?:(?!(?:rest)(?:/|$))[^/]+))(*:14844)'
                        .'|((?:(?!(?:rest)(?:/|$))[^/]+))/([^/]++)/([^/]++)(*:14902)'
                        .'|((?:(?!(?:rest)(?:/|$))[^/]+))/([^/]++)/relationships/([^/]++)(*:14974)'
                    .')'
                    .'|tt(?'
                        .'|achment/(?'
                            .'|view/widget/([^/]++)/([^/]++)(*:15030)'
                            .'|create/([^/]++)/([^/]++)(*:15064)'
                            .'|update/([^/]++)(*:15089)'
                            .'|((?:get|download))/(\\d+)/([^/]++)(*:15132)'
                        .')'
                        .'|ribute/(?'
                            .'|create/([^/]++)(*:15168)'
                            .'|save/([^/]++)(*:15191)'
                            .'|u(?'
                                .'|pdate/(\\d+)(*:15216)'
                                .'|nremove(?:/(\\d+))?(*:15244)'
                            .')'
                            .'|index/([^/]++)(*:15269)'
                            .'|remove(?:/(\\d+))?(*:15296)'
                            .'|family/(?'
                                .'|create/([^/]++)(*:15331)'
                                .'|update/([^/]++)(*:15356)'
                                .'|index/([^/]++)(*:15380)'
                                .'|delete/([^/]++)(*:15405)'
                                .'|view/(\\d+)(*:15425)'
                            .')'
                        .')'
                    .')'
                    .'|c(?'
                        .'|ti(?'
                            .'|onwidget/form/([^/]++)(*:15469)'
                            .'|vit(?'
                                .'|ies/(?'
                                    .'|view/([^/]++)(*:15505)'
                                    .'|([^/]++)/(?'
                                        .'|([^/]++)/context(*:15543)'
                                        .'|search/autocomplete(*:15572)'
                                    .')'
                                .')'
                                .'|y\\-(?'
                                    .'|contact/metrics/([^/]++)/([^/]++)(*:15623)'
                                    .'|list/view/widget/([^/]++)/([^/]++)(*:15667)'
                                .')'
                            .')'
                        .')'
                        .'|count(?'
                            .'|/(?'
                                .'|view/(\\d+)(*:15702)'
                                .'|update/(\\d+)(*:15724)'
                            .')'
                            .'|(?:/(html|json))?(*:15752)'
                            .'|/widget/(?'
                                .'|contacts(?:/(\\d+))?(*:15792)'
                                .'|info/(\\d+)(*:15812)'
                            .')'
                        .')'
                    .')'
                    .'|jax/operation/execute/([^/]++)(*:15855)'
                    .'|udit(?'
                        .'|(?:/(html|json))?(*:15889)'
                        .'|/history(?:/([a-zA-Z0-9_]+)(?:/([a-zA-Z0-9_-]+)(?:/([^/]++))?)?)?(*:15964)'
                    .')'
                .')'
                .'|/s(?'
                    .'|e(?'
                        .'|curity/(?'
                            .'|acl\\-access\\-levels/([\\w]+:[\\w\\:\\(\\)\\|]+)(?:/([\\w/]+))?(*:16050)'
                            .'|switch\\-organization(?:/(\\d+)(?:\\.(html|json))?)?(*:16109)'
                        .')'
                        .'|gment(?'
                            .'|(?:/(html|json))?(*:16145)'
                            .'|/(?'
                                .'|view/(\\d+)(*:16169)'
                                .'|update/(\\d+)(*:16191)'
                                .'|clone/(\\d+)(*:16212)'
                                .'|refresh/(\\d+)(*:16235)'
                            .')'
                        .')'
                    .')'
                    .'|ystem\\-calendar/(?'
                        .'|view/(\\d+)(*:16277)'
                        .'|update/(\\d+)(*:16299)'
                        .'|widget/(?'
                            .'|events/(\\d+)(*:16331)'
                            .'|info/(\\d+)(?:/(\\d+))?(*:16362)'
                        .')'
                        .'|event/view/(\\d+)(*:16389)'
                        .'|(\\d+)/event/create(*:16417)'
                        .'|event/update/(\\d+)(*:16445)'
                    .')'
                    .'|ales/customers/([^/]++)/search/autocomplete(*:16499)'
                .')'
                .'|/m(?'
                    .'|e(?'
                        .'|dia/(?'
                            .'|cache/(?'
                                .'|attachment/(?'
                                    .'|filter/([^/]++)/([0-9a-f]{32})/(\\d+)/([^/]++)(*:16594)'
                                    .'|resize/(\\d+)/(\\d+)/(\\d+)/([^/]++)(*:16637)'
                                .')'
                                .'|resolve/([a-z0-9_-]*)/(.+)(*:16674)'
                            .')'
                            .'|js/translation/([^/\\.]++)\\.json(*:16716)'
                            .'|([\\w-]+)/(.+)(*:16739)'
                            .'|js/routes\\.(json)(*:16766)'
                        .')'
                        .'|ssage\\-queue/jobs/(\\d+)(*:16800)'
                        .'|rge/([^/]++)/massAction/([^/]++)(*:16842)'
                        .'|nu/(?'
                            .'|global/(?'
                                .'|reset/([^/]++)(*:16882)'
                                .'|create/([^/]++)/([^/]++)(*:16916)'
                                .'|delete/([^/]++)/([^/]++)(*:16950)'
                                .'|show/([^/]++)/([^/]++)(*:16982)'
                                .'|hide/([^/]++)/([^/]++)(*:17014)'
                                .'|move/([^/]++)(*:17037)'
                                .'|([^/]++)(?'
                                    .'|(*:17058)'
                                    .'|/(?'
                                        .'|create(?:/([^/]++))?(*:17092)'
                                        .'|update/([^/]++)(*:17117)'
                                        .'|move(*:17131)'
                                    .')'
                                .')'
                            .')'
                            .'|user/(?'
                                .'|reset/([^/]++)(*:17166)'
                                .'|create/([^/]++)/([^/]++)(*:17200)'
                                .'|delete/([^/]++)/([^/]++)(*:17234)'
                                .'|show/([^/]++)/([^/]++)(*:17266)'
                                .'|hide/([^/]++)/([^/]++)(*:17298)'
                                .'|move/([^/]++)(*:17321)'
                                .'|([^/]++)(?'
                                    .'|(*:17342)'
                                    .'|/(?'
                                        .'|create(?:/([^/]++))?(*:17376)'
                                        .'|update/([^/]++)(*:17401)'
                                        .'|move(*:17415)'
                                    .')'
                                .')'
                            .')'
                        .')'
                    .')'
                    .'|arketing\\-(?'
                        .'|list/(?'
                            .'|view(?:/(\\d+))?(*:17466)'
                            .'|update(?:/(\\d+))?(*:17493)'
                        .')'
                        .'|activity/(?'
                            .'|widget/marketing\\-activities/(?'
                                .'|summary/(\\d+)(*:17561)'
                                .'|info/(\\d+)(*:17581)'
                            .')'
                            .'|view/widget/marketing\\-activities/(?'
                                .'|([^/]++)/([^/]++)(*:17646)'
                                .'|list/([^/]++)/([^/]++)(*:17678)'
                            .')'
                        .')'
                    .')'
                .')'
                .'|/e(?'
                    .'|m(?'
                        .'|ail/(?'
                            .'|a(?'
                                .'|utoresponserule/(?'
                                    .'|create(?:/([^/]++))?(*:17751)'
                                    .'|update/(\\d+)(*:17773)'
                                .')'
                                .'|ctivity/view/([^/]++)/([^/]++)(*:17814)'
                                .'|ttachment(?'
                                    .'|/(?'
                                        .'|(\\d+)(*:17845)'
                                        .'|(\\d+)/link(*:17865)'
                                    .')'
                                    .'|s/(\\d+)(*:17883)'
                                .')'
                            .')'
                            .'|view(?'
                                .'|/(?'
                                    .'|(\\d+)(*:17911)'
                                    .'|thread/(\\d+)(*:17933)'
                                    .'|user\\-thread/(\\d+)(*:17961)'
                                .')'
                                .'|\\-group/(\\d+)(*:17985)'
                            .')'
                            .'|widget/(?'
                                .'|thread/(\\d+)(*:18018)'
                                .'|user\\-thread/(\\d+)(*:18046)'
                            .')'
                            .'|reply(?'
                                .'|/(\\d+)(*:18071)'
                                .'|all/(\\d+)(*:18090)'
                            .')'
                            .'|forward/(\\d+)(*:18114)'
                            .'|body/(\\d+)(*:18134)'
                            .'|m(?'
                                .'|edia/cache/email_attachment/resize/(\\d+)/(\\d+)/(\\d+)(*:18200)'
                                .'|ark\\-seen/(\\d+)/(\\d+)(?:/(\\d+))?(*:18242)'
                            .')'
                            .'|toggle\\-seen/(\\d+)(*:18271)'
                            .'|([^/]++)/massAction/([^/]++)(*:18309)'
                            .'|emailtemplate(?'
                                .'|(?:/(html|json))?(*:18352)'
                                .'|/(?'
                                    .'|update(?:/(\\d+))?(*:18383)'
                                    .'|clone(?:/(\\d+))?(*:18409)'
                                    .'|preview(?:/(\\d+))?(*:18437)'
                                .')'
                            .')'
                            .'|autoresponserule/template/([^/]++)(*:18483)'
                        .')'
                        .'|bedded\\-form/(?'
                            .'|su(?'
                                .'|bmit/([-\\d\\w]+)(*:18530)'
                                .'|ccess/([-\\d\\w]+)(*:18556)'
                            .')'
                            .'|de(?'
                                .'|lete/([-\\d\\w]+)(*:18587)'
                                .'|fault\\-data/([^/]++)(*:18617)'
                            .')'
                            .'|update/([-\\d\\w]+)(*:18645)'
                            .'|view/([-\\d\\w]+)(*:18670)'
                            .'|info/([-\\d\\w]+)(*:18695)'
                        .')'
                    .')'
                    .'|ntit(?'
                        .'|ies/(?'
                            .'|([^/]++)(*:18729)'
                            .'|de(?'
                                .'|tailed/([^/]++)/([^/]++)(?:/([^/]++))?(*:18782)'
                                .'|lete/([^/]++)/item/([^/]++)(*:18819)'
                            .')'
                            .'|relation/([^/]++)/([^/]++)(?:/([^/]++))?(*:18870)'
                            .'|view/([^/]++)/item/([^/]++)(*:18907)'
                            .'|update/([^/]++)/item(?:/([^/]++))?(*:18951)'
                        .')'
                        .'|y(?'
                            .'|/(?'
                                .'|config/(?'
                                    .'|audit(?'
                                        .'|(?:/([a-zA-Z0-9_]+)(?:/(\\d+)(?:/([^/]++))?)?)?(*:19034)'
                                        .'|_field(?:/([a-zA-Z0-9_]+)(?:/(\\d+)(?:/([^/]++))?)?)?(*:19096)'
                                    .')'
                                    .'|update/([^/]++)(*:19122)'
                                    .'|view/([^/]++)(*:19145)'
                                    .'|field(?'
                                        .'|s(?:/(\\d+))?(*:19175)'
                                        .'|/(?'
                                            .'|update/([^/]++)(*:19204)'
                                            .'|search(?:/([^/]++))?(*:19234)'
                                        .')'
                                    .')'
                                    .'|widget/(?'
                                        .'|info/([^/]++)(*:19269)'
                                        .'|unique_keys/([^/]++)(*:19299)'
                                        .'|entity_fields/([^/]++)(*:19331)'
                                    .')'
                                .')'
                                .'|extend/(?'
                                    .'|update(?:/([^/]++))?(*:19373)'
                                    .'|entity/(?'
                                        .'|un(?'
                                            .'|ique\\-key(?:/(\\d+))?(*:19418)'
                                            .'|remove(?:/(\\d+))?(*:19445)'
                                        .')'
                                        .'|remove(?:/(\\d+))?(*:19473)'
                                    .')'
                                    .'|field/(?'
                                        .'|create(?:/(\\d+))?(*:19510)'
                                        .'|u(?'
                                            .'|pdate(?:/(\\d+))?(*:19540)'
                                            .'|nremove(?:/(\\d+))?(*:19568)'
                                        .')'
                                        .'|remove(?:/(\\d+))?(*:19596)'
                                    .')'
                                .')'
                            .')'
                            .'|\\-pagination/(?'
                                .'|first/([^/]++)/([^/]++)/([^/]++)(*:19657)'
                                .'|previous/([^/]++)/([^/]++)/([^/]++)(*:19702)'
                                .'|next/([^/]++)/([^/]++)/([^/]++)(*:19743)'
                                .'|last/([^/]++)/([^/]++)/([^/]++)(*:19784)'
                            .')'
                        .')'
                    .')'
                    .'|xport/(?'
                        .'|instant/([^/]++)(*:19822)'
                        .'|template/([^/]++)(*:19849)'
                        .'|download/(\\d+)(*:19873)'
                    .')'
                .')'
                .'|/d(?'
                    .'|a(?'
                        .'|shboard(?'
                            .'|/recent_emails/([\\w\\-]+)(?:/(inbox|sent|new)(?:/(full|tab))?)?(*:19966)'
                            .'|(?:\\.(html|json))?(*:19994)'
                            .'|/(?'
                                .'|view(?:/(\\d+))?(*:20023)'
                                .'|c(?'
                                    .'|onfigure/(\\d+)(*:20051)'
                                    .'|ampaign_(?'
                                        .'|lead/chart/([\\w\\-]+)(*:20092)'
                                        .'|opportunity/chart/([\\w\\-]+)(*:20129)'
                                        .'|by_close_revenue/chart/([\\w\\-]+)(*:20171)'
                                    .')'
                                .')'
                                .'|update(?:/(\\d+))?(*:20200)'
                                .'|widget/([\\w\\-]+)/([\\w\\-]+)(?:/($|\\w+))?(*:20249)'
                                .'|itemized_(?'
                                    .'|widget/([\\w\\-]+)/(\\w+)/([\\w\\-]+)(*:20303)'
                                    .'|data_widget/([\\w\\-]+)/(\\w+)/([\\w\\-]+)(*:20350)'
                                .')'
                                .'|grid/([^/]++)/([\\w\\:-]+)(*:20385)'
                                .'|my_calendar/([\\w\\-]+)(*:20416)'
                                .'|opportunit(?'
                                    .'|ies_by_lead_source/chart/([\\w\\-]+)(*:20473)'
                                    .'|y_state/chart/([\\w\\-]+)(*:20506)'
                                .')'
                            .')'
                        .')'
                        .'|tagrid/(?'
                            .'|widget/([\\w\\:-]+)(*:20546)'
                            .'|([\\w\\:-]+)(*:20566)'
                            .'|([\\w\\:-]+)/export(*:20593)'
                            .'|([\\w\\:\\-]+)/massAction/([\\w\\-]+)(*:20635)'
                            .'|([^/]++)/filter\\-metadata(*:20670)'
                        .')'
                    .')'
                    .'|ictionary/([^/]++)/(?'
                        .'|search(*:20710)'
                        .'|values(*:20726)'
                    .')'
                    .'|otmailer/(?'
                        .'|address\\-book/(?'
                            .'|synchronize(?'
                                .'|/(\\d+)(*:20786)'
                                .'|_datafields/(\\d+)(*:20813)'
                            .')'
                            .'|marketing\\-list/(?'
                                .'|disconnect/(\\d+)(*:20859)'
                                .'|buttons/(\\d+)(*:20882)'
                            .')'
                            .'|widget/manage\\-connection/marketing\\-list/(\\d+)(*:20940)'
                        .')'
                        .'|d(?'
                            .'|ata\\-field(?'
                                .'|/(?'
                                    .'|view/(\\d+)(*:20982)'
                                    .'|info/(\\d+)(*:21002)'
                                .')'
                                .'|(?:/(html|json))?(*:21030)'
                                .'|\\-mapping(?'
                                    .'|(?:/(html|json))?(*:21069)'
                                    .'|/update/(\\d+)(*:21092)'
                                .')'
                            .')'
                            .'|otmailer/(?'
                                .'|email\\-campaign\\-status/(\\d+)(*:21145)'
                                .'|sync\\-status/(\\d+)(*:21173)'
                                .'|integration\\-connection(?:/(\\d+))?(*:21217)'
                            .')'
                        .')'
                        .'|oauth/disconnect/(\\d+)(*:21251)'
                    .')'
                .')'
                .'|/c(?'
                    .'|on(?'
                        .'|fig/(?'
                            .'|mailbox/(?'
                                .'|u(?'
                                    .'|pdate/([^/]++)(*:21309)'
                                    .'|sers/search/([^/]++)(*:21339)'
                                .')'
                                .'|delete/([^/]++)(*:21365)'
                            .')'
                            .'|user/(?'
                                .'|(\\d+)(?:/([^/]++)(?:/([^/]++))?)?(*:21417)'
                                .'|profile(?:/([^/]++)(?:/([^/]++))?)?(*:21462)'
                            .')'
                            .'|system(?:/([^/]++)(?:/([^/]++))?)?(*:21507)'
                        .')'
                        .'|tact(?'
                            .'|/(?'
                                .'|address\\-book/(\\d+)(*:21548)'
                                .'|(\\d+)/address\\-create(*:21579)'
                                .'|(\\d+)/address\\-update(?:/(\\d+))?(*:21621)'
                                .'|view/(\\d+)(*:21641)'
                                .'|info/(\\d+)(*:21661)'
                                .'|update/(\\d+)(*:21683)'
                            .')'
                            .'|(?:/(html|json))?(*:21711)'
                            .'|/(?'
                                .'|widget/account\\-contacts/(\\d+)(*:21755)'
                                .'|group(?'
                                    .'|/update(?:/(\\d+))?(*:21791)'
                                    .'|(?:/(html|json))?(*:21818)'
                                .')'
                            .')'
                            .'|\\-(?'
                                .'|us/(?'
                                    .'|view/(\\d+)(*:21851)'
                                    .'|info/(\\d+)(*:21871)'
                                    .'|update/(\\d+)(*:21893)'
                                    .'|delete/(\\d+)(*:21915)'
                                .')'
                                .'|reason/(?'
                                    .'|update/(\\d+)(*:21948)'
                                    .'|delete/(\\d+)(*:21970)'
                                .')'
                            .')'
                        .')'
                    .')'
                    .'|ms/digital\\-asset/(?'
                        .'|update/(\\d+)(*:22017)'
                        .'|widget/choose/([^/]++)/([^/]++)(*:22058)'
                    .')'
                    .'|a(?'
                        .'|l(?'
                            .'|endar/(?'
                                .'|event/(?'
                                    .'|a(?'
                                        .'|jax/(?'
                                            .'|a(?'
                                                .'|ccepted/(\\d+)(*:22123)'
                                                .'|ttendees\\-autocomplete\\-data/([^/]++)(*:22170)'
                                            .')'
                                            .'|tentative/(\\d+)(*:22196)'
                                            .'|declined/(\\d+)(*:22220)'
                                        .')'
                                        .'|ctivity/view/([^/]++)/([^/]++)(*:22261)'
                                    .')'
                                    .'|view/(\\d+)(*:22282)'
                                    .'|widget/info/(\\d+)(?:/(\\d+))?(*:22320)'
                                    .'|update/(\\d+)(*:22342)'
                                    .'|delete/(\\d+)(*:22364)'
                                .')'
                                .'|view/(\\d+)(*:22385)'
                            .')'
                            .'|l/(?'
                                .'|activity/view/([^/]++)/([^/]++)(*:22432)'
                                .'|update/(\\d+)(*:22454)'
                                .'|view/([^/]++)(*:22477)'
                                .'|widget/info/(\\d+)(?:/(\\d+))?(*:22515)'
                            .')'
                        .')'
                        .'|mpaign/(?'
                            .'|update(?:/(\\d+))?(*:22554)'
                            .'|view/([^/]++)(*:22577)'
                            .'|e(?'
                                .'|vent/plot/([^/]++)/([^/]++)(*:22618)'
                                .'|mail/(?'
                                    .'|update(?:/(\\d+))?(*:22653)'
                                    .'|view/(\\d+)(*:22673)'
                                    .'|send/(\\d+)(*:22693)'
                                .')'
                            .')'
                        .')'
                        .'|se/(?'
                            .'|view/(\\d+)(*:22722)'
                            .'|widget/(?'
                                .'|account\\-cases/(\\d+)(*:22762)'
                                .'|contact\\-cases/(\\d+)(*:22792)'
                            .')'
                            .'|update/(\\d+)(*:22815)'
                            .'|(\\d+)/comment/list(?:\\.(json))?(*:22856)'
                            .'|(\\d+)/widget/comment(*:22886)'
                            .'|(\\d+)/comment/create(*:22916)'
                            .'|comment/(\\d+)/update(*:22946)'
                        .')'
                    .')'
                    .'|hannel(?'
                        .'|(?:/(html|json))?(*:22984)'
                        .'|/(?'
                            .'|update/(\\d+)(*:23010)'
                            .'|view/(\\d+)(*:23030)'
                            .'|widget/info/(\\d+)(*:23057)'
                            .'|integration/(?'
                                .'|create/(\\w+)(?:/([^/]++))?(*:23108)'
                                .'|update/(\\d+)(*:23130)'
                            .')'
                            .'|dashboard/chart/([\\w_-]+)(*:23166)'
                        .')'
                    .')'
                    .'|ustomer/customer/grid\\-dialog/([^/]++)(*:23216)'
                .')'
                .'|/user(?'
                    .'|/(?'
                        .'|group(?'
                            .'|/update(?:/(\\d+))?(*:23265)'
                            .'|(?:/(html|json))?(*:23292)'
                        .')'
                        .'|se(?'
                            .'|nd\\-forced\\-password\\-reset\\-email/(\\d+)(*:23348)'
                            .'|t\\-password/(\\d+)(*:23375)'
                        .')'
                        .'|r(?'
                            .'|eset/(\\w+)(*:23400)'
                            .'|ole(?'
                                .'|/(?'
                                    .'|view/(\\d+)(*:23430)'
                                    .'|update(?:/(\\d+))?(*:23457)'
                                .')'
                                .'|(?:/(html|json))?(*:23485)'
                            .')'
                        .')'
                        .'|view/(\\d+)(*:23507)'
                        .'|apigen/(\\d+)(*:23529)'
                        .'|update(?:/(\\d+))?(*:23556)'
                    .')'
                    .'|(?:/(html|json))?(*:23584)'
                    .'|/widget/info/(\\d+)(*:23612)'
                .')'
                .'|/t(?'
                    .'|ra(?'
                        .'|nslation/([^/]++)/massAction/([^/]++)(*:23670)'
                        .'|cking/website(?'
                            .'|(?:\\.(html|json))?(*:23714)'
                            .'|/(?'
                                .'|update/(\\d+)(*:23740)'
                                .'|view/(\\d+)(*:23760)'
                            .')'
                        .')'
                    .')'
                    .'|a(?'
                        .'|g(?'
                            .'|(?:/(html|json))?(*:23798)'
                            .'|/(?'
                                .'|update(?:/(\\d+))?(*:23829)'
                                .'|search(?:/(\\d+))?(*:23856)'
                            .')'
                        .')'
                        .'|xonomy(?'
                            .'|(?:/(html|json))?(*:23894)'
                            .'|/(?'
                                .'|update(?:/(\\d+))?(*:23925)'
                                .'|view/(\\d+)(*:23945)'
                                .'|widget/info/(\\d+)(*:23972)'
                            .')'
                        .')'
                        .'|sk/(?'
                            .'|widget/(?'
                                .'|sidebar\\-tasks(?:/(\\d+))?(*:24025)'
                                .'|info/(\\d+)(*:24045)'
                            .')'
                            .'|activity/view/(\\w+)/(\\d+)(*:24081)'
                            .'|u(?'
                                .'|ser/(\\d+)(*:24104)'
                                .'|pdate/(\\d+)(*:24125)'
                            .')'
                            .'|view/(\\d+)(*:24146)'
                        .')'
                    .')'
                .')'
                .'|/l(?'
                    .'|ocale/localization/(?'
                        .'|view/(\\d+)(*:24196)'
                        .'|update/(\\d+)(*:24218)'
                    .')'
                    .'|ead(?'
                        .'|/(?'
                            .'|address\\-book/(\\d+)(*:24258)'
                            .'|(\\d+)/address\\-create(*:24289)'
                            .'|(\\d+)/address\\-update(?:/(\\d+))?(*:24331)'
                            .'|view/(\\d+)(*:24351)'
                            .'|info/(\\d+)(*:24371)'
                            .'|update(?:/(\\d+))?(*:24398)'
                        .')'
                        .'|(?:/(html|json))?(*:24426)'
                        .'|/(?'
                            .'|widget/account\\-leads/(\\d+)(*:24467)'
                            .'|c(?'
                                .'|reate/([^/]++)(*:24495)'
                                .'|onvert/(\\d+)(*:24517)'
                            .')'
                            .'|disqualify/(\\d+)(*:24544)'
                        .')'
                    .')'
                .')'
                .'|/i(?'
                    .'|ntegration/(?'
                        .'|update/(\\d+)(*:24588)'
                        .'|schedule/(\\d+)(*:24612)'
                    .')'
                    .'|mport(?'
                        .'|/(?'
                            .'|validate/([^/]++)(*:24652)'
                            .'|process/([^/]++)(*:24678)'
                        .')'
                        .'|_export/job\\-error\\-log/(\\d+)\\.log(*:24723)'
                    .')'
                .')'
                .'|/not(?'
                    .'|ification/(?'
                        .'|email(?'
                            .'|(?:/(html|json))?(*:24780)'
                            .'|/update(?:/(\\d+))?(*:24808)'
                        .')'
                        .'|massnotification(?'
                            .'|(?:/(html|json))?(*:24855)'
                            .'|/(?'
                                .'|view/(\\d+)(*:24879)'
                                .'|info/(\\d+)(*:24899)'
                            .')'
                        .')'
                    .')'
                    .'|es/(?'
                        .'|view/(?'
                            .'|widget/([^/]++)/([^/]++)(*:24950)'
                            .'|([^/]++)/([^/]++)(*:24977)'
                        .')'
                        .'|widget/info/(\\d+)(?:/(\\d+))?(*:25016)'
                        .'|update/(\\d+)(*:25038)'
                    .')'
                .')'
                .'|/report(?'
                    .'|/(?'
                        .'|view/(?'
                            .'|(\\d+)(*:25077)'
                            .'|([-\\w]+)(*:25095)'
                        .')'
                        .'|update/(\\d+)(*:25118)'
                        .'|clone/(\\d+)(*:25139)'
                    .')'
                    .'|(?:/(html|json))?(*:25167)'
                    .'|/static/(\\w+)/(\\w+)(?:/(html|json))?(*:25213)'
                .')'
                .'|/processdefinition/view/([^/]++)(*:25256)'
                .'|/workflow(?'
                    .'|widget/(?'
                        .'|entity\\-workflows/([^/]++)/([^/]++)(*:25323)'
                        .'|transition/(?'
                            .'|create/attributes/([^/]++)/([^/]++)(*:25382)'
                            .'|edit/attributes/([^/]++)/([^/]++)(*:25425)'
                        .')'
                        .'|buttons/([^/]++)/([^/]++)(*:25461)'
                    .')'
                    .'|/(?'
                        .'|start/([^/]++)/([^/]++)(*:25499)'
                        .'|transit/([^/]++)/([^/]++)(*:25534)'
                    .')'
                    .'|definition/(?'
                        .'|update/([^/]++)(*:25574)'
                        .'|configure/([^/]++)(*:25602)'
                        .'|view/([^/]++)(*:25625)'
                        .'|activate\\-form/([^/]++)(*:25658)'
                    .')'
                .')'
                .'|/b2bcustomer(?'
                    .'|(?:/(html|json))?(*:25702)'
                    .'|/(?'
                        .'|view/(\\d+)(*:25726)'
                        .'|widget/(?'
                            .'|info/(\\d+)(*:25756)'
                            .'|b2bcustomer(?'
                                .'|\\-(?'
                                    .'|leads/(\\d+)(*:25796)'
                                    .'|opportunities/(\\d+)(*:25825)'
                                    .'|info/(\\d+)/channel/(\\d+)(*:25859)'
                                .')'
                                .'|s\\-info/account/(\\d+)/channel/(\\d+)(*:25905)'
                            .')'
                        .')'
                        .'|update(?:/(\\d+))?(*:25934)'
                    .')'
                .')'
            .')/?$}sD',
    ],
    [ // $dynamicRoutes
        54 => [[['_route' => 'oro_business_unit_view', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\BusinessUnitController::viewAction'], ['id'], null, null, false, true, null]],
        73 => [[['_route' => 'oro_business_unit_search', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\BusinessUnitController::searchAction'], ['organizationId'], null, null, false, true, null]],
        97 => [[['_route' => 'oro_business_unit_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\BusinessUnitController::updateAction'], ['id'], null, null, false, true, null]],
        122 => [[['_route' => 'oro_business_unit_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\BusinessUnitController::indexAction'], ['_format'], null, null, false, true, null]],
        151 => [[['_route' => 'oro_business_unit_widget_info', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\BusinessUnitController::infoAction'], ['id'], null, null, false, true, null]],
        170 => [[['_route' => 'oro_business_unit_widget_users', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\BusinessUnitController::usersAction'], ['id'], null, null, false, true, null]],
        207 => [[['_route' => 'oro_sales_opportunity_view', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\OpportunityController::viewAction'], ['id'], null, null, false, true, null]],
        225 => [[['_route' => 'oro_sales_opportunity_info', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\OpportunityController::infoAction'], ['id'], null, null, false, true, null]],
        250 => [[['_route' => 'oro_sales_opportunity_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\OpportunityController::updateAction'], ['id'], null, null, false, true, null]],
        276 => [[['_route' => 'oro_sales_opportunity_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\OpportunityController::indexAction'], ['_format'], null, null, false, true, null]],
        303 => [[['_route' => 'oro_sales_opportunity_data_channel_aware_create', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\OpportunityController::opportunityWithDataChannelCreateAction'], ['channelIds'], null, null, false, true, null]],
        320 => [[['_route' => 'oro_sales_opportunity_customer_aware_create', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\OpportunityController::opportunityWithCustomerCreateAction'], ['targetClass', 'targetId'], null, null, false, true, null]],
        344 => [[['_route' => 'oro_oauth2_view', 'type' => 'backoffice', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::viewAction'], ['id'], null, null, false, true, null]],
        366 => [[['_route' => 'oro_oauth2_frontend_view', 'type' => 'frontend', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::viewAction'], ['id'], null, null, false, true, null]],
        389 => [[['_route' => 'oro_oauth2_update', 'type' => 'backoffice', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::updateAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        411 => [[['_route' => 'oro_oauth2_frontend_update', 'type' => 'frontend', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::updateAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        431 => [[['_route' => 'oro_oauth2_server_client_update', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::updateClientAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        455 => [[['_route' => 'oro_oauth2_server_client_delete', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        477 => [[['_route' => 'oro_oauth2_server_client_deactivate', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::deactivateAction'], ['id'], ['POST' => 0], null, false, true, null]],
        500 => [[['_route' => 'oro_oauth2_server_client_activate', '_controller' => 'Oro\\Bundle\\OAuth2ServerBundle\\Controller\\ClientController::activateAction'], ['id'], ['POST' => 0], null, false, true, null]],
        570 => [
            [['_route' => 'oro_api_get_businessunits', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\Api\\Rest\\BusinessUnitController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_post_businessunit', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\Api\\Rest\\BusinessUnitController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null],
        ],
        626 => [
            [['_route' => 'oro_api_put_businessunit', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\Api\\Rest\\BusinessUnitController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_get_businessunit', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\Api\\Rest\\BusinessUnitController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_businessunit', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\Api\\Rest\\BusinessUnitController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        673 => [[['_route' => 'oro_api_options_businessunits', '_controller' => 'Oro\\Bundle\\OrganizationBundle\\Controller\\Api\\Rest\\BusinessUnitController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        731 => [
            [['_route' => 'oro_api_get_attachment', '_controller' => 'Oro\\Bundle\\AttachmentBundle\\Controller\\Api\\Rest\\AttachmentController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_attachment', '_controller' => 'Oro\\Bundle\\AttachmentBundle\\Controller\\Api\\Rest\\AttachmentController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        783 => [[['_route' => 'oro_api_get_file', '_controller' => 'Oro\\Bundle\\AttachmentBundle\\Controller\\Api\\Rest\\FileController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        850 => [[['_route' => 'oro_api_message_queue_job_interrupt_root_job', '_controller' => 'Oro\\Bundle\\MessageQueueBundle\\Controller\\Api\\Rest\\JobController::interruptRootJobAction', 'version' => 'latest', '_format' => 'json'], ['version', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        889 => [[['_route' => 'oro_api_get_emails', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        937 => [
            [['_route' => 'oro_api_get_email', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_email', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        980 => [
            [['_route' => 'oro_api_post_email', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null],
            [['_route' => 'oro_api_options_emails', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null],
        ],
        1037 => [[['_route' => 'oro_api_delete_autoresponserule', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\AutoResponseRuleController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        1083 => [[['_route' => 'oro_api_get_emailorigins', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailOriginController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        1135 => [[['_route' => 'oro_api_get_emailorigin', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailOriginController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        1181 => [[['_route' => 'oro_api_options_emailorigins', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailOriginController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        1235 => [[['_route' => 'oro_api_delete_emailtemplate', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailTemplateController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        1321 => [[['_route' => 'oro_api_get_emailtemplates', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailTemplateController::cgetAction', 'entityName' => null, 'includeNonEntity' => false, 'includeSystemTemplates' => true, '_format' => 'json', 'version' => 'latest'], ['version', 'entityName', 'includeNonEntity', 'includeSystemTemplates', '_format'], ['GET' => 0], null, false, true, null]],
        1379 => [[['_route' => 'oro_api_get_emailtemplate_variables', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailTemplateController::getVariablesAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        1448 => [[['_route' => 'oro_api_get_emailtemplate_compiled', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailTemplateController::getCompiledAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', 'entityId', '_format'], ['GET' => 0], null, false, true, null]],
        1509 => [[['_route' => 'oro_api_get_email_activity_relations_by_filters', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailActivityController::cgetByFiltersAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        1576 => [[['_route' => 'oro_api_get_email_activity_relations', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailActivityEntityController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        1644 => [[['_route' => 'oro_api_get_email_search_relations', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailActivitySearchController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        1713 => [[['_route' => 'oro_api_get_activity_email_suggestions', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Api\\Rest\\EmailActivitySuggestionController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        1752 => [[['_route' => 'oro_api_get_users', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        1797 => [[['_route' => 'oro_api_get_user', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        1836 => [[['_route' => 'oro_api_post_user', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        1884 => [
            [['_route' => 'oro_api_put_user', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_user', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        1936 => [[['_route' => 'oro_api_get_user_roles', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::getRolesAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        1988 => [[['_route' => 'oro_api_get_user_groups', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::getGroupsAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        2033 => [[['_route' => 'oro_api_get_user_filter', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::getFilterAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        2072 => [[['_route' => 'oro_api_options_users', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        2111 => [[['_route' => 'oro_api_get_roles', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\RoleController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        2156 => [[['_route' => 'oro_api_get_role', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\RoleController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        2195 => [[['_route' => 'oro_api_post_role', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\RoleController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        2243 => [
            [['_route' => 'oro_api_put_role', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\RoleController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_role', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\RoleController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        2299 => [[['_route' => 'oro_api_get_role_byname', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\RoleController::getBynameAction', '_format' => 'json', 'version' => 'latest'], ['version', 'name', '_format'], ['GET' => 0], null, false, true, null]],
        2338 => [[['_route' => 'oro_api_options_roles', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\RoleController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        2378 => [[['_route' => 'oro_api_get_groups', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\GroupController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        2424 => [[['_route' => 'oro_api_get_group', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\GroupController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        2464 => [[['_route' => 'oro_api_post_group', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\GroupController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        2513 => [
            [['_route' => 'oro_api_put_group', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\GroupController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_group', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\GroupController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        2554 => [[['_route' => 'oro_api_options_groups', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\GroupController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        2611 => [[['_route' => 'oro_api_get_user_permissions', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserPermissionController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        2661 => [[['_route' => 'oro_api_options_user_permissions', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\Api\\Rest\\UserPermissionController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        2704 => [[['_route' => 'oro_datagrid_api_rest_gridview_post', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\Api\\Rest\\GridViewController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        2756 => [
            [['_route' => 'oro_datagrid_api_rest_gridview_put', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\Api\\Rest\\GridViewController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_datagrid_api_rest_gridview_delete', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\Api\\Rest\\GridViewController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        2844 => [[['_route' => 'oro_datagrid_api_rest_gridview_default', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\Api\\Rest\\GridViewController::defaultAction', 'default' => false, 'gridName' => null, '_format' => 'json', 'version' => 'latest'], ['version', 'id', 'default', 'gridName', '_format'], ['POST' => 0], null, false, true, null]],
        2890 => [[['_route' => 'oro_api_get_translations', '_controller' => 'Oro\\Bundle\\TranslationBundle\\Controller\\Api\\Rest\\TranslationController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        2969 => [[['_route' => 'oro_api_patch_translation', '_controller' => 'Oro\\Bundle\\TranslationBundle\\Controller\\Api\\Rest\\TranslationController::patchAction', '_format' => 'json', 'version' => 'latest'], ['version', 'locale', 'domain', 'key', '_format'], ['PATCH' => 0], null, false, true, null]],
        3018 => [[['_route' => 'oro_api_fields_entity', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\Api\\Rest\\EntityController::fieldsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        3060 => [[['_route' => 'oro_api_get_entities', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\Api\\Rest\\EntityController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        3132 => [[['_route' => 'oro_api_get_entity_fields', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\Api\\Rest\\EntityFieldController::getFieldsAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entityName', '_format'], ['GET' => 0], null, false, true, null]],
        3182 => [[['_route' => 'oro_api_get_entity_aliases', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\Api\\Rest\\EntityAliasController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        3247 => [[['_route' => 'oro_api_patch_entity_data', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\Api\\Rest\\EntityDataController::patchAction', '_format' => 'json', 'version' => 'latest'], ['version', 'className', 'id', '_format'], ['PATCH' => 0], null, false, true, null]],
        3303 => [[['_route' => 'oro_api_delete_emailnotication', '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\Api\\Rest\\EmailNotificationController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        3364 => [[['_route' => 'oro_activity_list_api_get_list', '_controller' => 'Oro\\Bundle\\ActivityListBundle\\Controller\\Api\\Rest\\ActivityListController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entityClass', 'entityId', '_format'], ['GET' => 0], null, false, true, null]],
        3416 => [[['_route' => 'oro_activity_list_api_get_item', '_controller' => 'Oro\\Bundle\\ActivityListBundle\\Controller\\Api\\Rest\\ActivityListController::getActivityListItemAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entityId', '_format'], ['GET' => 0], null, false, true, null]],
        3488 => [[['_route' => 'oro_api_get_activitylist_activity_list_item', '_controller' => 'Oro\\Bundle\\ActivityListBundle\\Controller\\Api\\Rest\\ActivityListController::getActivityListItemAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entityId', '_format'], ['GET' => 0], null, false, true, null]],
        3546 => [[['_route' => 'oro_api_get_activitylists', '_controller' => 'Oro\\Bundle\\ActivityListBundle\\Controller\\Api\\Rest\\ActivityListController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entityClass', '_format'], ['GET' => 0], null, false, true, null]],
        3613 => [[['_route' => 'oro_api_get_activitylist_activity_list_option', '_controller' => 'Oro\\Bundle\\ActivityListBundle\\Controller\\Api\\Rest\\ActivityListController::getActivityListOptionAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        3659 => [[['_route' => 'oro_api_get_addresstypes', '_controller' => 'Oro\\Bundle\\AddressBundle\\Controller\\Api\\Rest\\AddressTypeController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        3716 => [[['_route' => 'oro_api_get_addresstype', '_controller' => 'Oro\\Bundle\\AddressBundle\\Controller\\Api\\Rest\\AddressTypeController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'name', '_format'], ['GET' => 0], null, false, true, null]],
        3759 => [[['_route' => 'oro_api_get_countries', '_controller' => 'Oro\\Bundle\\AddressBundle\\Controller\\Api\\Rest\\CountryController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        3813 => [[['_route' => 'oro_api_get_country', '_controller' => 'Oro\\Bundle\\AddressBundle\\Controller\\Api\\Rest\\CountryController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        3864 => [[['_route' => 'oro_api_get_region', '_controller' => 'Oro\\Bundle\\AddressBundle\\Controller\\Api\\Rest\\RegionController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        3924 => [[['_route' => 'oro_api_country_get_regions', '_controller' => 'Oro\\Bundle\\AddressBundle\\Controller\\Api\\Rest\\CountryRegionsController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'country', '_format'], ['GET' => 0], null, false, true, null]],
        3971 => [[['_route' => 'oro_api_get_configurations', '_controller' => 'Oro\\Bundle\\ConfigBundle\\Controller\\Api\\Rest\\ConfigurationController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4037 => [[['_route' => 'oro_api_get_configuration', '_controller' => 'Oro\\Bundle\\ConfigBundle\\Controller\\Api\\Rest\\ConfigurationController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'path', '_format'], ['GET' => 0], null, false, true, null]],
        4077 => [[['_route' => 'oro_api_get_audits', '_controller' => 'Oro\\Bundle\\DataAuditBundle\\Controller\\Api\\Rest\\AuditController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4123 => [[['_route' => 'oro_api_get_audit', '_controller' => 'Oro\\Bundle\\DataAuditBundle\\Controller\\Api\\Rest\\AuditController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        4169 => [[['_route' => 'oro_api_get_audit_fields', '_controller' => 'Oro\\Bundle\\DataAuditBundle\\Controller\\Api\\Rest\\AuditController::getFieldsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4209 => [[['_route' => 'oro_api_options_audits', '_controller' => 'Oro\\Bundle\\DataAuditBundle\\Controller\\Api\\Rest\\AuditController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        4262 => [[['_route' => 'oro_api_autocomplete_search', '_controller' => 'Oro\\Bundle\\FormBundle\\Controller\\Api\\Rest\\AutocompleteController::searchAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4321 => [[['_route' => 'oro_api_get_notes', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\Api\\Rest\\NoteController::cgetAction', '_format' => 'json'], ['version', 'entityClass', 'entityId', '_format'], ['GET' => 0], null, false, true, null]],
        4371 => [[['_route' => 'oro_api_get_note', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\Api\\Rest\\NoteController::getAction', '_format' => 'json'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        4415 => [[['_route' => 'oro_api_post_note', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\Api\\Rest\\NoteController::postAction', '_format' => 'json'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        4468 => [
            [['_route' => 'oro_api_put_note', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\Api\\Rest\\NoteController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_note', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\Api\\Rest\\NoteController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        4513 => [[['_route' => 'oro_api_options_notes', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\Api\\Rest\\NoteController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        4576 => [[['_route' => 'oro_api_querydesigner_fields_entity', '_controller' => 'Oro\\Bundle\\QueryDesignerBundle\\Controller\\Api\\Rest\\QueryDesignerEntityController::fieldsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4623 => [[['_route' => 'oro_api_delete_report', '_controller' => 'Oro\\Bundle\\ReportBundle\\Controller\\Api\\Rest\\ReportController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        4668 => [[['_route' => 'oro_api_get_search', '_controller' => 'Oro\\Bundle\\SearchBundle\\Controller\\Api\\SearchController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4717 => [[['_route' => 'oro_api_get_search_advanced', '_controller' => 'Oro\\Bundle\\SearchBundle\\Controller\\Api\\SearchAdvancedController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4764 => [[['_route' => 'oro_api_get_segment_items', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\Api\\Rest\\SegmentController::getItemsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        4812 => [[['_route' => 'oro_api_delete_segment', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\Api\\Rest\\SegmentController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        4865 => [[['_route' => 'oro_api_post_segment_run', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\Api\\Rest\\SegmentController::postRunAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        4927 => [
            [['_route' => 'oro_api_cget_sidebarwidgets', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\WidgetController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'placement', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_get_sidebarwidgets', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\WidgetController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'placement', '_format'], ['GET' => 0], null, false, true, null],
        ],
        4976 => [[['_route' => 'oro_api_post_sidebarwidgets', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\WidgetController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        5033 => [
            [['_route' => 'oro_api_put_sidebarwidgets', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\WidgetController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'widgetId', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_sidebarwidgets', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\WidgetController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'widgetId', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        5087 => [[['_route' => 'oro_api_get_sidebars', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\SidebarController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'position', '_format'], ['GET' => 0], null, false, true, null]],
        5129 => [[['_route' => 'oro_api_post_sidebars', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\SidebarController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        5177 => [[['_route' => 'oro_api_put_sidebars', '_controller' => 'Oro\\Bundle\\SidebarBundle\\Controller\\Api\\Rest\\SidebarController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'stateId', '_format'], ['PUT' => 0], null, false, true, null]],
        5221 => [[['_route' => 'oro_api_delete_tag', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\Api\\Rest\\TagController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        5271 => [[['_route' => 'oro_api_delete_taxonomy', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\Api\\Rest\\TaxonomyController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        5324 => [[['_route' => 'oro_api_post_taggable', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\Api\\Rest\\TaggableController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entity', 'entityId', '_format'], ['POST' => 0], null, false, true, null]],
        5368 => [
            [['_route' => 'oro_api_cget_windows', '_controller' => 'Oro\\Bundle\\WindowsBundle\\Controller\\Api\\WindowsStateController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_get_windows', '_controller' => 'Oro\\Bundle\\WindowsBundle\\Controller\\Api\\WindowsStateController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_post_windows', '_controller' => 'Oro\\Bundle\\WindowsBundle\\Controller\\Api\\WindowsStateController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null],
        ],
        5419 => [
            [['_route' => 'oro_api_put_windows', '_controller' => 'Oro\\Bundle\\WindowsBundle\\Controller\\Api\\WindowsStateController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'windowId', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_windows', '_controller' => 'Oro\\Bundle\\WindowsBundle\\Controller\\Api\\WindowsStateController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'windowId', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        5488 => [[['_route' => 'oro_api_workflow_start', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowController::startAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowName', 'transitionName', '_format'], ['POST' => 0], null, false, true, null]],
        5555 => [[['_route' => 'oro_api_workflow_transit', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowController::transitAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowItemId', 'transitionName', '_format'], ['POST' => 0], null, false, true, null]],
        5606 => [
            [['_route' => 'oro_api_workflow_get', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowController::getAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowItemId', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_workflow_delete', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowController::deleteAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowItemId', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        5669 => [[['_route' => 'oro_api_workflow_activate', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowController::activateAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowDefinition', '_format'], ['POST' => 0], null, false, true, null]],
        5733 => [[['_route' => 'oro_api_workflow_deactivate', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowController::deactivateAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowDefinition', '_format'], ['POST' => 0], null, false, true, null]],
        5799 => [
            [['_route' => 'oro_api_workflow_definition_get', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowDefinitionController::getAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowDefinition', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_workflow_definition_put', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowDefinitionController::putAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowDefinition', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        5868 => [[['_route' => 'oro_api_workflow_definition_post', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowDefinitionController::postAction', 'version' => 'latest', '_format' => 'json', 'workflowDefinition' => null], ['version', 'workflowDefinition', '_format'], ['POST' => 0], null, false, true, null]],
        5931 => [[['_route' => 'oro_api_workflow_definition_delete', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\WorkflowDefinitionController::deleteAction', 'version' => 'latest', '_format' => 'json'], ['version', 'workflowDefinition', '_format'], ['DELETE' => 0], null, false, true, null]],
        5979 => [[['_route' => 'oro_api_workflow_entity_get', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\EntityController::getAction', 'version' => 'latest', '_format' => 'json'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        6040 => [[['_route' => 'oro_api_process_activate', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\ProcessController::activateAction', 'version' => 'latest', '_format' => 'json'], ['version', 'processDefinition', '_format'], ['POST' => 0], null, false, true, null]],
        6103 => [[['_route' => 'oro_api_process_deactivate', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\Api\\Rest\\ProcessController::deactivateAction', 'version' => 'latest', '_format' => 'json'], ['version', 'processDefinition', '_format'], ['POST' => 0], null, false, true, null]],
        6171 => [
            [['_route' => 'oro_api_comment_get_items', '_controller' => 'Oro\\Bundle\\CommentBundle\\Controller\\Api\\Rest\\CommentController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'relationClass', 'relationId', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_comment_create_item', '_controller' => 'Oro\\Bundle\\CommentBundle\\Controller\\Api\\Rest\\CommentController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', 'relationClass', 'relationId', '_format'], ['POST' => 0], null, false, true, null],
        ],
        6222 => [
            [['_route' => 'oro_api_comment_get_item', '_controller' => 'Oro\\Bundle\\CommentBundle\\Controller\\Api\\Rest\\CommentController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_comment_update_item', '_controller' => 'Oro\\Bundle\\CommentBundle\\Controller\\Api\\Rest\\CommentController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['POST' => 0], null, false, true, null],
        ],
        6287 => [[['_route' => 'oro_api_comment_remove_attachment_item', '_controller' => 'Oro\\Bundle\\CommentBundle\\Controller\\Api\\Rest\\CommentController::removeAttachmentAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        6334 => [[['_route' => 'oro_api_comment_delete_item', '_controller' => 'Oro\\Bundle\\CommentBundle\\Controller\\Api\\Rest\\CommentController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        6401 => [
            [['_route' => 'oro_api_put_dashboard_widget', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\Api\\Rest\\WidgetController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'dashboardId', 'widgetId', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_dashboard_widget', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\Api\\Rest\\WidgetController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'dashboardId', 'widgetId', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        6469 => [[['_route' => 'oro_api_put_dashboard_widget_positions', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\Api\\Rest\\WidgetController::putPositionsAction', '_format' => 'json', 'version' => 'latest'], ['version', 'dashboardId', '_format'], ['PUT' => 0], null, false, true, null]],
        6534 => [[['_route' => 'oro_api_post_dashboard_widget_add_widget', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\Api\\Rest\\WidgetController::postAddWidgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        6584 => [[['_route' => 'oro_api_delete_dashboard', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\Api\\Rest\\DashboardController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        6645 => [[['_route' => 'oro_api_get_calendar_connections', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarConnectionController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        6704 => [[['_route' => 'oro_api_put_calendar_connection', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarConnectionController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null]],
        6757 => [[['_route' => 'oro_api_post_calendar_connection', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarConnectionController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        6816 => [[['_route' => 'oro_api_delete_calendar_connection', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarConnectionController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        6870 => [[['_route' => 'oro_api_options_calendar_connections', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarConnectionController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        6918 => [[['_route' => 'oro_api_get_calendarevents', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarEventController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        6972 => [[['_route' => 'oro_api_get_calendarevent', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarEventController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        7034 => [[['_route' => 'oro_api_get_calendarevent_by_calendar', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarEventController::getByCalendarAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', 'eventId', '_format'], ['GET' => 0], null, false, true, null]],
        7088 => [[['_route' => 'oro_api_put_calendarevent', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarEventController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null]],
        7136 => [[['_route' => 'oro_api_post_calendarevent', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarEventController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        7190 => [[['_route' => 'oro_api_delete_calendarevent', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarEventController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        7238 => [[['_route' => 'oro_api_options_calendarevents', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarEventController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        7289 => [[['_route' => 'oro_api_get_calendar_default', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\CalendarController::getDefaultAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        7344 => [[['_route' => 'oro_api_delete_systemcalendar', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\SystemCalendarController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        7393 => [[['_route' => 'oro_api_options_systemcalendars', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Api\\Rest\\SystemCalendarController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        7440 => [[['_route' => 'oro_api_get_contacts', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        7496 => [
            [['_route' => 'oro_api_get_contact', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_contact', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        7544 => [[['_route' => 'oro_api_post_contact', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        7597 => [[['_route' => 'oro_api_delete_contact', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        7644 => [[['_route' => 'oro_api_options_contacts', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        7696 => [[['_route' => 'oro_api_get_contactgroups', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactGroupController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        7757 => [
            [['_route' => 'oro_api_get_contactgroup', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactGroupController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_contactgroup', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactGroupController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        7810 => [[['_route' => 'oro_api_post_contactgroup', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactGroupController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        7868 => [[['_route' => 'oro_api_delete_contactgroup', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactGroupController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        7920 => [[['_route' => 'oro_api_options_contactgroups', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactGroupController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        7989 => [[['_route' => 'oro_api_get_contact_address', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactAddressController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'contactId', 'addressId', '_format'], ['GET' => 0], null, false, true, null]],
        8052 => [[['_route' => 'oro_api_get_contact_addresses', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactAddressController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'contactId', '_format'], ['GET' => 0], null, false, true, null]],
        8121 => [[['_route' => 'oro_api_delete_contact_address', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactAddressController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'contactId', 'addressId', '_format'], ['DELETE' => 0], null, false, true, null]],
        8201 => [[['_route' => 'oro_api_get_contact_address_by_type', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactAddressController::getByTypeAction', '_format' => 'json', 'version' => 'latest'], ['version', 'contactId', 'typeName', '_format'], ['GET' => 0], null, false, true, null]],
        8270 => [[['_route' => 'oro_api_get_contact_address_primary', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactAddressController::getPrimaryAction', '_format' => 'json', 'version' => 'latest'], ['version', 'contactId', '_format'], ['GET' => 0], null, false, true, null]],
        8326 => [[['_route' => 'oro_api_options_contact_addresses', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactAddressController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        8386 => [[['_route' => 'oro_api_get_contact_phones', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactPhoneController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'contactId', '_format'], ['GET' => 0], null, false, true, null]],
        8453 => [[['_route' => 'oro_api_get_contact_phone_primary', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactPhoneController::getPrimaryAction', '_format' => 'json', 'version' => 'latest'], ['version', 'contactId', '_format'], ['GET' => 0], null, false, true, null]],
        8507 => [[['_route' => 'oro_api_post_contact_phone', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactPhoneController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        8566 => [[['_route' => 'oro_api_delete_contact_phone', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactPhoneController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        8619 => [[['_route' => 'oro_api_options_contact_phones', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactPhoneController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        8673 => [[['_route' => 'oro_api_post_contact_email', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactEmailController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        8732 => [[['_route' => 'oro_api_delete_contact_email', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactEmailController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        8785 => [[['_route' => 'oro_api_options_contact_emails', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\Api\\Rest\\ContactEmailController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        8848 => [
            [['_route' => 'oro_api_get_navigationitems', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\NavigationItemController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'type', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_post_navigationitems', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\NavigationItemController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', 'type', '_format'], ['POST' => 0], null, false, true, null],
        ],
        8920 => [
            [['_route' => 'oro_api_put_navigationitems_id', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\NavigationItemController::putIdAction', '_format' => 'json', 'version' => 'latest'], ['version', 'type', 'itemId', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_navigationitems_id', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\NavigationItemController::deleteIdAction', '_format' => 'json', 'version' => 'latest'], ['version', 'type', 'itemId', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        8975 => [[['_route' => 'oro_api_get_shortcuts', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\ShortcutsController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'query', '_format'], ['GET' => 0], null, false, true, null]],
        9019 => [[['_route' => 'oro_api_get_pagestates', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\PagestateController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        9069 => [[['_route' => 'oro_api_get_pagestate', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\PagestateController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        9113 => [[['_route' => 'oro_api_post_pagestate', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\PagestateController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        9166 => [
            [['_route' => 'oro_api_put_pagestate', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\PagestateController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
            [['_route' => 'oro_api_delete_pagestate', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\PagestateController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null],
        ],
        9218 => [[['_route' => 'oro_api_get_pagestate_checkid', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\Api\\PagestateController::getCheckidAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        9302 => [[['_route' => 'oro_api_contact_marketinglist_information_field_type', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListController::contactInformationFieldTypeAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        9389 => [[['_route' => 'oro_api_entity_marketinglist_contact_information_fields', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListController::entityContactInformationFieldsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        9448 => [[['_route' => 'oro_api_delete_marketinglist', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        9519 => [[['_route' => 'oro_api_remove_marketinglist_removeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListRemovedItemController::removeAction', '_format' => 'json', 'version' => 'latest'], ['version', 'marketingList', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        9592 => [[['_route' => 'oro_api_unremove_marketinglist_removeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListRemovedItemController::unremoveAction', '_format' => 'json', 'version' => 'latest'], ['version', 'marketingList', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        9658 => [[['_route' => 'oro_api_post_marketinglist_removeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListRemovedItemController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        9729 => [[['_route' => 'oro_api_delete_marketinglist_removeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListRemovedItemController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        9805 => [[['_route' => 'oro_api_unsubscribe_marketinglist_unsubscribeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListUnsubscribedItemController::unsubscribeAction', '_format' => 'json', 'version' => 'latest'], ['version', 'marketingList', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        9879 => [[['_route' => 'oro_api_subscribe_marketinglist_unsubscribeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListUnsubscribedItemController::subscribeAction', '_format' => 'json', 'version' => 'latest'], ['version', 'marketingList', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        9950 => [[['_route' => 'oro_api_post_marketinglist_unsubscribeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListUnsubscribedItemController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        10026 => [[['_route' => 'oro_api_delete_marketinglist_unsubscribeditem', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\MarketingListUnsubscribedItemController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        10100 => [[['_route' => 'oro_api_post_marketinglist_segment_run', '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\Api\\Rest\\SegmentController::postRunAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        10158 => [[['_route' => 'oro_api_delete_tracking_website', '_controller' => 'Oro\\Bundle\\TrackingBundle\\Controller\\Api\\Rest\\TrackingWebsiteController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        10206 => [[['_route' => 'oro_api_get_accounts', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\Api\\Rest\\AccountController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        10263 => [
            [['_route' => 'oro_api_get_account', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\Api\\Rest\\AccountController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_account', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\Api\\Rest\\AccountController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        10312 => [[['_route' => 'oro_api_post_account', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\Api\\Rest\\AccountController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        10366 => [[['_route' => 'oro_api_delete_account', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\Api\\Rest\\AccountController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        10414 => [[['_route' => 'oro_api_options_accounts', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\Api\\Rest\\AccountController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        10459 => [[['_route' => 'oro_api_get_calls', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\Api\\Rest\\CallController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        10513 => [
            [['_route' => 'oro_api_get_call', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\Api\\Rest\\CallController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_call', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\Api\\Rest\\CallController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        10559 => [[['_route' => 'oro_api_post_call', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\Api\\Rest\\CallController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        10610 => [[['_route' => 'oro_api_delete_call', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\Api\\Rest\\CallController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        10655 => [[['_route' => 'oro_api_options_calls', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\Api\\Rest\\CallController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        10753 => [[['_route' => 'oro_api_get_emailcampaign_email_templates', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\Api\\Rest\\EmailTemplateController::cgetAction', '_format' => 'json', 'includeNonEntity' => false, 'includeSystemTemplates' => true, 'version' => 'latest'], ['version', 'id', 'includeNonEntity', 'includeSystemTemplates', '_format'], ['GET' => 0], null, false, true, null]],
        10796 => [[['_route' => 'oro_api_get_channels', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\Api\\Rest\\ChannelController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        10845 => [[['_route' => 'oro_api_delete_channel', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\Api\\Rest\\ChannelController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        10888 => [[['_route' => 'oro_api_options_channels', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\Api\\Rest\\ChannelController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        10939 => [[['_route' => 'oro_api_get_search_customers', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\Api\\Rest\\CustomerSearchController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        10990 => [[['_route' => 'oro_api_options_search_customers', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\Api\\Rest\\CustomerSearchController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        11041 => [[['_route' => 'oro_api_post_reminder_shown', '_controller' => 'Oro\\Bundle\\ReminderBundle\\Controller\\Api\\Rest\\ReminderController::postShownAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        11086 => [[['_route' => 'oro_api_get_tasks', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\Api\\Rest\\TaskController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        11140 => [
            [['_route' => 'oro_api_get_task', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\Api\\Rest\\TaskController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_task', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\Api\\Rest\\TaskController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        11186 => [[['_route' => 'oro_api_post_task', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\Api\\Rest\\TaskController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        11237 => [[['_route' => 'oro_api_delete_task', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\Api\\Rest\\TaskController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        11282 => [[['_route' => 'oro_api_options_tasks', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\Api\\Rest\\TaskController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        11335 => [[['_route' => 'oro_api_get_opportunities', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\OpportunityController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        11397 => [
            [['_route' => 'oro_api_get_opportunity', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\OpportunityController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_opportunity', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\OpportunityController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        11451 => [[['_route' => 'oro_api_post_opportunity', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\OpportunityController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        11510 => [[['_route' => 'oro_api_delete_opportunity', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\OpportunityController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        11563 => [[['_route' => 'oro_api_options_opportunities', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\OpportunityController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        11608 => [[['_route' => 'oro_api_get_leads', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        11662 => [
            [['_route' => 'oro_api_get_lead', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_lead', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        11708 => [[['_route' => 'oro_api_post_lead', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        11759 => [[['_route' => 'oro_api_delete_lead', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        11804 => [[['_route' => 'oro_api_options_leads', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        11865 => [[['_route' => 'oro_api_get_lead_addresses', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadAddressController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'leadId', '_format'], ['GET' => 0], null, false, true, null]],
        11932 => [[['_route' => 'oro_api_get_lead_address_primary', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadAddressController::getPrimaryAction', '_format' => 'json', 'version' => 'latest'], ['version', 'leadId', '_format'], ['GET' => 0], null, false, true, null]],
        11999 => [[['_route' => 'oro_api_delete_lead_address', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadAddressController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'leadId', 'addressId', '_format'], ['DELETE' => 0], null, false, true, null]],
        12053 => [[['_route' => 'oro_api_options_lead_addresses', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadAddressController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        12105 => [[['_route' => 'oro_api_get_b2bcustomers', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        12166 => [
            [['_route' => 'oro_api_get_b2bcustomer', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_put_b2bcustomer', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        12219 => [[['_route' => 'oro_api_post_b2bcustomer', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        12277 => [[['_route' => 'oro_api_delete_b2bcustomer', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        12329 => [[['_route' => 'oro_api_options_b2bcustomers', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        12381 => [[['_route' => 'oro_api_post_lead_phone', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadPhoneController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        12438 => [[['_route' => 'oro_api_delete_lead_phone', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadPhoneController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        12489 => [[['_route' => 'oro_api_options_lead_phones', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadPhoneController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        12539 => [[['_route' => 'oro_api_post_leademail', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadEmailController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        12595 => [[['_route' => 'oro_api_delete_leademail', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadEmailController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        12645 => [[['_route' => 'oro_api_options_leademails', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\LeadEmailController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        12704 => [[['_route' => 'oro_api_post_b2bcustomer_email', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerEmailController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        12768 => [[['_route' => 'oro_api_delete_b2bcustomer_email', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerEmailController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        12826 => [[['_route' => 'oro_api_options_b2bcustomer_emails', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerEmailController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        12891 => [[['_route' => 'oro_api_get_b2bcustomer_phones', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerPhoneController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'customerId', '_format'], ['GET' => 0], null, false, true, null]],
        12963 => [[['_route' => 'oro_api_get_b2bcustomer_phone_primary', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerPhoneController::getPrimaryAction', '_format' => 'json', 'version' => 'latest'], ['version', 'customerId', '_format'], ['GET' => 0], null, false, true, null]],
        13022 => [[['_route' => 'oro_api_post_b2bcustomer_phone', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerPhoneController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        13086 => [[['_route' => 'oro_api_delete_b2bcustomer_phone', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerPhoneController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        13144 => [[['_route' => 'oro_api_options_b2bcustomer_phones', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Api\\Rest\\B2bCustomerPhoneController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        13200 => [[['_route' => 'oro_api_get_contactrequest', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\Api\\Rest\\ContactRequestController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        13245 => [[['_route' => 'oro_case_api_get_cases', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CaseController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        13299 => [
            [['_route' => 'oro_case_api_get_case', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CaseController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_case_api_put_case', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CaseController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        13345 => [[['_route' => 'oro_case_api_post_case', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CaseController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['POST' => 0], null, false, true, null]],
        13396 => [[['_route' => 'oro_case_api_delete_case', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CaseController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        13441 => [[['_route' => 'oro_case_api_options_cases', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CaseController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        13500 => [[['_route' => 'oro_case_api_get_comments', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CommentController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        13562 => [
            [['_route' => 'oro_case_api_get_comment', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CommentController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_case_api_put_comment', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CommentController::putAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['PUT' => 0], null, false, true, null],
        ],
        13621 => [[['_route' => 'oro_case_api_post_comment', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CommentController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['POST' => 0], null, false, true, null]],
        13680 => [[['_route' => 'oro_case_api_delete_comment', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CommentController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        13733 => [[['_route' => 'oro_case_api_options_comments', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\Api\\Rest\\CommentController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        13804 => [[['_route' => 'oro_api_post_ticket_sync_case', '_controller' => 'Oro\\Bundle\\ZendeskBundle\\Controller\\Api\\Rest\\TicketController::postSyncCaseAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', 'channelId', '_format'], ['POST' => 0], null, false, true, null]],
        13870 => [[['_route' => 'oro_api_delete_dotmailer_datafield', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\Api\\Rest\\DataFieldController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        13947 => [[['_route' => 'oro_api_fields_dotmailer_datafield_mapping', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\Api\\Rest\\DataFieldMappingController::fieldsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        14021 => [[['_route' => 'oro_api_delete_dotmailer_datafield_mapping', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\Api\\Rest\\DataFieldMappingController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'id', '_format'], ['DELETE' => 0], null, false, true, null]],
        14074 => [[['_route' => 'oro_api_get_activity_target_all_types', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityTargetController::getAllTypesAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        14138 => [[['_route' => 'oro_api_get_activity_target_activity_types', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityTargetController::getActivityTypesAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entity', '_format'], ['GET' => 0], null, false, true, null]],
        14211 => [[['_route' => 'oro_api_get_activity_target_activities', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityTargetController::getActivitiesAction', '_format' => 'json', 'version' => 'latest'], ['version', 'entity', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        14279 => [[['_route' => 'oro_api_get_activity_context', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityContextController::getAction', '_format' => 'json', 'version' => 'latest'], ['version', 'activity', 'id', '_format'], ['GET' => 0], null, false, true, null]],
        14352 => [
            [['_route' => 'oro_api_get_activity_relations', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityEntityController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'activity', 'id', '_format'], ['GET' => 0], null, false, true, null],
            [['_route' => 'oro_api_post_activity_relation', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityEntityController::postAction', '_format' => 'json', 'version' => 'latest'], ['version', 'activity', 'id', '_format'], ['POST' => 0], null, false, true, null],
        ],
        14433 => [[['_route' => 'oro_api_delete_activity_relation', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityEntityController::deleteAction', '_format' => 'json', 'version' => 'latest'], ['version', 'activity', 'id', 'entity', 'entityId', '_format'], ['DELETE' => 0], null, false, true, null]],
        14504 => [[['_route' => 'oro_api_get_activity_search_relations', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivitySearchController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'activity', '_format'], ['GET' => 0], null, false, true, null]],
        14549 => [[['_route' => 'oro_api_get_activity_types', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityController::getTypesAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['GET' => 0], null, false, true, null]],
        14605 => [[['_route' => 'oro_api_get_activity_target_types', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityController::getTargetTypesAction', '_format' => 'json', 'version' => 'latest'], ['version', 'activity', '_format'], ['GET' => 0], null, false, true, null]],
        14650 => [[['_route' => 'oro_api_options_activities', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\Api\\Rest\\ActivityController::optionsAction', '_format' => 'json', 'version' => 'latest'], ['version', '_format'], ['OPTIONS' => 0], null, false, true, null]],
        14695 => [[['_route' => 'oro_api_get_dictionary_values', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\Api\\Rest\\DictionaryController::cgetAction', '_format' => 'json', 'version' => 'latest'], ['version', 'dictionary', '_format'], ['GET' => 0], null, false, true, null]],
        14726 => [[['_route' => 'nelmio_api_doc_index', 'view' => 'rest_json_api', '_controller' => 'Oro\\Bundle\\ApiBundle\\Controller\\RestApiDocController::indexAction'], ['view'], null, null, false, true, null]],
        14754 => [[['_route' => 'oro_rest_api_doc_resource', '_controller' => 'Oro\\Bundle\\ApiBundle\\Controller\\RestApiDocController::resourceAction'], ['view', 'resource'], null, null, false, true, null]],
        14804 => [[['_route' => 'oro_rest_api_item', '_controller' => 'Oro\\Bundle\\ApiBundle\\Controller\\RestApiController::itemAction'], ['entity', 'id'], null, null, false, true, null]],
        14844 => [[['_route' => 'oro_rest_api_list', '_controller' => 'Oro\\Bundle\\ApiBundle\\Controller\\RestApiController::listAction'], ['entity'], null, null, false, true, null]],
        14902 => [[['_route' => 'oro_rest_api_subresource', '_controller' => 'Oro\\Bundle\\ApiBundle\\Controller\\RestApiController::subresourceAction'], ['entity', 'id', 'association'], null, null, false, true, null]],
        14974 => [[['_route' => 'oro_rest_api_relationship', '_controller' => 'Oro\\Bundle\\ApiBundle\\Controller\\RestApiController::relationshipAction'], ['entity', 'id', 'association'], null, null, false, true, null]],
        15030 => [[['_route' => 'oro_attachment_widget_attachments', '_controller' => 'Oro\\Bundle\\AttachmentBundle\\Controller\\AttachmentController::widgetAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        15064 => [[['_route' => 'oro_attachment_create', '_controller' => 'Oro\\Bundle\\AttachmentBundle\\Controller\\AttachmentController::createAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        15089 => [[['_route' => 'oro_attachment_update', '_controller' => 'Oro\\Bundle\\AttachmentBundle\\Controller\\AttachmentController::updateAction'], ['id'], null, null, false, true, null]],
        15132 => [[['_route' => 'oro_attachment_get_file', '_controller' => 'oro_attachment.controller.file::getFileAction'], ['action', 'id', 'filename'], null, null, false, true, null]],
        15168 => [[['_route' => 'oro_attribute_create', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeController::createAction'], ['alias'], null, null, false, true, null]],
        15191 => [[['_route' => 'oro_attribute_save', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeController::saveAction'], ['alias'], null, null, false, true, null]],
        15216 => [[['_route' => 'oro_attribute_update', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeController::updateAction'], ['id'], null, null, false, true, null]],
        15244 => [[['_route' => 'oro_attribute_unremove', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeController::unremoveAction'], ['id'], ['POST' => 0], null, false, true, null]],
        15269 => [[['_route' => 'oro_attribute_index', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeController::indexAction'], ['alias'], null, null, false, true, null]],
        15296 => [[['_route' => 'oro_attribute_remove', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeController::removeAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        15331 => [[['_route' => 'oro_attribute_family_create', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeFamilyController::createAction'], ['alias'], null, null, false, true, null]],
        15356 => [[['_route' => 'oro_attribute_family_update', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeFamilyController::updateAction'], ['id'], null, null, false, true, null]],
        15380 => [[['_route' => 'oro_attribute_family_index', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeFamilyController::indexAction'], ['alias'], null, null, false, true, null]],
        15405 => [[['_route' => 'oro_attribute_family_delete', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeFamilyController::deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        15425 => [[['_route' => 'oro_attribute_family_view', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AttributeFamilyController::viewAction'], ['id'], null, null, false, true, null]],
        15469 => [[['_route' => 'oro_action_widget_form', '_controller' => 'Oro\\Bundle\\ActionBundle\\Controller\\WidgetController::formAction'], ['operationName'], null, null, false, true, null]],
        15505 => [[['_route' => 'oro_activity_view_activities', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\ActivityController::activitiesAction'], ['entity'], null, null, false, true, null]],
        15543 => [[['_route' => 'oro_activity_context', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\ActivityController::contextAction'], ['activity', 'id'], null, null, false, false, null]],
        15572 => [[['_route' => 'oro_activity_form_autocomplete_search', '_controller' => 'Oro\\Bundle\\ActivityBundle\\Controller\\AutocompleteController::autocompleteAction'], ['activity'], null, null, false, false, null]],
        15623 => [[['_route' => 'oro_activity_contact_metrics', '_controller' => 'Oro\\Bundle\\ActivityContactBundle\\Controller\\ActivityContactController::metricsAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        15667 => [[['_route' => 'oro_activity_list_widget_activities', '_controller' => 'Oro\\Bundle\\ActivityListBundle\\Controller\\ActivityListController::widgetAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        15702 => [[['_route' => 'oro_account_view', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\AccountController::viewAction'], ['id'], null, null, false, true, null]],
        15724 => [[['_route' => 'oro_account_update', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\AccountController::updateAction'], ['id'], null, null, false, true, null]],
        15752 => [[['_route' => 'oro_account_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\AccountController::indexAction'], ['_format'], null, null, false, true, null]],
        15792 => [[['_route' => 'oro_account_widget_contacts_info', 'id' => 0, '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\AccountController::contactsInfoAction'], ['id'], null, null, false, true, null]],
        15812 => [[['_route' => 'oro_account_widget_info', '_controller' => 'Oro\\Bundle\\AccountBundle\\Controller\\AccountController::infoAction'], ['id'], null, null, false, true, null]],
        15855 => [[['_route' => 'oro_action_operation_execute', '_controller' => 'Oro\\Bundle\\ActionBundle\\Controller\\AjaxController::executeAction'], ['operationName'], ['POST' => 0], null, false, true, null]],
        15889 => [[['_route' => 'oro_dataaudit_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\DataAuditBundle\\Controller\\AuditController::indexAction'], ['_format'], null, null, false, true, null]],
        15964 => [[['_route' => 'oro_dataaudit_history', 'entity' => 'entity', 'id' => 0, '_format' => 'html', '_controller' => 'Oro\\Bundle\\DataAuditBundle\\Controller\\AuditController::historyAction'], ['entity', 'id', '_format'], null, null, false, true, null]],
        16050 => [[['_route' => 'oro_security_access_levels', '_format' => 'json', 'permission' => null, '_controller' => 'Oro\\Bundle\\SecurityBundle\\Controller\\AclPermissionController::aclAccessLevelsAction'], ['oid', 'permission'], null, null, false, true, null]],
        16109 => [[['_route' => 'oro_security_switch_organization', 'id' => '0', '_format' => 'html', '_controller' => 'Oro\\Bundle\\SecurityBundle\\Controller\\SwitchOrganizationController::switchOrganizationAction'], ['id', '_format'], null, null, false, true, null]],
        16145 => [[['_route' => 'oro_segment_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\SegmentController::indexAction'], ['_format'], null, null, false, true, null]],
        16169 => [[['_route' => 'oro_segment_view', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\SegmentController::viewAction'], ['id'], null, null, false, true, null]],
        16191 => [[['_route' => 'oro_segment_update', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\SegmentController::updateAction'], ['id'], null, null, false, true, null]],
        16212 => [[['_route' => 'oro_segment_clone', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\SegmentController::cloneAction'], ['id'], null, null, false, true, null]],
        16235 => [[['_route' => 'oro_segment_refresh', '_controller' => 'Oro\\Bundle\\SegmentBundle\\Controller\\SegmentController::refreshAction'], ['id'], null, null, false, true, null]],
        16277 => [[['_route' => 'oro_system_calendar_view', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarController::viewAction'], ['id'], null, null, false, true, null]],
        16299 => [[['_route' => 'oro_system_calendar_update', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarController::updateAction'], ['id'], null, null, false, true, null]],
        16331 => [[['_route' => 'oro_system_calendar_widget_events', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarController::eventsAction'], ['id'], null, null, false, true, null]],
        16362 => [[['_route' => 'oro_system_calendar_event_widget_info', 'renderContexts' => true, '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarEventController::infoAction'], ['id', 'renderContexts'], null, null, false, true, null]],
        16389 => [[['_route' => 'oro_system_calendar_event_view', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarEventController::viewAction'], ['id'], null, null, false, true, null]],
        16417 => [[['_route' => 'oro_system_calendar_event_create', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarEventController::createAction'], ['id'], null, null, false, false, null]],
        16445 => [[['_route' => 'oro_system_calendar_event_update', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\SystemCalendarEventController::updateAction'], ['id'], null, null, false, true, null]],
        16499 => [[['_route' => 'oro_sales_customers_form_autocomplete_search', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\AutocompleteController::autocompleteCustomersAction'], ['ownerClassAlias'], null, null, false, false, null]],
        16594 => [[['_route' => 'oro_filtered_attachment', '_controller' => 'oro_attachment.controller.file::getFilteredImageAction'], ['filter', 'filterMd5', 'id', 'filename'], null, null, false, true, null]],
        16637 => [[['_route' => 'oro_resize_attachment', '_controller' => 'oro_attachment.controller.file::getResizedAttachmentImageAction'], ['id', 'width', 'height', 'filename'], null, null, false, true, null]],
        16674 => [[['_route' => 'oro_imagine_filter', '_controller' => 'oro_attachment.controller.imagine:getFilteredImageAction'], ['filter', 'path'], ['GET' => 0], null, false, true, null]],
        16716 => [[['_route' => 'oro_translation_jstranslation', '_controller' => 'Oro\\Bundle\\TranslationBundle\\Controller\\JsTranslationController:indexAction'], ['_locale'], null, null, false, false, null]],
        16739 => [[['_route' => 'oro_gaufrette_public_file', '_controller' => 'Oro\\Bundle\\GaufretteBundle\\Controller\\PublicFileController::getPublicFileAction'], ['subDirectory', 'fileName'], null, null, false, true, null]],
        16766 => [[['_route' => 'oro_navigation_js_routing_js', '_controller' => 'fos_js_routing.controller:indexAction'], ['_format'], ['GET' => 0], null, false, true, null]],
        16800 => [[['_route' => 'oro_message_queue_child_jobs', '_controller' => 'Oro\\Bundle\\MessageQueueBundle\\Controller\\JobController::childJobsAction'], ['id'], null, null, false, true, null]],
        16842 => [[['_route' => 'oro_entity_merge_massaction', '_controller' => 'Oro\\Bundle\\EntityMergeBundle\\Controller\\MergeController::mergeMassActionAction'], ['gridName', 'actionName'], null, null, false, true, null]],
        16882 => [[['_route' => 'oro_navigation_global_menu_ajax_reset', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalAjaxMenuController::resetAction'], ['menuName'], ['DELETE' => 0], null, false, true, null]],
        16916 => [[['_route' => 'oro_navigation_global_menu_ajax_create', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalAjaxMenuController::createAction'], ['menuName', 'parentKey'], ['POST' => 0], null, false, true, null]],
        16950 => [[['_route' => 'oro_navigation_global_menu_ajax_delete', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalAjaxMenuController::deleteAction'], ['menuName', 'key'], ['DELETE' => 0], null, false, true, null]],
        16982 => [[['_route' => 'oro_navigation_global_menu_ajax_show', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalAjaxMenuController::showAction'], ['menuName', 'key'], ['PUT' => 0], null, false, true, null]],
        17014 => [[['_route' => 'oro_navigation_global_menu_ajax_hide', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalAjaxMenuController::hideAction'], ['menuName', 'key'], ['PUT' => 0], null, false, true, null]],
        17037 => [[['_route' => 'oro_navigation_global_menu_ajax_move', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalAjaxMenuController::moveAction'], ['menuName'], ['PUT' => 0], null, false, true, null]],
        17058 => [[['_route' => 'oro_navigation_global_menu_view', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalMenuController::viewAction'], ['menuName'], null, null, false, true, null]],
        17092 => [[['_route' => 'oro_navigation_global_menu_create', 'parentKey' => null, '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalMenuController::createAction'], ['menuName', 'parentKey'], null, null, false, true, null]],
        17117 => [[['_route' => 'oro_navigation_global_menu_update', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalMenuController::updateAction'], ['menuName', 'key'], null, null, false, true, null]],
        17131 => [[['_route' => 'oro_navigation_global_menu_move', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\GlobalMenuController::moveAction'], ['menuName'], null, null, false, false, null]],
        17166 => [[['_route' => 'oro_navigation_user_menu_ajax_reset', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserAjaxMenuController::resetAction'], ['menuName'], ['DELETE' => 0], null, false, true, null]],
        17200 => [[['_route' => 'oro_navigation_user_menu_ajax_create', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserAjaxMenuController::createAction'], ['menuName', 'parentKey'], ['POST' => 0], null, false, true, null]],
        17234 => [[['_route' => 'oro_navigation_user_menu_ajax_delete', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserAjaxMenuController::deleteAction'], ['menuName', 'key'], ['DELETE' => 0], null, false, true, null]],
        17266 => [[['_route' => 'oro_navigation_user_menu_ajax_show', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserAjaxMenuController::showAction'], ['menuName', 'key'], ['PUT' => 0], null, false, true, null]],
        17298 => [[['_route' => 'oro_navigation_user_menu_ajax_hide', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserAjaxMenuController::hideAction'], ['menuName', 'key'], ['PUT' => 0], null, false, true, null]],
        17321 => [[['_route' => 'oro_navigation_user_menu_ajax_move', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserAjaxMenuController::moveAction'], ['menuName'], ['PUT' => 0], null, false, true, null]],
        17342 => [[['_route' => 'oro_navigation_user_menu_view', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserMenuController::viewAction'], ['menuName'], null, null, false, true, null]],
        17376 => [[['_route' => 'oro_navigation_user_menu_create', 'parentKey' => null, '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserMenuController::createAction'], ['menuName', 'parentKey'], null, null, false, true, null]],
        17401 => [[['_route' => 'oro_navigation_user_menu_update', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserMenuController::updateAction'], ['menuName', 'key'], null, null, false, true, null]],
        17415 => [[['_route' => 'oro_navigation_user_menu_move', '_controller' => 'Oro\\Bundle\\NavigationBundle\\Controller\\UserMenuController::moveAction'], ['menuName'], null, null, false, false, null]],
        17466 => [[['_route' => 'oro_marketing_list_view', 'id' => 0, '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\MarketingListController::viewAction'], ['id'], null, null, false, true, null]],
        17493 => [[['_route' => 'oro_marketing_list_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\MarketingListBundle\\Controller\\MarketingListController::updateAction'], ['id'], null, null, false, true, null]],
        17561 => [[['_route' => 'oro_marketing_activity_widget_summary', '_controller' => 'Oro\\Bundle\\MarketingActivityBundle\\Controller\\MarketingActivityController::summaryAction'], ['campaignId'], null, null, false, true, null]],
        17581 => [[['_route' => 'oro_marketing_activity_widget_marketing_activities_info', '_controller' => 'Oro\\Bundle\\MarketingActivityBundle\\Controller\\MarketingActivityController::infoAction'], ['id'], null, null, false, true, null]],
        17646 => [[['_route' => 'oro_marketing_activity_widget_marketing_activities', '_controller' => 'Oro\\Bundle\\MarketingActivityBundle\\Controller\\MarketingActivityController::widgetAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        17678 => [[['_route' => 'oro_marketing_activity_widget_marketing_activities_list', '_controller' => 'Oro\\Bundle\\MarketingActivityBundle\\Controller\\MarketingActivityController::listAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        17751 => [[['_route' => 'oro_email_autoresponserule_create', 'mailbox' => null, '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\AutoResponseRuleController::createAction'], ['mailbox'], null, null, false, true, null]],
        17773 => [[['_route' => 'oro_email_autoresponserule_update', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\AutoResponseRuleController::updateAction'], ['id'], null, null, false, true, null]],
        17814 => [[['_route' => 'oro_email_activity_view', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::activityAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        17845 => [[['_route' => 'oro_email_attachment', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::attachmentAction'], ['id'], null, null, false, true, null]],
        17865 => [[['_route' => 'oro_email_attachment_link', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::linkAction'], ['id'], ['POST' => 0], null, false, false, null]],
        17883 => [[['_route' => 'oro_email_body_attachments', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::downloadAttachmentsAction'], ['id'], null, null, false, true, null]],
        17911 => [[['_route' => 'oro_email_view', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::viewAction'], ['id'], null, null, false, true, null]],
        17933 => [[['_route' => 'oro_email_thread_view', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::viewThreadAction'], ['id'], null, null, false, true, null]],
        17961 => [[['_route' => 'oro_email_user_thread_view', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::viewUserThreadAction'], ['id'], null, null, false, true, null]],
        17985 => [[['_route' => 'oro_email_view_group', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::viewGroupAction'], ['id'], null, null, false, true, 1]],
        18018 => [[['_route' => 'oro_email_thread_widget', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::threadWidgetAction'], ['id'], null, null, false, true, null]],
        18046 => [[['_route' => 'oro_email_user_thread_widget', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::userThreadWidgetAction'], ['id'], null, null, false, true, null]],
        18071 => [[['_route' => 'oro_email_email_reply', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::replyAction'], ['id'], null, null, false, true, 1]],
        18090 => [[['_route' => 'oro_email_email_reply_all', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::replyAllAction'], ['id'], null, null, false, true, 1]],
        18114 => [[['_route' => 'oro_email_email_forward', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::forwardAction'], ['id'], null, null, false, true, 1]],
        18134 => [[['_route' => 'oro_email_body', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::bodyAction'], ['id'], null, null, false, true, null]],
        18200 => [[['_route' => 'oro_resize_email_attachment', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::getResizedAttachmentImageAction'], ['id', 'width', 'height'], null, null, false, true, null]],
        18242 => [[['_route' => 'oro_email_mark_seen', 'checkThread' => true, '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::markSeenAction'], ['id', 'status', 'checkThread'], ['POST' => 0], null, false, true, null]],
        18271 => [[['_route' => 'oro_email_toggle_seen', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::toggleSeenAction'], ['id'], ['POST' => 0], null, false, true, null]],
        18309 => [[['_route' => 'oro_email_mark_massaction', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailController::markMassAction'], ['gridName', 'actionName'], null, null, false, true, null]],
        18352 => [[['_route' => 'oro_email_emailtemplate_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailTemplateController::indexAction'], ['_format'], null, null, false, true, null]],
        18383 => [[['_route' => 'oro_email_emailtemplate_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailTemplateController::updateAction'], ['id'], null, null, false, true, null]],
        18409 => [[['_route' => 'oro_email_emailtemplate_clone', 'id' => 0, '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailTemplateController::cloneAction'], ['id'], null, null, false, true, null]],
        18437 => [[['_route' => 'oro_email_emailtemplate_preview', 'id' => 0, '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\EmailTemplateController::previewAction'], ['id'], null, null, false, true, null]],
        18483 => [[['_route' => 'oro_email_autoresponserule_edittemplate', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\AutoResponseRuleController::editTemplateAction'], ['id'], null, null, false, true, null]],
        18530 => [[['_route' => 'oro_embedded_form_submit', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbedFormController::formAction'], ['id'], null, null, false, true, null]],
        18556 => [[['_route' => 'oro_embedded_form_success', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbedFormController::formSuccessAction'], ['id'], null, null, false, true, null]],
        18587 => [[['_route' => 'oro_embedded_form_delete', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbeddedFormController::deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        18617 => [[['_route' => 'oro_embedded_form_default_data', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbeddedFormController::defaultDataAction'], ['formType'], ['GET' => 0], null, false, true, null]],
        18645 => [[['_route' => 'oro_embedded_form_update', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbeddedFormController::updateAction'], ['id'], null, null, false, true, null]],
        18670 => [[['_route' => 'oro_embedded_form_view', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbeddedFormController::viewAction'], ['id'], null, null, false, true, null]],
        18695 => [[['_route' => 'oro_embedded_form_info', '_controller' => 'Oro\\Bundle\\EmbeddedFormBundle\\Controller\\EmbeddedFormController::infoAction'], ['id'], null, null, false, true, null]],
        18729 => [[['_route' => 'oro_entity_index', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\EntitiesController::indexAction'], ['entityName'], null, null, false, true, null]],
        18782 => [[['_route' => 'oro_entity_detailed', 'id' => 0, 'fieldName' => '', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\EntitiesController::detailedAction'], ['id', 'entityName', 'fieldName'], null, null, false, true, null]],
        18819 => [[['_route' => 'oro_entity_delete', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\EntitiesController::deleteAction'], ['entityName', 'id'], ['DELETE' => 0], null, false, true, null]],
        18870 => [[['_route' => 'oro_entity_relation', 'id' => 0, 'className' => '', 'fieldName' => '', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\EntitiesController::relationAction'], ['id', 'entityName', 'fieldName'], null, null, false, true, null]],
        18907 => [[['_route' => 'oro_entity_view', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\EntitiesController::viewAction'], ['entityName', 'id'], null, null, false, true, null]],
        18951 => [[['_route' => 'oro_entity_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\EntitiesController::updateAction'], ['entityName', 'id'], null, null, false, true, null]],
        19034 => [[['_route' => 'oro_entityconfig_audit', 'entity' => 'entity', 'id' => 0, '_format' => 'html', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AuditController::auditAction'], ['entity', 'id', '_format'], null, null, false, true, null]],
        19096 => [[['_route' => 'oro_entityconfig_audit_field', 'entity' => 'entity', 'id' => 0, '_format' => 'html', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\AuditController::auditFieldAction'], ['entity', 'id', '_format'], null, null, false, true, null]],
        19122 => [[['_route' => 'oro_entityconfig_update', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::updateAction'], ['id'], null, null, false, true, null]],
        19145 => [[['_route' => 'oro_entityconfig_view', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::viewAction'], ['id'], null, null, false, true, null]],
        19175 => [[['_route' => 'oro_entityconfig_fields', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::fieldsAction'], ['id'], null, null, false, true, null]],
        19204 => [[['_route' => 'oro_entityconfig_field_update', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::fieldUpdateAction'], ['id'], null, null, false, true, null]],
        19234 => [[['_route' => 'oro_entityconfig_field_search', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::fieldSearchAction'], ['id'], null, null, false, true, null]],
        19269 => [[['_route' => 'oro_entityconfig_widget_info', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::infoAction'], ['id'], null, null, false, true, null]],
        19299 => [[['_route' => 'oro_entityconfig_widget_unique_keys', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::uniqueKeysAction'], ['id'], null, null, false, true, null]],
        19331 => [[['_route' => 'oro_entityconfig_widget_entity_fields', '_controller' => 'Oro\\Bundle\\EntityConfigBundle\\Controller\\ConfigController::entityFieldsAction'], ['id'], null, null, false, true, null]],
        19373 => [[['_route' => 'oro_entityextend_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ApplyController::updateAction'], ['id'], null, null, false, true, null]],
        19418 => [[['_route' => 'oro_entityextend_entity_unique_key', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigEntityGridController::uniqueAction'], ['id'], null, null, false, true, null]],
        19445 => [[['_route' => 'oro_entityextend_entity_unremove', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigEntityGridController::unremoveAction'], ['id'], ['POST' => 0], null, false, true, null]],
        19473 => [[['_route' => 'oro_entityextend_entity_remove', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigEntityGridController::removeAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        19510 => [[['_route' => 'oro_entityextend_field_create', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigFieldGridController::createAction'], ['id'], null, null, false, true, null]],
        19540 => [[['_route' => 'oro_entityextend_field_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigFieldGridController::updateAction'], ['id'], null, null, false, true, null]],
        19568 => [[['_route' => 'oro_entityextend_field_unremove', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigFieldGridController::unremoveAction'], ['id'], ['POST' => 0], null, false, true, null]],
        19596 => [[['_route' => 'oro_entityextend_field_remove', 'id' => 0, '_controller' => 'Oro\\Bundle\\EntityExtendBundle\\Controller\\ConfigFieldGridController::removeAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        19657 => [[['_route' => 'oro_entity_pagination_first', '_controller' => 'Oro\\Bundle\\EntityPaginationBundle\\Controller\\EntityPaginationController::firstAction'], ['_entityName', '_scope', '_routeName'], null, null, false, true, null]],
        19702 => [[['_route' => 'oro_entity_pagination_previous', '_controller' => 'Oro\\Bundle\\EntityPaginationBundle\\Controller\\EntityPaginationController::previousAction'], ['_entityName', '_scope', '_routeName'], null, null, false, true, null]],
        19743 => [[['_route' => 'oro_entity_pagination_next', '_controller' => 'Oro\\Bundle\\EntityPaginationBundle\\Controller\\EntityPaginationController::nextAction'], ['_entityName', '_scope', '_routeName'], null, null, false, true, null]],
        19784 => [[['_route' => 'oro_entity_pagination_last', '_controller' => 'Oro\\Bundle\\EntityPaginationBundle\\Controller\\EntityPaginationController::lastAction'], ['_entityName', '_scope', '_routeName'], null, null, false, true, null]],
        19822 => [[['_route' => 'oro_importexport_export_instant', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::instantExportAction'], ['processorAlias'], ['POST' => 0], null, false, true, null]],
        19849 => [[['_route' => 'oro_importexport_export_template', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::templateExportAction'], ['processorAlias'], null, null, false, true, null]],
        19873 => [[['_route' => 'oro_importexport_export_download', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::downloadExportResultAction'], ['jobId'], null, null, false, true, null]],
        19966 => [[['_route' => 'oro_email_dashboard_recent_emails', 'activeTab' => 'inbox', 'contentType' => 'full', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Dashboard\\DashboardController::recentEmailsAction'], ['widget', 'activeTab', 'contentType'], null, null, false, true, null]],
        19994 => [[['_route' => 'oro_dashboard_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::indexAction'], ['_format'], null, null, false, true, null]],
        20023 => [[['_route' => 'oro_dashboard_view', 'id' => '0', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::viewAction'], ['id'], null, null, false, true, null]],
        20051 => [[['_route' => 'oro_dashboard_configure', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::configureAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        20092 => [[['_route' => 'oro_campaign_dashboard_campaigns_leads_chart', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\Dashboard\\DashboardController::campaignLeadsAction'], ['widget'], null, null, false, true, null]],
        20129 => [[['_route' => 'oro_campaign_dashboard_campaigns_opportunity_chart', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\Dashboard\\DashboardController::campaignOpportunityAction'], ['widget'], null, null, false, true, null]],
        20171 => [[['_route' => 'oro_campaign_dashboard_campaigns_by_close_revenue_chart', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\Dashboard\\DashboardController::campaignByCloseRevenueAction'], ['widget'], null, null, false, true, null]],
        20200 => [[['_route' => 'oro_dashboard_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::updateAction'], ['id'], null, null, false, true, null]],
        20249 => [[['_route' => 'oro_dashboard_widget', 'bundle' => '', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::widgetAction'], ['widget', 'name', 'bundle'], null, null, false, true, null]],
        20303 => [[['_route' => 'oro_dashboard_itemized_widget', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::itemizedWidgetAction'], ['widget', 'bundle', 'name'], null, null, false, true, null]],
        20350 => [[['_route' => 'oro_dashboard_itemized_data_widget', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::itemizedDataWidgetAction'], ['widget', 'bundle', 'name'], null, null, false, true, null]],
        20385 => [[['_route' => 'oro_dashboard_grid', '_controller' => 'Oro\\Bundle\\DashboardBundle\\Controller\\DashboardController::gridAction'], ['widget', 'gridName'], null, null, false, true, null]],
        20416 => [[['_route' => 'oro_calendar_dashboard_my_calendar', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\Dashboard\\DashboardController::myCalendarAction'], ['widget'], null, null, false, true, null]],
        20473 => [[['_route' => 'oro_sales_dashboard_opportunities_by_lead_source_chart', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Dashboard\\DashboardController::opportunitiesByLeadSourceAction'], ['widget'], null, null, false, true, null]],
        20506 => [[['_route' => 'oro_sales_dashboard_opportunity_by_state_chart', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\Dashboard\\DashboardController::opportunityByStatusAction'], ['widget'], null, null, false, true, null]],
        20546 => [[['_route' => 'oro_datagrid_widget', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\GridController::widgetAction'], ['gridName'], null, null, false, true, null]],
        20566 => [[['_route' => 'oro_datagrid_index', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\GridController::getAction'], ['gridName'], null, null, false, true, null]],
        20593 => [[['_route' => 'oro_datagrid_export_action', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\GridController::exportAction'], ['gridName'], null, null, true, false, null]],
        20635 => [[['_route' => 'oro_datagrid_mass_action', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\GridController::massActionAction'], ['gridName', 'actionName'], null, null, false, true, null]],
        20670 => [[['_route' => 'oro_datagrid_filter_metadata', '_controller' => 'Oro\\Bundle\\DataGridBundle\\Controller\\GridController::filterMetadataAction'], ['gridName'], null, null, false, false, null]],
        20710 => [[['_route' => 'oro_dictionary_search', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\DictionaryController::searchAction'], ['dictionary'], null, null, false, false, null]],
        20726 => [[['_route' => 'oro_dictionary_value', '_controller' => 'Oro\\Bundle\\EntityBundle\\Controller\\DictionaryController::valuesAction'], ['dictionary'], null, null, false, false, null]],
        20786 => [[['_route' => 'oro_dotmailer_synchronize_adddress_book', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\AddressBookController::synchronizeAddressBookAction'], ['id'], ['POST' => 0], null, false, true, null]],
        20813 => [[['_route' => 'oro_dotmailer_synchronize_adddress_book_datafields', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\AddressBookController::synchronizeAddressBookDataFieldsAction'], ['id'], ['POST' => 0], null, false, true, null]],
        20859 => [[['_route' => 'oro_dotmailer_marketing_list_disconnect', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\AddressBookController::disconnectMarketingListAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        20882 => [[['_route' => 'oro_dotmailer_marketing_list_buttons', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\AddressBookController::connectionButtonsAction'], ['entity'], null, null, false, true, null]],
        20940 => [[['_route' => 'oro_dotmailer_marketing_list_connect', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\AddressBookController::addressBookConnectionUpdateAction'], ['id'], null, null, false, true, null]],
        20982 => [[['_route' => 'oro_dotmailer_datafield_view', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldController::viewAction'], ['id'], null, null, false, true, null]],
        21002 => [[['_route' => 'oro_dotmailer_datafield_info', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldController::infoAction'], ['id'], null, null, false, true, null]],
        21030 => [[['_route' => 'oro_dotmailer_datafield_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldController::indexAction'], ['_format'], null, null, false, true, null]],
        21069 => [[['_route' => 'oro_dotmailer_datafield_mapping_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldMappingController::indexAction'], ['_format'], null, null, false, true, null]],
        21092 => [[['_route' => 'oro_dotmailer_datafield_mapping_update', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DataFieldMappingController::updateAction'], ['id'], null, null, false, true, null]],
        21145 => [[['_route' => 'oro_dotmailer_email_campaign_status', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DotmailerController::emailCampaignStatsAction'], ['entity'], null, null, false, true, null]],
        21173 => [[['_route' => 'oro_dotmailer_sync_status', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DotmailerController::marketingListSyncStatusAction'], ['marketingList'], null, null, false, true, null]],
        21217 => [[['_route' => 'oro_dotmailer_integration_connection', 'id' => '0', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\DotmailerController::integrationConnectionAction'], ['id'], null, null, false, true, null]],
        21251 => [[['_route' => 'oro_dotmailer_oauth_disconnect', '_controller' => 'Oro\\Bundle\\DotmailerBundle\\Controller\\OauthController::disconnectAction'], ['id'], null, null, false, true, null]],
        21309 => [[['_route' => 'oro_email_mailbox_update', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Configuration\\MailboxController::updateAction'], ['id'], null, null, false, true, null]],
        21339 => [[['_route' => 'oro_email_mailbox_users_search', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Configuration\\MailboxController::searchUsersAction'], ['organizationId'], null, null, false, true, null]],
        21365 => [[['_route' => 'oro_email_mailbox_delete', '_controller' => 'Oro\\Bundle\\EmailBundle\\Controller\\Configuration\\MailboxController::deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        21417 => [[['_route' => 'oro_user_config', 'activeGroup' => null, 'activeSubGroup' => null, '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ConfigurationController::userConfigAction'], ['id', 'activeGroup', 'activeSubGroup'], null, null, false, true, null]],
        21462 => [[['_route' => 'oro_user_profile_configuration', 'activeGroup' => null, 'activeSubGroup' => null, '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ConfigurationController::userProfileConfigAction'], ['activeGroup', 'activeSubGroup'], null, null, false, true, null]],
        21507 => [[['_route' => 'oro_config_configuration_system', 'activeGroup' => null, 'activeSubGroup' => null, '_controller' => 'Oro\\Bundle\\ConfigBundle\\Controller\\ConfigurationController::systemAction'], ['activeGroup', 'activeSubGroup'], null, null, false, true, null]],
        21548 => [[['_route' => 'oro_contact_address_book', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactAddressController::addressBookAction'], ['id'], null, null, false, true, null]],
        21579 => [[['_route' => 'oro_contact_address_create', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactAddressController::createAction'], ['contactId'], null, null, false, false, null]],
        21621 => [[['_route' => 'oro_contact_address_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactAddressController::updateAction'], ['contactId', 'id'], null, null, false, true, null]],
        21641 => [[['_route' => 'oro_contact_view', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactController::viewAction'], ['id'], null, null, false, true, null]],
        21661 => [[['_route' => 'oro_contact_info', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactController::infoAction'], ['id'], null, null, false, true, null]],
        21683 => [[['_route' => 'oro_contact_update', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactController::updateAction'], ['id'], null, null, false, true, null]],
        21711 => [[['_route' => 'oro_contact_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactController::indexAction'], ['_format'], null, null, false, true, null]],
        21755 => [[['_route' => 'oro_account_widget_contacts', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\ContactController::accountContactsAction'], ['id'], null, null, false, true, null]],
        21791 => [[['_route' => 'oro_contact_group_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\GroupController::updateAction'], ['id'], null, null, false, true, null]],
        21818 => [[['_route' => 'oro_contact_group_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\ContactBundle\\Controller\\GroupController::indexAction'], ['_format'], null, null, false, true, null]],
        21851 => [[['_route' => 'oro_contactus_request_view', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactRequestController::viewAction'], ['id'], null, null, false, true, null]],
        21871 => [[['_route' => 'oro_contactus_request_info', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactRequestController::infoAction'], ['id'], null, null, false, true, null]],
        21893 => [[['_route' => 'oro_contactus_request_update', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactRequestController::updateAction'], ['id'], null, null, false, true, null]],
        21915 => [[['_route' => 'oro_contactus_request_delete', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactRequestController::deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        21948 => [[['_route' => 'oro_contactus_reason_update', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactReasonController::updateAction'], ['id'], null, null, false, true, null]],
        21970 => [[['_route' => 'oro_contactus_reason_delete', '_controller' => 'Oro\\Bundle\\ContactUsBundle\\Controller\\ContactReasonController::deleteAction'], ['id'], ['DELETE' => 0], null, false, true, null]],
        22017 => [[['_route' => 'oro_digital_asset_update', '_controller' => 'Oro\\Bundle\\DigitalAssetBundle\\Controller\\DigitalAssetController::updateAction'], ['id'], null, null, false, true, null]],
        22058 => [[['_route' => 'oro_digital_asset_widget_choose', '_controller' => 'Oro\\Bundle\\DigitalAssetBundle\\Controller\\DigitalAssetController::chooseAction'], ['parentEntityClass', 'parentEntityFieldName'], null, null, false, true, null]],
        22123 => [[['_route' => 'oro_calendar_event_accepted', 'status' => 'accepted', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\AjaxCalendarEventController::changeStatusAction'], ['id'], ['POST' => 0], null, false, true, null]],
        22170 => [[['_route' => 'oro_calendar_event_attendees_autocomplete_data', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\AjaxCalendarEventController::attendeesAutocompleteDataAction'], ['id'], null, null, false, true, null]],
        22196 => [[['_route' => 'oro_calendar_event_tentative', 'status' => 'tentative', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\AjaxCalendarEventController::changeStatusAction'], ['id'], ['POST' => 0], null, false, true, null]],
        22220 => [[['_route' => 'oro_calendar_event_declined', 'status' => 'declined', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\AjaxCalendarEventController::changeStatusAction'], ['id'], ['POST' => 0], null, false, true, null]],
        22261 => [[['_route' => 'oro_calendar_event_activity_view', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarEventController::activityAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        22282 => [[['_route' => 'oro_calendar_event_view', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarEventController::viewAction'], ['id'], null, null, false, true, null]],
        22320 => [[['_route' => 'oro_calendar_event_widget_info', 'renderContexts' => true, '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarEventController::infoAction'], ['id', 'renderContexts'], null, null, false, true, null]],
        22342 => [[['_route' => 'oro_calendar_event_update', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarEventController::updateAction'], ['id'], null, null, false, true, null]],
        22364 => [[['_route' => 'oro_calendar_event_delete', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarEventController::deleteAction'], ['id'], null, null, false, true, null]],
        22385 => [[['_route' => 'oro_calendar_view', '_controller' => 'Oro\\Bundle\\CalendarBundle\\Controller\\CalendarController::viewAction'], ['id'], null, null, false, true, null]],
        22432 => [[['_route' => 'oro_call_activity_view', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::activityAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        22454 => [[['_route' => 'oro_call_update', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::updateAction'], ['id'], null, null, false, true, null]],
        22477 => [[['_route' => 'oro_call_view', '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::viewAction'], ['id'], null, null, false, true, null]],
        22515 => [[['_route' => 'oro_call_widget_info', 'renderContexts' => true, '_controller' => 'Oro\\Bundle\\CallBundle\\Controller\\CallController::infoAction'], ['id', 'renderContexts'], null, null, false, true, null]],
        22554 => [[['_route' => 'oro_campaign_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\CampaignController::updateAction'], ['id'], null, null, false, true, null]],
        22577 => [[['_route' => 'oro_campaign_view', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\CampaignController::viewAction'], ['id'], null, null, false, true, null]],
        22618 => [[['_route' => 'oro_campaign_event_plot', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\CampaignEventController::plotAction'], ['period', 'campaign'], null, null, false, true, null]],
        22653 => [[['_route' => 'oro_email_campaign_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\EmailCampaignController::updateAction'], ['id'], null, null, false, true, null]],
        22673 => [[['_route' => 'oro_email_campaign_view', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\EmailCampaignController::viewAction'], ['id'], null, null, false, true, null]],
        22693 => [[['_route' => 'oro_email_campaign_send', '_controller' => 'Oro\\Bundle\\CampaignBundle\\Controller\\EmailCampaignController::sendAction'], ['id'], null, null, false, true, null]],
        22722 => [[['_route' => 'oro_case_view', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CaseController::viewAction'], ['id'], null, null, false, true, null]],
        22762 => [[['_route' => 'oro_case_account_widget_cases', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CaseController::accountCasesAction'], ['id'], null, null, false, true, null]],
        22792 => [[['_route' => 'oro_case_contact_widget_cases', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CaseController::contactCasesAction'], ['id'], null, null, false, true, null]],
        22815 => [[['_route' => 'oro_case_update', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CaseController::updateAction'], ['id'], null, null, false, true, null]],
        22856 => [[['_route' => 'oro_case_comment_list', '_format' => 'json', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CommentController::commentsListAction'], ['id', '_format'], null, null, false, true, null]],
        22886 => [[['_route' => 'oro_case_widget_comments', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CommentController::commentsWidgetAction'], ['id'], null, null, false, false, null]],
        22916 => [[['_route' => 'oro_case_comment_create', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CommentController::createAction'], ['caseId'], null, null, false, false, null]],
        22946 => [[['_route' => 'oro_case_comment_update', '_controller' => 'Oro\\Bundle\\CaseBundle\\Controller\\CommentController::updateAction'], ['id'], null, null, false, false, null]],
        22984 => [[['_route' => 'oro_channel_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\ChannelController::indexAction'], ['_format'], null, null, false, true, null]],
        23010 => [[['_route' => 'oro_channel_update', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\ChannelController::updateAction'], ['id'], null, null, false, true, null]],
        23030 => [[['_route' => 'oro_channel_view', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\ChannelController::viewAction'], ['id'], null, null, false, true, null]],
        23057 => [[['_route' => 'oro_channel_widget_info', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\ChannelController::infoAction'], ['id'], null, null, false, true, null]],
        23108 => [[['_route' => 'oro_channel_integration_create', 'channelName' => null, '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\ChannelIntegrationController::createAction'], ['type', 'channelName'], null, null, false, true, null]],
        23130 => [[['_route' => 'oro_channel_integration_update', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\ChannelIntegrationController::updateAction'], ['id'], null, null, false, true, null]],
        23166 => [[['_route' => 'oro_channel_dashboard_average_lifetime_sales_chart', '_controller' => 'Oro\\Bundle\\ChannelBundle\\Controller\\Dashboard\\DashboardController::averageLifetimeSalesAction'], ['widget'], null, null, false, true, null]],
        23216 => [[['_route' => 'oro_sales_customer_grid_dialog', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\CustomerController::gridDialogAction'], ['entityClass'], null, null, false, true, null]],
        23265 => [[['_route' => 'oro_user_group_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\GroupController::updateAction'], ['id'], null, null, false, true, null]],
        23292 => [[['_route' => 'oro_user_group_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\GroupController::indexAction'], ['_format'], null, null, false, true, null]],
        23348 => [[['_route' => 'oro_user_send_forced_password_reset_email', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ResetController::sendForcedResetEmailAction'], ['id'], null, null, false, true, null]],
        23375 => [[['_route' => 'oro_user_reset_set_password', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ResetController::setPasswordAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        23400 => [[['_route' => 'oro_user_reset_reset', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\ResetController::resetAction'], ['token'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        23430 => [[['_route' => 'oro_user_role_view', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\RoleController::viewAction'], ['id'], null, null, false, true, null]],
        23457 => [[['_route' => 'oro_user_role_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\RoleController::updateAction'], ['id'], null, null, false, true, null]],
        23485 => [[['_route' => 'oro_user_role_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\RoleController::indexAction'], ['_format'], null, null, false, true, null]],
        23507 => [[['_route' => 'oro_user_view', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::viewAction'], ['id'], null, null, false, true, null]],
        23529 => [[['_route' => 'oro_user_apigen', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::apigenAction'], ['id'], ['GET' => 0, 'POST' => 1], null, false, true, null]],
        23556 => [[['_route' => 'oro_user_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::updateAction'], ['id'], null, null, false, true, null]],
        23584 => [[['_route' => 'oro_user_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::indexAction'], ['_format'], null, null, false, true, null]],
        23612 => [[['_route' => 'oro_user_widget_info', '_controller' => 'Oro\\Bundle\\UserBundle\\Controller\\UserController::infoAction'], ['id'], null, null, false, true, null]],
        23670 => [[['_route' => 'oro_translation_mass_reset', '_controller' => 'Oro\\Bundle\\TranslationBundle\\Controller\\TranslationController::resetMassAction'], ['gridName', 'actionName'], null, null, false, true, null]],
        23714 => [[['_route' => 'oro_tracking_website_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\TrackingBundle\\Controller\\TrackingWebsiteController::indexAction'], ['_format'], null, null, false, true, null]],
        23740 => [[['_route' => 'oro_tracking_website_update', '_controller' => 'Oro\\Bundle\\TrackingBundle\\Controller\\TrackingWebsiteController::updateAction'], ['id'], null, null, false, true, null]],
        23760 => [[['_route' => 'oro_tracking_website_view', '_controller' => 'Oro\\Bundle\\TrackingBundle\\Controller\\TrackingWebsiteController::viewAction'], ['id'], null, null, false, true, null]],
        23798 => [[['_route' => 'oro_tag_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TagController::indexAction'], ['_format'], null, null, false, true, null]],
        23829 => [[['_route' => 'oro_tag_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TagController::updateAction'], ['id'], null, null, false, true, null]],
        23856 => [[['_route' => 'oro_tag_search', 'id' => 0, '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TagController::searchAction'], ['id'], null, null, false, true, null]],
        23894 => [[['_route' => 'oro_taxonomy_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TaxonomyController::indexAction'], ['_format'], null, null, false, true, null]],
        23925 => [[['_route' => 'oro_taxonomy_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TaxonomyController::updateAction'], ['id'], null, null, false, true, null]],
        23945 => [[['_route' => 'oro_taxonomy_view', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TaxonomyController::viewAction'], ['id'], null, null, false, true, null]],
        23972 => [[['_route' => 'oro_taxonomy_widget_info', '_controller' => 'Oro\\Bundle\\TagBundle\\Controller\\TaxonomyController::infoAction'], ['id'], null, null, false, true, null]],
        24025 => [[['_route' => 'oro_task_widget_sidebar_tasks', 'perPage' => 10, '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskController::tasksWidgetAction'], ['perPage'], null, null, false, true, null]],
        24045 => [[['_route' => 'oro_task_widget_info', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskController::infoAction'], ['id'], null, null, false, true, null]],
        24081 => [[['_route' => 'oro_task_activity_view', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskController::activityAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        24104 => [[['_route' => 'oro_task_user_tasks', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskController::userTasksAction'], ['user'], null, null, false, true, null]],
        24125 => [[['_route' => 'oro_task_update', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskCrudController::updateAction'], ['id'], null, null, false, true, null]],
        24146 => [[['_route' => 'oro_task_view', '_controller' => 'Oro\\Bundle\\TaskBundle\\Controller\\TaskCrudController::viewAction'], ['id'], null, null, false, true, null]],
        24196 => [[['_route' => 'oro_locale_localization_view', '_controller' => 'Oro\\Bundle\\LocaleBundle\\Controller\\LocalizationController::viewAction'], ['id'], null, null, false, true, null]],
        24218 => [[['_route' => 'oro_locale_localization_update', '_controller' => 'Oro\\Bundle\\LocaleBundle\\Controller\\LocalizationController::updateAction'], ['id'], null, null, false, true, null]],
        24258 => [[['_route' => 'oro_sales_lead_address_book', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadAddressController::addressBookAction'], ['id'], null, null, false, true, null]],
        24289 => [[['_route' => 'oro_sales_lead_address_create', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadAddressController::createAction'], ['leadId'], null, null, false, false, null]],
        24331 => [[['_route' => 'oro_sales_lead_address_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadAddressController::updateAction'], ['leadId', 'id'], null, null, false, true, null]],
        24351 => [[['_route' => 'oro_sales_lead_view', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::viewAction'], ['id'], null, null, false, true, null]],
        24371 => [[['_route' => 'oro_sales_lead_info', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::infoAction'], ['id'], null, null, false, true, null]],
        24398 => [[['_route' => 'oro_sales_lead_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::updateAction'], ['id'], null, null, false, true, null]],
        24426 => [[['_route' => 'oro_sales_lead_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::indexAction'], ['_format'], null, null, false, true, null]],
        24467 => [[['_route' => 'oro_sales_widget_account_leads', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::accountLeadsAction'], ['id'], null, null, false, true, null]],
        24495 => [[['_route' => 'oro_sales_lead_data_channel_aware_create', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::leadWithDataChannelCreateAction'], ['channelIds'], null, null, false, true, null]],
        24517 => [[['_route' => 'oro_sales_lead_convert_to_opportunity', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::convertToOpportunityAction'], ['id'], null, null, false, true, null]],
        24544 => [[['_route' => 'oro_sales_lead_disqualify', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\LeadController::disqualifyAction'], ['id'], ['POST' => 0], null, false, true, null]],
        24588 => [[['_route' => 'oro_integration_update', '_controller' => 'Oro\\Bundle\\IntegrationBundle\\Controller\\IntegrationController::updateAction'], ['id'], null, null, false, true, null]],
        24612 => [[['_route' => 'oro_integration_schedule', '_controller' => 'Oro\\Bundle\\IntegrationBundle\\Controller\\IntegrationController::scheduleAction'], ['id'], ['POST' => 0], null, false, true, null]],
        24652 => [[['_route' => 'oro_importexport_import_validate', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::importValidateAction'], ['processorAlias'], ['POST' => 0], null, false, true, null]],
        24678 => [[['_route' => 'oro_importexport_import_process', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::importProcessAction'], ['processorAlias'], ['POST' => 0], null, false, true, null]],
        24723 => [[['_route' => 'oro_importexport_job_error_log', '_controller' => 'Oro\\Bundle\\ImportExportBundle\\Controller\\ImportExportController::importExportJobErrorLogAction'], ['jobId'], null, null, false, false, null]],
        24780 => [[['_route' => 'oro_notification_emailnotification_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\EmailNotificationController::indexAction'], ['_format'], null, null, false, true, null]],
        24808 => [[['_route' => 'oro_notification_emailnotification_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\EmailNotificationController::updateAction'], ['id'], null, null, false, true, null]],
        24855 => [[['_route' => 'oro_notification_massnotification_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\MassNotificationController::indexAction'], ['_format'], null, null, false, true, null]],
        24879 => [[['_route' => 'oro_notification_massnotification_view', '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\MassNotificationController::viewAction'], ['id'], null, null, false, true, null]],
        24899 => [[['_route' => 'oro_notification_massnotification_info', '_controller' => 'Oro\\Bundle\\NotificationBundle\\Controller\\MassNotificationController::infoAction'], ['id'], null, null, false, true, null]],
        24950 => [[['_route' => 'oro_note_widget_notes', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\NoteController::widgetAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        24977 => [[['_route' => 'oro_note_notes', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\NoteController::getAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        25016 => [[['_route' => 'oro_note_widget_info', 'renderContexts' => true, '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\NoteController::infoAction'], ['id', 'renderContexts'], null, null, false, true, null]],
        25038 => [[['_route' => 'oro_note_update', '_controller' => 'Oro\\Bundle\\NoteBundle\\Controller\\NoteController::updateAction'], ['id'], null, null, false, true, null]],
        25077 => [[['_route' => 'oro_report_view', '_controller' => 'Oro\\Bundle\\ReportBundle\\Controller\\ReportController::viewAction'], ['id'], null, null, false, true, null]],
        25095 => [[['_route' => 'oro_report_view_grid', '_controller' => 'Oro\\Bundle\\ReportBundle\\Controller\\ReportController::viewFromGridAction'], ['gridName'], null, null, false, true, null]],
        25118 => [[['_route' => 'oro_report_update', '_controller' => 'Oro\\Bundle\\ReportBundle\\Controller\\ReportController::updateAction'], ['id'], null, null, false, true, null]],
        25139 => [[['_route' => 'oro_report_clone', '_controller' => 'Oro\\Bundle\\ReportBundle\\Controller\\ReportController::cloneAction'], ['id'], null, null, false, true, null]],
        25167 => [[['_route' => 'oro_report_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\ReportBundle\\Controller\\ReportController::indexAction'], ['_format'], null, null, false, true, null]],
        25213 => [[['_route' => 'oro_reportcrm_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\ReportCRMBundle\\Controller\\ReportController::indexAction'], ['reportGroupName', 'reportName', '_format'], null, null, false, true, null]],
        25256 => [[['_route' => 'oro_process_definition_view', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\ProcessDefinitionController::viewAction'], ['name'], null, null, false, true, null]],
        25323 => [[['_route' => 'oro_workflow_widget_entity_workflows', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WidgetController::entityWorkflowsAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        25382 => [[['_route' => 'oro_workflow_widget_start_transition_form', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WidgetController::startTransitionFormAction'], ['workflowName', 'transitionName'], null, null, false, true, null]],
        25425 => [[['_route' => 'oro_workflow_widget_transition_form', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WidgetController::transitionFormAction'], ['workflowItemId', 'transitionName'], null, null, false, true, null]],
        25461 => [[['_route' => 'oro_workflow_widget_buttons', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WidgetController::buttonsAction'], ['entityClass', 'entityId'], null, null, false, true, null]],
        25499 => [[['_route' => 'oro_workflow_start_transition_form', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowController::startTransitionAction'], ['workflowName', 'transitionName'], null, null, false, true, null]],
        25534 => [[['_route' => 'oro_workflow_transition_form', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowController::transitionAction'], ['workflowItemId', 'transitionName'], null, null, false, true, null]],
        25574 => [[['_route' => 'oro_workflow_definition_update', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowDefinitionController::updateAction'], ['name'], null, null, false, true, null]],
        25602 => [[['_route' => 'oro_workflow_definition_configure', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowDefinitionController::configureAction'], ['name'], null, null, false, true, null]],
        25625 => [[['_route' => 'oro_workflow_definition_view', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowDefinitionController::viewAction'], ['name'], null, null, false, true, null]],
        25658 => [[['_route' => 'oro_workflow_definition_activate_from_widget', '_controller' => 'Oro\\Bundle\\WorkflowBundle\\Controller\\WorkflowDefinitionController::activateFormAction'], ['name'], null, null, false, true, null]],
        25702 => [[['_route' => 'oro_sales_b2bcustomer_index', '_format' => 'html', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::indexAction'], ['_format'], null, null, false, true, null]],
        25726 => [[['_route' => 'oro_sales_b2bcustomer_view', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::viewAction'], ['id'], null, null, false, true, null]],
        25756 => [[['_route' => 'oro_sales_b2bcustomer_widget_info', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::infoAction'], ['id'], null, null, false, true, null]],
        25796 => [[['_route' => 'oro_sales_b2bcustomer_widget_leads', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::b2bCustomerLeadsAction'], ['id'], null, null, false, true, null]],
        25825 => [[['_route' => 'oro_sales_b2bcustomer_widget_opportunities', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::b2bCustomerOpportunitiesAction'], ['id'], null, null, false, true, null]],
        25859 => [[['_route' => 'oro_sales_widget_b2bcustomer_info', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::customerInfoAction'], ['id', 'channelId'], null, null, false, true, null]],
        25905 => [[['_route' => 'oro_sales_widget_account_b2bcustomers_info', '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::accountCustomersInfoAction'], ['accountId', 'channelId'], null, null, false, true, null]],
        25934 => [
            [['_route' => 'oro_sales_b2bcustomer_update', 'id' => 0, '_controller' => 'Oro\\Bundle\\SalesBundle\\Controller\\B2bCustomerController::updateAction'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    static function ($condition, $context, $request) { // $checkCondition
        switch ($condition) {
            case 1: return (($request !== null) && $request->get("_widgetContainer"));
        }
    },
];
