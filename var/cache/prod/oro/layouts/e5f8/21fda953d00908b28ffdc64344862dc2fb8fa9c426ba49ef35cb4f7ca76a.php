<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/block_types.yml
 */
class __Oro_Layout_Update_e5f821fda953d00908b28ffdc64344862dc2fb8fa9c426ba49ef35cb4f7ca76a implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroUI/layouts/default/block_types.html.twig' );
    }
}
