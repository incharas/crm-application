<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/FormBundle/Resources/views/layouts/default/layout.yml
 */
class __Oro_Layout_Update_f7285debe01242224bd038312ad73951e7543f7207c4c6298430b0cad4223a1d implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroForm/layouts/default/layout.html.twig' );
        $layoutManipulator->setFormTheme( '@OroForm/layouts/default/form_theme.html.twig' );
    }
}
