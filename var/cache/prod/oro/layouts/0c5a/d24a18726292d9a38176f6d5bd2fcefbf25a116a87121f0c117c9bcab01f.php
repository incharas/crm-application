<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/logo.yml
 */
class __Oro_Layout_Update_0c5ad24a18726292d9a38176f6d5bd2fcefbf25a116a87121f0c117c9bcab01f implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->add( 'logo', 'middle_bar_logo', 'logo', array (
          'width' => 167,
          'height' => 32,
        ) );
        $layoutManipulator->add( 'logo_print', 'page_container', 'logo', array (
          'attr' =>
          array (
            'class' => 'logo--print-only',
          ),
          'renderLink' => false,
        ), 'page_header' );
    }
}
