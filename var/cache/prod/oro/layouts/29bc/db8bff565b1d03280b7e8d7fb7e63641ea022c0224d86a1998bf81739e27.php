<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/page-header.yml
 */
class __Oro_Layout_Update_29bcdb8bff565b1d03280b7e8d7fb7e63641ea022c0224d86a1998bf81739e27 implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroUI/layouts/default/page/page-header.html.twig' );
        $layoutManipulator->add( 'page_header', 'page_container', 'container', array (
        ), NULL, true );
    }
}
