<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/middle_bar.yml
 */
class __Oro_Layout_Update_30466009f93cb3c3830682fe7e8ea75edfb8a20032d3469832f59ea2da38995b implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroUI/layouts/default/page/middle_bar.html.twig' );
        $layoutManipulator->add( 'middle_bar', 'page_header', 'container', array (
        ), NULL, true );
        $layoutManipulator->add( 'middle_bar_logo', 'middle_bar', 'container' );
        $layoutManipulator->add( 'middle_bar_center', 'middle_bar', 'container' );
        $layoutManipulator->add( 'middle_bar_right', 'middle_bar', 'container' );
    }
}
