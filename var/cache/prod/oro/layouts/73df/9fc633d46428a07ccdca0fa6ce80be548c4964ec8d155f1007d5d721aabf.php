<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/layouts/default/imports/datagrid_toolbar/layout_mobile.yml
 */
class __Oro_Layout_Update_73df9fc633d46428a07ccdca0fa6ce80be548c4964ec8d155f1007d5d721aabf implements Oro\Component\Layout\LayoutUpdateInterface, Oro\Component\Layout\LayoutUpdateImportInterface, Oro\Component\Layout\IsApplicableLayoutUpdateInterface
{
    private $import = null;
    private $parentLayoutUpdate = null;

    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        if (!$this->isApplicable($item->getContext())) {
            return;
        }

        if (null === $this->import) {
            throw new \RuntimeException('Missing import configuration for layout update');
        }

        if ($this->parentLayoutUpdate instanceof \Oro\Component\Layout\IsApplicableLayoutUpdateInterface
            && !$this->parentLayoutUpdate->isApplicable($item->getContext())) {
            return;
        }

        $layoutManipulator = new \Oro\Component\Layout\ImportLayoutManipulator($layoutManipulator, $this->import);
        $layoutManipulator->setBlockTheme( '@OroDataGrid/layouts/default/imports/datagrid_toolbar/layout_mobile.html.twig' );
        $layoutManipulator->remove( '__datagrid_toolbar_extra_actions' );
        $layoutManipulator->remove( '__datagrid_toolbar_tools_container' );
        $layoutManipulator->move( '__datagrid_toolbar_actions', 'datagrid_toolbar', NULL, true );
        $layoutManipulator->add( '__datagrid_toolbar_pagination_container', '__datagrid_toolbar', 'container' );
        $layoutManipulator->move( '__datagrid_toolbar_pagination', '__datagrid_toolbar_pagination_container' );
        $layoutManipulator->move( '__datagrid_toolbar_page_size', '__datagrid_toolbar_pagination_container', '__datagrid_toolbar_pagination' );
    }

    public function getImport()
    {
        return $this->import;
    }

    public function setImport(Oro\Component\Layout\Model\LayoutUpdateImport $import)
    {
        $this->import = $import;
    }

    public function setParentUpdate(Oro\Component\Layout\ImportsAwareLayoutUpdateInterface $parentLayoutUpdate)
    {
        $this->parentLayoutUpdate = $parentLayoutUpdate;
    }

    public function isApplicable(Oro\Component\Layout\ContextInterface $context)
    {
        return ($context["is_mobile"] == true);
    }
}
