<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/noscript.yml
 */
class __Oro_Layout_Update_16506001d6fab5b954d3772e49726facbc9ee8d27bf8f27611d9f4fcdcff7d02 implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroUI/layouts/default/page/noscript.html.twig' );
        $layoutManipulator->add( 'nosctipt', 'body', 'block', array (
        ), NULL, true );
    }
}
