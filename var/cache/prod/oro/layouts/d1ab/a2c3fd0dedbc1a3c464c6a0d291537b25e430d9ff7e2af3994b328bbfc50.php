<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/default.yml
 */
class __Oro_Layout_Update_d1aba2c3fd0dedbc1a3c464c6a0d291537b25e430d9ff7e2af3994b328bbfc50 implements Oro\Component\Layout\LayoutUpdateInterface, Oro\Component\Layout\IsApplicableLayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        if (!$this->isApplicable($item->getContext())) {
            return;
        }

        $layoutManipulator->add( 'head', 'root', 'head' );
        $layoutManipulator->add( 'title', 'head', 'title', array (
          'value' => 'Form',
        ) );
        $layoutManipulator->add( 'meta_charset', 'head', 'meta', array (
          'charset' => 'utf-8',
        ) );
        $layoutManipulator->add( 'meta_x_ua_compatible', 'head', 'meta', array (
          'http_equiv' => 'X-UA-Compatible',
          'content' => 'IE=edge,chrome=1',
        ) );
        $layoutManipulator->add( 'meta_viewport', 'head', 'meta', array (
          'name' => 'viewport',
          'content' => 'width=device-width, initial-scale=1.0',
        ) );
        $layoutManipulator->add( 'base_css', 'head', 'style' );
        $layoutManipulator->add( 'form_css', 'head', 'style', array (
          'content' => '=data["embedded_form_entity"].getCss()',
        ) );
        $layoutManipulator->add( 'content', 'root', 'body' );
        $layoutManipulator->setBlockTheme( '@OroEmbeddedForm/layouts/embedded_default/form.html.twig' );
    }

    public function isApplicable(Oro\Component\Layout\ContextInterface $context)
    {
        return ($context["embedded_form_inline"] == false);
    }
}
