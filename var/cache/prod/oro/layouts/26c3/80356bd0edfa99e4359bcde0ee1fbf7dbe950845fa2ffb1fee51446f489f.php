<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/dialog/dialog.yml
 */
class __Oro_Layout_Update_26c380356bd0edfa99e4359bcde0ee1fbf7dbe950845fa2ffb1fee51446f489f implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroUI/layouts/default/dialog/dialog.html.twig' );
        $layoutManipulator->add( 'widget_content', 'root', 'widget_content' );
        $layoutManipulator->add( 'widget_actions', 'widget_content', 'widget_actions' );
    }
}
