<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/components.yml
 */
class __Oro_Layout_Update_14289565bba5b618b3d9a6d56eae49ed58270904b353563dd1b5e62e86e4cd52 implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroUI/layouts/default/page/components.html.twig' );
        $layoutManipulator->add( 'copyright', 'page_footer', 'block', array (
        ), NULL, false );
    }
}
