<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/oro_embedded_form_submit/default.yml
 */
class __Oro_Layout_Update_5deff9a1dbac2799dffaa3a9a2a04c30972fd41337f46d5f0e648bb62967add9 implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->add( 'embedded_form_start', 'content', 'embed_form_start', array (
          'form_name' => 'embedded_form',
        ) );
        $layoutManipulator->add( 'embedded_form', 'content', 'embed_form_fields', array (
          'form_name' => 'embedded_form',
        ) );
        $layoutManipulator->add( 'embedded_form_end', 'content', 'embed_form_end', array (
          'form_name' => 'embedded_form',
        ) );
    }
}
