<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/oro_embedded_form_submit/form_session_id.yml
 */
class __Oro_Layout_Update_bdb46858c647870f6f8c4b47a161d467491c00fb7b5bc5daa54af9840dc14756 implements Oro\Component\Layout\LayoutUpdateInterface, Oro\Component\Layout\IsApplicableLayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        if (!$this->isApplicable($item->getContext())) {
            return;
        }

        $layoutManipulator->add( 'embedded_form_session_id', 'embedded_form', 'input', array (
          'type' => 'hidden',
          'name' => '=data["embedded_form_session_id_field_name"]',
          'value' => '=data["embedded_form_session_id"]',
        ) );
    }

    public function isApplicable(Oro\Component\Layout\ContextInterface $context)
    {
        return ($context["embedded_form"] !== null);
    }
}
