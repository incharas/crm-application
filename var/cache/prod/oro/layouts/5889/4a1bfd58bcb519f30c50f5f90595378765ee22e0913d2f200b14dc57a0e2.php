<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/layout.yml
 */
class __Oro_Layout_Update_58894a1bfd58bcb519f30c50f5f90595378765ee22e0913d2f200b14dc57a0e2 implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroEmbeddedForm/layouts/embedded_default/layout.html.twig' );
    }
}
