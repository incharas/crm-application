<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/page-footer.yml
 */
class __Oro_Layout_Update_c8a5da9cb0852823e6b680029ce3e5096d02eaa59d23fbfe74587dc304974696 implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroUI/layouts/default/page/page-footer.html.twig' );
        $layoutManipulator->add( 'page_footer', 'page_container', 'container', array (
        ), NULL, false );
        $layoutManipulator->add( 'page_footer_container', 'page_footer', 'container' );
        $layoutManipulator->add( 'page_footer_base', 'page_footer_container', 'container' );
        $layoutManipulator->add( 'page_footer_side', 'page_footer_container', 'container' );
    }
}
