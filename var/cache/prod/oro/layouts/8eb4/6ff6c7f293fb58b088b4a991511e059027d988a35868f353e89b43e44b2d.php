<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/UIBundle/Resources/views/layouts/default/page/layout_mobile.yml
 */
class __Oro_Layout_Update_8eb46ff6c7f293fb58b088b4a991511e059027d988a35868f353e89b43e44b2d implements Oro\Component\Layout\LayoutUpdateInterface, Oro\Component\Layout\IsApplicableLayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        if (!$this->isApplicable($item->getContext())) {
            return;
        }

        $layoutManipulator->setOption( 'meta_viewport', 'content', 'width=device-width, initial-scale=1, viewport-fit=cover' );
    }

    public function isApplicable(Oro\Component\Layout\ContextInterface $context)
    {
        return $context["is_mobile"];
    }
}
