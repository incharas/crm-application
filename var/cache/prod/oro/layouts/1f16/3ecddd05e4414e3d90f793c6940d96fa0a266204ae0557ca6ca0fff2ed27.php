<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/crm/src/Oro/Bundle/ContactUsBundle/Resources/views/layouts/embedded_default/oro_embedded_form_submit/default.yml
 */
class __Oro_Layout_Update_1f163ecddd05e4414e3d90f793c6940d96fa0a266204ae0557ca6ca0fff2ed27 implements Oro\Component\Layout\LayoutUpdateInterface, Oro\Component\Layout\IsApplicableLayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        if (!$this->isApplicable($item->getContext())) {
            return;
        }

        $layoutManipulator->setBlockTheme( '@OroContactUs/layouts/embedded_default/oro_embedded_form_submit/form.html.twig' );
        $layoutManipulator->setOption( 'embedded_form', 'preferred_fields', array (
          0 => 'firstName',
          1 => 'lastName',
          2 => 'emailAddress',
          3 => 'phone',
        ) );
        $layoutManipulator->add( 'name_group', 'embedded_form', 'fieldset' );
        $layoutManipulator->move( 'embedded_form_firstName', 'name_group' );
        $layoutManipulator->move( 'embedded_form_lastName', 'name_group' );
    }

    public function isApplicable(Oro\Component\Layout\ContextInterface $context)
    {
        return ($context["embedded_form_type"] == "Oro\\Bundle\\ContactUsBundle\\Form\\Type\\ContactRequestType");
    }
}
