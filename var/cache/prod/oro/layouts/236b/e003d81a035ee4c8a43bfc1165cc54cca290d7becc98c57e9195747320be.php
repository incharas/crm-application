<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EmbeddedFormBundle/Resources/views/layouts/embedded_default/oro_embedded_form_success/default.yml
 */
class __Oro_Layout_Update_236be003d81a035ee4c8a43bfc1165cc54cca290d7becc98c57e9195747320be implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->add( 'success_message', 'content', 'embed_form_success', array (
          'message' => '=data["embedded_form_entity"].getSuccessMessage()',
          'form_id' => '=data["embedded_form_entity"].getId()',
        ) );
    }
}
