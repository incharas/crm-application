<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/layouts/default/page/layout.yml
 */
class __Oro_Layout_Update_e118debe787bb18c857504789ffa4f72b3489821e1677f27c9e409681284193d implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroDataGrid/layouts/default/page/layout.html.twig' );
    }
}
