<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Resources/views/layouts/default/layout.yml
 */
class __Oro_Layout_Update_4f9f3009698f477d837c2da6e4c108e52987cd18d7d72eec6c40a08dcb5078f3 implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroEntityConfig/layouts/default/layout.html.twig' );
    }
}
