<?php

/**
 * Filename: /websites/frogdata/crm-application/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Resources/views/layouts/default/default.yml
 */
class __Oro_Layout_Update_afc292719f8e52bc784d2571a794197c9b2fd96b9bbd9d1d9c3b5a8af4ace69f implements Oro\Component\Layout\LayoutUpdateInterface
{
    public function updateLayout(
        Oro\Component\Layout\LayoutManipulatorInterface $layoutManipulator,
        Oro\Component\Layout\LayoutItemInterface $item,
    ) {
        $layoutManipulator->setBlockTheme( '@OroDataGrid/layouts/default/default.html.twig' );
    }
}
