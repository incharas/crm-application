<?php return array (
  '__features__' => 
  array (
    'system_information' => 
    array (
      'label' => 'oro.platform.feature.system_info.label',
      'description' => 'oro.platform.feature.system_info.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_platform_system_info',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.oro_platform_system_info',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'development_settings' => 
    array (
      'label' => 'oro.layout.feature.development_settings.label',
      'description' => 'oro.layout.feature.development_settings.description',
      'toggle' => 'oro_layout.development_settings_feature_enabled',
      'configuration' => 
      array (
        0 => 'development_settings',
        1 => 'development_layout_settings',
      ),
      'dependencies' => 
      array (
      ),
      'routes' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'manage_business_units' => 
    array (
      'label' => 'oro.organization.businessunit.feature.manage.label',
      'description' => 'oro.organization.businessunit.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_business_unit_create',
        1 => 'oro_business_unit_view',
        2 => 'oro_business_unit_search',
        3 => 'oro_business_unit_update',
        4 => 'oro_business_unit_index',
        5 => 'oro_business_unit_widget_info',
        6 => 'oro_business_unit_widget_users',
        7 => 'oro_api_get_businessunits',
        8 => 'oro_api_post_businessunit',
        9 => 'oro_api_put_businessunit',
        10 => 'oro_api_get_businessunit',
        11 => 'oro_api_delete_businessunit',
        12 => 'oro_api_options_businessunits',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\OrganizationBundle\\Entity\\BusinessUnit',
      ),
      'api_resources' => 
      array (
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.users_management.oro_business_unit_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'attachment_post_processors' => 
    array (
      'label' => 'oro.attachment.config.feature.attachment_processors.label',
      'description' => 'oro.attachment.config.feature.attachment_processors.description',
      'configuration' => 
      array (
        0 => 'attachment_processors_settings',
      ),
      'dependencies' => 
      array (
      ),
      'routes' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'attachment_post_processors_allowed' => 
    array (
      'label' => 'oro.attachment.config.feature.attachment_processors_allowed.label',
      'description' => 'oro.attachment.config.feature.attachment_processors_allowed.description',
      'toggle' => 'oro_attachment.processors_allowed',
      'dependencies' => 
      array (
        0 => 'attachment_post_processors',
      ),
      'configuration' => 
      array (
        0 => 'oro_attachment.jpeg_quality',
        1 => 'oro_attachment.png_quality',
      ),
      'routes' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'attachment_post_processing' => 
    array (
      'label' => 'oro.attachment.config.feature.attachment_processing.label',
      'description' => 'oro.attachment.config.feature.attachment_processing.description',
      'dependencies' => 
      array (
        0 => 'attachment_post_processors_allowed',
      ),
      'routes' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'attachment_webp' => 
    array (
      'label' => 'oro.attachment.config.feature.webp.label',
      'description' => 'oro.attachment.config.feature.webp.description',
      'configuration' => 
      array (
        0 => 'oro_attachment.webp_quality',
      ),
      'dependencies' => 
      array (
      ),
      'routes' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'attachment_original_filenames' => 
    array (
      'label' => 'oro.attachment.config.feature.original_file_names_enabled.label',
      'description' => 'oro.attachment.config.feature.original_file_names_enabled.description',
      'toggle' => 'oro_attachment.original_file_names_enabled',
      'dependencies' => 
      array (
      ),
      'routes' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'manage_jobs' => 
    array (
      'label' => 'oro.messagequeue.feature.manage_jobs.label',
      'description' => 'oro.messagequeue.feature.manage_jobs.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_message_queue_root_jobs',
        1 => 'oro_message_queue_child_jobs',
        2 => 'oro_api_message_queue_job_interrupt_root_job',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\MessageQueueBundle\\Entity\\Job',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.oro_message_queue_job',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'email' => 
    array (
      'label' => 'oro.email.feature.label',
      'description' => 'oro.email.feature.description',
      'toggle' => 'oro_email.feature_enabled',
      'routes' => 
      array (
        0 => 'oro_email_purge_emails_attachments',
        1 => 'oro_email_view',
        2 => 'oro_email_last',
        3 => 'oro_email_thread_view',
        4 => 'oro_email_thread_widget',
        5 => 'oro_email_items_view',
        6 => 'oro_email_view_group',
        7 => 'oro_email_activity_view',
        8 => 'oro_email_email_create',
        9 => 'oro_email_email_reply',
        10 => 'oro_email_email_reply_all',
        11 => 'oro_email_email_forward',
        12 => 'oro_email_body',
        13 => 'oro_email_attachment',
        14 => 'oro_resize_email_attachment',
        15 => 'oro_email_body_attachments',
        16 => 'oro_email_attachment_link',
        17 => 'oro_email_widget_emails',
        18 => 'oro_email_widget_base_emails',
        19 => 'oro_email_user_emails',
        20 => 'oro_email_user_sync_emails',
        21 => 'oro_email_user_thread_view',
        22 => 'oro_email_toggle_seen',
        23 => 'oro_email_mark_seen',
        24 => 'oro_email_mark_all_as_seen',
        25 => 'oro_email_mark_massaction',
        26 => 'oro_email_autocomplete_recipient',
        27 => 'oro_email_emailorigin_list',
        28 => 'oro_email_dashboard_recent_emails',
        29 => 'oro_api_get_emails',
        30 => 'oro_api_get_email',
        31 => 'oro_api_put_email',
        32 => 'oro_api_post_email',
        33 => 'oro_api_options_emails',
        34 => 'oro_api_get_emailorigins',
        35 => 'oro_api_get_emailorigin',
        36 => 'oro_api_options_emailorigins',
        37 => 'oro_api_get_email_activity_relations_by_filters',
        38 => 'oro_api_get_activity_email_suggestions',
        39 => 'oro_email_mailbox_update',
        40 => 'oro_email_mailbox_create',
        41 => 'oro_email_mailbox_delete',
        42 => 'oro_email_mailbox_users_search',
        43 => 'oro_api_get_email_activity_relations',
        44 => 'oro_api_get_email_search_relations',
      ),
      'configuration' => 
      array (
        0 => 'user_email_configuration',
        1 => 'email_autocomplete_configuration',
        2 => 'signature_configuration',
        3 => 'email_threads',
        4 => 'reply_configuration',
        5 => 'attachment_configuration',
        6 => 'mailboxes',
        7 => 'user_mailbox',
        8 => 'user_bar_settings',
        9 => 'integration_email_settings',
        10 => 'oro_hangouts_call.enable_google_hangouts_for_email',
        11 => 'google_imap_settings',
      ),
      'sidebar_widgets' => 
      array (
        0 => 'recent_emails',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'recent_emails',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\EmailBundle\\Entity\\Email',
        1 => 'Oro\\Bundle\\EmailBundle\\Entity\\EmailUser',
      ),
      'commands' => 
      array (
        0 => 'oro:cron:email-body-sync',
        1 => 'oro:cron:imap-sync',
      ),
      'cron_jobs' => 
      array (
        0 => 'oro:cron:email-body-sync',
        1 => 'oro:cron:imap-sync',
      ),
      'dependencies' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'manage_email_templates' => 
    array (
      'label' => 'oro.email.emailtemplate.feature.manage.label',
      'description' => 'oro.email.emailtemplate.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_email_emailtemplate_index',
        1 => 'oro_email_emailtemplate_update',
        2 => 'oro_email_emailtemplate_create',
        3 => 'oro_email_emailtemplate_clone',
        4 => 'oro_email_emailtemplate_preview',
        5 => 'oro_api_delete_emailtemplate',
        6 => 'oro_api_get_emailtemplates',
        7 => 'oro_api_get_emailtemplate_variables',
        8 => 'oro_api_get_emailtemplate_compiled',
      ),
      'commands' => 
      array (
        0 => 'oro:email:template:import',
        1 => 'oro:email:template:export',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\EmailBundle\\Entity\\EmailTemplate',
        1 => 'Oro\\Bundle\\EmailBundle\\Entity\\EmailTemplateTranslation',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.emails.oro_email_emailtemplate_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_scheduled_tasks' => 
    array (
      'label' => 'oro.cron.feature.scheduled_tasks.manage.label',
      'description' => 'oro.cron.feature.scheduled_tasks.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_cron_schedule_index',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\CronBundle\\Entity\\Schedule',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.oro_cron_schedule',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'web_api' => 
    array (
      'routes' => 
      array (
        0 => 'oro_user_apigen',
        1 => 'nelmio_api_doc_index',
        2 => 'oro_rest_api_doc_resource',
        3 => 'oro_oauth2_index',
        4 => 'oro_oauth2_view',
        5 => 'oro_oauth2_create',
        6 => 'oro_oauth2_update',
      ),
      'label' => 'oro.api.feature.web_api.label',
      'description' => 'oro.api.feature.web_api.description',
      'toggle' => 'oro_api.web_api',
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.users_management.backoffice_oauth_applications',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_users' => 
    array (
      'label' => 'oro.user.feature.manage.label',
      'description' => 'oro.user.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_user_create',
        1 => 'oro_user_update',
        2 => 'oro_user_index',
        3 => 'oro_user_view',
        4 => 'oro_user_widget_info',
        5 => 'oro_user_mass_password_reset',
        6 => 'oro_user_reset_set_password',
        7 => 'oro_user_config',
        8 => 'oro_api_get_users',
        9 => 'oro_api_get_user',
        10 => 'oro_api_post_user',
        11 => 'oro_api_put_user',
        12 => 'oro_api_delete_user',
        13 => 'oro_api_get_user_filter',
        14 => 'oro_api_options_users',
        15 => 'oro_api_get_user_permissions',
        16 => 'oro_api_options_user_permissions',
      ),
      'operations' => 
      array (
        0 => 'user_enable',
        1 => 'user_disable',
      ),
      'commands' => 
      array (
        0 => 'oro:user:create',
        1 => 'oro:user:list',
        2 => 'oro:user:update',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\UserBundle\\Entity\\User',
      ),
      'api_resources' => 
      array (
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.users_management.user_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_user_roles' => 
    array (
      'label' => 'oro.user.roles.feature.manage.label',
      'description' => 'oro.user.roles.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_user_role_create',
        1 => 'oro_user_role_view',
        2 => 'oro_user_role_update',
        3 => 'oro_user_role_index',
        4 => 'oro_api_get_user_roles',
      ),
      'operations' => 
      array (
        0 => 'clone_role',
        1 => 'oro_user_edit_role',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\UserBundle\\Entity\\Role',
      ),
      'api_resources' => 
      array (
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.users_management.user_roles',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_user_groups' => 
    array (
      'label' => 'oro.user.group.feature.manage.label',
      'description' => 'oro.user.group.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_user_group_create',
        1 => 'oro_user_group_update',
        2 => 'oro_user_group_index',
        3 => 'oro_api_get_user_groups',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\UserBundle\\Entity\\Group',
      ),
      'api_resources' => 
      array (
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.users_management.user_groups',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'user_login_attempts' => 
    array (
      'label' => 'oro.user.userloginattempt.feature.manage.label',
      'description' => 'oro.user.userloginattempt.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_user_login_attempts',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.users_management.login_attempts',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_languages' => 
    array (
      'label' => 'oro.translation.language.feature.manage.label',
      'description' => 'oro.translation.language.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_translation_language_index',
      ),
      'operations' => 
      array (
        0 => 'oro_translation_language_add',
        1 => 'oro_translation_language_disable',
        2 => 'oro_translation_language_enable',
        3 => 'oro_translation_language_import',
        4 => 'oro_translation_language_export',
        5 => 'oro_translation_language_load_base',
        6 => 'oro_translation_language_install',
        7 => 'oro_translation_language_update',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\TranslationBundle\\Entity\\Language',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\TranslationBundle\\Entity\\Language',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.localization.languages',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_translations' => 
    array (
      'label' => 'oro.translation.translation.feature.manage.label',
      'description' => 'oro.translation.translation.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_translation_translation_index',
        1 => 'oro_translation_mass_reset',
        2 => 'oro_api_get_translations',
        3 => 'oro_api_patch_translation',
      ),
      'operations' => 
      array (
        0 => 'oro_translation_translation_reset',
      ),
      'commands' => 
      array (
        0 => 'oro:translation:update',
        1 => 'oro:translation:rebuild-cache',
        2 => 'oro:translation:load',
        3 => 'oro:translation:dump',
        4 => 'oro:translation:dump-files',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\TranslationBundle\\Entity\\Translation',
        1 => 'Oro\\Bundle\\TranslationBundle\\Entity\\TranslationKey',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\TranslationBundle\\Entity\\Translation',
        1 => 'Oro\\Bundle\\TranslationBundle\\Entity\\TranslationKey',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.localization.translations',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_localizations' => 
    array (
      'label' => 'oro.locale.localization.feature.manage.label',
      'description' => 'oro.locale.localization.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_locale_localization_view',
        1 => 'oro_locale_localization_index',
        2 => 'oro_locale_localization_create',
        3 => 'oro_locale_localization_update',
      ),
      'commands' => 
      array (
        0 => 'oro:localization:localized-fallback-values:cleanup-unused',
        1 => 'oro:localization:dump',
        2 => 'oro:localization:update',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\LocaleBundle\\Entity\\Localization',
        1 => 'Oro\\Bundle\\LocaleBundle\\Entity\\LocalizedFallbackValue',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\LocaleBundle\\Entity\\Localization',
        1 => 'Oro\\Bundle\\LocaleBundle\\Entity\\LocalizedFallbackValue',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.localization.localizations',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'entity_management' => 
    array (
      'label' => 'oro.entity_config.feature.entity_management.label',
      'description' => 'oro.entity_config.feature.entity_management.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_entityconfig_index',
        1 => 'oro_entityconfig_view',
        2 => 'oro_entityconfig_update',
        3 => 'oro_entityconfig_fields',
        4 => 'oro_entityconfig_field_update',
        5 => 'oro_entityconfig_field_search',
        6 => 'oro_entityconfig_widget_info',
        7 => 'oro_entityconfig_widget_unique_keys',
        8 => 'oro_entityconfig_widget_entity_fields',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.entities_list.entity_management',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'digital_assets' => 
    array (
      'label' => 'oro.digitalasset.feature.label',
      'description' => 'oro.digitalasset.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_digital_asset_index',
        1 => 'oro_digital_asset_create',
        2 => 'oro_digital_asset_update',
        3 => 'oro_digital_asset_widget_choose',
        4 => 'oro_digital_asset_widget_choose_image',
        5 => 'oro_digital_asset_widget_choose_file',
      ),
      'operations' => 
      array (
        0 => 'digital_asset_delete',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.marketing_tab.digital_asset_list',
        1 => 'application_menu.system_tab.digital_asset_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_integrations' => 
    array (
      'label' => 'oro.integration.feature.manage.label',
      'description' => 'oro.integration.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_integration_index',
        1 => 'oro_integration_create',
        2 => 'oro_integration_update',
        3 => 'oro_integration_schedule',
        4 => 'oro_dotmailer_email_campaign_status',
        5 => 'oro_dotmailer_sync_status',
        6 => 'oro_dotmailer_ping',
        7 => 'oro_dotmailer_integration_connection',
        8 => 'oro_dotmailer_oauth_callback',
        9 => 'oro_dotmailer_oauth_disconnect',
        10 => 'oro_dotmailer_synchronize_adddress_book',
        11 => 'oro_dotmailer_synchronize_adddress_book_datafields',
        12 => 'oro_dotmailer_marketing_list_disconnect',
        13 => 'oro_dotmailer_marketing_list_connect',
        14 => 'oro_dotmailer_marketing_list_buttons',
        15 => 'oro_dotmailer_address_book_create',
        16 => 'oro_dotmailer_datafield_index',
        17 => 'oro_dotmailer_datafield_view',
        18 => 'oro_dotmailer_datafield_info',
        19 => 'oro_dotmailer_datafield_create',
        20 => 'oro_dotmailer_datafield_synchronize',
        21 => 'oro_dotmailer_datafield_mapping_index',
        22 => 'oro_dotmailer_datafield_mapping_update',
        23 => 'oro_dotmailer_datafield_mapping_create',
        24 => 'oro_api_delete_dotmailer_datafield',
        25 => 'oro_api_fields_dotmailer_datafield_mapping',
        26 => 'oro_api_delete_dotmailer_datafield_mapping',
      ),
      'commands' => 
      array (
        0 => 'oro:cron:integration:cleanup',
        1 => 'oro:cron:integration:sync',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\IntegrationBundle\\Entity\\Channel',
        1 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\Activity',
        2 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\AddressBook',
        3 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\AddressBookContact',
        4 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\AddressBookContactsExport',
        5 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\Campaign',
        6 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\CampaignSummary',
        7 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\Contact',
        8 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\DataField',
        9 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\DataFieldMapping',
        10 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\DataFieldMappingConfig',
        11 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\ChangedFieldLog',
        12 => 'Oro\\Bundle\\DotmailerBundle\\Entity\\OAuth',
        13 => 'Extend\\Entity\\EV_Dm_Cnt_Status',
        14 => 'Extend\\Entity\\EV_Dm_Cnt_Email_Type',
        15 => 'Extend\\Entity\\EV_Dm_Cnt_Opt_In_Type',
        16 => 'Extend\\Entity\\EV_Dm_Cmp_Status',
        17 => 'Extend\\Entity\\EV_Dm_Cmp_Reply_Action',
        18 => 'Extend\\Entity\\EV_Dm_Ab_Cnt_Exp_Type',
        19 => 'Extend\\Entity\\EV_Dm_Ab_Visibility',
        20 => 'Extend\\Entity\\EV_Dm_Import_Status',
        21 => 'Extend\\Entity\\EV_Dm_Df_Type',
        22 => 'Extend\\Entity\\EV_Dm_Df_Visibility',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\IntegrationBundle\\Entity\\Channel',
        1 => 'Extend\\Entity\\EV_Dm_Cnt_Status',
        2 => 'Extend\\Entity\\EV_Dm_Cnt_Email_Type',
        3 => 'Extend\\Entity\\EV_Dm_Cnt_Opt_In_Type',
        4 => 'Extend\\Entity\\EV_Dm_Cmp_Status',
        5 => 'Extend\\Entity\\EV_Dm_Cmp_Reply_Action',
        6 => 'Extend\\Entity\\EV_Dm_Ab_Cnt_Exp_Type',
        7 => 'Extend\\Entity\\EV_Dm_Ab_Visibility',
        8 => 'Extend\\Entity\\EV_Dm_Import_Status',
        9 => 'Extend\\Entity\\EV_Dm_Df_Type',
        10 => 'Extend\\Entity\\EV_Dm_Df_Visibility',
      ),
      'operations' => 
      array (
        0 => 'oro_integration_activate',
        1 => 'oro_integration_deactivate',
        2 => 'oro_integration_delete',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.integrations_submenu.integrations_list',
      ),
      'placeholder_items' => 
      array (
        0 => 'force_sync_button',
        1 => 'sync_button',
      ),
      'configuration' => 
      array (
        0 => 'dotmailer_settings',
      ),
      'dependencies' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'email_notification_rules' => 
    array (
      'label' => 'oro.notification.emailnotification.feature.label',
      'description' => 'oro.notification.emailnotification.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_notification_emailnotification_index',
        1 => 'oro_notification_emailnotification_update',
        2 => 'oro_notification_emailnotification_create',
        3 => 'oro_api_delete_emailnotication',
      ),
      'configuration' => 
      array (
        0 => 'oro_notification.email_notification_sender_email',
        1 => 'oro_notification.email_notification_sender_name',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\NotificationBundle\\Entity\\EmailNotification',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\NotificationBundle\\Entity\\EmailNotification',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.emails.oro_notification_emailnotification_list',
      ),
      'dependencies' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'email_mass_notifications' => 
    array (
      'label' => 'oro.notification.massnotification.feature.label',
      'description' => 'oro.notification.massnotification.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_notification_massnotification_index',
        1 => 'oro_notification_massnotification_view',
        2 => 'oro_notification_massnotification_info',
      ),
      'configuration' => 
      array (
        0 => 'oro_notification.mass_notification_recipients',
        1 => 'oro_notification.mass_notification_template',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\NotificationBundle\\Entity\\MassNotification',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\NotificationBundle\\Entity\\MassNotification',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.emails.oro_notification_massnotification_list',
      ),
      'dependencies' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'notification_alerts' => 
    array (
      'label' => 'oro.notification.notificationalert.feature.label',
      'description' => 'oro.notification.notificationalert.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_notification_notificationalert_index',
      ),
      'commands' => 
      array (
        0 => 'oro:notification:alerts:list',
        1 => 'oro:notification:alerts:cleanup',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\NotificationBundle\\Entity\\NotificationAlert',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\NotificationBundle\\Entity\\NotificationAlert',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.oro_notification_notificationalerts_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'activity_lists' => 
    array (
      'label' => 'oro.activitylist.feature.label',
      'description' => 'oro.activitylist.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_activity_list_widget_activities',
        1 => 'oro_activitylist_segment_activitycondition',
        2 => 'oro_activity_list_api_get_list',
        3 => 'oro_activity_list_api_get_item',
        4 => 'oro_api_get_activitylist_activity_list_item',
        5 => 'oro_api_get_activitylist_activity_list_option',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\ActivityListBundle\\Entity\\ActivityList',
        1 => 'Oro\\Bundle\\ActivityListBundle\\Entity\\ActivityOwner',
      ),
      'placeholder_items' => 
      array (
        0 => 'view_oro_activity_list',
        1 => 'update_oro_activity_list',
        2 => 'activity_condition',
      ),
      'configuration' => 
      array (
        0 => 'oro_activity_list.sorting_field',
        1 => 'oro_activity_list.sorting_direction',
        2 => 'oro_activity_list.per_page',
        3 => 'oro_activity_list.grouping',
      ),
      'dependencies' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'system_configuration' => 
    array (
      'label' => 'oro.config.feature.system_configuration.label',
      'description' => 'oro.config.feature.system_configuration.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_config_configuration_system',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.system_configuration',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'data_audit' => 
    array (
      'label' => 'oro.dataaudit.feature.manage.label',
      'description' => 'oro.dataaudit.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_entityconfig_audit',
        1 => 'oro_entityconfig_audit_field',
        2 => 'oro_dataaudit_index',
        3 => 'oro_dataaudit_history',
        4 => 'oro_api_get_audits',
        5 => 'oro_api_get_audit',
        6 => 'oro_api_get_audit_fields',
        7 => 'oro_api_options_audits',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\DataAuditBundle\\Entity\\Audit',
        1 => 'Oro\\Bundle\\DataAuditBundle\\Entity\\AuditField',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.audit_list',
      ),
      'placeholder_items' => 
      array (
        0 => 'change_history_block',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_embedded_forms' => 
    array (
      'label' => 'oro.embeddedform.feature.manage.label',
      'description' => 'oro.embeddedform.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_embedded_form_list',
        1 => 'oro_embedded_form_create',
        2 => 'oro_embedded_form_delete',
        3 => 'oro_embedded_form_default_data',
        4 => 'oro_embedded_form_update',
        5 => 'oro_embedded_form_view',
        6 => 'oro_embedded_form_info',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.integrations_submenu.embedded_forms',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'reports' => 
    array (
      'label' => 'oro.report.feature.label',
      'description' => 'oro.report.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_report_index',
        1 => 'oro_report_view',
        2 => 'oro_report_view_grid',
        3 => 'oro_report_create',
        4 => 'oro_report_update',
        5 => 'oro_report_clone',
        6 => 'oro_api_delete_report',
        7 => 'oro_reportcrm_index',
      ),
      'configuration' => 
      array (
        0 => 'oro_report.display_sql_query',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\ReportBundle\\Entity\\Report',
        1 => 'Oro\\Bundle\\ReportBundle\\Entity\\ReportType',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.reports_tab.manage_reports',
      ),
      'dependencies' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'segments' => 
    array (
      'label' => 'oro.segment.feature.label',
      'description' => 'oro.segment.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_segment_index',
        1 => 'oro_segment_view',
        2 => 'oro_segment_create',
        3 => 'oro_segment_update',
        4 => 'oro_segment_clone',
        5 => 'oro_segment_refresh',
        6 => 'oro_api_delete_segment',
        7 => 'oro_api_post_segment_run',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\SegmentBundle\\Entity\\Segment',
        1 => 'Oro\\Bundle\\SegmentBundle\\Entity\\SegmentSnapshot',
        2 => 'Oro\\Bundle\\SegmentBundle\\Entity\\SegmentType',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.reports_tab.segments_divider',
        1 => 'application_menu.reports_tab.manage_segments',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_tags' => 
    array (
      'label' => 'oro.tag.feature.manage.label',
      'description' => 'oro.tag.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_tag_index',
        1 => 'oro_tag_create',
        2 => 'oro_tag_update',
        3 => 'oro_api_delete_tag',
        4 => 'oro_api_post_taggable',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\TagBundle\\Entity\\Tag',
        1 => 'Oro\\Bundle\\TagBundle\\Entity\\Tagging',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\TagBundle\\Entity\\Tag',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.tags_management.oro_tag_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_taxonomies' => 
    array (
      'label' => 'oro.taxonomy.feature.manage.label',
      'description' => 'oro.taxonomy.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_taxonomy_index',
        1 => 'oro_taxonomy_view',
        2 => 'oro_taxonomy_create',
        3 => 'oro_taxonomy_update',
        4 => 'oro_taxonomy_widget_info',
        5 => 'oro_api_delete_taxonomy',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\TagBundle\\Entity\\Taxonomy',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\TagBundle\\Entity\\Taxonomy',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.tags_management.oro_taxonomy_list',
      ),
      'configuration' => 
      array (
        0 => 'oro_tag.taxonomy_colors',
      ),
      'dependencies' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_workflows' => 
    array (
      'label' => 'oro.workflow.feature.manage.label',
      'description' => 'oro.workflow.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_workflow_definition_index',
        1 => 'oro_workflow_definition_create',
        2 => 'oro_workflow_definition_update',
        3 => 'oro_workflow_definition_configure',
        4 => 'oro_workflow_definition_view',
        5 => 'oro_workflow_definition_activate_from_widget',
      ),
      'commands' => 
      array (
        0 => 'oro:workflow:definitions:load',
        1 => 'oro:debug:workflow:definitions',
        2 => 'oro:workflow:translations:dump',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowDefinition',
        1 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowEntityAcl',
        2 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowEntityAclIdentity',
        3 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowItem',
        4 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowRestriction',
        5 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowRestrictionIdentity',
        6 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowStep',
        7 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowTransitionRecord',
        8 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\TransitionCronTrigger',
        9 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\TransitionEventTrigger',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.workflow_definition_list',
      ),
      'operations' => 
      array (
        0 => 'clone_workflow',
        1 => 'oro_workflow_definition_configure',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_processes' => 
    array (
      'label' => 'oro.workflow.feature.process.manage.label',
      'description' => 'oro.workflow.feature.process.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_process_definition_index',
        1 => 'oro_process_definition_view',
      ),
      'commands' => 
      array (
        0 => 'oro:process:configuration:load',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\ProcessDefinition',
        1 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\ProcessJob',
        2 => 'Oro\\Bundle\\WorkflowBundle\\Entity\\ProcessTrigger',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.process_definition_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'dashboards' => 
    array (
      'label' => 'oro.dashboard.feature.label',
      'description' => 'oro.dashboard.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_dashboard_index',
        1 => 'oro_dashboard_view',
        2 => 'oro_dashboard_configure',
        3 => 'oro_dashboard_update',
        4 => 'oro_dashboard_create',
        5 => 'oro_dashboard_widget',
        6 => 'oro_api_put_dashboard_widget',
        7 => 'oro_api_delete_dashboard_widget',
        8 => 'oro_api_put_dashboard_widget_positions',
        9 => 'oro_api_post_dashboard_widget_add_widget',
        10 => 'oro_api_delete_dashboard',
        11 => 'oro_dashboard_itemized_widget',
        12 => 'oro_dashboard_itemized_data_widget',
        13 => 'oro_dashboard_quick_launchpad',
        14 => 'oro_dashboard_grid',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\DashboardBundle\\Entity\\Dashboard',
        1 => 'Oro\\Bundle\\DashboardBundle\\Entity\\ActiveDashboard',
        2 => 'Oro\\Bundle\\DashboardBundle\\Entity\\Widget',
        3 => 'Oro\\Bundle\\DashboardBundle\\Entity\\WidgetState',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'quick_launchpad',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.dashboard_tab.oro_dashboard_index',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'calendar_events_attendee_notifications' => 
    array (
      'label' => 'oro.calendar.feature.calendar_events_attendee_notifications.label',
      'description' => 'oro.calendar.feature.calendar_events_attendee_notifications.description',
      'allow_if_all_abstain' => true,
      'dependencies' => 
      array (
      ),
      'routes' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'calendar_events_attendee_duplications' => 
    array (
      'label' => 'oro.calendar.feature.calendar_events_attendee_duplications.label',
      'description' => 'oro.calendar.feature.calendar_events_attendee_duplications.description',
      'allow_if_all_abstain' => true,
      'dependencies' => 
      array (
      ),
      'routes' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'calendar' => 
    array (
      'label' => 'oro.calendar.feature.calendar.label',
      'description' => 'oro.calendar.feature.calendar.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_calendar_view_default',
        1 => 'oro_calendar_view',
        2 => 'oro_calendar_event_index',
        3 => 'oro_calendar_event_view',
        4 => 'oro_calendar_event_widget_info',
        5 => 'oro_calendar_event_activity_view',
        6 => 'oro_calendar_event_create',
        7 => 'oro_calendar_event_update',
        8 => 'oro_calendar_event_delete',
        9 => 'oro_system_calendar_index',
        10 => 'oro_system_calendar_view',
        11 => 'oro_system_calendar_create',
        12 => 'oro_system_calendar_update',
        13 => 'oro_system_calendar_widget_events',
        14 => 'oro_system_calendar_event_view',
        15 => 'oro_system_calendar_event_widget_info',
        16 => 'oro_system_calendar_event_create',
        17 => 'oro_system_calendar_event_update',
        18 => 'oro_api_get_calendar_connections',
        19 => 'oro_api_put_calendar_connection',
        20 => 'oro_api_post_calendar_connection',
        21 => 'oro_api_delete_calendar_connection',
        22 => 'oro_api_options_calendar_connections',
        23 => 'oro_api_get_calendarevents',
        24 => 'oro_api_get_calendarevent',
        25 => 'oro_api_get_calendarevent_by_calendar',
        26 => 'oro_api_put_calendarevent',
        27 => 'oro_api_post_calendarevent',
        28 => 'oro_api_delete_calendarevent',
        29 => 'oro_api_options_calendarevents',
        30 => 'oro_api_get_calendar_default',
        31 => 'oro_api_delete_systemcalendar',
        32 => 'oro_api_options_systemcalendars',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\CalendarBundle\\Entity\\Calendar',
        1 => 'Oro\\Bundle\\CalendarBundle\\Entity\\CalendarEvent',
        2 => 'Oro\\Bundle\\CalendarBundle\\Entity\\CalendarProperty',
        3 => 'Oro\\Bundle\\CalendarBundle\\Entity\\Recurrence',
        4 => 'Oro\\Bundle\\CalendarBundle\\Entity\\Attendee',
        5 => 'Oro\\Bundle\\CalendarBundle\\Entity\\SystemCalendar',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.activities_tab.calendar_event_list',
        1 => 'application_menu.usermenu.oro_calendar_view_default',
        2 => 'application_menu.calendar_menu',
        3 => 'application_menu.calendar_menu_mobile',
      ),
      'placeholder_items' => 
      array (
        0 => 'calendar_event_reminder_template',
        1 => 'oro_add_calendar_event_button',
        2 => 'oro_assign_calendar_event_button',
        3 => 'oro_add_calendar_event_link',
        4 => 'oro_assign_calendar_event_link',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'my_calendar',
      ),
      'configuration' => 
      array (
        0 => 'oro_calendar.calendar_colors',
        1 => 'oro_calendar.event_colors',
      ),
      'dependencies' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'manage_contact_groups' => 
    array (
      'label' => 'oro.contact.group.feature.manage.label',
      'description' => 'oro.contact.group.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_contact_group_index',
        1 => 'oro_contact_group_create',
        2 => 'oro_contact_group_update',
        3 => 'oro_api_get_contactgroups',
        4 => 'oro_api_get_contactgroup',
        5 => 'oro_api_put_contactgroup',
        6 => 'oro_api_post_contactgroup',
        7 => 'oro_api_delete_contactgroup',
        8 => 'oro_api_options_contactgroups',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\ContactBundle\\Entity\\Group',
      ),
      'api_resources' => 
      array (
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.contact_group_list',
        1 => 'shortcuts.shortcut_new_contact_group',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_contacts' => 
    array (
      'label' => 'oro.contact.feature.manage.label',
      'description' => 'oro.contact.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_contact_view',
        1 => 'oro_contact_info',
        2 => 'oro_contact_create',
        3 => 'oro_contact_update',
        4 => 'oro_contact_index',
        5 => 'oro_account_widget_contacts',
        6 => 'oro_account_widget_contacts_info',
        7 => 'oro_api_get_contacts',
        8 => 'oro_api_get_contact',
        9 => 'oro_api_put_contact',
        10 => 'oro_api_post_contact',
        11 => 'oro_api_delete_contact',
        12 => 'oro_api_options_contacts',
        13 => 'oro_contact_address_book',
        14 => 'oro_contact_address_create',
        15 => 'oro_contact_address_update',
        16 => 'oro_api_get_contact_address',
        17 => 'oro_api_get_contact_addresses',
        18 => 'oro_api_delete_contact_address',
        19 => 'oro_api_get_contact_address_by_type',
        20 => 'oro_api_get_contact_address_primary',
        21 => 'oro_api_options_contact_addresses',
        22 => 'oro_api_get_contact_phones',
        23 => 'oro_api_get_contact_phone_primary',
        24 => 'oro_api_post_contact_phone',
        25 => 'oro_api_delete_contact_phone',
        26 => 'oro_api_options_contact_phones',
        27 => 'oro_api_post_contact_email',
        28 => 'oro_api_delete_contact_email',
        29 => 'oro_api_options_contact_emails',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\ContactBundle\\Entity\\Contact',
        1 => 'Oro\\Bundle\\ContactBundle\\Entity\\ContactAddress',
        2 => 'Oro\\Bundle\\ContactBundle\\Entity\\ContactEmail',
        3 => 'Oro\\Bundle\\ContactBundle\\Entity\\ContactPhone',
        4 => 'Oro\\Bundle\\ContactBundle\\Entity\\Method',
        5 => 'Oro\\Bundle\\ContactBundle\\Entity\\Source',
      ),
      'api_resources' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
        0 => 'quick_launchpad.contacts',
        1 => 'my_contacts_activity',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.customers_tab.contact_list',
        1 => 'shortcuts.shortcut_new_contact',
        2 => 'shortcuts.shortcut_list_contacts',
      ),
      'placeholder_items' => 
      array (
        0 => 'contacts_launchpad',
        1 => 'oro_add_contact_button',
        2 => 'oro_add_contact_link',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'navigation_menu' => 
    array (
      'label' => 'oro.navigation.menu.feature.label',
      'description' => 'oro.navigation.menu.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_navigation_global_menu_index',
        1 => 'oro_navigation_global_menu_view',
        2 => 'oro_navigation_global_menu_create',
        3 => 'oro_navigation_global_menu_update',
        4 => 'oro_navigation_global_menu_move',
        5 => 'oro_navigation_global_menu_ajax_reset',
        6 => 'oro_navigation_global_menu_ajax_create',
        7 => 'oro_navigation_global_menu_ajax_delete',
        8 => 'oro_navigation_global_menu_ajax_show',
        9 => 'oro_navigation_global_menu_ajax_hide',
        10 => 'oro_navigation_global_menu_ajax_move',
        11 => 'oro_navigation_user_menu_index',
        12 => 'oro_navigation_user_menu_view',
        13 => 'oro_navigation_user_menu_create',
        14 => 'oro_navigation_user_menu_update',
        15 => 'oro_navigation_user_menu_move',
        16 => 'oro_navigation_user_menu_ajax_reset',
        17 => 'oro_navigation_user_menu_ajax_create',
        18 => 'oro_navigation_user_menu_ajax_delete',
        19 => 'oro_navigation_user_menu_ajax_show',
        20 => 'oro_navigation_user_menu_ajax_hide',
        21 => 'oro_navigation_user_menu_ajax_move',
      ),
      'commands' => 
      array (
        0 => 'oro:navigation:menu:reset',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\NavigationBundle\\Entity\\MenuUpdate',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.menu_list_default',
      ),
      'placeholder_items' => 
      array (
        0 => 'oro_edit_menus_button',
        1 => 'oro_edit_menus_link',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'marketing_list' => 
    array (
      'label' => 'oro.marketinglist.feature.label',
      'description' => 'oro.marketinglist.feature.description',
      'toggle' => 'oro_marketing_list.feature_enabled',
      'routes' => 
      array (
        0 => 'oro_marketing_list_index',
        1 => 'oro_marketing_list_view',
        2 => 'oro_marketing_list_create',
        3 => 'oro_marketing_list_update',
        4 => 'oro_api_contact_marketinglist_information_field_type',
        5 => 'oro_api_entity_marketinglist_contact_information_fields',
        6 => 'oro_api_delete_marketinglist',
        7 => 'oro_api_remove_marketinglist_removeditem',
        8 => 'oro_api_unremove_marketinglist_removeditem',
        9 => 'oro_api_post_marketinglist_removeditem',
        10 => 'oro_api_delete_marketinglist_removeditem',
        11 => 'oro_api_unsubscribe_marketinglist_unsubscribeditem',
        12 => 'oro_api_subscribe_marketinglist_unsubscribeditem',
        13 => 'oro_api_post_marketinglist_unsubscribeditem',
        14 => 'oro_api_delete_marketinglist_unsubscribeditem',
        15 => 'oro_api_post_marketinglist_segment_run',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\MarketingListBundle\\Entity\\MarketingList',
        1 => 'Oro\\Bundle\\MarketingListBundle\\Entity\\MarketingListItem',
        2 => 'Oro\\Bundle\\MarketingListBundle\\Entity\\MarketingListType',
      ),
      'mq_topics' => 
      array (
        0 => 'oro_marketing_list.message_queue.job.update_marketing_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'api_resources' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'tracking' => 
    array (
      'label' => 'oro.tracking.feature.label',
      'description' => 'oro.tracking.feature.description',
      'toggle' => 'oro_tracking.feature_enabled',
      'routes' => 
      array (
        0 => 'oro_tracking_data_create',
        1 => 'oro_tracking_website_index',
        2 => 'oro_tracking_website_create',
        3 => 'oro_tracking_website_update',
        4 => 'oro_tracking_website_view',
        5 => 'oro_api_delete_tracking_website',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\TrackingBundle\\Entity\\TrackingWebsite',
      ),
      'commands' => 
      array (
        0 => 'oro:cron:import-tracking',
        1 => 'oro:cron:tracking:parse',
      ),
      'cron_jobs' => 
      array (
        0 => 'oro:cron:import-tracking',
        1 => 'oro:cron:tracking:parse',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'manage_accounts' => 
    array (
      'label' => 'oro.account.feature.manage.label',
      'description' => 'oro.account.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_account_view',
        1 => 'oro_account_create',
        2 => 'oro_account_update',
        3 => 'oro_account_index',
        4 => 'oro_account_widget_contacts',
        5 => 'oro_account_widget_contacts_info',
        6 => 'oro_account_widget_info',
        7 => 'oro_api_get_accounts',
        8 => 'oro_api_get_account',
        9 => 'oro_api_put_account',
        10 => 'oro_api_post_account',
        11 => 'oro_api_delete_account',
        12 => 'oro_api_options_accounts',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\AccountBundle\\Entity\\Account',
      ),
      'api_resources' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
        0 => 'my_accounts_activity',
        1 => 'quick_launchpad.accounts',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.customers_tab.account_list',
        1 => 'shortcuts.shortcut_new_account',
        2 => 'shortcuts.shortcut_list_accounts',
      ),
      'placeholder_items' => 
      array (
        0 => 'accounts_launchpad',
        1 => 'account_lifetime_value',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'call' => 
    array (
      'label' => 'oro.call.feature.label',
      'description' => 'oro.call.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_call_index',
        1 => 'oro_call_view',
        2 => 'oro_call_create',
        3 => 'oro_call_update',
        4 => 'oro_call_activity_view',
        5 => 'oro_call_widget_calls',
        6 => 'oro_call_base_widget_calls',
        7 => 'oro_call_widget_info',
        8 => 'oro_api_get_calls',
        9 => 'oro_api_get_call',
        10 => 'oro_api_put_call',
        11 => 'oro_api_post_call',
        12 => 'oro_api_delete_call',
        13 => 'oro_api_options_calls',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\CallBundle\\Entity\\Call',
        1 => 'Oro\\Bundle\\CallBundle\\Entity\\CallDirection',
        2 => 'Oro\\Bundle\\CallBundle\\Entity\\CallStatus',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\CallBundle\\Entity\\Call',
        1 => 'Oro\\Bundle\\CallBundle\\Entity\\CallDirection',
        2 => 'Oro\\Bundle\\CallBundle\\Entity\\CallStatus',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.activities_tab.call_list',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'recent_calls',
      ),
      'placeholder_items' => 
      array (
        0 => 'oro_log_call_button',
        1 => 'oro_log_call_link',
        2 => 'oro_phone_action_log_call_button',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'campaign' => 
    array (
      'label' => 'oro.campaign.feature.label',
      'description' => 'oro.campaign.feature.description',
      'toggle' => 'oro_campaign.feature_enabled',
      'routes' => 
      array (
        0 => 'oro_campaign_index',
        1 => 'oro_campaign_create',
        2 => 'oro_campaign_update',
        3 => 'oro_campaign_view',
        4 => 'oro_campaign_event_plot',
        5 => 'oro_email_campaign_index',
        6 => 'oro_email_campaign_create',
        7 => 'oro_email_campaign_update',
        8 => 'oro_email_campaign_view',
        9 => 'oro_email_campaign_send',
        10 => 'oro_campaign_dashboard_campaigns_leads_chart',
        11 => 'oro_campaign_dashboard_campaigns_opportunity_chart',
        12 => 'oro_campaign_dashboard_campaigns_by_close_revenue_chart',
        13 => 'oro_api_get_emailcampaign_email_templates',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\CampaignBundle\\Entity\\Campaign',
        1 => 'Oro\\Bundle\\CampaignBundle\\Entity\\EmailCampaign',
      ),
      'configuration' => 
      array (
        0 => 'email_campaign_settings',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'campaigns_leads',
        1 => 'campaigns_opportunity',
        2 => 'campaigns_by_close_revenue',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\CampaignBundle\\Entity\\Campaign',
        1 => 'Oro\\Bundle\\CampaignBundle\\Entity\\EmailCampaign',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu > reports_tab > oro.campaign.entity_plural_label_report_tab',
      ),
      'commands' => 
      array (
        0 => 'oro:cron:send-email-campaigns',
      ),
      'cron_jobs' => 
      array (
        0 => 'oro:cron:send-email-campaigns',
      ),
      'dependencies' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'channels' => 
    array (
      'label' => 'oro.channel.feature.label',
      'description' => 'oro.channel.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_channel_index',
        1 => 'oro_channel_create',
        2 => 'oro_channel_update',
        3 => 'oro_channel_view',
        4 => 'oro_channel_widget_info',
        5 => 'oro_channel_integration_create',
        6 => 'oro_channel_integration_update',
        7 => 'oro_api_get_channels',
        8 => 'oro_api_options_channels',
        9 => 'oro_api_delete_channel',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\ChannelBundle\\Entity\\Channel',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\ChannelBundle\\Entity\\Channel',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'e_commerce',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.channels_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'task' => 
    array (
      'label' => 'oro.task.feature.label',
      'description' => 'oro.task.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_task_index',
        1 => 'oro_task_view',
        2 => 'oro_task_create',
        3 => 'oro_task_update',
        4 => 'oro_task_widget_sidebar_tasks',
        5 => 'oro_task_widget_info',
        6 => 'oro_task_activity_view',
        7 => 'oro_task_user_tasks',
        8 => 'oro_task_my_tasks',
        9 => 'oro_api_get_tasks',
        10 => 'oro_api_get_task',
        11 => 'oro_api_put_task',
        12 => 'oro_api_post_task',
        13 => 'oro_api_delete_task',
        14 => 'oro_api_options_tasks',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\TaskBundle\\Entity\\Task',
        1 => 'Oro\\Bundle\\TaskBundle\\Entity\\TaskPriority',
        2 => 'Extend\\Entity\\EV_Task_Status',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\TaskBundle\\Entity\\Task',
        1 => 'Oro\\Bundle\\TaskBundle\\Entity\\TaskPriority',
        2 => 'Extend\\Entity\\EV_Task_Status',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.activities_tab.task_list',
        1 => 'usermenu.oro_task_my_tasks',
      ),
      'sidebar_widgets' => 
      array (
        0 => 'assigned_tasks',
      ),
      'placeholder_items' => 
      array (
        0 => 'task_reminder_template',
        1 => 'oro_user_tasks',
        2 => 'oro_assign_task_button',
        3 => 'oro_assign_task_link',
        4 => 'oro_add_task_button',
        5 => 'oro_add_task_link',
        6 => 'oro_my_tasks_button',
      ),
      'workflows' => 
      array (
        0 => 'task_flow',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'marketingactivity' => 
    array (
      'label' => 'oro.marketingactivity.feature.label',
      'description' => 'oro.marketingactivity.feature.description',
      'toggle' => 'oro_marketing_activity.feature_enabled',
      'dependencies' => 
      array (
        0 => 'campaign',
      ),
      'routes' => 
      array (
        0 => 'oro_marketing_activity_widget_summary',
        1 => 'oro_marketing_activity_widget_marketing_activities',
        2 => 'oro_marketing_activity_widget_marketing_activities_info',
        3 => 'oro_marketing_activity_widget_marketing_activities_list',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\MarketingActivityBundle\\Entity\\MarketingActivity',
      ),
      'configuration' => 
      array (
      ),
      'entities' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
      'navigation_items' => 
      array (
      ),
    ),
    'sales_lead' => 
    array (
      'label' => 'oro.sales.lead.feature.label',
      'description' => 'oro.sales.lead.feature.description',
      'toggle' => 'oro_sales.lead_feature_enabled',
      'routes' => 
      array (
        0 => 'oro_sales_lead_view',
        1 => 'oro_sales_lead_info',
        2 => 'oro_sales_lead_create',
        3 => 'oro_sales_lead_update',
        4 => 'oro_sales_lead_index',
        5 => 'oro_sales_widget_account_leads',
        6 => 'oro_sales_lead_data_channel_aware_create',
        7 => 'oro_sales_lead_disqualify',
        8 => 'oro_sales_lead_convert_to_opportunity',
        9 => 'oro_sales_b2bcustomer_widget_leads',
      ),
      'workflows' => 
      array (
        0 => 'b2b_flow_lead',
      ),
      'processes' => 
      array (
        0 => 'convert_mailbox_email_to_lead',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\SalesBundle\\Entity\\Lead',
        1 => 'Oro\\Bundle\\SalesBundle\\Entity\\LeadAddress',
        2 => 'Oro\\Bundle\\SalesBundle\\Entity\\LeadEmail',
        3 => 'Oro\\Bundle\\SalesBundle\\Entity\\LeadPhone',
        4 => 'Extend\\Entity\\EV_Lead_Status',
        5 => 'Extend\\Entity\\EV_Lead_Source',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\SalesBundle\\Entity\\Lead',
        1 => 'Oro\\Bundle\\SalesBundle\\Entity\\LeadAddress',
        2 => 'Oro\\Bundle\\SalesBundle\\Entity\\LeadEmail',
        3 => 'Oro\\Bundle\\SalesBundle\\Entity\\LeadPhone',
      ),
      'placeholder_items' => 
      array (
        0 => 'leads_launchpad',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'leads_list',
        1 => 'campaigns_leads',
        2 => 'campaigns_by_close_revenue',
        3 => 'quick_launchpad.leads',
        4 => 'lead_statistics',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.reports_tab.static_reports_tab.leads_report_tab.leads_by_date',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
    ),
    'sales_opportunity' => 
    array (
      'label' => 'oro.sales.opportunity.feature.label',
      'description' => 'oro.sales.opportunity.feature.description',
      'toggle' => 'oro_sales.opportunity_feature_enabled',
      'routes' => 
      array (
        0 => 'oro_sales_opportunity_view',
        1 => 'oro_sales_opportunity_info',
        2 => 'oro_sales_opportunity_create',
        3 => 'oro_sales_opportunity_update',
        4 => 'oro_sales_opportunity_index',
        5 => 'oro_sales_opportunity_data_channel_aware_create',
        6 => 'oro_sales_opportunity_customer_aware_create',
        7 => 'oro_sales_b2bcustomer_widget_opportunities',
      ),
      'workflows' => 
      array (
        0 => 'opportunity_flow',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\SalesBundle\\Entity\\Opportunity',
        1 => 'Oro\\Bundle\\SalesBundle\\Entity\\OpportunityCloseReason',
        2 => 'Extend\\Entity\\EV_Opportunity_Status',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\SalesBundle\\Entity\\Opportunity',
        1 => 'Oro\\Bundle\\SalesBundle\\Entity\\OpportunityCloseReason',
      ),
      'configuration' => 
      array (
        0 => 'opportunity_status_probabilities',
        1 => 'display_settings',
      ),
      'placeholder_items' => 
      array (
        0 => 'opportunities_launchpad',
        1 => 'oro_sales_create_opportunity_link',
        2 => 'oro_sales_create_opportunity_button',
      ),
      'dashboard_widgets' => 
      array (
        0 => 'quick_launchpad.opportunities',
        1 => 'opportunities_by_lead_source_chart',
        2 => 'opportunities_by_state',
        3 => 'campaigns_opportunity',
        4 => 'forecast_of_opportunities',
        5 => 'average_lifetime_sales_chart',
        6 => 'campaigns_by_close_revenue',
        7 => 'opportunities_list',
        8 => 'opportunity_statistics',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.reports_tab.static_reports_tab.opportunities_report_tab.opportunities_by_status',
        1 => 'application_menu.reports_tab.static_reports_tab.accounts_report_tab.accounts_by_opportunities',
        2 => 'application_menu.reports_tab.static_reports_tab.opportunities_report_tab.won_by_period',
        3 => 'application_menu.reports_tab.static_reports_tab.opportunities_report_tab.total_forecast',
        4 => 'application_menu.reports_tab.static_reports_tab.accounts_report_tab.accounts_life_time_value',
      ),
      'commands' => 
      array (
        0 => 'oro:b2b:lifetime:recalculate',
      ),
      'dependencies' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'processes' => 
      array (
      ),
    ),
    'business_customers' => 
    array (
      'label' => 'oro.sales.b2bcustomer.feature.label',
      'description' => 'oro.sales.b2bcustomer.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_sales_b2bcustomer_index',
        1 => 'oro_sales_b2bcustomer_view',
        2 => 'oro_sales_b2bcustomer_widget_info',
        3 => 'oro_sales_b2bcustomer_create',
        4 => 'oro_sales_b2bcustomer_update',
        5 => 'oro_sales_widget_b2bcustomer_info',
        6 => 'oro_api_get_b2bcustomers',
        7 => 'oro_api_get_b2bcustomer',
        8 => 'oro_api_put_b2bcustomer',
        9 => 'oro_api_post_b2bcustomer',
        10 => 'oro_api_delete_b2bcustomer',
        11 => 'oro_api_options_b2bcustomers',
        12 => 'oro_api_post_b2bcustomer_email',
        13 => 'oro_api_delete_b2bcustomer_email',
        14 => 'oro_api_options_b2bcustomer_emails',
        15 => 'oro_api_get_b2bcustomer_phones',
        16 => 'oro_api_get_b2bcustomer_phone_primary',
        17 => 'oro_api_post_b2bcustomer_phone',
        18 => 'oro_api_delete_b2bcustomer_phone',
        19 => 'oro_api_options_b2bcustomer_phones',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomer',
        1 => 'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerEmail',
        2 => 'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerPhone',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomer',
        1 => 'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerEmail',
        2 => 'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerPhone',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.customers_tab.b2bcustomer_list',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'contact_requests' => 
    array (
      'label' => 'oro.contactus.contactrequest.feature.label',
      'description' => 'oro.contactus.contactrequest.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_contactus_request_index',
        1 => 'oro_contactus_request_view',
        2 => 'oro_contactus_request_info',
        3 => 'oro_contactus_request_create',
        4 => 'oro_contactus_request_update',
        5 => 'oro_contactus_request_delete',
        6 => 'oro_api_get_contactrequest',
      ),
      'workflows' => 
      array (
        0 => 'orocrm_contact_us_contact_request',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\ContactUsBundle\\Entity\\ContactRequest',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\ContactUsBundle\\Entity\\ContactRequest',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.activities_tab.oro_contactus_requests',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'manage_contact_reasons' => 
    array (
      'label' => 'oro.contactus.contactreason.feature.manage.label',
      'description' => 'oro.contactus.contactreason.feature.manage.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_contactus_reason_index',
        1 => 'oro_contactus_reason_create',
        2 => 'oro_contactus_reason_update',
        3 => 'oro_contactus_reason_delete',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\ContactUsBundle\\Entity\\ContactReason',
      ),
      'api_resources' => 
      array (
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.system_tab.oro_contactus_reasons',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'placeholder_items' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'processes' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
    'case' => 
    array (
      'label' => 'oro.case.feature.label',
      'description' => 'oro.case.feature.description',
      'allow_if_all_abstain' => true,
      'routes' => 
      array (
        0 => 'oro_case_index',
        1 => 'oro_case_view',
        2 => 'oro_case_create',
        3 => 'oro_case_update',
        4 => 'oro_case_account_widget_cases',
        5 => 'oro_case_contact_widget_cases',
        6 => 'oro_case_comment_list',
        7 => 'oro_case_comment_create',
        8 => 'oro_case_comment_update',
        9 => 'oro_case_widget_comments',
        10 => 'oro_case_api_get_cases',
        11 => 'oro_case_api_get_case',
        12 => 'oro_case_api_put_case',
        13 => 'oro_case_api_post_case',
        14 => 'oro_case_api_delete_case',
        15 => 'oro_case_api_options_cases',
        16 => 'oro_case_api_get_comments',
        17 => 'oro_case_api_get_comment',
        18 => 'oro_case_api_put_comment',
        19 => 'oro_case_api_post_comment',
        20 => 'oro_case_api_delete_comment',
        21 => 'oro_case_api_options_comments',
      ),
      'entities' => 
      array (
        0 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseEntity',
        1 => 'Oro\\Bundle\\CaseBundle\\Entity\\CasePriority',
        2 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseSource',
        3 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseStatus',
        4 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseComment',
        5 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseMailboxProcessSettings',
      ),
      'api_resources' => 
      array (
        0 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseEntity',
        1 => 'Oro\\Bundle\\CaseBundle\\Entity\\CasePriority',
        2 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseSource',
        3 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseStatus',
        4 => 'Oro\\Bundle\\CaseBundle\\Entity\\CaseComment',
      ),
      'navigation_items' => 
      array (
        0 => 'application_menu.activities_tab.oro_case_index',
      ),
      'placeholder_items' => 
      array (
        0 => 'oro_user_cases_grid',
        1 => 'oro_account_cases_grid',
        2 => 'oro_contact_cases_grid',
      ),
      'processes' => 
      array (
        0 => 'convert_mailbox_email_to_case',
      ),
      'dependencies' => 
      array (
      ),
      'configuration' => 
      array (
      ),
      'commands' => 
      array (
      ),
      'mq_topics' => 
      array (
      ),
      'cron_jobs' => 
      array (
      ),
      'operations' => 
      array (
      ),
      'sidebar_widgets' => 
      array (
      ),
      'workflows' => 
      array (
      ),
      'dashboard_widgets' => 
      array (
      ),
    ),
  ),
  '__internal__' => 
  array (
    'dependencies' => 
    array (
      'system_information' => 
      array (
      ),
      'development_settings' => 
      array (
      ),
      'manage_business_units' => 
      array (
      ),
      'attachment_post_processors' => 
      array (
      ),
      'attachment_post_processors_allowed' => 
      array (
        0 => 'attachment_post_processors',
      ),
      'attachment_post_processing' => 
      array (
        0 => 'attachment_post_processors_allowed',
        1 => 'attachment_post_processors',
      ),
      'attachment_webp' => 
      array (
      ),
      'attachment_original_filenames' => 
      array (
      ),
      'manage_jobs' => 
      array (
      ),
      'email' => 
      array (
      ),
      'manage_email_templates' => 
      array (
      ),
      'manage_scheduled_tasks' => 
      array (
      ),
      'web_api' => 
      array (
      ),
      'manage_users' => 
      array (
      ),
      'manage_user_roles' => 
      array (
      ),
      'manage_user_groups' => 
      array (
      ),
      'user_login_attempts' => 
      array (
      ),
      'manage_languages' => 
      array (
      ),
      'manage_translations' => 
      array (
      ),
      'manage_localizations' => 
      array (
      ),
      'entity_management' => 
      array (
      ),
      'digital_assets' => 
      array (
      ),
      'manage_integrations' => 
      array (
      ),
      'email_notification_rules' => 
      array (
      ),
      'email_mass_notifications' => 
      array (
      ),
      'notification_alerts' => 
      array (
      ),
      'activity_lists' => 
      array (
      ),
      'system_configuration' => 
      array (
      ),
      'data_audit' => 
      array (
      ),
      'manage_embedded_forms' => 
      array (
      ),
      'reports' => 
      array (
      ),
      'segments' => 
      array (
      ),
      'manage_tags' => 
      array (
      ),
      'manage_taxonomies' => 
      array (
      ),
      'manage_workflows' => 
      array (
      ),
      'manage_processes' => 
      array (
      ),
      'dashboards' => 
      array (
      ),
      'calendar_events_attendee_notifications' => 
      array (
      ),
      'calendar_events_attendee_duplications' => 
      array (
      ),
      'calendar' => 
      array (
      ),
      'manage_contact_groups' => 
      array (
      ),
      'manage_contacts' => 
      array (
      ),
      'navigation_menu' => 
      array (
      ),
      'marketing_list' => 
      array (
      ),
      'tracking' => 
      array (
      ),
      'manage_accounts' => 
      array (
      ),
      'call' => 
      array (
      ),
      'campaign' => 
      array (
      ),
      'channels' => 
      array (
      ),
      'task' => 
      array (
      ),
      'marketingactivity' => 
      array (
        0 => 'campaign',
      ),
      'sales_lead' => 
      array (
      ),
      'sales_opportunity' => 
      array (
      ),
      'business_customers' => 
      array (
      ),
      'contact_requests' => 
      array (
      ),
      'manage_contact_reasons' => 
      array (
      ),
      'case' => 
      array (
      ),
    ),
    'dependent_features' => 
    array (
      'system_information' => 
      array (
      ),
      'development_settings' => 
      array (
      ),
      'manage_business_units' => 
      array (
      ),
      'attachment_post_processors' => 
      array (
        0 => 'attachment_post_processors_allowed',
        1 => 'attachment_post_processing',
      ),
      'attachment_post_processors_allowed' => 
      array (
        0 => 'attachment_post_processing',
      ),
      'attachment_post_processing' => 
      array (
      ),
      'attachment_webp' => 
      array (
      ),
      'attachment_original_filenames' => 
      array (
      ),
      'manage_jobs' => 
      array (
      ),
      'email' => 
      array (
      ),
      'manage_email_templates' => 
      array (
      ),
      'manage_scheduled_tasks' => 
      array (
      ),
      'web_api' => 
      array (
      ),
      'manage_users' => 
      array (
      ),
      'manage_user_roles' => 
      array (
      ),
      'manage_user_groups' => 
      array (
      ),
      'user_login_attempts' => 
      array (
      ),
      'manage_languages' => 
      array (
      ),
      'manage_translations' => 
      array (
      ),
      'manage_localizations' => 
      array (
      ),
      'entity_management' => 
      array (
      ),
      'digital_assets' => 
      array (
      ),
      'manage_integrations' => 
      array (
      ),
      'email_notification_rules' => 
      array (
      ),
      'email_mass_notifications' => 
      array (
      ),
      'notification_alerts' => 
      array (
      ),
      'activity_lists' => 
      array (
      ),
      'system_configuration' => 
      array (
      ),
      'data_audit' => 
      array (
      ),
      'manage_embedded_forms' => 
      array (
      ),
      'reports' => 
      array (
      ),
      'segments' => 
      array (
      ),
      'manage_tags' => 
      array (
      ),
      'manage_taxonomies' => 
      array (
      ),
      'manage_workflows' => 
      array (
      ),
      'manage_processes' => 
      array (
      ),
      'dashboards' => 
      array (
      ),
      'calendar_events_attendee_notifications' => 
      array (
      ),
      'calendar_events_attendee_duplications' => 
      array (
      ),
      'calendar' => 
      array (
      ),
      'manage_contact_groups' => 
      array (
      ),
      'manage_contacts' => 
      array (
      ),
      'navigation_menu' => 
      array (
      ),
      'marketing_list' => 
      array (
      ),
      'tracking' => 
      array (
      ),
      'manage_accounts' => 
      array (
      ),
      'call' => 
      array (
      ),
      'campaign' => 
      array (
        0 => 'marketingactivity',
      ),
      'channels' => 
      array (
      ),
      'task' => 
      array (
      ),
      'marketingactivity' => 
      array (
      ),
      'sales_lead' => 
      array (
      ),
      'sales_opportunity' => 
      array (
      ),
      'business_customers' => 
      array (
      ),
      'contact_requests' => 
      array (
      ),
      'manage_contact_reasons' => 
      array (
      ),
      'case' => 
      array (
      ),
    ),
    'by_resource' => 
    array (
      'routes' => 
      array (
        'oro_platform_system_info' => 
        array (
          0 => 'system_information',
        ),
        'oro_business_unit_create' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_business_unit_view' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_business_unit_search' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_business_unit_update' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_business_unit_index' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_business_unit_widget_info' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_business_unit_widget_users' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_api_get_businessunits' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_api_post_businessunit' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_api_put_businessunit' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_api_get_businessunit' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_api_delete_businessunit' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_api_options_businessunits' => 
        array (
          0 => 'manage_business_units',
        ),
        'oro_message_queue_root_jobs' => 
        array (
          0 => 'manage_jobs',
        ),
        'oro_message_queue_child_jobs' => 
        array (
          0 => 'manage_jobs',
        ),
        'oro_api_message_queue_job_interrupt_root_job' => 
        array (
          0 => 'manage_jobs',
        ),
        'oro_email_purge_emails_attachments' => 
        array (
          0 => 'email',
        ),
        'oro_email_view' => 
        array (
          0 => 'email',
        ),
        'oro_email_last' => 
        array (
          0 => 'email',
        ),
        'oro_email_thread_view' => 
        array (
          0 => 'email',
        ),
        'oro_email_thread_widget' => 
        array (
          0 => 'email',
        ),
        'oro_email_items_view' => 
        array (
          0 => 'email',
        ),
        'oro_email_view_group' => 
        array (
          0 => 'email',
        ),
        'oro_email_activity_view' => 
        array (
          0 => 'email',
        ),
        'oro_email_email_create' => 
        array (
          0 => 'email',
        ),
        'oro_email_email_reply' => 
        array (
          0 => 'email',
        ),
        'oro_email_email_reply_all' => 
        array (
          0 => 'email',
        ),
        'oro_email_email_forward' => 
        array (
          0 => 'email',
        ),
        'oro_email_body' => 
        array (
          0 => 'email',
        ),
        'oro_email_attachment' => 
        array (
          0 => 'email',
        ),
        'oro_resize_email_attachment' => 
        array (
          0 => 'email',
        ),
        'oro_email_body_attachments' => 
        array (
          0 => 'email',
        ),
        'oro_email_attachment_link' => 
        array (
          0 => 'email',
        ),
        'oro_email_widget_emails' => 
        array (
          0 => 'email',
        ),
        'oro_email_widget_base_emails' => 
        array (
          0 => 'email',
        ),
        'oro_email_user_emails' => 
        array (
          0 => 'email',
        ),
        'oro_email_user_sync_emails' => 
        array (
          0 => 'email',
        ),
        'oro_email_user_thread_view' => 
        array (
          0 => 'email',
        ),
        'oro_email_toggle_seen' => 
        array (
          0 => 'email',
        ),
        'oro_email_mark_seen' => 
        array (
          0 => 'email',
        ),
        'oro_email_mark_all_as_seen' => 
        array (
          0 => 'email',
        ),
        'oro_email_mark_massaction' => 
        array (
          0 => 'email',
        ),
        'oro_email_autocomplete_recipient' => 
        array (
          0 => 'email',
        ),
        'oro_email_emailorigin_list' => 
        array (
          0 => 'email',
        ),
        'oro_email_dashboard_recent_emails' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_emails' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_email' => 
        array (
          0 => 'email',
        ),
        'oro_api_put_email' => 
        array (
          0 => 'email',
        ),
        'oro_api_post_email' => 
        array (
          0 => 'email',
        ),
        'oro_api_options_emails' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_emailorigins' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_emailorigin' => 
        array (
          0 => 'email',
        ),
        'oro_api_options_emailorigins' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_email_activity_relations_by_filters' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_activity_email_suggestions' => 
        array (
          0 => 'email',
        ),
        'oro_email_mailbox_update' => 
        array (
          0 => 'email',
        ),
        'oro_email_mailbox_create' => 
        array (
          0 => 'email',
        ),
        'oro_email_mailbox_delete' => 
        array (
          0 => 'email',
        ),
        'oro_email_mailbox_users_search' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_email_activity_relations' => 
        array (
          0 => 'email',
        ),
        'oro_api_get_email_search_relations' => 
        array (
          0 => 'email',
        ),
        'oro_email_emailtemplate_index' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_email_emailtemplate_update' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_email_emailtemplate_create' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_email_emailtemplate_clone' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_email_emailtemplate_preview' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_api_delete_emailtemplate' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_api_get_emailtemplates' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_api_get_emailtemplate_variables' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_api_get_emailtemplate_compiled' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro_cron_schedule_index' => 
        array (
          0 => 'manage_scheduled_tasks',
        ),
        'oro_user_apigen' => 
        array (
          0 => 'web_api',
        ),
        'nelmio_api_doc_index' => 
        array (
          0 => 'web_api',
        ),
        'oro_rest_api_doc_resource' => 
        array (
          0 => 'web_api',
        ),
        'oro_oauth2_index' => 
        array (
          0 => 'web_api',
        ),
        'oro_oauth2_view' => 
        array (
          0 => 'web_api',
        ),
        'oro_oauth2_create' => 
        array (
          0 => 'web_api',
        ),
        'oro_oauth2_update' => 
        array (
          0 => 'web_api',
        ),
        'oro_user_create' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_update' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_index' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_view' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_widget_info' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_mass_password_reset' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_reset_set_password' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_config' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_get_users' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_get_user' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_post_user' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_put_user' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_delete_user' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_get_user_filter' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_options_users' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_get_user_permissions' => 
        array (
          0 => 'manage_users',
        ),
        'oro_api_options_user_permissions' => 
        array (
          0 => 'manage_users',
        ),
        'oro_user_role_create' => 
        array (
          0 => 'manage_user_roles',
        ),
        'oro_user_role_view' => 
        array (
          0 => 'manage_user_roles',
        ),
        'oro_user_role_update' => 
        array (
          0 => 'manage_user_roles',
        ),
        'oro_user_role_index' => 
        array (
          0 => 'manage_user_roles',
        ),
        'oro_api_get_user_roles' => 
        array (
          0 => 'manage_user_roles',
        ),
        'oro_user_group_create' => 
        array (
          0 => 'manage_user_groups',
        ),
        'oro_user_group_update' => 
        array (
          0 => 'manage_user_groups',
        ),
        'oro_user_group_index' => 
        array (
          0 => 'manage_user_groups',
        ),
        'oro_api_get_user_groups' => 
        array (
          0 => 'manage_user_groups',
        ),
        'oro_user_login_attempts' => 
        array (
          0 => 'user_login_attempts',
        ),
        'oro_translation_language_index' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_translation_index' => 
        array (
          0 => 'manage_translations',
        ),
        'oro_translation_mass_reset' => 
        array (
          0 => 'manage_translations',
        ),
        'oro_api_get_translations' => 
        array (
          0 => 'manage_translations',
        ),
        'oro_api_patch_translation' => 
        array (
          0 => 'manage_translations',
        ),
        'oro_locale_localization_view' => 
        array (
          0 => 'manage_localizations',
        ),
        'oro_locale_localization_index' => 
        array (
          0 => 'manage_localizations',
        ),
        'oro_locale_localization_create' => 
        array (
          0 => 'manage_localizations',
        ),
        'oro_locale_localization_update' => 
        array (
          0 => 'manage_localizations',
        ),
        'oro_entityconfig_index' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_view' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_update' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_fields' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_field_update' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_field_search' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_widget_info' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_widget_unique_keys' => 
        array (
          0 => 'entity_management',
        ),
        'oro_entityconfig_widget_entity_fields' => 
        array (
          0 => 'entity_management',
        ),
        'oro_digital_asset_index' => 
        array (
          0 => 'digital_assets',
        ),
        'oro_digital_asset_create' => 
        array (
          0 => 'digital_assets',
        ),
        'oro_digital_asset_update' => 
        array (
          0 => 'digital_assets',
        ),
        'oro_digital_asset_widget_choose' => 
        array (
          0 => 'digital_assets',
        ),
        'oro_digital_asset_widget_choose_image' => 
        array (
          0 => 'digital_assets',
        ),
        'oro_digital_asset_widget_choose_file' => 
        array (
          0 => 'digital_assets',
        ),
        'oro_integration_index' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_integration_create' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_integration_update' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_integration_schedule' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_email_campaign_status' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_sync_status' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_ping' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_integration_connection' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_oauth_callback' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_oauth_disconnect' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_synchronize_adddress_book' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_synchronize_adddress_book_datafields' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_marketing_list_disconnect' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_marketing_list_connect' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_marketing_list_buttons' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_address_book_create' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_index' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_view' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_info' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_create' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_synchronize' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_mapping_index' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_mapping_update' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_dotmailer_datafield_mapping_create' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_api_delete_dotmailer_datafield' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_api_fields_dotmailer_datafield_mapping' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_api_delete_dotmailer_datafield_mapping' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_notification_emailnotification_index' => 
        array (
          0 => 'email_notification_rules',
        ),
        'oro_notification_emailnotification_update' => 
        array (
          0 => 'email_notification_rules',
        ),
        'oro_notification_emailnotification_create' => 
        array (
          0 => 'email_notification_rules',
        ),
        'oro_api_delete_emailnotication' => 
        array (
          0 => 'email_notification_rules',
        ),
        'oro_notification_massnotification_index' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'oro_notification_massnotification_view' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'oro_notification_massnotification_info' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'oro_notification_notificationalert_index' => 
        array (
          0 => 'notification_alerts',
        ),
        'oro_activity_list_widget_activities' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_activitylist_segment_activitycondition' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_activity_list_api_get_list' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_activity_list_api_get_item' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_api_get_activitylist_activity_list_item' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_api_get_activitylist_activity_list_option' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_config_configuration_system' => 
        array (
          0 => 'system_configuration',
        ),
        'oro_entityconfig_audit' => 
        array (
          0 => 'data_audit',
        ),
        'oro_entityconfig_audit_field' => 
        array (
          0 => 'data_audit',
        ),
        'oro_dataaudit_index' => 
        array (
          0 => 'data_audit',
        ),
        'oro_dataaudit_history' => 
        array (
          0 => 'data_audit',
        ),
        'oro_api_get_audits' => 
        array (
          0 => 'data_audit',
        ),
        'oro_api_get_audit' => 
        array (
          0 => 'data_audit',
        ),
        'oro_api_get_audit_fields' => 
        array (
          0 => 'data_audit',
        ),
        'oro_api_options_audits' => 
        array (
          0 => 'data_audit',
        ),
        'oro_embedded_form_list' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'oro_embedded_form_create' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'oro_embedded_form_delete' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'oro_embedded_form_default_data' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'oro_embedded_form_update' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'oro_embedded_form_view' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'oro_embedded_form_info' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'oro_report_index' => 
        array (
          0 => 'reports',
        ),
        'oro_report_view' => 
        array (
          0 => 'reports',
        ),
        'oro_report_view_grid' => 
        array (
          0 => 'reports',
        ),
        'oro_report_create' => 
        array (
          0 => 'reports',
        ),
        'oro_report_update' => 
        array (
          0 => 'reports',
        ),
        'oro_report_clone' => 
        array (
          0 => 'reports',
        ),
        'oro_api_delete_report' => 
        array (
          0 => 'reports',
        ),
        'oro_reportcrm_index' => 
        array (
          0 => 'reports',
        ),
        'oro_segment_index' => 
        array (
          0 => 'segments',
        ),
        'oro_segment_view' => 
        array (
          0 => 'segments',
        ),
        'oro_segment_create' => 
        array (
          0 => 'segments',
        ),
        'oro_segment_update' => 
        array (
          0 => 'segments',
        ),
        'oro_segment_clone' => 
        array (
          0 => 'segments',
        ),
        'oro_segment_refresh' => 
        array (
          0 => 'segments',
        ),
        'oro_api_delete_segment' => 
        array (
          0 => 'segments',
        ),
        'oro_api_post_segment_run' => 
        array (
          0 => 'segments',
        ),
        'oro_tag_index' => 
        array (
          0 => 'manage_tags',
        ),
        'oro_tag_create' => 
        array (
          0 => 'manage_tags',
        ),
        'oro_tag_update' => 
        array (
          0 => 'manage_tags',
        ),
        'oro_api_delete_tag' => 
        array (
          0 => 'manage_tags',
        ),
        'oro_api_post_taggable' => 
        array (
          0 => 'manage_tags',
        ),
        'oro_taxonomy_index' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'oro_taxonomy_view' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'oro_taxonomy_create' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'oro_taxonomy_update' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'oro_taxonomy_widget_info' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'oro_api_delete_taxonomy' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'oro_workflow_definition_index' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro_workflow_definition_create' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro_workflow_definition_update' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro_workflow_definition_configure' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro_workflow_definition_view' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro_workflow_definition_activate_from_widget' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro_process_definition_index' => 
        array (
          0 => 'manage_processes',
        ),
        'oro_process_definition_view' => 
        array (
          0 => 'manage_processes',
        ),
        'oro_dashboard_index' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_view' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_configure' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_update' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_create' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_widget' => 
        array (
          0 => 'dashboards',
        ),
        'oro_api_put_dashboard_widget' => 
        array (
          0 => 'dashboards',
        ),
        'oro_api_delete_dashboard_widget' => 
        array (
          0 => 'dashboards',
        ),
        'oro_api_put_dashboard_widget_positions' => 
        array (
          0 => 'dashboards',
        ),
        'oro_api_post_dashboard_widget_add_widget' => 
        array (
          0 => 'dashboards',
        ),
        'oro_api_delete_dashboard' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_itemized_widget' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_itemized_data_widget' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_quick_launchpad' => 
        array (
          0 => 'dashboards',
        ),
        'oro_dashboard_grid' => 
        array (
          0 => 'dashboards',
        ),
        'oro_calendar_view_default' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_view' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_event_index' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_event_view' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_event_widget_info' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_event_activity_view' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_event_create' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_event_update' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar_event_delete' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_index' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_view' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_create' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_update' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_widget_events' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_event_view' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_event_widget_info' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_event_create' => 
        array (
          0 => 'calendar',
        ),
        'oro_system_calendar_event_update' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_get_calendar_connections' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_put_calendar_connection' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_post_calendar_connection' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_delete_calendar_connection' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_options_calendar_connections' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_get_calendarevents' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_get_calendarevent' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_get_calendarevent_by_calendar' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_put_calendarevent' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_post_calendarevent' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_delete_calendarevent' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_options_calendarevents' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_get_calendar_default' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_delete_systemcalendar' => 
        array (
          0 => 'calendar',
        ),
        'oro_api_options_systemcalendars' => 
        array (
          0 => 'calendar',
        ),
        'oro_contact_group_index' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_contact_group_create' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_contact_group_update' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_api_get_contactgroups' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_api_get_contactgroup' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_api_put_contactgroup' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_api_post_contactgroup' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_api_delete_contactgroup' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_api_options_contactgroups' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'oro_contact_view' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_contact_info' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_contact_create' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_contact_update' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_contact_index' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_account_widget_contacts' => 
        array (
          0 => 'manage_contacts',
          1 => 'manage_accounts',
        ),
        'oro_account_widget_contacts_info' => 
        array (
          0 => 'manage_contacts',
          1 => 'manage_accounts',
        ),
        'oro_api_get_contacts' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_get_contact' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_put_contact' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_post_contact' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_delete_contact' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_options_contacts' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_contact_address_book' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_contact_address_create' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_contact_address_update' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_get_contact_address' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_get_contact_addresses' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_delete_contact_address' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_get_contact_address_by_type' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_get_contact_address_primary' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_options_contact_addresses' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_get_contact_phones' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_get_contact_phone_primary' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_post_contact_phone' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_delete_contact_phone' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_options_contact_phones' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_post_contact_email' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_delete_contact_email' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_api_options_contact_emails' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_navigation_global_menu_index' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_view' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_create' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_update' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_move' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_ajax_reset' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_ajax_create' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_ajax_delete' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_ajax_show' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_ajax_hide' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_global_menu_ajax_move' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_index' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_view' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_create' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_update' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_move' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_ajax_reset' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_ajax_create' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_ajax_delete' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_ajax_show' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_ajax_hide' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_navigation_user_menu_ajax_move' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_marketing_list_index' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_marketing_list_view' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_marketing_list_create' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_marketing_list_update' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_contact_marketinglist_information_field_type' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_entity_marketinglist_contact_information_fields' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_delete_marketinglist' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_remove_marketinglist_removeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_unremove_marketinglist_removeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_post_marketinglist_removeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_delete_marketinglist_removeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_unsubscribe_marketinglist_unsubscribeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_subscribe_marketinglist_unsubscribeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_post_marketinglist_unsubscribeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_delete_marketinglist_unsubscribeditem' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_api_post_marketinglist_segment_run' => 
        array (
          0 => 'marketing_list',
        ),
        'oro_tracking_data_create' => 
        array (
          0 => 'tracking',
        ),
        'oro_tracking_website_index' => 
        array (
          0 => 'tracking',
        ),
        'oro_tracking_website_create' => 
        array (
          0 => 'tracking',
        ),
        'oro_tracking_website_update' => 
        array (
          0 => 'tracking',
        ),
        'oro_tracking_website_view' => 
        array (
          0 => 'tracking',
        ),
        'oro_api_delete_tracking_website' => 
        array (
          0 => 'tracking',
        ),
        'oro_account_view' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_account_create' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_account_update' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_account_index' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_account_widget_info' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_api_get_accounts' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_api_get_account' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_api_put_account' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_api_post_account' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_api_delete_account' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_api_options_accounts' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_call_index' => 
        array (
          0 => 'call',
        ),
        'oro_call_view' => 
        array (
          0 => 'call',
        ),
        'oro_call_create' => 
        array (
          0 => 'call',
        ),
        'oro_call_update' => 
        array (
          0 => 'call',
        ),
        'oro_call_activity_view' => 
        array (
          0 => 'call',
        ),
        'oro_call_widget_calls' => 
        array (
          0 => 'call',
        ),
        'oro_call_base_widget_calls' => 
        array (
          0 => 'call',
        ),
        'oro_call_widget_info' => 
        array (
          0 => 'call',
        ),
        'oro_api_get_calls' => 
        array (
          0 => 'call',
        ),
        'oro_api_get_call' => 
        array (
          0 => 'call',
        ),
        'oro_api_put_call' => 
        array (
          0 => 'call',
        ),
        'oro_api_post_call' => 
        array (
          0 => 'call',
        ),
        'oro_api_delete_call' => 
        array (
          0 => 'call',
        ),
        'oro_api_options_calls' => 
        array (
          0 => 'call',
        ),
        'oro_campaign_index' => 
        array (
          0 => 'campaign',
        ),
        'oro_campaign_create' => 
        array (
          0 => 'campaign',
        ),
        'oro_campaign_update' => 
        array (
          0 => 'campaign',
        ),
        'oro_campaign_view' => 
        array (
          0 => 'campaign',
        ),
        'oro_campaign_event_plot' => 
        array (
          0 => 'campaign',
        ),
        'oro_email_campaign_index' => 
        array (
          0 => 'campaign',
        ),
        'oro_email_campaign_create' => 
        array (
          0 => 'campaign',
        ),
        'oro_email_campaign_update' => 
        array (
          0 => 'campaign',
        ),
        'oro_email_campaign_view' => 
        array (
          0 => 'campaign',
        ),
        'oro_email_campaign_send' => 
        array (
          0 => 'campaign',
        ),
        'oro_campaign_dashboard_campaigns_leads_chart' => 
        array (
          0 => 'campaign',
        ),
        'oro_campaign_dashboard_campaigns_opportunity_chart' => 
        array (
          0 => 'campaign',
        ),
        'oro_campaign_dashboard_campaigns_by_close_revenue_chart' => 
        array (
          0 => 'campaign',
        ),
        'oro_api_get_emailcampaign_email_templates' => 
        array (
          0 => 'campaign',
        ),
        'oro_channel_index' => 
        array (
          0 => 'channels',
        ),
        'oro_channel_create' => 
        array (
          0 => 'channels',
        ),
        'oro_channel_update' => 
        array (
          0 => 'channels',
        ),
        'oro_channel_view' => 
        array (
          0 => 'channels',
        ),
        'oro_channel_widget_info' => 
        array (
          0 => 'channels',
        ),
        'oro_channel_integration_create' => 
        array (
          0 => 'channels',
        ),
        'oro_channel_integration_update' => 
        array (
          0 => 'channels',
        ),
        'oro_api_get_channels' => 
        array (
          0 => 'channels',
        ),
        'oro_api_options_channels' => 
        array (
          0 => 'channels',
        ),
        'oro_api_delete_channel' => 
        array (
          0 => 'channels',
        ),
        'oro_task_index' => 
        array (
          0 => 'task',
        ),
        'oro_task_view' => 
        array (
          0 => 'task',
        ),
        'oro_task_create' => 
        array (
          0 => 'task',
        ),
        'oro_task_update' => 
        array (
          0 => 'task',
        ),
        'oro_task_widget_sidebar_tasks' => 
        array (
          0 => 'task',
        ),
        'oro_task_widget_info' => 
        array (
          0 => 'task',
        ),
        'oro_task_activity_view' => 
        array (
          0 => 'task',
        ),
        'oro_task_user_tasks' => 
        array (
          0 => 'task',
        ),
        'oro_task_my_tasks' => 
        array (
          0 => 'task',
        ),
        'oro_api_get_tasks' => 
        array (
          0 => 'task',
        ),
        'oro_api_get_task' => 
        array (
          0 => 'task',
        ),
        'oro_api_put_task' => 
        array (
          0 => 'task',
        ),
        'oro_api_post_task' => 
        array (
          0 => 'task',
        ),
        'oro_api_delete_task' => 
        array (
          0 => 'task',
        ),
        'oro_api_options_tasks' => 
        array (
          0 => 'task',
        ),
        'oro_marketing_activity_widget_summary' => 
        array (
          0 => 'marketingactivity',
        ),
        'oro_marketing_activity_widget_marketing_activities' => 
        array (
          0 => 'marketingactivity',
        ),
        'oro_marketing_activity_widget_marketing_activities_info' => 
        array (
          0 => 'marketingactivity',
        ),
        'oro_marketing_activity_widget_marketing_activities_list' => 
        array (
          0 => 'marketingactivity',
        ),
        'oro_sales_lead_view' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_lead_info' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_lead_create' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_lead_update' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_lead_index' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_widget_account_leads' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_lead_data_channel_aware_create' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_lead_disqualify' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_lead_convert_to_opportunity' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_b2bcustomer_widget_leads' => 
        array (
          0 => 'sales_lead',
        ),
        'oro_sales_opportunity_view' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_opportunity_info' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_opportunity_create' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_opportunity_update' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_opportunity_index' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_opportunity_data_channel_aware_create' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_opportunity_customer_aware_create' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_b2bcustomer_widget_opportunities' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_b2bcustomer_index' => 
        array (
          0 => 'business_customers',
        ),
        'oro_sales_b2bcustomer_view' => 
        array (
          0 => 'business_customers',
        ),
        'oro_sales_b2bcustomer_widget_info' => 
        array (
          0 => 'business_customers',
        ),
        'oro_sales_b2bcustomer_create' => 
        array (
          0 => 'business_customers',
        ),
        'oro_sales_b2bcustomer_update' => 
        array (
          0 => 'business_customers',
        ),
        'oro_sales_widget_b2bcustomer_info' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_get_b2bcustomers' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_get_b2bcustomer' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_put_b2bcustomer' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_post_b2bcustomer' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_delete_b2bcustomer' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_options_b2bcustomers' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_post_b2bcustomer_email' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_delete_b2bcustomer_email' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_options_b2bcustomer_emails' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_get_b2bcustomer_phones' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_get_b2bcustomer_phone_primary' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_post_b2bcustomer_phone' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_delete_b2bcustomer_phone' => 
        array (
          0 => 'business_customers',
        ),
        'oro_api_options_b2bcustomer_phones' => 
        array (
          0 => 'business_customers',
        ),
        'oro_contactus_request_index' => 
        array (
          0 => 'contact_requests',
        ),
        'oro_contactus_request_view' => 
        array (
          0 => 'contact_requests',
        ),
        'oro_contactus_request_info' => 
        array (
          0 => 'contact_requests',
        ),
        'oro_contactus_request_create' => 
        array (
          0 => 'contact_requests',
        ),
        'oro_contactus_request_update' => 
        array (
          0 => 'contact_requests',
        ),
        'oro_contactus_request_delete' => 
        array (
          0 => 'contact_requests',
        ),
        'oro_api_get_contactrequest' => 
        array (
          0 => 'contact_requests',
        ),
        'oro_contactus_reason_index' => 
        array (
          0 => 'manage_contact_reasons',
        ),
        'oro_contactus_reason_create' => 
        array (
          0 => 'manage_contact_reasons',
        ),
        'oro_contactus_reason_update' => 
        array (
          0 => 'manage_contact_reasons',
        ),
        'oro_contactus_reason_delete' => 
        array (
          0 => 'manage_contact_reasons',
        ),
        'oro_case_index' => 
        array (
          0 => 'case',
        ),
        'oro_case_view' => 
        array (
          0 => 'case',
        ),
        'oro_case_create' => 
        array (
          0 => 'case',
        ),
        'oro_case_update' => 
        array (
          0 => 'case',
        ),
        'oro_case_account_widget_cases' => 
        array (
          0 => 'case',
        ),
        'oro_case_contact_widget_cases' => 
        array (
          0 => 'case',
        ),
        'oro_case_comment_list' => 
        array (
          0 => 'case',
        ),
        'oro_case_comment_create' => 
        array (
          0 => 'case',
        ),
        'oro_case_comment_update' => 
        array (
          0 => 'case',
        ),
        'oro_case_widget_comments' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_get_cases' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_get_case' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_put_case' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_post_case' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_delete_case' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_options_cases' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_get_comments' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_get_comment' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_put_comment' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_post_comment' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_delete_comment' => 
        array (
          0 => 'case',
        ),
        'oro_case_api_options_comments' => 
        array (
          0 => 'case',
        ),
      ),
      'navigation_items' => 
      array (
        'application_menu.system_tab.oro_platform_system_info' => 
        array (
          0 => 'system_information',
        ),
        'application_menu.system_tab.users_management.oro_business_unit_list' => 
        array (
          0 => 'manage_business_units',
        ),
        'application_menu.system_tab.oro_message_queue_job' => 
        array (
          0 => 'manage_jobs',
        ),
        'application_menu.system_tab.emails.oro_email_emailtemplate_list' => 
        array (
          0 => 'manage_email_templates',
        ),
        'application_menu.system_tab.oro_cron_schedule' => 
        array (
          0 => 'manage_scheduled_tasks',
        ),
        'application_menu.system_tab.users_management.backoffice_oauth_applications' => 
        array (
          0 => 'web_api',
        ),
        'application_menu.system_tab.users_management.user_list' => 
        array (
          0 => 'manage_users',
        ),
        'application_menu.system_tab.users_management.user_roles' => 
        array (
          0 => 'manage_user_roles',
        ),
        'application_menu.system_tab.users_management.user_groups' => 
        array (
          0 => 'manage_user_groups',
        ),
        'application_menu.system_tab.users_management.login_attempts' => 
        array (
          0 => 'user_login_attempts',
        ),
        'application_menu.system_tab.localization.languages' => 
        array (
          0 => 'manage_languages',
        ),
        'application_menu.system_tab.localization.translations' => 
        array (
          0 => 'manage_translations',
        ),
        'application_menu.system_tab.localization.localizations' => 
        array (
          0 => 'manage_localizations',
        ),
        'application_menu.system_tab.entities_list.entity_management' => 
        array (
          0 => 'entity_management',
        ),
        'application_menu.marketing_tab.digital_asset_list' => 
        array (
          0 => 'digital_assets',
        ),
        'application_menu.system_tab.digital_asset_list' => 
        array (
          0 => 'digital_assets',
        ),
        'application_menu.system_tab.integrations_submenu.integrations_list' => 
        array (
          0 => 'manage_integrations',
        ),
        'application_menu.system_tab.emails.oro_notification_emailnotification_list' => 
        array (
          0 => 'email_notification_rules',
        ),
        'application_menu.system_tab.emails.oro_notification_massnotification_list' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'application_menu.system_tab.oro_notification_notificationalerts_list' => 
        array (
          0 => 'notification_alerts',
        ),
        'application_menu.system_tab.system_configuration' => 
        array (
          0 => 'system_configuration',
        ),
        'application_menu.system_tab.audit_list' => 
        array (
          0 => 'data_audit',
        ),
        'application_menu.system_tab.integrations_submenu.embedded_forms' => 
        array (
          0 => 'manage_embedded_forms',
        ),
        'application_menu.reports_tab.manage_reports' => 
        array (
          0 => 'reports',
        ),
        'application_menu.reports_tab.segments_divider' => 
        array (
          0 => 'segments',
        ),
        'application_menu.reports_tab.manage_segments' => 
        array (
          0 => 'segments',
        ),
        'application_menu.system_tab.tags_management.oro_tag_list' => 
        array (
          0 => 'manage_tags',
        ),
        'application_menu.system_tab.tags_management.oro_taxonomy_list' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'application_menu.system_tab.workflow_definition_list' => 
        array (
          0 => 'manage_workflows',
        ),
        'application_menu.system_tab.process_definition_list' => 
        array (
          0 => 'manage_processes',
        ),
        'application_menu.dashboard_tab.oro_dashboard_index' => 
        array (
          0 => 'dashboards',
        ),
        'application_menu.activities_tab.calendar_event_list' => 
        array (
          0 => 'calendar',
        ),
        'application_menu.usermenu.oro_calendar_view_default' => 
        array (
          0 => 'calendar',
        ),
        'application_menu.calendar_menu' => 
        array (
          0 => 'calendar',
        ),
        'application_menu.calendar_menu_mobile' => 
        array (
          0 => 'calendar',
        ),
        'application_menu.system_tab.contact_group_list' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'shortcuts.shortcut_new_contact_group' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'application_menu.customers_tab.contact_list' => 
        array (
          0 => 'manage_contacts',
        ),
        'shortcuts.shortcut_new_contact' => 
        array (
          0 => 'manage_contacts',
        ),
        'shortcuts.shortcut_list_contacts' => 
        array (
          0 => 'manage_contacts',
        ),
        'application_menu.system_tab.menu_list_default' => 
        array (
          0 => 'navigation_menu',
        ),
        'application_menu.customers_tab.account_list' => 
        array (
          0 => 'manage_accounts',
        ),
        'shortcuts.shortcut_new_account' => 
        array (
          0 => 'manage_accounts',
        ),
        'shortcuts.shortcut_list_accounts' => 
        array (
          0 => 'manage_accounts',
        ),
        'application_menu.activities_tab.call_list' => 
        array (
          0 => 'call',
        ),
        'application_menu > reports_tab > oro.campaign.entity_plural_label_report_tab' => 
        array (
          0 => 'campaign',
        ),
        'application_menu.system_tab.channels_list' => 
        array (
          0 => 'channels',
        ),
        'application_menu.activities_tab.task_list' => 
        array (
          0 => 'task',
        ),
        'usermenu.oro_task_my_tasks' => 
        array (
          0 => 'task',
        ),
        'application_menu.reports_tab.static_reports_tab.leads_report_tab.leads_by_date' => 
        array (
          0 => 'sales_lead',
        ),
        'application_menu.reports_tab.static_reports_tab.opportunities_report_tab.opportunities_by_status' => 
        array (
          0 => 'sales_opportunity',
        ),
        'application_menu.reports_tab.static_reports_tab.accounts_report_tab.accounts_by_opportunities' => 
        array (
          0 => 'sales_opportunity',
        ),
        'application_menu.reports_tab.static_reports_tab.opportunities_report_tab.won_by_period' => 
        array (
          0 => 'sales_opportunity',
        ),
        'application_menu.reports_tab.static_reports_tab.opportunities_report_tab.total_forecast' => 
        array (
          0 => 'sales_opportunity',
        ),
        'application_menu.reports_tab.static_reports_tab.accounts_report_tab.accounts_life_time_value' => 
        array (
          0 => 'sales_opportunity',
        ),
        'application_menu.customers_tab.b2bcustomer_list' => 
        array (
          0 => 'business_customers',
        ),
        'application_menu.activities_tab.oro_contactus_requests' => 
        array (
          0 => 'contact_requests',
        ),
        'application_menu.system_tab.oro_contactus_reasons' => 
        array (
          0 => 'manage_contact_reasons',
        ),
        'application_menu.activities_tab.oro_case_index' => 
        array (
          0 => 'case',
        ),
      ),
      'configuration' => 
      array (
        'development_settings' => 
        array (
          0 => 'development_settings',
        ),
        'development_layout_settings' => 
        array (
          0 => 'development_settings',
        ),
        'attachment_processors_settings' => 
        array (
          0 => 'attachment_post_processors',
        ),
        'oro_attachment.jpeg_quality' => 
        array (
          0 => 'attachment_post_processors_allowed',
        ),
        'oro_attachment.png_quality' => 
        array (
          0 => 'attachment_post_processors_allowed',
        ),
        'oro_attachment.webp_quality' => 
        array (
          0 => 'attachment_webp',
        ),
        'user_email_configuration' => 
        array (
          0 => 'email',
        ),
        'email_autocomplete_configuration' => 
        array (
          0 => 'email',
        ),
        'signature_configuration' => 
        array (
          0 => 'email',
        ),
        'email_threads' => 
        array (
          0 => 'email',
        ),
        'reply_configuration' => 
        array (
          0 => 'email',
        ),
        'attachment_configuration' => 
        array (
          0 => 'email',
        ),
        'mailboxes' => 
        array (
          0 => 'email',
        ),
        'user_mailbox' => 
        array (
          0 => 'email',
        ),
        'user_bar_settings' => 
        array (
          0 => 'email',
        ),
        'integration_email_settings' => 
        array (
          0 => 'email',
        ),
        'oro_hangouts_call.enable_google_hangouts_for_email' => 
        array (
          0 => 'email',
        ),
        'google_imap_settings' => 
        array (
          0 => 'email',
        ),
        'dotmailer_settings' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_notification.email_notification_sender_email' => 
        array (
          0 => 'email_notification_rules',
        ),
        'oro_notification.email_notification_sender_name' => 
        array (
          0 => 'email_notification_rules',
        ),
        'oro_notification.mass_notification_recipients' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'oro_notification.mass_notification_template' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'oro_activity_list.sorting_field' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_activity_list.sorting_direction' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_activity_list.per_page' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_activity_list.grouping' => 
        array (
          0 => 'activity_lists',
        ),
        'oro_report.display_sql_query' => 
        array (
          0 => 'reports',
        ),
        'oro_tag.taxonomy_colors' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'oro_calendar.calendar_colors' => 
        array (
          0 => 'calendar',
        ),
        'oro_calendar.event_colors' => 
        array (
          0 => 'calendar',
        ),
        'email_campaign_settings' => 
        array (
          0 => 'campaign',
        ),
        'opportunity_status_probabilities' => 
        array (
          0 => 'sales_opportunity',
        ),
        'display_settings' => 
        array (
          0 => 'sales_opportunity',
        ),
      ),
      'entities' => 
      array (
        'Oro\\Bundle\\OrganizationBundle\\Entity\\BusinessUnit' => 
        array (
          0 => 'manage_business_units',
        ),
        'Oro\\Bundle\\MessageQueueBundle\\Entity\\Job' => 
        array (
          0 => 'manage_jobs',
        ),
        'Oro\\Bundle\\EmailBundle\\Entity\\Email' => 
        array (
          0 => 'email',
        ),
        'Oro\\Bundle\\EmailBundle\\Entity\\EmailUser' => 
        array (
          0 => 'email',
        ),
        'Oro\\Bundle\\EmailBundle\\Entity\\EmailTemplate' => 
        array (
          0 => 'manage_email_templates',
        ),
        'Oro\\Bundle\\EmailBundle\\Entity\\EmailTemplateTranslation' => 
        array (
          0 => 'manage_email_templates',
        ),
        'Oro\\Bundle\\CronBundle\\Entity\\Schedule' => 
        array (
          0 => 'manage_scheduled_tasks',
        ),
        'Oro\\Bundle\\UserBundle\\Entity\\User' => 
        array (
          0 => 'manage_users',
        ),
        'Oro\\Bundle\\UserBundle\\Entity\\Role' => 
        array (
          0 => 'manage_user_roles',
        ),
        'Oro\\Bundle\\UserBundle\\Entity\\Group' => 
        array (
          0 => 'manage_user_groups',
        ),
        'Oro\\Bundle\\TranslationBundle\\Entity\\Language' => 
        array (
          0 => 'manage_languages',
        ),
        'Oro\\Bundle\\TranslationBundle\\Entity\\Translation' => 
        array (
          0 => 'manage_translations',
        ),
        'Oro\\Bundle\\TranslationBundle\\Entity\\TranslationKey' => 
        array (
          0 => 'manage_translations',
        ),
        'Oro\\Bundle\\LocaleBundle\\Entity\\Localization' => 
        array (
          0 => 'manage_localizations',
        ),
        'Oro\\Bundle\\LocaleBundle\\Entity\\LocalizedFallbackValue' => 
        array (
          0 => 'manage_localizations',
        ),
        'Oro\\Bundle\\IntegrationBundle\\Entity\\Channel' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\Activity' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\AddressBook' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\AddressBookContact' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\AddressBookContactsExport' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\Campaign' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\CampaignSummary' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\Contact' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\DataField' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\DataFieldMapping' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\DataFieldMappingConfig' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\ChangedFieldLog' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\DotmailerBundle\\Entity\\OAuth' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cnt_Status' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cnt_Email_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cnt_Opt_In_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cmp_Status' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cmp_Reply_Action' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Ab_Cnt_Exp_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Ab_Visibility' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Import_Status' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Df_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Df_Visibility' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\NotificationBundle\\Entity\\EmailNotification' => 
        array (
          0 => 'email_notification_rules',
        ),
        'Oro\\Bundle\\NotificationBundle\\Entity\\MassNotification' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'Oro\\Bundle\\NotificationBundle\\Entity\\NotificationAlert' => 
        array (
          0 => 'notification_alerts',
        ),
        'Oro\\Bundle\\ActivityListBundle\\Entity\\ActivityList' => 
        array (
          0 => 'activity_lists',
        ),
        'Oro\\Bundle\\ActivityListBundle\\Entity\\ActivityOwner' => 
        array (
          0 => 'activity_lists',
        ),
        'Oro\\Bundle\\DataAuditBundle\\Entity\\Audit' => 
        array (
          0 => 'data_audit',
        ),
        'Oro\\Bundle\\DataAuditBundle\\Entity\\AuditField' => 
        array (
          0 => 'data_audit',
        ),
        'Oro\\Bundle\\ReportBundle\\Entity\\Report' => 
        array (
          0 => 'reports',
        ),
        'Oro\\Bundle\\ReportBundle\\Entity\\ReportType' => 
        array (
          0 => 'reports',
        ),
        'Oro\\Bundle\\SegmentBundle\\Entity\\Segment' => 
        array (
          0 => 'segments',
        ),
        'Oro\\Bundle\\SegmentBundle\\Entity\\SegmentSnapshot' => 
        array (
          0 => 'segments',
        ),
        'Oro\\Bundle\\SegmentBundle\\Entity\\SegmentType' => 
        array (
          0 => 'segments',
        ),
        'Oro\\Bundle\\TagBundle\\Entity\\Tag' => 
        array (
          0 => 'manage_tags',
        ),
        'Oro\\Bundle\\TagBundle\\Entity\\Tagging' => 
        array (
          0 => 'manage_tags',
        ),
        'Oro\\Bundle\\TagBundle\\Entity\\Taxonomy' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowDefinition' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowEntityAcl' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowEntityAclIdentity' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowItem' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowRestriction' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowRestrictionIdentity' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowStep' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\WorkflowTransitionRecord' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\TransitionCronTrigger' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\TransitionEventTrigger' => 
        array (
          0 => 'manage_workflows',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\ProcessDefinition' => 
        array (
          0 => 'manage_processes',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\ProcessJob' => 
        array (
          0 => 'manage_processes',
        ),
        'Oro\\Bundle\\WorkflowBundle\\Entity\\ProcessTrigger' => 
        array (
          0 => 'manage_processes',
        ),
        'Oro\\Bundle\\DashboardBundle\\Entity\\Dashboard' => 
        array (
          0 => 'dashboards',
        ),
        'Oro\\Bundle\\DashboardBundle\\Entity\\ActiveDashboard' => 
        array (
          0 => 'dashboards',
        ),
        'Oro\\Bundle\\DashboardBundle\\Entity\\Widget' => 
        array (
          0 => 'dashboards',
        ),
        'Oro\\Bundle\\DashboardBundle\\Entity\\WidgetState' => 
        array (
          0 => 'dashboards',
        ),
        'Oro\\Bundle\\CalendarBundle\\Entity\\Calendar' => 
        array (
          0 => 'calendar',
        ),
        'Oro\\Bundle\\CalendarBundle\\Entity\\CalendarEvent' => 
        array (
          0 => 'calendar',
        ),
        'Oro\\Bundle\\CalendarBundle\\Entity\\CalendarProperty' => 
        array (
          0 => 'calendar',
        ),
        'Oro\\Bundle\\CalendarBundle\\Entity\\Recurrence' => 
        array (
          0 => 'calendar',
        ),
        'Oro\\Bundle\\CalendarBundle\\Entity\\Attendee' => 
        array (
          0 => 'calendar',
        ),
        'Oro\\Bundle\\CalendarBundle\\Entity\\SystemCalendar' => 
        array (
          0 => 'calendar',
        ),
        'Oro\\Bundle\\ContactBundle\\Entity\\Group' => 
        array (
          0 => 'manage_contact_groups',
        ),
        'Oro\\Bundle\\ContactBundle\\Entity\\Contact' => 
        array (
          0 => 'manage_contacts',
        ),
        'Oro\\Bundle\\ContactBundle\\Entity\\ContactAddress' => 
        array (
          0 => 'manage_contacts',
        ),
        'Oro\\Bundle\\ContactBundle\\Entity\\ContactEmail' => 
        array (
          0 => 'manage_contacts',
        ),
        'Oro\\Bundle\\ContactBundle\\Entity\\ContactPhone' => 
        array (
          0 => 'manage_contacts',
        ),
        'Oro\\Bundle\\ContactBundle\\Entity\\Method' => 
        array (
          0 => 'manage_contacts',
        ),
        'Oro\\Bundle\\ContactBundle\\Entity\\Source' => 
        array (
          0 => 'manage_contacts',
        ),
        'Oro\\Bundle\\NavigationBundle\\Entity\\MenuUpdate' => 
        array (
          0 => 'navigation_menu',
        ),
        'Oro\\Bundle\\MarketingListBundle\\Entity\\MarketingList' => 
        array (
          0 => 'marketing_list',
        ),
        'Oro\\Bundle\\MarketingListBundle\\Entity\\MarketingListItem' => 
        array (
          0 => 'marketing_list',
        ),
        'Oro\\Bundle\\MarketingListBundle\\Entity\\MarketingListType' => 
        array (
          0 => 'marketing_list',
        ),
        'Oro\\Bundle\\AccountBundle\\Entity\\Account' => 
        array (
          0 => 'manage_accounts',
        ),
        'Oro\\Bundle\\CallBundle\\Entity\\Call' => 
        array (
          0 => 'call',
        ),
        'Oro\\Bundle\\CallBundle\\Entity\\CallDirection' => 
        array (
          0 => 'call',
        ),
        'Oro\\Bundle\\CallBundle\\Entity\\CallStatus' => 
        array (
          0 => 'call',
        ),
        'Oro\\Bundle\\CampaignBundle\\Entity\\Campaign' => 
        array (
          0 => 'campaign',
        ),
        'Oro\\Bundle\\CampaignBundle\\Entity\\EmailCampaign' => 
        array (
          0 => 'campaign',
        ),
        'Oro\\Bundle\\ChannelBundle\\Entity\\Channel' => 
        array (
          0 => 'channels',
        ),
        'Oro\\Bundle\\TaskBundle\\Entity\\Task' => 
        array (
          0 => 'task',
        ),
        'Oro\\Bundle\\TaskBundle\\Entity\\TaskPriority' => 
        array (
          0 => 'task',
        ),
        'Extend\\Entity\\EV_Task_Status' => 
        array (
          0 => 'task',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\Lead' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\LeadAddress' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\LeadEmail' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\LeadPhone' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\Opportunity' => 
        array (
          0 => 'sales_opportunity',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\OpportunityCloseReason' => 
        array (
          0 => 'sales_opportunity',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomer' => 
        array (
          0 => 'business_customers',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerEmail' => 
        array (
          0 => 'business_customers',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerPhone' => 
        array (
          0 => 'business_customers',
        ),
        'Oro\\Bundle\\ContactUsBundle\\Entity\\ContactRequest' => 
        array (
          0 => 'contact_requests',
        ),
        'Oro\\Bundle\\ContactUsBundle\\Entity\\ContactReason' => 
        array (
          0 => 'manage_contact_reasons',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseEntity' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CasePriority' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseSource' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseStatus' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseComment' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseMailboxProcessSettings' => 
        array (
          0 => 'case',
        ),
      ),
      'sidebar_widgets' => 
      array (
        'recent_emails' => 
        array (
          0 => 'email',
        ),
        'assigned_tasks' => 
        array (
          0 => 'task',
        ),
      ),
      'dashboard_widgets' => 
      array (
        'recent_emails' => 
        array (
          0 => 'email',
        ),
        'quick_launchpad' => 
        array (
          0 => 'dashboards',
        ),
        'my_calendar' => 
        array (
          0 => 'calendar',
        ),
        'quick_launchpad.contacts' => 
        array (
          0 => 'manage_contacts',
        ),
        'my_contacts_activity' => 
        array (
          0 => 'manage_contacts',
        ),
        'my_accounts_activity' => 
        array (
          0 => 'manage_accounts',
        ),
        'quick_launchpad.accounts' => 
        array (
          0 => 'manage_accounts',
        ),
        'recent_calls' => 
        array (
          0 => 'call',
        ),
        'campaigns_leads' => 
        array (
          0 => 'campaign',
          1 => 'sales_lead',
        ),
        'campaigns_opportunity' => 
        array (
          0 => 'campaign',
          1 => 'sales_opportunity',
        ),
        'campaigns_by_close_revenue' => 
        array (
          0 => 'campaign',
          1 => 'sales_lead',
          2 => 'sales_opportunity',
        ),
        'e_commerce' => 
        array (
          0 => 'channels',
        ),
        'leads_list' => 
        array (
          0 => 'sales_lead',
        ),
        'quick_launchpad.leads' => 
        array (
          0 => 'sales_lead',
        ),
        'lead_statistics' => 
        array (
          0 => 'sales_lead',
        ),
        'quick_launchpad.opportunities' => 
        array (
          0 => 'sales_opportunity',
        ),
        'opportunities_by_lead_source_chart' => 
        array (
          0 => 'sales_opportunity',
        ),
        'opportunities_by_state' => 
        array (
          0 => 'sales_opportunity',
        ),
        'forecast_of_opportunities' => 
        array (
          0 => 'sales_opportunity',
        ),
        'average_lifetime_sales_chart' => 
        array (
          0 => 'sales_opportunity',
        ),
        'opportunities_list' => 
        array (
          0 => 'sales_opportunity',
        ),
        'opportunity_statistics' => 
        array (
          0 => 'sales_opportunity',
        ),
      ),
      'commands' => 
      array (
        'oro:cron:email-body-sync' => 
        array (
          0 => 'email',
        ),
        'oro:cron:imap-sync' => 
        array (
          0 => 'email',
        ),
        'oro:email:template:import' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro:email:template:export' => 
        array (
          0 => 'manage_email_templates',
        ),
        'oro:user:create' => 
        array (
          0 => 'manage_users',
        ),
        'oro:user:list' => 
        array (
          0 => 'manage_users',
        ),
        'oro:user:update' => 
        array (
          0 => 'manage_users',
        ),
        'oro:translation:update' => 
        array (
          0 => 'manage_translations',
        ),
        'oro:translation:rebuild-cache' => 
        array (
          0 => 'manage_translations',
        ),
        'oro:translation:load' => 
        array (
          0 => 'manage_translations',
        ),
        'oro:translation:dump' => 
        array (
          0 => 'manage_translations',
        ),
        'oro:translation:dump-files' => 
        array (
          0 => 'manage_translations',
        ),
        'oro:localization:localized-fallback-values:cleanup-unused' => 
        array (
          0 => 'manage_localizations',
        ),
        'oro:localization:dump' => 
        array (
          0 => 'manage_localizations',
        ),
        'oro:localization:update' => 
        array (
          0 => 'manage_localizations',
        ),
        'oro:cron:integration:cleanup' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro:cron:integration:sync' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro:notification:alerts:list' => 
        array (
          0 => 'notification_alerts',
        ),
        'oro:notification:alerts:cleanup' => 
        array (
          0 => 'notification_alerts',
        ),
        'oro:workflow:definitions:load' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro:debug:workflow:definitions' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro:workflow:translations:dump' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro:process:configuration:load' => 
        array (
          0 => 'manage_processes',
        ),
        'oro:navigation:menu:reset' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro:cron:import-tracking' => 
        array (
          0 => 'tracking',
        ),
        'oro:cron:tracking:parse' => 
        array (
          0 => 'tracking',
        ),
        'oro:cron:send-email-campaigns' => 
        array (
          0 => 'campaign',
        ),
        'oro:b2b:lifetime:recalculate' => 
        array (
          0 => 'sales_opportunity',
        ),
      ),
      'cron_jobs' => 
      array (
        'oro:cron:email-body-sync' => 
        array (
          0 => 'email',
        ),
        'oro:cron:imap-sync' => 
        array (
          0 => 'email',
        ),
        'oro:cron:import-tracking' => 
        array (
          0 => 'tracking',
        ),
        'oro:cron:tracking:parse' => 
        array (
          0 => 'tracking',
        ),
        'oro:cron:send-email-campaigns' => 
        array (
          0 => 'campaign',
        ),
      ),
      'operations' => 
      array (
        'user_enable' => 
        array (
          0 => 'manage_users',
        ),
        'user_disable' => 
        array (
          0 => 'manage_users',
        ),
        'clone_role' => 
        array (
          0 => 'manage_user_roles',
        ),
        'oro_user_edit_role' => 
        array (
          0 => 'manage_user_roles',
        ),
        'oro_translation_language_add' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_language_disable' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_language_enable' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_language_import' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_language_export' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_language_load_base' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_language_install' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_language_update' => 
        array (
          0 => 'manage_languages',
        ),
        'oro_translation_translation_reset' => 
        array (
          0 => 'manage_translations',
        ),
        'digital_asset_delete' => 
        array (
          0 => 'digital_assets',
        ),
        'oro_integration_activate' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_integration_deactivate' => 
        array (
          0 => 'manage_integrations',
        ),
        'oro_integration_delete' => 
        array (
          0 => 'manage_integrations',
        ),
        'clone_workflow' => 
        array (
          0 => 'manage_workflows',
        ),
        'oro_workflow_definition_configure' => 
        array (
          0 => 'manage_workflows',
        ),
      ),
      'api_resources' => 
      array (
        'Oro\\Bundle\\TranslationBundle\\Entity\\Language' => 
        array (
          0 => 'manage_languages',
        ),
        'Oro\\Bundle\\TranslationBundle\\Entity\\Translation' => 
        array (
          0 => 'manage_translations',
        ),
        'Oro\\Bundle\\TranslationBundle\\Entity\\TranslationKey' => 
        array (
          0 => 'manage_translations',
        ),
        'Oro\\Bundle\\LocaleBundle\\Entity\\Localization' => 
        array (
          0 => 'manage_localizations',
        ),
        'Oro\\Bundle\\LocaleBundle\\Entity\\LocalizedFallbackValue' => 
        array (
          0 => 'manage_localizations',
        ),
        'Oro\\Bundle\\IntegrationBundle\\Entity\\Channel' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cnt_Status' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cnt_Email_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cnt_Opt_In_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cmp_Status' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Cmp_Reply_Action' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Ab_Cnt_Exp_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Ab_Visibility' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Import_Status' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Df_Type' => 
        array (
          0 => 'manage_integrations',
        ),
        'Extend\\Entity\\EV_Dm_Df_Visibility' => 
        array (
          0 => 'manage_integrations',
        ),
        'Oro\\Bundle\\NotificationBundle\\Entity\\EmailNotification' => 
        array (
          0 => 'email_notification_rules',
        ),
        'Oro\\Bundle\\NotificationBundle\\Entity\\MassNotification' => 
        array (
          0 => 'email_mass_notifications',
        ),
        'Oro\\Bundle\\NotificationBundle\\Entity\\NotificationAlert' => 
        array (
          0 => 'notification_alerts',
        ),
        'Oro\\Bundle\\TagBundle\\Entity\\Tag' => 
        array (
          0 => 'manage_tags',
        ),
        'Oro\\Bundle\\TagBundle\\Entity\\Taxonomy' => 
        array (
          0 => 'manage_taxonomies',
        ),
        'Oro\\Bundle\\TrackingBundle\\Entity\\TrackingWebsite' => 
        array (
          0 => 'tracking',
        ),
        'Oro\\Bundle\\CallBundle\\Entity\\Call' => 
        array (
          0 => 'call',
        ),
        'Oro\\Bundle\\CallBundle\\Entity\\CallDirection' => 
        array (
          0 => 'call',
        ),
        'Oro\\Bundle\\CallBundle\\Entity\\CallStatus' => 
        array (
          0 => 'call',
        ),
        'Oro\\Bundle\\CampaignBundle\\Entity\\Campaign' => 
        array (
          0 => 'campaign',
        ),
        'Oro\\Bundle\\CampaignBundle\\Entity\\EmailCampaign' => 
        array (
          0 => 'campaign',
        ),
        'Oro\\Bundle\\ChannelBundle\\Entity\\Channel' => 
        array (
          0 => 'channels',
        ),
        'Oro\\Bundle\\TaskBundle\\Entity\\Task' => 
        array (
          0 => 'task',
        ),
        'Oro\\Bundle\\TaskBundle\\Entity\\TaskPriority' => 
        array (
          0 => 'task',
        ),
        'Extend\\Entity\\EV_Task_Status' => 
        array (
          0 => 'task',
        ),
        'Oro\\Bundle\\MarketingActivityBundle\\Entity\\MarketingActivity' => 
        array (
          0 => 'marketingactivity',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\Lead' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\LeadAddress' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\LeadEmail' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\LeadPhone' => 
        array (
          0 => 'sales_lead',
        ),
        'Extend\\Entity\\EV_Lead_Status' => 
        array (
          0 => 'sales_lead',
        ),
        'Extend\\Entity\\EV_Lead_Source' => 
        array (
          0 => 'sales_lead',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\Opportunity' => 
        array (
          0 => 'sales_opportunity',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\OpportunityCloseReason' => 
        array (
          0 => 'sales_opportunity',
        ),
        'Extend\\Entity\\EV_Opportunity_Status' => 
        array (
          0 => 'sales_opportunity',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomer' => 
        array (
          0 => 'business_customers',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerEmail' => 
        array (
          0 => 'business_customers',
        ),
        'Oro\\Bundle\\SalesBundle\\Entity\\B2bCustomerPhone' => 
        array (
          0 => 'business_customers',
        ),
        'Oro\\Bundle\\ContactUsBundle\\Entity\\ContactRequest' => 
        array (
          0 => 'contact_requests',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseEntity' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CasePriority' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseSource' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseStatus' => 
        array (
          0 => 'case',
        ),
        'Oro\\Bundle\\CaseBundle\\Entity\\CaseComment' => 
        array (
          0 => 'case',
        ),
      ),
      'placeholder_items' => 
      array (
        'force_sync_button' => 
        array (
          0 => 'manage_integrations',
        ),
        'sync_button' => 
        array (
          0 => 'manage_integrations',
        ),
        'view_oro_activity_list' => 
        array (
          0 => 'activity_lists',
        ),
        'update_oro_activity_list' => 
        array (
          0 => 'activity_lists',
        ),
        'activity_condition' => 
        array (
          0 => 'activity_lists',
        ),
        'change_history_block' => 
        array (
          0 => 'data_audit',
        ),
        'calendar_event_reminder_template' => 
        array (
          0 => 'calendar',
        ),
        'oro_add_calendar_event_button' => 
        array (
          0 => 'calendar',
        ),
        'oro_assign_calendar_event_button' => 
        array (
          0 => 'calendar',
        ),
        'oro_add_calendar_event_link' => 
        array (
          0 => 'calendar',
        ),
        'oro_assign_calendar_event_link' => 
        array (
          0 => 'calendar',
        ),
        'contacts_launchpad' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_add_contact_button' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_add_contact_link' => 
        array (
          0 => 'manage_contacts',
        ),
        'oro_edit_menus_button' => 
        array (
          0 => 'navigation_menu',
        ),
        'oro_edit_menus_link' => 
        array (
          0 => 'navigation_menu',
        ),
        'accounts_launchpad' => 
        array (
          0 => 'manage_accounts',
        ),
        'account_lifetime_value' => 
        array (
          0 => 'manage_accounts',
        ),
        'oro_log_call_button' => 
        array (
          0 => 'call',
        ),
        'oro_log_call_link' => 
        array (
          0 => 'call',
        ),
        'oro_phone_action_log_call_button' => 
        array (
          0 => 'call',
        ),
        'task_reminder_template' => 
        array (
          0 => 'task',
        ),
        'oro_user_tasks' => 
        array (
          0 => 'task',
        ),
        'oro_assign_task_button' => 
        array (
          0 => 'task',
        ),
        'oro_assign_task_link' => 
        array (
          0 => 'task',
        ),
        'oro_add_task_button' => 
        array (
          0 => 'task',
        ),
        'oro_add_task_link' => 
        array (
          0 => 'task',
        ),
        'oro_my_tasks_button' => 
        array (
          0 => 'task',
        ),
        'leads_launchpad' => 
        array (
          0 => 'sales_lead',
        ),
        'opportunities_launchpad' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_create_opportunity_link' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_sales_create_opportunity_button' => 
        array (
          0 => 'sales_opportunity',
        ),
        'oro_user_cases_grid' => 
        array (
          0 => 'case',
        ),
        'oro_account_cases_grid' => 
        array (
          0 => 'case',
        ),
        'oro_contact_cases_grid' => 
        array (
          0 => 'case',
        ),
      ),
      'mq_topics' => 
      array (
        'oro_marketing_list.message_queue.job.update_marketing_list' => 
        array (
          0 => 'marketing_list',
        ),
      ),
      'workflows' => 
      array (
        'task_flow' => 
        array (
          0 => 'task',
        ),
        'b2b_flow_lead' => 
        array (
          0 => 'sales_lead',
        ),
        'opportunity_flow' => 
        array (
          0 => 'sales_opportunity',
        ),
        'orocrm_contact_us_contact_request' => 
        array (
          0 => 'contact_requests',
        ),
      ),
      'processes' => 
      array (
        'convert_mailbox_email_to_lead' => 
        array (
          0 => 'sales_lead',
        ),
        'convert_mailbox_email_to_case' => 
        array (
          0 => 'case',
        ),
      ),
    ),
    'toggles' => 
    array (
      'oro_layout.development_settings_feature_enabled' => 'development_settings',
      'oro_attachment.processors_allowed' => 'attachment_post_processors_allowed',
      'oro_attachment.original_file_names_enabled' => 'attachment_original_filenames',
      'oro_email.feature_enabled' => 'email',
      'oro_api.web_api' => 'web_api',
      'oro_marketing_list.feature_enabled' => 'marketing_list',
      'oro_tracking.feature_enabled' => 'tracking',
      'oro_campaign.feature_enabled' => 'campaign',
      'oro_marketing_activity.feature_enabled' => 'marketingactivity',
      'oro_sales.lead_feature_enabled' => 'sales_lead',
      'oro_sales.opportunity_feature_enabled' => 'sales_opportunity',
    ),
  ),
);