<?php return array (
  'Oro\\Bundle\\OrganizationBundle\\Entity\\BusinessUnit' => 
  array (
    'create' => 
    array (
      0 => 'manage_business_units',
    ),
    'update' => 
    array (
      0 => 'manage_business_units',
    ),
    'delete' => 
    array (
      0 => 'manage_business_units',
    ),
    'delete_list' => 
    array (
      0 => 'manage_business_units',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_business_units',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_business_units',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_business_units',
    ),
  ),
  'Oro\\Bundle\\UserBundle\\Entity\\User' => 
  array (
    'create' => 
    array (
      0 => 'manage_users',
    ),
    'update' => 
    array (
      0 => 'manage_users',
    ),
    'delete' => 
    array (
      0 => 'manage_users',
    ),
    'delete_list' => 
    array (
      0 => 'manage_users',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_users',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_users',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_users',
    ),
  ),
  'Oro\\Bundle\\UserBundle\\Entity\\Role' => 
  array (
    'create' => 
    array (
      0 => 'manage_user_roles',
    ),
    'update' => 
    array (
      0 => 'manage_user_roles',
    ),
    'delete' => 
    array (
      0 => 'manage_user_roles',
    ),
    'delete_list' => 
    array (
      0 => 'manage_user_roles',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_user_roles',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_user_roles',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_user_roles',
    ),
  ),
  'Oro\\Bundle\\UserBundle\\Entity\\Group' => 
  array (
    'create' => 
    array (
      0 => 'manage_user_groups',
    ),
    'update' => 
    array (
      0 => 'manage_user_groups',
    ),
    'delete' => 
    array (
      0 => 'manage_user_groups',
    ),
    'delete_list' => 
    array (
      0 => 'manage_user_groups',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_user_groups',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_user_groups',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_user_groups',
    ),
  ),
  'Oro\\Bundle\\ContactBundle\\Entity\\Group' => 
  array (
    'create' => 
    array (
      0 => 'manage_contact_groups',
    ),
    'update' => 
    array (
      0 => 'manage_contact_groups',
    ),
    'delete' => 
    array (
      0 => 'manage_contact_groups',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contact_groups',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contact_groups',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contact_groups',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contact_groups',
    ),
  ),
  'Oro\\Bundle\\ContactBundle\\Entity\\Contact' => 
  array (
    'create' => 
    array (
      0 => 'manage_contacts',
    ),
    'update' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contacts',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
  ),
  'Oro\\Bundle\\ContactBundle\\Entity\\ContactAddress' => 
  array (
    'create' => 
    array (
      0 => 'manage_contacts',
    ),
    'update' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contacts',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
  ),
  'Oro\\Bundle\\ContactBundle\\Entity\\ContactEmail' => 
  array (
    'create' => 
    array (
      0 => 'manage_contacts',
    ),
    'update' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contacts',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
  ),
  'Oro\\Bundle\\ContactBundle\\Entity\\ContactPhone' => 
  array (
    'create' => 
    array (
      0 => 'manage_contacts',
    ),
    'update' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contacts',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
  ),
  'Oro\\Bundle\\ContactBundle\\Entity\\Method' => 
  array (
    'create' => 
    array (
      0 => 'manage_contacts',
    ),
    'update' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contacts',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
  ),
  'Oro\\Bundle\\ContactBundle\\Entity\\Source' => 
  array (
    'create' => 
    array (
      0 => 'manage_contacts',
    ),
    'update' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contacts',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contacts',
    ),
  ),
  'Oro\\Bundle\\AccountBundle\\Entity\\Account' => 
  array (
    'create' => 
    array (
      0 => 'manage_accounts',
    ),
    'update' => 
    array (
      0 => 'manage_accounts',
    ),
    'delete' => 
    array (
      0 => 'manage_accounts',
    ),
    'delete_list' => 
    array (
      0 => 'manage_accounts',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_accounts',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_accounts',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_accounts',
    ),
  ),
  'Oro\\Bundle\\ContactUsBundle\\Entity\\ContactReason' => 
  array (
    'create' => 
    array (
      0 => 'manage_contact_reasons',
    ),
    'update' => 
    array (
      0 => 'manage_contact_reasons',
    ),
    'delete' => 
    array (
      0 => 'manage_contact_reasons',
    ),
    'delete_list' => 
    array (
      0 => 'manage_contact_reasons',
    ),
    'update_relationship' => 
    array (
      0 => 'manage_contact_reasons',
    ),
    'add_relationship' => 
    array (
      0 => 'manage_contact_reasons',
    ),
    'delete_relationship' => 
    array (
      0 => 'manage_contact_reasons',
    ),
  ),
);