<?php return array (
  'embedded_default' => 
  array (
    'label' => 'Default theme for embedded forms',
    'groups' => 
    array (
      0 => 'embedded_forms',
    ),
    'image_placeholders' => 
    array (
    ),
    'extra_js_builds' => 
    array (
    ),
  ),
  'default' => 
  array (
    'label' => 'Default theme',
    'description' => 'Default theme.',
    'groups' => 
    array (
      0 => 'main',
    ),
    'config' => 
    array (
      'assets' => 
      array (
        'styles' => 
        array (
          'inputs' => 
          array (
            0 => 'bundles/oroui/default/scss/settings/global-settings.scss',
            1 => 'bundles/oroui/default/scss/settings/font-awesome/font-awesome.scss',
            2 => 'bundles/orodatagrid/default/scss/variables/no-data-config.scss',
            3 => 'bundles/orodatagrid/default/scss/variables/datagrid-table-config.scss',
            4 => 'bundles/orodatagrid/default/scss/variables/datagrid-header-cell-config.scss',
            5 => 'bundles/orodatagrid/default/scss/variables/datagrid-body-cell-config.scss',
            6 => 'bundles/orodatagrid/default/scss/variables/more-bar-holder-config.scss',
            7 => 'bundles/orodatagrid/default/scss/variables/data-blank-content-config.scss',
            8 => 'bundles/orodatagrid/default/scss/components/data-blank-content.scss',
            9 => 'bundles/orodatagrid/default/scss/components/no-data.scss',
            10 => 'bundles/orodatagrid/default/scss/components/oro-datagrid.scss',
            11 => 'bundles/orodatagrid/default/scss/components/datagrid-table.scss',
            12 => 'bundles/orodatagrid/default/scss/components/datagrid-header-cell.scss',
            13 => 'bundles/orodatagrid/default/scss/components/datagrid-body-cell.scss',
            14 => 'bundles/orodatagrid/default/scss/components/datagrid-row.scss',
            15 => 'bundles/orodatagrid/default/scss/components/launcher-list.scss',
            16 => 'bundles/orodatagrid/default/scss/components/launchers-dropdown-menu.scss',
            17 => 'bundles/orodatagrid/default/scss/components/more-bar-holder.scss',
            18 => 'bundles/orofilter/default/scss/variables/ui-timepicker-list-config.scss',
            19 => 'bundles/orofilter/default/scss/components/ui-timepicker-list.scss',
            20 => 'bundles/oroform/default/scss/settings/global-settings.scss',
            21 => 'bundles/oroform/default/scss/variables/catalog-switcher-config.scss',
            22 => 'bundles/oroform/default/scss/variables/checkbox-config.scss',
            23 => 'bundles/oroform/default/scss/variables/checkbox-label-config.scss',
            24 => 'bundles/oroform/default/scss/variables/datepicker-config.scss',
            25 => 'bundles/oroform/default/scss/variables/input-config.scss',
            26 => 'bundles/oroform/default/scss/variables/select-config.scss',
            27 => 'bundles/oroform/default/scss/variables/select2/select2-container-config.scss',
            28 => 'bundles/oroform/default/scss/variables/select2/select2-container-multi-config.scss',
            29 => 'bundles/oroform/default/scss/variables/oro-toolbar-config.scss',
            30 => 'bundles/oroform/default/scss/variables/textarea-config.scss',
            31 => 'bundles/oroform/default/scss/variables/label-config.scss',
            32 => 'bundles/oroform/default/scss/variables/form-row-config.scss',
            33 => 'bundles/oroform/default/scss/variables/focus-visible-config.scss',
            34 => 'bundles/oroform/default/scss/variables/floating-validation-message-config.scss',
            35 => 'bundles/oroform/default/scss/variables/validation-config.scss',
            36 => 'bundles/oroform/default/scss/components/datepicker.scss',
            37 => 'bundles/oroform/default/scss/components/oro-toolbar.scss',
            38 => 'bundles/oroform/default/scss/components/utilities.scss',
            39 => 'bundles/oroform/default/scss/components/required-label.scss',
            40 => 'bundles/oroform/default/scss/components/validation.scss',
            41 => 'bundles/oroform/default/scss/components/buttons-equal.scss',
            42 => 'bundles/oroform/default/scss/components/checkbox.scss',
            43 => 'bundles/oroform/default/scss/components/checkbox-label.scss',
            44 => 'bundles/oroform/default/scss/components/label.scss',
            45 => 'bundles/oroform/default/scss/components/form-row.scss',
            46 => 'bundles/oroform/default/scss/components/focus-visible.scss',
            47 => 'bundles/oroform/default/scss/components/floating-validation-message.scss',
            48 => 'bundles/oroform/default/scss/components/input.scss',
            49 => 'bundles/oroform/default/scss/components/fake-masked-input.scss',
            50 => 'bundles/oroform/default/scss/components/catalog-switcher.scss',
            51 => 'bundles/oroform/default/scss/components/select.scss',
            52 => 'bundles/oroform/default/scss/components/textarea.scss',
            53 => 'bundles/oroform/default/scss/components/select2/select2-container.scss',
            54 => 'bundles/oroform/default/scss/components/select2/select2-container-multi.scss',
          ),
          'output' => 'css/styles.css',
          'auto_rtl_inputs' => 
          array (
          ),
          'filters' => 
          array (
          ),
        ),
        'stylebook_styles' => 
        array (
          'inputs' => 
          array (
            0 => 'bundles/oroui/default/scss/settings/global-settings.scss',
            1 => 'bundles/oroui/default/scss/settings/font-awesome/font-awesome.scss',
            2 => 'bundles/oroform/default/scss/settings/global-settings.scss',
          ),
          'auto_rtl_inputs' => 
          array (
          ),
          'filters' => 
          array (
          ),
        ),
        'print_styles' => 
        array (
          'inputs' => 
          array (
            0 => 'bundles/oroui/default/scss/settings/global-settings.scss',
            1 => 'bundles/oroui/default/scss/settings/font-awesome/font-awesome.scss',
            2 => 'bundles/orodatagrid/default/scss/components/print/datagrid-header-cell.scss',
          ),
          'output' => 'css/styles-print.css',
          'auto_rtl_inputs' => 
          array (
            0 => 'bundles/oro*/**',
          ),
          'filters' => 
          array (
          ),
        ),
      ),
    ),
    'image_placeholders' => 
    array (
    ),
    'extra_js_builds' => 
    array (
    ),
  ),
  'view-switcher' => 
  array (
    'label' => 'View switcher theme',
    'description' => 'View switcher theme description.',
    'groups' => 
    array (
      0 => 'main',
    ),
    'config' => 
    array (
      'assets' => 
      array (
        'styles' => 
        array (
          'inputs' => 
          array (
            0 => 'bundles/oroui/css/scss/settings/global-settings.scss',
            1 => '~@oroinc/slick-carousel/slick/slick.scss',
            2 => 'bundles/oroui/css/scss/oro/loading/loading-bar.scss',
            3 => 'bundles/oroui/css/scss/font-awesome/_core.scss',
            4 => 'bundles/oroviewswitcher/view-switcher/scss/styles.scss',
          ),
          'output' => 'css/view-switcher.css',
          'auto_rtl_inputs' => 
          array (
          ),
          'filters' => 
          array (
          ),
        ),
      ),
    ),
    'image_placeholders' => 
    array (
    ),
    'extra_js_builds' => 
    array (
    ),
  ),
);