<?php

namespace ContainerTt1aQQa;


include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectRepository.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/collections/lib/Doctrine/Common/Collections/Selectable.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityRepository.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle/Repository/ServiceEntityRepositoryInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/doctrine-bundle/Repository/ServiceEntityRepository.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/SearchBundle/Engine/Orm/DBALPersisterInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/SearchBundle/Entity/Repository/SearchIndexRepository.php';
class SearchIndexRepository_57173b6 extends \Oro\Bundle\SearchBundle\Entity\Repository\SearchIndexRepository implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function search(\Oro\Bundle\SearchBundle\Query\Query $query)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'search', array('query' => $query), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->search($query);
    }
    public function getDocumentsCountGroupByEntityFQCN(\Oro\Bundle\SearchBundle\Query\Query $query) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDocumentsCountGroupByEntityFQCN', array('query' => $query), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDocumentsCountGroupByEntityFQCN($query);
    }
    public function getRecordsCount(\Oro\Bundle\SearchBundle\Query\Query $query)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRecordsCount', array('query' => $query), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRecordsCount($query);
    }
    public function getAggregatedData(\Oro\Bundle\SearchBundle\Query\Query $query)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAggregatedData', array('query' => $query), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAggregatedData($query);
    }
    public function setDriversClasses($drivers)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setDriversClasses', array('drivers' => $drivers), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setDriversClasses($drivers);
    }
    public function truncateIndex()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'truncateIndex', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->truncateIndex();
    }
    public function writeItem(\Oro\Bundle\SearchBundle\Entity\AbstractItem $item)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'writeItem', array('item' => $item), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->writeItem($item);
    }
    public function flushWrites()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'flushWrites', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->flushWrites();
    }
    public function getItemsForEntities(array $entities)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getItemsForEntities', array('entities' => $entities), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getItemsForEntities($entities);
    }
    public function setRegistry($registry)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setRegistry', array('registry' => $registry), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setRegistry($registry);
    }
    public function removeEntities(array $entityIds, $entityClass, $entityAlias = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'removeEntities', array('entityIds' => $entityIds, 'entityClass' => $entityClass, 'entityAlias' => $entityAlias), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->removeEntities($entityIds, $entityClass, $entityAlias);
    }
    public function createQueryBuilder($alias, $indexBy = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createQueryBuilder', array('alias' => $alias, 'indexBy' => $indexBy), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createQueryBuilder($alias, $indexBy);
    }
    public function createResultSetMappingBuilder($alias)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createResultSetMappingBuilder', array('alias' => $alias), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createResultSetMappingBuilder($alias);
    }
    public function createNamedQuery($queryName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createNamedQuery', array('queryName' => $queryName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createNamedQuery($queryName);
    }
    public function createNativeNamedQuery($queryName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createNativeNamedQuery', array('queryName' => $queryName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createNativeNamedQuery($queryName);
    }
    public function clear()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'clear', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->clear();
    }
    public function find($id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'find', array('id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->find($id, $lockMode, $lockVersion);
    }
    public function findAll()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'findAll', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->findAll();
    }
    public function findBy(array $criteria, ?array $orderBy = null, $limit = null, $offset = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'findBy', array('criteria' => $criteria, 'orderBy' => $orderBy, 'limit' => $limit, 'offset' => $offset), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->findBy($criteria, $orderBy, $limit, $offset);
    }
    public function findOneBy(array $criteria, ?array $orderBy = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'findOneBy', array('criteria' => $criteria, 'orderBy' => $orderBy), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->findOneBy($criteria, $orderBy);
    }
    public function count(array $criteria)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'count', array('criteria' => $criteria), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->count($criteria);
    }
    public function __call($method, $arguments)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__call', array('method' => $method, 'arguments' => $arguments), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->__call($method, $arguments);
    }
    public function getClassName()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getClassName', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getClassName();
    }
    public function matching(\Doctrine\Common\Collections\Criteria $criteria)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'matching', array('criteria' => $criteria), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->matching($criteria);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->driverRepo, $instance->drivers, $instance->registry, $instance->managers, $instance->_entityName, $instance->_em, $instance->_class);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry, string $entityClass)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\SearchBundle\\Entity\\Repository\\SearchIndexRepository');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->driverRepo, $this->drivers, $this->registry, $this->managers, $this->_entityName, $this->_em, $this->_class);
        }
        $this->valueHolder6655f->__construct($registry, $entityClass);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SearchBundle\\Entity\\Repository\\SearchIndexRepository');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SearchBundle\\Entity\\Repository\\SearchIndexRepository');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SearchBundle\\Entity\\Repository\\SearchIndexRepository');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SearchBundle\\Entity\\Repository\\SearchIndexRepository');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->driverRepo, $this->drivers, $this->registry, $this->managers, $this->_entityName, $this->_em, $this->_class);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('SearchIndexRepository_57173b6', false)) {
    \class_alias(__NAMESPACE__.'\\SearchIndexRepository_57173b6', 'SearchIndexRepository_57173b6', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/Download/TranslationDownloader.php';
class TranslationDownloader_7847ac2 extends \Oro\Bundle\TranslationBundle\Download\TranslationDownloader implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function fetchLanguageMetrics(string $languageCode) : ?array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'fetchLanguageMetrics', array('languageCode' => $languageCode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->fetchLanguageMetrics($languageCode);
    }
    public function downloadAndApplyTranslations(string $languageCode) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'downloadAndApplyTranslations', array('languageCode' => $languageCode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->downloadAndApplyTranslations($languageCode);
return;
    }
    public function downloadTranslationsArchive(string $languageCode, string $filePathToSaveDownloadedArchive) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'downloadTranslationsArchive', array('languageCode' => $languageCode, 'filePathToSaveDownloadedArchive' => $filePathToSaveDownloadedArchive), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->downloadTranslationsArchive($languageCode, $filePathToSaveDownloadedArchive);
return;
    }
    public function loadTranslationsFromArchive(string $pathToArchiveFile, string $languageCode) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'loadTranslationsFromArchive', array('pathToArchiveFile' => $pathToArchiveFile, 'languageCode' => $languageCode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->loadTranslationsFromArchive($pathToArchiveFile, $languageCode);
return;
    }
    public function getTmpDir(string $prefix) : string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getTmpDir', array('prefix' => $prefix), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getTmpDir($prefix);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\Download\TranslationDownloader $instance) {
            unset($instance->translationServiceAdapter, $instance->translationMetricsProvider, $instance->jsTranslationDumper, $instance->translationReader, $instance->databasePersister, $instance->doctrine);
        }, $instance, 'Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\TranslationBundle\Download\TranslationServiceAdapterInterface $translationServiceAdapter, \Oro\Bundle\TranslationBundle\Download\TranslationMetricsProviderInterface $translationMetricsProvider, \Oro\Bundle\TranslationBundle\Provider\JsTranslationDumper $jsTranslationDumper, \Symfony\Component\Translation\Reader\TranslationReader $translationReader, \Oro\Bundle\TranslationBundle\Translation\DatabasePersister $translationDatabasePersister, \Doctrine\Persistence\ManagerRegistry $doctrine)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\Download\TranslationDownloader $instance) {
            unset($instance->translationServiceAdapter, $instance->translationMetricsProvider, $instance->jsTranslationDumper, $instance->translationReader, $instance->databasePersister, $instance->doctrine);
        }, $this, 'Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($translationServiceAdapter, $translationMetricsProvider, $jsTranslationDumper, $translationReader, $translationDatabasePersister, $doctrine);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\Download\TranslationDownloader $instance) {
            unset($instance->translationServiceAdapter, $instance->translationMetricsProvider, $instance->jsTranslationDumper, $instance->translationReader, $instance->databasePersister, $instance->doctrine);
        }, $this, 'Oro\\Bundle\\TranslationBundle\\Download\\TranslationDownloader')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('TranslationDownloader_7847ac2', false)) {
    \class_alias(__NAMESPACE__.'\\TranslationDownloader_7847ac2', 'TranslationDownloader_7847ac2', false);
}

include_once \dirname(__DIR__, 4).'/vendor/knplabs/gaufrette/src/Gaufrette/FilesystemInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/knplabs/gaufrette/src/Gaufrette/Filesystem.php';
class Filesystem_f929d5e extends \Gaufrette\Filesystem implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getAdapter()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAdapter', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAdapter();
    }
    public function has($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'has', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->has($key);
    }
    public function rename($sourceKey, $targetKey)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'rename', array('sourceKey' => $sourceKey, 'targetKey' => $targetKey), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->rename($sourceKey, $targetKey);
    }
    public function get($key, $create = false)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'get', array('key' => $key, 'create' => $create), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->get($key, $create);
    }
    public function write($key, $content, $overwrite = false)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'write', array('key' => $key, 'content' => $content, 'overwrite' => $overwrite), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->write($key, $content, $overwrite);
    }
    public function read($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'read', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->read($key);
    }
    public function delete($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'delete', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->delete($key);
    }
    public function keys()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'keys', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->keys();
    }
    public function listKeys($prefix = '')
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'listKeys', array('prefix' => $prefix), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->listKeys($prefix);
    }
    public function mtime($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'mtime', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->mtime($key);
    }
    public function checksum($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'checksum', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->checksum($key);
    }
    public function size($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'size', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->size($key);
    }
    public function createStream($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createStream', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createStream($key);
    }
    public function createFile($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createFile', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createFile($key);
    }
    public function mimeType($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'mimeType', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->mimeType($key);
    }
    public function clearFileRegister()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'clearFileRegister', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->clearFileRegister();
    }
    public function removeFromRegister($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'removeFromRegister', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->removeFromRegister($key);
    }
    public function isDirectory($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isDirectory', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isDirectory($key);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->adapter, $instance->fileRegister);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Gaufrette\Adapter $adapter)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Gaufrette\\Filesystem');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->adapter, $this->fileRegister);
        }
        $this->valueHolder6655f->__construct($adapter);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Gaufrette\\Filesystem');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Gaufrette\\Filesystem');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Gaufrette\\Filesystem');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Gaufrette\\Filesystem');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->adapter, $this->fileRegister);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('Filesystem_f929d5e', false)) {
    \class_alias(__NAMESPACE__.'\\Filesystem_f929d5e', 'Filesystem_f929d5e', false);
}

include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/ObjectManager.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManagerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/EntityManager.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityBundle/ORM/OroEntityManager.php';
class OroEntityManager_a880071 extends \Oro\Bundle\EntityBundle\ORM\OroEntityManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setMetadataFactory(\Doctrine\ORM\Mapping\ClassMetadataFactory $metadataFactory)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setMetadataFactory', array('metadataFactory' => $metadataFactory), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setMetadataFactory($metadataFactory);
    }
    public function setMetadataReflectionService(\Doctrine\Persistence\Mapping\ReflectionService $reflectionService) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setMetadataReflectionService', array('reflectionService' => $reflectionService), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setMetadataReflectionService($reflectionService);
return;
    }
    public function close()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'close', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->close();
    }
    public function createQuery($dql = '')
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createQuery', array('dql' => $dql), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createQuery($dql);
    }
    public function clear($entityName = null) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'clear', array('entityName' => $entityName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->clear($entityName);
return;
    }
    public function getConnection()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getConnection', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getConnection();
    }
    public function getMetadataFactory()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMetadataFactory', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMetadataFactory();
    }
    public function getExpressionBuilder()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getExpressionBuilder', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getExpressionBuilder();
    }
    public function beginTransaction()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'beginTransaction', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->beginTransaction();
    }
    public function getCache()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getCache', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getCache();
    }
    public function transactional($func)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'transactional', array('func' => $func), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->transactional($func);
    }
    public function wrapInTransaction(callable $func)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'wrapInTransaction', array('func' => $func), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->wrapInTransaction($func);
    }
    public function commit()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'commit', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->commit();
    }
    public function rollback()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'rollback', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->rollback();
    }
    public function getClassMetadata($className)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getClassMetadata', array('className' => $className), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getClassMetadata($className);
    }
    public function createNamedQuery($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createNamedQuery', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createNamedQuery($name);
    }
    public function createNativeQuery($sql, \Doctrine\ORM\Query\ResultSetMapping $rsm)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createNativeQuery', array('sql' => $sql, 'rsm' => $rsm), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createNativeQuery($sql, $rsm);
    }
    public function createNamedNativeQuery($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createNamedNativeQuery', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createNamedNativeQuery($name);
    }
    public function createQueryBuilder()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createQueryBuilder', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createQueryBuilder();
    }
    public function flush($entity = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'flush', array('entity' => $entity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->flush($entity);
    }
    public function find($className, $id, $lockMode = null, $lockVersion = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'find', array('className' => $className, 'id' => $id, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->find($className, $id, $lockMode, $lockVersion);
    }
    public function getReference($entityName, $id)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getReference', array('entityName' => $entityName, 'id' => $id), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getReference($entityName, $id);
    }
    public function getPartialReference($entityName, $identifier)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getPartialReference', array('entityName' => $entityName, 'identifier' => $identifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getPartialReference($entityName, $identifier);
    }
    public function persist($entity)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'persist', array('entity' => $entity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->persist($entity);
    }
    public function remove($entity)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'remove', array('entity' => $entity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->remove($entity);
    }
    public function refresh($entity, ?int $lockMode = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'refresh', array('entity' => $entity, 'lockMode' => $lockMode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->refresh($entity, $lockMode);
    }
    public function detach($entity)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'detach', array('entity' => $entity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->detach($entity);
    }
    public function merge($entity)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'merge', array('entity' => $entity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->merge($entity);
    }
    public function copy($entity, $deep = false)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'copy', array('entity' => $entity, 'deep' => $deep), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->copy($entity, $deep);
    }
    public function lock($entity, $lockMode, $lockVersion = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'lock', array('entity' => $entity, 'lockMode' => $lockMode, 'lockVersion' => $lockVersion), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->lock($entity, $lockMode, $lockVersion);
    }
    public function getRepository($entityName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRepository', array('entityName' => $entityName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRepository($entityName);
    }
    public function contains($entity)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'contains', array('entity' => $entity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->contains($entity);
    }
    public function getEventManager()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEventManager', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEventManager();
    }
    public function getConfiguration()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getConfiguration', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getConfiguration();
    }
    public function isOpen()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isOpen', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isOpen();
    }
    public function getUnitOfWork()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getUnitOfWork', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getUnitOfWork();
    }
    public function getHydrator($hydrationMode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getHydrator($hydrationMode);
    }
    public function newHydrator($hydrationMode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'newHydrator', array('hydrationMode' => $hydrationMode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->newHydrator($hydrationMode);
    }
    public function getProxyFactory()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getProxyFactory', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getProxyFactory();
    }
    public function initializeObject($obj)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeObject', array('obj' => $obj), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->initializeObject($obj);
    }
    public function getFilters()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getFilters', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getFilters();
    }
    public function isFiltersStateClean()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isFiltersStateClean', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isFiltersStateClean();
    }
    public function hasFilters()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'hasFilters', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->hasFilters();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\EntityBundle\ORM\OroEntityManager $instance) {
            unset($instance->defaultQueryCacheLifetime);
        }, $instance, 'Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager')->__invoke($instance);
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $instance, 'Doctrine\\ORM\\EntityManager')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Doctrine\DBAL\Connection $conn, \Doctrine\ORM\Configuration $config, ?\Doctrine\Common\EventManager $eventManager = null)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\EntityBundle\ORM\OroEntityManager $instance) {
            unset($instance->defaultQueryCacheLifetime);
        }, $this, 'Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager')->__invoke($this);
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($conn, $config, $eventManager);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\EntityBundle\ORM\OroEntityManager $instance) {
            unset($instance->defaultQueryCacheLifetime);
        }, $this, 'Oro\\Bundle\\EntityBundle\\ORM\\OroEntityManager')->__invoke($this);
        \Closure::bind(function (\Doctrine\ORM\EntityManager $instance) {
            unset($instance->config, $instance->conn, $instance->metadataFactory, $instance->unitOfWork, $instance->eventManager, $instance->proxyFactory, $instance->repositoryFactory, $instance->expressionBuilder, $instance->closed, $instance->filterCollection, $instance->cache);
        }, $this, 'Doctrine\\ORM\\EntityManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('OroEntityManager_a880071', false)) {
    \class_alias(__NAMESPACE__.'\\OroEntityManager_a880071', 'OroEntityManager_a880071', false);
}

include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-menu/src/Knp/Menu/Renderer/RendererInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/knplabs/knp-menu/src/Knp/Menu/Renderer/TwigRenderer.php';
class TwigRenderer_85c1049 extends \Knp\Menu\Renderer\TwigRenderer implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function render(\Knp\Menu\ItemInterface $item, array $options = []) : string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'render', array('item' => $item, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->render($item, $options);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Knp\Menu\Renderer\TwigRenderer $instance) {
            unset($instance->environment, $instance->matcher, $instance->defaultOptions);
        }, $instance, 'Knp\\Menu\\Renderer\\TwigRenderer')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Twig\Environment $environment, string $template, \Knp\Menu\Matcher\MatcherInterface $matcher, array $defaultOptions = [])
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Knp\\Menu\\Renderer\\TwigRenderer');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Knp\Menu\Renderer\TwigRenderer $instance) {
            unset($instance->environment, $instance->matcher, $instance->defaultOptions);
        }, $this, 'Knp\\Menu\\Renderer\\TwigRenderer')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($environment, $template, $matcher, $defaultOptions);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Knp\\Menu\\Renderer\\TwigRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Knp\\Menu\\Renderer\\TwigRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Knp\\Menu\\Renderer\\TwigRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Knp\\Menu\\Renderer\\TwigRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Knp\Menu\Renderer\TwigRenderer $instance) {
            unset($instance->environment, $instance->matcher, $instance->defaultOptions);
        }, $this, 'Knp\\Menu\\Renderer\\TwigRenderer')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('TwigRenderer_85c1049', false)) {
    \class_alias(__NAMESPACE__.'\\TwigRenderer_85c1049', 'TwigRenderer_85c1049', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/api-doc-bundle/Nelmio/ApiDocBundle/Extractor/ApiDocExtractor.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/api-doc-bundle/Nelmio/ApiDocBundle/Extractor/CachingApiDocExtractor.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Component/Routing/Resolver/RouteOptionsResolverAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ApiBundle/ApiDoc/RestDocViewDetectorAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ApiBundle/ApiDoc/Extractor/ApiDocExtractorTrait.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ApiBundle/ApiDoc/Extractor/CachingApiDocExtractor.php';
class CachingApiDocExtractor_ee4c584 extends \Oro\Bundle\ApiBundle\ApiDoc\Extractor\CachingApiDocExtractor implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getRoutes()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRoutes', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRoutes();
    }
    public function all($view = 'default')
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'all', array('view' => $view), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->all($view);
    }
    public function extractAnnotations(array $routes, $view = 'default')
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'extractAnnotations', array('routes' => $routes, 'view' => $view), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->extractAnnotations($routes, $view);
    }
    public function warmUp(string $view = 'default') : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'warmUp', array('view' => $view), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->warmUp($view);
return;
    }
    public function clear(string $view = 'default') : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'clear', array('view' => $view), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->clear($view);
return;
    }
    public function getReflectionMethod($controller)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getReflectionMethod', array('controller' => $controller), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getReflectionMethod($controller);
    }
    public function get($controller, $route)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'get', array('controller' => $controller, 'route' => $route), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->get($controller, $route);
    }
    public function addParser(\Nelmio\ApiDocBundle\Parser\ParserInterface $parser)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'addParser', array('parser' => $parser), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->addParser($parser);
    }
    public function setRouteOptionsResolver(\Oro\Component\Routing\Resolver\RouteOptionsResolverInterface $routeOptionsResolver) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setRouteOptionsResolver', array('routeOptionsResolver' => $routeOptionsResolver), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setRouteOptionsResolver($routeOptionsResolver);
return;
    }
    public function setRestDocViewDetector(\Oro\Bundle\ApiBundle\ApiDoc\RestDocViewDetector $docViewDetector) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setRestDocViewDetector', array('docViewDetector' => $docViewDetector), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setRestDocViewDetector($docViewDetector);
return;
    }
    public function setApiDocAnnotationHandler(\Oro\Bundle\ApiBundle\ApiDoc\AnnotationHandler\ApiDocAnnotationHandlerInterface $apiDocAnnotationHandler) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setApiDocAnnotationHandler', array('apiDocAnnotationHandler' => $apiDocAnnotationHandler), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setApiDocAnnotationHandler($apiDocAnnotationHandler);
return;
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->container, $instance->router, $instance->reader, $instance->parsers, $instance->handlers, $instance->annotationsProviders);
        \Closure::bind(function (\Oro\Bundle\ApiBundle\ApiDoc\Extractor\CachingApiDocExtractor $instance) {
            unset($instance->cacheFile, $instance->debug, $instance->routes, $instance->routeOptionsResolver, $instance->docViewDetector, $instance->apiDocAnnotationHandler);
        }, $instance, 'Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor')->__invoke($instance);
        \Closure::bind(function (\Nelmio\ApiDocBundle\Extractor\CachingApiDocExtractor $instance) {
            unset($instance->cacheFile, $instance->debug);
        }, $instance, 'Nelmio\\ApiDocBundle\\Extractor\\CachingApiDocExtractor')->__invoke($instance);
        \Closure::bind(function (\Nelmio\ApiDocBundle\Extractor\ApiDocExtractor $instance) {
            unset($instance->commentExtractor);
        }, $instance, 'Nelmio\\ApiDocBundle\\Extractor\\ApiDocExtractor')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Symfony\Component\DependencyInjection\ContainerInterface $container, \Symfony\Component\Routing\RouterInterface $router, \Doctrine\Common\Annotations\Reader $reader, \Nelmio\ApiDocBundle\Util\DocCommentExtractor $commentExtractor, array $handlers, array $annotationsProviders, string $cacheFile, bool $debug = false)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->container, $this->router, $this->reader, $this->parsers, $this->handlers, $this->annotationsProviders);
        \Closure::bind(function (\Oro\Bundle\ApiBundle\ApiDoc\Extractor\CachingApiDocExtractor $instance) {
            unset($instance->cacheFile, $instance->debug, $instance->routes, $instance->routeOptionsResolver, $instance->docViewDetector, $instance->apiDocAnnotationHandler);
        }, $this, 'Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor')->__invoke($this);
        \Closure::bind(function (\Nelmio\ApiDocBundle\Extractor\CachingApiDocExtractor $instance) {
            unset($instance->cacheFile, $instance->debug);
        }, $this, 'Nelmio\\ApiDocBundle\\Extractor\\CachingApiDocExtractor')->__invoke($this);
        \Closure::bind(function (\Nelmio\ApiDocBundle\Extractor\ApiDocExtractor $instance) {
            unset($instance->commentExtractor);
        }, $this, 'Nelmio\\ApiDocBundle\\Extractor\\ApiDocExtractor')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($container, $router, $reader, $commentExtractor, $handlers, $annotationsProviders, $cacheFile, $debug);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->container, $this->router, $this->reader, $this->parsers, $this->handlers, $this->annotationsProviders);
        \Closure::bind(function (\Oro\Bundle\ApiBundle\ApiDoc\Extractor\CachingApiDocExtractor $instance) {
            unset($instance->cacheFile, $instance->debug, $instance->routes, $instance->routeOptionsResolver, $instance->docViewDetector, $instance->apiDocAnnotationHandler);
        }, $this, 'Oro\\Bundle\\ApiBundle\\ApiDoc\\Extractor\\CachingApiDocExtractor')->__invoke($this);
        \Closure::bind(function (\Nelmio\ApiDocBundle\Extractor\CachingApiDocExtractor $instance) {
            unset($instance->cacheFile, $instance->debug);
        }, $this, 'Nelmio\\ApiDocBundle\\Extractor\\CachingApiDocExtractor')->__invoke($this);
        \Closure::bind(function (\Nelmio\ApiDocBundle\Extractor\ApiDocExtractor $instance) {
            unset($instance->commentExtractor);
        }, $this, 'Nelmio\\ApiDocBundle\\Extractor\\ApiDocExtractor')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('CachingApiDocExtractor_ee4c584', false)) {
    \class_alias(__NAMESPACE__.'\\CachingApiDocExtractor_ee4c584', 'CachingApiDocExtractor_ee4c584', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ActivityListBundle/Filter/RelatedActivityDatagridFactory.php';
class RelatedActivityDatagridFactory_57e9779 extends \Oro\Bundle\ActivityListBundle\Filter\RelatedActivityDatagridFactory implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function createGrid(\Oro\Bundle\QueryDesignerBundle\Model\AbstractQueryDesigner $source) : \Oro\Bundle\DataGridBundle\Datagrid\DatagridInterface
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createGrid', array('source' => $source), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createGrid($source);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\ActivityListBundle\Filter\RelatedActivityDatagridFactory $instance) {
            unset($instance->datagridConfigurationBuilder, $instance->datagridBuilder, $instance->eventDispatcher);
        }, $instance, 'Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\QueryDesignerBundle\Grid\DatagridConfigurationBuilder $datagridConfigurationBuilder, \Oro\Bundle\DataGridBundle\Datagrid\Builder $datagridBuilder, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\ActivityListBundle\Filter\RelatedActivityDatagridFactory $instance) {
            unset($instance->datagridConfigurationBuilder, $instance->datagridBuilder, $instance->eventDispatcher);
        }, $this, 'Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($datagridConfigurationBuilder, $datagridBuilder, $eventDispatcher);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\ActivityListBundle\Filter\RelatedActivityDatagridFactory $instance) {
            unset($instance->datagridConfigurationBuilder, $instance->datagridBuilder, $instance->eventDispatcher);
        }, $this, 'Oro\\Bundle\\ActivityListBundle\\Filter\\RelatedActivityDatagridFactory')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('RelatedActivityDatagridFactory_57e9779', false)) {
    \class_alias(__NAMESPACE__.'\\RelatedActivityDatagridFactory_57e9779', 'RelatedActivityDatagridFactory_57e9779', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Config/ConfigManager.php';
class ConfigManager_0478e9a extends \Oro\Bundle\ConfigBundle\Config\ConfigManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function addManager(string $scope, \Oro\Bundle\ConfigBundle\Config\AbstractScopeManager $manager) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'addManager', array('scope' => $scope, 'manager' => $manager), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->addManager($scope, $manager);
return;
    }
    public function getScopeId()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getScopeId', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getScopeId();
    }
    public function setScopeIdFromEntity($entity)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setScopeIdFromEntity', array('entity' => $entity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setScopeIdFromEntity($entity);
    }
    public function setScopeId($scopeId)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setScopeId', array('scopeId' => $scopeId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setScopeId($scopeId);
    }
    public function getScopeEntityName()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getScopeEntityName', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getScopeEntityName();
    }
    public function getScopeInfo()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getScopeInfo', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getScopeInfo();
    }
    public function get($name, $default = false, $full = false, $scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'get', array('name' => $name, 'default' => $default, 'full' => $full, 'scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->get($name, $default, $full, $scopeIdentifier);
    }
    public function getValues($name, array $scopeIdentifiers, $default = false, $full = false)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getValues', array('name' => $name, 'scopeIdentifiers' => $scopeIdentifiers, 'default' => $default, 'full' => $full), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getValues($name, $scopeIdentifiers, $default, $full);
    }
    public function getInfo($name, $scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getInfo', array('name' => $name, 'scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getInfo($name, $scopeIdentifier);
    }
    public function set($name, $value, $scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'set', array('name' => $name, 'value' => $value, 'scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->set($name, $value, $scopeIdentifier);
    }
    public function reset($name, $scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'reset', array('name' => $name, 'scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->reset($name, $scopeIdentifier);
    }
    public function deleteScope($scopeIdentifier) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'deleteScope', array('scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->deleteScope($scopeIdentifier);
return;
    }
    public function flush($scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'flush', array('scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->flush($scopeIdentifier);
    }
    public function save($settings, $scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'save', array('settings' => $settings, 'scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->save($settings, $scopeIdentifier);
    }
    public function calculateChangeSet(array $settings, $scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'calculateChangeSet', array('settings' => $settings, 'scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->calculateChangeSet($settings, $scopeIdentifier);
    }
    public function reload($scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'reload', array('scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->reload($scopeIdentifier);
    }
    public function getSettingsByForm(\Symfony\Component\Form\FormInterface $form) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSettingsByForm', array('form' => $form), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSettingsByForm($form);
    }
    public function getSettingsDefaults($name, $full = false)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSettingsDefaults', array('name' => $name, 'full' => $full), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSettingsDefaults($name, $full);
    }
    public function getMergedWithParentValue($value, $name, $full = false, $scopeIdentifier = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMergedWithParentValue', array('value' => $value, 'name' => $name, 'full' => $full, 'scopeIdentifier' => $scopeIdentifier), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMergedWithParentValue($value, $name, $full, $scopeIdentifier);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->settings, $instance->managers, $instance->defaultManagers, $instance->scope, $instance->eventDispatcher);
        \Closure::bind(function (\Oro\Bundle\ConfigBundle\Config\ConfigManager $instance) {
            unset($instance->memoryCache);
        }, $instance, 'Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(string $scope, \Oro\Bundle\ConfigBundle\Config\ConfigDefinitionImmutableBag $configDefinition, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, \Oro\Bundle\CacheBundle\Provider\MemoryCache $memoryCache)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->settings, $this->managers, $this->defaultManagers, $this->scope, $this->eventDispatcher);
        \Closure::bind(function (\Oro\Bundle\ConfigBundle\Config\ConfigManager $instance) {
            unset($instance->memoryCache);
        }, $this, 'Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($scope, $configDefinition, $eventDispatcher, $memoryCache);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->settings, $this->managers, $this->defaultManagers, $this->scope, $this->eventDispatcher);
        \Closure::bind(function (\Oro\Bundle\ConfigBundle\Config\ConfigManager $instance) {
            unset($instance->memoryCache);
        }, $this, 'Oro\\Bundle\\ConfigBundle\\Config\\ConfigManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ConfigManager_0478e9a', false)) {
    \class_alias(__NAMESPACE__.'\\ConfigManager_0478e9a', 'ConfigManager_0478e9a', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Provider/ProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Provider/AbstractProvider.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ConfigBundle/Provider/SystemConfigurationFormProvider.php';
class SystemConfigurationFormProvider_ae7f894 extends \Oro\Bundle\ConfigBundle\Provider\SystemConfigurationFormProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getTree() : \Oro\Bundle\ConfigBundle\Config\Tree\GroupNodeDefinition
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getTree', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getTree();
    }
    public function getJsTree() : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJsTree', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJsTree();
    }
    public function getApiTree(?string $path = null) : ?\Oro\Bundle\ConfigBundle\Config\ApiTree\SectionDefinition
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getApiTree', array('path' => $path), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getApiTree($path);
    }
    public function getSubTree(string $subTreeName) : \Oro\Bundle\ConfigBundle\Config\Tree\GroupNodeDefinition
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSubTree', array('subTreeName' => $subTreeName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSubTree($subTreeName);
    }
    public function getDataTransformer(string $key) : ?\Oro\Bundle\ConfigBundle\Config\DataTransformerInterface
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDataTransformer', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDataTransformer($key);
    }
    public function getForm(string $groupName, \Oro\Bundle\ConfigBundle\Config\ConfigManager $configManager) : \Symfony\Component\Form\FormInterface
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getForm', array('groupName' => $groupName, 'configManager' => $configManager), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getForm($groupName, $configManager);
    }
    public function chooseActiveGroups(?string $activeGroup, ?string $activeSubGroup) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'chooseActiveGroups', array('activeGroup' => $activeGroup, 'activeSubGroup' => $activeSubGroup), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->chooseActiveGroups($activeGroup, $activeSubGroup);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->configBag, $instance->translator, $instance->formFactory, $instance->formRegistry, $instance->authorizationChecker, $instance->searchProvider, $instance->featureChecker, $instance->eventDispatcher, $instance->processedTrees, $instance->processedJsTrees, $instance->processedSubTrees);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\ConfigBundle\Config\ConfigBag $configBag, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Symfony\Component\Form\FormFactoryInterface $factory, \Symfony\Component\Form\FormRegistryInterface $formRegistry, \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker, \Oro\Bundle\ConfigBundle\Provider\ChainSearchProvider $searchProvider, \Oro\Bundle\FeatureToggleBundle\Checker\FeatureChecker $featureChecker, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Provider\\SystemConfigurationFormProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->configBag, $this->translator, $this->formFactory, $this->formRegistry, $this->authorizationChecker, $this->searchProvider, $this->featureChecker, $this->eventDispatcher, $this->processedTrees, $this->processedJsTrees, $this->processedSubTrees);
        }
        $this->valueHolder6655f->__construct($configBag, $translator, $factory, $formRegistry, $authorizationChecker, $searchProvider, $featureChecker, $eventDispatcher);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Provider\\SystemConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Provider\\SystemConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Provider\\SystemConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ConfigBundle\\Provider\\SystemConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->configBag, $this->translator, $this->formFactory, $this->formRegistry, $this->authorizationChecker, $this->searchProvider, $this->featureChecker, $this->eventDispatcher, $this->processedTrees, $this->processedJsTrees, $this->processedSubTrees);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('SystemConfigurationFormProvider_ae7f894', false)) {
    \class_alias(__NAMESPACE__.'\\SystemConfigurationFormProvider_ae7f894', 'SystemConfigurationFormProvider_ae7f894', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/CurrencyBundle/Provider/ViewTypeProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/CurrencyBundle/Provider/ViewTypeConfigProvider.php';
class ViewTypeConfigProvider_7122df5 extends \Oro\Bundle\CurrencyBundle\Provider\ViewTypeConfigProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getViewType()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getViewType', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getViewType();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->configManager, $instance->viewType);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\ConfigBundle\Config\ConfigManager $configManager)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\CurrencyBundle\\Provider\\ViewTypeConfigProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->configManager, $this->viewType);
        }
        $this->valueHolder6655f->__construct($configManager);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\CurrencyBundle\\Provider\\ViewTypeConfigProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\CurrencyBundle\\Provider\\ViewTypeConfigProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\CurrencyBundle\\Provider\\ViewTypeConfigProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\CurrencyBundle\\Provider\\ViewTypeConfigProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->configManager, $this->viewType);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ViewTypeConfigProvider_7122df5', false)) {
    \class_alias(__NAMESPACE__.'\\ViewTypeConfigProvider_7122df5', 'ViewTypeConfigProvider_7122df5', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DashboardBundle/Model/Factory.php';
class Factory_58a8560 extends \Oro\Bundle\DashboardBundle\Model\Factory implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function createDashboardModel(\Oro\Bundle\DashboardBundle\Entity\Dashboard $dashboard) : \Oro\Bundle\DashboardBundle\Model\DashboardModel
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createDashboardModel', array('dashboard' => $dashboard), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createDashboardModel($dashboard);
    }
    public function createWidgetModel(\Oro\Bundle\DashboardBundle\Entity\Widget $widget) : \Oro\Bundle\DashboardBundle\Model\WidgetModel
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createWidgetModel', array('widget' => $widget), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createWidgetModel($widget);
    }
    public function createVisibleWidgetModel(\Oro\Bundle\DashboardBundle\Entity\Widget $widget) : ?\Oro\Bundle\DashboardBundle\Model\WidgetModel
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createVisibleWidgetModel', array('widget' => $widget), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createVisibleWidgetModel($widget);
    }
    public function createWidgetCollection(\Oro\Bundle\DashboardBundle\Entity\Dashboard $dashboard) : \Oro\Bundle\DashboardBundle\Model\WidgetCollection
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createWidgetCollection', array('dashboard' => $dashboard), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createWidgetCollection($dashboard);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DashboardBundle\Model\Factory $instance) {
            unset($instance->configProvider, $instance->stateManager, $instance->widgetConfigs, $instance->dashboardTypeConfigProviders);
        }, $instance, 'Oro\\Bundle\\DashboardBundle\\Model\\Factory')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\DashboardBundle\Model\ConfigProvider $configProvider, \Oro\Bundle\DashboardBundle\Model\StateManager $stateManager, \Oro\Bundle\DashboardBundle\Model\WidgetConfigs $widgetConfigs, iterable $dashboardTypeConfigProviders)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\DashboardBundle\\Model\\Factory');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DashboardBundle\Model\Factory $instance) {
            unset($instance->configProvider, $instance->stateManager, $instance->widgetConfigs, $instance->dashboardTypeConfigProviders);
        }, $this, 'Oro\\Bundle\\DashboardBundle\\Model\\Factory')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($configProvider, $stateManager, $widgetConfigs, $dashboardTypeConfigProviders);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DashboardBundle\\Model\\Factory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DashboardBundle\\Model\\Factory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DashboardBundle\\Model\\Factory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DashboardBundle\\Model\\Factory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\DashboardBundle\Model\Factory $instance) {
            unset($instance->configProvider, $instance->stateManager, $instance->widgetConfigs, $instance->dashboardTypeConfigProviders);
        }, $this, 'Oro\\Bundle\\DashboardBundle\\Model\\Factory')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('Factory_58a8560', false)) {
    \class_alias(__NAMESPACE__.'\\Factory_58a8560', 'Factory_58a8560', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/CacheBundle/Provider/MemoryCacheProviderAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/CacheBundle/Provider/MemoryCacheProviderAwareTrait.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Datagrid/Builder.php';
class Builder_7487c02 extends \Oro\Bundle\DataGridBundle\Datagrid\Builder implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function build(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $config, \Oro\Bundle\DataGridBundle\Datagrid\ParameterBag $parameters, array $additionalParameters = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'build', array('config' => $config, 'parameters' => $parameters, 'additionalParameters' => $additionalParameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->build($config, $parameters, $additionalParameters);
    }
    public function setMemoryCacheProvider(?\Oro\Bundle\CacheBundle\Provider\MemoryCacheProviderInterface $memoryCacheProvider) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setMemoryCacheProvider', array('memoryCacheProvider' => $memoryCacheProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setMemoryCacheProvider($memoryCacheProvider);
return;
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Datagrid\Builder $instance) {
            unset($instance->baseDatagridClass, $instance->acceptorClass, $instance->eventDispatcher, $instance->dataSources, $instance->extensions, $instance->memoryCacheProvider);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(string $baseDatagridClass, string $acceptorClass, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, \Psr\Container\ContainerInterface $dataSources, iterable $extensions)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Datagrid\Builder $instance) {
            unset($instance->baseDatagridClass, $instance->acceptorClass, $instance->eventDispatcher, $instance->dataSources, $instance->extensions, $instance->memoryCacheProvider);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($baseDatagridClass, $acceptorClass, $eventDispatcher, $dataSources, $extensions);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Datagrid\Builder $instance) {
            unset($instance->baseDatagridClass, $instance->acceptorClass, $instance->eventDispatcher, $instance->dataSources, $instance->extensions, $instance->memoryCacheProvider);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Datagrid\\Builder')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('Builder_7487c02', false)) {
    \class_alias(__NAMESPACE__.'\\Builder_7487c02', 'Builder_7487c02', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Converter/DataConverterInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Context/ContextAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/ImportExport/DatagridDataConverter.php';
class DatagridDataConverter_1687c96 extends \Oro\Bundle\DataGridBundle\ImportExport\DatagridDataConverter implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function convertToExportFormat(array $exportedRecord, $skipNullValues = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'convertToExportFormat', array('exportedRecord' => $exportedRecord, 'skipNullValues' => $skipNullValues), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->convertToExportFormat($exportedRecord, $skipNullValues);
    }
    public function convertToImportFormat(array $importedRecord, $skipNullValues = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'convertToImportFormat', array('importedRecord' => $importedRecord, 'skipNullValues' => $skipNullValues), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->convertToImportFormat($importedRecord, $skipNullValues);
    }
    public function setImportExportContext(\Oro\Bundle\ImportExportBundle\Context\ContextInterface $context)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setImportExportContext', array('context' => $context), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setImportExportContext($context);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->translator, $instance->formatterProvider, $instance->context, $instance->formatters);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\ImportExport\DatagridDataConverter $instance) {
            unset($instance->datagridColumnsFromContextProvider, $instance->gridColumns);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\DataGridBundle\ImportExport\DatagridColumnsFromContextProviderInterface $datagridColumnsFromContextProvider, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Oro\Bundle\ImportExportBundle\Formatter\FormatterProvider $formatterProvider)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->translator, $this->formatterProvider, $this->context, $this->formatters);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\ImportExport\DatagridDataConverter $instance) {
            unset($instance->datagridColumnsFromContextProvider, $instance->gridColumns);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($datagridColumnsFromContextProvider, $translator, $formatterProvider);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->translator, $this->formatterProvider, $this->context, $this->formatters);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\ImportExport\DatagridDataConverter $instance) {
            unset($instance->datagridColumnsFromContextProvider, $instance->gridColumns);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridDataConverter')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('DatagridDataConverter_1687c96', false)) {
    \class_alias(__NAMESPACE__.'\\DatagridDataConverter_1687c96', 'DatagridDataConverter_1687c96', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/ImportExport/DatagridColumnsFromContextProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/ImportExport/DatagridColumnsFromContextProvider.php';
class DatagridColumnsFromContextProvider_ce17b4d extends \Oro\Bundle\DataGridBundle\ImportExport\DatagridColumnsFromContextProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getColumnsFromContext(\Oro\Bundle\ImportExportBundle\Context\ContextInterface $context) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getColumnsFromContext', array('context' => $context), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getColumnsFromContext($context);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\ImportExport\DatagridColumnsFromContextProvider $instance) {
            unset($instance->datagridManager, $instance->columnsStateProvider);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\DataGridBundle\Datagrid\Manager $datagridManager, \Oro\Bundle\DataGridBundle\Provider\State\DatagridStateProviderInterface $columnsStateProvider)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\ImportExport\DatagridColumnsFromContextProvider $instance) {
            unset($instance->datagridManager, $instance->columnsStateProvider);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($datagridManager, $columnsStateProvider);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\ImportExport\DatagridColumnsFromContextProvider $instance) {
            unset($instance->datagridManager, $instance->columnsStateProvider);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\ImportExport\\DatagridColumnsFromContextProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('DatagridColumnsFromContextProvider_ce17b4d', false)) {
    \class_alias(__NAMESPACE__.'\\DatagridColumnsFromContextProvider_ce17b4d', 'DatagridColumnsFromContextProvider_ce17b4d', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Provider/State/DatagridStateProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Provider/State/AbstractStateProvider.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Provider/State/ColumnsStateProvider.php';
class ColumnsStateProvider_621bf62 extends \Oro\Bundle\DataGridBundle\Provider\State\ColumnsStateProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getState(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration, \Oro\Bundle\DataGridBundle\Datagrid\ParameterBag $datagridParameters) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getState', array('datagridConfiguration' => $datagridConfiguration, 'datagridParameters' => $datagridParameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getState($datagridConfiguration, $datagridParameters);
    }
    public function getStateFromParameters(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration, \Oro\Bundle\DataGridBundle\Datagrid\ParameterBag $datagridParameters) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getStateFromParameters', array('datagridConfiguration' => $datagridConfiguration, 'datagridParameters' => $datagridParameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getStateFromParameters($datagridConfiguration, $datagridParameters);
    }
    public function getDefaultState(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDefaultState', array('datagridConfiguration' => $datagridConfiguration), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDefaultState($datagridConfiguration);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\ColumnsStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider')->__invoke($instance);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\DataGridBundle\Entity\Manager\GridViewManager $gridViewManager, \Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface $tokenAccessor, \Oro\Bundle\DataGridBundle\Tools\DatagridParametersHelper $datagridParametersHelper)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\ColumnsStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($gridViewManager, $tokenAccessor, $datagridParametersHelper);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\ColumnsStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\ColumnsStateProvider')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ColumnsStateProvider_621bf62', false)) {
    \class_alias(__NAMESPACE__.'\\ColumnsStateProvider_621bf62', 'ColumnsStateProvider_621bf62', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/DataGridBundle/Provider/State/SortersStateProvider.php';
class SortersStateProvider_7f73399 extends \Oro\Bundle\DataGridBundle\Provider\State\SortersStateProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getState(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration, \Oro\Bundle\DataGridBundle\Datagrid\ParameterBag $datagridParameters) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getState', array('datagridConfiguration' => $datagridConfiguration, 'datagridParameters' => $datagridParameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getState($datagridConfiguration, $datagridParameters);
    }
    public function getStateFromParameters(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration, \Oro\Bundle\DataGridBundle\Datagrid\ParameterBag $datagridParameters) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getStateFromParameters', array('datagridConfiguration' => $datagridConfiguration, 'datagridParameters' => $datagridParameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getStateFromParameters($datagridConfiguration, $datagridParameters);
    }
    public function getDefaultState(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDefaultState', array('datagridConfiguration' => $datagridConfiguration), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDefaultState($datagridConfiguration);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\SortersStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider')->__invoke($instance);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\DataGridBundle\Entity\Manager\GridViewManager $gridViewManager, \Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface $tokenAccessor, \Oro\Bundle\DataGridBundle\Tools\DatagridParametersHelper $datagridParametersHelper)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\SortersStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($gridViewManager, $tokenAccessor, $datagridParametersHelper);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\SortersStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\SortersStateProvider')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('SortersStateProvider_7f73399', false)) {
    \class_alias(__NAMESPACE__.'\\SortersStateProvider_7f73399', 'SortersStateProvider_7f73399', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Provider/EntityFieldProvider.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/crm-dotmailer/Provider/FieldProvider.php';
class FieldProvider_e371727 extends \Oro\Bundle\DotmailerBundle\Provider\FieldProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setEntityProvider(\Oro\Bundle\EntityBundle\Provider\EntityProvider $entityProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setEntityProvider', array('entityProvider' => $entityProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setEntityProvider($entityProvider);
    }
    public function setVirtualFieldProvider(\Oro\Bundle\EntityBundle\Provider\VirtualFieldProviderInterface $virtualFieldProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setVirtualFieldProvider', array('virtualFieldProvider' => $virtualFieldProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setVirtualFieldProvider($virtualFieldProvider);
    }
    public function setVirtualRelationProvider($virtualRelationProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setVirtualRelationProvider', array('virtualRelationProvider' => $virtualRelationProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setVirtualRelationProvider($virtualRelationProvider);
    }
    public function setExclusionProvider(\Oro\Bundle\EntityBundle\Provider\ExclusionProviderInterface $exclusionProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setExclusionProvider', array('exclusionProvider' => $exclusionProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setExclusionProvider($exclusionProvider);
    }
    public function enableCaching($enable = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'enableCaching', array('enable' => $enable), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->enableCaching($enable);
    }
    public function getRelations($entityName, $withEntityDetails = false, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRelations', array('entityName' => $entityName, 'withEntityDetails' => $withEntityDetails, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRelations($entityName, $withEntityDetails, $applyExclusions, $translate);
    }
    public function getEntityFields(string $entityName, int $options = 192) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntityFields', array('entityName' => $entityName, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntityFields($entityName, $options);
    }
    public function setLocale($locale)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLocale', array('locale' => $locale), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLocale($locale);
    }
    public function getLocale()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getLocale', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getLocale();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityProvider, $instance->virtualFieldProvider, $instance->virtualRelationProvider, $instance->exclusionProvider, $instance->entityConfigProvider, $instance->extendConfigProvider, $instance->entityClassResolver, $instance->fieldTypeHelper, $instance->translator, $instance->doctrine, $instance->hiddenFields, $instance->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $instance, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $extendConfigProvider, \Oro\Bundle\EntityBundle\ORM\EntityClassResolver $entityClassResolver, \Oro\Bundle\EntityExtendBundle\Extend\FieldTypeHelper $fieldTypeHelper, \Doctrine\Persistence\ManagerRegistry $doctrine, \Symfony\Contracts\Translation\TranslatorInterface $translator, $hiddenFields)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\DotmailerBundle\\Provider\\FieldProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityProvider, $this->virtualFieldProvider, $this->virtualRelationProvider, $this->exclusionProvider, $this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->fieldTypeHelper, $this->translator, $this->doctrine, $this->hiddenFields, $this->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($entityConfigProvider, $extendConfigProvider, $entityClassResolver, $fieldTypeHelper, $doctrine, $translator, $hiddenFields);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DotmailerBundle\\Provider\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DotmailerBundle\\Provider\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DotmailerBundle\\Provider\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\DotmailerBundle\\Provider\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityProvider, $this->virtualFieldProvider, $this->virtualRelationProvider, $this->exclusionProvider, $this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->fieldTypeHelper, $this->translator, $this->doctrine, $this->hiddenFields, $this->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('FieldProvider_e371727', false)) {
    \class_alias(__NAMESPACE__.'\\FieldProvider_e371727', 'FieldProvider_e371727', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Twig/Sandbox/TemplateRenderer.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Provider/EmailRenderer.php';
class EmailRenderer_bcb05e5 extends \Oro\Bundle\EmailBundle\Provider\EmailRenderer implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function compileMessage(\Oro\Bundle\EmailBundle\Model\EmailTemplateInterface $template, array $templateParams = []) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'compileMessage', array('template' => $template, 'templateParams' => $templateParams), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->compileMessage($template, $templateParams);
    }
    public function compilePreview(\Oro\Bundle\EmailBundle\Model\EmailTemplateInterface $template) : string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'compilePreview', array('template' => $template), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->compilePreview($template);
    }
    public function addExtension(\Twig\Extension\ExtensionInterface $extension) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'addExtension', array('extension' => $extension), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->addExtension($extension);
return;
    }
    public function addSystemVariableDefaultFilter(string $variable, string $filter) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'addSystemVariableDefaultFilter', array('variable' => $variable, 'filter' => $filter), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->addSystemVariableDefaultFilter($variable, $filter);
return;
    }
    public function renderTemplate(string $template, array $templateParams = []) : string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'renderTemplate', array('template' => $template, 'templateParams' => $templateParams), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->renderTemplate($template, $templateParams);
    }
    public function validateTemplate(string $template) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'validateTemplate', array('template' => $template), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->validateTemplate($template);
return;
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->environment);
        \Closure::bind(function (\Oro\Bundle\EmailBundle\Provider\EmailRenderer $instance) {
            unset($instance->translator, $instance->htmlTagHelper);
        }, $instance, 'Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer')->__invoke($instance);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Twig\Sandbox\TemplateRenderer $instance) {
            unset($instance->configProvider, $instance->systemVariableDefaultFilters, $instance->sandboxConfigured, $instance->entityVariableComputer, $instance->entityDataAccessor, $instance->inflector);
        }, $instance, 'Oro\\Bundle\\EntityBundle\\Twig\\Sandbox\\TemplateRenderer')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Twig\Environment $environment, \Oro\Bundle\EntityBundle\Twig\Sandbox\TemplateRendererConfigProviderInterface $configProvider, \Oro\Bundle\EntityBundle\Twig\Sandbox\VariableProcessorRegistry $variableProcessors, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Doctrine\Inflector\Inflector $inflector, \Oro\Bundle\UIBundle\Tools\HtmlTagHelper $htmlTagHelper)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->environment);
        \Closure::bind(function (\Oro\Bundle\EmailBundle\Provider\EmailRenderer $instance) {
            unset($instance->translator, $instance->htmlTagHelper);
        }, $this, 'Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Twig\Sandbox\TemplateRenderer $instance) {
            unset($instance->configProvider, $instance->systemVariableDefaultFilters, $instance->sandboxConfigured, $instance->entityVariableComputer, $instance->entityDataAccessor, $instance->inflector);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Twig\\Sandbox\\TemplateRenderer')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($environment, $configProvider, $variableProcessors, $translator, $inflector, $htmlTagHelper);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->environment);
        \Closure::bind(function (\Oro\Bundle\EmailBundle\Provider\EmailRenderer $instance) {
            unset($instance->translator, $instance->htmlTagHelper);
        }, $this, 'Oro\\Bundle\\EmailBundle\\Provider\\EmailRenderer')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Twig\Sandbox\TemplateRenderer $instance) {
            unset($instance->configProvider, $instance->systemVariableDefaultFilters, $instance->sandboxConfigured, $instance->entityVariableComputer, $instance->entityDataAccessor, $instance->inflector);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Twig\\Sandbox\\TemplateRenderer')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('EmailRenderer_bcb05e5', false)) {
    \class_alias(__NAMESPACE__.'\\EmailRenderer_bcb05e5', 'EmailRenderer_bcb05e5', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityBundle/Provider/EntityProvider.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EmailBundle/Provider/EmailTemplateEntityProvider.php';
class EmailTemplateEntityProvider_42eaac9 extends \Oro\Bundle\EmailBundle\Provider\EmailTemplateEntityProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setExclusionProvider(\Oro\Bundle\EntityBundle\Provider\ExclusionProviderInterface $exclusionProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setExclusionProvider', array('exclusionProvider' => $exclusionProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setExclusionProvider($exclusionProvider);
    }
    public function getEntities($sortByPluralLabel = true, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntities', array('sortByPluralLabel' => $sortByPluralLabel, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntities($sortByPluralLabel, $applyExclusions, $translate);
    }
    public function getEntity($entityName, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntity', array('entityName' => $entityName, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntity($entityName, $translate);
    }
    public function getEnabledEntity($entityName, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEnabledEntity', array('entityName' => $entityName, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEnabledEntity($entityName, $applyExclusions, $translate);
    }
    public function isIgnoredEntity($className)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isIgnoredEntity', array('className' => $className), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isIgnoredEntity($className);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityConfigProvider, $instance->extendConfigProvider, $instance->entityClassResolver, $instance->translator, $instance->exclusionProvider, $instance->featureChecker);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $extendConfigProvider, \Oro\Bundle\EntityBundle\ORM\EntityClassResolver $entityClassResolver, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Oro\Bundle\FeatureToggleBundle\Checker\FeatureChecker $featureChecker)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailTemplateEntityProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->translator, $this->exclusionProvider, $this->featureChecker);
        }
        $this->valueHolder6655f->__construct($entityConfigProvider, $extendConfigProvider, $entityClassResolver, $translator, $featureChecker);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailTemplateEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailTemplateEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailTemplateEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EmailBundle\\Provider\\EmailTemplateEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->translator, $this->exclusionProvider, $this->featureChecker);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('EmailTemplateEntityProvider_42eaac9', false)) {
    \class_alias(__NAMESPACE__.'\\EmailTemplateEntityProvider_42eaac9', 'EmailTemplateEntityProvider_42eaac9', false);
}

class EntityFieldProvider_5de77fa extends \Oro\Bundle\EntityBundle\Provider\EntityFieldProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setEntityProvider(\Oro\Bundle\EntityBundle\Provider\EntityProvider $entityProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setEntityProvider', array('entityProvider' => $entityProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setEntityProvider($entityProvider);
    }
    public function setVirtualFieldProvider(\Oro\Bundle\EntityBundle\Provider\VirtualFieldProviderInterface $virtualFieldProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setVirtualFieldProvider', array('virtualFieldProvider' => $virtualFieldProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setVirtualFieldProvider($virtualFieldProvider);
    }
    public function setVirtualRelationProvider($virtualRelationProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setVirtualRelationProvider', array('virtualRelationProvider' => $virtualRelationProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setVirtualRelationProvider($virtualRelationProvider);
    }
    public function setExclusionProvider(\Oro\Bundle\EntityBundle\Provider\ExclusionProviderInterface $exclusionProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setExclusionProvider', array('exclusionProvider' => $exclusionProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setExclusionProvider($exclusionProvider);
    }
    public function enableCaching($enable = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'enableCaching', array('enable' => $enable), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->enableCaching($enable);
    }
    public function getRelations($entityName, $withEntityDetails = false, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRelations', array('entityName' => $entityName, 'withEntityDetails' => $withEntityDetails, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRelations($entityName, $withEntityDetails, $applyExclusions, $translate);
    }
    public function getEntityFields(string $entityName, int $options = 192) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntityFields', array('entityName' => $entityName, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntityFields($entityName, $options);
    }
    public function setLocale($locale)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLocale', array('locale' => $locale), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLocale($locale);
    }
    public function getLocale()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getLocale', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getLocale();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityProvider, $instance->virtualFieldProvider, $instance->virtualRelationProvider, $instance->exclusionProvider, $instance->entityConfigProvider, $instance->extendConfigProvider, $instance->entityClassResolver, $instance->fieldTypeHelper, $instance->translator, $instance->doctrine, $instance->hiddenFields, $instance->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $instance, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $extendConfigProvider, \Oro\Bundle\EntityBundle\ORM\EntityClassResolver $entityClassResolver, \Oro\Bundle\EntityExtendBundle\Extend\FieldTypeHelper $fieldTypeHelper, \Doctrine\Persistence\ManagerRegistry $doctrine, \Symfony\Contracts\Translation\TranslatorInterface $translator, $hiddenFields)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityProvider, $this->virtualFieldProvider, $this->virtualRelationProvider, $this->exclusionProvider, $this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->fieldTypeHelper, $this->translator, $this->doctrine, $this->hiddenFields, $this->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($entityConfigProvider, $extendConfigProvider, $entityClassResolver, $fieldTypeHelper, $doctrine, $translator, $hiddenFields);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityProvider, $this->virtualFieldProvider, $this->virtualRelationProvider, $this->exclusionProvider, $this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->fieldTypeHelper, $this->translator, $this->doctrine, $this->hiddenFields, $this->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('EntityFieldProvider_5de77fa', false)) {
    \class_alias(__NAMESPACE__.'\\EntityFieldProvider_5de77fa', 'EntityFieldProvider_5de77fa', false);
}

class EntityProvider_09b1ff0 extends \Oro\Bundle\EntityBundle\Provider\EntityProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setExclusionProvider(\Oro\Bundle\EntityBundle\Provider\ExclusionProviderInterface $exclusionProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setExclusionProvider', array('exclusionProvider' => $exclusionProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setExclusionProvider($exclusionProvider);
    }
    public function getEntities($sortByPluralLabel = true, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntities', array('sortByPluralLabel' => $sortByPluralLabel, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntities($sortByPluralLabel, $applyExclusions, $translate);
    }
    public function getEntity($entityName, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntity', array('entityName' => $entityName, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntity($entityName, $translate);
    }
    public function getEnabledEntity($entityName, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEnabledEntity', array('entityName' => $entityName, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEnabledEntity($entityName, $applyExclusions, $translate);
    }
    public function isIgnoredEntity($className)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isIgnoredEntity', array('className' => $className), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isIgnoredEntity($className);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityConfigProvider, $instance->extendConfigProvider, $instance->entityClassResolver, $instance->translator, $instance->exclusionProvider, $instance->featureChecker);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $extendConfigProvider, \Oro\Bundle\EntityBundle\ORM\EntityClassResolver $entityClassResolver, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Oro\Bundle\FeatureToggleBundle\Checker\FeatureChecker $featureChecker)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->translator, $this->exclusionProvider, $this->featureChecker);
        }
        $this->valueHolder6655f->__construct($entityConfigProvider, $extendConfigProvider, $entityClassResolver, $translator, $featureChecker);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityBundle\\Provider\\EntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->translator, $this->exclusionProvider, $this->featureChecker);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('EntityProvider_09b1ff0', false)) {
    \class_alias(__NAMESPACE__.'\\EntityProvider_09b1ff0', 'EntityProvider_09b1ff0', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Component/MessageQueue/Consumption/MessageProcessorInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Component/MessageQueue/Client/TopicSubscriberInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Async/Import/ImportMessageProcessor.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Async/AttributeImportMessageProcessor.php';
class AttributeImportMessageProcessor_39d89d1 extends \Oro\Bundle\EntityConfigBundle\Async\AttributeImportMessageProcessor implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setMessageProducer(\Oro\Component\MessageQueue\Client\MessageProducerInterface $producer) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setMessageProducer', array('producer' => $producer), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setMessageProducer($producer);
return;
    }
    public function process(\Oro\Component\MessageQueue\Transport\MessageInterface $message, \Oro\Component\MessageQueue\Transport\SessionInterface $session)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'process', array('message' => $message, 'session' => $session), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->process($message, $session);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->producer);
        \Closure::bind(function (\Oro\Bundle\ImportExportBundle\Async\Import\ImportMessageProcessor $instance) {
            unset($instance->importHandler, $instance->jobRunner, $instance->logger, $instance->importExportResultSummarizer, $instance->fileManager, $instance->postponedRowsHandler);
        }, $instance, 'Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Component\MessageQueue\Job\JobRunner $jobRunner, \Oro\Bundle\ImportExportBundle\Async\ImportExportResultSummarizer $importExportResultSummarizer, \Psr\Log\LoggerInterface $logger, \Oro\Bundle\ImportExportBundle\File\FileManager $fileManager, \Oro\Bundle\ImportExportBundle\Handler\ImportHandler $importHandler, \Oro\Bundle\ImportExportBundle\Handler\PostponedRowsHandler $postponedRowsHandler)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Async\\AttributeImportMessageProcessor');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->producer);
        \Closure::bind(function (\Oro\Bundle\ImportExportBundle\Async\Import\ImportMessageProcessor $instance) {
            unset($instance->importHandler, $instance->jobRunner, $instance->logger, $instance->importExportResultSummarizer, $instance->fileManager, $instance->postponedRowsHandler);
        }, $this, 'Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($jobRunner, $importExportResultSummarizer, $logger, $fileManager, $importHandler, $postponedRowsHandler);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Async\\AttributeImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Async\\AttributeImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Async\\AttributeImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Async\\AttributeImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->producer);
        \Closure::bind(function (\Oro\Bundle\ImportExportBundle\Async\Import\ImportMessageProcessor $instance) {
            unset($instance->importHandler, $instance->jobRunner, $instance->logger, $instance->importExportResultSummarizer, $instance->fileManager, $instance->postponedRowsHandler);
        }, $this, 'Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('AttributeImportMessageProcessor_39d89d1', false)) {
    \class_alias(__NAMESPACE__.'\\AttributeImportMessageProcessor_39d89d1', 'AttributeImportMessageProcessor_39d89d1', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/EventListener/AttributesImportFinishNotificationListener.php';
class AttributesImportFinishNotificationListener_156a7b2 extends \Oro\Bundle\EntityConfigBundle\EventListener\AttributesImportFinishNotificationListener implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function onAfterAttributesImport(\Oro\Bundle\ImportExportBundle\Event\AfterJobExecutionEvent $event)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'onAfterAttributesImport', array('event' => $event), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->onAfterAttributesImport($event);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\EventListener\AttributesImportFinishNotificationListener $instance) {
            unset($instance->topicSender);
        }, $instance, 'Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\WebSocket\AttributesImportTopicSender $topicSender)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\EventListener\AttributesImportFinishNotificationListener $instance) {
            unset($instance->topicSender);
        }, $this, 'Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($topicSender);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\EventListener\AttributesImportFinishNotificationListener $instance) {
            unset($instance->topicSender);
        }, $this, 'Oro\\Bundle\\EntityConfigBundle\\EventListener\\AttributesImportFinishNotificationListener')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('AttributesImportFinishNotificationListener_156a7b2', false)) {
    \class_alias(__NAMESPACE__.'\\AttributesImportFinishNotificationListener_156a7b2', 'AttributesImportFinishNotificationListener_156a7b2', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/Provider/PropertyConfigBag.php';
class PropertyConfigBag_3139417 extends \Oro\Bundle\EntityConfigBundle\Provider\PropertyConfigBag implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getPropertyConfig($scope)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getPropertyConfig', array('scope' => $scope), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getPropertyConfig($scope);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\Provider\PropertyConfigBag $instance) {
            unset($instance->config, $instance->configObjects);
        }, $instance, 'Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(array $config)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\Provider\PropertyConfigBag $instance) {
            unset($instance->config, $instance->configObjects);
        }, $this, 'Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($config);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\Provider\PropertyConfigBag $instance) {
            unset($instance->config, $instance->configObjects);
        }, $this, 'Oro\\Bundle\\EntityConfigBundle\\Provider\\PropertyConfigBag')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('PropertyConfigBag_3139417', false)) {
    \class_alias(__NAMESPACE__.'\\PropertyConfigBag_3139417', 'PropertyConfigBag_3139417', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityConfigBundle/WebSocket/AttributesImportTopicSender.php';
class AttributesImportTopicSender_06deca0 extends \Oro\Bundle\EntityConfigBundle\WebSocket\AttributesImportTopicSender implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getTopic($configModelId)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getTopic', array('configModelId' => $configModelId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getTopic($configModelId);
    }
    public function send($configModel)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'send', array('configModel' => $configModel), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->send($configModel);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->connectionChecker);
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\WebSocket\AttributesImportTopicSender $instance) {
            unset($instance->websocketClient, $instance->tokenAccessor);
        }, $instance, 'Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\SyncBundle\Client\WebsocketClientInterface $websocketClient, \Oro\Bundle\SyncBundle\Client\ConnectionChecker $connectionChecker, \Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface $tokenAccessor)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->connectionChecker);
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\WebSocket\AttributesImportTopicSender $instance) {
            unset($instance->websocketClient, $instance->tokenAccessor);
        }, $this, 'Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($websocketClient, $connectionChecker, $tokenAccessor);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->connectionChecker);
        \Closure::bind(function (\Oro\Bundle\EntityConfigBundle\WebSocket\AttributesImportTopicSender $instance) {
            unset($instance->websocketClient, $instance->tokenAccessor);
        }, $this, 'Oro\\Bundle\\EntityConfigBundle\\WebSocket\\AttributesImportTopicSender')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('AttributesImportTopicSender_06deca0', false)) {
    \class_alias(__NAMESPACE__.'\\AttributesImportTopicSender_06deca0', 'AttributesImportTopicSender_06deca0', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityExtendBundle/Entity/Manager/AssociationManager.php';
class AssociationManager_e837bf4 extends \Oro\Bundle\EntityExtendBundle\Entity\Manager\AssociationManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getAssociationTargets($associationOwnerClass, $filter, $associationType, $associationKind = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAssociationTargets', array('associationOwnerClass' => $associationOwnerClass, 'filter' => $filter, 'associationType' => $associationType, 'associationKind' => $associationKind), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAssociationTargets($associationOwnerClass, $filter, $associationType, $associationKind);
    }
    public function getSingleOwnerFilter($scope, $attribute = 'enabled')
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSingleOwnerFilter', array('scope' => $scope, 'attribute' => $attribute), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSingleOwnerFilter($scope, $attribute);
    }
    public function getMultiOwnerFilter($scope, $attribute)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMultiOwnerFilter', array('scope' => $scope, 'attribute' => $attribute), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMultiOwnerFilter($scope, $attribute);
    }
    public function getMultiAssociationsQueryBuilder($associationOwnerClass, $filters, $joins, $associationTargets, $limit = null, $page = null, $orderBy = null, $callback = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMultiAssociationsQueryBuilder', array('associationOwnerClass' => $associationOwnerClass, 'filters' => $filters, 'joins' => $joins, 'associationTargets' => $associationTargets, 'limit' => $limit, 'page' => $page, 'orderBy' => $orderBy, 'callback' => $callback), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMultiAssociationsQueryBuilder($associationOwnerClass, $filters, $joins, $associationTargets, $limit, $page, $orderBy, $callback);
    }
    public function getAssociationSubQueryBuilder($associationOwnerClass, $targetEntityClass, $targetFieldName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAssociationSubQueryBuilder', array('associationOwnerClass' => $associationOwnerClass, 'targetEntityClass' => $targetEntityClass, 'targetFieldName' => $targetFieldName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAssociationSubQueryBuilder($associationOwnerClass, $targetEntityClass, $targetFieldName);
    }
    public function getMultiAssociationOwnersQueryBuilder($associationTargetClass, $filters, $joins, $associationOwners, $limit = null, $page = null, $orderBy = null, $callback = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMultiAssociationOwnersQueryBuilder', array('associationTargetClass' => $associationTargetClass, 'filters' => $filters, 'joins' => $joins, 'associationOwners' => $associationOwners, 'limit' => $limit, 'page' => $page, 'orderBy' => $orderBy, 'callback' => $callback), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMultiAssociationOwnersQueryBuilder($associationTargetClass, $filters, $joins, $associationOwners, $limit, $page, $orderBy, $callback);
    }
    public function getAssociationOwnersSubQueryBuilder($associationOwnerClass, $ownerFieldName, $targetIdFieldName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAssociationOwnersSubQueryBuilder', array('associationOwnerClass' => $associationOwnerClass, 'ownerFieldName' => $ownerFieldName, 'targetIdFieldName' => $targetIdFieldName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAssociationOwnersSubQueryBuilder($associationOwnerClass, $ownerFieldName, $targetIdFieldName);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->configManager, $instance->aclHelperLink, $instance->doctrineHelper, $instance->entityNameResolver, $instance->featureChecker);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\Config\ConfigManager $configManager, \Oro\Component\DependencyInjection\ServiceLink $aclHelperLink, \Oro\Bundle\EntityBundle\ORM\DoctrineHelper $doctrineHelper, \Oro\Bundle\EntityBundle\Provider\EntityNameResolver $entityNameResolver, \Oro\Bundle\FeatureToggleBundle\Checker\FeatureChecker $featureChecker)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Entity\\Manager\\AssociationManager');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->configManager, $this->aclHelperLink, $this->doctrineHelper, $this->entityNameResolver, $this->featureChecker);
        }
        $this->valueHolder6655f->__construct($configManager, $aclHelperLink, $doctrineHelper, $entityNameResolver, $featureChecker);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Entity\\Manager\\AssociationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Entity\\Manager\\AssociationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Entity\\Manager\\AssociationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Entity\\Manager\\AssociationManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->configManager, $this->aclHelperLink, $this->doctrineHelper, $this->entityNameResolver, $this->featureChecker);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('AssociationManager_e837bf4', false)) {
    \class_alias(__NAMESPACE__.'\\AssociationManager_e837bf4', 'AssociationManager_e837bf4', false);
}

include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/Mapping/Driver/MappingDriver.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/persistence/src/Persistence/Mapping/Driver/FileDriver.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/orm/lib/Doctrine/ORM/Mapping/Driver/YamlDriver.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/EntityExtendBundle/Tools/YamlDriver.php';
class YamlDriver_f0ca3ae extends \Oro\Bundle\EntityExtendBundle\Tools\YamlDriver implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function loadMetadataForClass($className, \Doctrine\Persistence\Mapping\ClassMetadata $metadata)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'loadMetadataForClass', array('className' => $className, 'metadata' => $metadata), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->loadMetadataForClass($className, $metadata);
    }
    public function setGlobalBasename($file)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setGlobalBasename', array('file' => $file), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setGlobalBasename($file);
    }
    public function getGlobalBasename()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getGlobalBasename', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getGlobalBasename();
    }
    public function getElement($className)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getElement', array('className' => $className), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getElement($className);
    }
    public function isTransient($className)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isTransient', array('className' => $className), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isTransient($className);
    }
    public function getAllClassNames()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAllClassNames', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAllClassNames();
    }
    public function getLocator()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getLocator', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getLocator();
    }
    public function setLocator(\Doctrine\Persistence\Mapping\Driver\FileLocator $locator)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLocator', array('locator' => $locator), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLocator($locator);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->locator, $instance->classCache, $instance->globalBasename);
        \Closure::bind(function (\Oro\Bundle\EntityExtendBundle\Tools\YamlDriver $instance) {
            unset($instance->schemas, $instance->configManager);
        }, $instance, 'Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(private \Oro\Bundle\EntityConfigBundle\Config\ConfigManager $configManager)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->locator, $this->classCache, $this->globalBasename);
        \Closure::bind(function (\Oro\Bundle\EntityExtendBundle\Tools\YamlDriver $instance) {
            unset($instance->schemas, $instance->configManager);
        }, $this, 'Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($configManager);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->locator, $this->classCache, $this->globalBasename);
        \Closure::bind(function (\Oro\Bundle\EntityExtendBundle\Tools\YamlDriver $instance) {
            unset($instance->schemas, $instance->configManager);
        }, $this, 'Oro\\Bundle\\EntityExtendBundle\\Tools\\YamlDriver')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('YamlDriver_f0ca3ae', false)) {
    \class_alias(__NAMESPACE__.'\\YamlDriver_f0ca3ae', 'YamlDriver_f0ca3ae', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/FilterBundle/Provider/State/FiltersStateProvider.php';
class FiltersStateProvider_1bdb91e extends \Oro\Bundle\FilterBundle\Provider\State\FiltersStateProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getState(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration, \Oro\Bundle\DataGridBundle\Datagrid\ParameterBag $datagridParameters) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getState', array('datagridConfiguration' => $datagridConfiguration, 'datagridParameters' => $datagridParameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getState($datagridConfiguration, $datagridParameters);
    }
    public function getStateFromParameters(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration, \Oro\Bundle\DataGridBundle\Datagrid\ParameterBag $datagridParameters) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getStateFromParameters', array('datagridConfiguration' => $datagridConfiguration, 'datagridParameters' => $datagridParameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getStateFromParameters($datagridConfiguration, $datagridParameters);
    }
    public function getDefaultState(\Oro\Bundle\DataGridBundle\Datagrid\Common\DatagridConfiguration $datagridConfiguration) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDefaultState', array('datagridConfiguration' => $datagridConfiguration), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDefaultState($datagridConfiguration);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\FilterBundle\Provider\State\FiltersStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $instance, 'Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider')->__invoke($instance);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $instance, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\DataGridBundle\Entity\Manager\GridViewManager $gridViewManager, \Oro\Bundle\SecurityBundle\Authentication\TokenAccessorInterface $tokenAccessor, \Oro\Bundle\DataGridBundle\Tools\DatagridParametersHelper $datagridParametersHelper)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\FilterBundle\Provider\State\FiltersStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $this, 'Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($gridViewManager, $tokenAccessor, $datagridParametersHelper);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\FilterBundle\Provider\State\FiltersStateProvider $instance) {
            unset($instance->datagridParametersHelper);
        }, $this, 'Oro\\Bundle\\FilterBundle\\Provider\\State\\FiltersStateProvider')->__invoke($this);
        \Closure::bind(function (\Oro\Bundle\DataGridBundle\Provider\State\AbstractStateProvider $instance) {
            unset($instance->tokenAccessor, $instance->gridViewManager, $instance->defaultGridView);
        }, $this, 'Oro\\Bundle\\DataGridBundle\\Provider\\State\\AbstractStateProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('FiltersStateProvider_1bdb91e', false)) {
    \class_alias(__NAMESPACE__.'\\FiltersStateProvider_1bdb91e', 'FiltersStateProvider_1bdb91e', false);
}

class ImportMessageProcessor_8dd07e7 extends \Oro\Bundle\ImportExportBundle\Async\Import\ImportMessageProcessor implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function process(\Oro\Component\MessageQueue\Transport\MessageInterface $message, \Oro\Component\MessageQueue\Transport\SessionInterface $session)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'process', array('message' => $message, 'session' => $session), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->process($message, $session);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\ImportExportBundle\Async\Import\ImportMessageProcessor $instance) {
            unset($instance->importHandler, $instance->jobRunner, $instance->logger, $instance->importExportResultSummarizer, $instance->fileManager, $instance->postponedRowsHandler);
        }, $instance, 'Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Component\MessageQueue\Job\JobRunner $jobRunner, \Oro\Bundle\ImportExportBundle\Async\ImportExportResultSummarizer $importExportResultSummarizer, \Psr\Log\LoggerInterface $logger, \Oro\Bundle\ImportExportBundle\File\FileManager $fileManager, \Oro\Bundle\ImportExportBundle\Handler\ImportHandler $importHandler, \Oro\Bundle\ImportExportBundle\Handler\PostponedRowsHandler $postponedRowsHandler)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\ImportExportBundle\Async\Import\ImportMessageProcessor $instance) {
            unset($instance->importHandler, $instance->jobRunner, $instance->logger, $instance->importExportResultSummarizer, $instance->fileManager, $instance->postponedRowsHandler);
        }, $this, 'Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($jobRunner, $importExportResultSummarizer, $logger, $fileManager, $importHandler, $postponedRowsHandler);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\ImportExportBundle\Async\Import\ImportMessageProcessor $instance) {
            unset($instance->importHandler, $instance->jobRunner, $instance->logger, $instance->importExportResultSummarizer, $instance->fileManager, $instance->postponedRowsHandler);
        }, $this, 'Oro\\Bundle\\ImportExportBundle\\Async\\Import\\ImportMessageProcessor')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ImportMessageProcessor_8dd07e7', false)) {
    \class_alias(__NAMESPACE__.'\\ImportMessageProcessor_8dd07e7', 'ImportMessageProcessor_8dd07e7', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Handler/AbstractHandler.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Handler/ExportHandler.php';
class ExportHandler_63577cf extends \Oro\Bundle\ImportExportBundle\Handler\ExportHandler implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getExportResult($jobName, $processorAlias, $processorType = 'export', $outputFormat = 'csv', $outputFilePrefix = null, array $options = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getExportResult', array('jobName' => $jobName, 'processorAlias' => $processorAlias, 'processorType' => $processorType, 'outputFormat' => $outputFormat, 'outputFilePrefix' => $outputFilePrefix, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getExportResult($jobName, $processorAlias, $processorType, $outputFormat, $outputFilePrefix, $options);
    }
    public function getExportingEntityIds($jobName, $processorType, $processorAlias, $options)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getExportingEntityIds', array('jobName' => $jobName, 'processorType' => $processorType, 'processorAlias' => $processorAlias, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getExportingEntityIds($jobName, $processorType, $processorAlias, $options);
    }
    public function exportResultFileMerge($jobName, $processorType, $outputFormat, array $files)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'exportResultFileMerge', array('jobName' => $jobName, 'processorType' => $processorType, 'outputFormat' => $outputFormat, 'files' => $files), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->exportResultFileMerge($jobName, $processorType, $outputFormat, $files);
    }
    public function handleExport($jobName, $processorAlias, $exportType = 'export', $outputFormat = 'csv', $outputFilePrefix = null, array $options = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'handleExport', array('jobName' => $jobName, 'processorAlias' => $processorAlias, 'exportType' => $exportType, 'outputFormat' => $outputFormat, 'outputFilePrefix' => $outputFilePrefix, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->handleExport($jobName, $processorAlias, $exportType, $outputFormat, $outputFilePrefix, $options);
    }
    public function handleDownloadExportResult($fileName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'handleDownloadExportResult', array('fileName' => $fileName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->handleDownloadExportResult($fileName);
    }
    public function getEntityName($processorType, $processorAlias) : ?string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntityName', array('processorType' => $processorType, 'processorAlias' => $processorAlias), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntityName($processorType, $processorAlias);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->jobExecutor, $instance->processorRegistry, $instance->entityConfigProvider, $instance->translator, $instance->writerChain, $instance->readerChain, $instance->batchFileManager, $instance->fileManager);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\ImportExportBundle\Job\JobExecutor $jobExecutor, \Oro\Bundle\ImportExportBundle\Processor\ProcessorRegistry $processorRegistry, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Oro\Bundle\ImportExportBundle\Writer\WriterChain $writerChain, \Oro\Bundle\ImportExportBundle\Reader\ReaderChain $readerChain, \Oro\Bundle\ImportExportBundle\File\BatchFileManager $batchFileManager, \Oro\Bundle\ImportExportBundle\File\FileManager $fileManager)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ExportHandler');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->jobExecutor, $this->processorRegistry, $this->entityConfigProvider, $this->translator, $this->writerChain, $this->readerChain, $this->batchFileManager, $this->fileManager);
        }
        $this->valueHolder6655f->__construct($jobExecutor, $processorRegistry, $entityConfigProvider, $translator, $writerChain, $readerChain, $batchFileManager, $fileManager);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ExportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ExportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ExportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ExportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->jobExecutor, $this->processorRegistry, $this->entityConfigProvider, $this->translator, $this->writerChain, $this->readerChain, $this->batchFileManager, $this->fileManager);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ExportHandler_63577cf', false)) {
    \class_alias(__NAMESPACE__.'\\ExportHandler_63577cf', 'ExportHandler_63577cf', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Handler/ImportHandler.php';
class ImportHandler_eb17db4 extends \Oro\Bundle\ImportExportBundle\Handler\ImportHandler implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function handle(string $process, string $jobName, string $processorAlias, array $options = []) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'handle', array('process' => $process, 'jobName' => $jobName, 'processorAlias' => $processorAlias, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->handle($process, $jobName, $processorAlias, $options);
    }
    public function splitImportFile(string $jobName, string $processorType, \Oro\Bundle\ImportExportBundle\Writer\FileStreamWriter $writer) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'splitImportFile', array('jobName' => $jobName, 'processorType' => $processorType, 'writer' => $writer), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->splitImportFile($jobName, $processorType, $writer);
    }
    public function handleImportValidation(string $jobName, string $processorAlias, array $options = []) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'handleImportValidation', array('jobName' => $jobName, 'processorAlias' => $processorAlias, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->handleImportValidation($jobName, $processorAlias, $options);
    }
    public function handleImport(string $jobName, string $processorAlias, array $options = []) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'handleImport', array('jobName' => $jobName, 'processorAlias' => $processorAlias, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->handleImport($jobName, $processorAlias, $options);
    }
    public function setImportingFileName($fileName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setImportingFileName', array('fileName' => $fileName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setImportingFileName($fileName);
    }
    public function setConfigurationOptions(array $options)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setConfigurationOptions', array('options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setConfigurationOptions($options);
    }
    public function getEntityName($processorType, $processorAlias) : ?string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntityName', array('processorType' => $processorType, 'processorAlias' => $processorAlias), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntityName($processorType, $processorAlias);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->configurationOptions, $instance->importingFileName, $instance->jobExecutor, $instance->processorRegistry, $instance->entityConfigProvider, $instance->translator, $instance->writerChain, $instance->readerChain, $instance->batchFileManager, $instance->fileManager);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\ImportExportBundle\Job\JobExecutor $jobExecutor, \Oro\Bundle\ImportExportBundle\Processor\ProcessorRegistry $processorRegistry, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Oro\Bundle\ImportExportBundle\Writer\WriterChain $writerChain, \Oro\Bundle\ImportExportBundle\Reader\ReaderChain $readerChain, \Oro\Bundle\ImportExportBundle\File\BatchFileManager $batchFileManager, \Oro\Bundle\ImportExportBundle\File\FileManager $fileManager)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ImportHandler');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->configurationOptions, $this->importingFileName, $this->jobExecutor, $this->processorRegistry, $this->entityConfigProvider, $this->translator, $this->writerChain, $this->readerChain, $this->batchFileManager, $this->fileManager);
        }
        $this->valueHolder6655f->__construct($jobExecutor, $processorRegistry, $entityConfigProvider, $translator, $writerChain, $readerChain, $batchFileManager, $fileManager);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ImportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ImportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ImportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Handler\\ImportHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->configurationOptions, $this->importingFileName, $this->jobExecutor, $this->processorRegistry, $this->entityConfigProvider, $this->translator, $this->writerChain, $this->readerChain, $this->batchFileManager, $this->fileManager);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ImportHandler_eb17db4', false)) {
    \class_alias(__NAMESPACE__.'\\ImportHandler_eb17db4', 'ImportHandler_eb17db4', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/ImportExportBundle/Job/JobExecutor.php';
class JobExecutor_2e691fd extends \Oro\Bundle\ImportExportBundle\Job\JobExecutor implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setEventDispatcher($eventDispatcher)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setEventDispatcher', array('eventDispatcher' => $eventDispatcher), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setEventDispatcher($eventDispatcher);
    }
    public function executeJob($jobType, $jobName, array $configuration = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'executeJob', array('jobType' => $jobType, 'jobName' => $jobName, 'configuration' => $configuration), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->executeJob($jobType, $jobName, $configuration);
    }
    public function getJobErrors($jobCode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJobErrors', array('jobCode' => $jobCode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJobErrors($jobCode);
    }
    public function getJobFailureExceptions($jobCode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJobFailureExceptions', array('jobCode' => $jobCode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJobFailureExceptions($jobCode);
    }
    public function getJob($jobType, $jobName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJob', array('jobType' => $jobType, 'jobName' => $jobName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJob($jobType, $jobName);
    }
    public function setValidationMode($validationMode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setValidationMode', array('validationMode' => $validationMode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setValidationMode($validationMode);
    }
    public function isValidationMode()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isValidationMode', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isValidationMode();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityManager, $instance->batchJobRegistry, $instance->contextRegistry, $instance->managerRegistry, $instance->batchJobRepository, $instance->eventDispatcher, $instance->validationMode, $instance->contextAggregatorRegistry);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\BatchBundle\Connector\ConnectorRegistry $jobRegistry, \Oro\Bundle\BatchBundle\Job\DoctrineJobRepository $batchJobRepository, \Oro\Bundle\ImportExportBundle\Context\ContextRegistry $contextRegistry, \Doctrine\Persistence\ManagerRegistry $managerRegistry, \Oro\Bundle\ImportExportBundle\Job\Context\ContextAggregatorRegistry $contextAggregatorRegistry)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Job\\JobExecutor');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityManager, $this->batchJobRegistry, $this->contextRegistry, $this->managerRegistry, $this->batchJobRepository, $this->eventDispatcher, $this->validationMode, $this->contextAggregatorRegistry);
        }
        $this->valueHolder6655f->__construct($jobRegistry, $batchJobRepository, $contextRegistry, $managerRegistry, $contextAggregatorRegistry);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Job\\JobExecutor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Job\\JobExecutor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Job\\JobExecutor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ImportExportBundle\\Job\\JobExecutor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityManager, $this->batchJobRegistry, $this->contextRegistry, $this->managerRegistry, $this->batchJobRepository, $this->eventDispatcher, $this->validationMode, $this->contextAggregatorRegistry);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('JobExecutor_2e691fd', false)) {
    \class_alias(__NAMESPACE__.'\\JobExecutor_2e691fd', 'JobExecutor_2e691fd', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/ImportExport/Job/Executor.php';
class Executor_618db2e extends \Oro\Bundle\IntegrationBundle\ImportExport\Job\Executor implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setEventDispatcher($eventDispatcher)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setEventDispatcher', array('eventDispatcher' => $eventDispatcher), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setEventDispatcher($eventDispatcher);
    }
    public function executeJob($jobType, $jobName, array $configuration = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'executeJob', array('jobType' => $jobType, 'jobName' => $jobName, 'configuration' => $configuration), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->executeJob($jobType, $jobName, $configuration);
    }
    public function getJobErrors($jobCode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJobErrors', array('jobCode' => $jobCode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJobErrors($jobCode);
    }
    public function getJobFailureExceptions($jobCode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJobFailureExceptions', array('jobCode' => $jobCode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJobFailureExceptions($jobCode);
    }
    public function getJob($jobType, $jobName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJob', array('jobType' => $jobType, 'jobName' => $jobName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJob($jobType, $jobName);
    }
    public function setValidationMode($validationMode)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setValidationMode', array('validationMode' => $validationMode), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setValidationMode($validationMode);
    }
    public function isValidationMode()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isValidationMode', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isValidationMode();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityManager, $instance->batchJobRegistry, $instance->contextRegistry, $instance->managerRegistry, $instance->batchJobRepository, $instance->eventDispatcher, $instance->validationMode, $instance->contextAggregatorRegistry);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\BatchBundle\Connector\ConnectorRegistry $jobRegistry, \Oro\Bundle\BatchBundle\Job\DoctrineJobRepository $batchJobRepository, \Oro\Bundle\ImportExportBundle\Context\ContextRegistry $contextRegistry, \Doctrine\Persistence\ManagerRegistry $managerRegistry, \Oro\Bundle\ImportExportBundle\Job\Context\ContextAggregatorRegistry $contextAggregatorRegistry)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\ImportExport\\Job\\Executor');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityManager, $this->batchJobRegistry, $this->contextRegistry, $this->managerRegistry, $this->batchJobRepository, $this->eventDispatcher, $this->validationMode, $this->contextAggregatorRegistry);
        }
        $this->valueHolder6655f->__construct($jobRegistry, $batchJobRepository, $contextRegistry, $managerRegistry, $contextAggregatorRegistry);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\ImportExport\\Job\\Executor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\ImportExport\\Job\\Executor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\ImportExport\\Job\\Executor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\ImportExport\\Job\\Executor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityManager, $this->batchJobRegistry, $this->contextRegistry, $this->managerRegistry, $this->batchJobRepository, $this->eventDispatcher, $this->validationMode, $this->contextAggregatorRegistry);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('Executor_618db2e', false)) {
    \class_alias(__NAMESPACE__.'\\Executor_618db2e', 'Executor_618db2e', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Manager/TypesRegistry.php';
class TypesRegistry_bf0da67 extends \Oro\Bundle\IntegrationBundle\Manager\TypesRegistry implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function addChannelType($typeName, \Oro\Bundle\IntegrationBundle\Provider\ChannelInterface $type)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'addChannelType', array('typeName' => $typeName, 'type' => $type), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->addChannelType($typeName, $type);
    }
    public function getRegisteredChannelTypes()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRegisteredChannelTypes', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRegisteredChannelTypes();
    }
    public function getIntegrationByType($typeName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getIntegrationByType', array('typeName' => $typeName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getIntegrationByType($typeName);
    }
    public function getAvailableChannelTypesChoiceList()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAvailableChannelTypesChoiceList', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAvailableChannelTypesChoiceList();
    }
    public function getAvailableIntegrationTypesDetailedData()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAvailableIntegrationTypesDetailedData', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAvailableIntegrationTypesDetailedData();
    }
    public function addTransportType($typeName, $integrationTypeName, \Oro\Bundle\IntegrationBundle\Provider\TransportInterface $type)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'addTransportType', array('typeName' => $typeName, 'integrationTypeName' => $integrationTypeName, 'type' => $type), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->addTransportType($typeName, $integrationTypeName, $type);
    }
    public function getTransportType($integrationTypeName, $transportType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getTransportType', array('integrationTypeName' => $integrationTypeName, 'transportType' => $transportType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getTransportType($integrationTypeName, $transportType);
    }
    public function getRegisteredTransportTypes($integrationType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRegisteredTransportTypes', array('integrationType' => $integrationType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRegisteredTransportTypes($integrationType);
    }
    public function getAvailableTransportTypesChoiceList($integrationType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAvailableTransportTypesChoiceList', array('integrationType' => $integrationType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAvailableTransportTypesChoiceList($integrationType);
    }
    public function getTransportTypeBySettingEntity(\Oro\Bundle\IntegrationBundle\Entity\Transport $transportEntity, $integrationType, $typeNameOnly = false)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getTransportTypeBySettingEntity', array('transportEntity' => $transportEntity, 'integrationType' => $integrationType, 'typeNameOnly' => $typeNameOnly), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getTransportTypeBySettingEntity($transportEntity, $integrationType, $typeNameOnly);
    }
    public function addConnectorType($typeName, $integrationTypeName, \Oro\Bundle\IntegrationBundle\Provider\ConnectorInterface $type)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'addConnectorType', array('typeName' => $typeName, 'integrationTypeName' => $integrationTypeName, 'type' => $type), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->addConnectorType($typeName, $integrationTypeName, $type);
    }
    public function getConnectorType($integrationType, $type)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getConnectorType', array('integrationType' => $integrationType, 'type' => $type), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getConnectorType($integrationType, $type);
    }
    public function getRegisteredConnectorsTypes($integrationType, $filterClosure = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRegisteredConnectorsTypes', array('integrationType' => $integrationType, 'filterClosure' => $filterClosure), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRegisteredConnectorsTypes($integrationType, $filterClosure);
    }
    public function getAvailableConnectorsTypesChoiceList($integrationType, $filterClosure = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAvailableConnectorsTypesChoiceList', array('integrationType' => $integrationType, 'filterClosure' => $filterClosure), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAvailableConnectorsTypesChoiceList($integrationType, $filterClosure);
    }
    public function supportsForceSync($integrationType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'supportsForceSync', array('integrationType' => $integrationType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->supportsForceSync($integrationType);
    }
    public function supportsSync($integrationType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'supportsSync', array('integrationType' => $integrationType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->supportsSync($integrationType);
    }
    public function getDefaultOwnerType($integrationType = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDefaultOwnerType', array('integrationType' => $integrationType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDefaultOwnerType($integrationType);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->integrationTypes, $instance->transportTypes, $instance->connectorTypes);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct()
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Manager\\TypesRegistry');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->integrationTypes, $this->transportTypes, $this->connectorTypes);
        }
        $this->valueHolder6655f->__construct();
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Manager\\TypesRegistry');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Manager\\TypesRegistry');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Manager\\TypesRegistry');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Manager\\TypesRegistry');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->integrationTypes, $this->transportTypes, $this->connectorTypes);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('TypesRegistry_bf0da67', false)) {
    \class_alias(__NAMESPACE__.'\\TypesRegistry_bf0da67', 'TypesRegistry_bf0da67', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Provider/SyncProcessorInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/psr/log/Psr/Log/LoggerAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Provider/LoggerStrategyAwareInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/psr/log/Psr/Log/LoggerAwareTrait.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Provider/AbstractSyncProcessor.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Provider/ReverseSyncProcessor.php';
class ReverseSyncProcessor_0ecbb23 extends \Oro\Bundle\IntegrationBundle\Provider\ReverseSyncProcessor implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function process(\Oro\Bundle\IntegrationBundle\Entity\Channel $integration, $connector, array $parameters = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'process', array('integration' => $integration, 'connector' => $connector, 'parameters' => $parameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->process($integration, $connector, $parameters);
    }
    public function getLoggerStrategy()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getLoggerStrategy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getLoggerStrategy();
    }
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLogger', array('logger' => $logger), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLogger($logger);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->doctrineRegistry, $instance->processorRegistry, $instance->jobExecutor, $instance->registry, $instance->eventDispatcher, $instance->logger);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Doctrine\Persistence\ManagerRegistry $doctrineRegistry, \Oro\Bundle\ImportExportBundle\Processor\ProcessorRegistry $processorRegistry, \Oro\Bundle\IntegrationBundle\ImportExport\Job\Executor $jobExecutor, \Oro\Bundle\IntegrationBundle\Manager\TypesRegistry $registry, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, ?\Oro\Bundle\IntegrationBundle\Logger\LoggerStrategy $logger = null)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\ReverseSyncProcessor');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->doctrineRegistry, $this->processorRegistry, $this->jobExecutor, $this->registry, $this->eventDispatcher, $this->logger);
        }
        $this->valueHolder6655f->__construct($doctrineRegistry, $processorRegistry, $jobExecutor, $registry, $eventDispatcher, $logger);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\ReverseSyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\ReverseSyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\ReverseSyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\ReverseSyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->doctrineRegistry, $this->processorRegistry, $this->jobExecutor, $this->registry, $this->eventDispatcher, $this->logger);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ReverseSyncProcessor_0ecbb23', false)) {
    \class_alias(__NAMESPACE__.'\\ReverseSyncProcessor_0ecbb23', 'ReverseSyncProcessor_0ecbb23', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/IntegrationBundle/Provider/SyncProcessor.php';
class SyncProcessor_85b423b extends \Oro\Bundle\IntegrationBundle\Provider\SyncProcessor implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function process(\Oro\Bundle\IntegrationBundle\Entity\Channel $integration, $connector = null, array $parameters = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'process', array('integration' => $integration, 'connector' => $connector, 'parameters' => $parameters), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->process($integration, $connector, $parameters);
    }
    public function getLoggerStrategy()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getLoggerStrategy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getLoggerStrategy();
    }
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLogger', array('logger' => $logger), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLogger($logger);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->doctrineRegistry, $instance->processorRegistry, $instance->jobExecutor, $instance->registry, $instance->eventDispatcher, $instance->logger);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Doctrine\Persistence\ManagerRegistry $doctrineRegistry, \Oro\Bundle\ImportExportBundle\Processor\ProcessorRegistry $processorRegistry, \Oro\Bundle\IntegrationBundle\ImportExport\Job\Executor $jobExecutor, \Oro\Bundle\IntegrationBundle\Manager\TypesRegistry $registry, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher, ?\Oro\Bundle\IntegrationBundle\Logger\LoggerStrategy $logger = null)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\SyncProcessor');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->doctrineRegistry, $this->processorRegistry, $this->jobExecutor, $this->registry, $this->eventDispatcher, $this->logger);
        }
        $this->valueHolder6655f->__construct($doctrineRegistry, $processorRegistry, $jobExecutor, $registry, $eventDispatcher, $logger);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\SyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\SyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\SyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\IntegrationBundle\\Provider\\SyncProcessor');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->doctrineRegistry, $this->processorRegistry, $this->jobExecutor, $this->registry, $this->eventDispatcher, $this->logger);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('SyncProcessor_85b423b', false)) {
    \class_alias(__NAMESPACE__.'\\SyncProcessor_85b423b', 'SyncProcessor_85b423b', false);
}

include_once \dirname(__DIR__, 4).'/vendor/psr/cache/src/CacheItemPoolInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Adapter/AdapterInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/cache-contracts/CacheInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/service-contracts/ResetInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/ResettableInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/cache/Adapter/ArrayAdapter.php';
class ArrayAdapter_f4a3501 extends \Symfony\Component\Cache\Adapter\ArrayAdapter implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function get(string $key, callable $callback, ?float $beta = null, ?array &$metadata = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'get', array('key' => $key, 'callback' => $callback, 'beta' => $beta, 'metadata' => $metadata), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->get($key, $callback, $beta, $metadata);
    }
    public function delete(string $key) : bool
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'delete', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->delete($key);
    }
    public function hasItem($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'hasItem', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->hasItem($key);
    }
    public function getItem($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getItem', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getItem($key);
    }
    public function getItems(array $keys = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getItems', array('keys' => $keys), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getItems($keys);
    }
    public function deleteItem($key)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'deleteItem', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->deleteItem($key);
    }
    public function deleteItems(array $keys)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'deleteItems', array('keys' => $keys), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->deleteItems($keys);
    }
    public function save(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'save', array('item' => $item), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->save($item);
    }
    public function saveDeferred(\Psr\Cache\CacheItemInterface $item)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'saveDeferred', array('item' => $item), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->saveDeferred($item);
    }
    public function commit()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'commit', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->commit();
    }
    public function clear(string $prefix = '')
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'clear', array('prefix' => $prefix), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->clear($prefix);
    }
    public function getValues()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getValues', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getValues();
    }
    public function reset()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'reset', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->reset();
    }
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLogger', array('logger' => $logger), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLogger($logger);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->logger);
        \Closure::bind(function (\Symfony\Component\Cache\Adapter\ArrayAdapter $instance) {
            unset($instance->storeSerialized, $instance->values, $instance->expiries, $instance->defaultLifetime, $instance->maxLifetime, $instance->maxItems);
        }, $instance, 'Symfony\\Component\\Cache\\Adapter\\ArrayAdapter')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(int $defaultLifetime = 0, bool $storeSerialized = true, float $maxLifetime = 0, int $maxItems = 0)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Symfony\\Component\\Cache\\Adapter\\ArrayAdapter');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->logger);
        \Closure::bind(function (\Symfony\Component\Cache\Adapter\ArrayAdapter $instance) {
            unset($instance->storeSerialized, $instance->values, $instance->expiries, $instance->defaultLifetime, $instance->maxLifetime, $instance->maxItems);
        }, $this, 'Symfony\\Component\\Cache\\Adapter\\ArrayAdapter')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($defaultLifetime, $storeSerialized, $maxLifetime, $maxItems);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\Cache\\Adapter\\ArrayAdapter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\Cache\\Adapter\\ArrayAdapter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\Cache\\Adapter\\ArrayAdapter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\Cache\\Adapter\\ArrayAdapter');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->logger);
        \Closure::bind(function (\Symfony\Component\Cache\Adapter\ArrayAdapter $instance) {
            unset($instance->storeSerialized, $instance->values, $instance->expiries, $instance->defaultLifetime, $instance->maxLifetime, $instance->maxItems);
        }, $this, 'Symfony\\Component\\Cache\\Adapter\\ArrayAdapter')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ArrayAdapter_f4a3501', false)) {
    \class_alias(__NAMESPACE__.'\\ArrayAdapter_f4a3501', 'ArrayAdapter_f4a3501', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Component/MessageQueue/Client/MessageProducerInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Component/MessageQueue/Client/MessageProducer.php';
class MessageProducer_ba0b794 extends \Oro\Component\MessageQueue\Client\MessageProducer implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setMiddlewares(iterable $middlewares) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setMiddlewares', array('middlewares' => $middlewares), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setMiddlewares($middlewares);
return;
    }
    public function send($topic, $message) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'send', array('topic' => $topic, 'message' => $message), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->send($topic, $message);
return;
    }
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLogger', array('logger' => $logger), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLogger($logger);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->logger);
        \Closure::bind(function (\Oro\Component\MessageQueue\Client\MessageProducer $instance) {
            unset($instance->driver, $instance->messageRouter, $instance->debug, $instance->middlewares);
        }, $instance, 'Oro\\Component\\MessageQueue\\Client\\MessageProducer')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Component\MessageQueue\Client\DriverInterface $driver, \Oro\Component\MessageQueue\Client\Router\MessageRouterInterface $messageRouter, bool $debug = false)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Component\\MessageQueue\\Client\\MessageProducer');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->logger);
        \Closure::bind(function (\Oro\Component\MessageQueue\Client\MessageProducer $instance) {
            unset($instance->driver, $instance->messageRouter, $instance->debug, $instance->middlewares);
        }, $this, 'Oro\\Component\\MessageQueue\\Client\\MessageProducer')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($driver, $messageRouter, $debug);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Client\\MessageProducer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Client\\MessageProducer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Client\\MessageProducer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Client\\MessageProducer');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->logger);
        \Closure::bind(function (\Oro\Component\MessageQueue\Client\MessageProducer $instance) {
            unset($instance->driver, $instance->messageRouter, $instance->debug, $instance->middlewares);
        }, $this, 'Oro\\Component\\MessageQueue\\Client\\MessageProducer')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('MessageProducer_ba0b794', false)) {
    \class_alias(__NAMESPACE__.'\\MessageProducer_ba0b794', 'MessageProducer_ba0b794', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Component/MessageQueue/Log/MessageProcessorClassProvider.php';
class MessageProcessorClassProvider_57a37c0 extends \Oro\Component\MessageQueue\Log\MessageProcessorClassProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getMessageProcessorClassByName(string $messageProcessorName) : string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMessageProcessorClassByName', array('messageProcessorName' => $messageProcessorName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMessageProcessorClassByName($messageProcessorName);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Component\MessageQueue\Log\MessageProcessorClassProvider $instance) {
            unset($instance->messageProcessorRegistry, $instance->messageProcessorClassByName);
        }, $instance, 'Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Component\MessageQueue\Client\MessageProcessorRegistryInterface $messageProcessorRegistry)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Component\MessageQueue\Log\MessageProcessorClassProvider $instance) {
            unset($instance->messageProcessorRegistry, $instance->messageProcessorClassByName);
        }, $this, 'Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($messageProcessorRegistry);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Component\MessageQueue\Log\MessageProcessorClassProvider $instance) {
            unset($instance->messageProcessorRegistry, $instance->messageProcessorClassByName);
        }, $this, 'Oro\\Component\\MessageQueue\\Log\\MessageProcessorClassProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('MessageProcessorClassProvider_57a37c0', false)) {
    \class_alias(__NAMESPACE__.'\\MessageProcessorClassProvider_57a37c0', 'MessageProcessorClassProvider_57a37c0', false);
}

include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Schema/Visitor/Visitor.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Schema/Visitor/NamespaceVisitor.php';
include_once \dirname(__DIR__, 4).'/vendor/doctrine/dbal/lib/Doctrine/DBAL/Schema/Visitor/AbstractVisitor.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/MigrationBundle/Tools/SchemaDumper.php';
class SchemaDumper_3b4a9bc extends \Oro\Bundle\MigrationBundle\Tools\SchemaDumper implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function acceptSchema(\Doctrine\DBAL\Schema\Schema $schema)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'acceptSchema', array('schema' => $schema), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->acceptSchema($schema);
    }
    public function dump(?array $allowedTables = null, $namespace = null, $className = 'AllMigration', $version = 'v1_0', ?array $extendedOptions = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'dump', array('allowedTables' => $allowedTables, 'namespace' => $namespace, 'className' => $className, 'version' => $version, 'extendedOptions' => $extendedOptions), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->dump($allowedTables, $namespace, $className, $version, $extendedOptions);
    }
    public function acceptNamespace($namespaceName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'acceptNamespace', array('namespaceName' => $namespaceName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->acceptNamespace($namespaceName);
    }
    public function acceptTable(\Doctrine\DBAL\Schema\Table $table)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'acceptTable', array('table' => $table), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->acceptTable($table);
    }
    public function acceptColumn(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Column $column)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'acceptColumn', array('table' => $table, 'column' => $column), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->acceptColumn($table, $column);
    }
    public function acceptForeignKey(\Doctrine\DBAL\Schema\Table $localTable, \Doctrine\DBAL\Schema\ForeignKeyConstraint $fkConstraint)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'acceptForeignKey', array('localTable' => $localTable, 'fkConstraint' => $fkConstraint), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->acceptForeignKey($localTable, $fkConstraint);
    }
    public function acceptIndex(\Doctrine\DBAL\Schema\Table $table, \Doctrine\DBAL\Schema\Index $index)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'acceptIndex', array('table' => $table, 'index' => $index), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->acceptIndex($table, $index);
    }
    public function acceptSequence(\Doctrine\DBAL\Schema\Sequence $sequence)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'acceptSequence', array('sequence' => $sequence), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->acceptSequence($sequence);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->schema, $instance->twig);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Twig\Environment $twig)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\MigrationBundle\\Tools\\SchemaDumper');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->schema, $this->twig);
        }
        $this->valueHolder6655f->__construct($twig);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\MigrationBundle\\Tools\\SchemaDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\MigrationBundle\\Tools\\SchemaDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\MigrationBundle\\Tools\\SchemaDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\MigrationBundle\\Tools\\SchemaDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->schema, $this->twig);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('SchemaDumper_3b4a9bc', false)) {
    \class_alias(__NAMESPACE__.'\\SchemaDumper_3b4a9bc', 'SchemaDumper_3b4a9bc', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/NotificationBundle/Provider/EmailNotificationEntityProvider.php';
class EmailNotificationEntityProvider_a2e52bf extends \Oro\Bundle\NotificationBundle\Provider\EmailNotificationEntityProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setExclusionProvider(\Oro\Bundle\EntityBundle\Provider\ExclusionProviderInterface $exclusionProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setExclusionProvider', array('exclusionProvider' => $exclusionProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setExclusionProvider($exclusionProvider);
    }
    public function getEntities($sortByPluralLabel = true, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntities', array('sortByPluralLabel' => $sortByPluralLabel, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntities($sortByPluralLabel, $applyExclusions, $translate);
    }
    public function getEntity($entityName, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntity', array('entityName' => $entityName, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntity($entityName, $translate);
    }
    public function getEnabledEntity($entityName, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEnabledEntity', array('entityName' => $entityName, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEnabledEntity($entityName, $applyExclusions, $translate);
    }
    public function isIgnoredEntity($className)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isIgnoredEntity', array('className' => $className), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isIgnoredEntity($className);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityConfigProvider, $instance->extendConfigProvider, $instance->entityClassResolver, $instance->translator, $instance->exclusionProvider, $instance->featureChecker);
        \Closure::bind(function (\Oro\Bundle\NotificationBundle\Provider\EmailNotificationEntityProvider $instance) {
            unset($instance->doctrine);
        }, $instance, 'Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $extendConfigProvider, \Oro\Bundle\EntityBundle\ORM\EntityClassResolver $entityClassResolver, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Oro\Bundle\FeatureToggleBundle\Checker\FeatureChecker $featureChecker, \Doctrine\Persistence\ManagerRegistry $doctrine)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->translator, $this->exclusionProvider, $this->featureChecker);
        \Closure::bind(function (\Oro\Bundle\NotificationBundle\Provider\EmailNotificationEntityProvider $instance) {
            unset($instance->doctrine);
        }, $this, 'Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($entityConfigProvider, $extendConfigProvider, $entityClassResolver, $translator, $featureChecker, $doctrine);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->translator, $this->exclusionProvider, $this->featureChecker);
        \Closure::bind(function (\Oro\Bundle\NotificationBundle\Provider\EmailNotificationEntityProvider $instance) {
            unset($instance->doctrine);
        }, $this, 'Oro\\Bundle\\NotificationBundle\\Provider\\EmailNotificationEntityProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('EmailNotificationEntityProvider_a2e52bf', false)) {
    \class_alias(__NAMESPACE__.'\\EmailNotificationEntityProvider_a2e52bf', 'EmailNotificationEntityProvider_a2e52bf', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/QueryDesignerBundle/QueryDesigner/FunctionProviderInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/QueryDesignerBundle/QueryDesigner/Manager.php';
class Manager_c00c27b extends \Oro\Bundle\QueryDesignerBundle\QueryDesigner\Manager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getMetadata($queryType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMetadata', array('queryType' => $queryType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMetadata($queryType);
    }
    public function createFilter($name, ?array $params = null)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createFilter', array('name' => $name, 'params' => $params), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createFilter($name, $params);
    }
    public function getFunction($name, $groupName, $groupType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getFunction', array('name' => $name, 'groupName' => $groupName, 'groupType' => $groupType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getFunction($name, $groupName, $groupType);
    }
    public function getExcludedProperties(array $filterNames)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getExcludedProperties', array('filterNames' => $filterNames), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getExcludedProperties($filterNames);
    }
    public function getMetadataForGrouping()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMetadataForGrouping', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMetadataForGrouping();
    }
    public function getMetadataForFunctions($groupType, $queryType)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getMetadataForFunctions', array('groupType' => $groupType, 'queryType' => $queryType), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getMetadataForFunctions($groupType, $queryType);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\QueryDesignerBundle\QueryDesigner\Manager $instance) {
            unset($instance->configProvider, $instance->configResolver, $instance->entityHierarchyProvider, $instance->translator, $instance->filterBag, $instance->config);
        }, $instance, 'Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\QueryDesignerBundle\QueryDesigner\ConfigurationProvider $configProvider, \Oro\Bundle\QueryDesignerBundle\QueryDesigner\ConfigurationResolver $configResolver, \Oro\Bundle\EntityBundle\Provider\EntityHierarchyProviderInterface $entityHierarchyProvider, \Oro\Bundle\FilterBundle\Filter\FilterBagInterface $filterBag, \Symfony\Contracts\Translation\TranslatorInterface $translator)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\QueryDesignerBundle\QueryDesigner\Manager $instance) {
            unset($instance->configProvider, $instance->configResolver, $instance->entityHierarchyProvider, $instance->translator, $instance->filterBag, $instance->config);
        }, $this, 'Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($configProvider, $configResolver, $entityHierarchyProvider, $filterBag, $translator);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\QueryDesignerBundle\QueryDesigner\Manager $instance) {
            unset($instance->configProvider, $instance->configResolver, $instance->entityHierarchyProvider, $instance->translator, $instance->filterBag, $instance->config);
        }, $this, 'Oro\\Bundle\\QueryDesignerBundle\\QueryDesigner\\Manager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('Manager_c00c27b', false)) {
    \class_alias(__NAMESPACE__.'\\Manager_c00c27b', 'Manager_c00c27b', false);
}

include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/Storage/Handler/AbstractSessionHandler.php';
include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/RedisConfigBundle/Session/Storage/Handler/RedisLockingSessionHandler.php';
class RedisLockingSessionHandler_7049562 extends \Oro\Bundle\RedisConfigBundle\Session\Storage\Handler\RedisLockingSessionHandler implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setLogLevel(string $logLevel) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLogLevel', array('logLevel' => $logLevel), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setLogLevel($logLevel);
return;
    }
    public function setTtl(int $ttl)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setTtl', array('ttl' => $ttl), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setTtl($ttl);
    }
    #[\ReturnTypeWillChange]
    public function close() : bool
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'close', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->close();
    }
    #[\ReturnTypeWillChange]
    public function updateTimestamp(string $id, string $data) : bool
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'updateTimestamp', array('id' => $id, 'data' => $data), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->updateTimestamp($id, $data);
    }
    #[\ReturnTypeWillChange]
    public function gc($maxlifetime) : int|false
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'gc', array('maxlifetime' => $maxlifetime), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->gc($maxlifetime);
    }
    public function shutdown() : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'shutdown', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->shutdown();
return;
    }
    #[\ReturnTypeWillChange]
    public function open($savePath, $sessionName)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'open', array('savePath' => $savePath, 'sessionName' => $sessionName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->open($savePath, $sessionName);
    }
    #[\ReturnTypeWillChange]
    public function validateId($sessionId)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'validateId', array('sessionId' => $sessionId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->validateId($sessionId);
    }
    #[\ReturnTypeWillChange]
    public function read($sessionId)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'read', array('sessionId' => $sessionId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->read($sessionId);
    }
    #[\ReturnTypeWillChange]
    public function write($sessionId, $data)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'write', array('sessionId' => $sessionId, 'data' => $data), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->write($sessionId, $data);
    }
    #[\ReturnTypeWillChange]
    public function destroy($sessionId)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'destroy', array('sessionId' => $sessionId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->destroy($sessionId);
    }
    public function setLogger(\Psr\Log\LoggerInterface $logger)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLogger', array('logger' => $logger), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLogger($logger);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->redis, $instance->ttl, $instance->prefix, $instance->locking, $instance->locked, $instance->lockKey, $instance->token, $instance->spinLockWait, $instance->lockMaxWait, $instance->logger);
        \Closure::bind(function (\Oro\Bundle\RedisConfigBundle\Session\Storage\Handler\RedisLockingSessionHandler $instance) {
            unset($instance->logLevel);
        }, $instance, 'Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler')->__invoke($instance);
        \Closure::bind(function (\Symfony\Component\HttpFoundation\Session\Storage\Handler\AbstractSessionHandler $instance) {
            unset($instance->sessionName, $instance->prefetchId, $instance->prefetchData, $instance->newSessionId, $instance->igbinaryEmptyData);
        }, $instance, 'Symfony\\Component\\HttpFoundation\\Session\\Storage\\Handler\\AbstractSessionHandler')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Predis\Client|\Redis $redis, array $options = [], string $prefix = 'session', bool $locking = true, int $spinLockWait = 150000)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->redis, $this->ttl, $this->prefix, $this->locking, $this->locked, $this->lockKey, $this->token, $this->spinLockWait, $this->lockMaxWait, $this->logger);
        \Closure::bind(function (\Oro\Bundle\RedisConfigBundle\Session\Storage\Handler\RedisLockingSessionHandler $instance) {
            unset($instance->logLevel);
        }, $this, 'Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler')->__invoke($this);
        \Closure::bind(function (\Symfony\Component\HttpFoundation\Session\Storage\Handler\AbstractSessionHandler $instance) {
            unset($instance->sessionName, $instance->prefetchId, $instance->prefetchData, $instance->newSessionId, $instance->igbinaryEmptyData);
        }, $this, 'Symfony\\Component\\HttpFoundation\\Session\\Storage\\Handler\\AbstractSessionHandler')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($redis, $options, $prefix, $locking, $spinLockWait);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->redis, $this->ttl, $this->prefix, $this->locking, $this->locked, $this->lockKey, $this->token, $this->spinLockWait, $this->lockMaxWait, $this->logger);
        \Closure::bind(function (\Oro\Bundle\RedisConfigBundle\Session\Storage\Handler\RedisLockingSessionHandler $instance) {
            unset($instance->logLevel);
        }, $this, 'Oro\\Bundle\\RedisConfigBundle\\Session\\Storage\\Handler\\RedisLockingSessionHandler')->__invoke($this);
        \Closure::bind(function (\Symfony\Component\HttpFoundation\Session\Storage\Handler\AbstractSessionHandler $instance) {
            unset($instance->sessionName, $instance->prefetchId, $instance->prefetchData, $instance->newSessionId, $instance->igbinaryEmptyData);
        }, $this, 'Symfony\\Component\\HttpFoundation\\Session\\Storage\\Handler\\AbstractSessionHandler')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
    public function __destruct()
    {
        $this->initializer1c70a || $this->valueHolder6655f->__destruct();
    }
}

if (!\class_exists('RedisLockingSessionHandler_7049562', false)) {
    \class_alias(__NAMESPACE__.'\\RedisLockingSessionHandler_7049562', 'RedisLockingSessionHandler_7049562', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/SegmentBundle/Query/SegmentQueryConverterFactory.php';
class SegmentQueryConverterFactory_134960d extends \Oro\Bundle\SegmentBundle\Query\SegmentQueryConverterFactory implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function createInstance() : \Oro\Bundle\SegmentBundle\Query\SegmentQueryConverter
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'createInstance', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->createInstance();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\SegmentBundle\Query\SegmentQueryConverterFactory $instance) {
            unset($instance->functionProvider, $instance->virtualFieldProvider, $instance->virtualRelationProvider, $instance->doctrineHelper, $instance->restrictionBuilder, $instance->segmentQueryConverterState);
        }, $instance, 'Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\QueryDesignerBundle\QueryDesigner\FunctionProviderInterface $functionProvider, \Oro\Bundle\EntityBundle\Provider\VirtualFieldProviderInterface $virtualFieldProvider, \Oro\Bundle\EntityBundle\Provider\VirtualRelationProviderInterface $virtualRelationProvider, \Oro\Bundle\EntityBundle\ORM\DoctrineHelper $doctrineHelper, \Oro\Bundle\QueryDesignerBundle\QueryDesigner\RestrictionBuilderInterface $restrictionBuilder, \Oro\Bundle\SegmentBundle\Query\SegmentQueryConverterState $segmentQueryConverterState)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\SegmentBundle\Query\SegmentQueryConverterFactory $instance) {
            unset($instance->functionProvider, $instance->virtualFieldProvider, $instance->virtualRelationProvider, $instance->doctrineHelper, $instance->restrictionBuilder, $instance->segmentQueryConverterState);
        }, $this, 'Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($functionProvider, $virtualFieldProvider, $virtualRelationProvider, $doctrineHelper, $restrictionBuilder, $segmentQueryConverterState);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\SegmentBundle\Query\SegmentQueryConverterFactory $instance) {
            unset($instance->functionProvider, $instance->virtualFieldProvider, $instance->virtualRelationProvider, $instance->doctrineHelper, $instance->restrictionBuilder, $instance->segmentQueryConverterState);
        }, $this, 'Oro\\Bundle\\SegmentBundle\\Query\\SegmentQueryConverterFactory')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('SegmentQueryConverterFactory_134960d', false)) {
    \class_alias(__NAMESPACE__.'\\SegmentQueryConverterFactory_134960d', 'SegmentQueryConverterFactory_134960d', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/SegmentBundle/Entity/Manager/SegmentManager.php';
class SegmentManager_8be4ae0 extends \Oro\Bundle\SegmentBundle\Entity\Manager\SegmentManager implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getSegmentTypeChoices() : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSegmentTypeChoices', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSegmentTypeChoices();
    }
    public function getSegmentByEntityName(string $entityName, ?string $term, int $page = 1, ?int $skippedSegmentId = null) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSegmentByEntityName', array('entityName' => $entityName, 'term' => $term, 'page' => $page, 'skippedSegmentId' => $skippedSegmentId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSegmentByEntityName($entityName, $term, $page, $skippedSegmentId);
    }
    public function findById(int $segmentId) : ?\Oro\Bundle\SegmentBundle\Entity\Segment
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'findById', array('segmentId' => $segmentId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->findById($segmentId);
    }
    public function getSegmentQueryBuilder(\Oro\Bundle\SegmentBundle\Entity\Segment $segment) : ?\Doctrine\ORM\QueryBuilder
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSegmentQueryBuilder', array('segment' => $segment), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSegmentQueryBuilder($segment);
    }
    public function filterBySegment(\Doctrine\ORM\QueryBuilder $queryBuilder, \Oro\Bundle\SegmentBundle\Entity\Segment $segment) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'filterBySegment', array('queryBuilder' => $queryBuilder, 'segment' => $segment), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->filterBySegment($queryBuilder, $segment);
return;
    }
    public function getEntityQueryBuilder(\Oro\Bundle\SegmentBundle\Entity\Segment $segment) : ?\Doctrine\ORM\QueryBuilder
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntityQueryBuilder', array('segment' => $segment), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntityQueryBuilder($segment);
    }
    public function getFilterSubQuery(\Oro\Bundle\SegmentBundle\Entity\Segment $segment, \Doctrine\ORM\QueryBuilder $externalQueryBuilder) : ?string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getFilterSubQuery', array('segment' => $segment, 'externalQueryBuilder' => $externalQueryBuilder), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getFilterSubQuery($segment, $externalQueryBuilder);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\SegmentBundle\Entity\Manager\SegmentManager $instance) {
            unset($instance->doctrine, $instance->queryBuilderRegistry, $instance->subQueryLimitHelper, $instance->aclHelper, $instance->logger);
        }, $instance, 'Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Doctrine\Persistence\ManagerRegistry $doctrine, \Oro\Bundle\SegmentBundle\Query\SegmentQueryBuilderRegistry $queryBuilderRegistry, \Oro\Bundle\QueryDesignerBundle\QueryDesigner\SubQueryLimitHelper $subQueryLimitHelper, \Oro\Bundle\SecurityBundle\ORM\Walker\AclHelper $aclHelper, \Psr\Log\LoggerInterface $logger)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\SegmentBundle\Entity\Manager\SegmentManager $instance) {
            unset($instance->doctrine, $instance->queryBuilderRegistry, $instance->subQueryLimitHelper, $instance->aclHelper, $instance->logger);
        }, $this, 'Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($doctrine, $queryBuilderRegistry, $subQueryLimitHelper, $aclHelper, $logger);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\SegmentBundle\Entity\Manager\SegmentManager $instance) {
            unset($instance->doctrine, $instance->queryBuilderRegistry, $instance->subQueryLimitHelper, $instance->aclHelper, $instance->logger);
        }, $this, 'Oro\\Bundle\\SegmentBundle\\Entity\\Manager\\SegmentManager')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('SegmentManager_8be4ae0', false)) {
    \class_alias(__NAMESPACE__.'\\SegmentManager_8be4ae0', 'SegmentManager_8be4ae0', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/EventListener/ClearDynamicTranslationCacheImportListener.php';
class ClearDynamicTranslationCacheImportListener_ad7c77a extends \Oro\Bundle\TranslationBundle\EventListener\ClearDynamicTranslationCacheImportListener implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function onAfterImportTranslations(\Oro\Bundle\ImportExportBundle\Event\AfterJobExecutionEvent $event) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'onAfterImportTranslations', array('event' => $event), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->onAfterImportTranslations($event);
return;
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\EventListener\ClearDynamicTranslationCacheImportListener $instance) {
            unset($instance->dynamicTranslationCache, $instance->jobName);
        }, $instance, 'Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\TranslationBundle\Translation\DynamicTranslationCache $dynamicTranslationCache, string $jobName)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\EventListener\ClearDynamicTranslationCacheImportListener $instance) {
            unset($instance->dynamicTranslationCache, $instance->jobName);
        }, $this, 'Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($dynamicTranslationCache, $jobName);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\EventListener\ClearDynamicTranslationCacheImportListener $instance) {
            unset($instance->dynamicTranslationCache, $instance->jobName);
        }, $this, 'Oro\\Bundle\\TranslationBundle\\EventListener\\ClearDynamicTranslationCacheImportListener')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ClearDynamicTranslationCacheImportListener_ad7c77a', false)) {
    \class_alias(__NAMESPACE__.'\\ClearDynamicTranslationCacheImportListener_ad7c77a', 'ClearDynamicTranslationCacheImportListener_ad7c77a', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/TranslationBundle/Provider/JsTranslationDumper.php';
class JsTranslationDumper_dd6ab5b extends \Oro\Bundle\TranslationBundle\Provider\JsTranslationDumper implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getAllLocales() : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAllLocales', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAllLocales();
    }
    public function dumpTranslations(array $locales = []) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'dumpTranslations', array('locales' => $locales), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->dumpTranslations($locales);
return;
    }
    public function dumpTranslationFile(string $locale) : string
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'dumpTranslationFile', array('locale' => $locale), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->dumpTranslationFile($locale);
    }
    public function isTranslationFileExist(string $locale) : bool
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'isTranslationFileExist', array('locale' => $locale), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->isTranslationFileExist($locale);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\Provider\JsTranslationDumper $instance) {
            unset($instance->generator, $instance->languageProvider, $instance->fileManager);
        }, $instance, 'Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\TranslationBundle\Provider\JsTranslationGenerator $generator, \Oro\Bundle\TranslationBundle\Provider\LanguageProvider $languageProvider, \Oro\Bundle\GaufretteBundle\FileManager $fileManager)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\Provider\JsTranslationDumper $instance) {
            unset($instance->generator, $instance->languageProvider, $instance->fileManager);
        }, $this, 'Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($generator, $languageProvider, $fileManager);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\TranslationBundle\Provider\JsTranslationDumper $instance) {
            unset($instance->generator, $instance->languageProvider, $instance->fileManager);
        }, $this, 'Oro\\Bundle\\TranslationBundle\\Provider\\JsTranslationDumper')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('JsTranslationDumper_dd6ab5b', false)) {
    \class_alias(__NAMESPACE__.'\\JsTranslationDumper_dd6ab5b', 'JsTranslationDumper_dd6ab5b', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/UserBundle/Handler/ResetPasswordHandler.php';
class ResetPasswordHandler_04faf98 extends \Oro\Bundle\UserBundle\Handler\ResetPasswordHandler implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function resetPasswordAndNotify(\Oro\Bundle\UserBundle\Entity\User $user) : bool
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'resetPasswordAndNotify', array('user' => $user), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->resetPasswordAndNotify($user);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\UserBundle\Handler\ResetPasswordHandler $instance) {
            unset($instance->mailManager, $instance->userManager, $instance->logger);
        }, $instance, 'Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\NotificationBundle\Manager\EmailNotificationManager $mailManager, \Oro\Bundle\UserBundle\Entity\UserManager $userManager, \Psr\Log\LoggerInterface $logger)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\UserBundle\Handler\ResetPasswordHandler $instance) {
            unset($instance->mailManager, $instance->userManager, $instance->logger);
        }, $this, 'Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($mailManager, $userManager, $logger);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\UserBundle\Handler\ResetPasswordHandler $instance) {
            unset($instance->mailManager, $instance->userManager, $instance->logger);
        }, $this, 'Oro\\Bundle\\UserBundle\\Handler\\ResetPasswordHandler')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('ResetPasswordHandler_04faf98', false)) {
    \class_alias(__NAMESPACE__.'\\ResetPasswordHandler_04faf98', 'ResetPasswordHandler_04faf98', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/UserBundle/Mailer/UserTemplateEmailSender.php';
class UserTemplateEmailSender_bcc95f3 extends \Oro\Bundle\UserBundle\Mailer\UserTemplateEmailSender implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function sendUserTemplateEmail(\Oro\Bundle\UserBundle\Entity\UserInterface $user, $emailTemplateName, array $emailTemplateParams = [], $scopeEntity = null) : int
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'sendUserTemplateEmail', array('user' => $user, 'emailTemplateName' => $emailTemplateName, 'emailTemplateParams' => $emailTemplateParams, 'scopeEntity' => $scopeEntity), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->sendUserTemplateEmail($user, $emailTemplateName, $emailTemplateParams, $scopeEntity);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\UserBundle\Mailer\UserTemplateEmailSender $instance) {
            unset($instance->notificationSettingsModel, $instance->emailTemplateManager);
        }, $instance, 'Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\NotificationBundle\Model\NotificationSettings $notificationSettingsModel, \Oro\Bundle\EmailBundle\Manager\EmailTemplateManager $emailTemplateManager)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Oro\Bundle\UserBundle\Mailer\UserTemplateEmailSender $instance) {
            unset($instance->notificationSettingsModel, $instance->emailTemplateManager);
        }, $this, 'Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($notificationSettingsModel, $emailTemplateManager);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Oro\Bundle\UserBundle\Mailer\UserTemplateEmailSender $instance) {
            unset($instance->notificationSettingsModel, $instance->emailTemplateManager);
        }, $this, 'Oro\\Bundle\\UserBundle\\Mailer\\UserTemplateEmailSender')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('UserTemplateEmailSender_bcc95f3', false)) {
    \class_alias(__NAMESPACE__.'\\UserTemplateEmailSender_bcc95f3', 'UserTemplateEmailSender_bcc95f3', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/UserBundle/Provider/UserConfigurationFormProvider.php';
class UserConfigurationFormProvider_6e2dd1e extends \Oro\Bundle\UserBundle\Provider\UserConfigurationFormProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setParentCheckboxLabel(string $label) : void
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setParentCheckboxLabel', array('label' => $label), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f->setParentCheckboxLabel($label);
return;
    }
    public function getTree() : \Oro\Bundle\ConfigBundle\Config\Tree\GroupNodeDefinition
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getTree', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getTree();
    }
    public function getJsTree() : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getJsTree', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getJsTree();
    }
    public function getApiTree(?string $path = null) : ?\Oro\Bundle\ConfigBundle\Config\ApiTree\SectionDefinition
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getApiTree', array('path' => $path), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getApiTree($path);
    }
    public function getSubTree(string $subTreeName) : \Oro\Bundle\ConfigBundle\Config\Tree\GroupNodeDefinition
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getSubTree', array('subTreeName' => $subTreeName), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getSubTree($subTreeName);
    }
    public function getDataTransformer(string $key) : ?\Oro\Bundle\ConfigBundle\Config\DataTransformerInterface
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDataTransformer', array('key' => $key), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDataTransformer($key);
    }
    public function getForm(string $groupName, \Oro\Bundle\ConfigBundle\Config\ConfigManager $configManager) : \Symfony\Component\Form\FormInterface
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getForm', array('groupName' => $groupName, 'configManager' => $configManager), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getForm($groupName, $configManager);
    }
    public function chooseActiveGroups(?string $activeGroup, ?string $activeSubGroup) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'chooseActiveGroups', array('activeGroup' => $activeGroup, 'activeSubGroup' => $activeSubGroup), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->chooseActiveGroups($activeGroup, $activeSubGroup);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->configBag, $instance->translator, $instance->formFactory, $instance->formRegistry, $instance->authorizationChecker, $instance->searchProvider, $instance->featureChecker, $instance->eventDispatcher, $instance->processedTrees, $instance->processedJsTrees, $instance->processedSubTrees);
        \Closure::bind(function (\Oro\Bundle\UserBundle\Provider\UserConfigurationFormProvider $instance) {
            unset($instance->parentCheckboxLabel);
        }, $instance, 'Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\ConfigBundle\Config\ConfigBag $configBag, \Symfony\Contracts\Translation\TranslatorInterface $translator, \Symfony\Component\Form\FormFactoryInterface $factory, \Symfony\Component\Form\FormRegistryInterface $formRegistry, \Symfony\Component\Security\Core\Authorization\AuthorizationCheckerInterface $authorizationChecker, \Oro\Bundle\ConfigBundle\Provider\ChainSearchProvider $searchProvider, \Oro\Bundle\FeatureToggleBundle\Checker\FeatureChecker $featureChecker, \Symfony\Component\EventDispatcher\EventDispatcherInterface $eventDispatcher)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->configBag, $this->translator, $this->formFactory, $this->formRegistry, $this->authorizationChecker, $this->searchProvider, $this->featureChecker, $this->eventDispatcher, $this->processedTrees, $this->processedJsTrees, $this->processedSubTrees);
        \Closure::bind(function (\Oro\Bundle\UserBundle\Provider\UserConfigurationFormProvider $instance) {
            unset($instance->parentCheckboxLabel);
        }, $this, 'Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($configBag, $translator, $factory, $formRegistry, $authorizationChecker, $searchProvider, $featureChecker, $eventDispatcher);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->configBag, $this->translator, $this->formFactory, $this->formRegistry, $this->authorizationChecker, $this->searchProvider, $this->featureChecker, $this->eventDispatcher, $this->processedTrees, $this->processedJsTrees, $this->processedSubTrees);
        \Closure::bind(function (\Oro\Bundle\UserBundle\Provider\UserConfigurationFormProvider $instance) {
            unset($instance->parentCheckboxLabel);
        }, $this, 'Oro\\Bundle\\UserBundle\\Provider\\UserConfigurationFormProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('UserConfigurationFormProvider_6e2dd1e', false)) {
    \class_alias(__NAMESPACE__.'\\UserConfigurationFormProvider_6e2dd1e', 'UserConfigurationFormProvider_6e2dd1e', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/platform/src/Oro/Bundle/WorkflowBundle/Field/FieldProvider.php';
class FieldProvider_084be6a extends \Oro\Bundle\WorkflowBundle\Field\FieldProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function setEntityProvider(\Oro\Bundle\EntityBundle\Provider\EntityProvider $entityProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setEntityProvider', array('entityProvider' => $entityProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setEntityProvider($entityProvider);
    }
    public function setVirtualFieldProvider(\Oro\Bundle\EntityBundle\Provider\VirtualFieldProviderInterface $virtualFieldProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setVirtualFieldProvider', array('virtualFieldProvider' => $virtualFieldProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setVirtualFieldProvider($virtualFieldProvider);
    }
    public function setVirtualRelationProvider($virtualRelationProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setVirtualRelationProvider', array('virtualRelationProvider' => $virtualRelationProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setVirtualRelationProvider($virtualRelationProvider);
    }
    public function setExclusionProvider(\Oro\Bundle\EntityBundle\Provider\ExclusionProviderInterface $exclusionProvider)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setExclusionProvider', array('exclusionProvider' => $exclusionProvider), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setExclusionProvider($exclusionProvider);
    }
    public function enableCaching($enable = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'enableCaching', array('enable' => $enable), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->enableCaching($enable);
    }
    public function getRelations($entityName, $withEntityDetails = false, $applyExclusions = true, $translate = true)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getRelations', array('entityName' => $entityName, 'withEntityDetails' => $withEntityDetails, 'applyExclusions' => $applyExclusions, 'translate' => $translate), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getRelations($entityName, $withEntityDetails, $applyExclusions, $translate);
    }
    public function getEntityFields(string $entityName, int $options = 192) : array
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEntityFields', array('entityName' => $entityName, 'options' => $options), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEntityFields($entityName, $options);
    }
    public function setLocale($locale)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setLocale', array('locale' => $locale), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setLocale($locale);
    }
    public function getLocale()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getLocale', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getLocale();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->entityProvider, $instance->virtualFieldProvider, $instance->virtualRelationProvider, $instance->exclusionProvider, $instance->entityConfigProvider, $instance->extendConfigProvider, $instance->entityClassResolver, $instance->fieldTypeHelper, $instance->translator, $instance->doctrine, $instance->hiddenFields, $instance->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $instance, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $entityConfigProvider, \Oro\Bundle\EntityConfigBundle\Provider\ConfigProvider $extendConfigProvider, \Oro\Bundle\EntityBundle\ORM\EntityClassResolver $entityClassResolver, \Oro\Bundle\EntityExtendBundle\Extend\FieldTypeHelper $fieldTypeHelper, \Doctrine\Persistence\ManagerRegistry $doctrine, \Symfony\Contracts\Translation\TranslatorInterface $translator, $hiddenFields)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\WorkflowBundle\\Field\\FieldProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->entityProvider, $this->virtualFieldProvider, $this->virtualRelationProvider, $this->exclusionProvider, $this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->fieldTypeHelper, $this->translator, $this->doctrine, $this->hiddenFields, $this->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($entityConfigProvider, $extendConfigProvider, $entityClassResolver, $fieldTypeHelper, $doctrine, $translator, $hiddenFields);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\WorkflowBundle\\Field\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\WorkflowBundle\\Field\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\WorkflowBundle\\Field\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\WorkflowBundle\\Field\\FieldProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->entityProvider, $this->virtualFieldProvider, $this->virtualRelationProvider, $this->exclusionProvider, $this->entityConfigProvider, $this->extendConfigProvider, $this->entityClassResolver, $this->fieldTypeHelper, $this->translator, $this->doctrine, $this->hiddenFields, $this->locale);
        \Closure::bind(function (\Oro\Bundle\EntityBundle\Provider\EntityFieldProvider $instance) {
            unset($instance->isCachingEnabled, $instance->metadata, $instance->metadataForEntitiesWithAssociations);
        }, $this, 'Oro\\Bundle\\EntityBundle\\Provider\\EntityFieldProvider')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('FieldProvider_084be6a', false)) {
    \class_alias(__NAMESPACE__.'\\FieldProvider_084be6a', 'FieldProvider_084be6a', false);
}

include_once \dirname(__DIR__, 4).'/vendor/oro/crm-zendesk/Model/EntityProvider/OroEntityProvider.php';
class OroEntityProvider_ae209d7 extends \Oro\Bundle\ZendeskBundle\Model\EntityProvider\OroEntityProvider implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getDefaultUser(\Oro\Bundle\IntegrationBundle\Entity\Channel $channel)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getDefaultUser', array('channel' => $channel), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getDefaultUser($channel);
    }
    public function getUser(\Oro\Bundle\ZendeskBundle\Entity\User $user, $defaultIfNotExist = false)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getUser', array('user' => $user, 'defaultIfNotExist' => $defaultIfNotExist), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getUser($user, $defaultIfNotExist);
    }
    public function getContact(\Oro\Bundle\ZendeskBundle\Entity\User $user)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getContact', array('user' => $user), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getContact($user);
    }
    public function getChannelById($channelId)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getChannelById', array('channelId' => $channelId), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getChannelById($channelId);
    }
    public function getEnabledChannels()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEnabledChannels', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEnabledChannels();
    }
    public function getEnabledTwoWaySyncChannels()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getEnabledTwoWaySyncChannels', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getEnabledTwoWaySyncChannels();
    }
    public function getAccountByContact(\Oro\Bundle\ContactBundle\Entity\Contact $contact)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getAccountByContact', array('contact' => $contact), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getAccountByContact($contact);
    }
    public function getCaseById($id)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getCaseById', array('id' => $id), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getCaseById($id);
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        unset($instance->registry, $instance->namePrefixes, $instance->nameSuffixes);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(\Doctrine\Persistence\ManagerRegistry $registry, array $namePrefixes, array $nameSuffixes)
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Oro\\Bundle\\ZendeskBundle\\Model\\EntityProvider\\OroEntityProvider');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        unset($this->registry, $this->namePrefixes, $this->nameSuffixes);
        }
        $this->valueHolder6655f->__construct($registry, $namePrefixes, $nameSuffixes);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ZendeskBundle\\Model\\EntityProvider\\OroEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ZendeskBundle\\Model\\EntityProvider\\OroEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ZendeskBundle\\Model\\EntityProvider\\OroEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Oro\\Bundle\\ZendeskBundle\\Model\\EntityProvider\\OroEntityProvider');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        unset($this->registry, $this->namePrefixes, $this->nameSuffixes);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('OroEntityProvider_ae209d7', false)) {
    \class_alias(__NAMESPACE__.'\\OroEntityProvider_ae209d7', 'OroEntityProvider_ae209d7', false);
}

include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/SessionBagInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/Flash/FlashBagInterface.php';
include_once \dirname(__DIR__, 4).'/vendor/symfony/http-foundation/Session/Flash/FlashBag.php';
class FlashBag_4fa968b extends \Symfony\Component\HttpFoundation\Session\Flash\FlashBag implements \ProxyManager\Proxy\VirtualProxyInterface
{
    private $valueHolder6655f = null;
    private $initializer1c70a = null;
    private static $publicProperties95c30 = [
        
    ];
    public function getName()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getName', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getName();
    }
    public function setName(string $name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setName', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setName($name);
    }
    public function initialize(array &$flashes)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initialize', array('flashes' => $flashes), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->initialize($flashes);
    }
    public function add(string $type, $message)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'add', array('type' => $type, 'message' => $message), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->add($type, $message);
    }
    public function peek(string $type, array $default = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'peek', array('type' => $type, 'default' => $default), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->peek($type, $default);
    }
    public function peekAll()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'peekAll', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->peekAll();
    }
    public function get(string $type, array $default = [])
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'get', array('type' => $type, 'default' => $default), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->get($type, $default);
    }
    public function all()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'all', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->all();
    }
    public function set(string $type, $messages)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'set', array('type' => $type, 'messages' => $messages), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->set($type, $messages);
    }
    public function setAll(array $messages)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'setAll', array('messages' => $messages), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->setAll($messages);
    }
    public function has(string $type)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'has', array('type' => $type), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->has($type);
    }
    public function keys()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'keys', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->keys();
    }
    public function getStorageKey()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'getStorageKey', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->getStorageKey();
    }
    public function clear()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'clear', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return $this->valueHolder6655f->clear();
    }
    public static function staticProxyConstructor($initializer)
    {
        static $reflection;
        $reflection = $reflection ?? new \ReflectionClass(__CLASS__);
        $instance   = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Symfony\Component\HttpFoundation\Session\Flash\FlashBag $instance) {
            unset($instance->name, $instance->flashes, $instance->storageKey);
        }, $instance, 'Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag')->__invoke($instance);
        $instance->initializer1c70a = $initializer;
        return $instance;
    }
    public function __construct(string $storageKey = '_symfony_flashes')
    {
        static $reflection;
        if (! $this->valueHolder6655f) {
            $reflection = $reflection ?? new \ReflectionClass('Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag');
            $this->valueHolder6655f = $reflection->newInstanceWithoutConstructor();
        \Closure::bind(function (\Symfony\Component\HttpFoundation\Session\Flash\FlashBag $instance) {
            unset($instance->name, $instance->flashes, $instance->storageKey);
        }, $this, 'Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag')->__invoke($this);
        }
        $this->valueHolder6655f->__construct($storageKey);
    }
    public function & __get($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__get', ['name' => $name], $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        if (isset(self::$publicProperties95c30[$name])) {
            return $this->valueHolder6655f->$name;
        }
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $backtrace = debug_backtrace(false, 1);
            trigger_error(
                sprintf(
                    'Undefined property: %s::$%s in %s on line %s',
                    $realInstanceReflection->getName(),
                    $name,
                    $backtrace[0]['file'],
                    $backtrace[0]['line']
                ),
                \E_USER_NOTICE
            );
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name) {
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __set($name, $value)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__set', array('name' => $name, 'value' => $value), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            $targetObject->$name = $value;
            return $targetObject->$name;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function & () use ($targetObject, $name, $value) {
            $targetObject->$name = $value;
            return $targetObject->$name;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = & $accessor();
        return $returnValue;
    }
    public function __isset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__isset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            return isset($targetObject->$name);
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            return isset($targetObject->$name);
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $returnValue = $accessor();
        return $returnValue;
    }
    public function __unset($name)
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__unset', array('name' => $name), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $realInstanceReflection = new \ReflectionClass('Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag');
        if (! $realInstanceReflection->hasProperty($name)) {
            $targetObject = $this->valueHolder6655f;
            unset($targetObject->$name);
            return;
        }
        $targetObject = $this->valueHolder6655f;
        $accessor = function () use ($targetObject, $name) {
            unset($targetObject->$name);
            return;
        };
        $backtrace = debug_backtrace(true, 2);
        $scopeObject = isset($backtrace[1]['object']) ? $backtrace[1]['object'] : new \ProxyManager\Stub\EmptyClassStub();
        $accessor = $accessor->bindTo($scopeObject, get_class($scopeObject));
        $accessor();
    }
    public function __clone()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__clone', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        $this->valueHolder6655f = clone $this->valueHolder6655f;
    }
    public function __sleep()
    {
        $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, '__sleep', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
        return array('valueHolder6655f');
    }
    public function __wakeup()
    {
        \Closure::bind(function (\Symfony\Component\HttpFoundation\Session\Flash\FlashBag $instance) {
            unset($instance->name, $instance->flashes, $instance->storageKey);
        }, $this, 'Symfony\\Component\\HttpFoundation\\Session\\Flash\\FlashBag')->__invoke($this);
    }
    public function setProxyInitializer(\Closure $initializer = null) : void
    {
        $this->initializer1c70a = $initializer;
    }
    public function getProxyInitializer() : ?\Closure
    {
        return $this->initializer1c70a;
    }
    public function initializeProxy() : bool
    {
        return $this->initializer1c70a && ($this->initializer1c70a->__invoke($valueHolder6655f, $this, 'initializeProxy', array(), $this->initializer1c70a) || 1) && $this->valueHolder6655f = $valueHolder6655f;
    }
    public function isProxyInitialized() : bool
    {
        return null !== $this->valueHolder6655f;
    }
    public function getWrappedValueHolderValue()
    {
        return $this->valueHolder6655f;
    }
}

if (!\class_exists('FlashBag_4fa968b', false)) {
    \class_alias(__NAMESPACE__.'\\FlashBag_4fa968b', 'FlashBag_4fa968b', false);
}
