<?php

namespace Symfony\Config;

use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

/**
 * This class is automatically generated to help in creating a config.
 */
class OroEntitySerializedFieldsConfig implements \Symfony\Component\Config\Builder\ConfigBuilderInterface
{
    private $dbalTypes;
    private $_usedProperties = [];

    /**
     * @param ParamConfigurator|list<mixed|ParamConfigurator> $value
     * @return $this
     */
    public function dbalTypes($value): self
    {
        $this->_usedProperties['dbalTypes'] = true;
        $this->dbalTypes = $value;

        return $this;
    }

    public function getExtensionAlias(): string
    {
        return 'oro_entity_serialized_fields';
    }

    public function __construct(array $value = [])
    {
        if (array_key_exists('dbal_types', $value)) {
            $this->_usedProperties['dbalTypes'] = true;
            $this->dbalTypes = $value['dbal_types'];
            unset($value['dbal_types']);
        }

        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }

    public function toArray(): array
    {
        $output = [];
        if (isset($this->_usedProperties['dbalTypes'])) {
            $output['dbal_types'] = $this->dbalTypes;
        }

        return $output;
    }

}
