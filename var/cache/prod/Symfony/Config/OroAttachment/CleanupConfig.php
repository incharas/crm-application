<?php

namespace Symfony\Config\OroAttachment;

use Symfony\Component\Config\Loader\ParamConfigurator;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;

/**
 * This class is automatically generated to help in creating a config.
 */
class CleanupConfig 
{
    private $collectAttachmentFilesBatchSize;
    private $loadExistingAttachmentsBatchSize;
    private $loadAttachmentsBatchSize;
    private $_usedProperties = [];

    /**
     * The number of attachment files that can be loaded from the filesystem at once.
     * @default 20000
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function collectAttachmentFilesBatchSize($value): self
    {
        $this->_usedProperties['collectAttachmentFilesBatchSize'] = true;
        $this->collectAttachmentFilesBatchSize = $value;

        return $this;
    }

    /**
     * The number of attachment entities that can be loaded from the database at once to check whether an attachment file is linked to an attachment entity.
     * @default 500
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function loadExistingAttachmentsBatchSize($value): self
    {
        $this->_usedProperties['loadExistingAttachmentsBatchSize'] = true;
        $this->loadExistingAttachmentsBatchSize = $value;

        return $this;
    }

    /**
     * The number of attachment entities that can be loaded from the database at once.
     * @default 10000
     * @param ParamConfigurator|mixed $value
     * @return $this
     */
    public function loadAttachmentsBatchSize($value): self
    {
        $this->_usedProperties['loadAttachmentsBatchSize'] = true;
        $this->loadAttachmentsBatchSize = $value;

        return $this;
    }

    public function __construct(array $value = [])
    {
        if (array_key_exists('collect_attachment_files_batch_size', $value)) {
            $this->_usedProperties['collectAttachmentFilesBatchSize'] = true;
            $this->collectAttachmentFilesBatchSize = $value['collect_attachment_files_batch_size'];
            unset($value['collect_attachment_files_batch_size']);
        }

        if (array_key_exists('load_existing_attachments_batch_size', $value)) {
            $this->_usedProperties['loadExistingAttachmentsBatchSize'] = true;
            $this->loadExistingAttachmentsBatchSize = $value['load_existing_attachments_batch_size'];
            unset($value['load_existing_attachments_batch_size']);
        }

        if (array_key_exists('load_attachments_batch_size', $value)) {
            $this->_usedProperties['loadAttachmentsBatchSize'] = true;
            $this->loadAttachmentsBatchSize = $value['load_attachments_batch_size'];
            unset($value['load_attachments_batch_size']);
        }

        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }

    public function toArray(): array
    {
        $output = [];
        if (isset($this->_usedProperties['collectAttachmentFilesBatchSize'])) {
            $output['collect_attachment_files_batch_size'] = $this->collectAttachmentFilesBatchSize;
        }
        if (isset($this->_usedProperties['loadExistingAttachmentsBatchSize'])) {
            $output['load_existing_attachments_batch_size'] = $this->loadExistingAttachmentsBatchSize;
        }
        if (isset($this->_usedProperties['loadAttachmentsBatchSize'])) {
            $output['load_attachments_batch_size'] = $this->loadAttachmentsBatchSize;
        }

        return $output;
    }

}
