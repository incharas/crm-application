<?php

namespace Symfony\Config;

require_once __DIR__.\DIRECTORY_SEPARATOR.'OroEntity'.\DIRECTORY_SEPARATOR.'EntityNameRepresentationConfig.php';

use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\Config\Loader\ParamConfigurator;

/**
 * This class is automatically generated to help in creating a config.
 */
class OroEntityConfig implements \Symfony\Component\Config\Builder\ConfigBuilderInterface
{
    private $entityNameRepresentation;
    private $defaultQueryCacheLifetime;
    private $_usedProperties = [];

    public function entityNameRepresentation(string $class, array $value = []): \Symfony\Config\OroEntity\EntityNameRepresentationConfig
    {
        if (!isset($this->entityNameRepresentation[$class])) {
            $this->_usedProperties['entityNameRepresentation'] = true;
            $this->entityNameRepresentation[$class] = new \Symfony\Config\OroEntity\EntityNameRepresentationConfig($value);
        } elseif (1 < \func_num_args()) {
            throw new InvalidConfigurationException('The node created by "entityNameRepresentation()" has already been initialized. You cannot pass values the second time you call entityNameRepresentation().');
        }

        return $this->entityNameRepresentation[$class];
    }

    /**
     * Default doctrine`s query cache lifetime
     * @default null
     * @param ParamConfigurator|int $value
     * @return $this
     */
    public function defaultQueryCacheLifetime($value): self
    {
        $this->_usedProperties['defaultQueryCacheLifetime'] = true;
        $this->defaultQueryCacheLifetime = $value;

        return $this;
    }

    public function getExtensionAlias(): string
    {
        return 'oro_entity';
    }

    public function __construct(array $value = [])
    {
        if (array_key_exists('entity_name_representation', $value)) {
            $this->_usedProperties['entityNameRepresentation'] = true;
            $this->entityNameRepresentation = array_map(function ($v) { return new \Symfony\Config\OroEntity\EntityNameRepresentationConfig($v); }, $value['entity_name_representation']);
            unset($value['entity_name_representation']);
        }

        if (array_key_exists('default_query_cache_lifetime', $value)) {
            $this->_usedProperties['defaultQueryCacheLifetime'] = true;
            $this->defaultQueryCacheLifetime = $value['default_query_cache_lifetime'];
            unset($value['default_query_cache_lifetime']);
        }

        if ([] !== $value) {
            throw new InvalidConfigurationException(sprintf('The following keys are not supported by "%s": ', __CLASS__).implode(', ', array_keys($value)));
        }
    }

    public function toArray(): array
    {
        $output = [];
        if (isset($this->_usedProperties['entityNameRepresentation'])) {
            $output['entity_name_representation'] = array_map(function ($v) { return $v->toArray(); }, $this->entityNameRepresentation);
        }
        if (isset($this->_usedProperties['defaultQueryCacheLifetime'])) {
            $output['default_query_cache_lifetime'] = $this->defaultQueryCacheLifetime;
        }

        return $output;
    }

}
